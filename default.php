<?php
use common\exceptions\AIException;
use core\AI;
ini_set('display_errors', 'On');
error_reporting(E_ALL);
ob_start();


if(0 > version_compare(PHP_VERSION, '7.2')){
    //die("AI Requires PHP 7.2 or higher");
}

if (isset($_SERVER["OS"])) {
    if (strpos(strtolower($_SERVER["OS"]), "windows")!==false) {
        $_SERVER["DOCUMENT_ROOT"] = dirname(__FILE__);
    }
}

if(!isset($_SERVER["DOCUMENT_ROOT"]) || $_SERVER["DOCUMENT_ROOT"]==""){
    $_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__);
}


define ( "BaseDir", $_SERVER ["DOCUMENT_ROOT"] );
require_once BaseDir.'/declare.php';
require_once BaseDir . "/common/exceptions/AIException.php";

register_shutdown_function(function(){
    //ob_clean();
    if ($error = error_get_last ()) {
        $customError = "Error";
        switch ($error ['type']) {
            case E_ERROR :
                $customError = "Fatal run-time errors.";
                break;
            case E_WARNING:
                $customError = "Run-time warnings (non-fatal errors).";
                break;
            case E_PARSE :
                $customError = "Compile-time parse errors.";
                break;
            case E_NOTICE:
                $customError = "Run time notice caused due to error in code";
                break;
            case E_CORE_ERROR :
                $customError = "Fatal errors that occur during PHP's initial startup.";
                break;
            case E_CORE_WARNING:
                $customError = "Warnings that occur during PHP’s initial startup";
                break;
            case E_COMPILE_ERROR :
                $customError = "Fatal compile-time errors.";
                break;
            case E_COMPILE_WARNING:
                $customError = "Compile-time warnings (non-fatal errors).";
                break;
            case E_USER_ERROR :
                $customError = "User-generated error message.";
                break;
            case E_USER_WARNING:
                $customError = "User-generated warning message.";
                break;
            case E_USER_NOTICE:
                $customError = "User-generated notice message";
                break;
            case E_STRICT:
                $customError = "Run-time notices";
                break;
            default:
                $customError = "Cannot identify the error type, good luck :(";
                break;
        }

        if(isset($_COOKIE["idExecution"])){
            $ai = AI::getInstance();
            $ai->getAIDAO()->saveEndExecution($_COOKIE["idExecution"], Functions::currentDate(), AI::FAIL);
        }
        //Functions::addLog($customError, Functions::SEVERE, json_encode($error));
        $ex = AIException::createInstanceFromErrorHandler(0, $error["message"], $error["file"], $error["line"])->construct("register_shudown_function", "default", array(), array(
            "error" => $error
        ));
        Functions::logException($ex->getMessage(), Functions::SEVERE, "Default.php", $ex);

        //TODO :: Podria ser interesante crear un proceso que determine si es encesario hacer un clean/reinicializar aquellos procesos que se han quedado en un estado no valido, limpiar ficheros, etc...


        $aipid = "-";
        if(isset($_COOKIE["idExecution"])){
            $aipid = $_COOKIE["idExecution"];
        }
        Functions::console("AI:: Fatal error: $customError. System exit. PID: ".$aipid);
        die();
    }

});



set_error_handler(function ($errNum, $errMessage, $errFile, $errLine ) {
    //if (error_reporting()) {
        throw AIException::createInstanceFromErrorHandler($errNum, $errMessage, $errFile, $errLine);
    //}
});

global $setup;

define("FILEPATH", $setup->repository);

$ai = AI::getInstance($setup);


//Prevent HTTP script execution on production environment.
//Redirect - 302 - to base url.
if(isset($_SERVER["HTTP_HOST"]) && !AI::getInstance()->isDev()){
	header("Location: http://".$_SERVER['SERVER_NAME']);
	die();
}

$ai->init();
$ai->setStartExecution(Functions::currentDate());

if ($ai->getAIDAO()->isExecuting(AI::INQUEUE)){
    Functions::console("AI::Other process still executing");
    die();
}


$idExecution = $ai->getAIDAO()->saveExecution($ai->getStartExecution(), AI::INQUEUE);
if($idExecution == null){
    Functions::console("AI::Can't save start execution");
    die();
}
$_COOKIE["idExecution"] = $idExecution;
$ai->setIdExecution($idExecution);
Functions::console("AI::Hello. Start executing system. PID: ".$idExecution);

try{
    Functions::console("AI::Start Inqueue");
    $ai->inqueue();
    Functions::console("AI::End Inqueue");
    Functions::console("AI::Clean up unconfirmed tasks");
    $ai->cleanUpTask();
    $ai->getAIDAO()->saveEndExecution($ai->getIdExecution(), Functions::currentDate(), AI::SUCCESSFULL);
    Functions::console("AI::Stopping system. Bye");
}
catch(AIException $aiex){
    Functions::logException($aiex->getMessage(), Functions::ERROR, "Default.php", $aiex);
    $ai->getAIDAO()->saveEndExecution($ai->getIdExecution(), Functions::currentDate(), AI::FAIL);
    echo "AI ERROR MESSAGE: ".$aiex->getMessage().", ON FILE: ". $aiex->getFile().":".$aiex->getLine();
}
catch (Throwable $e){
    $ex = AIException::createInstanceFromThrowable($e)->construct("Global", "Default");
    Functions::logException($e->getMessage(), Functions::ERROR, "Default.php", $ex);
    $ai->getAIDAO()->saveEndExecution($ai->getIdExecution(), Functions::currentDate(), AI::FAIL);
    echo "GENERAL ERROR MESSAGE: ".$e->getMessage().", ON FILE: ". $e->getFile().":".$e->getLine();
}
?>

