<?php

namespace preprocessorModule\preprocessors;

use common\exceptions\AIException;
use Functions;
use model\Task;
use model\TaskAditionalProperties;
use ZipArchive;
use harvestModule\sources\sourceComplements\GlobalSightService;
use harvestModule\sources\StructureSource;
use model\File;
use model\Analysis;
use model\State;
use model\TM;
use model\Language;

include_once BaseDir . '/simple_html_dom.php';
include_once BaseDir . '/model/Task.php';
include_once BaseDir . '/harvestModule/sources/sourceComplements/GlobalSightService.php';
class TripAdvisor {
    private const TMSCONNECTOR = "TMSCONNECTOR+ACTION";
    private const EMPTY = "EMPTY";
    private $globalSight;
    private $totalJobs;

    public function __construct(){

        $this->globalSight = new GlobalSightService("https://w11.globalsight.com", "trn2840", "tkxnnfAd");
    }

    public function executePreprocessor(Task $task){

        $newTasks = array();
        $jobsInfo = array();

            // check if the message is valid to be parsed
            if ($task->getMenssage() !== "" && !strcmp(self::TMSCONNECTOR, strtoupper($task->getMenssage())) == 0 && $task->getMenssage() !== null) {
                $task->setState(new State(State::PREPROCESSING));
                try {
                    $jobsInfo = $this->parseMessage($task->getMenssage());
                } catch (AIException $e) {
                    Functions::logException($e->getMessage(), Functions::WARNING, __CLASS__, $e);
                    $newTasks[] = $task;
                    return $newTasks;
                }
            }

            if (!empty($jobsInfo)) {
                $this->totalJobs = count($jobsInfo);
                $jobGroups = $this->groupJobs($jobsInfo);
                $success = $this->globalSight->login();
                // Generate task objects
                if ($success) {
                    foreach($jobGroups as $group => $jobs){
                        if(!empty($group)){

                            $taskIds = array();
                            $jobsId = "";
                            $targetLanguage = str_replace("-", "_", $task->getTargetsLanguage()[0]->getIdLanguage());
                            $GSJobs = array();
                            foreach($jobs as $job){
                                $GBtask = $this->globalSight->getSingleTask($job[0], GlobalSightService::TASK_STATUS_AVAILABLE);
                                if ($GBtask !== null) {
                                    $taskIds[] = $GBtask['taskId'];
                                    $GSJobs[] = $job;
                                    $jobsId .= $job[0] . "_";
                                }
                            }
                            $currentDate = new \DateTime();
                            $startDate = $task->getStartDate();
                            $diff = $currentDate->diff($startDate);
                            $minutes = $diff->h*60;
                            $minutes += $diff->i;
                            if (count($jobs) === count($taskIds) || $minutes >= 40) {
                                $title = $this->getTaskTitle($GSJobs, $jobsId);
                                $isInsights = (stripos($title,"INSIGHTS")!=false)?true:false;
                                $filesToTranslate = $this->getFilesToTranslate($GSJobs, $taskIds, $targetLanguage, $jobsId, $isInsights);
                                $referenceFiles = $this->getReferenceFiles($GSJobs, $taskIds, $jobsId, $task->getMenssage(), $title);
                                $properties = explode(":", strtoupper($group));
                                $additionalProperties = $this->getAdditionalProperties($properties[0], $properties[1], $properties[2], $GSJobs, $isInsights);
                                if (!empty($filesToTranslate) && !empty($referenceFiles) && $task->getDataSource() !== null) {
                                    $newTasks[] = $this->generateTask($task, $title, $additionalProperties, $filesToTranslate, $referenceFiles, $taskIds, $targetLanguage);
                                }
                            }
                        }
                    }
                } else {
                    $e = (new AIException("Could not connect to Global Sight"))->construct(__METHOD__,__NAMESPACE__, $args = func_get_args());
                    Functions::logException($e->getMessage(), Functions::WARNING, __CLASS__, $e);
                }
            } else {
                // no encontró los jobs
                $e = (new AIException("Could not parse message"))->construct(__METHOD__, __NAMESPACE__, $args = func_get_args(), array(
                   "taskUSID" =>  $task->getUniqueSourceId()
                ));
                Functions::logException("Could not parse message", Functions::INFO, __CLASS__, $e);
                $task->setState(new State(State::PREPROCESSED));
                $newTasks[] = $task;
            }


//        \Functions::print($newTasks);
        return $newTasks;
    }

    /**
     * Group the jobs by businessTo, generalType and contentType and returns an associative array
     * @param $jobsInfo
     * @return array
     */
    public function groupJobs($jobsInfo){
        $jobGroups = array();
        foreach ($jobsInfo as $job) {
            if(isset($job[1])) {
                if (preg_match("/.*?(B2B)/", $job[1])) {
                    if (preg_match("/.*?(VIATOR).*?(LEGAL)/", $job[1])) {
                        $jobGroups['b2b:Viator:Legal'][] = $job;
                        continue;
                    } else {
                        if (preg_match("/.*?(VIATOR)/", $job[1])) {
                            $jobGroups['b2b:Viator:Other'] [] = $job;
                            continue;
                        }
                    }
                    if (preg_match("/.*?(THEFORK).*?(LEGAL)/", $job[1])) {
                        $jobGroups['b2b:TheFork:Legal'][] = $job;
                        continue;
                    } else {
                        if (preg_match("/.*?(THEFORK)/", $job[1])) {
                            $jobGroups['b2b:TheFork:Other'][] = $job;
                            continue;
                        }
                    }
                    if (preg_match("/.*?(LEGAL)/", $job[1])) {
                        $jobGroups['b2b:Legal:Other'][] = $job;
                        continue;
                    } else {
                        if (preg_match("/.*?(B2B)/", $job[1])) {
                            $jobGroups['b2b:Other:Other'][] = $job;
                            continue;
                        }
                    }
                } else if (preg_match("/.*?(B2C)/", $job[1])) {
                    if (preg_match("/.*?(VIATOR).*?(LEGAL)/", $job[1])) {
                        $jobGroups['b2c:Viator:Legal'][] = $job;
                        continue;
                    } else {
                        if (preg_match("/.*?(VIATOR)/", $job[1])) {
                            $jobGroups['b2c:Viator:Other'][] = $job;
                            continue;
                        }
                    }
                    if (preg_match("/.*?(THEFORK).*?(LEGAL)/", $job[1])) {
                        $jobGroups['b2c:TheFork:Legal'][] = $job;
                        continue;
                    } else {
                        if (preg_match("/.*?(THEFORK)/", $job[1])) {
                            $jobGroups['b2c:TheFork:Other'][] = $job;
                            continue;
                        }
                    }
                    if (preg_match("/.*?(LEGAL)/", $job[1])) {
                        $jobGroups['b2c:Legal:Other'][] = $job;
                        continue;
                    } else {
                        if (preg_match("/.*?(B2C)/", $job[1])) {
                            $jobGroups['b2c:Other:Other'][] = $job;
                            continue;
                        }
                    }
                }
            }
        }
        return $jobGroups;
    }

    public function defaultPreprocess(Task $task){

        $newTasks = array();
        $newTasks[] = $task;
        return $newTasks;
    }

    /**
     * *
     * Generates a new Task object with some things of the original task and adding or modifying others, returns a single task object
     * Just like the TTS do, if there is a single task the wordcount will be the provided by falcon if it a grouped task the word count
     * will be from global sight
     *
     * @param Task $task
     * @param string $title
     * @param array $additionalProperties
     * @param array $files
     * @param array $referenceFiles
     * @param array $taskIds
     * @param string $targetLanguage
     * @return \model\Task
     */
    public function generateTask(Task $task, string $title, array $additionalProperties, array $files, array $referenceFiles, array $taskIds, string $targetLanguage){

        $newTask = new Task();
        $newTask->setStartDate($task->getStartDate());
        $newTask->setTitle($title);
        $newTask->setIdClientetask($task->getIdClientetask());
        $newTask->setDueDate($task->getDueDate());
        $newTask->setAssignedUser($task->getAssignedUser());
        $newTask->setCategory($task->getCategory());
        $newTask->setSourceLanguage($task->getSourceLanguage());
        $newTask->setStatusHistory($task->getStatusHistory());
        $newTask->setState(new State(State::PREPROCESSED));
        $newTask->setDataSource($task->getDataSource());
        $properties = $task->getAditionalProperties();
        $newTask->setAditionalProperties(array_merge($properties, $additionalProperties));
        $target = new Language();
        $target->setIdLanguage($task->getTargetsLanguage()[0]->getIdLanguage());
        $target->setLanguageName($task->getTargetsLanguage()[0]->getLanguageName());
        $analysis = $task->getTargetsLanguage()[0]->getAnalysis();
        $newTask->setTranslationMemories($this->getTranslationMemories($files, $task->getSourceLanguage(), $target));
        $newTask->setTargetsLanguage(array($target));
        if ($this->totalJobs > 1) {
            $newTask->getTargetsLanguage()[0]->setAnalysis($this->getTaskWordcount($taskIds, $targetLanguage));
            //$newTask->setAnalysis($this->getTaskWordcount($taskIds, $targetLanguage));

        }else{
            $newTask->getTargetsLanguage()[0]->setAnalysis($analysis);
        }
        $newTask->setFilesToTranslate($files);
        $newTask->setFilesForReference($referenceFiles);
        $newTask->setMenssage(urldecode($task->getMenssage()));
        $newTask->setRelatedOriginalData($task->getDataSource()->getOriginalData());
        $newTask->generateUniqueTaskId();
        $newTask->setTimeStamp(Functions::currentDate());
        $newTask->generateUniqueSourceId();
        return $newTask;
    }

    /**
     * *
     * Generates the additional properties for a task given the businessTo, generalType and contentType, returns an array containig
     * the additional properties
     *
     * @param string $businessTo
     * @param string $generalType
     * @param string $contentType
     * @param array $jobs
     * @return \model\TaskAditionalProperties[]
     */
    public function getAdditionalProperties(string $businessTo, string $generalType, string $contentType, array $jobs, $isInsights=false){

        $additionalProperties = array();

        $property = new TaskAditionalProperties();
        $property->setPropertyName("trip_businessTo");
        $property->setPropertyValue($businessTo);
        $additionalProperties[] = $property;

        $property = new TaskAditionalProperties();
        $property->setPropertyName("trip_generalType");
        $property->setPropertyValue($generalType);
        $additionalProperties[] = $property;

        $property = new TaskAditionalProperties();
        $property->setPropertyName("trip_contentType");
        $property->setPropertyValue($contentType);
        $additionalProperties[] = $property;
        if($isInsights){
            $property = new TaskAditionalProperties();
            $property->setPropertyName("trip_rtf_file_name");
            Functions::print($this->globalSight->getRTFFile());
            $property->setPropertyValue($this->globalSight->getRTFFile());
            $additionalProperties[] = $property;
        }else{
            $property = new TaskAditionalProperties();
            $property->setPropertyName("trip_xlf_file_name");
            $property->setPropertyValue($this->globalSight->getXlfFile());
            $additionalProperties[] = $property;
        }


        $jobsNames = "";
        foreach($jobs as $job) {
            $jobsNames .= $job[0] . " " . $job[1] . "<br>";
        }

        $property = new TaskAditionalProperties();
        $property->setPropertyName("trip_jobName");
        $property->setPropertyValue($jobsNames);
        $additionalProperties[] = $property;

        $taskType = "";
        if (count($jobs) > 1) {
            $taskType = "(GS_Multiple)";
        } else {
            if (isset($jobs[0][0])) {
                $taskType = "(GS_" . $jobs[0][0] . ")";
            }
        }

        $property = new TaskAditionalProperties();
        $property->setPropertyName("trip_taskGroup");
        $property->setPropertyValue($taskType);
        $additionalProperties[] = $property;

        return $additionalProperties;
    }

    /**
     * *
     * Gets the files to translate using the GlobalSightService class, returns an array with the files if the download was successfull
     * returns an empty array otherwise.
     *
     * @param array $jobsInfo
     * @param array $taskIds
     * @param string $targetLanguage
     * @param
     *        	$jobsId
     * @return \model\File[]
     * @return
     */
    public function getFilesToTranslate(array $jobsInfo, array $taskIds, string $targetLanguage, $jobsId, $isInsights = false){

        $jobsId = rtrim($jobsId, "_");
        $jobs = explode("_", $jobsId);
        sort($jobs, SORT_NUMERIC);
        $firstJob = current($jobs);
        $lastJob = end($jobs);
        $jobName = $firstJob . "-" . $lastJob;
        $path = $this->getDownloadPath($jobName, "GlobalSight");
        $fileName = "Jobs_" . $jobName . "_" . $targetLanguage . ".zip";
        $filesToTranslate = array();
        if(!file_exists($path.$fileName)){
            $file = null;
            $sourceFile = null;
            $sourceFile = $this->globalSight->getTasksFilesCombined($taskIds, $path, $fileName, $targetLanguage);

            if ($sourceFile !== null) {
                $file = new File();
                $file->setFileName($sourceFile['fileName']);
                $file->setFileNameExtension($sourceFile['extension']);
                $file->setMimeType($sourceFile['mime']);
                $file->setSize($sourceFile['size']);
                $file->setPath($sourceFile['path']);
                $file->setSourcePath($sourceFile['sourcePath']);
                $file->setChecksum("");
                $file->setFileType(File::SOURCE);
                $file->setSuccessfulDownload(1);
                $file->setPathForDownload($sourceFile['sourcePath']);
                $filesToTranslate[] = $file;
            }
            if($isInsights){
                @unlink($sourceFile['path']);
                $filesToTranslate = array();
                $totalTasks = count($taskIds);
                if($totalTasks == 1){
                    $sourceFile = $this->globalSight->downloadRTFFiles($taskIds[0],$path,$fileName);
                    $file = new File();
                    $file->setFileName($sourceFile['fileName']);
                    $file->setFileNameExtension($sourceFile['extension']);
                    $file->setMimeType($sourceFile['mime']);
                    $file->setSize($sourceFile['size']);
                    $file->setPath($sourceFile['path']);
                    $file->setSourcePath($sourceFile['sourcePath']);
                    $file->setChecksum("");
                    $file->setFileType(File::SOURCE);
                    $file->setSuccessfulDownload(1);
                    $file->setPathForDownload($sourceFile['sourcePath']);
                    $filesToTranslate[] = $file;
                }

            }
        }

        return $filesToTranslate;
    }

    /**
     * *
     * Gets the reference files using the GlobalSightService class, the files are downloaded and combined in a new zip archive
     * containing all the downloaded files inside a folder named Source, also contains the instructions file with the original message.
     * Returns an array with the new file if the file is damaged the returned array will be empty
     *
     * @param array $jobsInfo
     * @param array $taskIds
     * @param
     *        	$jobsId
     * @param string $message
     * @param string $title
     * @return array|\model\File[]
     */
    public function getReferenceFiles(array $jobsInfo, array $taskIds, $jobsId, string $message, string $title){

        $jobsId = rtrim($jobsId, "_");
        $jobs = explode("_", $jobsId);
        sort($jobs, SORT_NUMERIC);
        $firstJob = current($jobs);
        $lastJob = end($jobs);
        $jobName = $firstJob . "-" . $lastJob;
        $path = $this->getDownloadPath($jobName, "GlobalSight");
        $referenceFiles = array();
        $count = 0;
        // Get the files from global sight
        $fileName = "Ref_" . $title . ".zip";
        if (! file_exists($path . $fileName)) {
            foreach ($jobsInfo as $job) {
                if (isset($taskIds[$count])) {
                    $fileName = $job[1];
                    $fileName = str_replace("&nbsp;", "", $fileName);
                    $fileName = str_replace(" ", "_", $fileName);
                    $fileName = rtrim($fileName, "_");
                    $fileName .= ".zip";

                    if (!file_exists($path . $fileName)) {
                        $refFile = $this->globalSight->getTaskSourceFiles($taskIds[$count], $path, $fileName);
                        if ($refFile !== null) {
                            $newfile = new File();
                            $newfile->setFileName($refFile['fileName']);
                            $newfile->setFileNameExtension($refFile['extension']);
                            $newfile->setMimeType($refFile['mime']);
                            $newfile->setSize($refFile['size']);
                            $newfile->setPath($refFile['path']);
                            $newfile->setSourcePath($refFile['sourcePath']);
                            $newfile->setChecksum("");
                            $newfile->setSuccessfulDownload(1);
                            $newfile->setPathForDownload($refFile['sourcePath']);
                            $referenceFiles[] = $newfile;
                        }
                    }
                    //Reference files from comments
                    $fileName = $job[1];
                    $fileName = str_replace("&nbsp;", "", $fileName);
                    $fileName = str_replace(" ", "_", $fileName);
                    $fileName = rtrim($fileName, "_");
                    $fileName .= "comments.zip";

                    if (!file_exists($path . $fileName)) {
                        $path_reference = str_replace(FILEPATH, "", $path);
                        $refFile = $this->globalSight->getReferenceFilesFromComments($taskIds[$count], $jobs[$count], $path_reference, $path, $fileName);
                        if ($refFile !== null) {
                            $newfile = new File();
                            $newfile->setFileName($refFile['fileName']);
                            $newfile->setFileNameExtension($refFile['extension']);
                            $newfile->setMimeType($refFile['mime']);
                            $newfile->setSize($refFile['size']);
                            $newfile->setPath($refFile['path']);
                            $newfile->setSourcePath($refFile['sourcePath']);
                            $newfile->setChecksum("");
                            $newfile->setSuccessfulDownload(1);
                            $newfile->setPathForDownload($refFile['sourcePath']);
                            $referenceFiles[] = $newfile;
                        }
                    }

                }
                $count++;
            }
        }
        $countReference = count($referenceFiles);
        $countTaskIds = count($taskIds);
        // check if the number of files is equal to the number of task that were supposed to be downloaded
        if ($countReference < $countTaskIds) {
            $referenceFiles = array();
        } else {
            // Generate the new zip archive with all the downloaded files
            $fileName = "Ref_" . $title . ".zip";
            if (! file_exists($path . $fileName)) {

                $zip = new ZipArchive();
                $result = $zip->open($path . $fileName, ZipArchive::CREATE);
                if ($result === true) {
                    foreach($referenceFiles as $file) {
                        $zip->addFile($file->getPath(), "Source/" . $file->getFileName());
                    }
                    $zip->addFromString("instructions.html", urldecode($message));
                    $zip->close();
                    $newfile = new File();
                    $newfile->setFileName($fileName);
                    $newfile->setFileNameExtension("zip");
                    $newfile->setMimeType("application/zip");
                    $newfile->setSize(filesize($path . $fileName));
                    $newfile->setPath($path . $fileName);
                    $newfile->setSourcePath($path . $fileName);
                    $newfile->setChecksum("");
                    $newfile->setSuccessfulDownload(1);
                    $newfile->setPathForDownload($path . $fileName);
                    // check if everything is ok
                    $zip = new \ZipArchive();
                    $result = $zip->open($path . $fileName);
                    if ($result === true) {
                        foreach($referenceFiles as $file){
                            @unlink($file->getPath());
                        }
                        $referenceFiles = array();
                        $referenceFiles[] = $newfile;
                    } else {
                        @unlink($path . $fileName);
                    }
                }
            } else {
                $referenceFiles = array();
            }
        }

        return $referenceFiles;
    }

    /**
     * *
     * Gets the task wordcount using the GlobalSightService class
     *
     * @param array $taskIds
     * @param string $targetLanguage
     * @return \model\Analysis[]
     * @return
     */
    public function getTaskWordcount(array $taskIds, string $targetLanguage){
        $p101 = 0;
        $p100 = 0;
        $p95 = 0;
        $p85 = 0;
        $p75 = 0;
        $noMatch = 0;
        $repetitions = 0;
        $MT = 0;
        foreach($taskIds as $task) {
            $wc = $this->globalSight->getTaskWordCount($task, $targetLanguage);
            $p101 += $wc['inContextMatch'];
            $p100 += $wc['100'];
            $p95 += $wc['95'];
            $p85 += $wc['85'];
            $p75 += $wc['75'];
            $noMatch += $wc['noMatch'];
            $repetitions += $wc['repetitions'];
            if (isset($wc['MT'])) {
                $MT += $wc['MT'];
            }
        }

        $analysis = new Analysis();
        $analysis->setPercentage_101($p101);
        $analysis->setPercentage_100($p100);
        $analysis->setPercentage_95($p95);
        $analysis->setPercentage_85($p85);
        $analysis->setPercentage_75($p75);
        $analysis->setNotMatch($noMatch);
        $analysis->setRepetition($repetitions);
        $analysis->setMachineTanslation($MT);
        $analysis->calculateTotalWords();
        $analysis->calculateWeightedWords();
        return $analysis;
    }

    /**
     * *
     * Parse the message and get the jobs information (jobId and name)
     *
     * @param
     *            $message
     * @return string[][]
     * @return
     * @throws AIException
     */
    public function parseMessage($message){

        /* The message is stored with url format, first we need to decode it to get the html */
        $message = urldecode($message);
        $html = \str_get_html($message);
        // global array to store the jobs info
        $jobsInfo = array();
        /* This method will work for multiple or just one task inside a table tr tag */
        if($html === false){
            $ex = (new AIException("Cannot get HTML code from message"))->construct(__METHOD__, __NAMESPACE__, $func = func_get_args(), array("message" => $message));
            throw $ex;
        }
        $tRows = $html->find('table tr');

        foreach($tRows as $tr) {
            $job = array();
            $children = $tr->children();
            foreach($children as $child) {
                $split = explode(":", $child->plaintext);
                foreach($split as $s) {
                    // clean any return characters
                    $se = trim(preg_replace("[\n|\r|\n\r|\t|\x0B]", "", $s));

                    // check if the job id is present
                    if (is_numeric(substr($se, 0, 6))) {
                        $se = substr($se, 0, 6);
                    } else if(is_numeric(substr($se, 0, 5))){
                        $se = substr($se, 0, 5);
                    }
                    // for the single task format check if these strings are found, must not be stored in array
                    if (strpos(strtoupper($se), "JOB ID") === false && strpos(strtoupper($se), "PROJECT NAME") === false && strlen($se) > 0) {
                        $job[] = trim($se);
                    }
                }
            }
            // check if the job is useful and store it in global array
            if (isset($job[0]) && is_numeric($job[0])) {
                $jobsInfo[] = $job;
            }
        }
        return $jobsInfo;
        // print("<pre>" . print_r($jobsInfo, true) . "</pre>");
    }

    /**
     * *
     * Gets a path to store the files for each task
     *
     * @param string $jobsId
     * @param string $sourceName
     * @return string
     * @return
     */
    public function getDownloadPath(string $jobsId, string $sourceName){

        $filePath = FILEPATH . $sourceName . '/' . date("Ym") . '/' . $jobsId . '/';
        if (! file_exists($filePath)) {
            try {
                mkdir($filePath, 0760, true);
            } catch(\Exception $e) {
                Functions::addLog("Directory creation => " . $e->getMessage(), Functions::WARNING, json_encode($e->getTrace()));
            }
        }
        return $filePath;
    }

    /**
     * *
     * Generates the title for a task
     *
     * @param
     *        	$jobsInfo
     * @param
     *        	$jobsId
     * @return string
     */
    public function getTaskTitle($jobsInfo, $jobsId){

        $jobsId = rtrim($jobsId, "_");
        $title = "Task Title";
        if (count($jobsInfo) > 1) {
            $title = "Jobs[%JOBSID%]";
            $title = str_replace("%JOBSID%", $jobsId, $title);
        } else {
            if (isset($jobsInfo[0][1])) {
                $title = str_replace("&nbsp;", "", $jobsInfo[0][1]);
                $title = str_replace(" ", "_", $title);
                $title = rtrim($title, "_");
            }
        }
        return $title;
    }

    /**
     * Iterates over the files to translate and gets all the tmx files, returns an array of TM objects if the files where found
     * @param array $filesToTranslate
     * @param $source
     * @param $target
     * @param int $isMachineTrans
     * @return array
     */
    public function getTranslationMemories(array $filesToTranslate, $source, $target, $isMachineTrans=0){

        $taskTms = array();
        try {
            foreach($filesToTranslate as $file) {
                $zip = new \ZipArchive();
                $openResult = $zip->open($file->getPath());
                if ($openResult === true) {
                    $numFiles = $zip->numFiles;
                    for($i = 0; $i < $numFiles; $i ++) {
                        $fileName = $zip->getNameIndex($i);
                        if (preg_match('#\.(tmx)$#i', $fileName)) {
                            $fileInfo = pathinfo($fileName);
                            $zipFileInfo = pathinfo($file->getPath());
                            $tmName = $fileInfo['basename'];
                            $path = $zipFileInfo["dirname"];
                            copy("zip://" . $file->getPath() . "#" . $fileName, $path . "/" . $tmName);
                            $tmFile = new File();
                            $tmFile->setFileName($tmName);
                            $tmFile->setFileNameExtension($fileInfo['extension']);
                            $tmFile->setMimeType("text/xml");
                            $tmFile->setSize(filesize($path . "/" . $tmName));
                            $tmFile->setPath($path . "/" . $tmName);
                            $tmFile->setSourcePath($path . "/" . $tmName);
                            $tmFile->setChecksum("");
                            $tmFile->setSuccessfulDownload(1);
                            $tmFile->setPathForDownload($path . "/" . $tmName);

                            $tm = new TM();
                            $tm->setFile($tmFile);
                            $tm->setIsMachineTranslation($isMachineTrans);
                            $tm->setSourceLanguage($source);
                            $tm->setTargetLanguage($target);
                            $tm->setVersion("1");
                            $taskTms[] = $tm;
                        }
                    }
                }
                $zip->close();
            }
        } catch(\Exception $ex) {
            Functions::addLog($ex->getMessage(), Functions::WARNING, $ex->getTraceAsString());
        }

        // print("<pre>".print_r($taskTms, true)."</pre>");
        return $taskTms;
    }
}
?>