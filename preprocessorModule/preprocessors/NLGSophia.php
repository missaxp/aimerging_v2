<?php


namespace preprocessorModule\preprocessors;


use common\exceptions\AIException;
use core\AI;
use dataAccess\dao\TaskDAO;
use dbconn\SQLException;
use Exception;
use Functions;
use model\Analysis;
use model\State;
use model\Task;
use preprocessorModule\memoQ\MemoQFileManager;
use preprocessorModule\memoQ\MemoQProjectManager;
use preprocessorModule\memoQ\MemoQTmManager;

include_once BaseDir . "/common/exceptions/AIException.php";

class NLGSophia {

    public function __construct() {

    }

    public function executePreprocessor(Task $task) {
        if($task->getState()->getIdState() === State::COLLECTED && $task->getTargetsLanguage()[0]->getAnalysis()->getIdAnalis() !== null) {

            $states = array();
            $states[] = new State(State::PREPROCESSING);
            $sourceFiles = array();
            $translationMemoriesPath = null;
            $packageSourceFiles = array();
            foreach ($task->getFilesToTranslate() as $file) {
                if ($file->getFileNameExtension() == "sdlppx") {

                    $zip = new \ZipArchive();
                    $openResult = $zip->open($file->getPath());
                    if ($openResult === true) {

                        $numFiles = $zip->numFiles;
                        for ($i = 0; $i < $numFiles; $i++) {
                            $fileName = $zip->getNameIndex($i);
                            if (preg_match('#\.(sdlxliff)$#i', $fileName) || preg_match('#\.(sdltm)$#i', $fileName)) {
                                $fileInfo = pathinfo($fileName);
                                $zipFileInfo = pathinfo($file->getPath());
                                $unzippedFileName = $fileInfo['basename'];

                                $path = $zipFileInfo["dirname"];

                                $destPath = "";
                                if ($fileInfo['extension'] == "sdlxliff") {
                                    if (!file_exists($path . "/source")) {
                                        mkdir($path . "/source", 0760, true);
                                    }
                                    $destPath = $path . "/source/" . $unzippedFileName;
                                    if(!file_exists($destPath)){
                                        copy("zip://" . $file->getPath() . "#" . $fileName, $destPath);
                                        $packageSourceFiles[] = $destPath;
                                    }


                                }
                                if ($fileInfo['extension'] == "sdltm") {
                                    $translationMemoriesPath = $path . "/tm/";
                                    if (!file_exists($translationMemoriesPath)) {
                                        mkdir($translationMemoriesPath, 0760, true);
                                    }
                                    $date = new \DateTime();
                                    $newFileName = md5($unzippedFileName . filesize($file->getPath()) . $date->format("YmdHisu"));
                                    copy("zip://" . $file->getPath() . "#" . $fileName, $translationMemoriesPath . $newFileName . ".sdltm");

                                }

                            }
                        }
                    }
                }
                if (!empty($packageSourceFiles)) {
                    $sourceFiles = array_merge($sourceFiles, $packageSourceFiles);
                }

            }
            if (!empty($sourceFiles)) {
                $projectManager = new MemoQProjectManager();
                $fileManager = new MemoQFileManager();
                $tmManager = new MemoQTmManager();
                $projectInfo = array();
                $date = new \DateTime();
                $date->setTimezone(new \DateTimeZone(AI::getInstance()->getSetup()->timeZone_TMS));


                $projectInfo["Name"] = "TEST_AI_PROJECT_" . $date->format("YmdHisu");
                $sourceLang = $projectManager->getMemoqLanguageCode($task->getSourceLanguage()->getIdLanguage());
                $targetLang = $projectManager->getMemoqLanguageCode($task->getTargetsLanguage()[0]->getIdLanguage());
                $projectInfo["SourceLanguage"] = $sourceLang;
                $projectInfo["TargetLanguages"] = array(
                    $targetLang
                );
                $projectInfo["AllowOverlappingWorkflow"] = true;
                $projectInfo["EnableSplitJoin"] = true;
                $projectInfo["RecordVersionHistory"] = true;
                $projectInfo["PreventDeliveryOnQAError"] = true;
                $projectInfo["EnableCommunication"] = true;
                $tempProjectGuid = $projectManager->createProject($projectInfo);
                \Functions::console("Project: " . $projectInfo["Name"]." ". $tempProjectGuid);
                if ($tempProjectGuid !== null) {
                    $options = array();
                    $options["Name"] = "TEMP_AI_TM_" . $date->format("YmdHisu");
                    $options["SourceLanguage"] = $sourceLang;
                    $options["TargetLanguage"] = $targetLang;
                    $options["AllowReverseLookup"] = true;
                    $options["UseContext"] = true;
                    $tmGuid = $tmManager->createTM($options);
                    \Functions::console("TM: " . $options["Name"] . " ".$tmGuid);

                    if ($tmGuid !== null && $translationMemoriesPath !== null) {
                        \Functions::console("SDLTM path " . $translationMemoriesPath);
                        $tmxPath = null;
                        try {
                            $tmxPath = $this->exportSdlToTmx($translationMemoriesPath, $translationMemoriesPath."TM.tmx");
                            Functions::console("TMX path ".$tmxPath);
                        } catch (AIException $ex) {
                            Functions::logException("Cannot convert sdltm to tmx format: " . $ex->getMessage(), Functions::ERROR, __CLASS__, $ex);
                        }
                        $analysisOptions = array();
                        if ($tmxPath !== null) {
                            $tmManager->importFileToTM($tmGuid, $tmxPath);
                            $projectManager->assignTMToProject($tempProjectGuid, $tmGuid, $targetLang);

                        } else {
                            \Functions::addLog("Could not import TM, Analysis performed without TM", \Functions::WARNING, __CLASS__);
                        }

                        $uploadedFilesGuid = array();
                        foreach ($sourceFiles as $file) {
                            $guid = null;
                            if ($file !== null) {
                                $guid = $fileManager->uploadFile($file);
                            }

                            if ($guid !== null) {
                                $uploadedFilesGuid[] = $guid;
                            }

                        }
                        if (!empty($uploadedFilesGuid)) {
                            $fileGuids = $projectManager->addFilesToProject($tempProjectGuid, $uploadedFilesGuid);
                            if ($fileGuids !== null) {
                                $analysisOptions = array();
                                $analysisOptions["projectTM"] = true;
                                $analysisOptions["algorithm"] = \StatisticsAlgorithm::MemoQ;
                                $analysisOptions["homogenity"] = true;
                                $analysisOptions["repetitionsPrefOver100"] = true;
                                $wc = $projectManager->getDocumentsAnalysis($tempProjectGuid, $fileGuids, $analysisOptions);
                                $newWC = new Analysis();
                                $newWC->setPercentage_101($wc["101"]);
                                $newWC->setPercentage_100($wc["100"]);
                                $newWC->setPercentage_95($wc["95"]);
                                $newWC->setPercentage_85($wc["85"]);
                                $newWC->setPercentage_75($wc["75"]);
                                $newWC->setPercentage_50($wc["50"]);
                                $newWC->setRepetition($wc["repetitions"]);
                                $newWC->setNotMatch($wc["noMatch"]);
                                $newWC->calculateTotalWords();
                                $newWC->calculateWeightedWords();
                                $newWC->setIdAnalis($task->getTargetsLanguage()[0]->getAnalysis()->getIdAnalis());
                                $task->getTargetsLanguage()[0]->setAnalysis($newWC);
                                $taskDAO = new TaskDAO();
                                try {
                                    \Functions::console("Updating word count TASK => ".$task->getUniqueSourceId());
                                    $taskDAO->updateAnalysis($newWC);
                                } catch (\Exception $ex) {
                                    \Functions::addLog("Could not update task wordcount TASK => " . $task->getUniqueSourceId(), \Functions::WARNING,__METHOD__);
                                }


                            } else {
                                $task->getTargetsLanguage()[0]->setAnalysis(new Analysis());
                                \Functions::addLog("Could not get word count, TASK=>" . $task->getUniqueSourceId(), \Functions::WARNING, __CLASS__);
                            }
                        } else {
                            \Functions::addLog("Error importing documents TASK=>" . $task->getUniqueSourceId(), \Functions::WARNING, __CLASS__);
                        }
                    } else {
                        \Functions::addLog("Error when creating TM TASK=>" . $task->getUniqueSourceId(), \Functions::WARNING, __CLASS__);
                    }


                    $tmManager->deleteTM($tmGuid);
                    $projectManager->deleteProject($tempProjectGuid);
                } else {
                    \Functions::addLog("Error when creating temp project to analyze file TASK=>" . $task->getUniqueSourceId(), \Functions::WARNING, __CLASS__);
                }
                $states[] = new State(State::PREPROCESSED);
                try{
                    $taskDAO = new TaskDAO();
                    foreach($states as $state){
                        $taskDAO->changeStateTask($task->getUniqueSourceId(),$state->getIdState(),$state->getDescription(),$state->getTimeStamp()->format("Y-m-d H:i:s"));
                    }
                }catch(\dataAccess\SQLException $ex){
                    $AIEx = AIException::createInstanceFromSQLException($ex)->construct(__METHOD__,__NAMESPACE__,$a=func_get_args());
                    Functions::logException($ex->getMessage(), Functions::ERROR,__CLASS__,$AIEx);
                }

            }
        }
        return array($task);

    }

    /**
     * Given a folder with sdltm files, returns the Path with the generated TMX or causes an exception.
     * $exportFilePath can be set. If not, the generated tmx will be stored on OS temporary folder with uniqueId.
     *
     * @param String $sdlTmPath
     * @param String $exportFilePath
     * @throws AIException
     * @return string|boolean
     * @author phidalgo
     * @date   20190408
     */
    private function exportSdlToTmx(String $sdlTmPath, String $exportFilePath = null){
        $tmxPath = ($exportFilePath!==null? $exportFilePath:sys_get_temp_dir()."/".uniqid("TM_").".tmx");

        //MHERNANDEZ has modified the code created by:
        //https://github.com/TomasoAlbinoni/Trados-Studio-Resource-Converter
        //To be called with command line. This is written in java.
        $execCommand = "java -jar /ai/sdltm2tmx/Converter.jar m $sdlTmPath $tmxPath 2>&1";

        $resp =  exec($execCommand);

        if($resp==="0"){
            return $tmxPath;
        }

        $errMsg = null;
        switch($resp){
            case 1:
                $errMsg = "$resp - IoException";
                break;
            case 2:
                $errMsg = "$resp - IncorrectParameters";
                break;
            case 3:
                $errMsg = "$resp - UnsupportedEncodingException";
                break;
            case 4:
                $errMsg = "$resp - FileNotFoundException";
                break;
            case 5:
                $errMsg = "$resp - SQLException";
                break;
            case 6:
                $errMsg = "$resp - ClassNotFoundException";
                break;
            case 7:
                $errMsg = "$resp - SourceFilesNotFound";
                break;
            default:
                $errMsg = "Unkown Error: $resp";
                break;
        }

        throw (new AIException("Java error has ocurred:  $errMsg"))->construct(__METHOD__, __NAMESPACE__, $args = func_get_args(), array(
            "CommandToExecute" => $execCommand
        ));
    }
}