<?php
/**
 *
 * @author ZRoque
 * @version 1.0
 *          created on 30 nov. 2018
 */

namespace preprocessorModule;

use common\exceptions\AIException;
use core\AI;
use dataAccess\dao\PreprocessDAO;
use Functions;
use model\Task;
use PHPMailer\PHPMailer\Exception;
use dataAccess\dao\TaskDAO;
use Throwable;

include_once BaseDir . '/dataAccess/dao/PreprocessDAO.php';
include_once BaseDir . '/model/Task.php';
include_once BaseDir . '/dataAccess/dao/TaskDAO.php';
include_once BaseDir . '/AI.php';

class Preprocessor {
    private $preprocessDAO;

    public function __construct() {
        $this->preprocessDAO = new PreprocessDAO();
    }

    /***
     * Preprocess each task in the array
     * @param array $tasks
     * @return array $tasks
     */
    public function preprocessTasks(array $tasks) {
        Functions::console("PM::Start Preprocessor");
        $preprocessedTasks = array();
        foreach ($tasks as $task) {
            $preprocess = $this->getPreprocessToExecute($task);
            //echo $preprocess->getPreprocessName();
            if ($preprocess !== false) {
                //the task must be preprocessed
                $codeToExecute = $preprocess->getCodeToExecute();
                try {
                    $newTasks = $this->getPreprocessedTasks($codeToExecute, $task);
                } catch (AIException $e) {
                    continue;
                }
                $preprocessedTasks = array_merge($preprocessedTasks, $newTasks);
            } else {
                //The task should NOT be preprocessed
                $preprocessedTasks[] = $task;
            }


        }
        $taskDao = new TaskDAO();
        $aux = array();
        foreach ($preprocessedTasks as $newTask) {
            if ($newTask->taskCheck() && !AI::getInstance()->getBehavior()->checkIfTaskExist($newTask) && $taskDao->saveTask($newTask)) {
                $aux[] = $newTask;
            }
        }
        $preprocessedTasks = $aux;
        Functions::console("PM::End preprocessing tasks");
        return $preprocessedTasks;
    }

    private function getPreprocessToExecute(Task $task) {
        $preprocesses = $this->preprocessDAO->getPreprocessBySource($task->getDataSource()->getIdSource());
        foreach ($preprocesses as $preprocess) {
            $rules = $preprocess->getRules();
            $totalWeight = 0;
            foreach ($rules as $rule) {
                if (!$rule->check($task)) {
                    $totalWeight += $rule->getWeight();
                }

            }
            if ($totalWeight < 1) {
                return $preprocess;
            }
        }
        return false;
    }

    /**
     * Given a database className (AIDB -> Preprocess.codeToExecute), create an instance of
     * /preprocessorModule/preprocessors/* and call the executePreprocessor method.
     * Returns an array of tasks with the preprocessed tasks.
     * @param string $className
     * @param Task $task
     * @return Task[]
     * @throws AIException
     */
    private function getPreprocessedTasks(string $className, Task $task) {
        $rsp = array();
        try {
            $classPath = "/preprocessorModule/preprocessors/" . $className.".php";
            $className = "preprocessorModule\\preprocessors\\" . $className;
            if (!class_exists($className)) {
                require BaseDir.$classPath;
            }
            $object = new $className;
            $rsp = $object->executePreprocessor($task);
        } catch (Throwable $e) {
            $ex = AIException::createInstanceFromThrowable($e)->construct(__METHOD__, __NAMESPACE__, $args = func_get_args(), array(
                "task" => $task
            ));
            Functions::logException("Cannot preprocess task, error: " . $e->getMessage(), Functions::SEVERE, __CLASS__, $ex);
            throw $ex;
        }

        return $rsp;
    }
}

