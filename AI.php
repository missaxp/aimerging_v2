<?php

namespace core;



/**
 * @author Ljimenez
 * @version 1.0
 * created on 9 jul. 2018
 */

use common\exceptions\AIException;
use dataAccess\SQLException;
use harvestModule\Harvest;
use dataAccess\dao\SourceDAO;
use behaviorModule\Behavior;
use dataAccess\dao\AIDAO;
use dataAccess\dao\TaskDAO;
use model\Task;
use preprocessorModule\Preprocessor;
use Throwable;

include_once BaseDir. '/dataAccess/dao/AIDAO.php';
include_once BaseDir. '/dataAccess/dao/SourceDAO.php';
include_once BaseDir. '/harvestModule/Harvest.php';
include_once BaseDir. '/behaviorModule/Behavior.php';
include_once BaseDir. '/preprocessorModule/Preprocessor.php';

class AI{

    /**
     * Configuration of system
     *
     * @var setup
     */
    private static $setup = null;


    /**
     * Instance of AI class for all system execution
     *
     * @var AI
     */
    private static $AI = null;


    /**
     * Date and time of start execution.
     *
     * @var string
     */
    private $startExecution;

    public const ONDEMAND = 'ONDEMAND';
    public const INQUEUE = 'INQUEUE';
    public const REPOSITORY = 'REPOSITORY';
    public const SUCCESSFULL = 'OK';
    public const FAIL = 'FAIL';

    /**
     * Identifier of execution save on storage system
     *
     * @var string
     */
    private $idExecution;

    /**
     * Instance for all execution of system
     *
     * @var Harvest
     */
    private static $harvest;

    /**
     *
     * @var Preprocessor
     */
    private static $preprocessor;

    /**
     *
     *
     * @var Behavior
     */
    private static $behavior;

    /**
     *
     *
     * @var AIDAO
     */
    private $dao;

    /**
     *
     * @return AIDAO
     */
    public function getAIDAO(){
        return $this->dao;
    }

    /**
     *
     * @return Behavior
     */
    public function getBehavior(){
        return self::$behavior;
    }

    /**
     * Return a instance of AI class
     *
     * @param setup $setup configution for this execution
     * @throws \Exception if AI is null and setup if null
     * @return AI it is unique for all system execution
     */
    public static function getInstance(setup $setup = null){
        if(self::$AI == null){
            if($setup == null){
                throw new \Exception("Setup is needed for AI", 500);
            }
            self::$AI = new self($setup);
        }

        return self::$AI;
    }

    private function __construct(setup $setup){
        self::$setup = $setup;
    }

    /**
     * Instance the harvest and behavior attribute
     *
     */
    public function init(){
        AI::$harvest = new Harvest();
        AI::$preprocessor = new Preprocessor();
        AI::$behavior = new Behavior();
        $this->dao = new AIDAO();
    }


    /**
     * Gets environment value or false.
     *
     * @param string $value
     * @return setup
     */
    public function getSetup(){
        return self::$setup;
    }

    /**
     * Returns if is dev environment
     *
     * @return boolean
     */
    public function isDev(){
        return (isset(self::$setup->development) && is_bool(self::$setup->development)? self::$setup->development:true);
    }

    /**
     * Return the instance of Harvest class for this execution
     *
     * @return Harvest
     */
    public function getHarvest(){
        return self::$harvest;
    }

    /**
     * Return start date of the execution
     *
     * @return string
     */
    public function getStartExecution() {
        return $this->startExecution;
    }

    /**
     * Return identifier of exection generate by database
     *
     * @return string
     */
    public function getIdExecution() {
        return $this->idExecution;
    }

    /**
     * @param string $startExecution
     */
    public function setStartExecution($startExecution) {
        $this->startExecution = $startExecution;
    }

    /**
     * @param string $idExecution
     */
    public function setIdExecution($idExecution) {
        $this->idExecution = $idExecution;
    }

    /**
     * Execute the method for Source that are ondemand
     *
     * @param string $sourceName source name of Source to execute
     * @return boolean
     * @throws \Exception
     */
    public function ondemand(string $sourceName, $idTask = null){
        AI::getInstance()->getSetup()->execution_type = Task::ONDEMAND;
        if(self::$harvest->ondemandSource($sourceName,$idTask)){
            $tasks = self::$harvest->getTasks();
            $preprocessedTasks = AI::$preprocessor->preprocessTasks($tasks);
            $taskDAO = new TaskDAO();
            $taskToProcess = array();
            foreach ($preprocessedTasks as $task){
                $taskCompleted = $taskDAO->getTaskByUTID($task->getUniqueTaskId());
                $taskToProcess[] = $taskCompleted;
            }
            $taskToProcess = array_merge($taskToProcess,self::$harvest->getTasksToReprocess());

            self::$behavior->processTasks($taskToProcess,false);
        }
        else{
            throw new \Exception("No tasks were found on this Source => ".$sourceName);
        }
        return true;
    }

    /**
     * Executes the enquiue process
     *
     * @return
     * @throws AIException
     */
    public function inqueue(){
        AI::getInstance()->getSetup()->execution_type = Task::INQUEUE;
        $sourceDAO = new SourceDAO();
        try{
            if(AI::$AI->getSetup()->development){
                $sources = $sourceDAO->getSources();
            }else {
                $sources = $sourceDAO->getSourceToExecute();
            }
        } catch(SQLException $ex){
            $aiex = AIException::createInstanceFromSQLException($ex);
            $aiex->construct(__FUNCTION__, __NAMESPACE__, $args = func_get_args());
            throw $aiex;
        }

        try{
            AI::$harvest->startSearch($sources);
            $tasks = AI::$harvest->getTasks();
        } catch (Throwable $e){
            $ex = AIException::createInstanceFromThrowable($e);
            $ex->construct(__FUNCTION__, __NAMESPACE__, $args = func_get_args(), array(
                "message" => "HM: Error catched on Harvest startSearch"
            ));
            throw $ex;
        }
        try{
            $preprocessedTasks = AI::$preprocessor->preprocessTasks($tasks);
        } catch (Throwable $e){
            $ex = AIException::createInstanceFromThrowable($e);
            $ex->construct(__FUNCTION__, __NAMESPACE__, $args = func_get_args(), array(
                "message" => "PM: Error catched on Preprocessor preprocessTasks"
            ));
            throw $ex;
        }
        try{
            self::$behavior->processTasks($preprocessedTasks);
        } catch (Throwable $e){
            $ex = AIException::createInstanceFromThrowable($e);
            $ex->construct(__FUNCTION__, __NAMESPACE__, $args = func_get_args(), array(
                "message" => "BM: Error catched on Behavior processTasks"
            ));
            throw $ex;
        }

    }

    /**
     * Look for tasks that have exceeded the waiting time to be confirmed
     *
     */
    public function cleanUpTask(){
        $taskDAO = new TaskDAO();
        $taskDAO->cancelExpiredTasks($this->getSetup()->confirmTaskTimeout);
    }
}

class setup{
    /**
     * Determines the environment
     * @var boolean
     */
    public $development;

    /**
     * System version
     * @var string
     */
    public $version;

    /**
     * DB User
     * @var string
     */
    public $db_user;

    /**
     * DB Password
     * @var string
     */
    public $db_passwd;

    /**
     * DB Host
     * @var string
     */
    public $db_host;

    /**
     * DB Scheme (MySQL)
     * @var string
     */
    public $db_scheme;

    /**
     * DB Type DBCONN_MYSQL | DBCONN_ORACLE
     * @var string
     */
    public $db_type;

    /**
     * Email Server
     * @var string
     */
    public $mail_server;

    /**
     * Email to notificate system errors
     * @var string
     */
    public $mail_error_to;

    /**
     * From name for emails
     * @var string
     */
    public $mail_from;

    /**
     * Current timeZone
     * @var string
     */
    public $timeZone;


    /**
     * System start time
     * @var string
     */
    public $starttime;

    /**
     * If send emails when error to admin
     * @var boolean
     */
    public $send_email_when_error;


    /**
     * Active TMS system for action crate task (current: idcp)
     * @var string
     */
    public $default_TMS;

    /**
     * Active TMS TimeZone.
     * @var string
     */
    public $timeZone_TMS;

    /**
     * Time out when a task with status CONFIRM-EMAIL will be set to rejected.
     * @var integer
     */
    public $confirmTaskTimeout;

    /**
     * Number of tries a task will try to process their actions
     * @var integer
     */
    public $numberOfTries;

    /**
     * Send all notification emails to admin
     * @var boolean
     */
    public $sendNotificationEmailToAdmin;

    /**
     * from name email
     * @var string
     */
    public $mail_from_name;

    /**
     * Path for files
     *
     * @var string
     */
    public $repository;

    /**
     * Determines the tms environment
     *
     * @var boolean
     */
    public $tms_develoment;


    /**
     * Determines if the execution is from HTTP or not.
     * @var boolean
     */
    public $fromHTTP;

    /**
     * Determines the base URL for the task confirmation
     * @var string
     */
    public $webServiceDomain;

    /**
     * Private token used to create a hash to send to download API
     * @var string
     */
    public $apiPrivateToken = null;

    /**
     * URL API download Manager
     *
     * @var string
     */
    public $urlDownloadManager;

    /**
     * Size maximo of file support by tms.
     *
     * @var integer
     */
    public $maxSizeFile;

    /**
     * @var string
     */
    public $intervalForFiles;

    /**
     *
     *
     * @var string
     */
    public $invervalRepositoryClean;

    public function __construct(){

    }

}

?>