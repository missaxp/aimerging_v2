<?php

namespace preprocessorModule\memoQ;

use CATTools\memoQ\MemoQManager;

include_once BaseDir . '/CATTools/memoQ/tm/TMService.php';
include_once BaseDir . '/CATTools/memoQ/MemoQManager.php';

/**
 *
 * @author zroque
 * @version 1.0
 *          created on 06/02/2019
 */
class MemoQTmManager extends MemoQManager {

    public function __construct() {

    }

    /**
     * Deletes a TM from memoQ server given its guid
     * @param string $tmGuid
     * @return boolean
     */
    public function deleteTM(string $tmGuid) {
        $success = false;
        try {
            $client = new \TMService();
            $parameters = new \DeleteTM($tmGuid);
            $response = new \DeleteTMResponse($client->DeleteTM($parameters));
            $success = true;
        } catch (\Exception $ex) {
            \Functions::console($ex->getMessage() . " " . __METHOD__);
            $success = false;
        }
        return $success;
    }

    /**
     *
     * @param string $tmGuid
     * @param string $downloadPath
     * @return boolean
     */
    public function downloadTM(string $tmGuid, string $downloadPath) {
        $success = false;
        try {
            $client = new \TMService();
            $parameters = new \BeginChunkedTMXExport($tmGuid);
            $response = new \BeginChunkedTMXExportResponse($client->BeginChunkedTMXExport($parameters));
            print("<pre>" . print_r($response, true) . "</pre>");
            $sessionId = $response->BeginChunkedTMXExportResult->BeginChunkedTMXExportResult;

            $parameters = new \GetNextTMXChunk($sessionId);
            $response = new \GetNextTMXChunkResponse($client->GetNextTMXChunk($parameters));
            print("<pre>" . print_r($response, true) . "</pre>");
            $fileData = $response->GetNextTMXChunkResult->GetNextTMXChunkResult;
            $fileData = str_replace("utf-16", "UTF-16LE", $fileData);
            $writeResult = file_put_contents($downloadPath, $fileData);
            if ($writeResult !== false) {
                $success = true;
            }
            $parameters = new \EndChunkedTMXExport($sessionId);
            $response = new \EndChunkedTMXExportResponse($client->EndChunkedTMXExport($parameters));
            print("<pre>" . print_r($response, true) . "</pre>");
        } catch (\Exception $ex) {
            \Functions::addLog($ex->getMessage(), \Functions::ERROR, $ex->getTraceAsString());
            $this->addError($ex->getMessage());
        }
        return $success;
    }

    /**
     * Imports the content of the file to the specified tm on memoQ server
     * @param string $tmGuid
     * @param string $filePath
     * @return boolean
     */
    public function importFileToTM(string $tmGuid, string $filePath) {
        $success = false;
        try {
            $client = new \TMService();
            $parameters = new \BeginChunkedTMXImport($tmGuid);
            $response = new \BeginChunkedTMXImportResponse($client->BeginChunkedTMXImport($parameters));
            $sessionId = $response->BeginChunkedTMXImportResult->BeginChunkedTMXImportResult;
            //echo "SessionId: $sessionId<br />";
            //$tmChunkSize = 5242880; //5Mb
            $tmChunkSize = 500000; //488Kb
            $fileSize = filesize($filePath);
            $totalBytesRead = 0;
            $fp = fopen($filePath, "r");
            while (!feof($fp)) {
                $readBuffer = fread($fp, $tmChunkSize);
                if(mb_detect_encoding($readBuffer) !== "UTF-16LE"){
//                    $readBuffer = @iconv(mb_detect_encoding($readBuffer), "UTF-16LE//IGNORE", $readBuffer);
                    $readBuffer = mb_convert_encoding($readBuffer, "UTF-16LE");
                }


                $addChunk = new \AddNextTMXChunk($sessionId, $readBuffer);
                $response = new \AddNextTMXChunkResponse($client->AddNextTMXChunk($addChunk));
            }
            fclose($fp);

            $endChunkedImport = new \EndChunkedTMXImport($sessionId);
            $response = new \EndChunkedTMXImportResponse($client->EndChunkedTMXImport($endChunkedImport));
            $success = true;
        } catch (\Exception $ex) {
            \Functions::console($ex->getMessage() . " " . __METHOD__);
        }
        return $success;
    }

    /**
     * Updates the info of the specified tm, the options must be specified inside an associative array with the following data types:<br>
     * OptimizationPreference OptimizationPreference<br>
     * StoreDocumentFullPath boolean <br>
     * StoreDocumentName boolean <br>
     * Name string required <br>
     * Description string <br>
     * Client string <br>
     * Domain string <br>
     * If the options are not specified no changes to the current set up will be made
     * @param string $tmGuid
     * @param array $tmOptions
     * @return boolean
     */
    public function updateTMInfo(string $tmGuid, array $tmOptions) {
        $success = false;
        try {
            $client = new \TMService();
            $tmUpdateInfo = new \TMUpdateInfo(
                isset($tmOptions["OptimizationPreference"]) ? $tmOptions["OptimizationPreference"] : null,
                isset($tmOptions["StoreDocumentFullPath"]) ? $tmOptions["StoreDocumentFullPath"] : null,
                isset($tmOptions["StoreDocumentName"]) ? $tmOptions["StoreDocumentName"] : null,
                isset($tmOptions["Name"]) ? $tmOptions["Name"] : null,
                isset($tmOptions["Description"]) ? $tmOptions["Description"] : null,
                isset($tmOptions["Client"]) ? $tmOptions["Client"] : null,
                isset($tmOptions["Domain"]) ? $tmOptions["Domain"] : null);
            $parameters = new \UpdateProperties($tmUpdateInfo);
            $response = new \UpdatePropertiesResponse($client->UpdateProperties($parameters));
            $success = true;
        } catch (\Exception $ex) {
            \Functions::addLog($ex->getMessage(), \Functions::ERROR, $ex->getTraceAsString());
            $success = false;
            $this->addError($ex->getMessage());
        }
        return $success;
    }

    /**
     * Creates and publishes a TM on memoq server, the options for creation must be given in an associative array with the following keys and data types:<br>
     * AllowMultiple boolean<br>
     * AllowReverseLookup boolean <br>
     * NumOfEntries null <br>
     * OptimizationPreference OptimizationPreference<br>
     * StoreDocumentFullPath boolean <br>
     * StoreDocumentName boolean <br>
     * StoreFormatting boolean <br>
     * UseContext boolean <br>
     * UseIceSpiceContext boolean <br>
     * CreatorUsername string <br>
     * SourceLanguage string required <br>
     * TargetLanguage string required <br>
     * Name string required <br>
     * Description string <br>
     * Client string <br>
     * Domain string <br>
     * Project string <br>
     * Subject string <br>
     * Name, source and target are required or the tm will not be created
     * @param array $tmOptions
     * @return NULL|string
     */
    public function createTM(array $tmOptions) {
        $tmGuid = null;
        try {
            if (isset($tmOptions["Name"]) && isset($tmOptions["SourceLanguage"]) && isset($tmOptions["TargetLanguage"])) {
                $client = new \TMService();
                $tmInfo = new \TMInfo(
                    isset($tmOptions["AllowMultiple"]) ? $tmOptions["AllowMultiple"] : false,
                    isset($tmOptions["AllowReverseLookup"]) ? $tmOptions["AllowReverseLookup"] : false,
                    isset($tmOptions["NumOfEntries"]) ? $tmOptions["NumOfEntries"] : null,
                    isset($tmOptions["OptimizationPreference"]) ? $tmOptions["OptimizationPreference"] : null,
                    isset($tmOptions["StoreDocumentFullPath"]) ? $tmOptions["StoreDocumentFullPath"] : false,
                    isset($tmOptions["StoreDocumentName"]) ? $tmOptions["StoreDocumentName"] : false,
                    isset($tmOptions["StoreFormatting"]) ? $tmOptions["StoreFormatting"] : false,
                    isset($tmOptions["UseContext"]) ? $tmOptions["UseContext"] : false,
                    isset($tmOptions["UseIceSpiceContext"]) ? $tmOptions["UseIceSpiceContext"] : false,
                    $tmOptions["CreatorUsername"] = self::AISYSTEM_USER_GUID,
                    $tmOptions["SourceLanguage"],
                    $tmOptions["TargetLanguage"],
                    $tmOptions["Name"],
                    isset($tmOptions["Description"]) ? $tmOptions["Description"] : null,
                    isset($tmOptions["Client"]) ? $tmOptions["Client"] : null,
                    isset($tmOptions["Domain"]) ? $tmOptions["Domain"] : null,
                    isset($tmOptions["Project"]) ? $tmOptions["Project"] : null,
                    isset($tmOptions["Subject"]) ? $tmOptions["Subject"] : null);
                $parameters = new \CreateAndPublish($tmInfo);
                $response = new \CreateAndPublishResponse($client->CreateAndPublish($parameters));
                // 				print("<pre>" . print_r($response, true) . "</pre>");
                $tmGuid = $response->CreateAndPublishResult->CreateAndPublishResult;
            } else {
                \Functions::addLog("Name, source and target language are required, cannot create TM", \Functions::WARNING, __METHOD__);
            }
        } catch (\Exception $ex) {
            \Functions::addLog($ex->getMessage(), \Functions::ERROR, $ex->getTraceAsString());
            $this->addError($ex->getMessage());
        }
        return $tmGuid;
    }

    /**
     * Gets the guid of the tm searching by its name, optionally the source and target language can be used as a filter
     * @param string $tmName
     * @param string $sourceLang
     * @param string $targetLang
     * @return NULL|string
     */
    public function getTMGuid(string $tmName = null, string $sourceLang = null, string $targetLang = null) {
        $tmGuid = null;
        try {
            $client = new \TMService(); // object that represents the service of TM management
            $listTMS = new \ListTMs($sourceLang, $targetLang); // this object represents the parameters for the method
            $response = new \ListTMsResponse($client->ListTMs($listTMS)); // performs the request and returns the result
            // The actual result is nested in the response object
// 						print("<pre>" . print_r($response->ListTMsResult->ListTMsResult->TMInfo, true) . "</pre>");
            $tmList = $response->ListTMsResult->ListTMsResult->TMInfo;
            foreach ($tmList as $tm) {
                if ($tm->Name == $tmName) {
                    $tmGuid = $tm->Guid;
                    break;
                }
            }
        } catch (\Exception $ex) {
            \Functions::addLog($ex->getMessage(), \Functions::ERROR, $ex->getTraceAsString());
            $this->addError($ex->getMessage());
        }
        return $tmGuid;
    }

    public function listServerTM() {
        $tmGuid = null;
        try {
            $client = new \TMService(); // object that represents the service of TM management
            $listTMS = new \ListTMs(null, null); // this object represents the parameters for the method
            $response = new \ListTMsResponse($client->ListTMs($listTMS)); // performs the request and returns the result
            // The actual result is nested in the response object
            // 						print("<pre>" . print_r($response->ListTMsResult->ListTMsResult->TMInfo, true) . "</pre>");
            $tmList = $response->ListTMsResult->ListTMsResult->TMInfo;
            $result = array();
            foreach ($tmList as $tm) {
                $result[] = array("name" => $tm->Name, "value" => $tm->Guid);
            }
        } catch (\Exception $ex) {
            \Functions::addLog($ex->getMessage(), \Functions::ERROR, $ex->getTraceAsString());
            $this->addError($ex->getMessage());
        }
        return $result;
    }
}

