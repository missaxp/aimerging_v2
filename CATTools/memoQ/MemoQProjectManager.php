<?php

namespace preprocessorModule\memoQ;

use CATTools\memoQ\MemoQManager;

include_once BaseDir . '/CATTools/memoQ/ServerProject/ServerProjectService.php';
include_once BaseDir . '/CATTools/memoQ/MemoQManager.php';
/**
 *
 * @author zroque
 * @version 1.0
 *          created on 06/02/2019
 */
class MemoQProjectManager extends MemoQManager{


	public function __construct(){

	}
	/**
	 * Assigns a tm to a project, the tm must be created first in order to assign it
	 * @param string $projectGuid
	 * @param string $tmGuid
	 * @param string $targetLanguage
	 * @return boolean
	 */
	public function assignTMToProject(string $projectGuid, string $tmGuid, string $targetLanguage){
		$success = false;
		try{
			$client = new \ServerProjectService();
			$tmAssignments = new \ServerProjectTMAssignmentsForTargetLang($tmGuid, $tmGuid, $targetLanguage, array($tmGuid));
			$parameters = new \SetProjectTMs2($projectGuid, array($tmAssignments));
			$response = new \SetProjectTMs2Response($client->SetProjectTMs2($parameters));
			$success = true;
		}catch(\Exception $ex){
			\Functions::addLog($ex->getMessage(), \Functions::ERROR, $ex->getTraceAsString());
			$success = false;
			$this->addError($ex->getMessage());
		}
		return $success;
	}
	
	/**
	 * Lists the tms of a given project
	 * @param string $projectGuid
	 * @param string $targetLang
	 * @return
	 */
	public function listProjectTMs(string $projectGuid, string $targetLang){
		$client = new \ServerProjectService();
		$parameters = new \ListProjectTMs2($projectGuid, array($targetLang));
		$response = new \ListProjectTMs2Response($client->ListProjectTMs2($parameters));
		print("<pre>" . print_r($response, true) . "</pre>");
	}
	
	/**
	 * Lists all the projects on memoq server and returns an array with the information of each project
	 * @return array
	 */
	public function listServerProjects(){
		$projectList = array();
		try {
			$client = new \ServerProjectService();
			$listProjects = new \ListProjects(null);
			$response = new \ListProjectsResponse($client->ListProjects($listProjects));
			$projectList = $response->ListProjectsResult->ListProjectsResult->ServerProjectInfo;
			$result = array();
			foreach($projectList as $project){
				$result[] = array("name"=>$project->Name, "value"=>$project->ServerProjectGuid);
			}
		} catch(\Exception $ex) {
			\Functions::addLog($ex->getMessage(), \Functions::ERROR, $ex->getTraceAsString());
			$this->addError($ex->getMessage());
		}
		return $result;
	}

	/**
	 * Returns the project guid given its name, if the project's name is not found will return null
	 *
	 * @param string $projectName
	 * @return NULL|string
	 */
	public function getProjectGUID(string $projectName){

		$projectGUID = null;
		try {
			$client = new \ServerProjectService();
			$service = new \ListProjects(null);
			$response = new \ListProjectsResponse($client->ListProjects($service));
			$projectList = $response->ListProjectsResult->ListProjectsResult->ServerProjectInfo;
			foreach($projectList as $project) {
				if ($project->Name == $projectName) {
					$projectGUID = $project->ServerProjectGuid;
					break;
				}
			}
		} catch(\Exception $ex) {
			\Functions::addLog($ex->getMessage(), \Functions::ERROR, $ex->getTraceAsString());
			$this->addError($ex->getMessage());
		}
		return $projectGUID;
	}

	/**
	 * Creates a project on memoQ server with the specified options, the options must be given inside an asociative array with the following keys and posible values:<br>
	 * AllowOverlappingWorkflow boolean <br>
	 * AllowPackageCreation boolean<br>
	 * CreatorUser string containing the guid of the user that will be assigned to the project as PM<br>
	 * Deadline string with the date formatted as Y-m-d\TH:i:s\Z<br>
	 * DownloadPreview2 boolean<br>
	 * DownloadSkeleton2 boolean<br>
	 * EnableCommunication boolean<br>
	 * OmitHitsWithNoTargetTerm boolean<br>
	 * PackageResourceHandling ServerProjectResourcesInPackages object or null<br>
	 * PreventDeliveryOnQAError boolean<br>
	 * RecordVersionHistory boolean<br>
	 * StrictSubLangMatching boolean<br>
	 * CreateOfflineTMTBCopies boolean<br>
	 * DownloadPreview boolean<br>
	 * DownloadSkeleton boolean<br>
	 * EnableSplitJoin boolean<br>
	 * EnableWebTrans boolean<br>
	 * Name String cannot be empty<br>
	 * SourceLanguage string language code in the form xxx or xxx-XX<br>
	 * TargetLanguages string[] language codes in the form xxx or xxx-XX<br>
	 * 
	 * Name, source language and target languages values are required or the project will not be created.
	 * When the project is created the guid of the new project is returned, if any error occurs during the process the project
	 * will not be created and null will be returned
	 * @param array $projectInfo
	 * @return string|null
	 */
	public function createProject(array $projectInfo){

		$projectGuid = null;
		try {
			if(isset($projectInfo["Name"]) && isset($projectInfo["SourceLanguage"]) && isset($projectInfo["TargetLanguages"])){
				$client = new \ServerProjectService();
				$date = new \DateTime();
				$date->add(new \DateInterval("P3M"));
				$createInfo = new \ServerProjectDesktopDocsCreateInfo(
						isset($projectInfo["AllowOverlappingWorkflow"])?$projectInfo["AllowOverlappingWorkflow"]:false, 
						isset($projectInfo["AllowPackageCreation"])?$projectInfo["AllowPackageCreation"]:false, 
						$projectInfo["CreatorUser"] = self::AISYSTEM_USER_GUID,
						isset($projectInfo["Deadline"])?$projectInfo["Deadline"]:$date->format( 'Y-m-d\TH:i:s\Z'), 
						isset($projectInfo["DownloadPreview2"])?$projectInfo["DownloadPreview2"]:false, 
						isset($projectInfo["DownloadSkeleton2"])?$projectInfo["DownloadSkeleton2"]:false, 
						isset($projectInfo["EnableCommunication"])?$projectInfo["EnableCommunication"]:false, 
						isset($projectInfo["OmitHitsWithNoTargetTerm"])?$projectInfo["OmitHitsWithNoTargetTerm"]:false, 
						isset($projectInfo["PackageResourceHandling"])?$projectInfo["PackageResourceHandling"]:null, 
						isset($projectInfo["PreventDeliveryOnQAError"])?$projectInfo["PreventDeliveryOnQAError"]:false, 
						isset($projectInfo["RecordVersionHistory"])?$projectInfo["RecordVersionHistory"]:true, 
						isset($projectInfo["StrictSubLangMatching"])?$projectInfo["StrictSubLangMatching"]:false, 
						isset($projectInfo["CreateOfflineTMTBCopies"])?$projectInfo["CreateOfflineTMTBCopies"]:false, 
						isset($projectInfo["DownloadPreview"])?$projectInfo["DownloadPreview"]:false, 
						isset($projectInfo["DownloadSkeleton"])?$projectInfo["DownloadSkeleton"]:false, 
						isset($projectInfo["EnableSplitJoin"])?$projectInfo["EnableSplitJoin"]:false, 
						isset($projectInfo["EnableWebTrans"])?$projectInfo["EnableWebTrans"]:true, 
						$projectInfo["Name"], 
						$projectInfo["SourceLanguage"], 
						$projectInfo["TargetLanguages"]);
				$createProject = new \CreateProject2($createInfo);
				$response = new \CreateProject2Response($client->CreateProject2($createProject));
				$projectGuid = $response->CreateProject2Result->CreateProject2Result;
			}else{
				\Functions::addLog("Name, source and target language are required, cannot create project", \Functions::WARNING, __METHOD__);
			}
			
		} catch(\Exception $ex) {
			\Functions::addLog($ex->getMessage(), \Functions::ERROR, $ex->getTraceAsString());
			$this->addError($ex->getMessage());
		}
		return $projectGuid;
	}
	
	/**
	 * Creates a project from a handoff package on memoq server with the specified options, the options must be given inside an asociative array with the following keys and posible values:<br>
	 * AllowOverlappingWorkflow boolean <br>
	 * AllowPackageCreation boolean<br>
	 * CreatorUser string containing the guid of the user that will be assigned to the project as PM<br>
	 * Deadline string with the date formatted as Y-m-d\TH:i:s\Z<br>
	 * DownloadPreview2 boolean<br>
	 * DownloadSkeleton2 boolean<br>
	 * EnableCommunication boolean<br>
	 * OmitHitsWithNoTargetTerm boolean<br>
	 * PackageResourceHandling ServerProjectResourcesInPackages object or null<br>
	 * PreventDeliveryOnQAError boolean<br>
	 * RecordVersionHistory boolean<br>
	 * StrictSubLangMatching boolean<br>
	 * CreateOfflineTMTBCopies boolean<br>
	 * DownloadPreview boolean<br>
	 * DownloadSkeleton boolean<br>
	 * EnableSplitJoin boolean<br>
	 * EnableWebTrans boolean<br>
	 * Name String cannot be empty<br>
	 * SourceLanguage string language code in the form xxx or xxx-XX<br>
	 * TargetLanguages string[] language codes in the form xxx or xxx-XX<br>
	 * 
	 * Name, source language and target languages values are required or the project will not be created.
	 * When the project is created the guid of the new project is returned, if any error occurs during the process the project
	 * will not be created and null will be returned
	 * @param array $projectInfo
	 * @param string $packageGuid
	 * @return NULL|string
	 */
	public function createProjectFromPackage(array $projectInfo, string $packageGuid){
		$projectGuid = null;
		try {
			if(isset($projectInfo["Name"]) && isset($projectInfo["SourceLanguage"]) && isset($projectInfo["TargetLanguages"])){
				$client = new \ServerProjectService();
				$date = new \DateTime();
				$date->add(new \DateInterval("P3M"));
				$createInfo = new \ServerProjectDesktopDocsCreateInfo(
						isset($projectInfo["AllowOverlappingWorkflow"])?$projectInfo["AllowOverlappingWorkflow"]:null,
						isset($projectInfo["AllowPackageCreation"])?$projectInfo["AllowPackageCreation"]:null,
                        $projectInfo["CreatorUser"] = self::AISYSTEM_USER_GUID,
						isset($projectInfo["Deadline"])?$projectInfo["Deadline"]:$date->format( 'Y-m-d\TH:i:s\Z'),
						isset($projectInfo["DownloadPreview2"])?$projectInfo["DownloadPreview2"]:null,
						isset($projectInfo["DownloadSkeleton2"])?$projectInfo["DownloadSkeleton2"]:null,
						isset($projectInfo["EnableCommunication"])?$projectInfo["EnableCommunication"]:null,
						isset($projectInfo["OmitHitsWithNoTargetTerm"])?$projectInfo["OmitHitsWithNoTargetTerm"]:null,
						isset($projectInfo["PackageResourceHandling"])?$projectInfo["PackageResourceHandling"]:null,
						isset($projectInfo["PreventDeliveryOnQAError"])?$projectInfo["PreventDeliveryOnQAError"]:null,
						isset($projectInfo["RecordVersionHistory"])?$projectInfo["RecordVersionHistory"]:null,
						isset($projectInfo["StrictSubLangMatching"])?$projectInfo["StrictSubLangMatching"]:null,
						isset($projectInfo["CreateOfflineTMTBCopies"])?$projectInfo["CreateOfflineTMTBCopies"]:null,
						isset($projectInfo["DownloadPreview"])?$projectInfo["DownloadPreview"]:null,
						isset($projectInfo["DownloadSkeleton"])?$projectInfo["DownloadSkeleton"]:null,
						isset($projectInfo["EnableSplitJoin"])?$projectInfo["EnableSplitJoin"]:null,
						isset($projectInfo["EnableWebTrans"])?$projectInfo["EnableWebTrans"]:null,
						$projectInfo["Name"],
						$projectInfo["SourceLanguage"],
						$projectInfo["TargetLanguages"]);
				$createProject = new \CreateProjectFromPackage2($createInfo, $packageGuid);
				$response = new \CreateProjectFromPackage2Response($client->CreateProjectFromPackage2($createProject));
				$projectGuid = $response->CreateProjectFromPackage2Result->CreateProjectFromPackage2Result;
			}else{
				\Functions::addLog("Name, source and target language are required, cannot create project", \Functions::WARNING, __METHOD__);
			}
			
		} catch(\Exception $ex) {
			\Functions::addLog($ex->getMessage(), \Functions::ERROR, $ex->getTraceAsString());
			$this->addError($ex->getMessage());
		}
		return $projectGuid;
	}

	/**
	 * Updates the metadata of the project with the given options, which must be given inside an associative array with the following keys and data types: <br>
	 * Guid string required or the project will not be updated<br>
	 * Client string (optional)<br>
	 * Deadline string with the date formatted as Y-m-d\TH:i:s\Z (optional)<br>
	 * Description string (optional)<br>
	 * Domain string (optional)<br>
	 * If the optional parameters are not provided no changes will be made to the current setting on memoQ server
	 * @param array $updateInfo
	 * @return boolean
	 */
	public function updateProjectMetadata(array $updateInfo){
		$success = false;
		try{
			if(isset($updateInfo["Guid"])){
				$client = new \ServerProjectService();
				$spInfo = new \ServerProjectUpdateInfo(
					isset($updateInfo["Client"])?$updateInfo["Client"]:null,
					isset($updateInfo["Deadline"])?$updateInfo["Deadline"]:null,
					isset($updateInfo["Description"])?$updateInfo["Description"]:null,
					isset($updateInfo["Domain"])?$updateInfo["Domain"]:null,
					isset($updateInfo["Subject"])?$updateInfo["Subject"]:null,
					$updateInfo["Guid"]);
				$parameters = new \UpdateProject($spInfo);
				$response = new \UpdateProjectResponse($client->UpdateProject($parameters));
				$success = true;
			}else{
				\Functions::addLog("The project GUID is required, cannot update project", \Functions::ERROR, __METHOD__);
				$success = false;
			}
			
		}catch(\Exception $ex){
			\Functions::addLog($ex->getMessage(), \Functions::ERROR, $ex->getTraceAsString());
			$this->addError($ex->getMessage());
			$success = false;
		}
		return $success;
	}
	
	/**
	 * Renames a project given its guid
	 * @param string $projectGuid
	 * @param string $newProjectName
	 * @return boolean
	 */
	public function renameProject(string $projectGuid, string $newProjectName){
		$newProjectName = strip_tags($newProjectName);
		$newProjectName = $this->cleanInvalidCharacters($newProjectName);
		$success = false;
		try{
			$client = new \ServerProjectService();
			$parameters = new \RenameProject($projectGuid, $newProjectName);
			$response = new \RenameProjectResponse($client->RenameProject($parameters));
			$success = true;
		}catch(\Exception $ex){
			\Functions::addLog($ex->getMessage(), \Functions::ERROR, $ex->getTraceAsString());
			$this->addError($ex->getMessage());
			$success = false;
		}
		return $success;
	}
	
	/**
	 * Adds a new target language to a project given the project guid and the language code in the form xxx or xxx-XX
	 * @param string $projectGuid
	 * @param string $languageCode
	 * @return boolean
	 */
	public function addTargetLanguageToProject(string $projectGuid, string $languageCode){
		$success = false;
		try{
			$client = new \ServerProjectService();
			$languageInfo = new \ServerProjectAddLanguageInfo($languageCode);
			$parameters = new \AddLanguageToProject($projectGuid, $languageInfo);
			$response = new \AddLanguageToProjectResponse($client->AddLanguageToProject($parameters));
			$success = true;
		}catch(\Exception $ex){
			\Functions::addLog($ex->getMessage(), \Functions::ERROR, $ex->getTraceAsString());
			$success = false;
			$this->addError($ex->getMessage());
		}
		return $success;
	}

	/**
	 * Adds the files to the project, the files have to be uploaded before calling this method as it requires the file guids assigned by memoQ server
	 * The project guid is required or the files will not be added to the project.
	 * The target languages parameter specify to which of the existing target languages the documents should be imported to
	 * @param string $projectGuid
	 * @param array $fileGuids
	 * @param array $targetLanguages
	 * @param string $importSettingsXML
	 * @return NULL|array
	 */
	public function addFilesToProject(string $projectGuid, array $fileGuids, array $targetLanguages = null, string $importSettingsXML=null){
		$fileProjectGuids = null;
		try{
			if(count($fileGuids)>0 || strlen($projectGuid)>0){
				$client = new \ServerProjectService();
				$parameters = new \ImportTranslationDocuments($projectGuid, $fileGuids, $targetLanguages, $importSettingsXML);
				$response = new \ImportTranslationDocumentsResponse($client->ImportTranslationDocuments($parameters));
// 				print("<pre>" . print_r($response, true) . "</pre>");
				$results = $response->ImportTranslationDocumentsResult->ImportTranslationDocumentsResult->TranslationDocImportResultInfo;
				if(is_array($results)){
					foreach($results as $document){
						if(is_array($document->DocumentGuids->guid)){
							foreach($results->DocumentGuids->guid as $guid){
								$fileProjectGuids[] = $guid;
							}
						}else{
							$fileProjectGuids[] = $document->DocumentGuids->guid;
						}
						
						if($document->ResultStatus !== "Success"){
							\Functions::addLog($document->MainMessage, \Functions::ERROR, __METHOD__);
						}
					}
				}else{
					if(is_array($results->DocumentGuids->guid)){
						foreach($results->DocumentGuids->guid as $guid){
							$fileProjectGuids[] = $guid;
						}
					}else{
						$fileProjectGuids[] = $results->DocumentGuids->guid;
					}
					if($results->ResultStatus !== "Success"){
						\Functions::addLog($results->MainMessage, \Functions::ERROR, __METHOD__);
					}
				}
				
			}else{
				\Functions::addLog("The file guids are required, cannot add files to project", \Functions::ERROR, __METHOD__);
			}
		}catch(\Exception $ex){
            \Functions::console(substr($ex->getMessage(),0,107)." ".__METHOD__);
		}
		return $fileProjectGuids;

	}
	
	/**
	 * Lists all the documents in the specified project
	 * @param string $projectGuid
	 * @return array
	 */
	public function listProjectDocuments(string $projectGuid){
		$documentsList = array();
		try{
			$client = new \ServerProjectService();
			$parameters = new \ListProjectTranslationDocuments($projectGuid);
			$response = new \ListProjectTranslationDocumentsResponse($client->ListProjectTranslationDocuments($parameters));
			$documentsList = $response->ListProjectTranslationDocumentsResult->ListProjectTranslationDocumentsResult;
		}catch(\Exception $ex){
			\Functions::addLog($ex->getMessage(), \Functions::ERROR, $ex->getTraceAsString());
			$this->addError($ex->getMessage());
		}
		return $documentsList;
	}
	
	/**
	 * Gets the analysis results of the documents. The options to perform the analysis must be specified in an associative array with the following keys and data types:<br>
	 * algorithm string could be MemoQ or Trados
	 * projectTM boolean
	 * homogenity boolean
	 * detailsByTM boolean
	 * dissableCrossfileReps boolean
	 * includeLocked boolean
	 * repetitionsPrefOver100 boolean
	 * scIncludeTargetCount boolean
	 * scIncludeWithespaces bolean
	 * tagCharWeight float
	 * tagWordWeight float
	 * @param string $projectGuid
	 * @param array $documentsGuid
	 * @param array $analysisOptions
	 * @return NULL|array
	 */
	public function getDocumentsAnalysis(string $projectGuid, array $documentsGuid, array $analysisOptions=array()){
		$wordCount = null;
		$useProjectTM = false;
		$showCounts = false;
		$showResultsPerFile = false;
		$showStatusReport = true;
		if(isset($analysisOptions["projectTM"])){
			$useProjectTM = $analysisOptions["projectTM"];
		}
		try{
			$client = new \ServerProjectService();
			$statisticsOptions = new \StatisticsOptions(
					isset($analysisOptions["algorithm"])?$analysisOptions["algorithm"]:\StatisticsAlgorithm::Trados, 
					isset($analysisOptions["homogenity"])?$analysisOptions["homogenity"]:false,
					$useProjectTM,
					isset($analysisOptions["detailsByTM"])?$analysisOptions["detailsByTM"]:true,
					isset($analysisOptions["dissableCrossfileReps"])?$analysisOptions["dissableCrossfileReps"]:false,
					isset($analysisOptions["includeLocked"])?$analysisOptions["includeLocked"]:false,
					isset($analysisOptions["repetitionsPrefOver100"])?$analysisOptions["repetitionsPrefOver100"]:false,
					$showCounts,
					isset($analysisOptions["scIncludeTargetCount"])?$analysisOptions["scIncludeTargetCount"]:true,
					isset($analysisOptions["scIncludeWithespaces"])?$analysisOptions["scIncludeWithespaces"]:false,
					$showStatusReport,
					$showResultsPerFile,
					isset($analysisOptions["tagCharWeight"])?$analysisOptions["tagCharWeight"]:0,
					isset($analysisOptions["tagWordWeight"])?$analysisOptions["tagWordWeight"]:0);
			$parameters = new \GetStatisticsOnTranslationDocuments($projectGuid, $documentsGuid, $statisticsOptions, \StatisticsResultFormat::CSV_WithTable);
			$response = new \GetStatisticsOnTranslationDocumentsResponse($client->GetStatisticsOnTranslationDocuments($parameters));
			$analysisResults = $response->GetStatisticsOnTranslationDocumentsResult->GetStatisticsOnTranslationDocumentsResult;
			if($analysisResults->ResultStatus == "Success"){
				$overallStatistics = $analysisResults->ResultsForTargetLangs->StatisticsResultForLang->ResultData;
//                \Functions::print($overallStatistics);
				$overallStatistics = str_replace(array("\n","\r","\n\r","\t","\0","\x0B"), "", $overallStatistics);
				$overallStatistics = explode(";", $overallStatistics);
//                \Functions::print($overallStatistics);
                //Prueba
				if(!$useProjectTM){
					$wordCount = array();
					$wordCount["xtrans"] = (int)$overallStatistics[76];
					$wordCount["repetitions"] = (int)$overallStatistics[81];
					$wordCount["101"] = (int)$overallStatistics[86];
					$wordCount["100"] = (int)$overallStatistics[91];
					$wordCount["95"] = (int)$overallStatistics[96];
					$wordCount["85"] = (int)$overallStatistics[101];
					$wordCount["75"] = (int)$overallStatistics[106];
					$wordCount["50"] = (int)$overallStatistics[111];
					$wordCount["noMatch"] = (int)$overallStatistics[121];
				}else{
					$wordCount = array();
					$wordCount["xtrans"] = (int)$overallStatistics[14];
					$wordCount["repetitions"] = (int)$overallStatistics[19];
					$wordCount["101"] = (int)$overallStatistics[24];
					$wordCount["100"] = (int)$overallStatistics[29];
					$wordCount["95"] = (int)$overallStatistics[34];
					$wordCount["85"] = (int)$overallStatistics[39];
					$wordCount["75"] = (int)$overallStatistics[44];
					$wordCount["50"] = (int)$overallStatistics[49];
					$wordCount["noMatch"] = (int)$overallStatistics[59];
				}
				
// 				print("<pre>" . print_r($wordCount, true) . "</pre>");
			}else{
				\Functions::addLog($analysisResults->MainMessage, \Functions::ERROR, __METHOD__);
			}
// 			print("<pre>" . print_r($analysisResults->ResultsForTargetLangs->StatisticsResultForLang->ResultData, true) . "</pre>");
		}catch(\Exception $ex){
			\Functions::addLog($ex->getMessage(), \Functions::ERROR, $ex->getTraceAsString());
			$this->addError($ex->getMessage());
		}
		return $wordCount;
	}

	/**
	 * Deletes a project from memoQ server given its guid, returns true if the project was deleted successfully, false otherwise
	 * @param string $projectGuid
	 * @return boolean
	 */
	public function deleteProject(string $projectGuid){
		$success = false;
		try{
			$client = new \ServerProjectService();
			$parameters = new \DeleteProject($projectGuid);
			$response = new \DeleteProjectResponse($client->DeleteProject($parameters));
			$success = true;
		} catch(\Exception $ex) {
			\Functions::addLog($ex->getMessage(), \Functions::ERROR, $ex->getTraceAsString());
			$success = false;
			$this->addError($ex->getMessage());
		}
		return $success;
	}
	
	/**
	 * Remove invalid characters for the creation of folders or files on a windows system
	 * @param string $name
	 * @return string
	 */
	public function cleanInvalidCharacters(string $name){
		$bad = array_merge(
				array_map('chr', range(0,31)),
				array("<", ">", ":", '"', "/", "\\", "|", "?", "*"));
		return str_replace($bad, "_", substr($name,0,100));
	}
	
	/**
	 * Returns the language code for memoQ given the AI Language code
	 * @param string $aiLanguage
	 * @return NULL|string
	 */
	public function getMemoqLanguageCode(string $aiLanguage){
		$memoqLanguageCode = null;
		switch($aiLanguage){
			case "af":
			case "af-ZA":
				$memoqLanguageCode = "afr";
				break;
			case "ar":
			case "ar":
			case "ar-AE":
			case "ar-BH":
			case "ar-DZ":
			case "ar-EG":
			case "ar-IQ":
			case "ar-JO":
			case "ar-KW":
			case "ar-LB":
			case "ar-LY":
			case "ar-MA":
			case "ar-OM":
			case "ar-QA":
			case "ar-SA":
			case "ar-SY":
			case "ar-TN":
			case "ar-YE":
				$memoqLanguageCode = "ara";
				break;
			case "az":
			case "az-AZ":
				$memoqLanguageCode = "az";
				break;
			case "az-Cyrl-AZ":
				$memoqLanguageCode = "azf";
				break;
			case "eu":
			case "eu-ES":
				$memoqLanguageCode = "baq";
				break;
			case "be":
			case "be-BY":
				$memoqLanguageCode = "bel";
				break;
			case "bg":
			case "bg-BG":
				$memoqLanguageCode = "bul";
				break;
			case "ca":
			case "ca-ES":
				$memoqLanguageCode ="cat";
				break;
			case "hr":
			case "hr-BA":
			case "hr-HR":
				$memoqLanguageCode = "hrv";
				break;
			case "cs":
			case "cs-CZ":
				$memoqLanguageCode = "cze";
				break;
			case "da":
			case "da-DK":
				$memoqLanguageCode = "dan";
				break;
			case "en":
			case "en-AU":
			case "en-BZ":
			case "en-CA":
			case "en-CB":
			case "en-IE":
			case "en-JM":
			case "en-NZ":
			case "en-PH":
			case "en-ZA":
			case "en-TT":
			case "en-ZW":
			case "en-GB":
			case "en-US":
				$memoqLanguageCode = "eng";
				break;
			case "et":
			case "et-EE":
				$memoqLanguageCode ="est";
				break;
			case "es-BO":
			case "es-CL":
			case "es-CO":
			case "es-CR":
			case "es-DO":
			case "es-EC":
			case "es-SV":
			case "es-GT":
			case "es-HN":
			case "es-NI":
			case "es-PA":
			case "es-PY":
			case "es-PE":
			case "es-PR":
			case "es-UY":
			case "es-VE":
			case "es-AR":
			case "es-LatAm":
            case "es-LA":
				$memoqLanguageCode ="spa-M9";
				break;
			case "es-MX":
			case "es-Intl":
				$memoqLanguageCode = "spa-MX";
				break;
			case "es":
			case "es-ES":
				$memoqLanguageCode = "spa-ES";
				break;
			case "fi":
			case "fi-FI":
				$memoqLanguageCode ="fin";
				break;
			case "fr":
			case "fr-FR":
			case "fr-LU":
			case "fr-CH":
				$memoqLanguageCode ="fre";
				break;
			case "fr-CA":
				$memoqLanguageCode = "fre-CA";
				break;
			case "fr-MC":
				$memoqLanguageCode = "fre-MA";
				break;
			case "gl":
			case "gl-ES":
				$memoqLanguageCode = "glg";
				break;
			case "de":
			case "de-AT":
			case "de-DE":
			case "de-LI":
			case "de-LU":
			case "de-CH":
				$memoqLanguageCode = "ger";
				break;
			case "el":
			case "el-GR":
				$memoqLanguageCode = "gre";
				break;
			case "he":
			case "he-IL":
				$memoqLanguageCode = "heb";
				break;
			case "hu":
			case "hu-HU":
				$memoqLanguageCode = "hun";
			case "is-IS":
				$memoqLanguageCode = "ice";
				break;
			case "id":
			case "id-ID":
				$memoqLanguageCode = "ind";
				break;
			case "it":
			case "it-IT":
			case "it-CH":
				$memoqLanguageCode = "ita";
				break;
			case "ja":
			case "ja-JP":
				$memoqLanguageCode = "jpn";
				break;
			case "ko":
			case "ko-KR":
				$memoqLanguageCode ="kor";
				break;
			case "lv":
			case "lv-LV":
				$memoqLanguageCode = "lav";
				break;
			case "lt":
			case "lt-LT":
				$memoqLanguageCode = "lit";
				break;
			case "ms":
			case "ms-BN":
			case "ms-MY":
				$memoqLanguageCode = "msa";
				break;
			case "mt":
			case "mt-MT":
				$memoqLanguageCode = "mlt";
				break;
			case "nb":
			case "nb-NO":
			case "nn-NO":
				$memoqLanguageCode = "nor";
				break;
			case "nl":
			case "nl-BE":
			case "nl-NL":
				$memoqLanguageCode = "dut";
				break;
			case "pl":
			case "pl-PL":
				$memoqLanguageCode = "pol";
				break;
			case "pt":
				$memoqLanguageCode ="por";
				break;
			case "pt-BR":
				$memoqLanguageCode = "por-BR";
				break;
			case "pt-PT":
				$memoqLanguageCode ="por-PT";
				break;
			case "qu":
			case "qu-BO":
			case "qu-EC":
			case "qu-PE":
				$memoqLanguageCode = "quz";
				break;
			case "ro":
			case "ro-RO":
				$memoqLanguageCode ="rum";
				break;
			case "ru":
			case "ru-RU":
				$memoqLanguageCode ="rus";
				break;
			case "sr-Cyrl-BA":
			case "sr-Cyrl-SP":
			case "sr-BA":
			case "sr-SP":
				$memoqLanguageCode ="scc";
				break;
			case "sk":
			case "sk-SK":
				$memoqLanguageCode = "slo";
				break;
			case "sl":
			case "sl-SI":
				$memoqLanguageCode = "slv";
				break;
			case "sv":
			case "sv-FI":
			case "sv-SE":
				$memoqLanguageCode = "swe";
				break;
			case "tl":
			case "tl-PH":
				$memoqLanguageCode = "tgl";
				break;
			case "ta":
			case "ta-IN":
				$memoqLanguageCode ="tam";
				break;
			case "th":
			case "th-TH":
				$memoqLanguageCode = "tha";
				break;
			case "tr":
			case "tr-TR":
				$memoqLanguageCode ="tur";
				break;
			case "uk":
			case "uk-UA":
				$memoqLanguageCode = "ukr";
				break;
			case "ur":
			case "ur-PK":
				$memoqLanguageCode = "urd";
				break;
			case "vi":
			case "vi-VN":
				$memoqLanguageCode ="vie";
				break;
			case "nhd":
			case "gn":
			case "gnw":
			case "gug":
			case "gui":
			case "gun":
				$memoqLanguageCode ="grn";
				break;
			case "my":
			case "Mymr":
			case "obr":
				$memoqLanguageCode = "mya";
				break;
			case "hi":
				$memoqLanguageCode = "hin";
				break;
			case "Khmr":
				$memoqLanguageCode = "khm";
				break;
			case "lo":
				$memoqLanguageCode = "lao";
				break;
			case "la":
				$memoqLanguageCode = "lat";
				break;
			case "wo":
				$memoqLanguageCode ="wol";
				break;
			case "zh":
			case "zh-CN":
			case "zh-HK":
			case "zh-MO":
				$memoqLanguageCode = "zho-CN";
				break;
			case "zh-SG":
			case "zh-TW":
				$memoqLanguageCode ="zho-TW";
				break;
			default:
				\Functions::addLog("MemoQ does not support this language: ".$aiLanguage, \Functions::WARNING, __METHOD__);
				break;
				
		}
		return $memoqLanguageCode;
	}
}

