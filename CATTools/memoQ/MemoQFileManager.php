<?php

namespace preprocessorModule\memoQ;

use CATTools\memoQ\MemoQManager;

include_once BaseDir . '/CATTools/memoQ/filemanager/FileManagerService.php';
include_once BaseDir . '/CATTools/memoQ/MemoQManager.php';
/**
 *
 * @author zroque
 * @version 1.0
 *          created on 06/02/2019
 */
class MemoQFileManager extends MemoQManager{

	public function __construct(){

	}
	
	/**
	 * Uploads a file to memoq server and returns the guid assigned to it. Optionally the isZipped parameter indicates wheter the file is zipped, if so
	 * the file will be unzipped on the server
	 * @param string $fileName
	 * @param bool $isZipped
	 * @return string
	 */
	public function uploadFile(string $fileName, bool $isZipped=false){
		$fileGuid = null;
		try{
			$client = new \FileManagerService();
			$parameters = new \BeginChunkedFileUpload($fileName, $isZipped);
			$beginResponse = new \BeginChunkedFileUploadResponse($client->BeginChunkedFileUpload($parameters));
			$fileGuid = $beginResponse->BeginChunkedFileUploadResult->BeginChunkedFileUploadResult;
			
			$fileData = file_get_contents($fileName);
			if($fileData!==false){
				$addNext = new \AddNextFileChunk($fileGuid, $fileData);
				$addResponse = new \AddNextFileChunkResponse($client->AddNextFileChunk($addNext));
				
				$endUpload = new \EndChunkedFileUpload($fileGuid);
				$endUploadResponse = new \EndChunkedFileUploadResponse($client->EndChunkedFileUpload($endUpload));
			}else{
				\Functions::addLog("Error reading the file => ".$fileName, \Functions::ERROR, __METHOD__);
			}
			
			
		}catch(\Exception $ex){
			\Functions::addLog($ex->getMessage(), \Functions::ERROR, $ex->getTraceAsString());
			$this->addError($ex->getMessage());
		}
		return $fileGuid;
	}
}

