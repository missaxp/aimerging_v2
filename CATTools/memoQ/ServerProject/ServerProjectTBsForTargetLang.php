<?php

class ServerProjectTBsForTargetLang
{

    /**
     * @var guid[] $ExcludedTBsFromQA
     * @access public
     */
    public $ExcludedTBsFromQA = null;

    /**
     * @var guid $TBGuidTargetForNewTerms
     * @access public
     */
    public $TBGuidTargetForNewTerms = null;

    /**
     * @var guid[] $TBGuids
     * @access public
     */
    public $TBGuids = null;

    /**
     * @var string $TargetLangCode
     * @access public
     */
    public $TargetLangCode = null;

    /**
     * @param guid $TBGuidTargetForNewTerms
     * @access public
     */
    public function __construct($TBGuidTargetForNewTerms)
    {
      $this->TBGuidTargetForNewTerms = $TBGuidTargetForNewTerms;
    }

}
