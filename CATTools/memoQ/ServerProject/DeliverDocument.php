<?php

class DeliverDocument
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var DeliverDocumentRequest $deliveryOption
     * @access public
     */
    public $deliveryOption = null;

    /**
     * @param guid $serverProjectGuid
     * @param DeliverDocumentRequest $deliveryOption
     * @access public
     */
    public function __construct($serverProjectGuid, $deliveryOption)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->deliveryOption = $deliveryOption;
    }

}
