<?php

class StartPostTranslationAnalysisTaskResponse
{

    /**
     * @var TaskInfo $StartPostTranslationAnalysisTaskResult
     * @access public
     */
    public $StartPostTranslationAnalysisTaskResult = null;

    /**
     * @param TaskInfo $StartPostTranslationAnalysisTaskResult
     * @access public
     */
    public function __construct($StartPostTranslationAnalysisTaskResult)
    {
      $this->StartPostTranslationAnalysisTaskResult = $StartPostTranslationAnalysisTaskResult;
    }

}
