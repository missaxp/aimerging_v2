<?php

class ListAnalysisReportsResponse
{

    /**
     * @var AnalysisReportInfo[] $ListAnalysisReportsResult
     * @access public
     */
    public $ListAnalysisReportsResult = null;

    /**
     * @param AnalysisReportInfo[] $ListAnalysisReportsResult
     * @access public
     */
    public function __construct($ListAnalysisReportsResult)
    {
      $this->ListAnalysisReportsResult = $ListAnalysisReportsResult;
    }

}
