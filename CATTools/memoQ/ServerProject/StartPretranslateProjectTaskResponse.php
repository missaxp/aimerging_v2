<?php

class StartPretranslateProjectTaskResponse
{

    /**
     * @var TaskInfo $StartPretranslateProjectTaskResult
     * @access public
     */
    public $StartPretranslateProjectTaskResult = null;

    /**
     * @param TaskInfo $StartPretranslateProjectTaskResult
     * @access public
     */
    public function __construct($StartPretranslateProjectTaskResult)
    {
      $this->StartPretranslateProjectTaskResult = $StartPretranslateProjectTaskResult;
    }

}
