<?php

class SetProjectTBs2
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var guid[] $tbGuids
     * @access public
     */
    public $tbGuids = null;

    /**
     * @param guid $serverProjectGuid
     * @param guid[] $tbGuids
     * @access public
     */
    public function __construct($serverProjectGuid, $tbGuids)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->tbGuids = $tbGuids;
    }

}
