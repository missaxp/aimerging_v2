<?php

class AsiaOnlineGetDomainCombinationsResponse
{

    /**
     * @var AsiaOnlineGetDomainCombinationsResultInfo $AsiaOnlineGetDomainCombinationsResult
     * @access public
     */
    public $AsiaOnlineGetDomainCombinationsResult = null;

    /**
     * @param AsiaOnlineGetDomainCombinationsResultInfo $AsiaOnlineGetDomainCombinationsResult
     * @access public
     */
    public function __construct($AsiaOnlineGetDomainCombinationsResult)
    {
      $this->AsiaOnlineGetDomainCombinationsResult = $AsiaOnlineGetDomainCombinationsResult;
    }

}
