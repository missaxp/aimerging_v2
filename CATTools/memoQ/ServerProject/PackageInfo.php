<?php

class PackageInfo
{

    /**
     * @var dateTime $CreationTime
     * @access public
     */
    public $CreationTime = null;

    /**
     * @var dateTime $DownloadedTime
     * @access public
     */
    public $DownloadedTime = null;

    /**
     * @var guid $PackageID
     * @access public
     */
    public $PackageID = null;

    /**
     * @var guid $ProjectGuid
     * @access public
     */
    public $ProjectGuid = null;

    /**
     * @var int $SequenceNumber
     * @access public
     */
    public $SequenceNumber = null;

    /**
     * @var guid $UserGuid
     * @access public
     */
    public $UserGuid = null;

    /**
     * @param guid $PackageID
     * @param guid $ProjectGuid
     * @param int $SequenceNumber
     * @param guid $UserGuid
     * @access public
     */
    public function __construct($PackageID, $ProjectGuid, $SequenceNumber, $UserGuid)
    {
      $this->PackageID = $PackageID;
      $this->ProjectGuid = $ProjectGuid;
      $this->SequenceNumber = $SequenceNumber;
      $this->UserGuid = $UserGuid;
    }

}
