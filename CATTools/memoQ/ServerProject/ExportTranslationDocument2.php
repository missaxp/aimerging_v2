<?php

class ExportTranslationDocument2
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var guid $docGuid
     * @access public
     */
    public $docGuid = null;

    /**
     * @var DocumentExportOptions $options
     * @access public
     */
    public $options = null;

    /**
     * @param guid $serverProjectGuid
     * @param guid $docGuid
     * @param DocumentExportOptions $options
     * @access public
     */
    public function __construct($serverProjectGuid, $docGuid, $options)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->docGuid = $docGuid;
      $this->options = $options;
    }

}
