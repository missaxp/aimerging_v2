<?php

class AnalysisAsCSVResult
{

    /**
     * @var AnalysisAsCSVResultForLang[] $DataForTargetLangs
     * @access public
     */
    public $DataForTargetLangs = null;

    /**
     * @access public
     */
    public function __construct()
    {
    
    }

}
