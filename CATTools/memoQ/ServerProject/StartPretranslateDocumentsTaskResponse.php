<?php

class StartPretranslateDocumentsTaskResponse
{

    /**
     * @var TaskInfo $StartPretranslateDocumentsTaskResult
     * @access public
     */
    public $StartPretranslateDocumentsTaskResult = null;

    /**
     * @param TaskInfo $StartPretranslateDocumentsTaskResult
     * @access public
     */
    public function __construct($StartPretranslateDocumentsTaskResult)
    {
      $this->StartPretranslateDocumentsTaskResult = $StartPretranslateDocumentsTaskResult;
    }

}
