<?php

class CreateProjectResponse
{

    /**
     * @var guid $CreateProjectResult
     * @access public
     */
    public $CreateProjectResult = null;

    /**
     * @param guid $CreateProjectResult
     * @access public
     */
    public function __construct($CreateProjectResult)
    {
      $this->CreateProjectResult = $CreateProjectResult;
    }

}
