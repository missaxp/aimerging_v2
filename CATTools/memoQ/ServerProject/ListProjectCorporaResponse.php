<?php

class ListProjectCorporaResponse
{

    /**
     * @var ServerProjectCorporaAssignments $ListProjectCorporaResult
     * @access public
     */
    public $ListProjectCorporaResult = null;

    /**
     * @param ServerProjectCorporaAssignments $ListProjectCorporaResult
     * @access public
     */
    public function __construct($ListProjectCorporaResult)
    {
      $this->ListProjectCorporaResult = $ListProjectCorporaResult;
    }

}
