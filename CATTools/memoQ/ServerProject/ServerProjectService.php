<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/PackageDeliveryOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectResourcesInPackages.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ResultStatus.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentStatus.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectStatus.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AddProjectLanguageTBHandlingBehavior.php');
include_once(BaseDir.'/CATTools/memoQ/common/TMOptimizationPreference.php');
include_once(BaseDir.'/CATTools/memoQ/common/TBCaseSensitivity.php');
include_once(BaseDir.'/CATTools/memoQ/common/TBMatching.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/CorpusIndexingSchedule.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectRoles.php');
include_once(BaseDir.'/CATTools/memoQ/common/UserPackageWorkflowType.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/WorkflowStatus.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentAssignmentType.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/FirstAcceptUserDecision.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/FirstAcceptStatus.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SlicingMeasurementUnit.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PreviewCreation.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/BilingualDocFormat.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StatisticsAlgorithm.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StatisticsResultFormat.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PretranslateStateToConfirmAndLock.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PretranslateCopySourceToTargetConditions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/CustomPreTranslateParameter.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PretranslateExpectedFinalTranslationState.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PretranslateLookupBehavior.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AsiaOnlineTranslationStatus.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ConfirmAndUpdateSegmentStatuses.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ConfirmAndUpdateTMBehaviors.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ConfirmAndUpdateUserNameBehaviors.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/NewRevisionScenarioOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ExpectedFinalStateAfterXTranslate.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ExpectedSourceStateBeforeXTranslate.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/XTranslateScenario.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/XTranslateResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/XTranslateDocumentResult.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentHistoryItemType.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentRowsLockedHistoryItemInfoLockModes.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AutomatedActionStartedHistoryItemInfoAutomatedActionTypes.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PackageDeliveryResult.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocDeliveryResult.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/QAReportTypes.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GetApiVersion.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GetApiVersionResponse.php');
include_once(BaseDir.'/CATTools/memoQ/common/UnexpectedFault.php');
include_once(BaseDir.'/CATTools/memoQ/common/GenericFault.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectDesktopDocsCreateInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectCreateInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectConfidentialitySettings.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectNotificationSettings.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/CreateProject.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/CreateProjectResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/CreateProject2.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/CreateProject2Response.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/CreateProjectFromTemplate.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TemplateBasedProjectCreateInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/CreateProjectFromTemplateResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TemplateBasedProjectCreationResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TemplateBasedProjectCreationFault.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TemplateBasedProjectCreationInvalidMetaFault.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/UpdateProject.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectUpdateInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectCommunicationSettings.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/UpdateProjectResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GetProject.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GetProjectResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjects.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectListFilter.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetClosedStatusOfProject.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetClosedStatusOfProjectResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/WrapUpProject.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/WrapUpProjectResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/WrapUpProjectFault.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DeleteProject.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DeleteProjectResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/CannotStartOperationFault.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StartDeleteProjectTask.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StartDeleteProjectTaskResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TaskInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TaskStatus.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/RenameProject.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/RenameProjectResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/RenameProjectFault.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AddLanguageToProject.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectAddLanguageInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AddLanguageToProjectResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AddProjectLanguageFault.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectTMs2.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectTMs2Response.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectTMAssignmentDetails.php');
include_once(BaseDir.'/CATTools/memoQ/common/TMInfo.php');
include_once(BaseDir.'/CATTools/memoQ/common/HeavyResourceInfo.php');
include_once(BaseDir.'/CATTools/memoQ/common/ResourceInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetProjectTMs2.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectTMAssignmentsForTargetLang.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetProjectTMs2Response.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetProjectTBs.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetProjectTBsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetProjectTBs2.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetProjectTBs2Response.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectTBs.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectTBsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectTBAssignments.php');
include_once(BaseDir.'/CATTools/memoQ/common/TBInfo.php');
include_once(BaseDir.'/CATTools/memoQ/common/TBNewTermDefaultForLanguage.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetProjectTBs3.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectTBsForTargetLang.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetProjectTBs3Response.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectTBs3.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectTBs3Response.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetProjectCorpora.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetProjectCorporaResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectCorpora.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectCorporaResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectCorporaAssignments.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/CorpusInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/CorpusIndexingOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetProjectResourceAssignments.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectResourceAssignmentForResourceType.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ResourceType.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectResourceAssignment.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetProjectResourceAssignmentsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectResourceAssignments.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectResourceAssignmentsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectResourceAssignmentDetails.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/LightResourceInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/LightResourceInfoWithLang.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PathRuleResourceInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PathRuleType.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/FilterConfigResourceInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ProjectTemplateResourceInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectResourceAssignmentsForMultipleTypes.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectResourceAssignmentsForMultipleTypesResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetProjectUsers.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectUserInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetProjectUsersResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectUsers.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectUsersResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectUserInfoHeader.php');
include_once(BaseDir.'/CATTools/memoQ/common/UserInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectTranslationDocuments.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectTranslationDocumentsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectTranslationDocInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentUserRoleAssignmentDetails.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/UserInfoHeader.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectTranslationDocuments2.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListServerProjectTranslationDocument2Options.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectTranslationDocuments2Response.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectTranslationDocInfo2.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectTranslationDocSliceInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentDetailedAssignmentInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentDetailedRoleAssignmentInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentDetailedSingleUserAssignmentInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentAssigneeInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentFirstAcceptUserInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentGroupSourcingUserInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentDetailedFirstAcceptAssignmentInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentDetailedGroupSourcingAssignmentInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentDetailedSubvendorAssignmentInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectTranslationDocumentSlices.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListServerProjectTranslationDocumentSliceOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectTranslationDocumentSlicesResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectTranslationDocumentsGroupedBySourceFile.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListProjectTranslationDocumentsGroupedBySourceFileResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectTranslationDocumentsFromSameSourceFile.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectTranslationDocumentsDifferentLanguageTranslations.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectTranslationDocBasicInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DeleteTranslationDocument.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DeleteTranslationDocumentResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetProjectTranslationDocumentUserAssignments.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectTranslationDocumentUserAssignments.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentUserRoleAssignment.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetProjectTranslationDocumentUserAssignmentsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetTranslationDocumentAssignments.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetTranslationDocumentAssignmentsOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentAssignments.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentAssignmentInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentRoleAssignmentInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentSingleUserAssignmentInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentFirstAcceptAssignmentInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentGroupSourcingAssignmentInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentSubvendorAssignmentInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentNoUserAssignmentInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetTranslationDocumentAssignmentsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentAssignmentResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentRoleAssignmentResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentAssignmentFault.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListTranslationDocumentAssignments.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListTranslationDocumentAssignmentsOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListTranslationDocumentAssignmentsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentDetailedAssignments.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetDocumentWorkflowStatus.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectTranslationDocumentWorkflowStatusChange.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetDocumentWorkflowStatusResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetDocumentWorkflowStatusFault.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DistributeProject.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DistributeProjectResponse.php');
include_once(BaseDir.'/CATTools/memoQ/common/TMFault.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StartDistributeProjectTask.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StartDistributeProjectTaskResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ResetProject.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ResetProjectResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GetPackageDeliveryOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GetPackageDeliveryOptionsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PackageCreationNotAllowedFault.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetPackageDeliveryOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SetPackageDeliveryOptionsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DeliverDocument.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DeliverDocumentRequest.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DeliverDocumentResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DeliverDocumentFault.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SliceDocument.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SliceDocumentRequest.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SliceDocumentResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SliceDocumentFault.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ReconsolidateDocument.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ReconsolidateDocumentResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ReconsolidateDocumentFault.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ImportTranslationDocument.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ImportTranslationDocumentResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocImportResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ImportTranslationDocuments.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ImportTranslationDocumentsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ImportTranslationDocumentWithFilterConfigResource.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ImportTranslationDocumentWithFilterConfigResourceResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ImportTranslationDocumentsWithFilterConfigResource.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ImportTranslationDocumentsWithFilterConfigResourceResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ImportTranslationDocumentsWithOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ImportTranslationDocumentOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ImportTranslationDocumentsWithOptionsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ImportBilingualTranslationDocument.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ImportBilingualTranslationDocumentResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/UpdateTranslationDocumentFromBilingual.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/UpdateTranslationDocumentFromBilingualResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/UpdateTranslationDocumentFromTableRtf.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/UpdateWithTableRtfOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/UpdateTranslationDocumentFromTableRtfResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ExportTranslationDocument.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ExportTranslationDocumentResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocExportResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/FileResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ExportTranslationDocument2.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentExportOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ExportTranslationDocument2Response.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ExportTranslationDocumentAsXliffBilingual.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/XliffBilingualExportOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ExportTranslationDocumentAsXliffBilingualResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ExportTranslationDocumentAsRtfBilingual.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/RtfBilingualExportOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ExportTranslationDocumentAsRtfBilingualResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ExportTranslationDocumentAsTwoColumnRtf.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/TwoColumnRtfBilingualExportOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ExportTranslationDocumentAsTwoColumnRtfResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ExportTranslationDocumentsAsSingleTwoColumnRtf.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ExportTranslationDocumentsAsSingleTwoColumnRtfResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ReImportTranslationDocuments.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ReimportDocumentOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ReImportTranslationDocumentsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ReImportTranslationDocumentsWithFilterConfigResource.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ReImportTranslationDocumentsWithFilterConfigResourceResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AddTranslatedDocumentToLiveDocsCorpus.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AddTranslatedDocumentToLiveDocsCorpusResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GetStatisticsOnProject.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StatisticsOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GetStatisticsOnProjectResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StatisticsResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StatisticsResultForLang.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GetStatisticsOnTranslationDocuments.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GetStatisticsOnTranslationDocumentsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListAnalysisReports.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListAnalysisReportsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AnalysisReportInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GetAnalysisReportData.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GetAnalysisReportDataResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AnalysisResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AnalysisResultForLang.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AnalysisReportForDocument.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AnalysisReportItem.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AnalysisReportCounts.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GetAnalysisReportAsCSV.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GetAnalysisReportAsCSVResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AnalysisAsCSVResult.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AnalysisAsCSVResultForLang.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/RunAnalysis.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AnalysisOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/RunAnalysisResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StartStatisticsOnProjectTask.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StartStatisticsOnProjectTaskResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StartStatisticsOnTranslationDocumentsTask.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StartStatisticsOnTranslationDocumentsTaskResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListPostTranslationAnalysisReports.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListPostTranslationAnalysisReportsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PostTranslationAnalysisReportInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GetPostTranslationAnalysisReportData.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GetPostTranslationAnalysisReportDataResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PostTranslationAnalysisResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PostTranslationResultForLang.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PostTransAnalysisReportForDocument.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PostTransAnalysisReportForUser.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PostTransAnalysisReportItem.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PostTranslationReportCounts.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GetPostTranslationAnalysisReportAsCSV.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GetPostTranslationAnalysisReportAsCSVResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PostTranslationAnalysisAsCSVResult.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PostTranslationAnalysisAsCSVResultForLang.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/RunPostTranslationAnalysis.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PostTranslationAnalysisOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/RunPostTranslationAnalysisResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StartPostTranslationAnalysisTask.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StartPostTranslationAnalysisTaskResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PretranslateProject.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PretranslateOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PretranslateCopySourceToTargetBehavior.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PretranslateProjectResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PretranslateDocuments.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PretranslateDocumentsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StartPretranslateProjectTask.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StartPretranslateProjectTaskResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StartPretranslateDocumentsTask.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StartPretranslateDocumentsTaskResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AsiaOnlineGetLanguagePairCode.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AsiaOnlineGetLanguagePairCodeResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AsiaOnlineGetLanguagePairCodeResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AsiaOnlineGetDomainCombinations.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AsiaOnlineGetDomainCombinationsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AsiaOnlineGetDomainCombinationsResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AsiaOnlineDomainCombination.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AsiaOnlineBeginTranslation.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AsiaOnlineTranslateOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AsiaOnlineBeginTranslationResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AsiaOnlineBeginTranslationResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AsiaOnlineGetTranslationStatus.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AsiaOnlineGetTranslationStatusResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AsiaOnlineTranslationResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AsiaOnlineGetProjectIds.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AsiaOnlineGetProjectIdsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AsiaOnlineGetProjectIdsResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ConfirmAndUpdate.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ConfirmAndUpdateOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ConfirmAndUpdateResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ConfirmAndUpdateResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ConfirmAndUpdateDocError.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ConfirmAndUpdate2.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ConfirmAndUpdate2Response.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StartConfirmAndUpdateTask.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StartConfirmAndUpdateTaskResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/XTranslate.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/XTranslateOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/XTranslateDocInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/XTranslateResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StartXTranslateTask.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StartXTranslateTaskResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AssignmentChangeHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DeadlineChangeHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentBilingualImportHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentDeliverHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentImportHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentReturnHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentSlicingHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/FirstAcceptAssignHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/FirstAcceptAcceptHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/FirstAcceptDeclineHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/FirstAcceptFailedHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GroupSourcingAssignHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GroupSourcingDocumentDeliverHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SubvendorAssignDeadlineChangeHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/SubvendorAssignHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/WorkflowStatusChangeHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentXTranslationHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentRowsLockedHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentSnapshotCreatedHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/WorkingTMsDeletedHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ProjectLaunchedHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/AutomatedActionStartedHistoryItemInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GetDocumentHistory.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentHistoryRequest.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/GetDocumentHistoryResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListPackagesForProject.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListPackagesForProjectResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PackageInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListPackagesForProjectAndUser.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListPackagesForProjectAndUserResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PreparePackageForDownload.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PreparePackageForDownloadResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PreparePackageResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListContentOfPackages.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ListContentOfPackagesResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PackageContentInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PackageContentDocument.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DeliverPackage.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DeliverPackageResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/PackageDeliveryResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocDeliveryResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/CreateProjectFromPackage.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/CreateProjectFromPackageResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/CreateProjectFromPackage2.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/CreateProjectFromPackage2Response.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/UpdateProjectFromPackage.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/UpdateProjectFromPackageResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/CreateDeliveryPackage.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/CreateDeliveryPackageResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/CreateDeliveryResult.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/RunQAGetReport.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/RunQAGetReportOptions.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/RunQAGetReportResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/QAReport.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/QAReportForDocument.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StartQAReportTask.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/StartQAReportTaskResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/CreateImageLocalizationPack.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/CreateImageLocalizationPackResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ImportImageLocalizationPack.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ImportImageLocalizationPackResponse.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/ImportImageLocalizationPackResultInfo.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/RestoreArchive.php');
include_once(BaseDir.'/CATTools/memoQ/ServerProject/RestoreArchiveResponse.php');

class ServerProjectService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'PackageDeliveryOptions' => '\PackageDeliveryOptions',
      'ServerProjectRoles' => '\ServerProjectRoles',
      'CustomPreTranslateParameter' => '\CustomPreTranslateParameter',
      'NewRevisionScenarioOptions' => '\NewRevisionScenarioOptions',
      'XTranslateResultInfo' => '\XTranslateResultInfo',
      'XTranslateDocumentResult' => '\XTranslateDocumentResult',
      'GetApiVersion' => '\GetApiVersion',
      'GetApiVersionResponse' => '\GetApiVersionResponse',
      'UnexpectedFault' => '\UnexpectedFault',
      'GenericFault' => '\GenericFault',
      'ServerProjectDesktopDocsCreateInfo' => '\ServerProjectDesktopDocsCreateInfo',
      'ServerProjectCreateInfo' => '\ServerProjectCreateInfo',
      'ServerProjectConfidentialitySettings' => '\ServerProjectConfidentialitySettings',
      'ServerProjectNotificationSettings' => '\ServerProjectNotificationSettings',
      'CreateProject' => '\CreateProject',
      'CreateProjectResponse' => '\CreateProjectResponse',
      'CreateProject2' => '\CreateProject2',
      'CreateProject2Response' => '\CreateProject2Response',
      'CreateProjectFromTemplate' => '\CreateProjectFromTemplate',
      'TemplateBasedProjectCreateInfo' => '\TemplateBasedProjectCreateInfo',
      'CreateProjectFromTemplateResponse' => '\CreateProjectFromTemplateResponse',
      'TemplateBasedProjectCreationResultInfo' => '\TemplateBasedProjectCreationResultInfo',
      'ResultInfo' => '\ResultInfo',
      'TemplateBasedProjectCreationFault' => '\TemplateBasedProjectCreationFault',
      'TemplateBasedProjectCreationInvalidMetaFault' => '\TemplateBasedProjectCreationInvalidMetaFault',
      'UpdateProject' => '\UpdateProject',
      'ServerProjectUpdateInfo' => '\ServerProjectUpdateInfo',
      'ServerProjectCommunicationSettings' => '\ServerProjectCommunicationSettings',
      'UpdateProjectResponse' => '\UpdateProjectResponse',
      'GetProject' => '\GetProject',
      'GetProjectResponse' => '\GetProjectResponse',
      'ServerProjectInfo' => '\ServerProjectInfo',
      'ListProjects' => '\ListProjects',
      'ServerProjectListFilter' => '\ServerProjectListFilter',
      'ListProjectsResponse' => '\ListProjectsResponse',
      'SetClosedStatusOfProject' => '\SetClosedStatusOfProject',
      'SetClosedStatusOfProjectResponse' => '\SetClosedStatusOfProjectResponse',
      'WrapUpProject' => '\WrapUpProject',
      'WrapUpProjectResponse' => '\WrapUpProjectResponse',
      'WrapUpProjectFault' => '\WrapUpProjectFault',
      'DeleteProject' => '\DeleteProject',
      'DeleteProjectResponse' => '\DeleteProjectResponse',
      'CannotStartOperationFault' => '\CannotStartOperationFault',
      'StartDeleteProjectTask' => '\StartDeleteProjectTask',
      'StartDeleteProjectTaskResponse' => '\StartDeleteProjectTaskResponse',
      'TaskInfo' => '\TaskInfo',
      'RenameProject' => '\RenameProject',
      'RenameProjectResponse' => '\RenameProjectResponse',
      'RenameProjectFault' => '\RenameProjectFault',
      'AddLanguageToProject' => '\AddLanguageToProject',
      'ServerProjectAddLanguageInfo' => '\ServerProjectAddLanguageInfo',
      'AddLanguageToProjectResponse' => '\AddLanguageToProjectResponse',
      'AddProjectLanguageFault' => '\AddProjectLanguageFault',
      'ListProjectTMs2' => '\ListProjectTMs2',
      'ListProjectTMs2Response' => '\ListProjectTMs2Response',
      'ServerProjectTMAssignmentDetails' => '\ServerProjectTMAssignmentDetails',
      'TMInfo' => '\TMInfo',
    		'HeavyResourceInfo' => '\HeavyResourceInfo',
      'ResourceInfo' => '\ResourceInfo',
      'SetProjectTMs2' => '\SetProjectTMs2',
      'ServerProjectTMAssignmentsForTargetLang' => '\ServerProjectTMAssignmentsForTargetLang',
      'SetProjectTMs2Response' => '\SetProjectTMs2Response',
      'SetProjectTBs' => '\SetProjectTBs',
      'SetProjectTBsResponse' => '\SetProjectTBsResponse',
      'SetProjectTBs2' => '\SetProjectTBs2',
      'SetProjectTBs2Response' => '\SetProjectTBs2Response',
      'ListProjectTBs' => '\ListProjectTBs',
      'ListProjectTBsResponse' => '\ListProjectTBsResponse',
      'ServerProjectTBAssignments' => '\ServerProjectTBAssignments',
      'TBInfo' => '\TBInfo',
      'TBNewTermDefaultForLanguage' => '\TBNewTermDefaultForLanguage',
      'SetProjectTBs3' => '\SetProjectTBs3',
      'ServerProjectTBsForTargetLang' => '\ServerProjectTBsForTargetLang',
      'SetProjectTBs3Response' => '\SetProjectTBs3Response',
      'ListProjectTBs3' => '\ListProjectTBs3',
      'ListProjectTBs3Response' => '\ListProjectTBs3Response',
      'SetProjectCorpora' => '\SetProjectCorpora',
      'SetProjectCorporaResponse' => '\SetProjectCorporaResponse',
      'ListProjectCorpora' => '\ListProjectCorpora',
      'ListProjectCorporaResponse' => '\ListProjectCorporaResponse',
      'ServerProjectCorporaAssignments' => '\ServerProjectCorporaAssignments',
      'CorpusInfo' => '\CorpusInfo',
      'CorpusIndexingOptions' => '\CorpusIndexingOptions',
      'SetProjectResourceAssignments' => '\SetProjectResourceAssignments',
      'ServerProjectResourceAssignmentForResourceType' => '\ServerProjectResourceAssignmentForResourceType',
      'ServerProjectResourceAssignment' => '\ServerProjectResourceAssignment',
      'SetProjectResourceAssignmentsResponse' => '\SetProjectResourceAssignmentsResponse',
      'ListProjectResourceAssignments' => '\ListProjectResourceAssignments',
      'ListProjectResourceAssignmentsResponse' => '\ListProjectResourceAssignmentsResponse',
      'ServerProjectResourceAssignmentDetails' => '\ServerProjectResourceAssignmentDetails',
      'LightResourceInfo' => '\LightResourceInfo',
      'LightResourceInfoWithLang' => '\LightResourceInfoWithLang',
      'PathRuleResourceInfo' => '\PathRuleResourceInfo',
      'FilterConfigResourceInfo' => '\FilterConfigResourceInfo',
      'ProjectTemplateResourceInfo' => '\ProjectTemplateResourceInfo',
      'ListProjectResourceAssignmentsForMultipleTypes' => '\ListProjectResourceAssignmentsForMultipleTypes',
      'ListProjectResourceAssignmentsForMultipleTypesResponse' => '\ListProjectResourceAssignmentsForMultipleTypesResponse',
      'SetProjectUsers' => '\SetProjectUsers',
      'ServerProjectUserInfo' => '\ServerProjectUserInfo',
      'SetProjectUsersResponse' => '\SetProjectUsersResponse',
      'ListProjectUsers' => '\ListProjectUsers',
      'ListProjectUsersResponse' => '\ListProjectUsersResponse',
      'ServerProjectUserInfoHeader' => '\ServerProjectUserInfoHeader',
      'UserInfo' => '\UserInfo',
      'ListProjectTranslationDocuments' => '\ListProjectTranslationDocuments',
      'ListProjectTranslationDocumentsResponse' => '\ListProjectTranslationDocumentsResponse',
      'ServerProjectTranslationDocInfo' => '\ServerProjectTranslationDocInfo',
      'TranslationDocumentUserRoleAssignmentDetails' => '\TranslationDocumentUserRoleAssignmentDetails',
      'UserInfoHeader' => '\UserInfoHeader',
      'ListProjectTranslationDocuments2' => '\ListProjectTranslationDocuments2',
      'ListServerProjectTranslationDocument2Options' => '\ListServerProjectTranslationDocument2Options',
      'ListProjectTranslationDocuments2Response' => '\ListProjectTranslationDocuments2Response',
      'ServerProjectTranslationDocInfo2' => '\ServerProjectTranslationDocInfo2',
      'ServerProjectTranslationDocSliceInfo' => '\ServerProjectTranslationDocSliceInfo',
      'TranslationDocumentDetailedAssignmentInfo' => '\TranslationDocumentDetailedAssignmentInfo',
      'TranslationDocumentDetailedRoleAssignmentInfo' => '\TranslationDocumentDetailedRoleAssignmentInfo',
      'TranslationDocumentDetailedSingleUserAssignmentInfo' => '\TranslationDocumentDetailedSingleUserAssignmentInfo',
      'TranslationDocumentAssigneeInfo' => '\TranslationDocumentAssigneeInfo',
      'TranslationDocumentFirstAcceptUserInfo' => '\TranslationDocumentFirstAcceptUserInfo',
      'TranslationDocumentGroupSourcingUserInfo' => '\TranslationDocumentGroupSourcingUserInfo',
      'TranslationDocumentDetailedFirstAcceptAssignmentInfo' => '\TranslationDocumentDetailedFirstAcceptAssignmentInfo',
      'TranslationDocumentDetailedGroupSourcingAssignmentInfo' => '\TranslationDocumentDetailedGroupSourcingAssignmentInfo',
      'TranslationDocumentDetailedSubvendorAssignmentInfo' => '\TranslationDocumentDetailedSubvendorAssignmentInfo',
      'ListProjectTranslationDocumentSlices' => '\ListProjectTranslationDocumentSlices',
      'ListServerProjectTranslationDocumentSliceOptions' => '\ListServerProjectTranslationDocumentSliceOptions',
      'ListProjectTranslationDocumentSlicesResponse' => '\ListProjectTranslationDocumentSlicesResponse',
      'ListProjectTranslationDocumentsGroupedBySourceFile' => '\ListProjectTranslationDocumentsGroupedBySourceFile',
      'ListProjectTranslationDocumentsGroupedBySourceFileResponse' => '\ListProjectTranslationDocumentsGroupedBySourceFileResponse',
      'ServerProjectTranslationDocumentsFromSameSourceFile' => '\ServerProjectTranslationDocumentsFromSameSourceFile',
      'ServerProjectTranslationDocumentsDifferentLanguageTranslations' => '\ServerProjectTranslationDocumentsDifferentLanguageTranslations',
      'ServerProjectTranslationDocBasicInfo' => '\ServerProjectTranslationDocBasicInfo',
      'DeleteTranslationDocument' => '\DeleteTranslationDocument',
      'DeleteTranslationDocumentResponse' => '\DeleteTranslationDocumentResponse',
      'SetProjectTranslationDocumentUserAssignments' => '\SetProjectTranslationDocumentUserAssignments',
      'ServerProjectTranslationDocumentUserAssignments' => '\ServerProjectTranslationDocumentUserAssignments',
      'TranslationDocumentUserRoleAssignment' => '\TranslationDocumentUserRoleAssignment',
      'SetProjectTranslationDocumentUserAssignmentsResponse' => '\SetProjectTranslationDocumentUserAssignmentsResponse',
      'SetTranslationDocumentAssignments' => '\SetTranslationDocumentAssignments',
      'SetTranslationDocumentAssignmentsOptions' => '\SetTranslationDocumentAssignmentsOptions',
      'TranslationDocumentAssignments' => '\TranslationDocumentAssignments',
      'TranslationDocumentAssignmentInfo' => '\TranslationDocumentAssignmentInfo',
      'TranslationDocumentRoleAssignmentInfo' => '\TranslationDocumentRoleAssignmentInfo',
      'TranslationDocumentSingleUserAssignmentInfo' => '\TranslationDocumentSingleUserAssignmentInfo',
      'TranslationDocumentFirstAcceptAssignmentInfo' => '\TranslationDocumentFirstAcceptAssignmentInfo',
      'TranslationDocumentGroupSourcingAssignmentInfo' => '\TranslationDocumentGroupSourcingAssignmentInfo',
      'TranslationDocumentSubvendorAssignmentInfo' => '\TranslationDocumentSubvendorAssignmentInfo',
      'TranslationDocumentNoUserAssignmentInfo' => '\TranslationDocumentNoUserAssignmentInfo',
      'SetTranslationDocumentAssignmentsResponse' => '\SetTranslationDocumentAssignmentsResponse',
      'TranslationDocumentAssignmentResultInfo' => '\TranslationDocumentAssignmentResultInfo',
      'TranslationDocumentRoleAssignmentResultInfo' => '\TranslationDocumentRoleAssignmentResultInfo',
      'TranslationDocumentAssignmentFault' => '\TranslationDocumentAssignmentFault',
      'ListTranslationDocumentAssignments' => '\ListTranslationDocumentAssignments',
      'ListTranslationDocumentAssignmentsOptions' => '\ListTranslationDocumentAssignmentsOptions',
      'ListTranslationDocumentAssignmentsResponse' => '\ListTranslationDocumentAssignmentsResponse',
      'TranslationDocumentDetailedAssignments' => '\TranslationDocumentDetailedAssignments',
      'SetDocumentWorkflowStatus' => '\SetDocumentWorkflowStatus',
      'ServerProjectTranslationDocumentWorkflowStatusChange' => '\ServerProjectTranslationDocumentWorkflowStatusChange',
      'SetDocumentWorkflowStatusResponse' => '\SetDocumentWorkflowStatusResponse',
      'SetDocumentWorkflowStatusFault' => '\SetDocumentWorkflowStatusFault',
      'DistributeProject' => '\DistributeProject',
      'DistributeProjectResponse' => '\DistributeProjectResponse',
      'TMFault' => '\TMFault',
      'StartDistributeProjectTask' => '\StartDistributeProjectTask',
      'StartDistributeProjectTaskResponse' => '\StartDistributeProjectTaskResponse',
      'ResetProject' => '\ResetProject',
      'ResetProjectResponse' => '\ResetProjectResponse',
      'GetPackageDeliveryOptions' => '\GetPackageDeliveryOptions',
      'GetPackageDeliveryOptionsResponse' => '\GetPackageDeliveryOptionsResponse',
      'PackageCreationNotAllowedFault' => '\PackageCreationNotAllowedFault',
      'SetPackageDeliveryOptions' => '\SetPackageDeliveryOptions',
      'SetPackageDeliveryOptionsResponse' => '\SetPackageDeliveryOptionsResponse',
      'DeliverDocument' => '\DeliverDocument',
      'DeliverDocumentRequest' => '\DeliverDocumentRequest',
      'DeliverDocumentResponse' => '\DeliverDocumentResponse',
      'DeliverDocumentFault' => '\DeliverDocumentFault',
      'SliceDocument' => '\SliceDocument',
      'SliceDocumentRequest' => '\SliceDocumentRequest',
      'SliceDocumentResponse' => '\SliceDocumentResponse',
      'SliceDocumentFault' => '\SliceDocumentFault',
      'ReconsolidateDocument' => '\ReconsolidateDocument',
      'ReconsolidateDocumentResponse' => '\ReconsolidateDocumentResponse',
      'ReconsolidateDocumentFault' => '\ReconsolidateDocumentFault',
      'ImportTranslationDocument' => '\ImportTranslationDocument',
      'ImportTranslationDocumentResponse' => '\ImportTranslationDocumentResponse',
      'TranslationDocImportResultInfo' => '\TranslationDocImportResultInfo',
      'ImportTranslationDocuments' => '\ImportTranslationDocuments',
      'ImportTranslationDocumentsResponse' => '\ImportTranslationDocumentsResponse',
      'ImportTranslationDocumentWithFilterConfigResource' => '\ImportTranslationDocumentWithFilterConfigResource',
      'ImportTranslationDocumentWithFilterConfigResourceResponse' => '\ImportTranslationDocumentWithFilterConfigResourceResponse',
      'ImportTranslationDocumentsWithFilterConfigResource' => '\ImportTranslationDocumentsWithFilterConfigResource',
      'ImportTranslationDocumentsWithFilterConfigResourceResponse' => '\ImportTranslationDocumentsWithFilterConfigResourceResponse',
      'ImportTranslationDocumentsWithOptions' => '\ImportTranslationDocumentsWithOptions',
      'ImportTranslationDocumentOptions' => '\ImportTranslationDocumentOptions',
      'ImportTranslationDocumentsWithOptionsResponse' => '\ImportTranslationDocumentsWithOptionsResponse',
      'ImportBilingualTranslationDocument' => '\ImportBilingualTranslationDocument',
      'ImportBilingualTranslationDocumentResponse' => '\ImportBilingualTranslationDocumentResponse',
      'UpdateTranslationDocumentFromBilingual' => '\UpdateTranslationDocumentFromBilingual',
      'UpdateTranslationDocumentFromBilingualResponse' => '\UpdateTranslationDocumentFromBilingualResponse',
      'UpdateTranslationDocumentFromTableRtf' => '\UpdateTranslationDocumentFromTableRtf',
      'UpdateWithTableRtfOptions' => '\UpdateWithTableRtfOptions',
      'UpdateTranslationDocumentFromTableRtfResponse' => '\UpdateTranslationDocumentFromTableRtfResponse',
      'ExportTranslationDocument' => '\ExportTranslationDocument',
      'ExportTranslationDocumentResponse' => '\ExportTranslationDocumentResponse',
      'TranslationDocExportResultInfo' => '\TranslationDocExportResultInfo',
      'FileResultInfo' => '\FileResultInfo',
      'ExportTranslationDocument2' => '\ExportTranslationDocument2',
      'DocumentExportOptions' => '\DocumentExportOptions',
      'ExportTranslationDocument2Response' => '\ExportTranslationDocument2Response',
      'ExportTranslationDocumentAsXliffBilingual' => '\ExportTranslationDocumentAsXliffBilingual',
      'XliffBilingualExportOptions' => '\XliffBilingualExportOptions',
      'ExportTranslationDocumentAsXliffBilingualResponse' => '\ExportTranslationDocumentAsXliffBilingualResponse',
      'ExportTranslationDocumentAsRtfBilingual' => '\ExportTranslationDocumentAsRtfBilingual',
      'RtfBilingualExportOptions' => '\RtfBilingualExportOptions',
      'ExportTranslationDocumentAsRtfBilingualResponse' => '\ExportTranslationDocumentAsRtfBilingualResponse',
      'ExportTranslationDocumentAsTwoColumnRtf' => '\ExportTranslationDocumentAsTwoColumnRtf',
      'TwoColumnRtfBilingualExportOptions' => '\TwoColumnRtfBilingualExportOptions',
      'ExportTranslationDocumentAsTwoColumnRtfResponse' => '\ExportTranslationDocumentAsTwoColumnRtfResponse',
      'ExportTranslationDocumentsAsSingleTwoColumnRtf' => '\ExportTranslationDocumentsAsSingleTwoColumnRtf',
      'ExportTranslationDocumentsAsSingleTwoColumnRtfResponse' => '\ExportTranslationDocumentsAsSingleTwoColumnRtfResponse',
      'ReImportTranslationDocuments' => '\ReImportTranslationDocuments',
      'ReimportDocumentOptions' => '\ReimportDocumentOptions',
      'ReImportTranslationDocumentsResponse' => '\ReImportTranslationDocumentsResponse',
      'ReImportTranslationDocumentsWithFilterConfigResource' => '\ReImportTranslationDocumentsWithFilterConfigResource',
      'ReImportTranslationDocumentsWithFilterConfigResourceResponse' => '\ReImportTranslationDocumentsWithFilterConfigResourceResponse',
      'AddTranslatedDocumentToLiveDocsCorpus' => '\AddTranslatedDocumentToLiveDocsCorpus',
      'AddTranslatedDocumentToLiveDocsCorpusResponse' => '\AddTranslatedDocumentToLiveDocsCorpusResponse',
      'GetStatisticsOnProject' => '\GetStatisticsOnProject',
      'StatisticsOptions' => '\StatisticsOptions',
      'GetStatisticsOnProjectResponse' => '\GetStatisticsOnProjectResponse',
      'StatisticsResultInfo' => '\StatisticsResultInfo',
      'StatisticsResultForLang' => '\StatisticsResultForLang',
      'GetStatisticsOnTranslationDocuments' => '\GetStatisticsOnTranslationDocuments',
      'GetStatisticsOnTranslationDocumentsResponse' => '\GetStatisticsOnTranslationDocumentsResponse',
      'ListAnalysisReports' => '\ListAnalysisReports',
      'ListAnalysisReportsResponse' => '\ListAnalysisReportsResponse',
      'AnalysisReportInfo' => '\AnalysisReportInfo',
      'GetAnalysisReportData' => '\GetAnalysisReportData',
      'GetAnalysisReportDataResponse' => '\GetAnalysisReportDataResponse',
      'AnalysisResultInfo' => '\AnalysisResultInfo',
      'AnalysisResultForLang' => '\AnalysisResultForLang',
      'AnalysisReportForDocument' => '\AnalysisReportForDocument',
      'AnalysisReportItem' => '\AnalysisReportItem',
      'AnalysisReportCounts' => '\AnalysisReportCounts',
      'GetAnalysisReportAsCSV' => '\GetAnalysisReportAsCSV',
      'GetAnalysisReportAsCSVResponse' => '\GetAnalysisReportAsCSVResponse',
      'AnalysisAsCSVResult' => '\AnalysisAsCSVResult',
      'AnalysisAsCSVResultForLang' => '\AnalysisAsCSVResultForLang',
      'RunAnalysis' => '\RunAnalysis',
      'AnalysisOptions' => '\AnalysisOptions',
      'RunAnalysisResponse' => '\RunAnalysisResponse',
      'StartStatisticsOnProjectTask' => '\StartStatisticsOnProjectTask',
      'StartStatisticsOnProjectTaskResponse' => '\StartStatisticsOnProjectTaskResponse',
      'StartStatisticsOnTranslationDocumentsTask' => '\StartStatisticsOnTranslationDocumentsTask',
      'StartStatisticsOnTranslationDocumentsTaskResponse' => '\StartStatisticsOnTranslationDocumentsTaskResponse',
      'ListPostTranslationAnalysisReports' => '\ListPostTranslationAnalysisReports',
      'ListPostTranslationAnalysisReportsResponse' => '\ListPostTranslationAnalysisReportsResponse',
      'PostTranslationAnalysisReportInfo' => '\PostTranslationAnalysisReportInfo',
      'GetPostTranslationAnalysisReportData' => '\GetPostTranslationAnalysisReportData',
      'GetPostTranslationAnalysisReportDataResponse' => '\GetPostTranslationAnalysisReportDataResponse',
      'PostTranslationAnalysisResultInfo' => '\PostTranslationAnalysisResultInfo',
      'PostTranslationResultForLang' => '\PostTranslationResultForLang',
      'PostTransAnalysisReportForDocument' => '\PostTransAnalysisReportForDocument',
      'PostTransAnalysisReportForUser' => '\PostTransAnalysisReportForUser',
      'PostTransAnalysisReportItem' => '\PostTransAnalysisReportItem',
      'PostTranslationReportCounts' => '\PostTranslationReportCounts',
      'GetPostTranslationAnalysisReportAsCSV' => '\GetPostTranslationAnalysisReportAsCSV',
      'GetPostTranslationAnalysisReportAsCSVResponse' => '\GetPostTranslationAnalysisReportAsCSVResponse',
      'PostTranslationAnalysisAsCSVResult' => '\PostTranslationAnalysisAsCSVResult',
      'PostTranslationAnalysisAsCSVResultForLang' => '\PostTranslationAnalysisAsCSVResultForLang',
      'RunPostTranslationAnalysis' => '\RunPostTranslationAnalysis',
      'PostTranslationAnalysisOptions' => '\PostTranslationAnalysisOptions',
      'RunPostTranslationAnalysisResponse' => '\RunPostTranslationAnalysisResponse',
      'StartPostTranslationAnalysisTask' => '\StartPostTranslationAnalysisTask',
      'StartPostTranslationAnalysisTaskResponse' => '\StartPostTranslationAnalysisTaskResponse',
      'PretranslateProject' => '\PretranslateProject',
      'PretranslateOptions' => '\PretranslateOptions',
      'PretranslateCopySourceToTargetBehavior' => '\PretranslateCopySourceToTargetBehavior',
      'PretranslateProjectResponse' => '\PretranslateProjectResponse',
      'PretranslateDocuments' => '\PretranslateDocuments',
      'PretranslateDocumentsResponse' => '\PretranslateDocumentsResponse',
      'StartPretranslateProjectTask' => '\StartPretranslateProjectTask',
      'StartPretranslateProjectTaskResponse' => '\StartPretranslateProjectTaskResponse',
      'StartPretranslateDocumentsTask' => '\StartPretranslateDocumentsTask',
      'StartPretranslateDocumentsTaskResponse' => '\StartPretranslateDocumentsTaskResponse',
      'AsiaOnlineGetLanguagePairCode' => '\AsiaOnlineGetLanguagePairCode',
      'AsiaOnlineGetLanguagePairCodeResponse' => '\AsiaOnlineGetLanguagePairCodeResponse',
      'AsiaOnlineGetLanguagePairCodeResultInfo' => '\AsiaOnlineGetLanguagePairCodeResultInfo',
      'AsiaOnlineGetDomainCombinations' => '\AsiaOnlineGetDomainCombinations',
      'AsiaOnlineGetDomainCombinationsResponse' => '\AsiaOnlineGetDomainCombinationsResponse',
      'AsiaOnlineGetDomainCombinationsResultInfo' => '\AsiaOnlineGetDomainCombinationsResultInfo',
      'AsiaOnlineDomainCombination' => '\AsiaOnlineDomainCombination',
      'AsiaOnlineBeginTranslation' => '\AsiaOnlineBeginTranslation',
      'AsiaOnlineTranslateOptions' => '\AsiaOnlineTranslateOptions',
      'AsiaOnlineBeginTranslationResponse' => '\AsiaOnlineBeginTranslationResponse',
      'AsiaOnlineBeginTranslationResultInfo' => '\AsiaOnlineBeginTranslationResultInfo',
      'AsiaOnlineGetTranslationStatus' => '\AsiaOnlineGetTranslationStatus',
      'AsiaOnlineGetTranslationStatusResponse' => '\AsiaOnlineGetTranslationStatusResponse',
      'AsiaOnlineTranslationResultInfo' => '\AsiaOnlineTranslationResultInfo',
      'AsiaOnlineGetProjectIds' => '\AsiaOnlineGetProjectIds',
      'AsiaOnlineGetProjectIdsResponse' => '\AsiaOnlineGetProjectIdsResponse',
      'AsiaOnlineGetProjectIdsResultInfo' => '\AsiaOnlineGetProjectIdsResultInfo',
      'ConfirmAndUpdate' => '\ConfirmAndUpdate',
      'ConfirmAndUpdateOptions' => '\ConfirmAndUpdateOptions',
      'ConfirmAndUpdateResponse' => '\ConfirmAndUpdateResponse',
      'ConfirmAndUpdateResultInfo' => '\ConfirmAndUpdateResultInfo',
      'ConfirmAndUpdateDocError' => '\ConfirmAndUpdateDocError',
      'ConfirmAndUpdate2' => '\ConfirmAndUpdate2',
      'ConfirmAndUpdate2Response' => '\ConfirmAndUpdate2Response',
      'StartConfirmAndUpdateTask' => '\StartConfirmAndUpdateTask',
      'StartConfirmAndUpdateTaskResponse' => '\StartConfirmAndUpdateTaskResponse',
      'XTranslate' => '\XTranslate',
      'XTranslateOptions' => '\XTranslateOptions',
      'XTranslateDocInfo' => '\XTranslateDocInfo',
      'XTranslateResponse' => '\XTranslateResponse',
      'StartXTranslateTask' => '\StartXTranslateTask',
      'StartXTranslateTaskResponse' => '\StartXTranslateTaskResponse',
      'AssignmentChangeHistoryItemInfo' => '\AssignmentChangeHistoryItemInfo',
      'DocumentHistoryItemInfo' => '\DocumentHistoryItemInfo',
      'DeadlineChangeHistoryItemInfo' => '\DeadlineChangeHistoryItemInfo',
      'DocumentBilingualImportHistoryItemInfo' => '\DocumentBilingualImportHistoryItemInfo',
      'DocumentDeliverHistoryItemInfo' => '\DocumentDeliverHistoryItemInfo',
      'DocumentImportHistoryItemInfo' => '\DocumentImportHistoryItemInfo',
      'DocumentReturnHistoryItemInfo' => '\DocumentReturnHistoryItemInfo',
      'DocumentSlicingHistoryItemInfo' => '\DocumentSlicingHistoryItemInfo',
      'FirstAcceptAssignHistoryItemInfo' => '\FirstAcceptAssignHistoryItemInfo',
      'FirstAcceptAcceptHistoryItemInfo' => '\FirstAcceptAcceptHistoryItemInfo',
      'FirstAcceptDeclineHistoryItemInfo' => '\FirstAcceptDeclineHistoryItemInfo',
      'FirstAcceptFailedHistoryItemInfo' => '\FirstAcceptFailedHistoryItemInfo',
      'GroupSourcingAssignHistoryItemInfo' => '\GroupSourcingAssignHistoryItemInfo',
      'GroupSourcingDocumentDeliverHistoryItemInfo' => '\GroupSourcingDocumentDeliverHistoryItemInfo',
      'SubvendorAssignDeadlineChangeHistoryItemInfo' => '\SubvendorAssignDeadlineChangeHistoryItemInfo',
      'SubvendorAssignHistoryItemInfo' => '\SubvendorAssignHistoryItemInfo',
      'WorkflowStatusChangeHistoryItemInfo' => '\WorkflowStatusChangeHistoryItemInfo',
      'DocumentXTranslationHistoryItemInfo' => '\DocumentXTranslationHistoryItemInfo',
      'DocumentRowsLockedHistoryItemInfo' => '\DocumentRowsLockedHistoryItemInfo',
      'DocumentSnapshotCreatedHistoryItemInfo' => '\DocumentSnapshotCreatedHistoryItemInfo',
      'WorkingTMsDeletedHistoryItemInfo' => '\WorkingTMsDeletedHistoryItemInfo',
      'ProjectLaunchedHistoryItemInfo' => '\ProjectLaunchedHistoryItemInfo',
      'AutomatedActionStartedHistoryItemInfo' => '\AutomatedActionStartedHistoryItemInfo',
      'GetDocumentHistory' => '\GetDocumentHistory',
      'DocumentHistoryRequest' => '\DocumentHistoryRequest',
      'GetDocumentHistoryResponse' => '\GetDocumentHistoryResponse',
      'ListPackagesForProject' => '\ListPackagesForProject',
      'ListPackagesForProjectResponse' => '\ListPackagesForProjectResponse',
      'PackageInfo' => '\PackageInfo',
      'ListPackagesForProjectAndUser' => '\ListPackagesForProjectAndUser',
      'ListPackagesForProjectAndUserResponse' => '\ListPackagesForProjectAndUserResponse',
      'PreparePackageForDownload' => '\PreparePackageForDownload',
      'PreparePackageForDownloadResponse' => '\PreparePackageForDownloadResponse',
      'PreparePackageResultInfo' => '\PreparePackageResultInfo',
      'ListContentOfPackages' => '\ListContentOfPackages',
      'ListContentOfPackagesResponse' => '\ListContentOfPackagesResponse',
      'PackageContentInfo' => '\PackageContentInfo',
      'PackageContentDocument' => '\PackageContentDocument',
      'DeliverPackage' => '\DeliverPackage',
      'DeliverPackageResponse' => '\DeliverPackageResponse',
      'PackageDeliveryResultInfo' => '\PackageDeliveryResultInfo',
      'DocDeliveryResultInfo' => '\DocDeliveryResultInfo',
      'CreateProjectFromPackage' => '\CreateProjectFromPackage',
      'CreateProjectFromPackageResponse' => '\CreateProjectFromPackageResponse',
      'CreateProjectFromPackage2' => '\CreateProjectFromPackage2',
      'CreateProjectFromPackage2Response' => '\CreateProjectFromPackage2Response',
      'UpdateProjectFromPackage' => '\UpdateProjectFromPackage',
      'UpdateProjectFromPackageResponse' => '\UpdateProjectFromPackageResponse',
      'CreateDeliveryPackage' => '\CreateDeliveryPackage',
      'CreateDeliveryPackageResponse' => '\CreateDeliveryPackageResponse',
      'CreateDeliveryResult' => '\CreateDeliveryResult',
      'RunQAGetReport' => '\RunQAGetReport',
      'RunQAGetReportOptions' => '\RunQAGetReportOptions',
      'RunQAGetReportResponse' => '\RunQAGetReportResponse',
      'QAReport' => '\QAReport',
      'QAReportForDocument' => '\QAReportForDocument',
      'StartQAReportTask' => '\StartQAReportTask',
      'StartQAReportTaskResponse' => '\StartQAReportTaskResponse',
      'CreateImageLocalizationPack' => '\CreateImageLocalizationPack',
      'CreateImageLocalizationPackResponse' => '\CreateImageLocalizationPackResponse',
      'ImportImageLocalizationPack' => '\ImportImageLocalizationPack',
      'ImportImageLocalizationPackResponse' => '\ImportImageLocalizationPackResponse',
      'ImportImageLocalizationPackResultInfo' => '\ImportImageLocalizationPackResultInfo',
      'RestoreArchive' => '\RestoreArchive',
      'RestoreArchiveResponse' => '\RestoreArchiveResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'http://memoq2.idisc.es:8080/memoqservices/serverproject?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param GetApiVersion $parameters
     * @access public
     * @return GetApiVersionResponse
     */
    public function GetApiVersion(GetApiVersion $parameters)
    {
      return $this->__soapCall('GetApiVersion', array($parameters));
    }

    /**
     * @param CreateProject $parameters
     * @access public
     * @return CreateProjectResponse
     */
    public function CreateProject(CreateProject $parameters)
    {
      return $this->__soapCall('CreateProject', array($parameters));
    }

    /**
     * @param CreateProject2 $parameters
     * @access public
     * @return CreateProject2Response
     */
    public function CreateProject2(CreateProject2 $parameters)
    {
      return $this->__soapCall('CreateProject2', array($parameters));
    }

    /**
     * @param CreateProjectFromTemplate $parameters
     * @access public
     * @return CreateProjectFromTemplateResponse
     */
    public function CreateProjectFromTemplate(CreateProjectFromTemplate $parameters)
    {
      return $this->__soapCall('CreateProjectFromTemplate', array($parameters));
    }

    /**
     * @param UpdateProject $parameters
     * @access public
     * @return UpdateProjectResponse
     */
    public function UpdateProject(UpdateProject $parameters)
    {
      return $this->__soapCall('UpdateProject', array($parameters));
    }

    /**
     * @param GetProject $parameters
     * @access public
     * @return GetProjectResponse
     */
    public function GetProject(GetProject $parameters)
    {
      return $this->__soapCall('GetProject', array($parameters));
    }

    /**
     * @param ListProjects $parameters
     * @access public
     * @return ListProjectsResponse
     */
    public function ListProjects(ListProjects $parameters)
    {
      return $this->__soapCall('ListProjects', array($parameters));
    }

    /**
     * @param SetClosedStatusOfProject $parameters
     * @access public
     * @return SetClosedStatusOfProjectResponse
     */
    public function SetClosedStatusOfProject(SetClosedStatusOfProject $parameters)
    {
      return $this->__soapCall('SetClosedStatusOfProject', array($parameters));
    }

    /**
     * @param WrapUpProject $parameters
     * @access public
     * @return WrapUpProjectResponse
     */
    public function WrapUpProject(WrapUpProject $parameters)
    {
      return $this->__soapCall('WrapUpProject', array($parameters));
    }

    /**
     * @param DeleteProject $parameters
     * @access public
     * @return DeleteProjectResponse
     */
    public function DeleteProject(DeleteProject $parameters)
    {
      return $this->__soapCall('DeleteProject', array($parameters));
    }

    /**
     * @param StartDeleteProjectTask $parameters
     * @access public
     * @return StartDeleteProjectTaskResponse
     */
    public function StartDeleteProjectTask(StartDeleteProjectTask $parameters)
    {
      return $this->__soapCall('StartDeleteProjectTask', array($parameters));
    }

    /**
     * @param RenameProject $parameters
     * @access public
     * @return RenameProjectResponse
     */
    public function RenameProject(RenameProject $parameters)
    {
      return $this->__soapCall('RenameProject', array($parameters));
    }

    /**
     * @param AddLanguageToProject $parameters
     * @access public
     * @return AddLanguageToProjectResponse
     */
    public function AddLanguageToProject(AddLanguageToProject $parameters)
    {
      return $this->__soapCall('AddLanguageToProject', array($parameters));
    }

    /**
     * @param ListProjectTMs2 $parameters
     * @access public
     * @return ListProjectTMs2Response
     */
    public function ListProjectTMs2(ListProjectTMs2 $parameters)
    {
      return $this->__soapCall('ListProjectTMs2', array($parameters));
    }

    /**
     * @param SetProjectTMs2 $parameters
     * @access public
     * @return SetProjectTMs2Response
     */
    public function SetProjectTMs2(SetProjectTMs2 $parameters)
    {
      return $this->__soapCall('SetProjectTMs2', array($parameters));
    }

    /**
     * @param SetProjectTBs $parameters
     * @access public
     * @return SetProjectTBsResponse
     */
    public function SetProjectTBs(SetProjectTBs $parameters)
    {
      return $this->__soapCall('SetProjectTBs', array($parameters));
    }

    /**
     * @param SetProjectTBs2 $parameters
     * @access public
     * @return SetProjectTBs2Response
     */
    public function SetProjectTBs2(SetProjectTBs2 $parameters)
    {
      return $this->__soapCall('SetProjectTBs2', array($parameters));
    }

    /**
     * @param ListProjectTBs $parameters
     * @access public
     * @return ListProjectTBsResponse
     */
    public function ListProjectTBs(ListProjectTBs $parameters)
    {
      return $this->__soapCall('ListProjectTBs', array($parameters));
    }

    /**
     * @param SetProjectTBs3 $parameters
     * @access public
     * @return SetProjectTBs3Response
     */
    public function SetProjectTBs3(SetProjectTBs3 $parameters)
    {
      return $this->__soapCall('SetProjectTBs3', array($parameters));
    }

    /**
     * @param ListProjectTBs3 $parameters
     * @access public
     * @return ListProjectTBs3Response
     */
    public function ListProjectTBs3(ListProjectTBs3 $parameters)
    {
      return $this->__soapCall('ListProjectTBs3', array($parameters));
    }

    /**
     * @param SetProjectCorpora $parameters
     * @access public
     * @return SetProjectCorporaResponse
     */
    public function SetProjectCorpora(SetProjectCorpora $parameters)
    {
      return $this->__soapCall('SetProjectCorpora', array($parameters));
    }

    /**
     * @param ListProjectCorpora $parameters
     * @access public
     * @return ListProjectCorporaResponse
     */
    public function ListProjectCorpora(ListProjectCorpora $parameters)
    {
      return $this->__soapCall('ListProjectCorpora', array($parameters));
    }

    /**
     * @param SetProjectResourceAssignments $parameters
     * @access public
     * @return SetProjectResourceAssignmentsResponse
     */
    public function SetProjectResourceAssignments(SetProjectResourceAssignments $parameters)
    {
      return $this->__soapCall('SetProjectResourceAssignments', array($parameters));
    }

    /**
     * @param ListProjectResourceAssignments $parameters
     * @access public
     * @return ListProjectResourceAssignmentsResponse
     */
    public function ListProjectResourceAssignments(ListProjectResourceAssignments $parameters)
    {
      return $this->__soapCall('ListProjectResourceAssignments', array($parameters));
    }

    /**
     * @param ListProjectResourceAssignmentsForMultipleTypes $parameters
     * @access public
     * @return ListProjectResourceAssignmentsForMultipleTypesResponse
     */
    public function ListProjectResourceAssignmentsForMultipleTypes(ListProjectResourceAssignmentsForMultipleTypes $parameters)
    {
      return $this->__soapCall('ListProjectResourceAssignmentsForMultipleTypes', array($parameters));
    }

    /**
     * @param SetProjectUsers $parameters
     * @access public
     * @return SetProjectUsersResponse
     */
    public function SetProjectUsers(SetProjectUsers $parameters)
    {
      return $this->__soapCall('SetProjectUsers', array($parameters));
    }

    /**
     * @param ListProjectUsers $parameters
     * @access public
     * @return ListProjectUsersResponse
     */
    public function ListProjectUsers(ListProjectUsers $parameters)
    {
      return $this->__soapCall('ListProjectUsers', array($parameters));
    }

    /**
     * @param ListProjectTranslationDocuments $parameters
     * @access public
     * @return ListProjectTranslationDocumentsResponse
     */
    public function ListProjectTranslationDocuments(ListProjectTranslationDocuments $parameters)
    {
      return $this->__soapCall('ListProjectTranslationDocuments', array($parameters));
    }

    /**
     * @param ListProjectTranslationDocuments2 $parameters
     * @access public
     * @return ListProjectTranslationDocuments2Response
     */
    public function ListProjectTranslationDocuments2(ListProjectTranslationDocuments2 $parameters)
    {
      return $this->__soapCall('ListProjectTranslationDocuments2', array($parameters));
    }

    /**
     * @param ListProjectTranslationDocumentSlices $parameters
     * @access public
     * @return ListProjectTranslationDocumentSlicesResponse
     */
    public function ListProjectTranslationDocumentSlices(ListProjectTranslationDocumentSlices $parameters)
    {
      return $this->__soapCall('ListProjectTranslationDocumentSlices', array($parameters));
    }

    /**
     * @param ListProjectTranslationDocumentsGroupedBySourceFile $parameters
     * @access public
     * @return ListProjectTranslationDocumentsGroupedBySourceFileResponse
     */
    public function ListProjectTranslationDocumentsGroupedBySourceFile(ListProjectTranslationDocumentsGroupedBySourceFile $parameters)
    {
      return $this->__soapCall('ListProjectTranslationDocumentsGroupedBySourceFile', array($parameters));
    }

    /**
     * @param DeleteTranslationDocument $parameters
     * @access public
     * @return DeleteTranslationDocumentResponse
     */
    public function DeleteTranslationDocument(DeleteTranslationDocument $parameters)
    {
      return $this->__soapCall('DeleteTranslationDocument', array($parameters));
    }

    /**
     * @param SetProjectTranslationDocumentUserAssignments $parameters
     * @access public
     * @return SetProjectTranslationDocumentUserAssignmentsResponse
     */
    public function SetProjectTranslationDocumentUserAssignments(SetProjectTranslationDocumentUserAssignments $parameters)
    {
      return $this->__soapCall('SetProjectTranslationDocumentUserAssignments', array($parameters));
    }

    /**
     * @param SetTranslationDocumentAssignments $parameters
     * @access public
     * @return SetTranslationDocumentAssignmentsResponse
     */
    public function SetTranslationDocumentAssignments(SetTranslationDocumentAssignments $parameters)
    {
      return $this->__soapCall('SetTranslationDocumentAssignments', array($parameters));
    }

    /**
     * @param ListTranslationDocumentAssignments $parameters
     * @access public
     * @return ListTranslationDocumentAssignmentsResponse
     */
    public function ListTranslationDocumentAssignments(ListTranslationDocumentAssignments $parameters)
    {
      return $this->__soapCall('ListTranslationDocumentAssignments', array($parameters));
    }

    /**
     * @param SetDocumentWorkflowStatus $parameters
     * @access public
     * @return SetDocumentWorkflowStatusResponse
     */
    public function SetDocumentWorkflowStatus(SetDocumentWorkflowStatus $parameters)
    {
      return $this->__soapCall('SetDocumentWorkflowStatus', array($parameters));
    }

    /**
     * @param DistributeProject $parameters
     * @access public
     * @return DistributeProjectResponse
     */
    public function DistributeProject(DistributeProject $parameters)
    {
      return $this->__soapCall('DistributeProject', array($parameters));
    }

    /**
     * @param StartDistributeProjectTask $parameters
     * @access public
     * @return StartDistributeProjectTaskResponse
     */
    public function StartDistributeProjectTask(StartDistributeProjectTask $parameters)
    {
      return $this->__soapCall('StartDistributeProjectTask', array($parameters));
    }

    /**
     * @param ResetProject $parameters
     * @access public
     * @return ResetProjectResponse
     */
    public function ResetProject(ResetProject $parameters)
    {
      return $this->__soapCall('ResetProject', array($parameters));
    }

    /**
     * @param GetPackageDeliveryOptions $parameters
     * @access public
     * @return GetPackageDeliveryOptionsResponse
     */
    public function GetPackageDeliveryOptions(GetPackageDeliveryOptions $parameters)
    {
      return $this->__soapCall('GetPackageDeliveryOptions', array($parameters));
    }

    /**
     * @param SetPackageDeliveryOptions $parameters
     * @access public
     * @return SetPackageDeliveryOptionsResponse
     */
    public function SetPackageDeliveryOptions(SetPackageDeliveryOptions $parameters)
    {
      return $this->__soapCall('SetPackageDeliveryOptions', array($parameters));
    }

    /**
     * @param DeliverDocument $parameters
     * @access public
     * @return DeliverDocumentResponse
     */
    public function DeliverDocument(DeliverDocument $parameters)
    {
      return $this->__soapCall('DeliverDocument', array($parameters));
    }

    /**
     * @param SliceDocument $parameters
     * @access public
     * @return SliceDocumentResponse
     */
    public function SliceDocument(SliceDocument $parameters)
    {
      return $this->__soapCall('SliceDocument', array($parameters));
    }

    /**
     * @param ReconsolidateDocument $parameters
     * @access public
     * @return ReconsolidateDocumentResponse
     */
    public function ReconsolidateDocument(ReconsolidateDocument $parameters)
    {
      return $this->__soapCall('ReconsolidateDocument', array($parameters));
    }

    /**
     * @param ImportTranslationDocument $parameters
     * @access public
     * @return ImportTranslationDocumentResponse
     */
    public function ImportTranslationDocument(ImportTranslationDocument $parameters)
    {
      return $this->__soapCall('ImportTranslationDocument', array($parameters));
    }

    /**
     * @param ImportTranslationDocuments $parameters
     * @access public
     * @return ImportTranslationDocumentsResponse
     */
    public function ImportTranslationDocuments(ImportTranslationDocuments $parameters)
    {
      return $this->__soapCall('ImportTranslationDocuments', array($parameters));
    }

    /**
     * @param ImportTranslationDocumentWithFilterConfigResource $parameters
     * @access public
     * @return ImportTranslationDocumentWithFilterConfigResourceResponse
     */
    public function ImportTranslationDocumentWithFilterConfigResource(ImportTranslationDocumentWithFilterConfigResource $parameters)
    {
      return $this->__soapCall('ImportTranslationDocumentWithFilterConfigResource', array($parameters));
    }

    /**
     * @param ImportTranslationDocumentsWithFilterConfigResource $parameters
     * @access public
     * @return ImportTranslationDocumentsWithFilterConfigResourceResponse
     */
    public function ImportTranslationDocumentsWithFilterConfigResource(ImportTranslationDocumentsWithFilterConfigResource $parameters)
    {
      return $this->__soapCall('ImportTranslationDocumentsWithFilterConfigResource', array($parameters));
    }

    /**
     * @param ImportTranslationDocumentsWithOptions $parameters
     * @access public
     * @return ImportTranslationDocumentsWithOptionsResponse
     */
    public function ImportTranslationDocumentsWithOptions(ImportTranslationDocumentsWithOptions $parameters)
    {
      return $this->__soapCall('ImportTranslationDocumentsWithOptions', array($parameters));
    }

    /**
     * @param ImportBilingualTranslationDocument $parameters
     * @access public
     * @return ImportBilingualTranslationDocumentResponse
     */
    public function ImportBilingualTranslationDocument(ImportBilingualTranslationDocument $parameters)
    {
      return $this->__soapCall('ImportBilingualTranslationDocument', array($parameters));
    }

    /**
     * @param UpdateTranslationDocumentFromBilingual $parameters
     * @access public
     * @return UpdateTranslationDocumentFromBilingualResponse
     */
    public function UpdateTranslationDocumentFromBilingual(UpdateTranslationDocumentFromBilingual $parameters)
    {
      return $this->__soapCall('UpdateTranslationDocumentFromBilingual', array($parameters));
    }

    /**
     * @param UpdateTranslationDocumentFromTableRtf $parameters
     * @access public
     * @return UpdateTranslationDocumentFromTableRtfResponse
     */
    public function UpdateTranslationDocumentFromTableRtf(UpdateTranslationDocumentFromTableRtf $parameters)
    {
      return $this->__soapCall('UpdateTranslationDocumentFromTableRtf', array($parameters));
    }

    /**
     * @param ExportTranslationDocument $parameters
     * @access public
     * @return ExportTranslationDocumentResponse
     */
    public function ExportTranslationDocument(ExportTranslationDocument $parameters)
    {
      return $this->__soapCall('ExportTranslationDocument', array($parameters));
    }

    /**
     * @param ExportTranslationDocument2 $parameters
     * @access public
     * @return ExportTranslationDocument2Response
     */
    public function ExportTranslationDocument2(ExportTranslationDocument2 $parameters)
    {
      return $this->__soapCall('ExportTranslationDocument2', array($parameters));
    }

    /**
     * @param ExportTranslationDocumentAsXliffBilingual $parameters
     * @access public
     * @return ExportTranslationDocumentAsXliffBilingualResponse
     */
    public function ExportTranslationDocumentAsXliffBilingual(ExportTranslationDocumentAsXliffBilingual $parameters)
    {
      return $this->__soapCall('ExportTranslationDocumentAsXliffBilingual', array($parameters));
    }

    /**
     * @param ExportTranslationDocumentAsRtfBilingual $parameters
     * @access public
     * @return ExportTranslationDocumentAsRtfBilingualResponse
     */
    public function ExportTranslationDocumentAsRtfBilingual(ExportTranslationDocumentAsRtfBilingual $parameters)
    {
      return $this->__soapCall('ExportTranslationDocumentAsRtfBilingual', array($parameters));
    }

    /**
     * @param ExportTranslationDocumentAsTwoColumnRtf $parameters
     * @access public
     * @return ExportTranslationDocumentAsTwoColumnRtfResponse
     */
    public function ExportTranslationDocumentAsTwoColumnRtf(ExportTranslationDocumentAsTwoColumnRtf $parameters)
    {
      return $this->__soapCall('ExportTranslationDocumentAsTwoColumnRtf', array($parameters));
    }

    /**
     * @param ExportTranslationDocumentsAsSingleTwoColumnRtf $parameters
     * @access public
     * @return ExportTranslationDocumentsAsSingleTwoColumnRtfResponse
     */
    public function ExportTranslationDocumentsAsSingleTwoColumnRtf(ExportTranslationDocumentsAsSingleTwoColumnRtf $parameters)
    {
      return $this->__soapCall('ExportTranslationDocumentsAsSingleTwoColumnRtf', array($parameters));
    }

    /**
     * @param ReImportTranslationDocuments $parameters
     * @access public
     * @return ReImportTranslationDocumentsResponse
     */
    public function ReImportTranslationDocuments(ReImportTranslationDocuments $parameters)
    {
      return $this->__soapCall('ReImportTranslationDocuments', array($parameters));
    }

    /**
     * @param ReImportTranslationDocumentsWithFilterConfigResource $parameters
     * @access public
     * @return ReImportTranslationDocumentsWithFilterConfigResourceResponse
     */
    public function ReImportTranslationDocumentsWithFilterConfigResource(ReImportTranslationDocumentsWithFilterConfigResource $parameters)
    {
      return $this->__soapCall('ReImportTranslationDocumentsWithFilterConfigResource', array($parameters));
    }

    /**
     * @param AddTranslatedDocumentToLiveDocsCorpus $parameters
     * @access public
     * @return AddTranslatedDocumentToLiveDocsCorpusResponse
     */
    public function AddTranslatedDocumentToLiveDocsCorpus(AddTranslatedDocumentToLiveDocsCorpus $parameters)
    {
      return $this->__soapCall('AddTranslatedDocumentToLiveDocsCorpus', array($parameters));
    }

    /**
     * @param GetStatisticsOnProject $parameters
     * @access public
     * @return GetStatisticsOnProjectResponse
     */
    public function GetStatisticsOnProject(GetStatisticsOnProject $parameters)
    {
      return $this->__soapCall('GetStatisticsOnProject', array($parameters));
    }

    /**
     * @param GetStatisticsOnTranslationDocuments $parameters
     * @access public
     * @return GetStatisticsOnTranslationDocumentsResponse
     */
    public function GetStatisticsOnTranslationDocuments(GetStatisticsOnTranslationDocuments $parameters)
    {
      return $this->__soapCall('GetStatisticsOnTranslationDocuments', array($parameters));
    }

    /**
     * @param ListAnalysisReports $parameters
     * @access public
     * @return ListAnalysisReportsResponse
     */
    public function ListAnalysisReports(ListAnalysisReports $parameters)
    {
      return $this->__soapCall('ListAnalysisReports', array($parameters));
    }

    /**
     * @param GetAnalysisReportData $parameters
     * @access public
     * @return GetAnalysisReportDataResponse
     */
    public function GetAnalysisReportData(GetAnalysisReportData $parameters)
    {
      return $this->__soapCall('GetAnalysisReportData', array($parameters));
    }

    /**
     * @param GetAnalysisReportAsCSV $parameters
     * @access public
     * @return GetAnalysisReportAsCSVResponse
     */
    public function GetAnalysisReportAsCSV(GetAnalysisReportAsCSV $parameters)
    {
      return $this->__soapCall('GetAnalysisReportAsCSV', array($parameters));
    }

    /**
     * @param RunAnalysis $parameters
     * @access public
     * @return RunAnalysisResponse
     */
    public function RunAnalysis(RunAnalysis $parameters)
    {
      return $this->__soapCall('RunAnalysis', array($parameters));
    }

    /**
     * @param StartStatisticsOnProjectTask $parameters
     * @access public
     * @return StartStatisticsOnProjectTaskResponse
     */
    public function StartStatisticsOnProjectTask(StartStatisticsOnProjectTask $parameters)
    {
      return $this->__soapCall('StartStatisticsOnProjectTask', array($parameters));
    }

    /**
     * @param StartStatisticsOnTranslationDocumentsTask $parameters
     * @access public
     * @return StartStatisticsOnTranslationDocumentsTaskResponse
     */
    public function StartStatisticsOnTranslationDocumentsTask(StartStatisticsOnTranslationDocumentsTask $parameters)
    {
      return $this->__soapCall('StartStatisticsOnTranslationDocumentsTask', array($parameters));
    }

    /**
     * @param ListPostTranslationAnalysisReports $parameters
     * @access public
     * @return ListPostTranslationAnalysisReportsResponse
     */
    public function ListPostTranslationAnalysisReports(ListPostTranslationAnalysisReports $parameters)
    {
      return $this->__soapCall('ListPostTranslationAnalysisReports', array($parameters));
    }

    /**
     * @param GetPostTranslationAnalysisReportData $parameters
     * @access public
     * @return GetPostTranslationAnalysisReportDataResponse
     */
    public function GetPostTranslationAnalysisReportData(GetPostTranslationAnalysisReportData $parameters)
    {
      return $this->__soapCall('GetPostTranslationAnalysisReportData', array($parameters));
    }

    /**
     * @param GetPostTranslationAnalysisReportAsCSV $parameters
     * @access public
     * @return GetPostTranslationAnalysisReportAsCSVResponse
     */
    public function GetPostTranslationAnalysisReportAsCSV(GetPostTranslationAnalysisReportAsCSV $parameters)
    {
      return $this->__soapCall('GetPostTranslationAnalysisReportAsCSV', array($parameters));
    }

    /**
     * @param RunPostTranslationAnalysis $parameters
     * @access public
     * @return RunPostTranslationAnalysisResponse
     */
    public function RunPostTranslationAnalysis(RunPostTranslationAnalysis $parameters)
    {
      return $this->__soapCall('RunPostTranslationAnalysis', array($parameters));
    }

    /**
     * @param StartPostTranslationAnalysisTask $parameters
     * @access public
     * @return StartPostTranslationAnalysisTaskResponse
     */
    public function StartPostTranslationAnalysisTask(StartPostTranslationAnalysisTask $parameters)
    {
      return $this->__soapCall('StartPostTranslationAnalysisTask', array($parameters));
    }

    /**
     * @param PretranslateProject $parameters
     * @access public
     * @return PretranslateProjectResponse
     */
    public function PretranslateProject(PretranslateProject $parameters)
    {
      return $this->__soapCall('PretranslateProject', array($parameters));
    }

    /**
     * @param PretranslateDocuments $parameters
     * @access public
     * @return PretranslateDocumentsResponse
     */
    public function PretranslateDocuments(PretranslateDocuments $parameters)
    {
      return $this->__soapCall('PretranslateDocuments', array($parameters));
    }

    /**
     * @param StartPretranslateProjectTask $parameters
     * @access public
     * @return StartPretranslateProjectTaskResponse
     */
    public function StartPretranslateProjectTask(StartPretranslateProjectTask $parameters)
    {
      return $this->__soapCall('StartPretranslateProjectTask', array($parameters));
    }

    /**
     * @param StartPretranslateDocumentsTask $parameters
     * @access public
     * @return StartPretranslateDocumentsTaskResponse
     */
    public function StartPretranslateDocumentsTask(StartPretranslateDocumentsTask $parameters)
    {
      return $this->__soapCall('StartPretranslateDocumentsTask', array($parameters));
    }

    /**
     * @param AsiaOnlineGetLanguagePairCode $parameters
     * @access public
     * @return AsiaOnlineGetLanguagePairCodeResponse
     */
    public function AsiaOnlineGetLanguagePairCode(AsiaOnlineGetLanguagePairCode $parameters)
    {
      return $this->__soapCall('AsiaOnlineGetLanguagePairCode', array($parameters));
    }

    /**
     * @param AsiaOnlineGetDomainCombinations $parameters
     * @access public
     * @return AsiaOnlineGetDomainCombinationsResponse
     */
    public function AsiaOnlineGetDomainCombinations(AsiaOnlineGetDomainCombinations $parameters)
    {
      return $this->__soapCall('AsiaOnlineGetDomainCombinations', array($parameters));
    }

    /**
     * @param AsiaOnlineBeginTranslation $parameters
     * @access public
     * @return AsiaOnlineBeginTranslationResponse
     */
    public function AsiaOnlineBeginTranslation(AsiaOnlineBeginTranslation $parameters)
    {
      return $this->__soapCall('AsiaOnlineBeginTranslation', array($parameters));
    }

    /**
     * @param AsiaOnlineGetTranslationStatus $parameters
     * @access public
     * @return AsiaOnlineGetTranslationStatusResponse
     */
    public function AsiaOnlineGetTranslationStatus(AsiaOnlineGetTranslationStatus $parameters)
    {
      return $this->__soapCall('AsiaOnlineGetTranslationStatus', array($parameters));
    }

    /**
     * @param AsiaOnlineGetProjectIds $parameters
     * @access public
     * @return AsiaOnlineGetProjectIdsResponse
     */
    public function AsiaOnlineGetProjectIds(AsiaOnlineGetProjectIds $parameters)
    {
      return $this->__soapCall('AsiaOnlineGetProjectIds', array($parameters));
    }

    /**
     * @param ConfirmAndUpdate $parameters
     * @access public
     * @return ConfirmAndUpdateResponse
     */
    public function ConfirmAndUpdate(ConfirmAndUpdate $parameters)
    {
      return $this->__soapCall('ConfirmAndUpdate', array($parameters));
    }

    /**
     * @param ConfirmAndUpdate2 $parameters
     * @access public
     * @return ConfirmAndUpdate2Response
     */
    public function ConfirmAndUpdate2(ConfirmAndUpdate2 $parameters)
    {
      return $this->__soapCall('ConfirmAndUpdate2', array($parameters));
    }

    /**
     * @param StartConfirmAndUpdateTask $parameters
     * @access public
     * @return StartConfirmAndUpdateTaskResponse
     */
    public function StartConfirmAndUpdateTask(StartConfirmAndUpdateTask $parameters)
    {
      return $this->__soapCall('StartConfirmAndUpdateTask', array($parameters));
    }

    /**
     * @param XTranslate $parameters
     * @access public
     * @return XTranslateResponse
     */
    public function XTranslate(XTranslate $parameters)
    {
      return $this->__soapCall('XTranslate', array($parameters));
    }

    /**
     * @param StartXTranslateTask $parameters
     * @access public
     * @return StartXTranslateTaskResponse
     */
    public function StartXTranslateTask(StartXTranslateTask $parameters)
    {
      return $this->__soapCall('StartXTranslateTask', array($parameters));
    }

    /**
     * @param GetDocumentHistory $parameters
     * @access public
     * @return GetDocumentHistoryResponse
     */
    public function GetDocumentHistory(GetDocumentHistory $parameters)
    {
      return $this->__soapCall('GetDocumentHistory', array($parameters));
    }

    /**
     * @param ListPackagesForProject $parameters
     * @access public
     * @return ListPackagesForProjectResponse
     */
    public function ListPackagesForProject(ListPackagesForProject $parameters)
    {
      return $this->__soapCall('ListPackagesForProject', array($parameters));
    }

    /**
     * @param ListPackagesForProjectAndUser $parameters
     * @access public
     * @return ListPackagesForProjectAndUserResponse
     */
    public function ListPackagesForProjectAndUser(ListPackagesForProjectAndUser $parameters)
    {
      return $this->__soapCall('ListPackagesForProjectAndUser', array($parameters));
    }

    /**
     * @param PreparePackageForDownload $parameters
     * @access public
     * @return PreparePackageForDownloadResponse
     */
    public function PreparePackageForDownload(PreparePackageForDownload $parameters)
    {
      return $this->__soapCall('PreparePackageForDownload', array($parameters));
    }

    /**
     * @param ListContentOfPackages $parameters
     * @access public
     * @return ListContentOfPackagesResponse
     */
    public function ListContentOfPackages(ListContentOfPackages $parameters)
    {
      return $this->__soapCall('ListContentOfPackages', array($parameters));
    }

    /**
     * @param DeliverPackage $parameters
     * @access public
     * @return DeliverPackageResponse
     */
    public function DeliverPackage(DeliverPackage $parameters)
    {
      return $this->__soapCall('DeliverPackage', array($parameters));
    }

    /**
     * @param CreateProjectFromPackage $parameters
     * @access public
     * @return CreateProjectFromPackageResponse
     */
    public function CreateProjectFromPackage(CreateProjectFromPackage $parameters)
    {
      return $this->__soapCall('CreateProjectFromPackage', array($parameters));
    }

    /**
     * @param CreateProjectFromPackage2 $parameters
     * @access public
     * @return CreateProjectFromPackage2Response
     */
    public function CreateProjectFromPackage2(CreateProjectFromPackage2 $parameters)
    {
      return $this->__soapCall('CreateProjectFromPackage2', array($parameters));
    }

    /**
     * @param UpdateProjectFromPackage $parameters
     * @access public
     * @return UpdateProjectFromPackageResponse
     */
    public function UpdateProjectFromPackage(UpdateProjectFromPackage $parameters)
    {
      return $this->__soapCall('UpdateProjectFromPackage', array($parameters));
    }

    /**
     * @param CreateDeliveryPackage $parameters
     * @access public
     * @return CreateDeliveryPackageResponse
     */
    public function CreateDeliveryPackage(CreateDeliveryPackage $parameters)
    {
      return $this->__soapCall('CreateDeliveryPackage', array($parameters));
    }

    /**
     * @param RunQAGetReport $parameters
     * @access public
     * @return RunQAGetReportResponse
     */
    public function RunQAGetReport(RunQAGetReport $parameters)
    {
      return $this->__soapCall('RunQAGetReport', array($parameters));
    }

    /**
     * @param StartQAReportTask $parameters
     * @access public
     * @return StartQAReportTaskResponse
     */
    public function StartQAReportTask(StartQAReportTask $parameters)
    {
      return $this->__soapCall('StartQAReportTask', array($parameters));
    }

    /**
     * @param CreateImageLocalizationPack $parameters
     * @access public
     * @return CreateImageLocalizationPackResponse
     */
    public function CreateImageLocalizationPack(CreateImageLocalizationPack $parameters)
    {
      return $this->__soapCall('CreateImageLocalizationPack', array($parameters));
    }

    /**
     * @param ImportImageLocalizationPack $parameters
     * @access public
     * @return ImportImageLocalizationPackResponse
     */
    public function ImportImageLocalizationPack(ImportImageLocalizationPack $parameters)
    {
      return $this->__soapCall('ImportImageLocalizationPack', array($parameters));
    }

    /**
     * @param RestoreArchive $parameters
     * @access public
     * @return RestoreArchiveResponse
     */
    public function RestoreArchive(RestoreArchive $parameters)
    {
      return $this->__soapCall('RestoreArchive', array($parameters));
    }

}
