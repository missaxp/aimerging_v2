<?php

class GetStatisticsOnTranslationDocumentsResponse
{

    /**
     * @var StatisticsResultInfo $GetStatisticsOnTranslationDocumentsResult
     * @access public
     */
    public $GetStatisticsOnTranslationDocumentsResult = null;

    /**
     * @param StatisticsResultInfo $GetStatisticsOnTranslationDocumentsResult
     * @access public
     */
    public function __construct($GetStatisticsOnTranslationDocumentsResult)
    {
      $this->GetStatisticsOnTranslationDocumentsResult = $GetStatisticsOnTranslationDocumentsResult;
    }

}
