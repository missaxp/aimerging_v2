<?php

class ListProjectTBs3
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var string[] $targetLanguages
     * @access public
     */
    public $targetLanguages = null;

    /**
     * @param guid $serverProjectGuid
     * @param string[] $targetLanguages
     * @access public
     */
    public function __construct($serverProjectGuid, $targetLanguages)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->targetLanguages = $targetLanguages;
    }

}
