<?php

class SliceDocumentResponse
{

    /**
     * @var ServerProjectTranslationDocSliceInfo[] $SliceDocumentResult
     * @access public
     */
    public $SliceDocumentResult = null;

    /**
     * @param ServerProjectTranslationDocSliceInfo[] $SliceDocumentResult
     * @access public
     */
    public function __construct($SliceDocumentResult)
    {
      $this->SliceDocumentResult = $SliceDocumentResult;
    }

}
