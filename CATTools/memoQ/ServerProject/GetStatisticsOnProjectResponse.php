<?php

class GetStatisticsOnProjectResponse
{

    /**
     * @var StatisticsResultInfo $GetStatisticsOnProjectResult
     * @access public
     */
    public $GetStatisticsOnProjectResult = null;

    /**
     * @param StatisticsResultInfo $GetStatisticsOnProjectResult
     * @access public
     */
    public function __construct($GetStatisticsOnProjectResult)
    {
      $this->GetStatisticsOnProjectResult = $GetStatisticsOnProjectResult;
    }

}
