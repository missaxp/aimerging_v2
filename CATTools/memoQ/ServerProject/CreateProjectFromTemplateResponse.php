<?php

class CreateProjectFromTemplateResponse
{

    /**
     * @var TemplateBasedProjectCreationResultInfo $CreateProjectFromTemplateResult
     * @access public
     */
    public $CreateProjectFromTemplateResult = null;

    /**
     * @param TemplateBasedProjectCreationResultInfo $CreateProjectFromTemplateResult
     * @access public
     */
    public function __construct($CreateProjectFromTemplateResult)
    {
      $this->CreateProjectFromTemplateResult = $CreateProjectFromTemplateResult;
    }

}
