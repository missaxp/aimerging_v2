<?php

class ListPackagesForProject
{

    /**
     * @var guid $projectGuid
     * @access public
     */
    public $projectGuid = null;

    /**
     * @param guid $projectGuid
     * @access public
     */
    public function __construct($projectGuid)
    {
      $this->projectGuid = $projectGuid;
    }

}
