<?php

class CreateProjectFromTemplate
{

    /**
     * @var TemplateBasedProjectCreateInfo $createInfo
     * @access public
     */
    public $createInfo = null;

    /**
     * @param TemplateBasedProjectCreateInfo $createInfo
     * @access public
     */
    public function __construct($createInfo)
    {
      $this->createInfo = $createInfo;
    }

}
