<?php

class SetProjectTBs3
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var ServerProjectTBsForTargetLang[] $tbAssignments
     * @access public
     */
    public $tbAssignments = null;

    /**
     * @param guid $serverProjectGuid
     * @param ServerProjectTBsForTargetLang[] $tbAssignments
     * @access public
     */
    public function __construct($serverProjectGuid, $tbAssignments)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->tbAssignments = $tbAssignments;
    }

}
