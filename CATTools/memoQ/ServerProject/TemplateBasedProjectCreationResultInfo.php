<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/ResultInfo.php');

class TemplateBasedProjectCreationResultInfo extends ResultInfo
{

    /**
     * @var guid $ProjectGuid
     * @access public
     */
    public $ProjectGuid = null;

    /**
     * @param ResultStatus $ResultStatus
     * @param guid $ProjectGuid
     * @access public
     */
    public function __construct($ResultStatus, $ProjectGuid)
    {
      parent::__construct($ResultStatus);
      $this->ProjectGuid = $ProjectGuid;
    }

}
