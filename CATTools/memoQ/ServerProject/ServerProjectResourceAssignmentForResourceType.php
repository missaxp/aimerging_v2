<?php

class ServerProjectResourceAssignmentForResourceType
{

    /**
     * @var ResourceType $ResourceType
     * @access public
     */
    public $ResourceType = null;

    /**
     * @var ServerProjectResourceAssignment[] $ServerProjectResourceAssignment
     * @access public
     */
    public $ServerProjectResourceAssignment = null;

    /**
     * @param ResourceType $ResourceType
     * @access public
     */
    public function __construct($ResourceType)
    {
      $this->ResourceType = $ResourceType;
    }

}
