<?php

class DocumentRowsLockedHistoryItemInfoLockModes
{
    const __default = 'AllRows';
    const AllRows = 'AllRows';
    const SpecificRows = 'SpecificRows';
    const RowInDifferentLanguage = 'RowInDifferentLanguage';


}
