<?php

class CreateImageLocalizationPack
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var guid[] $documentGuids
     * @access public
     */
    public $documentGuids = null;

    /**
     * @param guid $serverProjectGuid
     * @param guid[] $documentGuids
     * @access public
     */
    public function __construct($serverProjectGuid, $documentGuids)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->documentGuids = $documentGuids;
    }

}
