<?php

class ImportTranslationDocument
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var guid $fileGuid
     * @access public
     */
    public $fileGuid = null;

    /**
     * @var string[] $targetLangCodes
     * @access public
     */
    public $targetLangCodes = null;

    /**
     * @var string $importSettingsXML
     * @access public
     */
    public $importSettingsXML = null;

    /**
     * @param guid $serverProjectGuid
     * @param guid $fileGuid
     * @param string[] $targetLangCodes
     * @param string $importSettingsXML
     * @access public
     */
    public function __construct($serverProjectGuid, $fileGuid, $targetLangCodes, $importSettingsXML)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->fileGuid = $fileGuid;
      $this->targetLangCodes = $targetLangCodes;
      $this->importSettingsXML = $importSettingsXML;
    }

}
