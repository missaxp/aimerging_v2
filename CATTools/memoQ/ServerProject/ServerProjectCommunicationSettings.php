<?php

class ServerProjectCommunicationSettings
{

    /**
     * @var ServerProjectConfidentialitySettings $ConfidentialitySettings
     * @access public
     */
    public $ConfidentialitySettings = null;

    /**
     * @var boolean $EnableCommunication
     * @access public
     */
    public $EnableCommunication = null;

    /**
     * @var ServerProjectNotificationSettings $NotificationSettings
     * @access public
     */
    public $NotificationSettings = null;

    /**
     * @var boolean $PreventDeliveryOnQAError
     * @access public
     */
    public $PreventDeliveryOnQAError = null;

    /**
     * @access public
     */
    public function __construct()
    {
    
    }

}
