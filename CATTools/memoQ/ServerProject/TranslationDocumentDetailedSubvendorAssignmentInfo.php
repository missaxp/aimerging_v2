<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentDetailedRoleAssignmentInfo.php');

class TranslationDocumentDetailedSubvendorAssignmentInfo extends TranslationDocumentDetailedRoleAssignmentInfo
{

    /**
     * @var TranslationDocumentAssigneeInfo $SubvendorGroup
     * @access public
     */
    public $SubvendorGroup = null;

    /**
     * @param TranslationDocumentAssignmentType $AssignmentType
     * @param dateTime $Deadline
     * @param int $RoleId
     * @access public
     */
    public function __construct($AssignmentType, $Deadline, $RoleId)
    {
      parent::__construct($AssignmentType, $Deadline, $RoleId);
    }

}
