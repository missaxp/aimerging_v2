<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentHistoryItemInfo.php');

class DocumentRowsLockedHistoryItemInfo extends DocumentHistoryItemInfo
{

    /**
     * @var DocumentRowsLockedHistoryItemInfoLockModes $LockMode
     * @access public
     */
    public $LockMode = null;

    /**
     * @var boolean $LockNonRepetitions
     * @access public
     */
    public $LockNonRepetitions = null;

    /**
     * @var boolean $LockPreTranslatedSegments
     * @access public
     */
    public $LockPreTranslatedSegments = null;

    /**
     * @var boolean $LockRepetitions
     * @access public
     */
    public $LockRepetitions = null;

    /**
     * @var boolean $LockReviewer1ConfirmedSegments
     * @access public
     */
    public $LockReviewer1ConfirmedSegments = null;

    /**
     * @var boolean $LockReviewer2ConfirmedSegments
     * @access public
     */
    public $LockReviewer2ConfirmedSegments = null;

    /**
     * @var boolean $LockSegments
     * @access public
     */
    public $LockSegments = null;

    /**
     * @var boolean $LockTranslatorConfirmedSegments
     * @access public
     */
    public $LockTranslatorConfirmedSegments = null;

    /**
     * @param guid $DocumentOrDivisionGuid
     * @param DocumentHistoryItemType $HistoryItemType
     * @param guid $ModifierSVGroupId
     * @param guid $ModifierUserGuid
     * @param dateTime $TimeStamp
     * @param DocumentRowsLockedHistoryItemInfoLockModes $LockMode
     * @param boolean $LockNonRepetitions
     * @param boolean $LockPreTranslatedSegments
     * @param boolean $LockRepetitions
     * @param boolean $LockReviewer1ConfirmedSegments
     * @param boolean $LockReviewer2ConfirmedSegments
     * @param boolean $LockSegments
     * @param boolean $LockTranslatorConfirmedSegments
     * @access public
     */
    public function __construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp, $LockMode, $LockNonRepetitions, $LockPreTranslatedSegments, $LockRepetitions, $LockReviewer1ConfirmedSegments, $LockReviewer2ConfirmedSegments, $LockSegments, $LockTranslatorConfirmedSegments)
    {
      parent::__construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp);
      $this->LockMode = $LockMode;
      $this->LockNonRepetitions = $LockNonRepetitions;
      $this->LockPreTranslatedSegments = $LockPreTranslatedSegments;
      $this->LockRepetitions = $LockRepetitions;
      $this->LockReviewer1ConfirmedSegments = $LockReviewer1ConfirmedSegments;
      $this->LockReviewer2ConfirmedSegments = $LockReviewer2ConfirmedSegments;
      $this->LockSegments = $LockSegments;
      $this->LockTranslatorConfirmedSegments = $LockTranslatorConfirmedSegments;
    }

}
