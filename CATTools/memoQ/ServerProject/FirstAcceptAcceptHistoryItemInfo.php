<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentHistoryItemInfo.php');

class FirstAcceptAcceptHistoryItemInfo extends DocumentHistoryItemInfo
{

    /**
     * @var int $Role
     * @access public
     */
    public $Role = null;

    /**
     * @param guid $DocumentOrDivisionGuid
     * @param DocumentHistoryItemType $HistoryItemType
     * @param guid $ModifierSVGroupId
     * @param guid $ModifierUserGuid
     * @param dateTime $TimeStamp
     * @param int $Role
     * @access public
     */
    public function __construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp, $Role)
    {
      parent::__construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp);
      $this->Role = $Role;
    }

}
