<?php

class SetProjectTBs
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var guid[] $tbGuids
     * @access public
     */
    public $tbGuids = null;

    /**
     * @var guid $primaryTB
     * @access public
     */
    public $primaryTB = null;

    /**
     * @param guid $serverProjectGuid
     * @param guid[] $tbGuids
     * @param guid $primaryTB
     * @access public
     */
    public function __construct($serverProjectGuid, $tbGuids, $primaryTB)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->tbGuids = $tbGuids;
      $this->primaryTB = $primaryTB;
    }

}
