<?php

class StartStatisticsOnTranslationDocumentsTaskResponse
{

    /**
     * @var TaskInfo $StartStatisticsOnTranslationDocumentsTaskResult
     * @access public
     */
    public $StartStatisticsOnTranslationDocumentsTaskResult = null;

    /**
     * @param TaskInfo $StartStatisticsOnTranslationDocumentsTaskResult
     * @access public
     */
    public function __construct($StartStatisticsOnTranslationDocumentsTaskResult)
    {
      $this->StartStatisticsOnTranslationDocumentsTaskResult = $StartStatisticsOnTranslationDocumentsTaskResult;
    }

}
