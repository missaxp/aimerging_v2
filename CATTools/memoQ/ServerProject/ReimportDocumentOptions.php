<?php

class ReimportDocumentOptions
{

    /**
     * @var guid[] $DocumentsToReplace
     * @access public
     */
    public $DocumentsToReplace = null;

    /**
     * @var guid $FileGuid
     * @access public
     */
    public $FileGuid = null;

    /**
     * @var boolean $KeepUserAssignments
     * @access public
     */
    public $KeepUserAssignments = null;

    /**
     * @param guid $FileGuid
     * @param boolean $KeepUserAssignments
     * @access public
     */
    public function __construct($FileGuid, $KeepUserAssignments)
    {
      $this->FileGuid = $FileGuid;
      $this->KeepUserAssignments = $KeepUserAssignments;
    }

}
