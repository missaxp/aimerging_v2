<?php

class AnalysisReportForDocument
{

    /**
     * @var guid $DocumentGuid
     * @access public
     */
    public $DocumentGuid = null;

    /**
     * @var AnalysisReportItem $Summary
     * @access public
     */
    public $Summary = null;

    /**
     * @param guid $DocumentGuid
     * @access public
     */
    public function __construct($DocumentGuid)
    {
      $this->DocumentGuid = $DocumentGuid;
    }

}
