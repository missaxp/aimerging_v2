<?php

class SetProjectCorpora
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var guid[] $corporaGuids
     * @access public
     */
    public $corporaGuids = null;

    /**
     * @param guid $serverProjectGuid
     * @param guid[] $corporaGuids
     * @access public
     */
    public function __construct($serverProjectGuid, $corporaGuids)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->corporaGuids = $corporaGuids;
    }

}
