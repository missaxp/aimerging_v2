<?php

class ListProjectTranslationDocumentSlices
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var guid $documentGuid
     * @access public
     */
    public $documentGuid = null;

    /**
     * @var ListServerProjectTranslationDocumentSliceOptions $options
     * @access public
     */
    public $options = null;

    /**
     * @param guid $serverProjectGuid
     * @param guid $documentGuid
     * @param ListServerProjectTranslationDocumentSliceOptions $options
     * @access public
     */
    public function __construct($serverProjectGuid, $documentGuid, $options)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->documentGuid = $documentGuid;
      $this->options = $options;
    }

}
