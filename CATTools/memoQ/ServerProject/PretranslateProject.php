<?php

class PretranslateProject
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var string[] $targetLangCodes
     * @access public
     */
    public $targetLangCodes = null;

    /**
     * @var PretranslateOptions $options
     * @access public
     */
    public $options = null;

    /**
     * @param guid $serverProjectGuid
     * @param string[] $targetLangCodes
     * @param PretranslateOptions $options
     * @access public
     */
    public function __construct($serverProjectGuid, $targetLangCodes, $options)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->targetLangCodes = $targetLangCodes;
      $this->options = $options;
    }

}
