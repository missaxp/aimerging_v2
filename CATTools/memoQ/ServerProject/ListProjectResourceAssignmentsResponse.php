<?php

class ListProjectResourceAssignmentsResponse
{

    /**
     * @var ServerProjectResourceAssignmentDetails[] $ListProjectResourceAssignmentsResult
     * @access public
     */
    public $ListProjectResourceAssignmentsResult = null;

    /**
     * @param ServerProjectResourceAssignmentDetails[] $ListProjectResourceAssignmentsResult
     * @access public
     */
    public function __construct($ListProjectResourceAssignmentsResult)
    {
      $this->ListProjectResourceAssignmentsResult = $ListProjectResourceAssignmentsResult;
    }

}
