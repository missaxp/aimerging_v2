<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/LightResourceInfo.php');

class ProjectTemplateResourceInfo extends LightResourceInfo
{

    /**
     * @var string $SourceLangCode
     * @access public
     */
    public $SourceLangCode = null;

    /**
     * @var string[] $TargetLangCodes
     * @access public
     */
    public $TargetLangCodes = null;

    /**
     * @param guid $Guid
     * @param boolean $Readonly
     * @param boolean $IsDefault
     * @access public
     */
    public function __construct($Guid, $Readonly, $IsDefault)
    {
      parent::__construct($Guid, $Readonly, $IsDefault);
    }

}
