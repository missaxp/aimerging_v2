<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentHistoryItemInfo.php');

class AutomatedActionStartedHistoryItemInfo extends DocumentHistoryItemInfo
{

    /**
     * @var AutomatedActionStartedHistoryItemInfoAutomatedActionTypes $ActionType
     * @access public
     */
    public $ActionType = null;

    /**
     * @param guid $DocumentOrDivisionGuid
     * @param DocumentHistoryItemType $HistoryItemType
     * @param guid $ModifierSVGroupId
     * @param guid $ModifierUserGuid
     * @param dateTime $TimeStamp
     * @param AutomatedActionStartedHistoryItemInfoAutomatedActionTypes $ActionType
     * @access public
     */
    public function __construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp, $ActionType)
    {
      parent::__construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp);
      $this->ActionType = $ActionType;
    }

}
