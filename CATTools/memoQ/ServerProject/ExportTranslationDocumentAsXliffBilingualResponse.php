<?php

class ExportTranslationDocumentAsXliffBilingualResponse
{

    /**
     * @var FileResultInfo $ExportTranslationDocumentAsXliffBilingualResult
     * @access public
     */
    public $ExportTranslationDocumentAsXliffBilingualResult = null;

    /**
     * @param FileResultInfo $ExportTranslationDocumentAsXliffBilingualResult
     * @access public
     */
    public function __construct($ExportTranslationDocumentAsXliffBilingualResult)
    {
      $this->ExportTranslationDocumentAsXliffBilingualResult = $ExportTranslationDocumentAsXliffBilingualResult;
    }

}
