<?php

class SetPackageDeliveryOptions
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var PackageDeliveryOptions $options
     * @access public
     */
    public $options = null;

    /**
     * @param guid $serverProjectGuid
     * @param PackageDeliveryOptions $options
     * @access public
     */
    public function __construct($serverProjectGuid, $options)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->options = $options;
    }

}
