<?php

class PreparePackageResultInfo
{

    /**
     * @var boolean $PackageReady
     * @access public
     */
    public $PackageReady = null;

    /**
     * @var guid $ResultFileId
     * @access public
     */
    public $ResultFileId = null;

    /**
     * @param boolean $PackageReady
     * @access public
     */
    public function __construct($PackageReady)
    {
      $this->PackageReady = $PackageReady;
    }

}
