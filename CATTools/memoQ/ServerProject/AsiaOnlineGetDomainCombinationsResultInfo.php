<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/ResultInfo.php');

class AsiaOnlineGetDomainCombinationsResultInfo extends ResultInfo
{

    /**
     * @var AsiaOnlineDomainCombination[] $DomainCombinations
     * @access public
     */
    public $DomainCombinations = null;

    /**
     * @var int $LanguagePairCode
     * @access public
     */
    public $LanguagePairCode = null;

    /**
     * @param ResultStatus $ResultStatus
     * @param int $LanguagePairCode
     * @access public
     */
    public function __construct($ResultStatus, $LanguagePairCode)
    {
      parent::__construct($ResultStatus);
      $this->LanguagePairCode = $LanguagePairCode;
    }

}
