<?php

class CreateProject2Response
{

    /**
     * @var guid $CreateProject2Result
     * @access public
     */
    public $CreateProject2Result = null;

    /**
     * @param guid $CreateProject2Result
     * @access public
     */
    public function __construct($CreateProject2Result)
    {
      $this->CreateProject2Result = $CreateProject2Result;
    }

}
