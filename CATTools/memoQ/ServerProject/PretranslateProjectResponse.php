<?php

class PretranslateProjectResponse
{

    /**
     * @var ResultInfo $PretranslateProjectResult
     * @access public
     */
    public $PretranslateProjectResult = null;

    /**
     * @param ResultInfo $PretranslateProjectResult
     * @access public
     */
    public function __construct($PretranslateProjectResult)
    {
      $this->PretranslateProjectResult = $PretranslateProjectResult;
    }

}
