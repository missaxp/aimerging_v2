<?php

class GetDocumentHistoryResponse
{

    /**
     * @var DocumentHistoryItemInfo[] $GetDocumentHistoryResult
     * @access public
     */
    public $GetDocumentHistoryResult = null;

    /**
     * @param DocumentHistoryItemInfo[] $GetDocumentHistoryResult
     * @access public
     */
    public function __construct($GetDocumentHistoryResult)
    {
      $this->GetDocumentHistoryResult = $GetDocumentHistoryResult;
    }

}
