<?php

class CreateProjectFromPackage2Response
{

    /**
     * @var guid $CreateProjectFromPackage2Result
     * @access public
     */
    public $CreateProjectFromPackage2Result = null;

    /**
     * @param guid $CreateProjectFromPackage2Result
     * @access public
     */
    public function __construct($CreateProjectFromPackage2Result)
    {
      $this->CreateProjectFromPackage2Result = $CreateProjectFromPackage2Result;
    }

}
