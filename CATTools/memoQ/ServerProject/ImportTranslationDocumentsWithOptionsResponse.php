<?php

class ImportTranslationDocumentsWithOptionsResponse
{

    /**
     * @var TranslationDocImportResultInfo[] $ImportTranslationDocumentsWithOptionsResult
     * @access public
     */
    public $ImportTranslationDocumentsWithOptionsResult = null;

    /**
     * @param TranslationDocImportResultInfo[] $ImportTranslationDocumentsWithOptionsResult
     * @access public
     */
    public function __construct($ImportTranslationDocumentsWithOptionsResult)
    {
      $this->ImportTranslationDocumentsWithOptionsResult = $ImportTranslationDocumentsWithOptionsResult;
    }

}
