<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/ResultInfo.php');

class TranslationDocumentRoleAssignmentResultInfo extends ResultInfo
{

    /**
     * @var string $ErrorCode
     * @access public
     */
    public $ErrorCode = null;

    /**
     * @var int $RoleId
     * @access public
     */
    public $RoleId = null;

    /**
     * @param ResultStatus $ResultStatus
     * @param int $RoleId
     * @access public
     */
    public function __construct($ResultStatus, $RoleId)
    {
      parent::__construct($ResultStatus);
      $this->RoleId = $RoleId;
    }

}
