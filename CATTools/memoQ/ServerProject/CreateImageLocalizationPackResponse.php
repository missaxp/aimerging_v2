<?php

class CreateImageLocalizationPackResponse
{

    /**
     * @var FileResultInfo $CreateImageLocalizationPackResult
     * @access public
     */
    public $CreateImageLocalizationPackResult = null;

    /**
     * @param FileResultInfo $CreateImageLocalizationPackResult
     * @access public
     */
    public function __construct($CreateImageLocalizationPackResult)
    {
      $this->CreateImageLocalizationPackResult = $CreateImageLocalizationPackResult;
    }

}
