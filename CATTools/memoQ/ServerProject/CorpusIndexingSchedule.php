<?php

class CorpusIndexingSchedule
{
    const __default = 'OnContentChanged';
    const OnContentChanged = 'OnContentChanged';
    const Once = 'Once';
    const EveryHour = 'EveryHour';
    const EveryFourHours = 'EveryFourHours';
    const EveryEightHours = 'EveryEightHours';
    const EveryTwelveHours = 'EveryTwelveHours';
    const Daily = 'Daily';
    const Weekly = 'Weekly';


}
