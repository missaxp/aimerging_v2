<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentDetailedRoleAssignmentInfo.php');

class TranslationDocumentDetailedFirstAcceptAssignmentInfo extends TranslationDocumentDetailedRoleAssignmentInfo
{

    /**
     * @var dateTime $FirstAcceptDeadline
     * @access public
     */
    public $FirstAcceptDeadline = null;

    /**
     * @var FirstAcceptStatus $Status
     * @access public
     */
    public $Status = null;

    /**
     * @var TranslationDocumentFirstAcceptUserInfo[] $Users
     * @access public
     */
    public $Users = null;

    /**
     * @param TranslationDocumentAssignmentType $AssignmentType
     * @param dateTime $Deadline
     * @param int $RoleId
     * @param dateTime $FirstAcceptDeadline
     * @param FirstAcceptStatus $Status
     * @access public
     */
    public function __construct($AssignmentType, $Deadline, $RoleId, $FirstAcceptDeadline, $Status)
    {
      parent::__construct($AssignmentType, $Deadline, $RoleId);
      $this->FirstAcceptDeadline = $FirstAcceptDeadline;
      $this->Status = $Status;
    }

}
