<?php

include_once(BaseDir.'/CATTools/memoQ/common/HeavyResourceInfo.php');

class CorpusInfo extends HeavyResourceInfo
{

    /**
     * @var CorpusIndexingOptions $IndexingOptions
     * @access public
     */
    public $IndexingOptions = null;

    /**
     * @var string[] $LanguageCodes
     * @access public
     */
    public $LanguageCodes = null;

    /**
     * @param guid $Guid
     * @param boolean $Readonly
     * @access public
     */
    public function __construct($Guid, $Readonly)
    {
      parent::__construct($Guid, $Readonly);
    }

}
