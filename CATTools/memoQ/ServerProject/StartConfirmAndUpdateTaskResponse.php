<?php

class StartConfirmAndUpdateTaskResponse
{

    /**
     * @var TaskInfo $StartConfirmAndUpdateTaskResult
     * @access public
     */
    public $StartConfirmAndUpdateTaskResult = null;

    /**
     * @param TaskInfo $StartConfirmAndUpdateTaskResult
     * @access public
     */
    public function __construct($StartConfirmAndUpdateTaskResult)
    {
      $this->StartConfirmAndUpdateTaskResult = $StartConfirmAndUpdateTaskResult;
    }

}
