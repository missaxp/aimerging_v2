<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/TemplateBasedProjectCreationFault.php');

class TemplateBasedProjectCreationInvalidMetaFault extends TemplateBasedProjectCreationFault
{

    /**
     * @var string[] $AllowedValues
     * @access public
     */
    public $AllowedValues = null;

    /**
     * @access public
     */
    public function __construct()
    {
      parent::__construct();
    }

}
