<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/LightResourceInfo.php');

class LightResourceInfoWithLang extends LightResourceInfo
{

    /**
     * @var string $LanguageCode
     * @access public
     */
    public $LanguageCode = null;

    /**
     * @param guid $Guid
     * @param boolean $Readonly
     * @param boolean $IsDefault
     * @access public
     */
    public function __construct($Guid, $Readonly, $IsDefault)
    {
      parent::__construct($Guid, $Readonly, $IsDefault);
    }

}
