<?php

class StartDeleteProjectTaskResponse
{

    /**
     * @var TaskInfo $StartDeleteProjectTaskResult
     * @access public
     */
    public $StartDeleteProjectTaskResult = null;

    /**
     * @param TaskInfo $StartDeleteProjectTaskResult
     * @access public
     */
    public function __construct($StartDeleteProjectTaskResult)
    {
      $this->StartDeleteProjectTaskResult = $StartDeleteProjectTaskResult;
    }

}
