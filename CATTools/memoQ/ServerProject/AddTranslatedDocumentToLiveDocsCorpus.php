<?php

class AddTranslatedDocumentToLiveDocsCorpus
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var guid $documentGuid
     * @access public
     */
    public $documentGuid = null;

    /**
     * @var guid $corpusId
     * @access public
     */
    public $corpusId = null;

    /**
     * @param guid $serverProjectGuid
     * @param guid $documentGuid
     * @param guid $corpusId
     * @access public
     */
    public function __construct($serverProjectGuid, $documentGuid, $corpusId)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->documentGuid = $documentGuid;
      $this->corpusId = $corpusId;
    }

}
