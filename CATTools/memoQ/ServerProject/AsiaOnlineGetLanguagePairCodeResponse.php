<?php

class AsiaOnlineGetLanguagePairCodeResponse
{

    /**
     * @var AsiaOnlineGetLanguagePairCodeResultInfo $AsiaOnlineGetLanguagePairCodeResult
     * @access public
     */
    public $AsiaOnlineGetLanguagePairCodeResult = null;

    /**
     * @param AsiaOnlineGetLanguagePairCodeResultInfo $AsiaOnlineGetLanguagePairCodeResult
     * @access public
     */
    public function __construct($AsiaOnlineGetLanguagePairCodeResult)
    {
      $this->AsiaOnlineGetLanguagePairCodeResult = $AsiaOnlineGetLanguagePairCodeResult;
    }

}
