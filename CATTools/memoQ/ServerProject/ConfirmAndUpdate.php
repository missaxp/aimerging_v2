<?php

class ConfirmAndUpdate
{

    /**
     * @var string $sessionId
     * @access public
     */
    public $sessionId = null;

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var guid[] $translationDocGuids
     * @access public
     */
    public $translationDocGuids = null;

    /**
     * @var ConfirmAndUpdateOptions $options
     * @access public
     */
    public $options = null;

    /**
     * @param string $sessionId
     * @param guid $serverProjectGuid
     * @param guid[] $translationDocGuids
     * @param ConfirmAndUpdateOptions $options
     * @access public
     */
    public function __construct($sessionId, $serverProjectGuid, $translationDocGuids, $options)
    {
      $this->sessionId = $sessionId;
      $this->serverProjectGuid = $serverProjectGuid;
      $this->translationDocGuids = $translationDocGuids;
      $this->options = $options;
    }

}
