<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/ResultInfo.php');

class TranslationDocumentAssignmentResultInfo extends ResultInfo
{

    /**
     * @var string $ErrorCode
     * @access public
     */
    public $ErrorCode = null;

    /**
     * @var TranslationDocumentRoleAssignmentResultInfo[] $RoleAssignmentResults
     * @access public
     */
    public $RoleAssignmentResults = null;

    /**
     * @param ResultStatus $ResultStatus
     * @access public
     */
    public function __construct($ResultStatus)
    {
      parent::__construct($ResultStatus);
    }

}
