<?php

class TranslationDocumentAssigneeInfo
{

    /**
     * @var guid $AssigneeGuid
     * @access public
     */
    public $AssigneeGuid = null;

    /**
     * @var string $AssigneeName
     * @access public
     */
    public $AssigneeName = null;

    /**
     * @param guid $AssigneeGuid
     * @access public
     */
    public function __construct($AssigneeGuid)
    {
      $this->AssigneeGuid = $AssigneeGuid;
    }

}
