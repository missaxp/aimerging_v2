<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/LightResourceInfo.php');

class PathRuleResourceInfo extends LightResourceInfo
{

    /**
     * @var PathRuleType $RuleType
     * @access public
     */
    public $RuleType = null;

    /**
     * @param guid $Guid
     * @param boolean $Readonly
     * @param boolean $IsDefault
     * @param PathRuleType $RuleType
     * @access public
     */
    public function __construct($Guid, $Readonly, $IsDefault, $RuleType)
    {
      parent::__construct($Guid, $Readonly, $IsDefault);
      $this->RuleType = $RuleType;
    }

}
