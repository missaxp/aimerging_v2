<?php

class UpdateWithTableRtfOptions
{

    /**
     * @var boolean $DoNotUpdateIfSourceIsDifferent
     * @access public
     */
    public $DoNotUpdateIfSourceIsDifferent = null;

    /**
     * @var boolean $ImportEditsAsTrackedChanges
     * @access public
     */
    public $ImportEditsAsTrackedChanges = null;

    /**
     * @var boolean $TagsMustHaveMqInternalStyle
     * @access public
     */
    public $TagsMustHaveMqInternalStyle = null;

    /**
     * @param boolean $DoNotUpdateIfSourceIsDifferent
     * @param boolean $ImportEditsAsTrackedChanges
     * @param boolean $TagsMustHaveMqInternalStyle
     * @access public
     */
    public function __construct($DoNotUpdateIfSourceIsDifferent, $ImportEditsAsTrackedChanges, $TagsMustHaveMqInternalStyle)
    {
      $this->DoNotUpdateIfSourceIsDifferent = $DoNotUpdateIfSourceIsDifferent;
      $this->ImportEditsAsTrackedChanges = $ImportEditsAsTrackedChanges;
      $this->TagsMustHaveMqInternalStyle = $TagsMustHaveMqInternalStyle;
    }

}
