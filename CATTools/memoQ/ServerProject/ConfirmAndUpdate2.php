<?php

class ConfirmAndUpdate2
{

    /**
     * @var guid $userGuid
     * @access public
     */
    public $userGuid = null;

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var guid[] $translationDocGuids
     * @access public
     */
    public $translationDocGuids = null;

    /**
     * @var ConfirmAndUpdateOptions $options
     * @access public
     */
    public $options = null;

    /**
     * @param guid $userGuid
     * @param guid $serverProjectGuid
     * @param guid[] $translationDocGuids
     * @param ConfirmAndUpdateOptions $options
     * @access public
     */
    public function __construct($userGuid, $serverProjectGuid, $translationDocGuids, $options)
    {
      $this->userGuid = $userGuid;
      $this->serverProjectGuid = $serverProjectGuid;
      $this->translationDocGuids = $translationDocGuids;
      $this->options = $options;
    }

}
