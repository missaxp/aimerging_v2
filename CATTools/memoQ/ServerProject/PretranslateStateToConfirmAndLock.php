<?php

class PretranslateStateToConfirmAndLock
{
    const __default = 'None';
    const None = 'None';
    const ExactMatch = 'ExactMatch';
    const ExactMatchWithContext = 'ExactMatchWithContext';
    const IceSpiceMatch = 'IceSpiceMatch';


}
