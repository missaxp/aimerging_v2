<?php

class TaskInfo
{

    /**
     * @var int $ProgressPercentage
     * @access public
     */
    public $ProgressPercentage = null;

    /**
     * @var TaskStatus $Status
     * @access public
     */
    public $Status = null;

    /**
     * @var guid $TaskId
     * @access public
     */
    public $TaskId = null;

    /**
     * @param int $ProgressPercentage
     * @param TaskStatus $Status
     * @param guid $TaskId
     * @access public
     */
    public function __construct($ProgressPercentage, $Status, $TaskId)
    {
      $this->ProgressPercentage = $ProgressPercentage;
      $this->Status = $Status;
      $this->TaskId = $TaskId;
    }

}
