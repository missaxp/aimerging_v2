<?php

class GetPostTranslationAnalysisReportAsCSVResponse
{

    /**
     * @var PostTranslationAnalysisAsCSVResult $GetPostTranslationAnalysisReportAsCSVResult
     * @access public
     */
    public $GetPostTranslationAnalysisReportAsCSVResult = null;

    /**
     * @param PostTranslationAnalysisAsCSVResult $GetPostTranslationAnalysisReportAsCSVResult
     * @access public
     */
    public function __construct($GetPostTranslationAnalysisReportAsCSVResult)
    {
      $this->GetPostTranslationAnalysisReportAsCSVResult = $GetPostTranslationAnalysisReportAsCSVResult;
    }

}
