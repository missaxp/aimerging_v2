<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentHistoryItemInfo.php');

class SubvendorAssignHistoryItemInfo extends DocumentHistoryItemInfo
{

    /**
     * @var dateTime $Deadline
     * @access public
     */
    public $Deadline = null;

    /**
     * @var int $Role
     * @access public
     */
    public $Role = null;

    /**
     * @var guid $SubvendorGroupId
     * @access public
     */
    public $SubvendorGroupId = null;

    /**
     * @param guid $DocumentOrDivisionGuid
     * @param DocumentHistoryItemType $HistoryItemType
     * @param guid $ModifierSVGroupId
     * @param guid $ModifierUserGuid
     * @param dateTime $TimeStamp
     * @param dateTime $Deadline
     * @param int $Role
     * @param guid $SubvendorGroupId
     * @access public
     */
    public function __construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp, $Deadline, $Role, $SubvendorGroupId)
    {
      parent::__construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp);
      $this->Deadline = $Deadline;
      $this->Role = $Role;
      $this->SubvendorGroupId = $SubvendorGroupId;
    }

}
