<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentHistoryItemInfo.php');

class GroupSourcingDocumentDeliverHistoryItemInfo extends DocumentHistoryItemInfo
{

    /**
     * @var boolean $DocumentComplete
     * @access public
     */
    public $DocumentComplete = null;

    /**
     * @var int $Role
     * @access public
     */
    public $Role = null;

    /**
     * @param guid $DocumentOrDivisionGuid
     * @param DocumentHistoryItemType $HistoryItemType
     * @param guid $ModifierSVGroupId
     * @param guid $ModifierUserGuid
     * @param dateTime $TimeStamp
     * @param boolean $DocumentComplete
     * @param int $Role
     * @access public
     */
    public function __construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp, $DocumentComplete, $Role)
    {
      parent::__construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp);
      $this->DocumentComplete = $DocumentComplete;
      $this->Role = $Role;
    }

}
