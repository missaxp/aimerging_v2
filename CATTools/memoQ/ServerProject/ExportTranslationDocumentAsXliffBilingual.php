<?php

class ExportTranslationDocumentAsXliffBilingual
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var guid $docGuid
     * @access public
     */
    public $docGuid = null;

    /**
     * @var XliffBilingualExportOptions $options
     * @access public
     */
    public $options = null;

    /**
     * @param guid $serverProjectGuid
     * @param guid $docGuid
     * @param XliffBilingualExportOptions $options
     * @access public
     */
    public function __construct($serverProjectGuid, $docGuid, $options)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->docGuid = $docGuid;
      $this->options = $options;
    }

}
