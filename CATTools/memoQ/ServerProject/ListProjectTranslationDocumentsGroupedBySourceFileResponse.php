<?php

class ListProjectTranslationDocumentsGroupedBySourceFileResponse
{

    /**
     * @var ServerProjectTranslationDocumentsFromSameSourceFile[] $ListProjectTranslationDocumentsGroupedBySourceFileResult
     * @access public
     */
    public $ListProjectTranslationDocumentsGroupedBySourceFileResult = null;

    /**
     * @param ServerProjectTranslationDocumentsFromSameSourceFile[] $ListProjectTranslationDocumentsGroupedBySourceFileResult
     * @access public
     */
    public function __construct($ListProjectTranslationDocumentsGroupedBySourceFileResult)
    {
      $this->ListProjectTranslationDocumentsGroupedBySourceFileResult = $ListProjectTranslationDocumentsGroupedBySourceFileResult;
    }

}
