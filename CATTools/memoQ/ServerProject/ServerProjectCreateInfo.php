<?php

class ServerProjectCreateInfo
{

    /**
     * @var boolean $AllowOverlappingWorkflow
     * @access public
     */
    public $AllowOverlappingWorkflow = null;

    /**
     * @var boolean $AllowPackageCreation
     * @access public
     */
    public $AllowPackageCreation = null;

    /**
     * @var string $Client
     * @access public
     */
    public $Client = null;

    /**
     * @var guid $CreatorUser
     * @access public
     */
    public $CreatorUser = null;

    /**
     * @var string $CustomMetas
     * @access public
     */
    public $CustomMetas = null;

    /**
     * @var dateTime $Deadline
     * @access public
     */
    public $Deadline = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var string $Domain
     * @access public
     */
    public $Domain = null;

    /**
     * @var boolean $DownloadPreview2
     * @access public
     */
    public $DownloadPreview2 = null;

    /**
     * @var boolean $DownloadSkeleton2
     * @access public
     */
    public $DownloadSkeleton2 = null;

    /**
     * @var boolean $EnableCommunication
     * @access public
     */
    public $EnableCommunication = null;

    /**
     * @var string $Name
     * @access public
     */
    public $Name = null;

    /**
     * @var boolean $OmitHitsWithNoTargetTerm
     * @access public
     */
    public $OmitHitsWithNoTargetTerm = null;

    /**
     * @var PackageDeliveryOptions $PackageDeliveryOptions
     * @access public
     */
    public $PackageDeliveryOptions = null;

    /**
     * @var ServerProjectResourcesInPackages $PackageResourceHandling
     * @access public
     */
    public $PackageResourceHandling = null;

    /**
     * @var boolean $PreventDeliveryOnQAError
     * @access public
     */
    public $PreventDeliveryOnQAError = null;

    /**
     * @var string $Project
     * @access public
     */
    public $Project = null;

    /**
     * @var boolean $RecordVersionHistory
     * @access public
     */
    public $RecordVersionHistory = null;

    /**
     * @var string $SourceLanguageCode
     * @access public
     */
    public $SourceLanguageCode = null;

    /**
     * @var boolean $StrictSubLangMatching
     * @access public
     */
    public $StrictSubLangMatching = null;

    /**
     * @var string $Subject
     * @access public
     */
    public $Subject = null;

    /**
     * @var string[] $TargetLanguageCodes
     * @access public
     */
    public $TargetLanguageCodes = null;

    /**
     * @param boolean $AllowOverlappingWorkflow
     * @param boolean $AllowPackageCreation
     * @param guid $CreatorUser
     * @param dateTime $Deadline
     * @param boolean $DownloadPreview2
     * @param boolean $DownloadSkeleton2
     * @param boolean $EnableCommunication
     * @param boolean $OmitHitsWithNoTargetTerm
     * @param ServerProjectResourcesInPackages $PackageResourceHandling
     * @param boolean $PreventDeliveryOnQAError
     * @param boolean $RecordVersionHistory
     * @param boolean $StrictSubLangMatching
     * @access public
     */
    public function __construct($AllowOverlappingWorkflow, $AllowPackageCreation, $CreatorUser, $Deadline, $DownloadPreview2, $DownloadSkeleton2, $EnableCommunication, $OmitHitsWithNoTargetTerm, $PackageResourceHandling, $PreventDeliveryOnQAError, $RecordVersionHistory, $StrictSubLangMatching, $Name, $SourceLanguage, $TargetLanguages)
    {
      $this->AllowOverlappingWorkflow = $AllowOverlappingWorkflow;
      $this->AllowPackageCreation = $AllowPackageCreation;
      $this->CreatorUser = $CreatorUser;
      $this->Deadline = $Deadline;
      $this->DownloadPreview2 = $DownloadPreview2;
      $this->DownloadSkeleton2 = $DownloadSkeleton2;
      $this->EnableCommunication = $EnableCommunication;
      $this->OmitHitsWithNoTargetTerm = $OmitHitsWithNoTargetTerm;
      $this->PackageResourceHandling = $PackageResourceHandling;
      $this->PreventDeliveryOnQAError = $PreventDeliveryOnQAError;
      $this->RecordVersionHistory = $RecordVersionHistory;
      $this->StrictSubLangMatching = $StrictSubLangMatching;
      $this->Name = $Name;
      $this->SourceLanguageCode = $SourceLanguage;
      $this->TargetLanguageCodes = $TargetLanguages;
    }

}
