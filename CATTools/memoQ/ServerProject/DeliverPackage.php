<?php

class DeliverPackage
{

    /**
     * @var guid $fileGuid
     * @access public
     */
    public $fileGuid = null;

    /**
     * @param guid $fileGuid
     * @access public
     */
    public function __construct($fileGuid)
    {
      $this->fileGuid = $fileGuid;
    }

}
