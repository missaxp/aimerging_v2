<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/ResultInfo.php');

class AnalysisResultInfo extends ResultInfo
{

    /**
     * @var AnalysisResultForLang[] $ResultsForTargetLangs
     * @access public
     */
    public $ResultsForTargetLangs = null;

    /**
     * @param ResultStatus $ResultStatus
     * @access public
     */
    public function __construct($ResultStatus)
    {
      parent::__construct($ResultStatus);
    }

}
