<?php

class ServerProjectUserInfoHeader
{

    /**
     * @var boolean $PermForLicense
     * @access public
     */
    public $PermForLicense = null;

    /**
     * @var ServerProjectRoles $ProjectRoles
     * @access public
     */
    public $ProjectRoles = null;

    /**
     * @var UserInfo $User
     * @access public
     */
    public $User = null;

    /**
     * @param boolean $PermForLicense
     * @access public
     */
    public function __construct($PermForLicense)
    {
      $this->PermForLicense = $PermForLicense;
    }

}
