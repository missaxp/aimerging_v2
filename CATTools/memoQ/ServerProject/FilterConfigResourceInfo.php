<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/LightResourceInfo.php');

class FilterConfigResourceInfo extends LightResourceInfo
{

    /**
     * @var string $FilterName
     * @access public
     */
    public $FilterName = null;

    /**
     * @param guid $Guid
     * @param boolean $Readonly
     * @param boolean $IsDefault
     * @access public
     */
    public function __construct($Guid, $Readonly, $IsDefault)
    {
      parent::__construct($Guid, $Readonly, $IsDefault);
    }

}
