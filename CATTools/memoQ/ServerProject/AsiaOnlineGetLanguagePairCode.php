<?php

class AsiaOnlineGetLanguagePairCode
{

    /**
     * @var string $memoQSourceLangCode
     * @access public
     */
    public $memoQSourceLangCode = null;

    /**
     * @var string $memoQTargetLangCode
     * @access public
     */
    public $memoQTargetLangCode = null;

    /**
     * @param string $memoQSourceLangCode
     * @param string $memoQTargetLangCode
     * @access public
     */
    public function __construct($memoQSourceLangCode, $memoQTargetLangCode)
    {
      $this->memoQSourceLangCode = $memoQSourceLangCode;
      $this->memoQTargetLangCode = $memoQTargetLangCode;
    }

}
