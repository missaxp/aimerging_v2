<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentRoleAssignmentInfo.php');

class TranslationDocumentFirstAcceptAssignmentInfo extends TranslationDocumentRoleAssignmentInfo
{

    /**
     * @var dateTime $FirstAcceptDeadline
     * @access public
     */
    public $FirstAcceptDeadline = null;

    /**
     * @var guid[] $UserGuids
     * @access public
     */
    public $UserGuids = null;

    /**
     * @param TranslationDocumentAssignmentType $AssignmentType
     * @param dateTime $Deadline
     * @param int $RoleId
     * @param dateTime $FirstAcceptDeadline
     * @access public
     */
    public function __construct($AssignmentType, $Deadline, $RoleId, $FirstAcceptDeadline)
    {
      parent::__construct($AssignmentType, $Deadline, $RoleId);
      $this->FirstAcceptDeadline = $FirstAcceptDeadline;
    }

}
