<?php

class StartXTranslateTask
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var XTranslateOptions $options
     * @access public
     */
    public $options = null;

    /**
     * @param guid $serverProjectGuid
     * @param XTranslateOptions $options
     * @access public
     */
    public function __construct($serverProjectGuid, $options)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->options = $options;
    }

}
