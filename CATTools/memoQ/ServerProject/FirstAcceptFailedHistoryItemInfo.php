<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentHistoryItemInfo.php');

class FirstAcceptFailedHistoryItemInfo extends DocumentHistoryItemInfo
{

    /**
     * @var dateTime $Deadline
     * @access public
     */
    public $Deadline = null;

    /**
     * @var int $Role
     * @access public
     */
    public $Role = null;

    /**
     * @param guid $DocumentOrDivisionGuid
     * @param DocumentHistoryItemType $HistoryItemType
     * @param guid $ModifierSVGroupId
     * @param guid $ModifierUserGuid
     * @param dateTime $TimeStamp
     * @param dateTime $Deadline
     * @param int $Role
     * @access public
     */
    public function __construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp, $Deadline, $Role)
    {
      parent::__construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp);
      $this->Deadline = $Deadline;
      $this->Role = $Role;
    }

}
