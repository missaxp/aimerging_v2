<?php

class CreateDeliveryPackageResponse
{

    /**
     * @var CreateDeliveryResult $CreateDeliveryPackageResult
     * @access public
     */
    public $CreateDeliveryPackageResult = null;

    /**
     * @param CreateDeliveryResult $CreateDeliveryPackageResult
     * @access public
     */
    public function __construct($CreateDeliveryPackageResult)
    {
      $this->CreateDeliveryPackageResult = $CreateDeliveryPackageResult;
    }

}
