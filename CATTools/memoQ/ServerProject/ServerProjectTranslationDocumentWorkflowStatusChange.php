<?php

class ServerProjectTranslationDocumentWorkflowStatusChange
{

    /**
     * @var guid $DocumentGuid
     * @access public
     */
    public $DocumentGuid = null;

    /**
     * @var WorkflowStatus $WorkflowStatus
     * @access public
     */
    public $WorkflowStatus = null;

    /**
     * @param guid $DocumentGuid
     * @param WorkflowStatus $WorkflowStatus
     * @access public
     */
    public function __construct($DocumentGuid, $WorkflowStatus)
    {
      $this->DocumentGuid = $DocumentGuid;
      $this->WorkflowStatus = $WorkflowStatus;
    }

}
