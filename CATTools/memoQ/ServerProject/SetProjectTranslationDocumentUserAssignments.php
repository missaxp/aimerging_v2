<?php

class SetProjectTranslationDocumentUserAssignments
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var ServerProjectTranslationDocumentUserAssignments[] $assignments
     * @access public
     */
    public $assignments = null;

    /**
     * @param guid $serverProjectGuid
     * @param ServerProjectTranslationDocumentUserAssignments[] $assignments
     * @access public
     */
    public function __construct($serverProjectGuid, $assignments)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->assignments = $assignments;
    }

}
