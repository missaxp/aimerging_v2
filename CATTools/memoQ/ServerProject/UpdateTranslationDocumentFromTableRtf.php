<?php

class UpdateTranslationDocumentFromTableRtf
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var guid $fileGuid
     * @access public
     */
    public $fileGuid = null;

    /**
     * @var UpdateWithTableRtfOptions $opt
     * @access public
     */
    public $opt = null;

    /**
     * @param guid $serverProjectGuid
     * @param guid $fileGuid
     * @param UpdateWithTableRtfOptions $opt
     * @access public
     */
    public function __construct($serverProjectGuid, $fileGuid, $opt)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->fileGuid = $fileGuid;
      $this->opt = $opt;
    }

}
