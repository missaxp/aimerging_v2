<?php

class ListProjectTranslationDocuments2
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var ListServerProjectTranslationDocument2Options $options
     * @access public
     */
    public $options = null;

    /**
     * @param guid $serverProjectGuid
     * @param ListServerProjectTranslationDocument2Options $options
     * @access public
     */
    public function __construct($serverProjectGuid, $options)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->options = $options;
    }

}
