<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentAssigneeInfo.php');

class TranslationDocumentGroupSourcingUserInfo extends TranslationDocumentAssigneeInfo
{

    /**
     * @var dateTime $DocumentTaken
     * @access public
     */
    public $DocumentTaken = null;

    /**
     * @param guid $AssigneeGuid
     * @access public
     */
    public function __construct($AssigneeGuid)
    {
      parent::__construct($AssigneeGuid);
    }

}
