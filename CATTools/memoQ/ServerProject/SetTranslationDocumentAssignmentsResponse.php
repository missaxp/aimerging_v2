<?php

class SetTranslationDocumentAssignmentsResponse
{

    /**
     * @var TranslationDocumentAssignmentResultInfo[] $SetTranslationDocumentAssignmentsResult
     * @access public
     */
    public $SetTranslationDocumentAssignmentsResult = null;

    /**
     * @param TranslationDocumentAssignmentResultInfo[] $SetTranslationDocumentAssignmentsResult
     * @access public
     */
    public function __construct($SetTranslationDocumentAssignmentsResult)
    {
      $this->SetTranslationDocumentAssignmentsResult = $SetTranslationDocumentAssignmentsResult;
    }

}
