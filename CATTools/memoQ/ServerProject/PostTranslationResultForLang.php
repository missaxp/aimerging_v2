<?php

class PostTranslationResultForLang
{

    /**
     * @var PostTransAnalysisReportForDocument[] $ByDocument
     * @access public
     */
    public $ByDocument = null;

    /**
     * @var PostTransAnalysisReportForUser[] $ByUser
     * @access public
     */
    public $ByUser = null;

    /**
     * @var PostTransAnalysisReportItem $Summary
     * @access public
     */
    public $Summary = null;

    /**
     * @var string $TargetLangCode
     * @access public
     */
    public $TargetLangCode = null;

    /**
     * @access public
     */
    public function __construct()
    {
    
    }

}
