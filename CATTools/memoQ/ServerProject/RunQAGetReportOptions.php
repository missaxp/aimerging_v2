<?php

class RunQAGetReportOptions
{

    /**
     * @var guid[] $DocumentGuids
     * @access public
     */
    public $DocumentGuids = null;

    /**
     * @var boolean $IncludeLockedRows
     * @access public
     */
    public $IncludeLockedRows = null;

    /**
     * @var string $ReportDisplayLanguage
     * @access public
     */
    public $ReportDisplayLanguage = null;

    /**
     * @var QAReportTypes $ReportType
     * @access public
     */
    public $ReportType = null;

    /**
     * @param boolean $IncludeLockedRows
     * @param QAReportTypes $ReportType
     * @access public
     */
    public function __construct($IncludeLockedRows, $ReportType)
    {
      $this->IncludeLockedRows = $IncludeLockedRows;
      $this->ReportType = $ReportType;
    }

}
