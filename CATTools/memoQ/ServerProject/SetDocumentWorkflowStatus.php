<?php

class SetDocumentWorkflowStatus
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var ServerProjectTranslationDocumentWorkflowStatusChange[] $workflowChanges
     * @access public
     */
    public $workflowChanges = null;

    /**
     * @param guid $serverProjectGuid
     * @param ServerProjectTranslationDocumentWorkflowStatusChange[] $workflowChanges
     * @access public
     */
    public function __construct($serverProjectGuid, $workflowChanges)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->workflowChanges = $workflowChanges;
    }

}
