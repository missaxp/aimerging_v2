<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentDetailedRoleAssignmentInfo.php');

class TranslationDocumentDetailedGroupSourcingAssignmentInfo extends TranslationDocumentDetailedRoleAssignmentInfo
{

    /**
     * @var TranslationDocumentGroupSourcingUserInfo[] $Users
     * @access public
     */
    public $Users = null;

    /**
     * @param TranslationDocumentAssignmentType $AssignmentType
     * @param dateTime $Deadline
     * @param int $RoleId
     * @access public
     */
    public function __construct($AssignmentType, $Deadline, $RoleId)
    {
      parent::__construct($AssignmentType, $Deadline, $RoleId);
    }

}
