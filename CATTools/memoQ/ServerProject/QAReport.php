<?php

class QAReport
{

    /**
     * @var string $DetailedReport
     * @access public
     */
    public $DetailedReport = null;

    /**
     * @var QAReportForDocument[] $ReportsPerDocument
     * @access public
     */
    public $ReportsPerDocument = null;

    /**
     * @access public
     */
    public function __construct()
    {
    
    }

}
