<?php

class GetAnalysisReportAsCSVResponse
{

    /**
     * @var AnalysisAsCSVResult $GetAnalysisReportAsCSVResult
     * @access public
     */
    public $GetAnalysisReportAsCSVResult = null;

    /**
     * @param AnalysisAsCSVResult $GetAnalysisReportAsCSVResult
     * @access public
     */
    public function __construct($GetAnalysisReportAsCSVResult)
    {
      $this->GetAnalysisReportAsCSVResult = $GetAnalysisReportAsCSVResult;
    }

}
