<?php

class ServerProjectTranslationDocSliceInfo
{

    /**
     * @var int $ConfirmedCharacterCount
     * @access public
     */
    public $ConfirmedCharacterCount = null;

    /**
     * @var int $ConfirmedSegmentCount
     * @access public
     */
    public $ConfirmedSegmentCount = null;

    /**
     * @var int $ConfirmedWordCount
     * @access public
     */
    public $ConfirmedWordCount = null;

    /**
     * @var DocumentStatus $DocumentStatus
     * @access public
     */
    public $DocumentStatus = null;

    /**
     * @var int $FirstRowIndex
     * @access public
     */
    public $FirstRowIndex = null;

    /**
     * @var int $LastRowIndex
     * @access public
     */
    public $LastRowIndex = null;

    /**
     * @var int $LockedCharacterCount
     * @access public
     */
    public $LockedCharacterCount = null;

    /**
     * @var int $LockedSegmentCount
     * @access public
     */
    public $LockedSegmentCount = null;

    /**
     * @var int $LockedWordCount
     * @access public
     */
    public $LockedWordCount = null;

    /**
     * @var int $ProofreadCharacterCount
     * @access public
     */
    public $ProofreadCharacterCount = null;

    /**
     * @var int $ProofreadSegmentCount
     * @access public
     */
    public $ProofreadSegmentCount = null;

    /**
     * @var int $ProofreadWordCount
     * @access public
     */
    public $ProofreadWordCount = null;

    /**
     * @var int $Reviewer1ConfirmedCharacterCount
     * @access public
     */
    public $Reviewer1ConfirmedCharacterCount = null;

    /**
     * @var int $Reviewer1ConfirmedSegmentCount
     * @access public
     */
    public $Reviewer1ConfirmedSegmentCount = null;

    /**
     * @var int $Reviewer1ConfirmedWordCount
     * @access public
     */
    public $Reviewer1ConfirmedWordCount = null;

    /**
     * @var guid $SliceGuid
     * @access public
     */
    public $SliceGuid = null;

    /**
     * @var int $TotalCharacterCount
     * @access public
     */
    public $TotalCharacterCount = null;

    /**
     * @var int $TotalSegmentCount
     * @access public
     */
    public $TotalSegmentCount = null;

    /**
     * @var int $TotalWordCount
     * @access public
     */
    public $TotalWordCount = null;

    /**
     * @var TranslationDocumentDetailedAssignmentInfo[] $UserAssignments
     * @access public
     */
    public $UserAssignments = null;

    /**
     * @var WorkflowStatus $WorkflowStatus
     * @access public
     */
    public $WorkflowStatus = null;

    /**
     * @param int $ConfirmedCharacterCount
     * @param int $ConfirmedSegmentCount
     * @param int $ConfirmedWordCount
     * @param DocumentStatus $DocumentStatus
     * @param int $FirstRowIndex
     * @param int $LastRowIndex
     * @param int $LockedCharacterCount
     * @param int $LockedSegmentCount
     * @param int $LockedWordCount
     * @param int $ProofreadCharacterCount
     * @param int $ProofreadSegmentCount
     * @param int $ProofreadWordCount
     * @param int $Reviewer1ConfirmedCharacterCount
     * @param int $Reviewer1ConfirmedSegmentCount
     * @param int $Reviewer1ConfirmedWordCount
     * @param guid $SliceGuid
     * @param int $TotalCharacterCount
     * @param int $TotalSegmentCount
     * @param int $TotalWordCount
     * @param WorkflowStatus $WorkflowStatus
     * @access public
     */
    public function __construct($ConfirmedCharacterCount, $ConfirmedSegmentCount, $ConfirmedWordCount, $DocumentStatus, $FirstRowIndex, $LastRowIndex, $LockedCharacterCount, $LockedSegmentCount, $LockedWordCount, $ProofreadCharacterCount, $ProofreadSegmentCount, $ProofreadWordCount, $Reviewer1ConfirmedCharacterCount, $Reviewer1ConfirmedSegmentCount, $Reviewer1ConfirmedWordCount, $SliceGuid, $TotalCharacterCount, $TotalSegmentCount, $TotalWordCount, $WorkflowStatus)
    {
      $this->ConfirmedCharacterCount = $ConfirmedCharacterCount;
      $this->ConfirmedSegmentCount = $ConfirmedSegmentCount;
      $this->ConfirmedWordCount = $ConfirmedWordCount;
      $this->DocumentStatus = $DocumentStatus;
      $this->FirstRowIndex = $FirstRowIndex;
      $this->LastRowIndex = $LastRowIndex;
      $this->LockedCharacterCount = $LockedCharacterCount;
      $this->LockedSegmentCount = $LockedSegmentCount;
      $this->LockedWordCount = $LockedWordCount;
      $this->ProofreadCharacterCount = $ProofreadCharacterCount;
      $this->ProofreadSegmentCount = $ProofreadSegmentCount;
      $this->ProofreadWordCount = $ProofreadWordCount;
      $this->Reviewer1ConfirmedCharacterCount = $Reviewer1ConfirmedCharacterCount;
      $this->Reviewer1ConfirmedSegmentCount = $Reviewer1ConfirmedSegmentCount;
      $this->Reviewer1ConfirmedWordCount = $Reviewer1ConfirmedWordCount;
      $this->SliceGuid = $SliceGuid;
      $this->TotalCharacterCount = $TotalCharacterCount;
      $this->TotalSegmentCount = $TotalSegmentCount;
      $this->TotalWordCount = $TotalWordCount;
      $this->WorkflowStatus = $WorkflowStatus;
    }

}
