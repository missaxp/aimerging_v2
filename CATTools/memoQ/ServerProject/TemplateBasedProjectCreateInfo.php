<?php

class TemplateBasedProjectCreateInfo
{

    /**
     * @var string $Client
     * @access public
     */
    public $Client = null;

    /**
     * @var guid $CreatorUser
     * @access public
     */
    public $CreatorUser = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var string $Domain
     * @access public
     */
    public $Domain = null;

    /**
     * @var string $Name
     * @access public
     */
    public $Name = null;

    /**
     * @var string $Project
     * @access public
     */
    public $Project = null;

    /**
     * @var string[] $ProjectCreationAspects
     * @access public
     */
    public $ProjectCreationAspects = null;

    /**
     * @var string $SourceLanguageCode
     * @access public
     */
    public $SourceLanguageCode = null;

    /**
     * @var string $Subject
     * @access public
     */
    public $Subject = null;

    /**
     * @var string[] $TargetLanguageCodes
     * @access public
     */
    public $TargetLanguageCodes = null;

    /**
     * @var guid $TemplateGuid
     * @access public
     */
    public $TemplateGuid = null;

    /**
     * @param guid $CreatorUser
     * @param guid $TemplateGuid
     * @access public
     */
    public function __construct($CreatorUser, $TemplateGuid)
    {
      $this->CreatorUser = $CreatorUser;
      $this->TemplateGuid = $TemplateGuid;
    }

}
