<?php

class AutomatedActionStartedHistoryItemInfoAutomatedActionTypes
{
    const __default = 'CreateAnalysisReport';
    const CreateAnalysisReport = 'CreateAnalysisReport';
    const PreTranslate = 'PreTranslate';
    const LockSegments = 'LockSegments';
    const CreateSnapshot = 'CreateSnapshot';
    const CopySourceToTargetWhereEmpty = 'CopySourceToTargetWhereEmpty';
    const RunPostTranslationAnalysis = 'RunPostTranslationAnalysis';
    const XTranslate = 'XTranslate';
    const ConfirmAndUpdate = 'ConfirmAndUpdate';
    const DeleteWorkingTMs = 'DeleteWorkingTMs';
    const CreateProgressReport = 'CreateProgressReport';
    const AutoAssignUsersToDocuments = 'AutoAssignUsersToDocuments';
    const LaunchProject = 'LaunchProject';
    const ReturnToSource = 'ReturnToSource';
    const PutUpForFirstAccept = 'PutUpForFirstAccept';
    const CalculateEditDistance = 'CalculateEditDistance';
    const CalculateReviewerEditDistance = 'CalculateReviewerEditDistance';
    const SendToAO = 'SendToAO';
    const ChangeWorkflowStatus = 'ChangeWorkflowStatus';
    const ExecuteCustomCode = 'ExecuteCustomCode';
    const SourceDocumentExport = 'SourceDocumentExport';
    const TargetDocumentExport = 'TargetDocumentExport';
    const MQXLIFFExport = 'MQXLIFFExport';
    const TwoColumnRTFExport = 'TwoColumnRTFExport';
    const TMExport = 'TMExport';
    const TBExport = 'TBExport';
    const LiveDocsExport = 'LiveDocsExport';
    const LTExport = 'LTExport';
    const QAReportExport = 'QAReportExport';
    const LQAStatisticsExport = 'LQAStatisticsExport';
    const LQAErrorListExport = 'LQAErrorListExport';
    const ExportChangeTrackedReport = 'ExportChangeTrackedReport';
    const CreateReviewerChangeReport = 'CreateReviewerChangeReport';
    const ExportDeliveredCustomerPortalFiles = 'ExportDeliveredCustomerPortalFiles';
    const DeliverToCustomerPortal = 'DeliverToCustomerPortal';


}
