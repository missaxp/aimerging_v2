<?php

class AnalysisReportCounts
{

    /**
     * @var int $SegmentCount
     * @access public
     */
    public $SegmentCount = null;

    /**
     * @var int $SourceAsianCharCount
     * @access public
     */
    public $SourceAsianCharCount = null;

    /**
     * @var int $SourceCharCount
     * @access public
     */
    public $SourceCharCount = null;

    /**
     * @var int $SourceNonAsianWordCount
     * @access public
     */
    public $SourceNonAsianWordCount = null;

    /**
     * @var int $SourceTagCount
     * @access public
     */
    public $SourceTagCount = null;

    /**
     * @var int $SourceWordCount
     * @access public
     */
    public $SourceWordCount = null;

    /**
     * @param int $SegmentCount
     * @param int $SourceAsianCharCount
     * @param int $SourceCharCount
     * @param int $SourceNonAsianWordCount
     * @param int $SourceTagCount
     * @param int $SourceWordCount
     * @access public
     */
    public function __construct($SegmentCount, $SourceAsianCharCount, $SourceCharCount, $SourceNonAsianWordCount, $SourceTagCount, $SourceWordCount)
    {
      $this->SegmentCount = $SegmentCount;
      $this->SourceAsianCharCount = $SourceAsianCharCount;
      $this->SourceCharCount = $SourceCharCount;
      $this->SourceNonAsianWordCount = $SourceNonAsianWordCount;
      $this->SourceTagCount = $SourceTagCount;
      $this->SourceWordCount = $SourceWordCount;
    }

}
