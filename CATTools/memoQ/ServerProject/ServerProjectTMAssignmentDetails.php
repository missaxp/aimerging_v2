<?php

class ServerProjectTMAssignmentDetails
{

    /**
     * @var guid $MasterTMGuid
     * @access public
     */
    public $MasterTMGuid = null;

    /**
     * @var guid $PrimaryTMGuid
     * @access public
     */
    public $PrimaryTMGuid = null;

    /**
     * @var TMInfo[] $TMs
     * @access public
     */
    public $TMs = null;

    /**
     * @var string $TargetLangCode
     * @access public
     */
    public $TargetLangCode = null;

    /**
     * @param guid $MasterTMGuid
     * @param guid $PrimaryTMGuid
     * @access public
     */
    public function __construct($MasterTMGuid, $PrimaryTMGuid)
    {
      $this->MasterTMGuid = $MasterTMGuid;
      $this->PrimaryTMGuid = $PrimaryTMGuid;
    }

}
