<?php

class ServerProjectNotificationSettings
{

    /**
     * @var boolean $AllChaotsourcingUserDeliver
     * @access public
     */
    public $AllChaotsourcingUserDeliver = null;

    /**
     * @var boolean $AttachDistributionFile
     * @access public
     */
    public $AttachDistributionFile = null;

    /**
     * @var boolean $AutomatedActionFailed
     * @access public
     */
    public $AutomatedActionFailed = null;

    /**
     * @var boolean $BiddingCancelled
     * @access public
     */
    public $BiddingCancelled = null;

    /**
     * @var boolean $BiddingDistribution
     * @access public
     */
    public $BiddingDistribution = null;

    /**
     * @var boolean $BiddingExpired
     * @access public
     */
    public $BiddingExpired = null;

    /**
     * @var boolean $BiddingExpiredPM
     * @access public
     */
    public $BiddingExpiredPM = null;

    /**
     * @var boolean $BiddingFailed
     * @access public
     */
    public $BiddingFailed = null;

    /**
     * @var boolean $BiddingLost
     * @access public
     */
    public $BiddingLost = null;

    /**
     * @var boolean $BiddingUserAssignedTo
     * @access public
     */
    public $BiddingUserAssignedTo = null;

    /**
     * @var boolean $BiddingUserTakenFrom
     * @access public
     */
    public $BiddingUserTakenFrom = null;

    /**
     * @var boolean $BiddingWon
     * @access public
     */
    public $BiddingWon = null;

    /**
     * @var boolean $CPConnectionDeleted
     * @access public
     */
    public $CPConnectionDeleted = null;

    /**
     * @var boolean $CPContentModified
     * @access public
     */
    public $CPContentModified = null;

    /**
     * @var boolean $CPContentSentBack
     * @access public
     */
    public $CPContentSentBack = null;

    /**
     * @var boolean $CPDocumentsReimported
     * @access public
     */
    public $CPDocumentsReimported = null;

    /**
     * @var boolean $CPFailedAutoAssign
     * @access public
     */
    public $CPFailedAutoAssign = null;

    /**
     * @var boolean $CPPollingFailed
     * @access public
     */
    public $CPPollingFailed = null;

    /**
     * @var boolean $CPProgressReport
     * @access public
     */
    public $CPProgressReport = null;

    /**
     * @var boolean $CPSkippedUncompletedDocsFromReimport
     * @access public
     */
    public $CPSkippedUncompletedDocsFromReimport = null;

    /**
     * @var boolean $ChaotsourcingDistribution
     * @access public
     */
    public $ChaotsourcingDistribution = null;

    /**
     * @var boolean $ChaotsourcingUserAssignedTo
     * @access public
     */
    public $ChaotsourcingUserAssignedTo = null;

    /**
     * @var boolean $ChaotsourcingUserDeliver
     * @access public
     */
    public $ChaotsourcingUserDeliver = null;

    /**
     * @var boolean $ChaotsourcingUserTakenFrom
     * @access public
     */
    public $ChaotsourcingUserTakenFrom = null;

    /**
     * @var boolean $ChaotsourcingWorkReturn
     * @access public
     */
    public $ChaotsourcingWorkReturn = null;

    /**
     * @var boolean $ChaotsourcingWorkflowStatusChanged
     * @access public
     */
    public $ChaotsourcingWorkflowStatusChanged = null;

    /**
     * @var boolean $DeadlineChanged
     * @access public
     */
    public $DeadlineChanged = null;

    /**
     * @var boolean $DiscussionTopicChanged
     * @access public
     */
    public $DiscussionTopicChanged = null;

    /**
     * @var boolean $Distribution
     * @access public
     */
    public $Distribution = null;

    /**
     * @var boolean $DistributionInPackageProject
     * @access public
     */
    public $DistributionInPackageProject = null;

    /**
     * @var boolean $KeepPMsOnCC
     * @access public
     */
    public $KeepPMsOnCC = null;

    /**
     * @var boolean $MissedDeadline
     * @access public
     */
    public $MissedDeadline = null;

    /**
     * @var boolean $PMDocumentStatusChange
     * @access public
     */
    public $PMDocumentStatusChange = null;

    /**
     * @var boolean $PMProjectStatusChange
     * @access public
     */
    public $PMProjectStatusChange = null;

    /**
     * @var boolean $PackageContentChanged
     * @access public
     */
    public $PackageContentChanged = null;

    /**
     * @var boolean $SVDeadlineChanged
     * @access public
     */
    public $SVDeadlineChanged = null;

    /**
     * @var boolean $SVGroupAssignedTo
     * @access public
     */
    public $SVGroupAssignedTo = null;

    /**
     * @var boolean $SVGroupTakenFrom
     * @access public
     */
    public $SVGroupTakenFrom = null;

    /**
     * @var boolean $SubVendorGroupDistribution
     * @access public
     */
    public $SubVendorGroupDistribution = null;

    /**
     * @var boolean $UpcomingDeadline
     * @access public
     */
    public $UpcomingDeadline = null;

    /**
     * @var boolean $UserAssignedTo
     * @access public
     */
    public $UserAssignedTo = null;

    /**
     * @var boolean $UserAssignedToInPackageProject
     * @access public
     */
    public $UserAssignedToInPackageProject = null;

    /**
     * @var boolean $UserTakenFrom
     * @access public
     */
    public $UserTakenFrom = null;

    /**
     * @var boolean $WorkForward
     * @access public
     */
    public $WorkForward = null;

    /**
     * @var boolean $WorkReturn
     * @access public
     */
    public $WorkReturn = null;

    /**
     * @var boolean $WorkflowStatusChanged
     * @access public
     */
    public $WorkflowStatusChanged = null;

    /**
     * @access public
     */
    public function __construct()
    {
    
    }

}
