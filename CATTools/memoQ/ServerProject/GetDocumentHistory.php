<?php

class GetDocumentHistory
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var DocumentHistoryRequest $request
     * @access public
     */
    public $request = null;

    /**
     * @param guid $serverProjectGuid
     * @param DocumentHistoryRequest $request
     * @access public
     */
    public function __construct($serverProjectGuid, $request)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->request = $request;
    }

}
