<?php

class RtfBilingualExportOptions
{

    /**
     * @var boolean $SegmentedContextEmptyTranslation
     * @access public
     */
    public $SegmentedContextEmptyTranslation = null;

    /**
     * @var boolean $SuppressContext
     * @access public
     */
    public $SuppressContext = null;

    /**
     * @param boolean $SegmentedContextEmptyTranslation
     * @param boolean $SuppressContext
     * @access public
     */
    public function __construct($SegmentedContextEmptyTranslation, $SuppressContext)
    {
      $this->SegmentedContextEmptyTranslation = $SegmentedContextEmptyTranslation;
      $this->SuppressContext = $SuppressContext;
    }

}
