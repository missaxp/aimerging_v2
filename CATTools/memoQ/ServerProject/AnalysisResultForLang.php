<?php

class AnalysisResultForLang
{

    /**
     * @var AnalysisReportForDocument[] $ByDocument
     * @access public
     */
    public $ByDocument = null;

    /**
     * @var AnalysisReportItem $Summary
     * @access public
     */
    public $Summary = null;

    /**
     * @var string $TargetLangCode
     * @access public
     */
    public $TargetLangCode = null;

    /**
     * @access public
     */
    public function __construct()
    {
    
    }

}
