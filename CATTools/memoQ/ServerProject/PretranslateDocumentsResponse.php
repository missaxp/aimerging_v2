<?php

class PretranslateDocumentsResponse
{

    /**
     * @var ResultInfo $PretranslateDocumentsResult
     * @access public
     */
    public $PretranslateDocumentsResult = null;

    /**
     * @param ResultInfo $PretranslateDocumentsResult
     * @access public
     */
    public function __construct($PretranslateDocumentsResult)
    {
      $this->PretranslateDocumentsResult = $PretranslateDocumentsResult;
    }

}
