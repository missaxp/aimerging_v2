<?php

class ListProjectTBs3Response
{

    /**
     * @var ServerProjectTBsForTargetLang[] $ListProjectTBs3Result
     * @access public
     */
    public $ListProjectTBs3Result = null;

    /**
     * @param ServerProjectTBsForTargetLang[] $ListProjectTBs3Result
     * @access public
     */
    public function __construct($ListProjectTBs3Result)
    {
      $this->ListProjectTBs3Result = $ListProjectTBs3Result;
    }

}
