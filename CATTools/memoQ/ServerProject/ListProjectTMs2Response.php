<?php

class ListProjectTMs2Response
{

    /**
     * @var ServerProjectTMAssignmentDetails[] $ListProjectTMs2Result
     * @access public
     */
    public $ListProjectTMs2Result = null;

    /**
     * @param ServerProjectTMAssignmentDetails[] $ListProjectTMs2Result
     * @access public
     */
    public function __construct($ListProjectTMs2Result)
    {
      $this->ListProjectTMs2Result = $ListProjectTMs2Result;
    }

}
