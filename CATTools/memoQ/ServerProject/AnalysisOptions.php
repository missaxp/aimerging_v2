<?php

class AnalysisOptions
{

    /**
     * @var guid[] $DocumentGuids
     * @access public
     */
    public $DocumentGuids = null;

    /**
     * @var string[] $LanguageCodes
     * @access public
     */
    public $LanguageCodes = null;

    /**
     * @var string $Note
     * @access public
     */
    public $Note = null;

    /**
     * @var boolean $RepetitionPreferenceOver100
     * @access public
     */
    public $RepetitionPreferenceOver100 = null;

    /**
     * @var boolean $StoreReportInProject
     * @access public
     */
    public $StoreReportInProject = null;

    /**
     * @var float $TagWeightChar
     * @access public
     */
    public $TagWeightChar = null;

    /**
     * @var float $TagWeightWord
     * @access public
     */
    public $TagWeightWord = null;

    /**
     * @param boolean $RepetitionPreferenceOver100
     * @param boolean $StoreReportInProject
     * @param float $TagWeightChar
     * @param float $TagWeightWord
     * @access public
     */
    public function __construct($RepetitionPreferenceOver100, $StoreReportInProject, $TagWeightChar, $TagWeightWord)
    {
      $this->RepetitionPreferenceOver100 = $RepetitionPreferenceOver100;
      $this->StoreReportInProject = $StoreReportInProject;
      $this->TagWeightChar = $TagWeightChar;
      $this->TagWeightWord = $TagWeightWord;
    }

}
