<?php

class ServerProjectTBAssignments
{

    /**
     * @var guid $PrimaryTBGuid
     * @access public
     */
    public $PrimaryTBGuid = null;

    /**
     * @var TBInfo[] $TBs
     * @access public
     */
    public $TBs = null;

    /**
     * @param guid $PrimaryTBGuid
     * @access public
     */
    public function __construct($PrimaryTBGuid)
    {
      $this->PrimaryTBGuid = $PrimaryTBGuid;
    }

}
