<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/FileResultInfo.php');

class TranslationDocExportResultInfo extends FileResultInfo
{

    /**
     * @var int[] $ErrorSegmentIndices
     * @access public
     */
    public $ErrorSegmentIndices = null;

    /**
     * @param ResultStatus $ResultStatus
     * @param guid $FileGuid
     * @access public
     */
    public function __construct($ResultStatus, $FileGuid)
    {
      parent::__construct($ResultStatus, $FileGuid);
    }

}
