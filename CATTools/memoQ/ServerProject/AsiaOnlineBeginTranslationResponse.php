<?php

class AsiaOnlineBeginTranslationResponse
{

    /**
     * @var AsiaOnlineBeginTranslationResultInfo[] $AsiaOnlineBeginTranslationResult
     * @access public
     */
    public $AsiaOnlineBeginTranslationResult = null;

    /**
     * @param AsiaOnlineBeginTranslationResultInfo[] $AsiaOnlineBeginTranslationResult
     * @access public
     */
    public function __construct($AsiaOnlineBeginTranslationResult)
    {
      $this->AsiaOnlineBeginTranslationResult = $AsiaOnlineBeginTranslationResult;
    }

}
