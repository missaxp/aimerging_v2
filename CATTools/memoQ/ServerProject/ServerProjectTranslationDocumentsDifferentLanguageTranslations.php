<?php

class ServerProjectTranslationDocumentsDifferentLanguageTranslations
{

    /**
     * @var ServerProjectTranslationDocBasicInfo[] $Documents
     * @access public
     */
    public $Documents = null;

    /**
     * @var boolean $IsMultilingualGroup
     * @access public
     */
    public $IsMultilingualGroup = null;

    /**
     * @param boolean $IsMultilingualGroup
     * @access public
     */
    public function __construct($IsMultilingualGroup)
    {
      $this->IsMultilingualGroup = $IsMultilingualGroup;
    }

}
