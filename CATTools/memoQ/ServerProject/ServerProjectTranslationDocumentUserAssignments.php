<?php

class ServerProjectTranslationDocumentUserAssignments
{

    /**
     * @var guid $DocumentGuid
     * @access public
     */
    public $DocumentGuid = null;

    /**
     * @var TranslationDocumentUserRoleAssignment[] $UserRoleAssignments
     * @access public
     */
    public $UserRoleAssignments = null;

    /**
     * @param guid $DocumentGuid
     * @access public
     */
    public function __construct($DocumentGuid)
    {
      $this->DocumentGuid = $DocumentGuid;
    }

}
