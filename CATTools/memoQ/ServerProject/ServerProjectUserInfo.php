<?php

class ServerProjectUserInfo
{

    /**
     * @var boolean $PermForLicense
     * @access public
     */
    public $PermForLicense = null;

    /**
     * @var ServerProjectRoles $ProjectRoles
     * @access public
     */
    public $ProjectRoles = null;

    /**
     * @var guid $UserGuid
     * @access public
     */
    public $UserGuid = null;

    /**
     * @param boolean $PermForLicense
     * @param guid $UserGuid
     * @access public
     */
    public function __construct($PermForLicense, $UserGuid)
    {
      $this->PermForLicense = $PermForLicense;
      $this->UserGuid = $UserGuid;
    }

}
