<?php

class ReImportTranslationDocumentsResponse
{

    /**
     * @var TranslationDocImportResultInfo[] $ReImportTranslationDocumentsResult
     * @access public
     */
    public $ReImportTranslationDocumentsResult = null;

    /**
     * @param TranslationDocImportResultInfo[] $ReImportTranslationDocumentsResult
     * @access public
     */
    public function __construct($ReImportTranslationDocumentsResult)
    {
      $this->ReImportTranslationDocumentsResult = $ReImportTranslationDocumentsResult;
    }

}
