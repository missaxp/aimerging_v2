<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentRoleAssignmentInfo.php');

class TranslationDocumentSubvendorAssignmentInfo extends TranslationDocumentRoleAssignmentInfo
{

    /**
     * @var guid $SubvendorGroupGuid
     * @access public
     */
    public $SubvendorGroupGuid = null;

    /**
     * @param TranslationDocumentAssignmentType $AssignmentType
     * @param dateTime $Deadline
     * @param int $RoleId
     * @param guid $SubvendorGroupGuid
     * @access public
     */
    public function __construct($AssignmentType, $Deadline, $RoleId, $SubvendorGroupGuid)
    {
      parent::__construct($AssignmentType, $Deadline, $RoleId);
      $this->SubvendorGroupGuid = $SubvendorGroupGuid;
    }

}
