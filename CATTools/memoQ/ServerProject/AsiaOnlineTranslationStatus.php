<?php

class AsiaOnlineTranslationStatus
{
    const __default = 'S00_NotStarted';
    const S00_NotStarted = 'S00_NotStarted';
    const S01_Exported = 'S01_Exported';
    const S02_Uploaded = 'S02_Uploaded';
    const S03_MTranslated = 'S03_MTranslated';
    const S04_ResultDownloaded = 'S04_ResultDownloaded';
    const S10_UpdatedInProject = 'S10_UpdatedInProject';
    const S11_ExportFailed = 'S11_ExportFailed';
    const S12_UploadFailed = 'S12_UploadFailed';
    const S13_TranslationFailed = 'S13_TranslationFailed';
    const S14_DownloadFailed = 'S14_DownloadFailed';
    const S15_UpdateFailed = 'S15_UpdateFailed';
    const S16_StoppedWaiting = 'S16_StoppedWaiting';


}
