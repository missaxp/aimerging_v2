<?php

class CreateProjectFromPackageResponse
{

    /**
     * @var guid $CreateProjectFromPackageResult
     * @access public
     */
    public $CreateProjectFromPackageResult = null;

    /**
     * @param guid $CreateProjectFromPackageResult
     * @access public
     */
    public function __construct($CreateProjectFromPackageResult)
    {
      $this->CreateProjectFromPackageResult = $CreateProjectFromPackageResult;
    }

}
