<?php

class ListPostTranslationAnalysisReports
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @param guid $serverProjectGuid
     * @access public
     */
    public function __construct($serverProjectGuid)
    {
      $this->serverProjectGuid = $serverProjectGuid;
    }

}
