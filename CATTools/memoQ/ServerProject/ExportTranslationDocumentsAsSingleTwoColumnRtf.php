<?php

class ExportTranslationDocumentsAsSingleTwoColumnRtf
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var guid[] $docGuids
     * @access public
     */
    public $docGuids = null;

    /**
     * @var TwoColumnRtfBilingualExportOptions $options
     * @access public
     */
    public $options = null;

    /**
     * @param guid $serverProjectGuid
     * @param guid[] $docGuids
     * @param TwoColumnRtfBilingualExportOptions $options
     * @access public
     */
    public function __construct($serverProjectGuid, $docGuids, $options)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->docGuids = $docGuids;
      $this->options = $options;
    }

}
