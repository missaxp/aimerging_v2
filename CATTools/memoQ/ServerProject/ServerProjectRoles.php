<?php

class ServerProjectRoles
{

    /**
     * @var boolean $ProjectManager
     * @access public
     */
    public $ProjectManager = null;

    /**
     * @var boolean $Terminologist
     * @access public
     */
    public $Terminologist = null;

    /**
     * @param boolean $ProjectManager
     * @param boolean $Terminologist
     * @access public
     */
    public function __construct($ProjectManager, $Terminologist)
    {
      $this->ProjectManager = $ProjectManager;
      $this->Terminologist = $Terminologist;
    }

}
