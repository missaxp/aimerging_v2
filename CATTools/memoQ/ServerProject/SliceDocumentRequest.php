<?php

class SliceDocumentRequest
{

    /**
     * @var guid $DocumentGuid
     * @access public
     */
    public $DocumentGuid = null;

    /**
     * @var SlicingMeasurementUnit $MeasurementUnit
     * @access public
     */
    public $MeasurementUnit = null;

    /**
     * @var int $NumberOfParts
     * @access public
     */
    public $NumberOfParts = null;

    /**
     * @param guid $DocumentGuid
     * @param SlicingMeasurementUnit $MeasurementUnit
     * @param int $NumberOfParts
     * @access public
     */
    public function __construct($DocumentGuid, $MeasurementUnit, $NumberOfParts)
    {
      $this->DocumentGuid = $DocumentGuid;
      $this->MeasurementUnit = $MeasurementUnit;
      $this->NumberOfParts = $NumberOfParts;
    }

}
