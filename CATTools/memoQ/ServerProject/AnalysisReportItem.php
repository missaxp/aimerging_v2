<?php

class AnalysisReportItem
{

    /**
     * @var AnalysisReportCounts $All
     * @access public
     */
    public $All = null;

    /**
     * @var AnalysisReportCounts $Fragments
     * @access public
     */
    public $Fragments = null;

    /**
     * @var AnalysisReportCounts $Hit100
     * @access public
     */
    public $Hit100 = null;

    /**
     * @var AnalysisReportCounts $Hit101
     * @access public
     */
    public $Hit101 = null;

    /**
     * @var AnalysisReportCounts $Hit50_74
     * @access public
     */
    public $Hit50_74 = null;

    /**
     * @var AnalysisReportCounts $Hit75_84
     * @access public
     */
    public $Hit75_84 = null;

    /**
     * @var AnalysisReportCounts $Hit85_94
     * @access public
     */
    public $Hit85_94 = null;

    /**
     * @var AnalysisReportCounts $Hit95_99
     * @access public
     */
    public $Hit95_99 = null;

    /**
     * @var AnalysisReportCounts $NoMatch
     * @access public
     */
    public $NoMatch = null;

    /**
     * @var AnalysisReportCounts $XTranslated
     * @access public
     */
    public $XTranslated = null;

    /**
     * @access public
     */
    public function __construct()
    {
    
    }

}
