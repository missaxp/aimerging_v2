<?php

class TranslationDocumentUserRoleAssignmentDetails
{

    /**
     * @var dateTime $DeadLine
     * @access public
     */
    public $DeadLine = null;

    /**
     * @var int $DocumentAssignmentRole
     * @access public
     */
    public $DocumentAssignmentRole = null;

    /**
     * @var UserInfoHeader $UserInfoHeader
     * @access public
     */
    public $UserInfoHeader = null;

    /**
     * @param dateTime $DeadLine
     * @param int $DocumentAssignmentRole
     * @access public
     */
    public function __construct($DeadLine, $DocumentAssignmentRole)
    {
      $this->DeadLine = $DeadLine;
      $this->DocumentAssignmentRole = $DocumentAssignmentRole;
    }

}
