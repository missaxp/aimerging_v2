<?php

class RenameProject
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var string $newName
     * @access public
     */
    public $newName = null;

    /**
     * @param guid $serverProjectGuid
     * @param string $newName
     * @access public
     */
    public function __construct($serverProjectGuid, $newName)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->newName = $newName;
    }

}
