<?php

class PackageDeliveryOptions
{

    /**
     * @var boolean $AllowPartialDelivery
     * @access public
     */
    public $AllowPartialDelivery = null;

    /**
     * @var boolean $AutoConfirmAndUpdateOnDelivery
     * @access public
     */
    public $AutoConfirmAndUpdateOnDelivery = null;

    /**
     * @param boolean $AllowPartialDelivery
     * @param boolean $AutoConfirmAndUpdateOnDelivery
     * @access public
     */
    public function __construct($AllowPartialDelivery, $AutoConfirmAndUpdateOnDelivery)
    {
      $this->AllowPartialDelivery = $AllowPartialDelivery;
      $this->AutoConfirmAndUpdateOnDelivery = $AutoConfirmAndUpdateOnDelivery;
    }

}
