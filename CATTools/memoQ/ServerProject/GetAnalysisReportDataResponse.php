<?php

class GetAnalysisReportDataResponse
{

    /**
     * @var AnalysisResultInfo $GetAnalysisReportDataResult
     * @access public
     */
    public $GetAnalysisReportDataResult = null;

    /**
     * @param AnalysisResultInfo $GetAnalysisReportDataResult
     * @access public
     */
    public function __construct($GetAnalysisReportDataResult)
    {
      $this->GetAnalysisReportDataResult = $GetAnalysisReportDataResult;
    }

}
