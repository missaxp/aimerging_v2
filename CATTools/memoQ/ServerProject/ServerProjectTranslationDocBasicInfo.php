<?php

class ServerProjectTranslationDocBasicInfo
{

    /**
     * @var guid $DocumentGuid
     * @access public
     */
    public $DocumentGuid = null;

    /**
     * @var string $Name
     * @access public
     */
    public $Name = null;

    /**
     * @var string $TargetLanguageCode
     * @access public
     */
    public $TargetLanguageCode = null;

    /**
     * @param guid $DocumentGuid
     * @access public
     */
    public function __construct($DocumentGuid)
    {
      $this->DocumentGuid = $DocumentGuid;
    }

}
