<?php

class ListContentOfPackagesResponse
{

    /**
     * @var PackageContentInfo[] $ListContentOfPackagesResult
     * @access public
     */
    public $ListContentOfPackagesResult = null;

    /**
     * @param PackageContentInfo[] $ListContentOfPackagesResult
     * @access public
     */
    public function __construct($ListContentOfPackagesResult)
    {
      $this->ListContentOfPackagesResult = $ListContentOfPackagesResult;
    }

}
