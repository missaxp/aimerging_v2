<?php

class ImportImageLocalizationPack
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var guid $uploadedPackageFileId
     * @access public
     */
    public $uploadedPackageFileId = null;

    /**
     * @param guid $serverProjectGuid
     * @param guid $uploadedPackageFileId
     * @access public
     */
    public function __construct($serverProjectGuid, $uploadedPackageFileId)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->uploadedPackageFileId = $uploadedPackageFileId;
    }

}
