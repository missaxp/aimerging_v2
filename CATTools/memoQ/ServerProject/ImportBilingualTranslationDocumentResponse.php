<?php

class ImportBilingualTranslationDocumentResponse
{

    /**
     * @var TranslationDocImportResultInfo[] $ImportBilingualTranslationDocumentResult
     * @access public
     */
    public $ImportBilingualTranslationDocumentResult = null;

    /**
     * @param TranslationDocImportResultInfo[] $ImportBilingualTranslationDocumentResult
     * @access public
     */
    public function __construct($ImportBilingualTranslationDocumentResult)
    {
      $this->ImportBilingualTranslationDocumentResult = $ImportBilingualTranslationDocumentResult;
    }

}
