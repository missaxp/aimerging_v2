<?php

class PostTranslationAnalysisAsCSVResult
{

    /**
     * @var PostTranslationAnalysisAsCSVResultForLang[] $DataForTargetLangs
     * @access public
     */
    public $DataForTargetLangs = null;

    /**
     * @access public
     */
    public function __construct()
    {
    
    }

}
