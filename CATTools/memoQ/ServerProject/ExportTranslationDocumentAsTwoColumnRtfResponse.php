<?php

class ExportTranslationDocumentAsTwoColumnRtfResponse
{

    /**
     * @var FileResultInfo $ExportTranslationDocumentAsTwoColumnRtfResult
     * @access public
     */
    public $ExportTranslationDocumentAsTwoColumnRtfResult = null;

    /**
     * @param FileResultInfo $ExportTranslationDocumentAsTwoColumnRtfResult
     * @access public
     */
    public function __construct($ExportTranslationDocumentAsTwoColumnRtfResult)
    {
      $this->ExportTranslationDocumentAsTwoColumnRtfResult = $ExportTranslationDocumentAsTwoColumnRtfResult;
    }

}
