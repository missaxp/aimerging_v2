<?php

class PretranslateDocuments
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var guid[] $translationDocGuids
     * @access public
     */
    public $translationDocGuids = null;

    /**
     * @var PretranslateOptions $options
     * @access public
     */
    public $options = null;

    /**
     * @param guid $serverProjectGuid
     * @param guid[] $translationDocGuids
     * @param PretranslateOptions $options
     * @access public
     */
    public function __construct($serverProjectGuid, $translationDocGuids, $options)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->translationDocGuids = $translationDocGuids;
      $this->options = $options;
    }

}
