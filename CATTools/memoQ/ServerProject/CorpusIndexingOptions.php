<?php

class CorpusIndexingOptions
{

    /**
     * @var boolean $EnableExpressIndexing
     * @access public
     */
    public $EnableExpressIndexing = null;

    /**
     * @var boolean $IndexOnlyWhenPublished
     * @access public
     */
    public $IndexOnlyWhenPublished = null;

    /**
     * @var boolean $IndexingEnabled
     * @access public
     */
    public $IndexingEnabled = null;

    /**
     * @var CorpusIndexingSchedule $Schedule
     * @access public
     */
    public $Schedule = null;

    /**
     * @param boolean $EnableExpressIndexing
     * @param boolean $IndexOnlyWhenPublished
     * @param boolean $IndexingEnabled
     * @param CorpusIndexingSchedule $Schedule
     * @access public
     */
    public function __construct($EnableExpressIndexing, $IndexOnlyWhenPublished, $IndexingEnabled, $Schedule)
    {
      $this->EnableExpressIndexing = $EnableExpressIndexing;
      $this->IndexOnlyWhenPublished = $IndexOnlyWhenPublished;
      $this->IndexingEnabled = $IndexingEnabled;
      $this->Schedule = $Schedule;
    }

}
