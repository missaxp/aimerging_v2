<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/ResultInfo.php');

class CreateDeliveryResult extends ResultInfo
{

    /**
     * @var guid[] $DocumentsNotFinished
     * @access public
     */
    public $DocumentsNotFinished = null;

    /**
     * @var guid $FileId
     * @access public
     */
    public $FileId = null;

    /**
     * @param ResultStatus $ResultStatus
     * @param guid $FileId
     * @access public
     */
    public function __construct($ResultStatus, $FileId)
    {
      parent::__construct($ResultStatus);
      $this->FileId = $FileId;
    }

}
