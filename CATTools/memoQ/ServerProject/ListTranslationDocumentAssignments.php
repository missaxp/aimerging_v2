<?php

class ListTranslationDocumentAssignments
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var ListTranslationDocumentAssignmentsOptions $options
     * @access public
     */
    public $options = null;

    /**
     * @param guid $serverProjectGuid
     * @param ListTranslationDocumentAssignmentsOptions $options
     * @access public
     */
    public function __construct($serverProjectGuid, $options)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->options = $options;
    }

}
