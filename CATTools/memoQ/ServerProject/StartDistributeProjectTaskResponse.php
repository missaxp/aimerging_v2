<?php

class StartDistributeProjectTaskResponse
{

    /**
     * @var TaskInfo $StartDistributeProjectTaskResult
     * @access public
     */
    public $StartDistributeProjectTaskResult = null;

    /**
     * @param TaskInfo $StartDistributeProjectTaskResult
     * @access public
     */
    public function __construct($StartDistributeProjectTaskResult)
    {
      $this->StartDistributeProjectTaskResult = $StartDistributeProjectTaskResult;
    }

}
