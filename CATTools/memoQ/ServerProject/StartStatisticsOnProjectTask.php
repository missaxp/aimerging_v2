<?php

class StartStatisticsOnProjectTask
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var string[] $targetLangCodes
     * @access public
     */
    public $targetLangCodes = null;

    /**
     * @var StatisticsOptions $options
     * @access public
     */
    public $options = null;

    /**
     * @var StatisticsResultFormat $resultFormat
     * @access public
     */
    public $resultFormat = null;

    /**
     * @param guid $serverProjectGuid
     * @param string[] $targetLangCodes
     * @param StatisticsOptions $options
     * @param StatisticsResultFormat $resultFormat
     * @access public
     */
    public function __construct($serverProjectGuid, $targetLangCodes, $options, $resultFormat)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->targetLangCodes = $targetLangCodes;
      $this->options = $options;
      $this->resultFormat = $resultFormat;
    }

}
