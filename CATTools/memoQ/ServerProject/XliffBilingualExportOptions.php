<?php

class XliffBilingualExportOptions
{

    /**
     * @var boolean $FullVersionHistory
     * @access public
     */
    public $FullVersionHistory = null;

    /**
     * @var boolean $IncludeSkeleton
     * @access public
     */
    public $IncludeSkeleton = null;

    /**
     * @var boolean $SaveCompressed
     * @access public
     */
    public $SaveCompressed = null;

    /**
     * @param boolean $FullVersionHistory
     * @param boolean $IncludeSkeleton
     * @param boolean $SaveCompressed
     * @access public
     */
    public function __construct($FullVersionHistory, $IncludeSkeleton, $SaveCompressed)
    {
      $this->FullVersionHistory = $FullVersionHistory;
      $this->IncludeSkeleton = $IncludeSkeleton;
      $this->SaveCompressed = $SaveCompressed;
    }

}
