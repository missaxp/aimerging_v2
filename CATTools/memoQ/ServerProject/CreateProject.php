<?php

class CreateProject
{

    /**
     * @var ServerProjectCreateInfo $spInfo
     * @access public
     */
    public $spInfo = null;

    /**
     * @param ServerProjectCreateInfo $spInfo
     * @access public
     */
    public function __construct($spInfo)
    {
      $this->spInfo = $spInfo;
    }

}
