<?php

class RestoreArchiveResponse
{

    /**
     * @var guid $RestoreArchiveResult
     * @access public
     */
    public $RestoreArchiveResult = null;

    /**
     * @param guid $RestoreArchiveResult
     * @access public
     */
    public function __construct($RestoreArchiveResult)
    {
      $this->RestoreArchiveResult = $RestoreArchiveResult;
    }

}
