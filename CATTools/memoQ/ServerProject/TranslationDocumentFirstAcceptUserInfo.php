<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentAssigneeInfo.php');

class TranslationDocumentFirstAcceptUserInfo extends TranslationDocumentAssigneeInfo
{

    /**
     * @var FirstAcceptUserDecision $Decision
     * @access public
     */
    public $Decision = null;

    /**
     * @param guid $AssigneeGuid
     * @param FirstAcceptUserDecision $Decision
     * @access public
     */
    public function __construct($AssigneeGuid, $Decision)
    {
      parent::__construct($AssigneeGuid);
      $this->Decision = $Decision;
    }

}
