<?php

class TaskStatus
{
    const __default = 'InvalidTask';
    const InvalidTask = 'InvalidTask';
    const Pending = 'Pending';
    const Executing = 'Executing';
    const Cancelled = 'Cancelled';
    const Completed = 'Completed';
    const Failed = 'Failed';


}
