<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/PostTransAnalysisReportItem.php');

class PostTransAnalysisReportForUser extends PostTransAnalysisReportItem
{

    /**
     * @var string $Username
     * @access public
     */
    public $Username = null;

    /**
     * @access public
     */
    public function __construct()
    {
      parent::__construct();
    }

}
