<?php

class GetPackageDeliveryOptionsResponse
{

    /**
     * @var PackageDeliveryOptions $GetPackageDeliveryOptionsResult
     * @access public
     */
    public $GetPackageDeliveryOptionsResult = null;

    /**
     * @param PackageDeliveryOptions $GetPackageDeliveryOptionsResult
     * @access public
     */
    public function __construct($GetPackageDeliveryOptionsResult)
    {
      $this->GetPackageDeliveryOptionsResult = $GetPackageDeliveryOptionsResult;
    }

}
