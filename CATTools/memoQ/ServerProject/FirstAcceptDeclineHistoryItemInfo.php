<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentHistoryItemInfo.php');

class FirstAcceptDeclineHistoryItemInfo extends DocumentHistoryItemInfo
{

    /**
     * @var boolean $FirstAcceptFailed
     * @access public
     */
    public $FirstAcceptFailed = null;

    /**
     * @var int $Role
     * @access public
     */
    public $Role = null;

    /**
     * @param guid $DocumentOrDivisionGuid
     * @param DocumentHistoryItemType $HistoryItemType
     * @param guid $ModifierSVGroupId
     * @param guid $ModifierUserGuid
     * @param dateTime $TimeStamp
     * @param boolean $FirstAcceptFailed
     * @param int $Role
     * @access public
     */
    public function __construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp, $FirstAcceptFailed, $Role)
    {
      parent::__construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp);
      $this->FirstAcceptFailed = $FirstAcceptFailed;
      $this->Role = $Role;
    }

}
