<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/ResultInfo.php');

class TranslationDocImportResultInfo extends ResultInfo
{

    /**
     * @var guid[] $DocumentGuids
     * @access public
     */
    public $DocumentGuids = null;

    /**
     * @param ResultStatus $ResultStatus
     * @access public
     */
    public function __construct($ResultStatus)
    {
      parent::__construct($ResultStatus);
    }

}
