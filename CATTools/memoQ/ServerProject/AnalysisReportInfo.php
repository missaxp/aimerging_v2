<?php

class AnalysisReportInfo
{

    /**
     * @var dateTime $Created
     * @access public
     */
    public $Created = null;

    /**
     * @var string $CreatedBy
     * @access public
     */
    public $CreatedBy = null;

    /**
     * @var guid $ID
     * @access public
     */
    public $ID = null;

    /**
     * @var string[] $Languages
     * @access public
     */
    public $Languages = null;

    /**
     * @var string $Note
     * @access public
     */
    public $Note = null;

    /**
     * @param dateTime $Created
     * @param guid $ID
     * @access public
     */
    public function __construct($Created, $ID)
    {
      $this->Created = $Created;
      $this->ID = $ID;
    }

}
