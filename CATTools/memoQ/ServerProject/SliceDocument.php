<?php

class SliceDocument
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var SliceDocumentRequest $options
     * @access public
     */
    public $options = null;

    /**
     * @param guid $serverProjectGuid
     * @param SliceDocumentRequest $options
     * @access public
     */
    public function __construct($serverProjectGuid, $options)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->options = $options;
    }

}
