<?php

class PretranslateLookupBehavior
{
    const __default = 'ExactMatchWithContext';
    const ExactMatchWithContext = 'ExactMatchWithContext';
    const ExactMatch = 'ExactMatch';
    const GoodMatch = 'GoodMatch';
    const AnyMatch = 'AnyMatch';


}
