<?php

class ReconsolidateDocument
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var guid $documentGuid
     * @access public
     */
    public $documentGuid = null;

    /**
     * @param guid $serverProjectGuid
     * @param guid $documentGuid
     * @access public
     */
    public function __construct($serverProjectGuid, $documentGuid)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->documentGuid = $documentGuid;
    }

}
