<?php

class ImportTranslationDocumentsWithFilterConfigResourceResponse
{

    /**
     * @var TranslationDocImportResultInfo[] $ImportTranslationDocumentsWithFilterConfigResourceResult
     * @access public
     */
    public $ImportTranslationDocumentsWithFilterConfigResourceResult = null;

    /**
     * @param TranslationDocImportResultInfo[] $ImportTranslationDocumentsWithFilterConfigResourceResult
     * @access public
     */
    public function __construct($ImportTranslationDocumentsWithFilterConfigResourceResult)
    {
      $this->ImportTranslationDocumentsWithFilterConfigResourceResult = $ImportTranslationDocumentsWithFilterConfigResourceResult;
    }

}
