<?php

class ImportTranslationDocuments
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var guid[] $fileGuids
     * @access public
     */
    public $fileGuids = null;

    /**
     * @var string[] $targetLangCodes
     * @access public
     */
    public $targetLangCodes = null;

    /**
     * @var string $importSettingsXML
     * @access public
     */
    public $importSettingsXML = null;

    /**
     * @param guid $serverProjectGuid
     * @param guid[] $fileGuids
     * @param string[] $targetLangCodes
     * @param string $importSettingsXML
     * @access public
     */
    public function __construct($serverProjectGuid, $fileGuids, $targetLangCodes, $importSettingsXML)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->fileGuids = $fileGuids;
      $this->targetLangCodes = $targetLangCodes;
      $this->importSettingsXML = $importSettingsXML;
    }

}
