<?php

class AsiaOnlineTranslateOptions
{

    /**
     * @var int $DomainCombinationCode
     * @access public
     */
    public $DomainCombinationCode = null;

    /**
     * @var int $JobTimeoutHours
     * @access public
     */
    public $JobTimeoutHours = null;

    /**
     * @var int $ProjectId
     * @access public
     */
    public $ProjectId = null;

    /**
     * @var boolean $SubmitEditedRows
     * @access public
     */
    public $SubmitEditedRows = null;

    /**
     * @var boolean $SubmitNotStartedAndFragmentAssembledRows
     * @access public
     */
    public $SubmitNotStartedAndFragmentAssembledRows = null;

    /**
     * @var boolean $SubmitPreTranslatedRows
     * @access public
     */
    public $SubmitPreTranslatedRows = null;

    /**
     * @var int $SubmitPreTranslatedRowsMatchThreshold
     * @access public
     */
    public $SubmitPreTranslatedRowsMatchThreshold = null;

    /**
     * @param int $DomainCombinationCode
     * @param int $JobTimeoutHours
     * @param int $ProjectId
     * @param boolean $SubmitEditedRows
     * @param boolean $SubmitNotStartedAndFragmentAssembledRows
     * @param boolean $SubmitPreTranslatedRows
     * @param int $SubmitPreTranslatedRowsMatchThreshold
     * @access public
     */
    public function __construct($DomainCombinationCode, $JobTimeoutHours, $ProjectId, $SubmitEditedRows, $SubmitNotStartedAndFragmentAssembledRows, $SubmitPreTranslatedRows, $SubmitPreTranslatedRowsMatchThreshold)
    {
      $this->DomainCombinationCode = $DomainCombinationCode;
      $this->JobTimeoutHours = $JobTimeoutHours;
      $this->ProjectId = $ProjectId;
      $this->SubmitEditedRows = $SubmitEditedRows;
      $this->SubmitNotStartedAndFragmentAssembledRows = $SubmitNotStartedAndFragmentAssembledRows;
      $this->SubmitPreTranslatedRows = $SubmitPreTranslatedRows;
      $this->SubmitPreTranslatedRowsMatchThreshold = $SubmitPreTranslatedRowsMatchThreshold;
    }

}
