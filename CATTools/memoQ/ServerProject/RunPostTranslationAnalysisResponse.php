<?php

class RunPostTranslationAnalysisResponse
{

    /**
     * @var PostTranslationAnalysisResultInfo $RunPostTranslationAnalysisResult
     * @access public
     */
    public $RunPostTranslationAnalysisResult = null;

    /**
     * @param PostTranslationAnalysisResultInfo $RunPostTranslationAnalysisResult
     * @access public
     */
    public function __construct($RunPostTranslationAnalysisResult)
    {
      $this->RunPostTranslationAnalysisResult = $RunPostTranslationAnalysisResult;
    }

}
