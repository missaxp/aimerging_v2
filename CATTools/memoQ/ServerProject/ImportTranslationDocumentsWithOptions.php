<?php

class ImportTranslationDocumentsWithOptions
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var ImportTranslationDocumentOptions[] $importDocOptions
     * @access public
     */
    public $importDocOptions = null;

    /**
     * @param guid $serverProjectGuid
     * @param ImportTranslationDocumentOptions[] $importDocOptions
     * @access public
     */
    public function __construct($serverProjectGuid, $importDocOptions)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->importDocOptions = $importDocOptions;
    }

}
