<?php

class ListPackagesForProjectResponse
{

    /**
     * @var PackageInfo[] $ListPackagesForProjectResult
     * @access public
     */
    public $ListPackagesForProjectResult = null;

    /**
     * @param PackageInfo[] $ListPackagesForProjectResult
     * @access public
     */
    public function __construct($ListPackagesForProjectResult)
    {
      $this->ListPackagesForProjectResult = $ListPackagesForProjectResult;
    }

}
