<?php

class AsiaOnlineGetDomainCombinations
{

    /**
     * @var int $languagePairCode
     * @access public
     */
    public $languagePairCode = null;

    /**
     * @param int $languagePairCode
     * @access public
     */
    public function __construct($languagePairCode)
    {
      $this->languagePairCode = $languagePairCode;
    }

}
