<?php

class AddLanguageToProject
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var ServerProjectAddLanguageInfo $addLanguageInfo
     * @access public
     */
    public $addLanguageInfo = null;

    /**
     * @param guid $serverProjectGuid
     * @param ServerProjectAddLanguageInfo $addLanguageInfo
     * @access public
     */
    public function __construct($serverProjectGuid, $addLanguageInfo)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->addLanguageInfo = $addLanguageInfo;
    }

}
