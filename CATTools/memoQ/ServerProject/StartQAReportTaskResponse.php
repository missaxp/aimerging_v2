<?php

class StartQAReportTaskResponse
{

    /**
     * @var TaskInfo $StartQAReportTaskResult
     * @access public
     */
    public $StartQAReportTaskResult = null;

    /**
     * @param TaskInfo $StartQAReportTaskResult
     * @access public
     */
    public function __construct($StartQAReportTaskResult)
    {
      $this->StartQAReportTaskResult = $StartQAReportTaskResult;
    }

}
