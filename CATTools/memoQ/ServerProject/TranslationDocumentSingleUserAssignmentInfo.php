<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentRoleAssignmentInfo.php');

class TranslationDocumentSingleUserAssignmentInfo extends TranslationDocumentRoleAssignmentInfo
{

    /**
     * @var guid $UserGuid
     * @access public
     */
    public $UserGuid = null;

    /**
     * @param TranslationDocumentAssignmentType $AssignmentType
     * @param dateTime $Deadline
     * @param int $RoleId
     * @param guid $UserGuid
     * @access public
     */
    public function __construct($AssignmentType, $Deadline, $RoleId, $UserGuid)
    {
      parent::__construct($AssignmentType, $Deadline, $RoleId);
      $this->UserGuid = $UserGuid;
    }

}
