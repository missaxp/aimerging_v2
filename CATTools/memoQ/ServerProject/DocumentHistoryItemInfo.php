<?php

class DocumentHistoryItemInfo
{

    /**
     * @var guid $DocumentOrDivisionGuid
     * @access public
     */
    public $DocumentOrDivisionGuid = null;

    /**
     * @var string $DocumentOrDivisionName
     * @access public
     */
    public $DocumentOrDivisionName = null;

    /**
     * @var DocumentHistoryItemType $HistoryItemType
     * @access public
     */
    public $HistoryItemType = null;

    /**
     * @var guid $ModifierSVGroupId
     * @access public
     */
    public $ModifierSVGroupId = null;

    /**
     * @var guid $ModifierUserGuid
     * @access public
     */
    public $ModifierUserGuid = null;

    /**
     * @var dateTime $TimeStamp
     * @access public
     */
    public $TimeStamp = null;

    /**
     * @param guid $DocumentOrDivisionGuid
     * @param DocumentHistoryItemType $HistoryItemType
     * @param guid $ModifierSVGroupId
     * @param guid $ModifierUserGuid
     * @param dateTime $TimeStamp
     * @access public
     */
    public function __construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp)
    {
      $this->DocumentOrDivisionGuid = $DocumentOrDivisionGuid;
      $this->HistoryItemType = $HistoryItemType;
      $this->ModifierSVGroupId = $ModifierSVGroupId;
      $this->ModifierUserGuid = $ModifierUserGuid;
      $this->TimeStamp = $TimeStamp;
    }

}
