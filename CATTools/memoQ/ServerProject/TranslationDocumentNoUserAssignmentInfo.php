<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentRoleAssignmentInfo.php');

class TranslationDocumentNoUserAssignmentInfo extends TranslationDocumentRoleAssignmentInfo
{

    /**
     * @param TranslationDocumentAssignmentType $AssignmentType
     * @param dateTime $Deadline
     * @param int $RoleId
     * @access public
     */
    public function __construct($AssignmentType, $Deadline, $RoleId)
    {
      parent::__construct($AssignmentType, $Deadline, $RoleId);
    }

}
