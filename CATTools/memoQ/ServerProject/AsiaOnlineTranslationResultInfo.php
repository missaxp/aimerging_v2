<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/ResultInfo.php');

class AsiaOnlineTranslationResultInfo extends ResultInfo
{

    /**
     * @var dateTime $DeadlineOrFinished
     * @access public
     */
    public $DeadlineOrFinished = null;

    /**
     * @var guid $DocumentId
     * @access public
     */
    public $DocumentId = null;

    /**
     * @var string $DocumentName
     * @access public
     */
    public $DocumentName = null;

    /**
     * @var string $ErrorMessage
     * @access public
     */
    public $ErrorMessage = null;

    /**
     * @var AsiaOnlineTranslationStatus $Status
     * @access public
     */
    public $Status = null;

    /**
     * @var string $TargetLang
     * @access public
     */
    public $TargetLang = null;

    /**
     * @param ResultStatus $ResultStatus
     * @param dateTime $DeadlineOrFinished
     * @param guid $DocumentId
     * @param AsiaOnlineTranslationStatus $Status
     * @access public
     */
    public function __construct($ResultStatus, $DeadlineOrFinished, $DocumentId, $Status)
    {
      parent::__construct($ResultStatus);
      $this->DeadlineOrFinished = $DeadlineOrFinished;
      $this->DocumentId = $DocumentId;
      $this->Status = $Status;
    }

}
