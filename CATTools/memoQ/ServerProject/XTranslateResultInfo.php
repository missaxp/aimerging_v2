<?php

class XTranslateResultInfo
{

    /**
     * @var XTranslateDocumentResult[] $DocumentResults
     * @access public
     */
    public $DocumentResults = null;

    /**
     * @access public
     */
    public function __construct()
    {
    
    }

}
