<?php

class XTranslateOptions
{

    /**
     * @var XTranslateDocInfo[] $DocInfos
     * @access public
     */
    public $DocInfos = null;

    /**
     * @var NewRevisionScenarioOptions $NewRevisionOptions
     * @access public
     */
    public $NewRevisionOptions = null;

    /**
     * @var boolean $WorkWithContextIds
     * @access public
     */
    public $WorkWithContextIds = null;

    /**
     * @var XTranslateScenario $XTranslateScenario
     * @access public
     */
    public $XTranslateScenario = null;

    /**
     * @param boolean $WorkWithContextIds
     * @param XTranslateScenario $XTranslateScenario
     * @access public
     */
    public function __construct($WorkWithContextIds, $XTranslateScenario)
    {
      $this->WorkWithContextIds = $WorkWithContextIds;
      $this->XTranslateScenario = $XTranslateScenario;
    }

}
