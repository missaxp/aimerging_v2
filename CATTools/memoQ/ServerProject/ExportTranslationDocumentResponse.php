<?php

class ExportTranslationDocumentResponse
{

    /**
     * @var TranslationDocExportResultInfo $ExportTranslationDocumentResult
     * @access public
     */
    public $ExportTranslationDocumentResult = null;

    /**
     * @param TranslationDocExportResultInfo $ExportTranslationDocumentResult
     * @access public
     */
    public function __construct($ExportTranslationDocumentResult)
    {
      $this->ExportTranslationDocumentResult = $ExportTranslationDocumentResult;
    }

}
