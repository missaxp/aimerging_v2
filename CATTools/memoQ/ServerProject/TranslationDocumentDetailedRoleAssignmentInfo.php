<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentDetailedAssignmentInfo.php');

class TranslationDocumentDetailedRoleAssignmentInfo extends TranslationDocumentDetailedAssignmentInfo
{

    /**
     * @var int $RoleId
     * @access public
     */
    public $RoleId = null;

    /**
     * @param TranslationDocumentAssignmentType $AssignmentType
     * @param dateTime $Deadline
     * @param int $RoleId
     * @access public
     */
    public function __construct($AssignmentType, $Deadline, $RoleId)
    {
      parent::__construct($AssignmentType, $Deadline);
      $this->RoleId = $RoleId;
    }

}
