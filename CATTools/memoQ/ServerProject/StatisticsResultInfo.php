<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/ResultInfo.php');

class StatisticsResultInfo extends ResultInfo
{

    /**
     * @var StatisticsResultForLang[] $ResultsForTargetLangs
     * @access public
     */
    public $ResultsForTargetLangs = null;

    /**
     * @param ResultStatus $ResultStatus
     * @access public
     */
    public function __construct($ResultStatus)
    {
      parent::__construct($ResultStatus);
    }

}
