<?php

class StartXTranslateTaskResponse
{

    /**
     * @var TaskInfo $StartXTranslateTaskResult
     * @access public
     */
    public $StartXTranslateTaskResult = null;

    /**
     * @param TaskInfo $StartXTranslateTaskResult
     * @access public
     */
    public function __construct($StartXTranslateTaskResult)
    {
      $this->StartXTranslateTaskResult = $StartXTranslateTaskResult;
    }

}
