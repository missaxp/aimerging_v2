<?php

class StartStatisticsOnProjectTaskResponse
{

    /**
     * @var TaskInfo $StartStatisticsOnProjectTaskResult
     * @access public
     */
    public $StartStatisticsOnProjectTaskResult = null;

    /**
     * @param TaskInfo $StartStatisticsOnProjectTaskResult
     * @access public
     */
    public function __construct($StartStatisticsOnProjectTaskResult)
    {
      $this->StartStatisticsOnProjectTaskResult = $StartStatisticsOnProjectTaskResult;
    }

}
