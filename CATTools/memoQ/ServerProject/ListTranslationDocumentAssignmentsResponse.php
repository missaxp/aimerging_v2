<?php

class ListTranslationDocumentAssignmentsResponse
{

    /**
     * @var TranslationDocumentDetailedAssignments[] $ListTranslationDocumentAssignmentsResult
     * @access public
     */
    public $ListTranslationDocumentAssignmentsResult = null;

    /**
     * @param TranslationDocumentDetailedAssignments[] $ListTranslationDocumentAssignmentsResult
     * @access public
     */
    public function __construct($ListTranslationDocumentAssignmentsResult)
    {
      $this->ListTranslationDocumentAssignmentsResult = $ListTranslationDocumentAssignmentsResult;
    }

}
