<?php

class ExportTranslationDocumentsAsSingleTwoColumnRtfResponse
{

    /**
     * @var FileResultInfo $ExportTranslationDocumentsAsSingleTwoColumnRtfResult
     * @access public
     */
    public $ExportTranslationDocumentsAsSingleTwoColumnRtfResult = null;

    /**
     * @param FileResultInfo $ExportTranslationDocumentsAsSingleTwoColumnRtfResult
     * @access public
     */
    public function __construct($ExportTranslationDocumentsAsSingleTwoColumnRtfResult)
    {
      $this->ExportTranslationDocumentsAsSingleTwoColumnRtfResult = $ExportTranslationDocumentsAsSingleTwoColumnRtfResult;
    }

}
