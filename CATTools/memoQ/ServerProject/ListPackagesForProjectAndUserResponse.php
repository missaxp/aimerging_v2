<?php

class ListPackagesForProjectAndUserResponse
{

    /**
     * @var PackageInfo[] $ListPackagesForProjectAndUserResult
     * @access public
     */
    public $ListPackagesForProjectAndUserResult = null;

    /**
     * @param PackageInfo[] $ListPackagesForProjectAndUserResult
     * @access public
     */
    public function __construct($ListPackagesForProjectAndUserResult)
    {
      $this->ListPackagesForProjectAndUserResult = $ListPackagesForProjectAndUserResult;
    }

}
