<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/DocumentHistoryItemInfo.php');

class WorkflowStatusChangeHistoryItemInfo extends DocumentHistoryItemInfo
{

    /**
     * @var WorkflowStatus $NewWorkflowStatus
     * @access public
     */
    public $NewWorkflowStatus = null;

    /**
     * @var WorkflowStatus $OldWorkflowStatus
     * @access public
     */
    public $OldWorkflowStatus = null;

    /**
     * @param guid $DocumentOrDivisionGuid
     * @param DocumentHistoryItemType $HistoryItemType
     * @param guid $ModifierSVGroupId
     * @param guid $ModifierUserGuid
     * @param dateTime $TimeStamp
     * @param WorkflowStatus $NewWorkflowStatus
     * @param WorkflowStatus $OldWorkflowStatus
     * @access public
     */
    public function __construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp, $NewWorkflowStatus, $OldWorkflowStatus)
    {
      parent::__construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp);
      $this->NewWorkflowStatus = $NewWorkflowStatus;
      $this->OldWorkflowStatus = $OldWorkflowStatus;
    }

}
