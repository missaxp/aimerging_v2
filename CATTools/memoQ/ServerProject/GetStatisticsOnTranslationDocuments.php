<?php

class GetStatisticsOnTranslationDocuments
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var guid[] $translationDocGuids
     * @access public
     */
    public $translationDocGuids = null;

    /**
     * @var StatisticsOptions $options
     * @access public
     */
    public $options = null;

    /**
     * @var StatisticsResultFormat $resultFormat
     * @access public
     */
    public $resultFormat = null;

    /**
     * @param guid $serverProjectGuid
     * @param guid[] $translationDocGuids
     * @param StatisticsOptions $options
     * @param StatisticsResultFormat $resultFormat
     * @access public
     */
    public function __construct($serverProjectGuid, $translationDocGuids, $options, $resultFormat)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->translationDocGuids = $translationDocGuids;
      $this->options = $options;
      $this->resultFormat = $resultFormat;
    }

}
