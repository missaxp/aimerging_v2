<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/FileResultInfo.php');

class ImportImageLocalizationPackResultInfo extends FileResultInfo
{

    /**
     * @var string[] $SkippedImages
     * @access public
     */
    public $SkippedImages = null;

    /**
     * @param ResultStatus $ResultStatus
     * @param guid $FileGuid
     * @access public
     */
    public function __construct($ResultStatus, $FileGuid)
    {
      parent::__construct($ResultStatus, $FileGuid);
    }

}
