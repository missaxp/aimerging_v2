<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentDetailedRoleAssignmentInfo.php');

class TranslationDocumentDetailedSingleUserAssignmentInfo extends TranslationDocumentDetailedRoleAssignmentInfo
{

    /**
     * @var TranslationDocumentAssigneeInfo $User
     * @access public
     */
    public $User = null;

    /**
     * @param TranslationDocumentAssignmentType $AssignmentType
     * @param dateTime $Deadline
     * @param int $RoleId
     * @access public
     */
    public function __construct($AssignmentType, $Deadline, $RoleId)
    {
      parent::__construct($AssignmentType, $Deadline, $RoleId);
    }

}
