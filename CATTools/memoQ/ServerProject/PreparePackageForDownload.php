<?php

class PreparePackageForDownload
{

    /**
     * @var guid $packageId
     * @access public
     */
    public $packageId = null;

    /**
     * @param guid $packageId
     * @access public
     */
    public function __construct($packageId)
    {
      $this->packageId = $packageId;
    }

}
