<?php

class ListProjectTranslationDocumentSlicesResponse
{

    /**
     * @var ServerProjectTranslationDocSliceInfo[] $ListProjectTranslationDocumentSlicesResult
     * @access public
     */
    public $ListProjectTranslationDocumentSlicesResult = null;

    /**
     * @param ServerProjectTranslationDocSliceInfo[] $ListProjectTranslationDocumentSlicesResult
     * @access public
     */
    public function __construct($ListProjectTranslationDocumentSlicesResult)
    {
      $this->ListProjectTranslationDocumentSlicesResult = $ListProjectTranslationDocumentSlicesResult;
    }

}
