<?php

class DeliverDocumentRequest
{

    /**
     * @var guid $DeliveringUserGuid
     * @access public
     */
    public $DeliveringUserGuid = null;

    /**
     * @var guid $DocumentGuid
     * @access public
     */
    public $DocumentGuid = null;

    /**
     * @var boolean $ReturnDocToPreviousActor
     * @access public
     */
    public $ReturnDocToPreviousActor = null;

    /**
     * @param guid $DeliveringUserGuid
     * @param guid $DocumentGuid
     * @param boolean $ReturnDocToPreviousActor
     * @access public
     */
    public function __construct($DeliveringUserGuid, $DocumentGuid, $ReturnDocToPreviousActor)
    {
      $this->DeliveringUserGuid = $DeliveringUserGuid;
      $this->DocumentGuid = $DocumentGuid;
      $this->ReturnDocToPreviousActor = $ReturnDocToPreviousActor;
    }

}
