<?php

class RunAnalysisResponse
{

    /**
     * @var AnalysisResultInfo $RunAnalysisResult
     * @access public
     */
    public $RunAnalysisResult = null;

    /**
     * @param AnalysisResultInfo $RunAnalysisResult
     * @access public
     */
    public function __construct($RunAnalysisResult)
    {
      $this->RunAnalysisResult = $RunAnalysisResult;
    }

}
