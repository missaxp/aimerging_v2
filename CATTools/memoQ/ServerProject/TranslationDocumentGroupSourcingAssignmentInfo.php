<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/TranslationDocumentRoleAssignmentInfo.php');

class TranslationDocumentGroupSourcingAssignmentInfo extends TranslationDocumentRoleAssignmentInfo
{

    /**
     * @var guid[] $UserGuids
     * @access public
     */
    public $UserGuids = null;

    /**
     * @param TranslationDocumentAssignmentType $AssignmentType
     * @param dateTime $Deadline
     * @param int $RoleId
     * @access public
     */
    public function __construct($AssignmentType, $Deadline, $RoleId)
    {
      parent::__construct($AssignmentType, $Deadline, $RoleId);
    }

}
