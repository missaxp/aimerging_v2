<?php

class RestoreArchive
{

    /**
     * @var guid $archiveFileID
     * @access public
     */
    public $archiveFileID = null;

    /**
     * @param guid $archiveFileID
     * @access public
     */
    public function __construct($archiveFileID)
    {
      $this->archiveFileID = $archiveFileID;
    }

}
