<?php

class RunPostTranslationAnalysis
{

    /**
     * @var guid $serverProjectGuid
     * @access public
     */
    public $serverProjectGuid = null;

    /**
     * @var PostTranslationAnalysisOptions $options
     * @access public
     */
    public $options = null;

    /**
     * @param guid $serverProjectGuid
     * @param PostTranslationAnalysisOptions $options
     * @access public
     */
    public function __construct($serverProjectGuid, $options)
    {
      $this->serverProjectGuid = $serverProjectGuid;
      $this->options = $options;
    }

}
