<?php

class UpdateTranslationDocumentFromTableRtfResponse
{

    /**
     * @var TranslationDocImportResultInfo[] $UpdateTranslationDocumentFromTableRtfResult
     * @access public
     */
    public $UpdateTranslationDocumentFromTableRtfResult = null;

    /**
     * @param TranslationDocImportResultInfo[] $UpdateTranslationDocumentFromTableRtfResult
     * @access public
     */
    public function __construct($UpdateTranslationDocumentFromTableRtfResult)
    {
      $this->UpdateTranslationDocumentFromTableRtfResult = $UpdateTranslationDocumentFromTableRtfResult;
    }

}
