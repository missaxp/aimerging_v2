<?php

class ServerProjectConfidentialitySettings
{

    /**
     * @var boolean $DisableMTPlugins
     * @access public
     */
    public $DisableMTPlugins = null;

    /**
     * @var boolean $DisableTBPlugins
     * @access public
     */
    public $DisableTBPlugins = null;

    /**
     * @var boolean $DisableTMPlugins
     * @access public
     */
    public $DisableTMPlugins = null;

    /**
     * @access public
     */
    public function __construct()
    {
    
    }

}
