<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/ResultInfo.php');

class AsiaOnlineGetLanguagePairCodeResultInfo extends ResultInfo
{

    /**
     * @var int $AOLanguagePairId
     * @access public
     */
    public $AOLanguagePairId = null;

    /**
     * @param ResultStatus $ResultStatus
     * @param int $AOLanguagePairId
     * @access public
     */
    public function __construct($ResultStatus, $AOLanguagePairId)
    {
      parent::__construct($ResultStatus);
      $this->AOLanguagePairId = $AOLanguagePairId;
    }

}
