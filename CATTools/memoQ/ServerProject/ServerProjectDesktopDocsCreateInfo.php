<?php

include_once(BaseDir.'/CATTools/memoQ/ServerProject/ServerProjectCreateInfo.php');

class ServerProjectDesktopDocsCreateInfo extends ServerProjectCreateInfo
{

    /**
     * @var string $CallbackWebServiceUrl
     * @access public
     */
    public $CallbackWebServiceUrl = null;

    /**
     * @var ServerProjectConfidentialitySettings $ConfidentialitySettings
     * @access public
     */
    public $ConfidentialitySettings = null;

    /**
     * @var boolean $CreateOfflineTMTBCopies
     * @access public
     */
    public $CreateOfflineTMTBCopies = null;

    /**
     * @var boolean $DownloadPreview
     * @access public
     */
    public $DownloadPreview = null;

    /**
     * @var boolean $DownloadSkeleton
     * @access public
     */
    public $DownloadSkeleton = null;

    /**
     * @var boolean $EnableSplitJoin
     * @access public
     */
    public $EnableSplitJoin = null;

    /**
     * @var boolean $EnableWebTrans
     * @access public
     */
    public $EnableWebTrans = null;

    /**
     * @var ServerProjectNotificationSettings $NotificationSettings
     * @access public
     */
    public $NotificationSettings = null;

    /**
     * @param boolean $AllowOverlappingWorkflow
     * @param boolean $AllowPackageCreation
     * @param guid $CreatorUser
     * @param dateTime $Deadline
     * @param boolean $DownloadPreview2
     * @param boolean $DownloadSkeleton2
     * @param boolean $EnableCommunication
     * @param boolean $OmitHitsWithNoTargetTerm
     * @param ServerProjectResourcesInPackages $PackageResourceHandling
     * @param boolean $PreventDeliveryOnQAError
     * @param boolean $RecordVersionHistory
     * @param boolean $StrictSubLangMatching
     * @param boolean $CreateOfflineTMTBCopies
     * @param boolean $DownloadPreview
     * @param boolean $DownloadSkeleton
     * @param boolean $EnableSplitJoin
     * @param boolean $EnableWebTrans
     * @access public
     */
    public function __construct($AllowOverlappingWorkflow, $AllowPackageCreation, $CreatorUser, $Deadline, $DownloadPreview2, $DownloadSkeleton2, $EnableCommunication, $OmitHitsWithNoTargetTerm, $PackageResourceHandling, $PreventDeliveryOnQAError, $RecordVersionHistory, $StrictSubLangMatching, $CreateOfflineTMTBCopies, $DownloadPreview, $DownloadSkeleton, $EnableSplitJoin, $EnableWebTrans, $Name, $SourceLanguage, $TargetLanguages)
    {
      parent::__construct($AllowOverlappingWorkflow, $AllowPackageCreation, $CreatorUser, $Deadline, $DownloadPreview2, $DownloadSkeleton2, $EnableCommunication, $OmitHitsWithNoTargetTerm, $PackageResourceHandling, $PreventDeliveryOnQAError, $RecordVersionHistory, $StrictSubLangMatching, $Name, $SourceLanguage, $TargetLanguages);
      $this->CreateOfflineTMTBCopies = $CreateOfflineTMTBCopies;
      $this->DownloadPreview = $DownloadPreview;
      $this->DownloadSkeleton = $DownloadSkeleton;
      $this->EnableSplitJoin = $EnableSplitJoin;
      $this->EnableWebTrans = $EnableWebTrans;
    }

}
