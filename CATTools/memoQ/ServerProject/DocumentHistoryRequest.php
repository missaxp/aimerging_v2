<?php

class DocumentHistoryRequest
{

    /**
     * @var guid[] $DocumentGuids
     * @access public
     */
    public $DocumentGuids = null;

    /**
     * @access public
     */
    public function __construct()
    {
    
    }

}
