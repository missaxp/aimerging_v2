<?php

class Login
{

    /**
     * @var string $userName
     * @access public
     */
    public $userName = null;

    /**
     * @var string $passwordHash
     * @access public
     */
    public $passwordHash = null;

    /**
     * @param string $userName
     * @param string $passwordHash
     * @access public
     */
    public function __construct($userName, $passwordHash)
    {
      $this->userName = $userName;
      $this->passwordHash = $passwordHash;
    }

}
