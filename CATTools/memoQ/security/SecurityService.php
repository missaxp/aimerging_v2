<?php

include_once(BaseDir.'/CATTools/memoQ/common/UserPackageWorkflowType.php');
include_once(BaseDir.'/CATTools/memoQ/security/ListUsers.php');
include_once(BaseDir.'/CATTools/memoQ/security/ListUsersResponse.php');
include_once(BaseDir.'/CATTools/memoQ/common/UserInfo.php');
include_once(BaseDir.'/CATTools/memoQ/common/UnexpectedFault.php');
include_once(BaseDir.'/CATTools/memoQ/common/GenericFault.php');
include_once(BaseDir.'/CATTools/memoQ/security/ListSubvendorManagers.php');
include_once(BaseDir.'/CATTools/memoQ/security/ListSubvendorManagersResponse.php');
include_once(BaseDir.'/CATTools/memoQ/security/GetUser.php');
include_once(BaseDir.'/CATTools/memoQ/security/GetUserResponse.php');
include_once(BaseDir.'/CATTools/memoQ/security/CreateUser.php');
include_once(BaseDir.'/CATTools/memoQ/security/CreateUserResponse.php');
include_once(BaseDir.'/CATTools/memoQ/security/UpdateUser.php');
include_once(BaseDir.'/CATTools/memoQ/security/UpdateUserResponse.php');
include_once(BaseDir.'/CATTools/memoQ/security/DeleteUser.php');
include_once(BaseDir.'/CATTools/memoQ/security/DeleteUserResponse.php');
include_once(BaseDir.'/CATTools/memoQ/security/ListGroupsOfUser.php');
include_once(BaseDir.'/CATTools/memoQ/security/ListGroupsOfUserResponse.php');
include_once(BaseDir.'/CATTools/memoQ/security/GroupInfo.php');
include_once(BaseDir.'/CATTools/memoQ/security/SetGroupsOfUser.php');
include_once(BaseDir.'/CATTools/memoQ/security/SetGroupsOfUserResponse.php');
include_once(BaseDir.'/CATTools/memoQ/security/AddSubvendorManagerToGroup.php');
include_once(BaseDir.'/CATTools/memoQ/security/AddSubvendorManagerToGroupResponse.php');
include_once(BaseDir.'/CATTools/memoQ/security/RemoveSubvendorManagerFromGroup.php');
include_once(BaseDir.'/CATTools/memoQ/security/RemoveSubvendorManagerFromGroupResponse.php');
include_once(BaseDir.'/CATTools/memoQ/security/ListGroups.php');
include_once(BaseDir.'/CATTools/memoQ/security/ListGroupsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/security/ListSubvendorGroups.php');
include_once(BaseDir.'/CATTools/memoQ/security/ListSubvendorGroupsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/security/GetGroup.php');
include_once(BaseDir.'/CATTools/memoQ/security/GetGroupResponse.php');
include_once(BaseDir.'/CATTools/memoQ/security/CreateGroup.php');
include_once(BaseDir.'/CATTools/memoQ/security/CreateGroupResponse.php');
include_once(BaseDir.'/CATTools/memoQ/security/UpdateGroup.php');
include_once(BaseDir.'/CATTools/memoQ/security/UpdateGroupResponse.php');
include_once(BaseDir.'/CATTools/memoQ/security/DeleteGroup.php');
include_once(BaseDir.'/CATTools/memoQ/security/DeleteGroupResponse.php');
include_once(BaseDir.'/CATTools/memoQ/security/ListUsersOfGroup.php');
include_once(BaseDir.'/CATTools/memoQ/security/ListUsersOfGroupResponse.php');
include_once(BaseDir.'/CATTools/memoQ/security/SetUsersOfGroup.php');
include_once(BaseDir.'/CATTools/memoQ/security/SetUsersOfGroupResponse.php');
include_once(BaseDir.'/CATTools/memoQ/security/SetObjectPermissions.php');
include_once(BaseDir.'/CATTools/memoQ/security/ObjectPermission.php');
include_once(BaseDir.'/CATTools/memoQ/security/SetObjectPermissionsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/security/ListObjectPermissions.php');
include_once(BaseDir.'/CATTools/memoQ/security/ListObjectPermissionsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/security/Login.php');
include_once(BaseDir.'/CATTools/memoQ/security/LoginResponse.php');
include_once(BaseDir.'/CATTools/memoQ/security/Logout.php');
include_once(BaseDir.'/CATTools/memoQ/security/LogoutResponse.php');

class SecurityService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'ListUsers' => '\ListUsers',
      'ListUsersResponse' => '\ListUsersResponse',
      'UserInfo' => '\UserInfo',
      'UnexpectedFault' => '\UnexpectedFault',
      'GenericFault' => '\GenericFault',
      'ListSubvendorManagers' => '\ListSubvendorManagers',
      'ListSubvendorManagersResponse' => '\ListSubvendorManagersResponse',
      'GetUser' => '\GetUser',
      'GetUserResponse' => '\GetUserResponse',
      'CreateUser' => '\CreateUser',
      'CreateUserResponse' => '\CreateUserResponse',
      'UpdateUser' => '\UpdateUser',
      'UpdateUserResponse' => '\UpdateUserResponse',
      'DeleteUser' => '\DeleteUser',
      'DeleteUserResponse' => '\DeleteUserResponse',
      'ListGroupsOfUser' => '\ListGroupsOfUser',
      'ListGroupsOfUserResponse' => '\ListGroupsOfUserResponse',
      'GroupInfo' => '\GroupInfo',
      'SetGroupsOfUser' => '\SetGroupsOfUser',
      'SetGroupsOfUserResponse' => '\SetGroupsOfUserResponse',
      'AddSubvendorManagerToGroup' => '\AddSubvendorManagerToGroup',
      'AddSubvendorManagerToGroupResponse' => '\AddSubvendorManagerToGroupResponse',
      'RemoveSubvendorManagerFromGroup' => '\RemoveSubvendorManagerFromGroup',
      'RemoveSubvendorManagerFromGroupResponse' => '\RemoveSubvendorManagerFromGroupResponse',
      'ListGroups' => '\ListGroups',
      'ListGroupsResponse' => '\ListGroupsResponse',
      'ListSubvendorGroups' => '\ListSubvendorGroups',
      'ListSubvendorGroupsResponse' => '\ListSubvendorGroupsResponse',
      'GetGroup' => '\GetGroup',
      'GetGroupResponse' => '\GetGroupResponse',
      'CreateGroup' => '\CreateGroup',
      'CreateGroupResponse' => '\CreateGroupResponse',
      'UpdateGroup' => '\UpdateGroup',
      'UpdateGroupResponse' => '\UpdateGroupResponse',
      'DeleteGroup' => '\DeleteGroup',
      'DeleteGroupResponse' => '\DeleteGroupResponse',
      'ListUsersOfGroup' => '\ListUsersOfGroup',
      'ListUsersOfGroupResponse' => '\ListUsersOfGroupResponse',
      'SetUsersOfGroup' => '\SetUsersOfGroup',
      'SetUsersOfGroupResponse' => '\SetUsersOfGroupResponse',
      'SetObjectPermissions' => '\SetObjectPermissions',
      'ObjectPermission' => '\ObjectPermission',
      'SetObjectPermissionsResponse' => '\SetObjectPermissionsResponse',
      'ListObjectPermissions' => '\ListObjectPermissions',
      'ListObjectPermissionsResponse' => '\ListObjectPermissionsResponse',
      'Login' => '\Login',
      'LoginResponse' => '\LoginResponse',
      'Logout' => '\Logout',
      'LogoutResponse' => '\LogoutResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'http://memoq2.idisc.es:8080/memoqservices/security?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param ListUsers $parameters
     * @access public
     * @return ListUsersResponse
     */
    public function ListUsers(ListUsers $parameters)
    {
      return $this->__soapCall('ListUsers', array($parameters));
    }

    /**
     * @param ListSubvendorManagers $parameters
     * @access public
     * @return ListSubvendorManagersResponse
     */
    public function ListSubvendorManagers(ListSubvendorManagers $parameters)
    {
      return $this->__soapCall('ListSubvendorManagers', array($parameters));
    }

    /**
     * @param GetUser $parameters
     * @access public
     * @return GetUserResponse
     */
    public function GetUser(GetUser $parameters)
    {
      return $this->__soapCall('GetUser', array($parameters));
    }

    /**
     * @param CreateUser $parameters
     * @access public
     * @return CreateUserResponse
     */
    public function CreateUser(CreateUser $parameters)
    {
      return $this->__soapCall('CreateUser', array($parameters));
    }

    /**
     * @param UpdateUser $parameters
     * @access public
     * @return UpdateUserResponse
     */
    public function UpdateUser(UpdateUser $parameters)
    {
      return $this->__soapCall('UpdateUser', array($parameters));
    }

    /**
     * @param DeleteUser $parameters
     * @access public
     * @return DeleteUserResponse
     */
    public function DeleteUser(DeleteUser $parameters)
    {
      return $this->__soapCall('DeleteUser', array($parameters));
    }

    /**
     * @param ListGroupsOfUser $parameters
     * @access public
     * @return ListGroupsOfUserResponse
     */
    public function ListGroupsOfUser(ListGroupsOfUser $parameters)
    {
      return $this->__soapCall('ListGroupsOfUser', array($parameters));
    }

    /**
     * @param SetGroupsOfUser $parameters
     * @access public
     * @return SetGroupsOfUserResponse
     */
    public function SetGroupsOfUser(SetGroupsOfUser $parameters)
    {
      return $this->__soapCall('SetGroupsOfUser', array($parameters));
    }

    /**
     * @param AddSubvendorManagerToGroup $parameters
     * @access public
     * @return AddSubvendorManagerToGroupResponse
     */
    public function AddSubvendorManagerToGroup(AddSubvendorManagerToGroup $parameters)
    {
      return $this->__soapCall('AddSubvendorManagerToGroup', array($parameters));
    }

    /**
     * @param RemoveSubvendorManagerFromGroup $parameters
     * @access public
     * @return RemoveSubvendorManagerFromGroupResponse
     */
    public function RemoveSubvendorManagerFromGroup(RemoveSubvendorManagerFromGroup $parameters)
    {
      return $this->__soapCall('RemoveSubvendorManagerFromGroup', array($parameters));
    }

    /**
     * @param ListGroups $parameters
     * @access public
     * @return ListGroupsResponse
     */
    public function ListGroups(ListGroups $parameters)
    {
      return $this->__soapCall('ListGroups', array($parameters));
    }

    /**
     * @param ListSubvendorGroups $parameters
     * @access public
     * @return ListSubvendorGroupsResponse
     */
    public function ListSubvendorGroups(ListSubvendorGroups $parameters)
    {
      return $this->__soapCall('ListSubvendorGroups', array($parameters));
    }

    /**
     * @param GetGroup $parameters
     * @access public
     * @return GetGroupResponse
     */
    public function GetGroup(GetGroup $parameters)
    {
      return $this->__soapCall('GetGroup', array($parameters));
    }

    /**
     * @param CreateGroup $parameters
     * @access public
     * @return CreateGroupResponse
     */
    public function CreateGroup(CreateGroup $parameters)
    {
      return $this->__soapCall('CreateGroup', array($parameters));
    }

    /**
     * @param UpdateGroup $parameters
     * @access public
     * @return UpdateGroupResponse
     */
    public function UpdateGroup(UpdateGroup $parameters)
    {
      return $this->__soapCall('UpdateGroup', array($parameters));
    }

    /**
     * @param DeleteGroup $parameters
     * @access public
     * @return DeleteGroupResponse
     */
    public function DeleteGroup(DeleteGroup $parameters)
    {
      return $this->__soapCall('DeleteGroup', array($parameters));
    }

    /**
     * @param ListUsersOfGroup $parameters
     * @access public
     * @return ListUsersOfGroupResponse
     */
    public function ListUsersOfGroup(ListUsersOfGroup $parameters)
    {
      return $this->__soapCall('ListUsersOfGroup', array($parameters));
    }

    /**
     * @param SetUsersOfGroup $parameters
     * @access public
     * @return SetUsersOfGroupResponse
     */
    public function SetUsersOfGroup(SetUsersOfGroup $parameters)
    {
      return $this->__soapCall('SetUsersOfGroup', array($parameters));
    }

    /**
     * @param SetObjectPermissions $parameters
     * @access public
     * @return SetObjectPermissionsResponse
     */
    public function SetObjectPermissions(SetObjectPermissions $parameters)
    {
      return $this->__soapCall('SetObjectPermissions', array($parameters));
    }

    /**
     * @param ListObjectPermissions $parameters
     * @access public
     * @return ListObjectPermissionsResponse
     */
    public function ListObjectPermissions(ListObjectPermissions $parameters)
    {
      return $this->__soapCall('ListObjectPermissions', array($parameters));
    }

    /**
     * @param Login $parameters
     * @access public
     * @return LoginResponse
     */
    public function Login(Login $parameters)
    {
      return $this->__soapCall('Login', array($parameters));
    }

    /**
     * @param Logout $parameters
     * @access public
     * @return LogoutResponse
     */
    public function Logout(Logout $parameters)
    {
      return $this->__soapCall('Logout', array($parameters));
    }

}
