<?php

class ListGroupsOfUserResponse
{

    /**
     * @var GroupInfo[] $ListGroupsOfUserResult
     * @access public
     */
    public $ListGroupsOfUserResult = null;

    /**
     * @param GroupInfo[] $ListGroupsOfUserResult
     * @access public
     */
    public function __construct($ListGroupsOfUserResult)
    {
      $this->ListGroupsOfUserResult = $ListGroupsOfUserResult;
    }

}
