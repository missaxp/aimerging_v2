<?php

class GroupInfo
{

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var guid $GroupGuid
     * @access public
     */
    public $GroupGuid = null;

    /**
     * @var string $GroupName
     * @access public
     */
    public $GroupName = null;

    /**
     * @var boolean $IsDisabled
     * @access public
     */
    public $IsDisabled = null;

    /**
     * @var boolean $IsSubvendorGroup
     * @access public
     */
    public $IsSubvendorGroup = null;

    /**
     * @param guid $GroupGuid
     * @param boolean $IsDisabled
     * @param boolean $IsSubvendorGroup
     * @access public
     */
    public function __construct($GroupGuid, $IsDisabled, $IsSubvendorGroup)
    {
      $this->GroupGuid = $GroupGuid;
      $this->IsDisabled = $IsDisabled;
      $this->IsSubvendorGroup = $IsSubvendorGroup;
    }

}
