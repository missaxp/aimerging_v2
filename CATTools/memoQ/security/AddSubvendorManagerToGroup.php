<?php

class AddSubvendorManagerToGroup
{

    /**
     * @var guid $userGuid
     * @access public
     */
    public $userGuid = null;

    /**
     * @var guid $groupGuid
     * @access public
     */
    public $groupGuid = null;

    /**
     * @param guid $userGuid
     * @param guid $groupGuid
     * @access public
     */
    public function __construct($userGuid, $groupGuid)
    {
      $this->userGuid = $userGuid;
      $this->groupGuid = $groupGuid;
    }

}
