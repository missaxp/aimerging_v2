<?php

class CreateGroupResponse
{

    /**
     * @var guid $CreateGroupResult
     * @access public
     */
    public $CreateGroupResult = null;

    /**
     * @param guid $CreateGroupResult
     * @access public
     */
    public function __construct($CreateGroupResult)
    {
      $this->CreateGroupResult = $CreateGroupResult;
    }

}
