<?php

class ListSubvendorManagersResponse
{

    /**
     * @var UserInfo[] $ListSubvendorManagersResult
     * @access public
     */
    public $ListSubvendorManagersResult = null;

    /**
     * @param UserInfo[] $ListSubvendorManagersResult
     * @access public
     */
    public function __construct($ListSubvendorManagersResult)
    {
      $this->ListSubvendorManagersResult = $ListSubvendorManagersResult;
    }

}
