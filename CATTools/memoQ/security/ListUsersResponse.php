<?php

class ListUsersResponse
{

    /**
     * @var UserInfo[] $ListUsersResult
     * @access public
     */
    public $ListUsersResult = null;

    /**
     * @param UserInfo[] $ListUsersResult
     * @access public
     */
    public function __construct($ListUsersResult)
    {
      $this->ListUsersResult = $ListUsersResult;
    }

}
