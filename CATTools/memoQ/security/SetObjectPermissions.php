<?php

class SetObjectPermissions
{

    /**
     * @var guid $objectGuid
     * @access public
     */
    public $objectGuid = null;

    /**
     * @var ObjectPermission[] $permissions
     * @access public
     */
    public $permissions = null;

    /**
     * @param guid $objectGuid
     * @param ObjectPermission[] $permissions
     * @access public
     */
    public function __construct($objectGuid, $permissions)
    {
      $this->objectGuid = $objectGuid;
      $this->permissions = $permissions;
    }

}
