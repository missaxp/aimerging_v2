<?php

namespace preprocessorModule\memoQ;

use CATTools\memoQ\MemoQManager;

include_once BaseDir . '/CATTools/memoQ/security/SecurityService.php';
include_once BaseDir . '/CATTools/memoQ/MemoQManager.php';
/**
 *
 * @author zroque
 * @version 1.0
 *          created on 06/02/2019
 */
class MemoQSecurityManager extends MemoQManager{

	public function __construct(){

	}
	
	/**
	 * Lists all the users on memoq server and returns an array with the information for each user
	 * @return array
	 */
	public function listUsers(){
		$userList = array();
		try {
			$client = new \SecurityService();
			$users = new \ListUsers();
			$response = new \ListUsersResponse($client->ListUsers($users));
			$userList = $response->ListUsersResult->ListUsersResult->UserInfo;
		} catch(\Exception $ex) {
			\Functions::addLog($ex->getMessage(), \Functions::ERROR, $ex->getTraceAsString());
			$this->addError($ex->getMessage());
		}
		return $userList;
	}
	
	/**
	 * Returns the GUID of the given user name, if the username is not found will return null
	 *
	 * @param string $userName
	 * @return NULL|string
	 */
	public function getUserGuid(string $userName){
		
		$userGuid = null;
		try {
			$client = new \SecurityService();
			$users = new \ListUsers();
			$response = new \ListUsersResponse($client->ListUsers($users));
			$usersList = $response->ListUsersResult->ListUsersResult->UserInfo;
			foreach($usersList as $user) {
				if ($user->UserName == $userName) {
					$userGuid = $user->UserGuid;
				}
			}
		} catch(\Exception $ex) {
			\Functions::addLog($ex->getMessage(), \Functions::ERROR, $ex->getTraceAsString());
			$this->addError($ex->getMessage());
		}
		return $userGuid;
	}
}

