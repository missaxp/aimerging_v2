<?php

class ResourceListFilter
{

    /**
     * @var string $Client
     * @access public
     */
    public $Client = null;

    /**
     * @var string $Domain
     * @access public
     */
    public $Domain = null;

    /**
     * @var string $NameOrDescription
     * @access public
     */
    public $NameOrDescription = null;

    /**
     * @var string $Project
     * @access public
     */
    public $Project = null;

    /**
     * @var string $Subject
     * @access public
     */
    public $Subject = null;

    /**
     * @access public
     */
    public function __construct()
    {
    
    }

}
