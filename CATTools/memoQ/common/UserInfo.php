<?php

class UserInfo
{

    /**
     * @var string $Address
     * @access public
     */
    public $Address = null;

    /**
     * @var string $EmailAddress
     * @access public
     */
    public $EmailAddress = null;

    /**
     * @var string $FullName
     * @access public
     */
    public $FullName = null;

    /**
     * @var boolean $IsDisabled
     * @access public
     */
    public $IsDisabled = null;

    /**
     * @var boolean $IsSubvendorManager
     * @access public
     */
    public $IsSubvendorManager = null;

    /**
     * @var string $LTEmailAddress
     * @access public
     */
    public $LTEmailAddress = null;

    /**
     * @var string $LTFullName
     * @access public
     */
    public $LTFullName = null;

    /**
     * @var string $LTUsername
     * @access public
     */
    public $LTUsername = null;

    /**
     * @var string $LanguagePairs
     * @access public
     */
    public $LanguagePairs = null;

    /**
     * @var int $MergeType
     * @access public
     */
    public $MergeType = null;

    /**
     * @var string $MobilePhoneNumber
     * @access public
     */
    public $MobilePhoneNumber = null;

    /**
     * @var UserPackageWorkflowType $PackageWorkflowType
     * @access public
     */
    public $PackageWorkflowType = null;

    /**
     * @var string $Password
     * @access public
     */
    public $Password = null;

    /**
     * @var string $PhoneNumber
     * @access public
     */
    public $PhoneNumber = null;

    /**
     * @var guid $SecondarySID
     * @access public
     */
    public $SecondarySID = null;

    /**
     * @var guid $UserGuid
     * @access public
     */
    public $UserGuid = null;

    /**
     * @var string $UserName
     * @access public
     */
    public $UserName = null;

    /**
     * @param boolean $IsDisabled
     * @param boolean $IsSubvendorManager
     * @param int $MergeType
     * @param UserPackageWorkflowType $PackageWorkflowType
     * @param guid $UserGuid
     * @access public
     */
    public function __construct($IsDisabled, $IsSubvendorManager, $MergeType, $PackageWorkflowType, $UserGuid)
    {
      $this->IsDisabled = $IsDisabled;
      $this->IsSubvendorManager = $IsSubvendorManager;
      $this->MergeType = $MergeType;
      $this->PackageWorkflowType = $PackageWorkflowType;
      $this->UserGuid = $UserGuid;
    }

}
