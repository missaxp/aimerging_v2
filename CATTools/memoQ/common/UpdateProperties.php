<?php

class UpdateProperties
{

    /**
     * @var TBUpdateInfo $updateInfo
     * @access public
     */
    public $updateInfo = null;

    /**
     * @param TBUpdateInfo $updateInfo
     * @access public
     */
    public function __construct($updateInfo)
    {
      $this->updateInfo = $updateInfo;
    }

}
