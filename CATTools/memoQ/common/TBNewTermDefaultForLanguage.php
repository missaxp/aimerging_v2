<?php

class TBNewTermDefaultForLanguage
{

    /**
     * @var TBCaseSensitivity $CaseSensitivity
     * @access public
     */
    public $CaseSensitivity = null;

    /**
     * @var string $LanguageCode
     * @access public
     */
    public $LanguageCode = null;

    /**
     * @var TBMatching $Matching
     * @access public
     */
    public $Matching = null;

    /**
     * @param TBCaseSensitivity $CaseSensitivity
     * @param TBMatching $Matching
     * @access public
     */
    public function __construct($CaseSensitivity, $Matching)
    {
      $this->CaseSensitivity = $CaseSensitivity;
      $this->Matching = $Matching;
    }

}
