<?php

include_once(BaseDir.'/CATTools/memoQ/common/ResourceUpdateInfo.php');

class HeavyResourceUpdateInfo extends ResourceUpdateInfo
{

    /**
     * @var string $Client
     * @access public
     */
    public $Client = null;

    /**
     * @var string $Domain
     * @access public
     */
    public $Domain = null;

    /**
     * @var string $Project
     * @access public
     */
    public $Project = null;

    /**
     * @var string $Subject
     * @access public
     */
    public $Subject = null;

    /**
     * @param guid $Guid
     * @param boolean $Readonly
     * @access public
     */
    public function __construct($Guid, $Readonly)
    {
      parent::__construct($Guid, $Readonly);
    }

}
