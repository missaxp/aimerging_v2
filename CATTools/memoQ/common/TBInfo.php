<?php

include_once(BaseDir.'/CATTools/memoQ/common/HeavyResourceInfo.php');

class TBInfo extends HeavyResourceInfo
{

    /**
     * @var string $CreatorUsername
     * @access public
     */
    public $CreatorUsername = null;

    /**
     * @var boolean $IsModerated
     * @access public
     */
    public $IsModerated = null;

    /**
     * @var boolean $IsQTerm
     * @access public
     */
    public $IsQTerm = null;

    /**
     * @var string[] $LanguageCodes
     * @access public
     */
    public $LanguageCodes = null;

    /**
     * @var boolean $ModLateDisclosure
     * @access public
     */
    public $ModLateDisclosure = null;

    /**
     * @var TBNewTermDefaultForLanguage[] $NewTermDefaults
     * @access public
     */
    public $NewTermDefaults = null;

    /**
     * @param guid $Guid
     * @param boolean $Readonly
     * @param boolean $IsModerated
     * @param boolean $IsQTerm
     * @param boolean $ModLateDisclosure
     * @access public
     */
    public function __construct($Guid, $Readonly, $IsModerated, $IsQTerm, $ModLateDisclosure)
    {
      parent::__construct($Guid, $Readonly);
      $this->IsModerated = $IsModerated;
      $this->IsQTerm = $IsQTerm;
      $this->ModLateDisclosure = $ModLateDisclosure;
    }

}
