<?php

class ResourceInfo
{

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var guid $Guid
     * @access public
     */
    public $Guid = null;

    /**
     * @var string $Name
     * @access public
     */
    public $Name = null;

    /**
     * @var boolean $Readonly
     * @access public
     */
    public $Readonly = null;

    /**
     * @param guid $Guid
     * @param boolean $Readonly
     * @access public
     */
    public function __construct($Guid, $Readonly)
    {
      $this->Guid = $Guid;
      $this->Readonly = $Readonly;
    }

}
