<?php

class CreateAndPublishResponse
{

    /**
     * @var guid $CreateAndPublishResult
     * @access public
     */
    public $CreateAndPublishResult = null;

    /**
     * @param guid $CreateAndPublishResult
     * @access public
     */
    public function __construct($CreateAndPublishResult)
    {
      $this->CreateAndPublishResult = $CreateAndPublishResult;
    }

}
