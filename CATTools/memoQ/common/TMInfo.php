<?php

include_once(BaseDir.'/CATTools/memoQ/common/HeavyResourceInfo.php');

class TMInfo extends HeavyResourceInfo
{

    /**
     * @var boolean $AllowMultiple
     * @access public
     */
    public $AllowMultiple = null;

    /**
     * @var boolean $AllowReverseLookup
     * @access public
     */
    public $AllowReverseLookup = null;

    /**
     * @var string $CreatorUsername
     * @access public
     */
    public $CreatorUsername = null;

    /**
     * @var int $NumOfEntries
     * @access public
     */
    public $NumOfEntries = null;

    /**
     * @var TMOptimizationPreference $OptimizationPreference
     * @access public
     */
    public $OptimizationPreference = null;

    /**
     * @var string $SourceLanguageCode
     * @access public
     */
    public $SourceLanguageCode = null;

    /**
     * @var boolean $StoreDocumentFullPath
     * @access public
     */
    public $StoreDocumentFullPath = null;

    /**
     * @var boolean $StoreDocumentName
     * @access public
     */
    public $StoreDocumentName = null;

    /**
     * @var boolean $StoreFormatting
     * @access public
     */
    public $StoreFormatting = null;

    /**
     * @var string $TargetLanguageCode
     * @access public
     */
    public $TargetLanguageCode = null;

    /**
     * @var boolean $UseContext
     * @access public
     */
    public $UseContext = null;

    /**
     * @var boolean $UseIceSpiceContext
     * @access public
     */
    public $UseIceSpiceContext = null;
    
    /**
     * @var string $Name
     * @access public
     */
    public $Name = null;
    
    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;
    
    /**
     * @var string $Guid
     * @access public
     */
    public $Guid = null;
    
    /**
     * @var string $Client
     * @access public
     */
    public $Client = null;
    
    /**
     * @var string  $Domain
     * @access public
     */
    public $Domain = null;
    
    /**
     * @var string $Project
     * @access public
     */
    public $Project = null;
    
    /**
     * @var string $Subject
     * @access public
     */
    public $Subject = null;
    
    /**
     * @var boolean $Readonly	
     * @access public
     */
    public $Readonly = null;

    /**
     * @param guid $Guid
     * @param boolean $Readonly
     * @param boolean $AllowMultiple
     * @param boolean $AllowReverseLookup
     * @param int $NumOfEntries
     * @param TMOptimizationPreference $OptimizationPreference
     * @param boolean $StoreDocumentFullPath
     * @param boolean $StoreDocumentName
     * @param boolean $StoreFormatting
     * @param boolean $UseContext
     * @param boolean $UseIceSpiceContext
     * @access public
     */
    public function __construct(/*$Guid, $Readonly,*/ $AllowMultiple, $AllowReverseLookup, $NumOfEntries, $OptimizationPreference, $StoreDocumentFullPath, $StoreDocumentName, $StoreFormatting, $UseContext, $UseIceSpiceContext, $CreatorUsername, $SourceLanguageCode, $TargetLanguageCode, $Name, $Description, $Client, $Domain, $Project, $Subject)
    {
    //  parent::__construct($Guid, $Readonly);
      $this->AllowMultiple = $AllowMultiple;
      $this->AllowReverseLookup = $AllowReverseLookup;
      $this->NumOfEntries = $NumOfEntries;
      $this->OptimizationPreference = $OptimizationPreference;
      $this->StoreDocumentFullPath = $StoreDocumentFullPath;
      $this->StoreDocumentName = $StoreDocumentName;
      $this->StoreFormatting = $StoreFormatting;
      $this->UseContext = $UseContext;
      $this->UseIceSpiceContext = $UseIceSpiceContext;
      $this->CreatorUsername = $CreatorUsername;
      $this->SourceLanguageCode = $SourceLanguageCode;
      $this->TargetLanguageCode = $TargetLanguageCode;
      $this->Name = $Name;
      $this->Description = $Description;
      $this->Client = $Client;
      $this->Domain = $Domain;
      $this->Project = $Project;
      $this->Subject = $Subject;
    }

}
