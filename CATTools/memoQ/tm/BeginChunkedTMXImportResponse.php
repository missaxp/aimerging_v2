<?php

class BeginChunkedTMXImportResponse
{

    /**
     * @var guid $BeginChunkedTMXImportResult
     * @access public
     */
    public $BeginChunkedTMXImportResult = null;

    /**
     * @param guid $BeginChunkedTMXImportResult
     * @access public
     */
    public function __construct($BeginChunkedTMXImportResult)
    {
      $this->BeginChunkedTMXImportResult = $BeginChunkedTMXImportResult;
    }

}
