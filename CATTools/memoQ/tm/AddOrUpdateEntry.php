<?php

class AddOrUpdateEntry
{

    /**
     * @var string $sessionId
     * @access public
     */
    public $sessionId = null;

    /**
     * @var string $segmentDataXml
     * @access public
     */
    public $segmentDataXml = null;

    /**
     * @var guid $tmGuid
     * @access public
     */
    public $tmGuid = null;

    /**
     * @param string $sessionId
     * @param string $segmentDataXml
     * @param guid $tmGuid
     * @access public
     */
    public function __construct($sessionId, $segmentDataXml, $tmGuid)
    {
      $this->sessionId = $sessionId;
      $this->segmentDataXml = $segmentDataXml;
      $this->tmGuid = $tmGuid;
    }

}
