<?php

include_once(BaseDir.'/CATTools/memoQ/common/TMOptimizationPreference.php');
include_once(BaseDir.'/CATTools/memoQ/common/CreateAndPublish.php');
include_once(BaseDir.'/CATTools/memoQ/common/TMInfo.php');
include_once(BaseDir.'/CATTools/memoQ/common/HeavyResourceInfo.php');
include_once(BaseDir.'/CATTools/memoQ/common/ResourceInfo.php');
include_once(BaseDir.'/CATTools/memoQ/common/CreateAndPublishResponse.php');
include_once(BaseDir.'/CATTools/memoQ/common/UnexpectedFault.php');
include_once(BaseDir.'/CATTools/memoQ/common/GenericFault.php');
include_once(BaseDir.'/CATTools/memoQ/tm/ListTMs.php');
include_once(BaseDir.'/CATTools/memoQ/tm/ListTMsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tm/ListTMs2.php');
include_once(BaseDir.'/CATTools/memoQ/tm/TMListFilter.php');
include_once(BaseDir.'/CATTools/memoQ/common/ResourceListFilter.php');
include_once(BaseDir.'/CATTools/memoQ/tm/ListTMs2Response.php');
include_once(BaseDir.'/CATTools/memoQ/tm/GetTMInfo.php');
include_once(BaseDir.'/CATTools/memoQ/tm/GetTMInfoResponse.php');
include_once(BaseDir.'/CATTools/memoQ/common/UpdateProperties.php');
include_once(BaseDir.'/CATTools/memoQ/tm/TMUpdateInfo.php');
include_once(BaseDir.'/CATTools/memoQ/common/HeavyResourceUpdateInfo.php');
include_once(BaseDir.'/CATTools/memoQ/common/ResourceUpdateInfo.php');
include_once(BaseDir.'/CATTools/memoQ/common/UpdatePropertiesResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tm/DeleteTM.php');
include_once(BaseDir.'/CATTools/memoQ/tm/DeleteTMResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tm/BeginChunkedTMXImport.php');
include_once(BaseDir.'/CATTools/memoQ/tm/BeginChunkedTMXImportResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tm/AddNextTMXChunk.php');
include_once(BaseDir.'/CATTools/memoQ/tm/AddNextTMXChunkResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tm/EndChunkedTMXImport.php');
include_once(BaseDir.'/CATTools/memoQ/tm/EndChunkedTMXImportResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tm/TmxImportResult.php');
include_once(BaseDir.'/CATTools/memoQ/tm/BeginChunkedTMXExport.php');
include_once(BaseDir.'/CATTools/memoQ/tm/BeginChunkedTMXExportResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tm/GetNextTMXChunk.php');
include_once(BaseDir.'/CATTools/memoQ/tm/GetNextTMXChunkResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tm/EndChunkedTMXExport.php');
include_once(BaseDir.'/CATTools/memoQ/tm/EndChunkedTMXExportResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tm/LookupSegment.php');
include_once(BaseDir.'/CATTools/memoQ/tm/LookupSegmentRequest.php');
include_once(BaseDir.'/CATTools/memoQ/tm/InlineTagStrictness.php');
include_once(BaseDir.'/CATTools/memoQ/tm/LookupSegmentResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tm/InvalidSessionIdFault.php');
include_once(BaseDir.'/CATTools/memoQ/tm/UnauthorizedAccessFault.php');
include_once(BaseDir.'/CATTools/memoQ/tm/NoLicenseFault.php');
include_once(BaseDir.'/CATTools/memoQ/common/TMFault.php');
include_once(BaseDir.'/CATTools/memoQ/tm/RequestXmlFormatFault.php');
include_once(BaseDir.'/CATTools/memoQ/tm/Concordance.php');
include_once(BaseDir.'/CATTools/memoQ/tm/ConcordanceRequest.php');
include_once(BaseDir.'/CATTools/memoQ/tm/ConcordanceResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tm/AddOrUpdateEntry.php');
include_once(BaseDir.'/CATTools/memoQ/tm/AddOrUpdateEntryResponse.php');

class TMService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'CreateAndPublish' => '\CreateAndPublish',
      'TMInfo' => '\TMInfo',
    		'HeavyResourceInfo' => '\HeavyResourceInfo',
      'ResourceInfo' => '\ResourceInfo',
      'CreateAndPublishResponse' => '\CreateAndPublishResponse',
      'UnexpectedFault' => '\UnexpectedFault',
      'GenericFault' => '\GenericFault',
      'ListTMs' => '\ListTMs',
      'ListTMsResponse' => '\ListTMsResponse',
      'ListTMs2' => '\ListTMs2',
      'TMListFilter' => '\TMListFilter',
      'ResourceListFilter' => '\ResourceListFilter',
      'ListTMs2Response' => '\ListTMs2Response',
      'GetTMInfo' => '\GetTMInfo',
      'GetTMInfoResponse' => '\GetTMInfoResponse',
      'UpdateProperties' => '\UpdateProperties',
      'TMUpdateInfo' => '\TMUpdateInfo',
      'HeavyResourceUpdateInfo' => '\HeavyResourceUpdateInfo',
      'ResourceUpdateInfo' => '\ResourceUpdateInfo',
      'UpdatePropertiesResponse' => '\UpdatePropertiesResponse',
      'DeleteTM' => '\DeleteTM',
      'DeleteTMResponse' => '\DeleteTMResponse',
      'BeginChunkedTMXImport' => '\BeginChunkedTMXImport',
      'BeginChunkedTMXImportResponse' => '\BeginChunkedTMXImportResponse',
      'AddNextTMXChunk' => '\AddNextTMXChunk',
      'AddNextTMXChunkResponse' => '\AddNextTMXChunkResponse',
      'EndChunkedTMXImport' => '\EndChunkedTMXImport',
      'EndChunkedTMXImportResponse' => '\EndChunkedTMXImportResponse',
      'TmxImportResult' => '\TmxImportResult',
      'BeginChunkedTMXExport' => '\BeginChunkedTMXExport',
      'BeginChunkedTMXExportResponse' => '\BeginChunkedTMXExportResponse',
      'GetNextTMXChunk' => '\GetNextTMXChunk',
      'GetNextTMXChunkResponse' => '\GetNextTMXChunkResponse',
      'EndChunkedTMXExport' => '\EndChunkedTMXExport',
      'EndChunkedTMXExportResponse' => '\EndChunkedTMXExportResponse',
      'LookupSegment' => '\LookupSegment',
      'LookupSegmentRequest' => '\LookupSegmentRequest',
      'LookupSegmentResponse' => '\LookupSegmentResponse',
      'InvalidSessionIdFault' => '\InvalidSessionIdFault',
      'UnauthorizedAccessFault' => '\UnauthorizedAccessFault',
      'NoLicenseFault' => '\NoLicenseFault',
      'TMFault' => '\TMFault',
      'RequestXmlFormatFault' => '\RequestXmlFormatFault',
      'Concordance' => '\Concordance',
      'ConcordanceRequest' => '\ConcordanceRequest',
      'ConcordanceResponse' => '\ConcordanceResponse',
      'AddOrUpdateEntry' => '\AddOrUpdateEntry',
      'AddOrUpdateEntryResponse' => '\AddOrUpdateEntryResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'http://memoq2.idisc.es:8080/memoqservices/tm?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param CreateAndPublish $parameters
     * @access public
     * @return CreateAndPublishResponse
     */
    public function CreateAndPublish(CreateAndPublish $parameters)
    {
      return $this->__soapCall('CreateAndPublish', array($parameters));
    }

    /**
     * @param ListTMs $parameters
     * @access public
     * @return ListTMsResponse
     */
    public function ListTMs(ListTMs $parameters)
    {
      return $this->__soapCall('ListTMs', array($parameters));
    }

    /**
     * @param ListTMs2 $parameters
     * @access public
     * @return ListTMs2Response
     */
    public function ListTMs2(ListTMs2 $parameters)
    {
      return $this->__soapCall('ListTMs2', array($parameters));
    }

    /**
     * @param GetTMInfo $parameters
     * @access public
     * @return GetTMInfoResponse
     */
    public function GetTMInfo(GetTMInfo $parameters)
    {
      return $this->__soapCall('GetTMInfo', array($parameters));
    }

    /**
     * @param UpdateProperties $parameters
     * @access public
     * @return UpdatePropertiesResponse
     */
    public function UpdateProperties(UpdateProperties $parameters)
    {
      return $this->__soapCall('UpdateProperties', array($parameters));
    }

    /**
     * @param DeleteTM $parameters
     * @access public
     * @return DeleteTMResponse
     */
    public function DeleteTM(DeleteTM $parameters)
    {
      return $this->__soapCall('DeleteTM', array($parameters));
    }

    /**
     * @param BeginChunkedTMXImport $parameters
     * @access public
     * @return BeginChunkedTMXImportResponse
     */
    public function BeginChunkedTMXImport(BeginChunkedTMXImport $parameters)
    {
      return $this->__soapCall('BeginChunkedTMXImport', array($parameters));
    }

    /**
     * @param AddNextTMXChunk $parameters
     * @access public
     * @return AddNextTMXChunkResponse
     */
    public function AddNextTMXChunk(AddNextTMXChunk $parameters)
    {
      return $this->__soapCall('AddNextTMXChunk', array($parameters));
    }

    /**
     * @param EndChunkedTMXImport $parameters
     * @access public
     * @return EndChunkedTMXImportResponse
     */
    public function EndChunkedTMXImport(EndChunkedTMXImport $parameters)
    {
      return $this->__soapCall('EndChunkedTMXImport', array($parameters));
    }

    /**
     * @param BeginChunkedTMXExport $parameters
     * @access public
     * @return BeginChunkedTMXExportResponse
     */
    public function BeginChunkedTMXExport(BeginChunkedTMXExport $parameters)
    {
      return $this->__soapCall('BeginChunkedTMXExport', array($parameters));
    }

    /**
     * @param GetNextTMXChunk $parameters
     * @access public
     * @return GetNextTMXChunkResponse
     */
    public function GetNextTMXChunk(GetNextTMXChunk $parameters)
    {
      return $this->__soapCall('GetNextTMXChunk', array($parameters));
    }

    /**
     * @param EndChunkedTMXExport $parameters
     * @access public
     * @return EndChunkedTMXExportResponse
     */
    public function EndChunkedTMXExport(EndChunkedTMXExport $parameters)
    {
      return $this->__soapCall('EndChunkedTMXExport', array($parameters));
    }

    /**
     * @param LookupSegment $parameters
     * @access public
     * @return LookupSegmentResponse
     */
    public function LookupSegment(LookupSegment $parameters)
    {
      return $this->__soapCall('LookupSegment', array($parameters));
    }

    /**
     * @param Concordance $parameters
     * @access public
     * @return ConcordanceResponse
     */
    public function Concordance(Concordance $parameters)
    {
      return $this->__soapCall('Concordance', array($parameters));
    }

    /**
     * @param AddOrUpdateEntry $parameters
     * @access public
     * @return AddOrUpdateEntryResponse
     */
    public function AddOrUpdateEntry(AddOrUpdateEntry $parameters)
    {
      return $this->__soapCall('AddOrUpdateEntry', array($parameters));
    }

}
