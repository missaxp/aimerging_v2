<?php

class ListTMs2
{

    /**
     * @var TMListFilter $filter
     * @access public
     */
    public $filter = null;

    /**
     * @param TMListFilter $filter
     * @access public
     */
    public function __construct($filter)
    {
      $this->filter = $filter;
    }

}
