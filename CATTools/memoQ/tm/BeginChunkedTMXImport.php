<?php

class BeginChunkedTMXImport
{

    /**
     * @var guid $tmGuid
     * @access public
     */
    public $tmGuid = null;

    /**
     * @param guid $tmGuid
     * @access public
     */
    public function __construct($tmGuid)
    {
      $this->tmGuid = $tmGuid;
    }

}
