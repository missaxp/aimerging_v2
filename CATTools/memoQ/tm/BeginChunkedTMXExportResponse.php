<?php

class BeginChunkedTMXExportResponse
{

    /**
     * @var guid $BeginChunkedTMXExportResult
     * @access public
     */
    public $BeginChunkedTMXExportResult = null;

    /**
     * @param guid $BeginChunkedTMXExportResult
     * @access public
     */
    public function __construct($BeginChunkedTMXExportResult)
    {
      $this->BeginChunkedTMXExportResult = $BeginChunkedTMXExportResult;
    }

}
