<?php

class GetNextTMXChunkResponse
{

    /**
     * @var base64Binary $GetNextTMXChunkResult
     * @access public
     */
    public $GetNextTMXChunkResult = null;

    /**
     * @param base64Binary $GetNextTMXChunkResult
     * @access public
     */
    public function __construct($GetNextTMXChunkResult)
    {
      $this->GetNextTMXChunkResult = $GetNextTMXChunkResult;
    }

}
