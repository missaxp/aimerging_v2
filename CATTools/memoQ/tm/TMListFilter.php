<?php

include_once(BaseDir.'/CATTools/memoQ/common/ResourceListFilter.php');

class TMListFilter extends ResourceListFilter
{

    /**
     * @var boolean $LazyLanguageCheck
     * @access public
     */
    public $LazyLanguageCheck = null;

    /**
     * @var string $SourceLangCode
     * @access public
     */
    public $SourceLangCode = null;

    /**
     * @var string $TargetLangCode
     * @access public
     */
    public $TargetLangCode = null;

    /**
     * @param boolean $LazyLanguageCheck
     * @access public
     */
    public function __construct($LazyLanguageCheck)
    {
      parent::__construct();
      $this->LazyLanguageCheck = $LazyLanguageCheck;
    }

}
