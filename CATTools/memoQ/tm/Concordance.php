<?php

class Concordance
{

    /**
     * @var string $sessionId
     * @access public
     */
    public $sessionId = null;

    /**
     * @var string $searchExpression
     * @access public
     */
    public $searchExpression = null;

    /**
     * @var guid $tmGuid
     * @access public
     */
    public $tmGuid = null;

    /**
     * @var ConcordanceRequest $options
     * @access public
     */
    public $options = null;

    /**
     * @param string $sessionId
     * @param string $searchExpression
     * @param guid $tmGuid
     * @param ConcordanceRequest $options
     * @access public
     */
    public function __construct($sessionId, $searchExpression, $tmGuid, $options)
    {
      $this->sessionId = $sessionId;
      $this->searchExpression = $searchExpression;
      $this->tmGuid = $tmGuid;
      $this->options = $options;
    }

}
