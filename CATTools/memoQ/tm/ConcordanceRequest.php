<?php

class ConcordanceRequest
{

    /**
     * @var boolean $CaseSensitive
     * @access public
     */
    public $CaseSensitive = null;

    /**
     * @var boolean $NumericEquivalence
     * @access public
     */
    public $NumericEquivalence = null;

    /**
     * @var int $ResultsLimit
     * @access public
     */
    public $ResultsLimit = null;

    /**
     * @var boolean $ReverseLookup
     * @access public
     */
    public $ReverseLookup = null;

    /**
     * @param boolean $CaseSensitive
     * @param boolean $NumericEquivalence
     * @param int $ResultsLimit
     * @param boolean $ReverseLookup
     * @access public
     */
    public function __construct($CaseSensitive, $NumericEquivalence, $ResultsLimit, $ReverseLookup)
    {
      $this->CaseSensitive = $CaseSensitive;
      $this->NumericEquivalence = $NumericEquivalence;
      $this->ResultsLimit = $ResultsLimit;
      $this->ReverseLookup = $ReverseLookup;
    }

}
