<?php

class ListTMs2Response
{

    /**
     * @var TMInfo[] $ListTMs2Result
     * @access public
     */
    public $ListTMs2Result = null;

    /**
     * @param TMInfo[] $ListTMs2Result
     * @access public
     */
    public function __construct($ListTMs2Result)
    {
      $this->ListTMs2Result = $ListTMs2Result;
    }

}
