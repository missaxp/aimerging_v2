<?php

class GetTMInfoResponse
{

    /**
     * @var TMInfo $GetTMInfoResult
     * @access public
     */
    public $GetTMInfoResult = null;

    /**
     * @param TMInfo $GetTMInfoResult
     * @access public
     */
    public function __construct($GetTMInfoResult)
    {
      $this->GetTMInfoResult = $GetTMInfoResult;
    }

}
