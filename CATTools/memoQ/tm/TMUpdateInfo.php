<?php

include_once(BaseDir.'/CATTools/memoQ/common/HeavyResourceUpdateInfo.php');

class TMUpdateInfo extends HeavyResourceUpdateInfo
{

    /**
     * @var TMOptimizationPreference $OptimizationPreference
     * @access public
     */
    public $OptimizationPreference = null;

    /**
     * @var boolean $StoreDocumentFullPath
     * @access public
     */
    public $StoreDocumentFullPath = null;

    /**
     * @var boolean $StoreDocumentName
     * @access public
     */
    public $StoreDocumentName = null;
    
    /**
     * @var string $Name
     * @access public
     */
    public $Name = null;
    
    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;
    
    /**
     * @var string $Client
     * @access public
     */
    public $Client = null;
    
    /**
     * @var string  $Domain
     * @access public
     */
    public $Domain = null;

    /**
     * @param guid $Guid
     * @param boolean $Readonly
     * @access public
     */
    public function __construct(/*$Guid, $Readonly*/$OptimizationPreference, $StoreDocumentFullPath, $StoreDocumentName, $Name, $Description, $Client, $Domain)
    {
//       parent::__construct($Guid, $Readonly);
    	$this->OptimizationPreference = $OptimizationPreference;
    	$this->StoreDocumentFullPath = $StoreDocumentFullPath;
    	$this->StoreDocumentName = $StoreDocumentName;
    	$this->Name = $Name;
    	$this->Description = $Description;
    	$this->Client = $Client;
    	$this->Domain = $Domain;
    }

}
