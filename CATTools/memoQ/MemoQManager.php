<?php

namespace CATTools\memoQ;

/**
 *
 * @author zroque
 * @version 1.0
 *          created on 28/03/2019
 */
class MemoQManager {
	private $errors = array();
	public const AISYSTEM_USER_GUID = "9af1b883-b25b-e911-946c-00155d64c92a";

	public function __construct(){

	}

	/**
	 *
	 * @return array
	 */
	public function getErrors(){

		return $this->errors;
	}

	/**
	 *
	 * @param
	 *        	String
	 */
	public function addError($error){

		$this->errors[] = $error;
	}

	public function hasErrors(){
		return (count($this->errors) > 0);
	}
}

