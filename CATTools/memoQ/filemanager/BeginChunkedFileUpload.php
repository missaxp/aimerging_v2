<?php

class BeginChunkedFileUpload
{

    /**
     * @var string $fileName
     * @access public
     */
    public $fileName = null;

    /**
     * @var boolean $isZipped
     * @access public
     */
    public $isZipped = null;

    /**
     * @param string $fileName
     * @param boolean $isZipped
     * @access public
     */
    public function __construct($fileName, $isZipped)
    {
      $this->fileName = $fileName;
      $this->isZipped = $isZipped;
    }

}
