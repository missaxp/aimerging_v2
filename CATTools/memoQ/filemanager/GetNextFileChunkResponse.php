<?php

class GetNextFileChunkResponse
{

    /**
     * @var base64Binary $GetNextFileChunkResult
     * @access public
     */
    public $GetNextFileChunkResult = null;

    /**
     * @param base64Binary $GetNextFileChunkResult
     * @access public
     */
    public function __construct($GetNextFileChunkResult)
    {
      $this->GetNextFileChunkResult = $GetNextFileChunkResult;
    }

}
