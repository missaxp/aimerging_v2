<?php

class BeginChunkedFileDownload
{

    /**
     * @var guid $fileGuid
     * @access public
     */
    public $fileGuid = null;

    /**
     * @var boolean $zip
     * @access public
     */
    public $zip = null;

    /**
     * @param guid $fileGuid
     * @param boolean $zip
     * @access public
     */
    public function __construct($fileGuid, $zip)
    {
      $this->fileGuid = $fileGuid;
      $this->zip = $zip;
    }

}
