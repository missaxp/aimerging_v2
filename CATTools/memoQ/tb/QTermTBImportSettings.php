<?php

class QTermTBImportSettings
{

    /**
     * @var boolean $ExpandPickLists
     * @access public
     */
    public $ExpandPickLists = null;

    /**
     * @var boolean $ImportSpacesAsUnderscoreInFieldNames
     * @access public
     */
    public $ImportSpacesAsUnderscoreInFieldNames = null;

    /**
     * @var guid $MultitermZipFileId
     * @access public
     */
    public $MultitermZipFileId = null;

    /**
     * @param boolean $ExpandPickLists
     * @param boolean $ImportSpacesAsUnderscoreInFieldNames
     * @param guid $MultitermZipFileId
     * @access public
     */
    public function __construct($ExpandPickLists, $ImportSpacesAsUnderscoreInFieldNames, $MultitermZipFileId)
    {
      $this->ExpandPickLists = $ExpandPickLists;
      $this->ImportSpacesAsUnderscoreInFieldNames = $ImportSpacesAsUnderscoreInFieldNames;
      $this->MultitermZipFileId = $MultitermZipFileId;
    }

}
