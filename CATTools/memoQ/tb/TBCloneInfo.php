<?php

class TBCloneInfo
{

    /**
     * @var guid $SourceTBGuid
     * @access public
     */
    public $SourceTBGuid = null;

    /**
     * @var string $TargetTBName
     * @access public
     */
    public $TargetTBName = null;

    /**
     * @param guid $SourceTBGuid
     * @access public
     */
    public function __construct($SourceTBGuid)
    {
      $this->SourceTBGuid = $SourceTBGuid;
    }

}
