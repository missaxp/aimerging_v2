<?php

class GetNextCSVChunkResponse
{

    /**
     * @var base64Binary $GetNextCSVChunkResult
     * @access public
     */
    public $GetNextCSVChunkResult = null;

    /**
     * @param base64Binary $GetNextCSVChunkResult
     * @access public
     */
    public function __construct($GetNextCSVChunkResult)
    {
      $this->GetNextCSVChunkResult = $GetNextCSVChunkResult;
    }

}
