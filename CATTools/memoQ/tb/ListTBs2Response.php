<?php

class ListTBs2Response
{

    /**
     * @var TBInfo[] $ListTBs2Result
     * @access public
     */
    public $ListTBs2Result = null;

    /**
     * @param TBInfo[] $ListTBs2Result
     * @access public
     */
    public function __construct($ListTBs2Result)
    {
      $this->ListTBs2Result = $ListTBs2Result;
    }

}
