<?php

class CSVImportSettings
{

    /**
     * @var string $DefaultDomain
     * @access public
     */
    public $DefaultDomain = null;

    /**
     * @var string $DefaultSubject
     * @access public
     */
    public $DefaultSubject = null;

    /**
     * @var string $DefaultUserName
     * @access public
     */
    public $DefaultUserName = null;

    /**
     * @var boolean $UpdateBasedOnEntryIDs
     * @access public
     */
    public $UpdateBasedOnEntryIDs = null;

    /**
     * @param boolean $UpdateBasedOnEntryIDs
     * @access public
     */
    public function __construct($UpdateBasedOnEntryIDs)
    {
      $this->UpdateBasedOnEntryIDs = $UpdateBasedOnEntryIDs;
    }

}
