<?php

class ListTBsResponse
{

    /**
     * @var TBInfo[] $ListTBsResult
     * @access public
     */
    public $ListTBsResult = null;

    /**
     * @param TBInfo[] $ListTBsResult
     * @access public
     */
    public function __construct($ListTBsResult)
    {
      $this->ListTBsResult = $ListTBsResult;
    }

}
