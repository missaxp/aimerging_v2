<?php

class BeginChunkedMultiTermExportResponse
{

    /**
     * @var guid $BeginChunkedMultiTermExportResult
     * @access public
     */
    public $BeginChunkedMultiTermExportResult = null;

    /**
     * @var string $xdt
     * @access public
     */
    public $xdt = null;

    /**
     * @var string $xdl
     * @access public
     */
    public $xdl = null;

    /**
     * @param guid $BeginChunkedMultiTermExportResult
     * @param string $xdt
     * @param string $xdl
     * @access public
     */
    public function __construct($BeginChunkedMultiTermExportResult, $xdt, $xdl)
    {
      $this->BeginChunkedMultiTermExportResult = $BeginChunkedMultiTermExportResult;
      $this->xdt = $xdt;
      $this->xdl = $xdl;
    }

}
