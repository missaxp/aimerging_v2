<?php

class BeginChunkedCSVImportResponse
{

    /**
     * @var guid $BeginChunkedCSVImportResult
     * @access public
     */
    public $BeginChunkedCSVImportResult = null;

    /**
     * @param guid $BeginChunkedCSVImportResult
     * @access public
     */
    public function __construct($BeginChunkedCSVImportResult)
    {
      $this->BeginChunkedCSVImportResult = $BeginChunkedCSVImportResult;
    }

}
