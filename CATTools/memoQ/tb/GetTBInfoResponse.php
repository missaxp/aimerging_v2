<?php

class GetTBInfoResponse
{

    /**
     * @var TBInfo $GetTBInfoResult
     * @access public
     */
    public $GetTBInfoResult = null;

    /**
     * @param TBInfo $GetTBInfoResult
     * @access public
     */
    public function __construct($GetTBInfoResult)
    {
      $this->GetTBInfoResult = $GetTBInfoResult;
    }

}
