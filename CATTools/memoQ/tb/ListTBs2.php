<?php

class ListTBs2
{

    /**
     * @var TBFilter $tbFilter
     * @access public
     */
    public $tbFilter = null;

    /**
     * @param TBFilter $tbFilter
     * @access public
     */
    public function __construct($tbFilter)
    {
      $this->tbFilter = $tbFilter;
    }

}
