<?php

class EndChunkedCSVImportResponse
{

    /**
     * @var CSVImportResult $EndChunkedCSVImportResult
     * @access public
     */
    public $EndChunkedCSVImportResult = null;

    /**
     * @param CSVImportResult $EndChunkedCSVImportResult
     * @access public
     */
    public function __construct($EndChunkedCSVImportResult)
    {
      $this->EndChunkedCSVImportResult = $EndChunkedCSVImportResult;
    }

}
