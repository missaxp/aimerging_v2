<?php

include_once(BaseDir.'/CATTools/memoQ/common/ResourceListFilter.php');

class TBFilter extends ResourceListFilter
{

    /**
     * @var TBFilterLangMode $LangMode
     * @access public
     */
    public $LangMode = null;

    /**
     * @var string[] $LanguageCodes
     * @access public
     */
    public $LanguageCodes = null;

    /**
     * @param TBFilterLangMode $LangMode
     * @access public
     */
    public function __construct($LangMode)
    {
      parent::__construct();
      $this->LangMode = $LangMode;
    }

}
