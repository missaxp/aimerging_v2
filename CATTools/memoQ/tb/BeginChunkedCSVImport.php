<?php

class BeginChunkedCSVImport
{

    /**
     * @var guid $tbGuid
     * @access public
     */
    public $tbGuid = null;

    /**
     * @var CSVImportSettings $importSettings
     * @access public
     */
    public $importSettings = null;

    /**
     * @param guid $tbGuid
     * @param CSVImportSettings $importSettings
     * @access public
     */
    public function __construct($tbGuid, $importSettings)
    {
      $this->tbGuid = $tbGuid;
      $this->importSettings = $importSettings;
    }

}
