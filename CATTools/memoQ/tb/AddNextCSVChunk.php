<?php

class AddNextCSVChunk
{

    /**
     * @var guid $sessionId
     * @access public
     */
    public $sessionId = null;

    /**
     * @var base64Binary $data
     * @access public
     */
    public $data = null;

    /**
     * @param guid $sessionId
     * @param base64Binary $data
     * @access public
     */
    public function __construct($sessionId, $data)
    {
      $this->sessionId = $sessionId;
      $this->data = $data;
    }

}
