<?php

class CloneTBResponse
{

    /**
     * @var guid $CloneTBResult
     * @access public
     */
    public $CloneTBResult = null;

    /**
     * @param guid $CloneTBResult
     * @access public
     */
    public function __construct($CloneTBResult)
    {
      $this->CloneTBResult = $CloneTBResult;
    }

}
