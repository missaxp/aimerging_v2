<?php

include_once(BaseDir.'/CATTools/memoQ/common/TBCaseSensitivity.php');
include_once(BaseDir.'/CATTools/memoQ/common/TBMatching.php');
include_once(BaseDir.'/CATTools/memoQ/common/CreateAndPublish.php');
include_once(BaseDir.'/CATTools/memoQ/common/TBInfo.php');
include_once(BaseDir.'/CATTools/memoQ/common/HeavyResourceInfo.php');
include_once(BaseDir.'/CATTools/memoQ/common/ResourceInfo.php');
include_once(BaseDir.'/CATTools/memoQ/common/TBNewTermDefaultForLanguage.php');
include_once(BaseDir.'/CATTools/memoQ/common/CreateAndPublishResponse.php');
include_once(BaseDir.'/CATTools/memoQ/common/UnexpectedFault.php');
include_once(BaseDir.'/CATTools/memoQ/common/GenericFault.php');
include_once(BaseDir.'/CATTools/memoQ/tb/CloneTB.php');
include_once(BaseDir.'/CATTools/memoQ/tb/TBCloneInfo.php');
include_once(BaseDir.'/CATTools/memoQ/tb/CloneTBResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tb/ListTBs.php');
include_once(BaseDir.'/CATTools/memoQ/tb/ListTBsResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tb/ListTBs2.php');
include_once(BaseDir.'/CATTools/memoQ/tb/TBFilter.php');
include_once(BaseDir.'/CATTools/memoQ/common/ResourceListFilter.php');
include_once(BaseDir.'/CATTools/memoQ/tb/TBFilterLangMode.php');
include_once(BaseDir.'/CATTools/memoQ/tb/ListTBs2Response.php');
include_once(BaseDir.'/CATTools/memoQ/tb/GetTBInfo.php');
include_once(BaseDir.'/CATTools/memoQ/tb/GetTBInfoResponse.php');
include_once(BaseDir.'/CATTools/memoQ/common/UpdateProperties.php');
include_once(BaseDir.'/CATTools/memoQ/tb/TBUpdateInfo.php');
include_once(BaseDir.'/CATTools/memoQ/common/HeavyResourceUpdateInfo.php');
include_once(BaseDir.'/CATTools/memoQ/common/ResourceUpdateInfo.php');
include_once(BaseDir.'/CATTools/memoQ/common/UpdatePropertiesResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tb/DeleteTB.php');
include_once(BaseDir.'/CATTools/memoQ/tb/DeleteTBResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tb/ConvertToQTerm.php');
include_once(BaseDir.'/CATTools/memoQ/tb/ConvertToQTermResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tb/BeginChunkedCSVImport.php');
include_once(BaseDir.'/CATTools/memoQ/tb/CSVImportSettings.php');
include_once(BaseDir.'/CATTools/memoQ/tb/BeginChunkedCSVImportResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tb/AddNextCSVChunk.php');
include_once(BaseDir.'/CATTools/memoQ/tb/AddNextCSVChunkResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tb/EndChunkedCSVImport.php');
include_once(BaseDir.'/CATTools/memoQ/tb/EndChunkedCSVImportResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tb/CSVImportResult.php');
include_once(BaseDir.'/CATTools/memoQ/tb/BeginChunkedCSVExport.php');
include_once(BaseDir.'/CATTools/memoQ/tb/BeginChunkedCSVExportResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tb/BeginChunkedMultiTermExport.php');
include_once(BaseDir.'/CATTools/memoQ/tb/BeginChunkedMultiTermExportResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tb/GetNextExportChunk.php');
include_once(BaseDir.'/CATTools/memoQ/tb/GetNextExportChunkResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tb/EndChunkedExport.php');
include_once(BaseDir.'/CATTools/memoQ/tb/EndChunkedExportResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tb/GetNextCSVChunk.php');
include_once(BaseDir.'/CATTools/memoQ/tb/GetNextCSVChunkResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tb/EndChunkedCSVExport.php');
include_once(BaseDir.'/CATTools/memoQ/tb/EndChunkedCSVExportResponse.php');
include_once(BaseDir.'/CATTools/memoQ/tb/CreateQTermTBFromMultiterm.php');
include_once(BaseDir.'/CATTools/memoQ/tb/QTermTBImportSettings.php');
include_once(BaseDir.'/CATTools/memoQ/tb/CreateQTermTBFromMultitermResponse.php');

class TBService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'CreateAndPublish' => '\CreateAndPublish',
      'TBInfo' => '\TBInfo',
      'HeavyResourceInfo' => '\HeavyResourceInfo',
      'ResourceInfo' => '\ResourceInfo',
      'TBNewTermDefaultForLanguage' => '\TBNewTermDefaultForLanguage',
      'CreateAndPublishResponse' => '\CreateAndPublishResponse',
      'UnexpectedFault' => '\UnexpectedFault',
      'GenericFault' => '\GenericFault',
      'CloneTB' => '\CloneTB',
      'TBCloneInfo' => '\TBCloneInfo',
      'CloneTBResponse' => '\CloneTBResponse',
      'ListTBs' => '\ListTBs',
      'ListTBsResponse' => '\ListTBsResponse',
      'ListTBs2' => '\ListTBs2',
      'TBFilter' => '\TBFilter',
      'ResourceListFilter' => '\ResourceListFilter',
      'ListTBs2Response' => '\ListTBs2Response',
      'GetTBInfo' => '\GetTBInfo',
      'GetTBInfoResponse' => '\GetTBInfoResponse',
      'UpdateProperties' => '\UpdateProperties',
      'TBUpdateInfo' => '\TBUpdateInfo',
      'HeavyResourceUpdateInfo' => '\HeavyResourceUpdateInfo',
      'ResourceUpdateInfo' => '\ResourceUpdateInfo',
      'UpdatePropertiesResponse' => '\UpdatePropertiesResponse',
      'DeleteTB' => '\DeleteTB',
      'DeleteTBResponse' => '\DeleteTBResponse',
      'ConvertToQTerm' => '\ConvertToQTerm',
      'ConvertToQTermResponse' => '\ConvertToQTermResponse',
      'BeginChunkedCSVImport' => '\BeginChunkedCSVImport',
      'CSVImportSettings' => '\CSVImportSettings',
      'BeginChunkedCSVImportResponse' => '\BeginChunkedCSVImportResponse',
      'AddNextCSVChunk' => '\AddNextCSVChunk',
      'AddNextCSVChunkResponse' => '\AddNextCSVChunkResponse',
      'EndChunkedCSVImport' => '\EndChunkedCSVImport',
      'EndChunkedCSVImportResponse' => '\EndChunkedCSVImportResponse',
      'CSVImportResult' => '\CSVImportResult',
      'BeginChunkedCSVExport' => '\BeginChunkedCSVExport',
      'BeginChunkedCSVExportResponse' => '\BeginChunkedCSVExportResponse',
      'BeginChunkedMultiTermExport' => '\BeginChunkedMultiTermExport',
      'BeginChunkedMultiTermExportResponse' => '\BeginChunkedMultiTermExportResponse',
      'GetNextExportChunk' => '\GetNextExportChunk',
      'GetNextExportChunkResponse' => '\GetNextExportChunkResponse',
      'EndChunkedExport' => '\EndChunkedExport',
      'EndChunkedExportResponse' => '\EndChunkedExportResponse',
      'GetNextCSVChunk' => '\GetNextCSVChunk',
      'GetNextCSVChunkResponse' => '\GetNextCSVChunkResponse',
      'EndChunkedCSVExport' => '\EndChunkedCSVExport',
      'EndChunkedCSVExportResponse' => '\EndChunkedCSVExportResponse',
      'CreateQTermTBFromMultiterm' => '\CreateQTermTBFromMultiterm',
      'QTermTBImportSettings' => '\QTermTBImportSettings',
      'CreateQTermTBFromMultitermResponse' => '\CreateQTermTBFromMultitermResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'http://memoq2.idisc.es:8080/memoqservices/tb?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param CreateAndPublish $parameters
     * @access public
     * @return CreateAndPublishResponse
     */
    public function CreateAndPublish(CreateAndPublish $parameters)
    {
      return $this->__soapCall('CreateAndPublish', array($parameters));
    }

    /**
     * @param CloneTB $parameters
     * @access public
     * @return CloneTBResponse
     */
    public function CloneTB(CloneTB $parameters)
    {
      return $this->__soapCall('CloneTB', array($parameters));
    }

    /**
     * @param ListTBs $parameters
     * @access public
     * @return ListTBsResponse
     */
    public function ListTBs(ListTBs $parameters)
    {
      return $this->__soapCall('ListTBs', array($parameters));
    }

    /**
     * @param ListTBs2 $parameters
     * @access public
     * @return ListTBs2Response
     */
    public function ListTBs2(ListTBs2 $parameters)
    {
      return $this->__soapCall('ListTBs2', array($parameters));
    }

    /**
     * @param GetTBInfo $parameters
     * @access public
     * @return GetTBInfoResponse
     */
    public function GetTBInfo(GetTBInfo $parameters)
    {
      return $this->__soapCall('GetTBInfo', array($parameters));
    }

    /**
     * @param UpdateProperties $parameters
     * @access public
     * @return UpdatePropertiesResponse
     */
    public function UpdateProperties(UpdateProperties $parameters)
    {
      return $this->__soapCall('UpdateProperties', array($parameters));
    }

    /**
     * @param DeleteTB $parameters
     * @access public
     * @return DeleteTBResponse
     */
    public function DeleteTB(DeleteTB $parameters)
    {
      return $this->__soapCall('DeleteTB', array($parameters));
    }

    /**
     * @param ConvertToQTerm $parameters
     * @access public
     * @return ConvertToQTermResponse
     */
    public function ConvertToQTerm(ConvertToQTerm $parameters)
    {
      return $this->__soapCall('ConvertToQTerm', array($parameters));
    }

    /**
     * @param BeginChunkedCSVImport $parameters
     * @access public
     * @return BeginChunkedCSVImportResponse
     */
    public function BeginChunkedCSVImport(BeginChunkedCSVImport $parameters)
    {
      return $this->__soapCall('BeginChunkedCSVImport', array($parameters));
    }

    /**
     * @param AddNextCSVChunk $parameters
     * @access public
     * @return AddNextCSVChunkResponse
     */
    public function AddNextCSVChunk(AddNextCSVChunk $parameters)
    {
      return $this->__soapCall('AddNextCSVChunk', array($parameters));
    }

    /**
     * @param EndChunkedCSVImport $parameters
     * @access public
     * @return EndChunkedCSVImportResponse
     */
    public function EndChunkedCSVImport(EndChunkedCSVImport $parameters)
    {
      return $this->__soapCall('EndChunkedCSVImport', array($parameters));
    }

    /**
     * @param BeginChunkedCSVExport $parameters
     * @access public
     * @return BeginChunkedCSVExportResponse
     */
    public function BeginChunkedCSVExport(BeginChunkedCSVExport $parameters)
    {
      return $this->__soapCall('BeginChunkedCSVExport', array($parameters));
    }

    /**
     * @param BeginChunkedMultiTermExport $parameters
     * @access public
     * @return BeginChunkedMultiTermExportResponse
     */
    public function BeginChunkedMultiTermExport(BeginChunkedMultiTermExport $parameters)
    {
      return $this->__soapCall('BeginChunkedMultiTermExport', array($parameters));
    }

    /**
     * @param GetNextExportChunk $parameters
     * @access public
     * @return GetNextExportChunkResponse
     */
    public function GetNextExportChunk(GetNextExportChunk $parameters)
    {
      return $this->__soapCall('GetNextExportChunk', array($parameters));
    }

    /**
     * @param EndChunkedExport $parameters
     * @access public
     * @return EndChunkedExportResponse
     */
    public function EndChunkedExport(EndChunkedExport $parameters)
    {
      return $this->__soapCall('EndChunkedExport', array($parameters));
    }

    /**
     * @param GetNextCSVChunk $parameters
     * @access public
     * @return GetNextCSVChunkResponse
     */
    public function GetNextCSVChunk(GetNextCSVChunk $parameters)
    {
      return $this->__soapCall('GetNextCSVChunk', array($parameters));
    }

    /**
     * @param EndChunkedCSVExport $parameters
     * @access public
     * @return EndChunkedCSVExportResponse
     */
    public function EndChunkedCSVExport(EndChunkedCSVExport $parameters)
    {
      return $this->__soapCall('EndChunkedCSVExport', array($parameters));
    }

    /**
     * @param CreateQTermTBFromMultiterm $parameters
     * @access public
     * @return CreateQTermTBFromMultitermResponse
     */
    public function CreateQTermTBFromMultiterm(CreateQTermTBFromMultiterm $parameters)
    {
      return $this->__soapCall('CreateQTermTBFromMultiterm', array($parameters));
    }

}
