<?php

class CloneTB
{

    /**
     * @var TBCloneInfo $cloneInfo
     * @access public
     */
    public $cloneInfo = null;

    /**
     * @param TBCloneInfo $cloneInfo
     * @access public
     */
    public function __construct($cloneInfo)
    {
      $this->cloneInfo = $cloneInfo;
    }

}
