<?php

class CreateQTermTBFromMultiterm
{

    /**
     * @var TBInfo $tbInfo
     * @access public
     */
    public $tbInfo = null;

    /**
     * @var QTermTBImportSettings $settings
     * @access public
     */
    public $settings = null;

    /**
     * @param TBInfo $tbInfo
     * @param QTermTBImportSettings $settings
     * @access public
     */
    public function __construct($tbInfo, $settings)
    {
      $this->tbInfo = $tbInfo;
      $this->settings = $settings;
    }

}
