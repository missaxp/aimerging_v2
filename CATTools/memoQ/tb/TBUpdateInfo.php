<?php

include_once(BaseDir.'/CATTools/memoQ/common/HeavyResourceUpdateInfo.php');

class TBUpdateInfo extends HeavyResourceUpdateInfo
{

    /**
     * @var string[] $LanguageCodes
     * @access public
     */
    public $LanguageCodes = null;

    /**
     * @var TBNewTermDefaultForLanguage[] $NewTermDefaults
     * @access public
     */
    public $NewTermDefaults = null;

    /**
     * @param guid $Guid
     * @param boolean $Readonly
     * @access public
     */
    public function __construct($Guid, $Readonly)
    {
      parent::__construct($Guid, $Readonly);
    }

}
