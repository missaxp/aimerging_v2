<?php

class CreateQTermTBFromMultitermResponse
{

    /**
     * @var guid $CreateQTermTBFromMultitermResult
     * @access public
     */
    public $CreateQTermTBFromMultitermResult = null;

    /**
     * @param guid $CreateQTermTBFromMultitermResult
     * @access public
     */
    public function __construct($CreateQTermTBFromMultitermResult)
    {
      $this->CreateQTermTBFromMultitermResult = $CreateQTermTBFromMultitermResult;
    }

}
