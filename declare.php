<?php
namespace core;

ini_set('display_errors', 'On');
error_reporting(E_ALL);

include_once 'AI.php';
define("MAX_LOAD",6000);

/**
 * @author phidalgo
 * @version 1.0
 * created on 16 jul. 2018
 */

/**
 *
 * @var \core\setup $setup
 */
$setup = new setup();

$setup->development = false;
$setup->tms_develoment = false;
$setup->timeZone = "UTC"; //Set server WORKING timeZone.
$setup->timeZone_TMS = "Europe/Madrid";
$setup->starttime = microtime(true);
$setup->default_TMS = "idcp";
$setup->version = "1.01 First production deployment";
$setup->db_type = DBCONN_MYSQL;
$setup->mail_from = "no-reply@infodisc.es";
$setup->mail_from_name = "AI Automatic Task Manager";
$setup->maxExecutionTime = 600;
$setup->fromHTTP = isset($_SERVER["HTTP_HOST"]);
$setup->apiPrivateToken = "80058b69204c84de9163e03d642d340a66c77bff"; //sha1 generado a partir de un string aleatorio de 161 caracteres.
$setup->execution_type = "IN_QUEUE";

if($setup->development){
	$setup->mail_error_to = "mhernandez@idisc.com";
	$setup->urlDownloadManager = "http://ai.linux/api/v1/core/helper/downloadFile";
	$setup->db_user = "root";
	$setup->db_passwd = "aI@2018!";
	$setup->db_scheme = "AI";
	$setup->db_host = "192.168.100.218";
	$setup->mail_server = "cache2.idisc.es";
	$setup->repository = "/webs/ai/testTMSData/";
	$setup->webServiceDomain = "http://ai.linux/api/v1/tasks/confirm/";
	$setup->send_email_when_error = true;
	$setup->confirmTaskTimeout = 3;
	$setup->numberOfTries = 5;
	$setup->sendNotificationEmailToAdmin = false;
	$setup->maxSizeFile = 50000000;
	$setup->intervalForFiles = "P7D";
	$setup->invervalRepositoryClean = "P1DT17:00:00";
}
else{
	$setup->mail_error_to = "informatica_xalapa@idisc.com";
	$setup->urlDownloadManager = "http://ai.idisc.es/api/v1/core/helper/downloadFile";
	$setup->db_user = "ai_user";
	$setup->db_passwd = "1985!p0mod0Ro.0724";
	$setup->db_scheme = "AI";
	$setup->db_host = "ferro.iit.idisc.es";
	$setup->mail_server = "cache2.idisc.es";
	$setup->send_email_when_error = true;
	$setup->confirmTaskTimeout = 72; //En H si se quiere en minutos hay que tocar el método.
	$setup->numberOfTries = 5;
	$setup->repository = "/ai/repository/";
	$setup->webServiceDomain = "http://ai.idisc.es/api/v1/tasks/confirm/";
	$setup->sendNotificationEmailToAdmin = true;
	$setup->maxSizeFile = 50000000;
	$setup->intervalForFiles = "P7D";
    $setup->invervalRepositoryClean = "P7DT1:00:00";
}


date_default_timezone_set($setup->timeZone);
set_time_limit($setup->maxExecutionTime);
?>