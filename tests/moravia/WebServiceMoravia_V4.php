<?php
namespace harvestModule\sources\connectService;

use common\Excel;

/**
 * Moravia API Version 3 implementation.
 * We have been implementing REST calls as we have needed them.
 * Please note that the newest Moravia API version is Version 4.
 * @https://projects.moravia.com/Api/help
 * 
 * Service account is needed.
 */
use common\downloadManager;

class WebServiceMoravia_V4 {

	private $auth_token;

	const TASK_LIST_NUMBER = 1;
	const API = 'https://projects.moravia.com/Api/';

	private $clientId;
	private $clientSecret;
	private $serviceAccount;

	/**
	 *
	 * Moravia MNET Service Account for global access.
	 * @param string $clientId
	 * @param string $clientSecret
	 * @param string $serviceAccount
	 */
	public function __construct($clientId, $clientSecret, $serviceAccount){
		\basicHttpClient::setAuthenticationMethod(\basicHttpClient::$HEADER);
		$this->clientId = $clientId;
		$this->clientSecret = $clientSecret;
		$this->serviceAccount = $serviceAccount;
	}

	/**
	 * Get auth token. This token can be stored and used several times.
	 * @return mixed
	 * @return
	 */
	public function getAuthentication(){
		$post = 'grant_type=service&client_id='.$this->clientId.'&client_secret='.$this->clientSecret.'&scope=symfonie2-api&service_account='.$this->serviceAccount;
		$ch = curl_init('https://login.moravia.com/connect/token');
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$post);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		$result = curl_exec($ch);
		curl_close($ch);
		$response = json_decode($result,true);

		if(!isset($response["error"])){
			$this->auth_token = $response["access_token"];
		}

		return $response;
	}

	/**
	 * Get task amounts
	 * @param integer $taskId
	 * @return array|boolean
	 */
	public function getTaskAmounts($taskId){
		$url = self::API.'V4/TaskAmounts?$filter=TaskId%20eq%20'.$taskId;
		$Rest = new \basicHttpClient($url,$this->auth_token);
		$rsp = json_decode($Rest->createRequest(),true);
		if(is_array($rsp) && isset($rsp["value"])){
			return $rsp["value"];
		}
		return false;
	}

	/**
	 * Get wordcount analysis. Output wordcount format can be diferent for each parser.
	 * @param integer $taskId
	 * @return array|boolean
	 */
	public function getWordCountAnalyses($taskId){
		$url = self::API.'V4/WordCountAnalyses?$filter=TaskId%20eq%20'.$taskId;
		$Rest = new \basicHttpClient($url,$this->auth_token);
		$rsp = json_decode($Rest->createRequest(),true);
		if(is_array($rsp) && isset($rsp["value"])){
			return $rsp["value"];
		}
		return false;
	}

	/**
	 * Get task list given a default example filter.
	 * @return array|boolean
	 */
	public function getTasks4(){
		$filter = '$filter=(state%20eq%20Moravia.Symfonie.Data.TaskStates%27Order%27)%20and%20isArchived%20eq%20false$orderby=OrderDate%20desc';
		$url = self::API."V4/Tasks?$filter";
		$Rest = new \basicHttpClient($url,$this->auth_token);
		return json_decode($Rest->createRequest(),true);
	}
	
	/**
	 * Get task list given a default example filter.
	 * @return array|boolean
	 */
	public function getTasks(){
		$url = self::API.'V4/Tasks?$filter=(state%20eq%20Moravia.Symfonie.Data.TaskStates%27Order%27)%20and%20isArchived%20eq%20false&$count=true&subscribed=true&$orderby=OrderDate%20desc';
		$Rest = new \basicHttpClient($url,$this->auth_token);
		return json_decode($Rest->createRequest(),true);
	}

	/**
	 * Get a task
	 * @param integer $taskId
	 * @return array|boolean
	 */
	public function getTaskById($taskId){
		$url = self::API.'V4/Tasks?$filter=id%20eq%20'.$taskId;
		$Rest = new \basicHttpClient($url,$this->auth_token);
		$rsp = json_decode($Rest->createRequest(),true);
		if(is_array($rsp) && isset($rsp["value"])){
			return $rsp["value"];
		}
		return false;
	}

	/**
	 * Get task attachments given a task id
	 * @param integer $id
	 * @return array|boolean
	 */
	public function getUserattachmentsById($id){
		$url = self::API."V4/TaskAttachments?\$filter=(TaskId%20eq%20$id)";
		$Rest = new \basicHttpClient($url,$this->auth_token);
		$rsp = json_decode($Rest->createRequest(),true);
		if(is_array($rsp) && isset($rsp["value"])){
			return $rsp["value"];


		}
		return false;
	}

	/**
	 * Download attachments given a task id
	 * @param integer $id
	 * @return array|boolean
	 */
	public function downloadAttachment($url,$file_name, $download_path){
      if(!file_exists($download_path)){
            try{
                mkdir($download_path,0760,true);
            }catch (\Exception $e){
                $this->addLog("Directory creation => ".$e->getMessage(), \Functions::WARNING,json_encode($e->getTrace()));
            }
        }
		$basicHttpClient = new \basicHttpClient($url,$this->auth_token);
		return $basicHttpClient->createRequest();
	}
	
	/**
	 * Get a task
	 * @param integer $taskId
	 * @return array|boolean
	 */
	public function changeTaskAssignee($taskId, $assigneeId){
		$jsonBody = '{"Assignees": [{"UserId": '.$assigneeId.', "AssignedOn": null}]}';

		https://projects.moravia.com/Api/V4/Tasks(9801071)
		$url = self::API."V4/Tasks($taskId)";
		$bhttpc = new \basicHttpClient($url,$this->auth_token, "PATCH");
		$bhttpc->setBody($jsonBody);
		$bhttpc->setContenttype("application/json");
		$rsp = $bhttpc->createRequest(true);
		
		echo print_r($rsp, true)."<br />";
		echo "Raaaesponse Status: ".$bhttpc->getResponseStatus()."<br />";
		die();
		
		return false;
		return $basicHttpClient->createRequest();
		return $basicHttpClient->downloadFile($download_path.$file_name);

	}
	
	//usuari CNeIdISC
	//56073
	
	/**
	 * Action accept a task given task id
	 * @param integer $id
	 * @return array|boolean
	 */
	public function acceptTask($id){
		$jsonBody = '{ "taskCommand": "Accept" }';
		$url = self::API."V4/Tasks(".$id.")/Default.ExecuteTaskCommand";
		
		$bhttpc = new \basicHttpClient($url, $this->auth_token, "POST");
		$bhttpc->setBody($jsonBody);
		$bhttpc->setContenttype("application/json");
		$rsp = $bhttpc->createRequest();
		
		//Si l'ha acceptat no retorna res, però l'estat és 200.
		//Si no es pot acceptar (pq ja està acceptada, per exemple), retorna 400.
		//OJO! retorna 400 si ja esta acceptada, no sé més casos.
		return ($bhttpc->getResponseStatus()==200);
	}
	
	public function getTMSAccess($attachmentId){
		$url = Self::API."V4/TaskAttachments($attachmentId)/Default.GetTmsAccess";
		$Rest = new \basicHttpClient($url,$this->auth_token,\basicHttpClient::_POST);
		$Rest->setContenttype("application/json");
		$rsp = json_decode($Rest->createRequest(),true);
		return $rsp;
	}
}
?>