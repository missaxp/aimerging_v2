<?php

namespace tests;

use sources\Moravia_V1;


error_reporting(E_ALL);
ini_set('display_errors', '1');
include 'sources/Moravia_V1.php';

$moravia = new Moravia_V1($users);

$attachments = json_decode('{"taskattachments":[{"task_id":135509,"is_uploaded_to_tms":false,"is_completed_in_tms":false,"is_uploading":false,"path":"\\\\archive\\2014\\symfonie\\334\\135509\\_In\\135509_Cycle_2_-_Purple__PEP__es_sources.zip","file_type":"Source","checksum":"5f723b1afe822553855e927cd8e987352adfd115b7005c2d8db75a977fa168a7","mime_type":"application/octet-stream","size":776398,"created_at":"2014-06-24T12:09:50.393","updated_at":"2014-06-24T12:09:50.393","allowed_actions":[],"name":"135509_Cycle_2_-_Purple__PEP__es_sources.zip","id":355450},{"task_id":135509,"is_uploaded_to_tms":false,"is_completed_in_tms":false,"is_uploading":false,"path":"\\\\archive\\2014\\symfonie\\334\\135509\\_Ref\\135509_Cycle_2_-_Purple__PEP__es_references.zip","file_type":"TaskReference","checksum":"49ef2408aa9747ffa5bb7dd0684e9c563c4ec512ee379148d9e7c970f8194252","mime_type":"application/octet-stream","size":13790163,"created_at":"2014-06-24T12:10:01.703","updated_at":"2014-06-24T12:10:01.703","allowed_actions":[],"name":"135509_Cycle_2_-_Purple__PEP__es_references.zip","id":355460},{"task_id":135509,"is_uploaded_to_tms":false,"is_completed_in_tms":false,"is_uploading":false,"path":"\\\\archive\\2014\\symfonie\\334\\135509\\_Analysis\\analysis_VCS_14-4_C2_ES_Purple_esn.xml","file_type":"Analysis","checksum":"139aeaaacab345ad23471c16ebca4f5c74322e4bd854c83a7decdc6e1a760ce4","mime_type":"application/octet-stream","size":3040,"parser":"Passolo","created_at":"2014-06-24T12:10:02.157","updated_at":"2014-06-24T12:10:02.217","allowed_actions":[],"name":"analysis_VCS_14-4_C2_ES_Purple_esn.xml","id":355461}]}');

$files = $moravia->generateAttachments($attachments);

foreach ($files as $file){
	echo "<p>$file->getFileName()</p>";
}