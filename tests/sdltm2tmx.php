<?php


/**
 * Given a folder with sdltm files, returns the Path with the generated TMX or causes an exception.
 * $exportFilePath can be set. If not, the generated tmx will be stored on OS temporary folder with uniqueId.
 * 
 * @param String $sdlTmPath
 * @param String $exportFilePath
 * @throws Exception
 * @return string|boolean
 * @author phidalgo
 * @date   20190408
 */
function sdltm2tmx(String $sdlTmPath, String $exportFilePath = null){
	$tmxPath = ($exportFilePath!==null? $exportFilePath:sys_get_temp_dir()."/".uniqid("TM_").".tmx");
	
	//MHERNANDEZ has modified the code created by:
	//https://github.com/TomasoAlbinoni/Trados-Studio-Resource-Converter
	//To be called with command line. This is written in java.
	$execCommand = "java -jar /ai/sdltm2tmx/Converter.jar m $sdlTmPath $tmxPath 2>&1";
	
	$resp =  exec($execCommand);
	
	if($resp==="0"){
		return $tmxPath;
	}
	
	$errMsg = null;
	switch($resp){
		case 1:
			$errMsg = "$resp - IoException";
			break;
		case 2:
			$errMsg = "$resp - IncorrectParameters";
			break;
		case 3:
			$errMsg = "$resp - UnsupportedEncodingException";
			break;
		case 4:
			$errMsg = "$resp - FileNotFoundException";
			break;
		case 5:
			$errMsg = "$resp - SQLException";
			break;
		case 6:
			$errMsg = "$resp - ClassNotFoundException";
			break;
		case 7:
			$errMsg = "$resp - SourceFilesNotFound";
			break;
		default:
			$errMsg = "Unkown Error: $resp";
			break;
	}
	
	throw new Exception("Java error has ocurred:  $errMsg");
	return false;
}

echo sdltm2tmx("/webs/tms");
?>