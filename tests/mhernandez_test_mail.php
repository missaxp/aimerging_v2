<?php

ini_set('display_errors', 'On');
error_reporting(E_ALL);

/**
 * Definicion de variables entre Default, functions Ai y harvest
 */



define ( "BaseDir", "/webs/ai" );


use common\exceptions\sources\apple_liox\MailException;
use core\AI;
use \harvestModule\sources\AppleLiox;
use \harvestModule\sources\connectService\AppleLioxConnect;
use \harvestModule\sources\sourceComplements\WorldServerProject;
use \common\exceptions\sources\apple_liox\WorldServerException;

use \harvestModule\sources\connectService\AppleLioxMail;
use \model\APIUser;
use model\Source;


require_once BaseDir . "/model/APIUser.php";
require_once BaseDir . "/model/Source.php";
require_once BaseDir . "/harvestModule/sources/AppleLiox.php";
require_once BaseDir . "/harvestModule/sources/connectService/AppleLioxMail.php";
require_once BaseDir . "/harvestModule/sources/connectService/AppleLioxConnect.php";
include_once BaseDir . '/declare.php';


//require BaseDir . "/harvestModule/sources/sourceComplements/WorldServerProject.php";

//require_once BaseDir.'/declare.php';


global $setup;
$ai = AI::getInstance($setup);

$ai->init();



/*

$user = new APIUser();
$user->setPassWord("Apple@2017");
$user->setUserName("rmteam@idisc.com");
$user->setIdApiUser(12);
$users = array($user);

$source = new Source();
$source->setSourceName(Source::APPLE_LIOX_WS);
$source->setIdSource(6);
$source->setUsers($users);
*/

$incoming_mails = glob("no_ws_found/" . "*.msg");

foreach ($incoming_mails as $mail){
    $mail_name = explode("/", $mail);
    $mail_name = end ($mail_name);
    echo "<br>**************************************************************". $mail_name .":<br>";
    //Functions::console("Analizando: $mail = ");
    try{
        $apple_mail = new AppleLioxMail($mail);
        $apple_mail_data = $apple_mail->analyzeMail();

        echo "<pre>";
        var_export($apple_mail_data["project_id"]);
        echo "</pre>";

    } catch (MailException $e){
        echo $e->getMessage();
    } catch (Exception $e){
        echo $e->getMessage();
    }
    echo "<br>";
}
