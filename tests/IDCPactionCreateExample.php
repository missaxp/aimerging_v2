<?php

/**
* @author phidalgo
* @version 1.0
* created on 14 jul. 2018
*/


include_once BaseDir. '/AI.php';
include_once BaseDir. '/dataAccess/ConnectionDB.php';
include_once BaseDir. '/behaviorModule/tms/tms.php';
include_once BaseDir. '/behaviorModule/tms/idcp.php';
include_once BaseDir. '/model/Language.php';
include_once BaseDir. '/dataAccess/dao/LanguageDAO.php';
include_once BaseDir. '/model/Analysis.php';
include_once BaseDir. '/model/Task.php';
include_once BaseDir. '/behaviorModule/actionsModule/EmailManager.php';
include_once BaseDir. '/dataAccess/dao/TaskDAO.php';

use behaviorModule\tms\idcp\idcp;
use model\Language;
use dataAccess\dao\LanguageDAO;
use behaviorModule\tms\idcp\taskStatus;
use model\Analysis;
use dataAccess\dao\TaskDAO;
use behaviorModule\tms\idcp\taskType;
use core\AI;

$setup = new stdClass();
$setup->development = true;
$setup->version = "v1.00 Alpha Release";

AI::getInstance($setup);


$taskDAO = new TaskDAO();

$task = $taskDAO->getTask("93f7731e3b53b8a85d5bf53249970ec6");


//Esto es código necesario para el ejemplo
//Esta información la recogeríamos de la ACCIÓN createTask de DB para ese proceso en cuestión.
$langdao = new LanguageDAO();
$lang = new Language();
$lang->setIdLanguage("en-US");
$lang->setLanguageName("English");

$tlangs = array();
$tlang = new Language();
$tlang->setIdLanguage("es-ES");
$tlang->setLanguageName("Spanish");
$tlangs[] = $tlang;
$tlang = new Language();
$tlang->setIdLanguage("ca-ES");
$tlang->setLanguageName("Catalan");
$tlangs[] = $tlang;


//Hasta aqui es código necesario para el ejemplo.

//Ante cualquier error en configuración de algun método del objeto, tms hará un throw new tmsException. Deberíamos capturar estos errores y que el createAction los controlara.
//Imagino que tenéis o programareis una clase de excepciones genérica o alguna manera de controlar errores. 
//He creado una clase abstracta en tms.php dodne he añadiro 1 código de error, pero si creéis oportuno podemos crear más códigos de error, siempre que fuera necesario.

//new idcp($task). 
//Hablamos que el createAction deberia tener un switch-case en el que creara el objeto TMS que estuviera configurado en el setup del AI, pero creo que lo mejor seria actuar como está descrito en el documento.
//Los módulos de Rules, actions, notifications, etc, dependen de Behavior. Por tanto creo que la clase behavior tendrá un método llamado por ejemplo tms que nos devolverá un nuevo objeto TMS.
//Este metodo BM::tms() obtentrá de AI (y AI lo obtendrá del setup de DB/configuración de la clase) el TMS a usar y hará el include si hiciera falta del fichero específico del tms y devolverá un objeto de clase tms.

//Ejemplo:
//$tms = BM->tms($task); en vez de usar $tms = new idcp($task);




$tms = new idcp($task);


//El título y la descripción, lo mismo. el createAction debería parsear title y description para ver si se han usado tags especiales %PROJECT_ID%, etc para reemplazarlos por los valores
//de task.
$tms->setTitle("prova AI4-dual-task file");
$tms->setDescription("descripció");

//Seteamos los idiomas
$tms->setSourceLanguage($lang);
$tms->setTargetLanguages($tlangs);

//Aqui deberíamos coger campo "dueDate" del createAction en formato +2 -2 por ejemplo y traducirlo a un new \DateTime() y llamar al metodo ->add() o ->remove() dias y luego pasar
//el objeto datetime al ->setDueDate.
$tms->setDueDate(new DateTime());

/*
echo "title: ".$tms->getTitle()."<br />";
echo "desc: ".$tms->getDescription()."<br />";
echo "Slang: ".$tms->getSourceLanguage()->getLanguageName()."<br />";
foreach ($tms->getTargetLanguages() as $lang){
	echo "Tlang: ".$lang->getLanguageName()."<br />";
}
*/

//$tms->setProperty() son propiedades específicas del TMS (en el Wireframe aparece el botón Add Property, seria esto.
//Estas propiedades dependen de cada tms y en este caso están específicadas en el código. Todos los tms tendrán esta estructura de propiedades, cambiarán las propiedades
//Si hay que modificar alguna cosa, ya lo haremos, de momento funciona así:
//$tms->setProperty("property_name", "value");
//Estos valores serán recolectados del crateAction y en función de si el tipo de la propiedades "Manual" o "Dinámico" pues debermos diretamente poner el valor guardado en el create action - para "manual" - o 
//deberemos coger el valor actual del objeto $task si el tipo es "Dinámico", y asociarlo a este método.
$tms->setProperty("tipus_tasca", taskType::_TR);
$tms->setProperty("estat", taskStatus::_READY);
$tms->setProperty("translator", 'PHIDALGO');
$tms->setProperty("projecte", "215154");

//Aqui seteamos el wordcount. En este caso deberíamos coger el de createAction si se cumple que está el checkbox marcado de "overwrite wordcount" y si se cumple la propriedad de 
//$task->getAnalysis()->calculateTotalWords() es menor que el valor guardado en "maxWords" (ver Wireframes o documentacion) o directamente en análisis de objeto task.
$tms->setWordCount(new Analysis());


//Get getTmsproperties nos permitirá obtener un listado de propierdades disponibles para setear. Esto irá bien para UI.
//El objeto tmsProperty tiene varios parámetros y algunos de ellos son "allowedValues", "required", etc.
//Este objeto además de tener las propierdades válidas, también guarda el valor seteado en SetProperty para cada propierdad, en el caso que esa propiedad sea permitida.
//Por ejemplo hay una propeirdad del IDCP que es "translator", esta a su vez tiene unos v alores válidos ($property->getAllowedValues()) y si el valor seteado no está
//Dentro de los valores permitidos, hará throw exception.

/*
foreach($tms->getTmsProperties() as $property){
	if($property->getValue()!==false){
		echo $property->getName().": ".$property->getValue()."<br />";
	}
}
*/

//Como esta parte no es nada que se pueda configurar desde e l create action, podria directamente cogerlo dentro de la clase mediante el objeto "task" que tiene
//Pero prefiero hacerlo asi por si en un futuro permitimos alguna configuración y/o queremos añadir algun fichero adicional mas que no esté en @task.

$tms->setFilesToTranslate($task->getFilesToTranslate());
$tms->setFilesForReference($task->getFilesForReference());

//Una vez seteada toda la configuración del create Action al TMS, damos a create. Create devuelve true o false.

var_dump($tms->create());

//Hay dos métodos llamados $tms->hasErrors(); $tms->hasWarnings() que determinan si ha habido errores o warnings. esto debería recogerlo Action y guardarlo para notifications.
var_dump($tms->getError());

var_dump($tms->getWarning());

die();

?>