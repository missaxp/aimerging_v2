<?php


use core\AI;
use model\Language;
use common\downloadManager;

/**
* @author Traductor
* @version 1.0
* created on 11/09/2018
*/
ini_set('display_errors', 'On');
error_reporting(E_ALL);


// include 'dataAccess/dbConn.php';
// $CON_STRING = "(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = cesi.iit.idisc.es)(PORT = 1521)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = idisc)))";
// $CON_USER = "webtraduc";
// $CON_PWD = "webtraduc";

// try {
// 	$base = new dbConn (DBCONN_ORACLE, $CON_USER, $CON_PWD, $CON_STRING);
// } catch (Exception $e) {
// 	echo "Error";
// }

// $areas = array();
// $sql = "select codi,descripcio,area,a_prop_area from sygesprojectes where nivell='1' and obert='A' and departament like '%T%' order by codi asc";
// $q = $base->execute($sql);
// while($q->fetch()){
// 	$areas[] = $q->CODI;
// }
// $q->close();

// var_dump($areas);

// $url = "http://ai.linux/api/v1/process/8";
// //$this->auth = $auth;
// $ch = curl_init();
// curl_setopt($ch,CURLOPT_URL,$url);
// curl_setopt($ch,CURLOPT_CUSTOMREQUEST, "GET");
// curl_setopt($ch,CURLOPT_TIMEOUT,"30");
// curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
// $output = curl_exec($ch);

// $output = json_decode($output, true);

// var_dump($output);

// $task = array();
// $task["Description"] = null;

// echo (isset($task["Description"]) && $task["Description"] != "")?$task["Description"]:"Empty";

// include 'Process.php';

// $process = new Action();

// var_dump(json_encode(get_object_vars($process)));

// $clase = new stdClass();

// $clase->idprocess = "Hola";
// $properties = get_object_vars($clase);

// var_dump($properties);

define ( "BaseDir", "/webs/ai" );
require BaseDir.'/declare.php';

global $setup;
define("FILEPATH", $setup->repository);
$ai = AI::getInstance($setup);
$ai->init();


// $id = Language::filterLanguage('vl-ES');

// Functions::console($id);

// define("FILEPATH", $setup->repository);

$dm = new downloadManager();
$rsp = $dm->setFileUrl("https://bm.oettli.com/jobworkzip/O-64917-TED-007.zip?ProjektArt=3&job=1489879")
->setAsync(true)
->setFileName("prueba.zip")
->setDownloadPath("/testAsync/")
->addHeader(array(
		'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0',
		'Accept-Encoding: gzip, deflate, br',
		'Cookie: JSESSIONID=10224AB2600CB165D44C655682593F4C'
))
->run();

var_dump($rsp);
// die(var_dump($rsp));
// echo dirname(__FILE__);