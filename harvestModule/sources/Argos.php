<?php


namespace harvestModule\sources;


use core\AI;
use core\PlancakeEmailParser;
use dataAccess\dao\TaskDAO;
use Functions;
use harvestModule\Harvest;
use harvestModule\sources\sourceComplements\ArgosConnector;
use harvestModule\sources\sourceComplements\ArgosException;
use model\Analysis;
use model\AvailableTask;
use model\DataSource;
use model\File;
use model\Source;
use model\State;
use model\Task;

require BaseDir. "/harvestModule/sources/sourceComplements/ArgosConnector.php";

class Argos extends StructureSource
{
    private $url;
    private $pathMails;

    public function __construct(array $users = array(), Source $source = null)
    {
        parent::__construct($users, $source);
        $this->url = 'https://supplier.argosmultilingual.com';
        if($this->development){
            $this->pathMails = "/webs/ai/testTMSData/";
        }else{
            $this->pathMails = "/ai/sources/argos/";
        }
    }

    public function acceptTask(Task $task)
    {
        $result = false;
        try{
            $argosConnector = new ArgosConnector($this->users[0]->getUserName(),$this->users[0]->getPassWord());
            $result = $argosConnector->acceptTask($task->getDataSource()->getTmsTaskUrl());
        }catch(ArgosException $ex){

        }

        return $result;
    }

    public function searchMails(){
        $messages = glob($this->pathMails."*.msg");
        $mails = array();

        foreach ($messages as $message) {
            $mail = new PlancakeEmailParser(file_get_contents($message));
            $subject = $mail->getSubject();

            $sections = explode(",",$subject);
            $id = "";
            if(count($sections) >= 2){
                $sections = explode(" ",trim($sections[1]));
                $id = $sections[0];
            }else{
                continue;
            }
            $mails[$id] = $message;
        }

        return $mails;
    }

    public function collectTask()
    {
        $taskDAO = new TaskDAO();
        $mails = $this->searchMails();
        foreach ($this->users as $user){
            $argosConnector = null;
            try{
                $argosConnector = new ArgosConnector($user->getUserName(),$user->getPassWord(),$this->url);
                $tasks = $argosConnector->getTasks(ArgosConnector::ASSIGNMENT,$mails);
            }catch (ArgosException $ex){
                $tasks = array();
            }

            foreach ($mails as $key => $value) {
                if(!isset($tasks[$key])){
                    Functions::console("tasks with id $key not found path: $value");
                    Functions::addLog("tasks with id $key not found path: $value", Functions::ERROR, __CLASS__);
                }
            }

            foreach ($tasks as $task){
                try{
                    $newTask = $this->createTask($task,$argosConnector,$user);
                    /**/
                    if($taskDAO->getTaskByUTID($newTask->getUniqueTaskId())->getUniqueTaskId() == null) {
                        if ($argosConnector !== null) {
                            $pathFile = "Argos/" . $newTask->getIdClientetask() . "/";
                            foreach ($newTask->getFilesForReference() as &$file) {
                                $argosConnector->downloadFile($file->getPathForDownload(), $pathFile, $newTask->getIdClientetask() . ".zip", $newTask->getTargetsLanguage()[0]->getIdLanguage());
                                if (file_exists($file->getPath())) {
                                    $file->setSuccessfulDownload(true);
                                }
                            }
                        }
                        $this->newTasks[] = $newTask;
                    }
                }catch (\Exception $ex){

                }

            }
        }

        foreach ($this->newTasks as $task){
            if(isset($mails[$task->getIdClientetask()])){
                rename($mails[$task->getIdClientetask()],$this->pathMails."processed/".$task->getIdClientetask().".msg");
            }
        }

        parent::collectTask();
        return (count($this->newTasks) > 0);
    }

    private function moveDamagedFiles(){
        $messages = glob($this->pathMails."*.msg");
        foreach ($messages as $message) {
            if(file_exists($message)){
                $messageName = str_replace($this->pathMails,"",$message);
                rename($message,$this->pathMails."damaged/".$messageName);
            }
        }
    }

    protected function createTask($taskData,$tms = null,$user = null)
    {
        $newTask = new Task();
        $newTask->construct(
            \Functions::currentDate(),
            $taskData["id"],
            $taskData["id"]." ".$taskData["client"],
            isset($taskData["instructions"])?$taskData["instructions"]:"",
            \Functions::currentDateAsObject(),
            \Functions::createFormatDate($taskData["dueDate"],"Europe/Madrid"),
            "");

        $newTask->setAssignedUser($user->getIdApiUser());
        $sourceLanguage = parent::identifySourceLanguage($taskData['source']);
        if ($sourceLanguage != null) {
            $newTask->setSourceLanguage($sourceLanguage);
        }
        $targets = parent::identifyTargetLanguage(array($taskData['target']));
        $newTask->setTargetsLanguage($targets);
        $additionalProperties = $this->collectProperties(array(), $taskData);
        $newTask->setAditionalProperties($additionalProperties);

        $dataSource = new DataSource();
        $dataSource->construct(
            $this->source->getSourceName(),
            AI::getInstance()->getSetup()->execution_type,
            Functions::currentDate(),
            Harvest::TMS,
            $this->url,
            $taskData["taskLink"],
            json_encode($taskData),
            $this->source->getIdSource(),
            $user->getIdApiUser()
        );
        $newTask->setDataSource($dataSource);

        $analysis = $this->generateAnalsis($taskData);
        if (!empty($newTask->getTargetsLanguage())) {
            $newTask->getTargetsLanguage()[0]->setAnalysis($analysis);
        }
        $newTask->setState(new State(State::COLLECTED));
        $newTask->generateUniqueTaskId();

        if(isset($taskData["linkFiles"])){
            $pathFile = "Argos/".$newTask->getIdClientetask()."/";
            $file = new File();
            $file->construct(
                $taskData["id"].".zip",
                "zip",
                "",
                "Download all as zip",
                Functions::currentDate(),
                $taskData["linkFiles"],
                "",
                Harvest::TMS,
                File::REFERENCE,
                false,
                0);

            $file->setPathForDownload($taskData["linkFiles"]);
            $file->setPath(AI::getInstance()->getSetup()->repository.$pathFile. $file->getFileName());
            $newTask->addFileForReference($file);
        }
        $newTask->setTimeStamp(Functions::currentDate());
        $newTask->generateUniqueSourceId();

        return $newTask;
    }

    public function getAvailableTask()
    {
        foreach ($this->users as $user){
            $argosConnector = new ArgosConnector($user->getUserName(),$user->getPassWord(),$this->url);
            try{
                $tasks = $argosConnector->getTasks();
            }catch (ArgosException $ex){
                $tasks = array();
            }
            foreach ($tasks as $task){
                $newTask = new AvailableTask();

                $newTask->setTitle($task["id"]." ".$task["client"]);
                $newTask->setDueDate(\Functions::createFormatDate($task["dueDate"],"Europe/Madrid")->format("Y-m-d H:i:s"));
                $newTask->setIdClientTask($task["id"]);
                $newTask->setMessage(isset($task["instructions"])?$task["instructions"]:"");

                $additionalProperties = $this->collectProperties(array(), $task);
                $newTask->setAdditionalProperties($additionalProperties);

                $sourceLanguage = parent::identifySourceLanguage($task['source']);
                if ($sourceLanguage != null) {
                    $newTask->setSourceLanguage($sourceLanguage);
                }

                $targets = parent::identifyTargetLanguage(array($task['target']));
                $newTask->setTargetsLanguage($targets);

                $this->availableTasks[] = $newTask;
            }
        }
        return $this->availableTasks;
    }

    public function collectTaskByIdClientTask($idTask)
    {
        $taskFound = false;
        foreach ($this->users as $user){
            $argosConnector = new ArgosConnector($user->getUserName(),$user->getPassWord(),$this->url);
            try{
                $tasks = $argosConnector->getTasks();
            }catch (ArgosException $ex){
                $tasks = array();
            }
            foreach ($tasks as $task){
                if($task["id"] == $idTask){
                    $newTask = $this->createTask($task,$argosConnector,$user);
                    $this->newTasks[] = $newTask;
                    $taskFound = true;
                    break;
                }
            }
            if($taskFound){
                break;
            }
        }
        parent::collectTask();
        return (count($this->newTasks) > 0);
    }

    protected function generateAnalsis(&$wordCount)
    {
        $analysis = new Analysis();
        if(isset($wordCount["wordCount"])){
            foreach ($wordCount["wordCount"] as $key => $value){
                switch ($key){
                    case '100%':
                        $analysis->setPercentage_100($value);
                        break;
                    case '95-99%':
                        $analysis->setPercentage_95($value);
                        break;
                    case '85-94%':
                        $analysis->setPercentage_85($value);
                        break;
                    case '75-84%':
                        $analysis->setPercentage_75($value);
                        break;
                    case '50-74%':
                        $analysis->setPercentage_50($value);
                        break;
                    case 'Repetitions':
                        $analysis->setRepetition($value);
                        break;
                    case 'No Match':
                        $analysis->setNotMatch($value);
                        break;
                    case 'In Context':
                        $analysis->setPercentage_101($value);
                        break;
                }
            }
        }else{
            if(isset($wordCount["Workload"]) && is_numeric($wordCount["Workload"])){
                $analysis->setMinute($wordCount["Workload"]);
            }
        }

        if(isset($wordCount["MT"]) && !empty($wordCount["MT"])){
            $analysis->setMachineTanslation($analysis->getNotMatch()+$analysis->getMachineTanslation());
            $analysis->setNotMatch(0);
            $analysis->setMachineTanslation($analysis->getPercentage_50()+$analysis->getMachineTanslation());
            $analysis->setPercentage_50(0);
        }

        $analysis->calculateTotalWords();
        $analysis->calculateWeightedWords();

        return $analysis;
    }

}
?>