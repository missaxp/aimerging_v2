<?php

/**
 * @author Ljimenez
 * @version 1.0
 * created on 26 jun. 2018
 */
namespace harvestModule\sources;

use model\Task;
use model\DataSource;
use harvestModule\sources\connectService\WebServiceMoravia_V1;
use model\File;
use model\Analysis;
use model\Source;
use dataAccess\dao\LanguageDAO;
use model\State;
use harvestModule\sources\connectService\Rest;
use Functions;

include_once BaseDir. '/model/Task.php';
include_once BaseDir. '/harvestModule/sources/connectService/Rest.php';
include_once BaseDir. '/model/File.php';
include_once BaseDir. '/model/Analysis.php';
include_once BaseDir. '/Functions.php';
include_once BaseDir. '/model/DataSource.php';
include_once BaseDir. '/harvestModule/sources/connectService/WebServiceMoravia_V1.php';
include_once BaseDir. '/dataAccess/dao/LanguageDAO.php';
include_once BaseDir. '/harvestModule/sources/StructureSource.php';


class Moravia_V1 extends StructureSource{
	
	private $url = "https://symfonie.moravia.com";
	private static $skip = "0";
	private static $top = "10";
	private static $filter = "(state_id+eq+3)";
	private $pathForDownload = 'https://symfonie.moravia.com/api/taskattachment/Download?attachmentId=';
	public const HARVESTMETHOD = "API";
	private $properties;
	
	/**
	 *
	 * @var int
	 */
	private $currentUser;
	
	public function __construct(array $users, Source $source){
		parent::__construct($users, $source);
		$this->properties = array(
				"parent_task_id"
		);
		
		if($this->development){
			Moravia_V1::$skip = "0";
			Moravia_V1::$top = "2";
			Moravia_V1::$filter = "(state_id+eq+5)";
		}
	}
	
	public function collectTask() {
		foreach ($this->users as $user){
			\Functions::console("HM::".$this->source->getSourceName()." - Collecting tasks for user: ".$user->getUserName());
			$this->currentUser++;
			$moravia = new WebServiceMoravia_V1($user);
			
			// 			$portalData = $moravia->getProjects();
			// 			$projects = $portalData["projects"];
			// 			foreach($projects as $key => $project){
			
			$tasks = $moravia->getTasks(array('$skip' => Moravia_V1::$skip,'$top' => Moravia_V1::$top,'$filter' => Moravia_V1::$filter));
			// 				$tasks = $moravia->getTasks(array("project_id" => 334,'$skip' => "0",'$top' => "1",'$filter' => "(state_id+eq+5)"));
			
			//die(var_dump($tasks));
			if($tasks!=null && $tasks!==false && count($tasks["tasks"])>0){
				$i = 0;
				$numberTasks = count($tasks["tasks"]);
				while($i < $numberTasks){
					
					$task = $tasks["tasks"][$i];
					$newTask = new Task();
					$newTask->setAssignedUser($user->getIdApiUser());
					$newTask->construct(
							Functions::currentDate(),
							isset($task["id"])? $task["id"]:"",
							isset($task["name"])? $task["id"]."_".$task["name"]:"",
							isset($task["description"])? $task["description"]:"",
							Functions::createFormatDate(isset($task["start_date"])? $task["start_date"]:\Functions::currentDate()),
							Functions::createFormatDate(isset($task["due_date"])? $task["due_date"]:\Functions::currentDate()),
							isset($task["id_project"])? $task["id_project"]:""
							);
					
					
					
					
					$sourceLanguage = $this->identifySourceLanguage((isset($task["source_language_id"])? $task["source_language_id"]:""));
					if($sourceLanguage != null){
						$newTask->setSourceLanguage($sourceLanguage);
					}
					
					$targetLanguage = $this->identifyTargetLanguage(isset($task["language_ids"])? $task["language_ids"]:array());
					$newTask->setTargetsLanguage($targetLanguage);
					
					$attachments = $moravia->getUserattachmentsById($task["id"]);
					$attachments = $attachments["taskattachments"];
					
					foreach($attachments as $key => $value){
						$extention = explode(".",$value["name"]);
						
						if(count($extention) > 1)
							$extention = $extention[count($extention)-1];
							else
								$extention = "zip";
								$file = new File();
								$file->construct(
										isset($value["name"])?$value["name"]:"file.zip",
										$extention,
										isset($value["mime_type"])?$value["mime_type"]:"",
										"",
										\Functions::currentDate(),
										isset($value["path"])?$value["path"]:"",
										isset($value["checksum"])?$value["checksum"]:"",
										Moravia_V1::HARVESTMETHOD,
										isset($value["file_type"])?$value["file_type"]:"",
										false,
										isset($value["size"])?($value["size"]):1
										);
								
								$file->setIdFile($value["id"]);
								if($file->getFileType()== File::SOURCE)
									$newTask->addFileToTranslate($file);
								else
									$newTask->addFileForReference($file);
					}
					
					$wordCount = $moravia->getWordCountById($task["id"]);
					$analysis = $this->generateAnalsis($wordCount);
					if (!empty($newTask->getTargetsLanguage())){
						$newTask->getTargetsLanguage()[0]->setAnalysis($analysis);
					}
					
					$aditionalProperties = parent::collectProperties($this->properties, $task);
					$newTask->setAditionalProperties($aditionalProperties);
					
					$dataSource = new DataSource();
					$dataSource->construct(
							$this->source->getSourceName(),
							"IN",
							\Functions::currentDate(),
							Moravia_V1::HARVESTMETHOD,
							$this->url,
							$this->url.'/#/projects/'.$task["project_id"].'/'.$task["id"],
							json_encode($task),
							$this->source->getIdSource(),
							$user->getIdApiUser()
							);
					$newTask->setDataSource($dataSource);
					$newTask->setState(new State(State::COLLECTED));
					
					$newTask->generateUniqueTaskId();
					$newTask->setTimeStamp(\Functions::currentDate());
					$newTask->generateUniqueSourceId();
					
					if($this->source->getDownload()){
						foreach ($newTask->getFilesForReference() as &$file){
							$pathForDownload = parent::generatePathForDownload($task["Id"], $this->source->getSourceName(), $newTask->getUniqueTaskId());
							$file->setPath($pathForDownload.$file->getFileName());
							$successfullDownload = $this->downloadFile($file);
							$file->setSuccessfulDownload($successfullDownload);
						}
						
						foreach ($newTask->getFilesToTranslate() as &$file){
							$pathForDownload = parent::generatePathForDownload($task["Id"], $this->source->getSourceName(), $newTask->getUniqueTaskId());
							$file->setPath($pathForDownload.$file->getFileName());
							$successfullDownload = $this->downloadFile($file);
							$file->setSuccessfulDownload($successfullDownload);
						}
					}
					
					$this->newTasks[] = $newTask;
					$i++;
				}
			}
			// 				if($this->development)
				// 					break;
				// 			}
			//var_dump($this->contador);
		}
		parent::collectTask();
		return (count($this->newTasks) > 0);
	}
	
	protected function generateAnalsis(&$wordCount){
		$weight = 0.0;
		$analysis = null;
		if(isset( $wordCount["taskamounts"][0]["prices"][0]["price_word_counts"])){
			
			$formatAnalysis = $wordCount["taskamounts"][0]["prices"][0]["price_word_counts"];
			$analysis = new Analysis();
			foreach($formatAnalysis as $data){
				$weight += $data["weight"] * $data["value"];
				switch($data["category"]){
					case "untranslated (Words)":
						$analysis->setNotMatch($analysis->getNotMatch() + $data["value"]);
						break;
					case "untranslated repeated (Words)":
						$analysis->setRepetition($analysis->getRepetition()+$data["value"]);
						break;
					case "autotrans 100% (Words)":
						$analysis->setPercentage_100($analysis->getPercentage_100()+$data["value"]);
						break;
					case "autotrans 95% - 99% (Words)":
						$analysis->setPercentage_95($analysis->getPercentage_95()+$data["value"]);
						break;
					case "autotrans 85% - 94% (Words)":
						$analysis->setPercentage_85($analysis->getPercentage_85()+$data["value"]);
						break;
					case "autotrans 75% - 84% (Words)":
						$analysis->setPercentage_75($analysis->getPercentage_75()+$data["value"]);
						break;
					case "autotrans 30% - 74% (Words)":
						$analysis->setPercentage_50($analysis->getPercentage_50()+$data["value"]);
						break;
					case "forreview (Words)":
						$analysis->setPercentage_100($analysis->getPercentage_100()+$data["value"]);
						break;
					case "100% (Words)":
						$analysis->setPercentage_100($analysis->getPercentage_100()+$data["value"]);
						break;
					case "95% - 99% (Words)":
						$analysis->setPercentage_95($analysis->getPercentage_95()+$data["value"]);
						break;
					case "85% - 94% (Words)":
						$analysis->setPercentage_85($analysis->getPercentage_85()+$data["value"]);
						break;
					case "75% - 84% (Words)":
						$analysis->setPercentage_75($analysis->getPercentage_75()+$data["value"]);
						break;
					case "50% - 74% (Words)":
						$analysis->setPercentage_50($analysis->getPercentage_50()+$data["value"]);
						break;
					case "No Match (Words)":
						$analysis->setNotMatch($analysis->getNotMatch() + $data["value"]);
						break;
					case "Cross-File Repeated (Words)":
						$analysis->setRepetition($analysis->getRepetition()+$data["value"]);
						break;
					case "Repeated (Words)":
						$analysis->setRepetition($analysis->getRepetition()+$data["value"]);
						break;
					case "Repetitions (Words)":
						$analysis->setRepetition($analysis->getRepetition()+$data["value"]);
						break;
					case "total (Words)":
						$analysis->setTotalWords($data["value"]);
						break;
					default:
						break;
				}
			}
		}
		if($analysis != null){
			$analysis->setWeightedWord($weight);
		}
		
		return $analysis;
	}
	
	public function downloadFile(File &$file, Task &$task = null){
		$result = true;
		if(!file_exists($file->getPath())){
			set_time_limit(0);
			$file->setPathForDownload($file->getSourcePath());
			$rest = new Rest($file->getSourcePath());
			$rest->setHeader('Authorization: authToken '.$this->users[$this->currentUser-1]->getToken());
			$data = $rest->createRequest();
			$pathForDownload = parent::generatePathForDownload($task->getIdClientetask(), $this->source->getSourceName(), $task->getUniqueTaskId());
			$result = $this->saveDownload($file, $pathForDownload, $data);
			
		}
		$file->setPathForDownload($file->getSourcePath());
		return $result;
	}
	
	protected function identifySourceLanguage(string $idLanguage){
		$languageDAO = new LanguageDAO();
		$language = $languageDAO->getMoraviaV1Language($idLanguage);
		if($language == null){
			parent::addLog('identifier not match => '.$idLanguage, \Functions::WARNING,"File => ".__FILE__."    Method => ".__METHOD__."    Line => ".__LINE__);
		}
		return $language;
	}
	
	protected function identifyTargetLanguage(array $idlanguages){
		$targets = array();
		$languageDAO = new LanguageDAO();
		foreach ($idlanguages as $idLanguage){
			$language = $languageDAO->getMoraviaV1Language($idLanguage);
			if($language != null){
				$targets[] = $language;
			}else{
				parent::addLog('identifier not match => '.$idLanguage, \Functions::WARNING,"File => ".__FILE__."    Method => ".__METHOD__."    Line => ".__LINE__);
			}
		}
		return $targets;
	}
}
?>