<?php

namespace harvestModule\sources;

use common\exceptions\AIException;
use core\AI;
use common\ZipFile;
use core\PlancakeEmailParser;
use Functions;
use model\Source;
use harvestModule\sources\sourceComplements\PlunetConnector;
use model\Task;
use model\AvailableTask;
use model\DataSource;
use model\Analysis;
use model\File;
use harvestModule\Harvest;
use dataAccess\dao\TaskDAO;


include BaseDir . '/harvestModule/sources/sourceComplements/PlunetConnector.php';

/**
 *
 *
 * @author Ljimenez
 * @version 1.0
 * created on 14/02/2019
 */
class NLG extends StructureSource{
    /**
     * @var PlunetConnector
     */
    private static $plunet = null;
    private $urlPlunet;
    private $pathMails;
    private static $mail_parsers = array("STRICTLY CONFIDENTIAL!","Job","DAT:","FKM:","TechPubs esES:","TechPubs_esES:","TechPubs:");

    public function __construct(array $users, Source $source){
        parent::__construct($users, $source);
        $this->urlPlunet = "https://bm.oettli.com";

        if($this->development){
            $this->pathMails = "/webs/ai/testTMSData/";
        }else{
            $this->pathMails = '/ai/sources/nlg_tasks/';
        }
    }

    public function initPlunnet($user, $password,$idUser){
        if(NLG::$plunet == null || NLG::$plunet->getIdUser() != $idUser){
            NLG::$plunet = new PlunetConnector($user,$password,$this->urlPlunet);
        }
    }

    public function clean($string){
        return preg_replace('/[[:cntrl:]]/', '', $string);
    }

        /**
        *Validates subjects in emails for processe them
        */
    public function parserMail($idTask){
        foreach (self::$mail_parsers as $parser){
            if(stripos($idTask,$parser)!== false){
                $idTask = str_replace(strtoupper($parser),"",strtoupper($idTask));
                $idTask = explode(':',$idTask)[0];
                return $idTask;
            }
        }
        return($idTask);
    }


    public function analyzeMail(){
        $messages = glob($this->pathMails."*.msg");
        $mails = array();
        foreach ($messages as $message){
            $messageName = str_replace($this->pathMails,"",$message);
            $mail = new PlancakeEmailParser(file_get_contents($message));
            $message = $mail->getHTMLBody();
            $subject = $mail->getSubject();
            $idTask = explode(" - ",$subject);
            $idTask = $idTask[0];
            $idTask = $this->parserMail($idTask);
            $idTask = trim($idTask);
            unset($mail);

            $sections = array(
                "Fee"=>'<span style="text-decoration: underline;">Fee</span>',
                "Instructions"=>'<span style="text-decoration: underline;">Special Instructions</span>',
                "Deadline"=>'<span style="text-decoration: underline;">Requested project delivery</span>',
                "Project_description"=>'<strong>Project description</strong>',
                "Language"=>'<strong>Languages</strong>',
                "Customer"=>'<strong>Customer</strong>'
            );

            $isTextPlain = false; 
            if(mb_detect_encoding($message) === 'ASCII'){
                $sections = array(
                    "Fee"=>'Fee',
                    "Instructions"=>'INSTRUCTIONS:',
                    "Deadline"=>'DEADLINE:',
                    "Project_description"=>'PROJECT DESCRIPTION:',
                    "Language"=>'LANGUAGES:',
                    "Customer"=>'CUSTOMER:'
                );
                $message = base64_decode($message);
                $isTextPlain = true;
                
            }
            
            $count = $this->searchSectionInMail($sections['Fee'],$message,$isTextPlain);
            if($count != null){
                $count = strip_tags($count,"<br>");
                $array = explode("<br>",$count);

                $keys = array(
                    "ICE"=>array("Context"),
                    "REP"=>array("Repetitions"),
                    "100%"=>array("100%"),
                    "95-99%"=>array("95-99%"),
                    "85-94%"=>array("85-94%"),
                    "75-84%"=>array("75-84%"),
                    "50-74%"=>array("50-74%"),
                    "NM"=>array("No Match","Not translated"),
                    "H"=>array("Hour(s)")
                ); 

                $analysis = array();

                foreach ($array as $value){
                    foreach ($keys as $key => $breakPoints) {
                        foreach ($breakPoints as $breakPoint){
                            if (strpos($value, $breakPoint) !== false) {
                                if (strpos($value, "Word") !== false) {
                                    $start = strpos($value, "Word");
                                    $count = substr($value, 0, $start);
                                    $analysis[$key] = trim($count);
                                    break;
                                } else {
                                    $start = strpos($value, "Hour");
                                    $count = substr($value, 0, $start);
                                    $analysis[$key] = trim($count);
                                    break;
                                }
                            }
                        }
                    }
                }
                if(isset($analysis["H"])){
                    $analysis["H"] = floatval($analysis["H"]) * 60;
                }

                $mails[$idTask]["Analysis"] = $analysis;
            }

            $section = $this->searchSectionInMail($sections['Instructions'],$message,$isTextPlain);
            $date = $this->searchSectionInMail($sections['Deadline'],$message,$isTextPlain);
            

            try{
                $date = strip_tags($date);
                $date = explode("to",$date);
                $date = end($date);
                $date = explode(" ",$date);
                array_pop($date);
                $date = implode($date," ");
                $date = trim($date);
            }catch (\Throwable $th){
                $exception = AIException::createInstanceFromThrowable($th)->construct(__METHOD__,__NAMESPACE__,$args=func_get_args());
                Functions::logException($exception->getMessage(),Functions::WARNING,__CLASS__,$exception);
            }

            $projectDescription = $this->searchSectionInMail($sections['Project_description'],$message,$isTextPlain,"<strong>");

            if($projectDescription != null){
                $mails[$idTask]["project_description"] = trim(strip_tags($projectDescription));
                $mails[$idTask]["project_description"] = $this->clean($mails[$idTask]["project_description"]);
                $mails[$idTask]["project_description"] = substr($mails[$idTask]["project_description"],1,strlen($mails[$idTask]["project_description"]));
            }

            $languages = $this->searchSectionInMail($sections['Language'],$message,$isTextPlain);

            if($languages != null){
                $languages = strip_tags($languages);
                $languages = explode('/',$languages);
                if(count($languages) === 2){
                    $sourceLanguage = $languages[0];
                    $targetLanguage = $languages[1];

                    $mails[$idTask]["sourceLanguage"] = $this->filterLanguage($sourceLanguage);
                    $mails[$idTask]["targetLanguage"] = $this->filterLanguage($targetLanguage);
                }
            }           
            if($section != null){
                if(!$this->development){
                    $mails[$idTask]["Instructions"] = $section;
                }
            }
            if($date !== null){
                $mails[$idTask]["dueDate"] = $date;
            }else{
                $mails[$idTask]["dueDate"] = "";
            }
            $customer = $this->searchSectionInMail($sections['Customer'],$message,$isTextPlain,"<strong>");
            if($customer != null){
                $mails[$idTask]["Customer"] = trim(strip_tags($customer));
                $mails[$idTask]["Customer"] = $this->clean($mails[$idTask]["Customer"]);
                if($mails[$idTask]["Customer"][0] == ':')
                    $mails[$idTask]["Customer"] = substr($mails[$idTask]["Customer"],1,strlen($mails[$idTask]["Customer"]));
            }

            $mails[$idTask]["id"] = $idTask;
            $mails[$idTask]["Message"][] =  $messageName;
        }
        return $mails;
    }

    private function searchSectionInMail($idTag,$mail,$isTextPlain,$limit = null){
        if($isTextPlain == false){
            $mail = explode("<br>",$mail);
            
            $count = 0;
            foreach($mail as $mail_line){
                if(strpos($mail_line,$idTag) !== false){
                    break;
                }
                $count++;
            }
            $mail = array_slice($mail,$count+1);
            $count = 0;
            foreach($mail as $mail_line){
                if(strpos($mail_line,"text-decoration: underline") !== false){
                    break;
                }
                $count++;
            }
            $section = array_slice($mail,0,$count);
            $section = implode('<br>',$section);
            return $section;
        }else{
            $array_mail = explode("\n",$mail);
            $count = 0;
            foreach($array_mail as $line_mail){
                if(strpos($line_mail,$idTag) !== false){
                    return $array_mail[$count+1];
                }
                $count++;
            }
            return null;
        }
    }

    public function collectTask(){
        $sessionInterrupted = false;
        $taskDAO = new TaskDAO();
        $mails = $this->analyzeMail();
        if(!empty($mails)){
            foreach($this->users as $user) {
                $this->initPlunnet($user->getUserName(),$user->getPassWord(),$user->getIdApiUser());
                if(NLG::$plunet->isConnect()){
                    $links = NLG::$plunet->filterTaskLinks(PlunetConnector::INPROCESS,$mails);
                    if($links !== null){
                        foreach($links as $link) {
                            $taskData = NLG::$plunet->getTaskData($link);
                            if (!empty($taskData)) {
                                $task = new Task();
                                if (isset($mails[$taskData["idClient"]]["Instructions"]) && $mails[$taskData["idClient"]]["Instructions"] != null) {
                                    $instruction = $mails[$taskData["idClient"]]["Instructions"];
                                } else {
                                    $instruction = ($taskData['instruction'] != null) ? $taskData['instruction'] : "";
                                }
                                if (isset($mails[$taskData["idClient"]]["Customer"])) {
                                    $taskData["Customer"] = $mails[$taskData["idClient"]]["Customer"];
                                }
                                if (isset($mails[$taskData["idClient"]]["project_description"])) {
                                    $taskData["project_description"] = $mails[$taskData["idClient"]]["project_description"];
                                }
                                if(isset($mails[$taskData["idClient"]]["sourceLanguage"])){
                                    $taskData["sourceLanguage"] = $mails[$taskData["idClient"]]["sourceLanguage"];
                                }else{
                                    $taskData["sourceLanguage"] = 'es_ES';
                                    $ex = (new AIException("Source languaje not found"))->construct(__METHOD__, __NAMESPACE__, $args = func_get_args());
                                    Functions::logException("Source languaje not found", Functions::WARNING, __CLASS__, $ex);
                                }
                                if(isset($mails[$taskData["idClient"]]["targetLanguage"])){
                                    $taskData["targetLanguage"] = $mails[$taskData["idClient"]]["targetLanguage"];
                                }
                                $task->construct(
                                    Functions::currentDate(),
                                    $taskData['idClient'],
                                    $taskData['idClient'],
                                    $instruction,
                                    Functions::createFormatDate(isset($taskData['starDate']) ? $taskData['starDate'] : "", "Europe/Madrid"),
                                    Functions::createFormatDate(isset($taskData['dueDate']) ? $taskData['dueDate'] : $mails[$taskData["idClient"]]["dueDate"], "Europe/Madrid"),
                                    ""
                                );
                                $task->setAssignedUser($user->getIdApiUser());
                                $sourceLanguage = parent::identifySourceLanguage($taskData['sourceLanguage']);
                                if ($sourceLanguage != null) {
                                    $task->setSourceLanguage($sourceLanguage);
                                }
                                if(isset($taskData['targetLanguage'])){
                                    $targets = parent::identifyTargetLanguage(array($taskData['targetLanguage']));
                                    $task->setTargetsLanguage($targets);
                                }
                                $dataSource = new DataSource();
                                $aditionalProperties = $this->collectProperties(array(), $taskData);
                                $task->setAditionalProperties($aditionalProperties);
                                $dataSource->construct(
                                    $this->source->getSourceName(),
                                    AI::getInstance()->getSetup()->execution_type,
                                    Functions::currentDate(),
                                    "TMS",
                                    $this->urlPlunet,
                                    $this->urlPlunet . $taskData['link'],
                                    $taskData['originalData'],
                                    $this->source->getIdSource(),
                                    $user->getIdApiUser()
                                );
                                $analysis = $this->generateAnalsis($mails[$task->getIdClientetask()]["Analysis"]);
                                if (!empty($task->getTargetsLanguage())) {
                                    $task->getTargetsLanguage()[0]->setAnalysis($analysis);
                                }
                                $task->setDataSource($dataSource);
                                $task->generateUniqueTaskId();

                                /*if ($taskData['link']) {
                                    $file = new File();
                                    $file->construct(
                                        $task->getIdClientetask() . ".zip",
                                        "zip",
                                        "",
                                        "File with all attachments",
                                        Functions::currentDate(),
                                        $this->urlPlunet . $taskData['link'],
                                        "",
                                        Harvest::TMS,
                                        File::REFERENCE,
                                        0,
                                        0
                                    );
                                    $file->setPathForDownload($this->urlPlunet.$taskData['link']);
                                    $file->setPath("/ai/repository/NLG/Plunet/" . $task->getIdClientetask() . "/" . $file->getFileName());
                                    $task->addFileForReference($file);
                                }*/
                                $files = $this->downloadFiles($task->getIdClientetask(), null, $taskData["linkOptional"]);
                                if($files !== null){
                                    if(!empty($files)){
                                        foreach ($files as $file){
                                            if($file->getFileType() == File::SOURCE){
                                                $task->addFileToTranslate($file);
                                            }else{
                                                $task->addFileForReference($file);
                                            }
                                        }
                                    }
                                }else{
                                    Functions::console('NLG:: Variable $file is null in the task '.$task->getIdClientetask());
                                    $ex = (new AIException("NLG:: the session has been interrupted"))->construct(__METHOD__, __NAMESPACE__, $args = func_get_args(), array(
                                        "reason" => 'Variable $file is null in the task '.$task->getIdClientetask(),
                                    ));
                                    Functions::logException($ex->getMessage(), Functions::WARNING, __CLASS__, $ex);
                                    $sessionInterrupted = true;
                                    break;
                                }
                                $task->setTimeStamp(Functions::currentDate());
                                $task->generateUniqueSourceId();
                                if($taskDAO->getTaskByUTID($task->getUniqueTaskId())->getUniqueTaskId() == null){
                                    foreach ($task->getAttachments() as &$file){
                                        $asyncHash = NLG::$plunet->downloadFile($file->getFileName(), $file->getPathForDownload(),$task->getTitle()."/");
                                        if($asyncHash != null){
                                            $taskDAO->saveTaskFileAsyncDownload(array(
                                                "asyncHash" => $asyncHash,
                                                "downloadPath" => $file->getPathForDownload(),
                                                "isDownloading" => true,
                                                "isDownloaded" => false,
                                                "filePath" => ""
                                            ));
                                            $file->setAsyncHash($asyncHash);
                                        }
                                    }
                                    $this->newTasks[] = $task;
                                }
                            }else{
                                Functions::console('Sesion interrumpida - Empty task data');
                                $ex =  (new AIException("NLG:: the session has been interrupted"))->construct(__METHOD__, __NAMESPACE__, $args = func_get_args(), array(
                                    "current_user" => $user,
                                    "reason" => "Empty task data",
                                    "link" => $link
                                ));
                                Functions::logException($ex->getMessage(), Functions::WARNING, __CLASS__, $ex);
                                $sessionInterrupted = true;
                                break;
                            }
                        }
                    }
                    else {
                        Functions::console('Sesion interrumpida - Variable $links is null');
                        $ex =  (new AIException("NLG:: the session has been interrupted"))->construct(__METHOD__, __NAMESPACE__, $args = func_get_args(), array(
                            "current_user" => $user,
                            "reason" => 'Variable $links is null',
                            "email" => $mails
                        ));
                        Functions::logException($ex->getMessage(), Functions::WARNING, __CLASS__, $ex);
                        $sessionInterrupted = true;
                        break;
                    }
                }
            }
        }

        foreach ($this->newTasks as $task){
            if(isset($mails[$task->getIdClientetask()])){
                if(@copy($this->pathMails.$mails[$task->getIdClientetask()]["Message"][0],$this->pathMails."processed/".$mails[$task->getIdClientetask()]["id"].".msg")){
                    if(isset($mails[$task->getIdClientetask()]["Message"]) && is_array($mails[$task->getIdClientetask()]["Message"])){
                        foreach ($mails[$task->getIdClientetask()]["Message"] as $message){
                            @unlink($this->pathMails.$message);
                            Functions::console("Mensaje borrado: ".$this->pathMails.$message);
                        }
                    }

                }
            }
        }

        if($sessionInterrupted === false){
            $this->moveDamagedFiles();
        }

        parent::collectTask();
        return (count($this->newTasks) > 0);
    }

    private function moveDamagedFiles(){
        $messages = glob($this->pathMails."*.msg");
        foreach ($messages as $message) {
            if(file_exists($message)){
                $messageName = str_replace($this->pathMails,"",$message);
                rename($message,$this->pathMails."damaged/".$messageName);
            }
        }
    }

    protected function generateAnalsis(&$wordCount){
        $analysis = new Analysis();
        if(is_array($wordCount)){
            foreach ($wordCount as $key => $count){
                switch ($key){
                    case "ICE":
                        $analysis->setPercentage_101($count);
                        break;
                    case "REP":
                        $analysis->setRepetition($count);
                        break;
                    case "100%":
                        $analysis->setPercentage_100($count);
                        break;
                    case "95-99%":
                        $analysis->setPercentage_95($count);
                        break;
                    case "85-94%":
                        $analysis->setPercentage_85($count);
                        break;
                    case "75-84%":
                        $analysis->setPercentage_75($count);
                        break;
                    case "50-74%":
                        $analysis->setPercentage_50($count);
                        break;
                    case "NM":
                        $analysis->setNotMatch($count);
                        break;
                    case "H":
                        $analysis->setMinute($count);
                        break;
                }
            }
        }
        $analysis->calculateTotalWords();
        $analysis->calculateWeightedWords();
        return $analysis;
    }

    public function downloadFile(File &$file, Task &$task = null) {
        $result = false;
        $this->initPlunnet($this->users[0]->getUserName(),$this->users[0]->getPassWord(),$this->users[0]->getIdApiUser());

        if (NLG::$plunet->isConnect()) {
            /*if($task->getTries() == 1){
                $link = null;
                foreach ($task->getAditionalProperties() as $property) {
                    if($property->getPropertyName() == "linkOptional"){
                        $link = $property->getPropertyValue();
                        break;
                    }
                }
                if($link != null){
                    $taskDAO = new TaskDAO();
                    if($taskDAO->deleteFile($file->getIdFile(),$file->getFileType()) && file_exists($file->getPath())){
                        @unlink($file->getPath());
                    }
                    $this->downloadFiles($plunet, $task->getIdClientetask(), $task->getUniqueSourceId(), $link);
                }
            }else{*/
            $asyncHash = NLG::$plunet->downloadFile($file->getFileName(), $file->getPathForDownload(),$task->getTitle()."/");
            if($asyncHash != null){
                $file->setAsyncHash($asyncHash);
                $taskDAO = new TaskDAO();
                $taskDAO->saveTaskFileAsyncDownload(array(
                    "asyncHash" => $asyncHash,
                    "downloadPath" => $file->getSourcePath(),
                    "isDownloading" => true,
                    "isDownloaded" => false,
                    "filePath" => ""
                ));
//				$result = $taskDAO->updateFile($file);
            }
            // }
        }

        return $result;
    }

    /**
     *
     * @param string $fileName
     * @param string $uniqueSourceId
     * @param string $url
     * @return File[]
     * @throws \Exception
     */
    private function downloadFiles(string $fileName, $uniqueSourceId, string $url){
        $filesFounds = array();
        $po = array($fileName.".pdf" => true, $fileName.".rtf" => true);
        $files = NLG::$plunet->singleFileDownload($url);
        if($files !== null){
            if(isset($files["source"])){
                foreach ($files["source"] as $fileData){
                    if($fileData["size"] > 0 && !isset($po[$fileData["name"].".".$fileData["type"]])){
                        if ($fileData["type"] != "rtf" && strpos(strtoupper($fileData["name"]),"ANALYSIS") === false){
                            $file = new File();
                            $file->construct(
                                $fileData["name"].".".$fileData["type"],
                                $fileData["type"],
                                $fileData["type"],
                                "File to download",
                                Functions::currentDate(),
                                $fileData['link'],
                                "",
                                Harvest::TMS,
                                File::SOURCE,
                                false,
                                0);
                            $file->setPathForDownload($fileData['link']);
                            $file->setPath("/ai/repository/NLG/Plunet/".$fileName."/".$fileData["name"].".".$fileData["type"]);
                            $filesFounds[] = $file;
                        }
                    }

                }
            }

            if(isset($files["reference"])){
                foreach ($files["reference"] as $dir => $fileData){
                    foreach ($fileData as $data){
                        if($data["size"] > 0){
                            $file = new File();
                            $file->construct(
                                $data["name"].".".$data["type"],
                                $data["type"],
                                $data["type"],
                                "File to download",
                                Functions::currentDate(),
                                $data['link'],
                                "",
                                Harvest::TMS,
                                File::REFERENCE,
                                false,
                                0);
                            $file->setPathForDownload($data['link']);
                            $file->setPath("");
                            $filesFounds[] = $file;
                        }
                    }
                }
            }
        }else{
            $filesFounds = null;
        }

        return $filesFounds;

    }

    public function structureTask($task)
    {
        /*$filesToDelete = array();
        $filesToUnZip = array();
        if(count($task->getFilesForReference()) === 1){
            $list = ZipFile::listFiles($task->getFilesForReference()[0]->getPath());
            if($list !== false){
                foreach ($list as $fileName){
                    if(preg_match('#\.(rtf)$#i', $fileName) || strpos($fileName,$task->getIdClientetask().".pdf") !== false){
                        $filesToDelete[] = $fileName;
                    }
                }

                foreach ($list as $fileName){
                    if (preg_match('#\.(sdlxliff)$#i', $fileName) || preg_match('#\.(sdltm)$#i', $fileName) || preg_match('#\.(sdlppx)$#i', $fileName)){
                        $filesToUnZip[] = $fileName;
                    }
                }
            }
            ZipFile::deleteFilesOnZip($task->getFilesForReference()[0]->getPath(),$filesToDelete);
            //ZipFile::unZipAnyFiles($task->getFilesForReference()[0]->getPath(), $filesToUnZip, AI::getInstance()->getSetup()->repository."NLG/Plunet/");
        }*/
    }

    /**
     * Filter the values ​​languages ​​according to the name
     *
     * @param string $language name language
     * @return string language identifier
     */
    private function filterLanguage($language){
        $idLanguage = "es-ES";
        $language = trim($language);
        if ($language != null) {
            switch ($language){
                case "English (USA)":
                    $idLanguage = 'en-US';
                    break;
                case 'Spanish (Spain)':
                    $idLanguage = 'es-ES';
                    break;
                case 'Spanish (Latin America)':
                    $idLanguage = 'es-LA';
                    break;
                case 'Spanish (Mexico)':
                    $idLanguage = 'es-MX';
                    break;
                case 'English (UK)':
                    $idLanguage = 'en-GB';
                    break;
                case 'French (France)':
                    $idLanguage = 'fr-FR';
                    break;
                case 'German (Germany)':
                    $idLanguage = 'de-DE';
                    break;
                case 'Portuguese (Brazil)':
                    $idLanguage = 'pt-BR';
                    break;
                case 'Catalan':
                    $idLanguage = 'ca-ES';
                    break;
                case 'Spanish (Puerto Rico)':
                    $idLanguage = 'es-PR';
                    break;
                default:
                    break;
            }
        }

        return $idLanguage;
    }

    public function createTask($taskData,$tms = null,$user = null){
        $task = new Task();
        $task->construct(
            Functions::currentDate(),
            $taskData['idClient'],
            $taskData['idClient'],
            $taskData['instructions'],
            Functions::createFormatDate(isset($taskData['starDate']) ? $taskData['starDate'] : "", "Europe/Madrid"),
            Functions::createFormatDate($taskData['dueDate'], "Europe/Madrid"),
            ""
        );
        $task->setAssignedUser($user->getIdApiUser());
        $sourceLanguage = parent::identifySourceLanguage($taskData['sourceLanguage']);
        if ($sourceLanguage != null) {
            $task->setSourceLanguage($sourceLanguage);
        }
        $targets = parent::identifyTargetLanguage(array($taskData['targetLanguage']));
        $task->setTargetsLanguage($targets);

        $dataSource = new DataSource();
        $additionalProperties = $this->collectProperties(array(), $taskData);
        $task->setAditionalProperties($additionalProperties);
        $dataSource->construct(
            $this->source->getSourceName(),
            "IN",
            Functions::currentDate(),
            "TMS",
            $this->urlPlunet,
            $this->urlPlunet . $taskData['link'],
            $taskData['originalData'],
            $this->source->getIdSource(),
            $user->getIdApiUser()
        );
        $analysis = $this->generateAnalsis($taskData["Analysis"]);
        if (!empty($task->getTargetsLanguage())) {
            $task->getTargetsLanguage()[0]->setAnalysis($analysis);
        }
        $task->setDataSource($dataSource);
        $task->generateUniqueTaskId();

        if ($taskData['link']) {
            $file = new File();
            $file->construct(
                $task->getIdClientetask() . ".zip",
                "zip",
                "",
                "File with all attachments",
                Functions::currentDate(),
                $this->urlPlunet . $taskData['link'],
                "",
                Harvest::TMS,
                File::REFERENCE,
                0,
                0
            );
            $file->setPathForDownload($this->urlPlunet.$taskData['link']);
            $file->setPath("/ai/repository/NLG/Plunet/" . $task->getIdClientetask() . "/" . $file->getFileName());
            $task->addFileForReference($file);
        }
        return $task;
    }

    public function getAvailableTask(){
        $sessionInterrupted = false;
        $taskDAO = new TaskDAO();
        $mails = $this->analyzeMail();
        if(!empty($mails)){
            foreach($this->users as $user) {
                $this->initPlunnet($user->getUserName(),$user->getPassWord(),$user->getIdApiUser());
                if(NLG::$plunet->isConnect()){
                    $links = NLG::$plunet->filterTaskLinks(PlunetConnector::INPROCESS,$mails);
                    if($links !== null){
                        foreach($links as $link) {
                            $taskData = NLG::$plunet->getTaskData($link);
                            if (!empty($taskData)) {
                                $task = new AvailableTask();
                                if (isset($mails[$taskData["idClient"]]["Instructions"]) && $mails[$taskData["idClient"]]["Instructions"] != null) {
                                    $instruction = $mails[$taskData["idClient"]]["Instructions"];
                                } else {
                                    $instruction = ($taskData['instruction'] != null) ? $taskData['instruction'] : "";
                                }
                                if (isset($mails[$taskData["idClient"]]["Customer"])) {
                                    $taskData["Customer"] = $mails[$taskData["idClient"]]["Customer"];
                                }
                                if (isset($mails[$taskData["idClient"]]["project_description"])) {
                                    $taskData["project_description"] = $mails[$taskData["idClient"]]["project_description"];
                                }
                                if(isset($mails[$taskData["idClient"]]["sourceLanguage"])){
                                    $taskData["sourceLanguage"] = $mails[$taskData["idClient"]]["sourceLanguage"];
                                }
                                if(isset($mails[$taskData["idClient"]]["targetLanguage"])){
                                    $taskData["targetLanguage"] = $mails[$taskData["idClient"]]["targetLanguage"];
                                }
                                $task->setTitle($taskData['idClient']);
                                $task->setIdClientTask($taskData["idClient"]);
                                $task->setMessage($instruction);
                                $task->setDueDate(Functions::createFormatDate(isset($taskData['dueDate']) ? $taskData['dueDate'] : $mails[$taskData["idClient"]]["dueDate"], "Europe/Madrid")->format("Y-m-d H:i:s"));

                                $sourceLanguage = parent::identifySourceLanguage($taskData['sourceLanguage']);
                                if ($sourceLanguage != null) {
                                    $task->setSourceLanguage($sourceLanguage);
                                }
                                $targets = parent::identifyTargetLanguage(array($taskData['targetLanguage']));
                                $task->setTargetsLanguage($targets);

                                $aditionalProperties = $this->collectProperties(array(), $taskData);
                                $task->setAdditionalProperties($aditionalProperties);

                                $analysis = $this->generateAnalsis($mails[$task->getIdClienttask()]["Analysis"]);
                                if (!empty($task->getTargetsLanguage())) {
                                    $task->getTargetsLanguage()[0]->setAnalysis($analysis);
                                }
                                $this->availableTasks[] = $task;
                            }else{
                                break;
                            }
                        }
                    }
                    else {
                        break;
                    }
                }
            }

        }

        return $this->availableTasks;
    }

    public function collectTaskByIdClientTask($idTask)
    {
        $taskDAO = new TaskDAO();
        $mails = $this->analyzeMail();
        $task = null;
        $mail = array();
        if(isset($mails[$idTask])){
            $mail[$idTask] = $mails[$idTask];
            foreach($this->users as $user) {
                $this->initPlunnet($user->getUserName(),$user->getPassWord(),$user->getIdApiUser());
                if(NLG::$plunet->isConnect()) {
                    $link = NLG::$plunet->filterTaskLinks(PlunetConnector::INPROCESS, $mail);
                    if($link !== null){
                        if($link != null){
                            $taskData = NLG::$plunet->getTaskData($link);
                            if (!empty($taskData)) {
                                if (isset($mails[$taskData["idClient"]]["Instructions"]) && $mails[$taskData["idClient"]]["Instructions"] != null) {
                                    $taskData['instructions'] = $mails[$taskData["idClient"]]["Instructions"];
                                } else {
                                    $taskData['instructions'] = ($taskData['instruction'] != null) ? $taskData['instruction'] : "";
                                }
                                if (isset($mails[$taskData["idClient"]]["Customer"])) {
                                    $taskData["Customer"] = $mails[$taskData["idClient"]]["Customer"];
                                }
                                if (!isset($taskData['dueDate'])) {
                                    $taskData['dueDate'] = $mails[$taskData["idClient"]]["dueDate"];
                                }
                                $taskData["Analysis"] = array();
                                if (isset($mails[$taskData["idClient"]]["Analysis"])) {
                                    $taskData["Analysis"] = $mails[$taskData["idClient"]]["Analysis"];
                                }
                                if (isset($mails[$taskData["idClient"]]["project_description"])) {
                                    $taskData["project_description"] = $mails[$taskData["idClient"]]["project_description"];
                                }
                                if (isset($mails[$taskData["idClient"]]["sourceLanguage"])) {
                                    $taskData["sourceLanguage"] = $mails[$taskData["idClient"]]["sourceLanguage"];
                                }
                                if (isset($mails[$taskData["idClient"]]["targetLanguage"])) {
                                    $taskData["targetLanguage"] = $mails[$taskData["idClient"]]["targetLanguage"];
                                }
                                $task = $this->createTask($taskData, null, $user);
                                $task->setTimeStamp(Functions::currentDate());
                                $task->generateUniqueSourceId();
                                if ($taskDAO->getTaskByUTID($task->getUniqueTaskId())->getUniqueTaskId() == null) {
                                    foreach ($task->getAttachments() as &$file) {
                                        $asyncHash = NLG::$plunet->downloadFile($file->getFileName(), $file->getPathForDownload(), $task->getTitle() . "/");
                                        if ($asyncHash != null) {
                                            $taskDAO->saveTaskFileAsyncDownload(array(
                                                "asyncHash" => $asyncHash,
                                                "downloadPath" => $file->getPathForDownload(),
                                                "isDownloading" => true,
                                                "isDownloaded" => false,
                                                "filePath" => ""
                                            ));
                                            $file->setAsyncHash($asyncHash);
                                        }
                                    }
                                    $this->newTasks[] = $task;
                                }
                            }else{
                                throw new \Exception("Session interrupted");
                            }
                        }
                    }else{
                        throw new \Exception("Session interrupted");
                    }
                }
            }
        }else{
            throw new \Exception("the mail with idClientTask = $idTask was not found");
        }

        foreach ($this->newTasks as $task){
            if(isset($mails[$task->getIdClientetask()])){
                if(@copy($this->pathMails.$mails[$task->getIdClientetask()]["Message"][0],$this->pathMails."processed/".$mails[$task->getIdClientetask()]["id"].".msg")){
                    if(isset($mails[$task->getIdClientetask()]["Message"]) && is_array($mails[$task->getIdClientetask()]["Message"])){
                        foreach ($mails[$task->getIdClientetask()]["Message"] as $message){
                            @unlink($this->pathMails.$message);
                            Functions::console("Mensaje borrado: ".$this->pathMails.$message);
                        }
                    }

                }
            }
        }

        parent::collectTask();
        return (count($this->newTasks) > 0);
    }
}

?>