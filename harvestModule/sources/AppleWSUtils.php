<?php
namespace harvestModule\sources;
class MailProperties{
    private $fullFileName;
    private $fileName;
    private $fileWithoutExtension;
    private $timesProcessed;

    private function __construct(){}

    /**
     * @return mixed
     */
    public function getFullFileName()
    {
        return $this->fullFileName;
    }

    /**
     * @param mixed $fullFileName
     * @return MailProperties
     */
    public function setFullFileName($fullFileName)
    {
        $this->fullFileName = $fullFileName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     * @return MailProperties
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFileWithoutExtension()
    {
        return $this->fileWithoutExtension;
    }

    /**
     * @param mixed $fileWithoutExtension
     * @return MailProperties
     */
    public function setFileWithoutExtension($fileWithoutExtension)
    {
        $this->fileWithoutExtension = $fileWithoutExtension;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTimesProcessed()
    {
        return $this->timesProcessed;
    }

    /**
     * @param mixed $timesProcessed
     * @return MailProperties
     */
    public function setTimesProcessed($timesProcessed)
    {
        $this->timesProcessed = $timesProcessed;
        return $this;
    }

    public static function getMailProperiesFromMailFile($fileUrl){
        $mail_name = explode("/", $fileUrl);
        $mail_name = end($mail_name);
        $mail_data_without_extension = explode(".", $mail_name)[0];

        $nameParts = explode("--", "$mail_name");
        $times_processed = 0;
        if (count($nameParts) == 3) {
            $times_processed = intval($nameParts[1]);
            $mail_data_without_extension = explode("--", "$mail_name")[0];
        }

        $result = new MailProperties();
        $result->fileName = $mail_name;
        $result->fileWithoutExtension = $mail_data_without_extension;
        $result->fullFileName = $fileUrl;
        $result->timesProcessed = $times_processed;

        return $result;
    }


}

abstract class AppleLioxProperties{
    const PROJECT_TEAM = "projectTeam";
    const EDDITING_SCOPE = "edditing_scope";
    const PROJECTS_TITLE = "projects_title";
    const TASK_TYPE = "taskType";
    const PROJECT_IDS_TODO  = "project_ids_todo";
}

abstract class AppleLioxMailTeams {
    const SharedServices = "SharedServices";
    const SalesTraining = "SalesTraining";
    const AppleCareInquira = "AppleCareInquira";
    const AppleHeart = "AppleHeart";
    const AppleCareKbase = "AppleCareKbase";
    const Unknown = "Unknown";
}