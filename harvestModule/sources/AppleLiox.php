<?php

/**
 * @author mhernandez
 * @version 1.0
 * created on 8 mar 2019
 */

namespace harvestModule\sources;

use common\exceptions\AIException;
use common\exceptions\ParseDateException;
use common\exceptions\sources\apple_liox\ProjectIdNotFoundException;
use common\exceptions\sources\apple_liox\WorldServerException;
use common\exceptions\sources\LTBDownloaderException;
use common\ZipFile;
use core\AI;
use dataAccess\dao\TaskDAO;
use DateTime;
use DateTimeZone;
use Exception;
use Functions;
use harvestModule\sources\connectService\AppleLioxConnect;
use harvestModule\sources\connectService\AppleLioxMail;
use harvestModule\sources\sourceComplements\LTBDownloader;
use harvestModule\sources\sourceComplements\WorldServerConnect;
use common\exceptions\sources\apple_liox\MailException;
use model\Analysis;
use model\AvailableTask;
use model\File;
use model\Source;
use model\State;
use model\Task;
use model\DataSource;
use model\TaskAditionalProperties;
use Throwable;

include_once BaseDir . '/dataAccess/dao/LanguageDAO.php';
include_once BaseDir . '/harvestModule/sources/StructureSource.php';
include_once BaseDir . '/harvestModule/sources/connectService/AppleLioxMail.php';
include_once BaseDir . '/harvestModule/sources/connectService/AppleLioxConnect.php';
include_once BaseDir . '/model/Task.php';
include_once BaseDir . '/Functions.php';
include_once BaseDir . '/common/ZipFile.php';
include_once BaseDir . '/harvestModule/sources/sourceComplements/LTBDownloader.php';
include_once BaseDir . "/common/exceptions/sources/LTBDownloaderException.php";
include_once BaseDir . "/common/exceptions/AIException.php";
include_once BaseDir . "/common/exceptions/ParseDateException.php";
include_once BaseDir . '/dataAccess/dao/TaskDAO.php';
include_once BaseDir . "/model/AvailableTask.php";

abstract class AppleLioxAddittionalProperties
{
    const PROJECT_TEAM = "projectTeam";
    const EDDITING_SCOPE = "edditing_scope";
    const PROJECTS_TITLE = "projects_title";
    const TASK_TYPE = "taskType";
    const PROJECT_IDS_TODO  = "project_ids_todo";
}

abstract class AppleLioxTeams
{
    const SharedServices = "SharedServices";
    const SalesTraining = "SalesTraining";
    const AppleCareInquira = "AppleCareInquira";
    const AppleHeart = "AppleHeart";
    const AppleCareKbase = "AppleCareKbase";
    const Unknown = "Unknown";
}

/**
 * This class collect tasks according to the information found in mails
 * Class AppleLiox
 * @package harvestModule\sources
 */
class AppleLiox extends StructureSource
{
    public static $repository_path = "AppleLioxWS/";
    /**
     * @var String Mails path
     */
    public static $appleliox_aps_mail_path;
    /**
     * @var WorldServerConnect|null Should have an instance to share the cookies and authorization token
     */
    protected $appleConnectService = null;
    /**
     * Type of harvestMethod to define in the task creation
     */
    private const HARVESMETHOD = 'Email';
    /**
     * @var String path to save the mails that where parsed correctly
     */
    private static $mail_success_directory;
    /**
     * @var String path to save the mails that where parsed incorrectly
     */
    private static $mail_damaged_directory;
    /**
     * Define how many times a mail must be proccess to be moved to damaged
     */
    private const TIMES_TO_REPROCCESS_MAIL = 5;


    /**
     * AppleLiox constructor.
     * @param array $users User list to connect to the world server. In this stage only one user is used.
     * @param Source $source The source
     * @throws Exception When it can't log in the WorldServer
     */
    public function __construct(array $users, Source $source)
    {
        parent::__construct($users, $source);

        AppleLiox::$appleliox_aps_mail_path = $this->development ? '/webs/ai/apple_mails/' : '/ai/sources/apple_lionbridge/';
        AppleLiox::$mail_success_directory = $this->development ? '/webs/ai/apple_mails/processed/' : '/ai/sources/apple_lionbridge/processed/';
        AppleLiox::$mail_damaged_directory = $this->development ? '/webs/ai/apple_mails/damaged/' : '/ai/sources/apple_lionbridge/damaged/';

        //We get the first element because we only have one user and we don't know how to detect diferents mail addressee
        if (count($users) != 1) {
            throw (new AIException("Unsupported number of user for AppleLiox"))
                ->construct(__FUNCTION__, __NAMESPACE__, $ARGS = func_get_args());
        }
        try {
            $this->appleConnectService = new AppleLioxConnect($users[0]);
            $this->appleConnectService->startConnection();
        } catch(AIException $e){
            throw $e;
        } catch (Throwable $e) {
            throw (new AIException("Imposible crear la instancia del conector. Mensaje de error: " . $e->getMessage()))
                ->construct(__FUNCTION__, __NAMESPACE__, $args = func_get_args());
        }
    }

    private function getMailFromIdClientTask($idTask){
        $incoming_mails = glob(AppleLiox::$appleliox_aps_mail_path . "*.msg");
        foreach ($incoming_mails as $mail) {
            try{
                $apple_mail = new AppleLioxMail($mail);
                $mail_data = $apple_mail->analyzeMail();
                $idClientTaskMail = $this->generateClientId($mail_data["project_id"]);
                if($idClientTaskMail == $idTask){
                    return $mail;
                }
            } catch(Throwable $t){
                continue;
            }
        }
        return false;
    }
    public function collectTaskByIdClientTask($idTask)
    {
        $mail = $this->getMailFromIdClientTask($idTask);
        if($mail !== false){
            try{
                $mail_name = explode("/", $mail);
                $mail_name = end($mail_name);
                $mail_data_without_extension = explode(".", $mail_name)[0];
    
                $nameParts = explode("--", "$mail_name");
                $times_processed = 0;
                if (count($nameParts) == 3) {
                    $times_processed = intval($nameParts[1]);
                    $mail_data_without_extension = explode("--", "$mail_name")[0];
                }
                try {
                    $apple_mail = new AppleLioxMail($mail);
                    $mail_data = $apple_mail->analyzeMail();
                } catch (MailException $e) {
                    rename($mail, AppleLiox::$mail_damaged_directory . $mail_name);
                    Functions::logErrorToDB($e->getMessage() . " en mail: $mail", Functions::INFO, __CLASS__);
                    return false;
                } catch (ProjectIdNotFoundException $ex) {
                    rename($mail, AppleLiox::$mail_damaged_directory . $mail_name);
                    Functions::logException("Cannot find WS project ID en mail: $mail", Functions::WARNING, __CLASS__, $ex);
                    return false;
                } catch (Throwable $ex2) {
                    $ex2 = (AIException::createInstanceFromThrowable($ex2))->construct(__FUNCTION__, __NAMESPACE__, $args = func_get_args());
                    Functions::logException("Cannot parse mail, reason:" . $ex2->getMessage() . " en mail: $mail", Functions::ERROR, __CLASS__, $ex2);
                    return false;
                }
    
                if ($this->hasProjectId($mail_data)) {
                    $projects_info = array();
                    try {
                        $projects_info = $this->appleConnectService->downloadProjects($mail_data["project_id"]);
                    } catch (WorldServerException $e) {
                        if ($times_processed >= self::TIMES_TO_REPROCCESS_MAIL) {
                            rename($mail, AppleLiox::$mail_damaged_directory . $mail_name);
                        } else {
                            $times_processed += 1;
                            rename($mail, AppleLiox::$appleliox_aps_mail_path . $mail_data_without_extension . "--" . $times_processed . "--.msg");
                        }
                        Functions::logException($e->getMessage() . " en mail: $mail", Functions::WARNING, __CLASS__, $e);
                        return false;
                    }
    
                    $analysis_list = array();
                    $additional_instructions = "";
                    $targetLanguages_list = array();
                    $filesForReference = array();
    
                    //$instructions = "";
                    $project_titles = array();
                    $project_ids_todo = array();
                    for ($i = 0; $i < count($projects_info); $i++) {
                        $project_info = $projects_info[$i];
                        $project_titles[] = "[" . $project_info["idWS"]. "]" . $project_info["basic_info"]->detail[1]->value;
                        $project_ids_todo[] =  $project_info["idWS"];
                        if (is_array($project_info["extended_info"]["transinstructions"])) {
                            foreach ($project_info["extended_info"]["transinstructions"] as $instruction) {
                                $additional_instructions .= $instruction . " ";
                            }
                        }
                        if ($project_info == null) {
                            Functions::addLog("Mail $mail: Project not found (" . $mail_data["project_id"][$i] . ") in the WS", Functions::WARNING, __CLASS__);
                            return false;
                        }
                        $analysis = $this->createAnalysisFromJson($project_info["wwc"]);
                        if ($analysis == null) {
                            return false;
                        }
                        $analysis_list[] = $analysis;
    
    
                        if (isset($project_info["files"]["REFERENCE"])) {
                            foreach ($project_info["files"]["REFERENCE"] as $sourceFile) {
                                $filesForReference[] = $sourceFile["path"];
                            }
                        }
    
                        if (!in_array($project_info["wwc"]["target_language"], $targetLanguages_list) && $project_info["wwc"]["target_language"] != null) {
                            $targetLanguages_list[] = $project_info["wwc"]["target_language"];
                        }
                    }
                    if (empty($analysis_list)) {
                        $logData = array("project_id" => $mail_data["project_id"], "file" => $mail_data["file"]);
                        $ex = (new AIException("Cannot find WS projects info"))->construct(__METHOD__, __NAMESPACE__, $args = func_get_args(), $logData);
                        Functions::logException("Cannot find WS projects info", Functions::WARNING, __CLASS__, $ex);
                        if ($times_processed >= self::TIMES_TO_REPROCCESS_MAIL) {
                            rename($mail, AppleLiox::$mail_damaged_directory . $mail_name);
                        } else {
                            $times_processed += 1;
                            rename($mail, AppleLiox::$appleliox_aps_mail_path . $mail_data_without_extension . "--" . $times_processed . "--.msg");
                        }
                        return false;
                    }
    
                    $idClientTask = $this->generateClientId($mail_data["project_id"]);
    
                    if(!$this->isNewClientId($idClientTask)){
                        $ex = new AIException("Task already registered");
                        Functions::logException("Task already registered: $idClientTask", Functions::INFO, __CLASS__, $ex);
                        rename($mail, AppleLiox::$mail_damaged_directory . $mail_name);
                        return false;
                    }
    
                    $task = new Task();
                    $title = $this->createTaskTitle($mail_data["subject"], $mail_data["project_id"]);
        
                    //Mail files
                    if(is_array($mail_data["attachments"])){
                        foreach ($mail_data["attachments"] as $attachment){
                            $filesForReference[] = $attachment;
                        }
                    }
                    //LTB File
                    try {
                        if ($mail_data["ltb_configurations_link"] != "") {
                            $destinationPath = self::$repository_path . "ltb/";
        
                            $ltbLink = $mail_data["ltb_configurations_link"];
                            $ltbLink = str_replace("\n", "", $ltbLink);
                            $ltbLink = htmlentities($ltbLink, ENT_QUOTES | ENT_IGNORE, "UTF-8");
                            $ltbLink = str_replace('&nbsp;','',$ltbLink);
        
                            $ltbDownloader = new LTBDownloader($ltbLink, $destinationPath);
                            $files = $ltbDownloader->downloadLTB();
                            foreach ($files as $file) {
                                $filesForReference[] = AI::getInstance()->getSetup()->repository . $file["path"];
                            }
                        }
                    } catch (LTBDownloaderException $e) {
                        if ($times_processed >= self::TIMES_TO_REPROCCESS_MAIL) {
                            Functions::logException("LTB File not found. Task will be collected without this file", Functions::WARNING, __CLASS__, $e);
                        } else {
                            $times_processed += 1;
                            rename($mail, AppleLiox::$appleliox_aps_mail_path . $mail_data_without_extension . "--" . $times_processed . "--.msg");
                            Functions::logException("LTB File not found. Mail will be processed on next execution", Functions::INFO, __CLASS__, $e);
                            return false;
                        }
                    }
        
        
        
                    // Creating zip file
                    $destinationFolder = AI::getInstance()->getSetup()->repository . self::$repository_path . uniqid() . "/";
                    mkdir($destinationFolder);
        
                    $destinationHTMLFile = $destinationFolder . "instructions.html";
                    $htmlMail = $apple_mail->getBodyHTML();
                    $htmlMail = str_replace($mail_data["deadline"], "", $htmlMail);
                    $htmlMail = str_replace($mail_data["project_name"], implode("\n", $project_titles), $htmlMail);
        
                    if ($this->createInstructionsFile($destinationHTMLFile, $htmlMail)) {
                        $filesForReference[] = $destinationHTMLFile;
                    } else {
                        Functions::addLog("Cannot create instructions file", Functions::WARNING, __class__);
                    }
        
                    $zipFileName = "taskFiles.zip";
                    $destinationZipFile = $destinationFolder . $zipFileName;
                    try {
                        ZipFile::create_zip($filesForReference, "" . $destinationZipFile);
                    } catch (AIException $e){
                        Functions::logException("Cannot create zip file in" . " mail: $mail", Functions::WARNING, __CLASS__, $e);
                        return false;
                    }
        
                    $file = new File();
                    $file->construct(
                        "" . $zipFileName,
                        "zip",
                        null,
                        null,
                        Functions::currentDate(),
                        null,
                        null,
                        "AppleWorldServer",
                        File::REFERENCE,
                        true,
                        filesize($destinationZipFile)
                    );
                    $file->setPath($destinationZipFile);
                    $task->addFileForReference($file);
        
                    $message = ($mail_data["additional_instructions"] != null && $mail_data["additional_instructions"] !== "") ? $mail_data["additional_instructions"] : " ";
                    try{
                        $dueDate = $this->parseDeadLine($mail_data["deadline"]);
                    } catch (ParseDateException $e){
                        $dueDate = $this->getTodayEOBDate();
                        $message = $this->appendDueDateParseError($message);
                    }
        
                    $task->construct(
                        Functions::currentDate(),
                        $idClientTask,
                        $title,
                        $message,
                        Functions::currentDate(),
                        $dueDate,
                        ""
                    );
                    $task->setMenssage($task->getMenssage() . $additional_instructions); //mail instructions + WS instructions
        
                    $sourceLanguage = parent::identifySourceLanguage($this->clearSourceLanguage("English (US)"));
                    if ($sourceLanguage != null) {
                        $task->setSourceLanguage($sourceLanguage);
                    }
                    if ($targetLanguages_list == null || empty($targetLanguages_list)) {
                        $targets = parent::identifyTargetLanguage(array("Spanish (Spain)"));
                    } else {
                        $targets = parent::identifyTargetLanguage($targetLanguages_list);
                    }
                    if (isset($targets[0])) {
                        $targets[0]->setAnalysis($this->joinAnalysis($analysis_list));
                    }
                    $task->setTargetsLanguage($targets);
        
                    $dataSource = new DataSource();
        
                    $dataSource->construct(
                        $this->source->getSourceName(),
                        AI::getInstance()->getSetup()->execution_type,
                        Functions::currentDate(),
                        self::HARVESMETHOD,
                        WorldServerConnect::BASE_URL,
                        "",
                        file_get_contents($mail),
                        $this->source->getIdSource(),
                        $this->users[0]->getIdApiUser()
                    );
                    //Removing he original data on development to improve reading process
                    if(AI::getInstance()->getSetup()->development){
                        $dataSource->setOriginalData("");
                    }
                    $task->setDataSource($dataSource);
                    $task->setState(new State(State::COLLECTED));
        
                    $task->generateUniqueTaskId();
                    $task->setStartDate(Functions::currentDate());
                    $task->setTimeStamp(Functions::currentDate());
                    $task->generateUniqueSourceId();
        
                    $project_titles = implode("\n", $project_titles);
                    $edditing_scope = $mail_data["editting_scope"];
                    $projectTeam = $mail_data["projectTeam"];
                    $taskType = $mail_data["task"];
        
                    $additional_prop_raw_data = array(
                        AppleLioxAddittionalProperties::PROJECT_TEAM => $projectTeam,
                        AppleLioxAddittionalProperties::EDDITING_SCOPE => $edditing_scope,
                        AppleLioxAddittionalProperties::PROJECTS_TITLE => $project_titles,
                        AppleLioxAddittionalProperties::TASK_TYPE => $taskType,
                        AppleLioxAddittionalProperties::PROJECT_IDS_TODO => implode("-", $project_ids_todo),
                    );
        
                    $task->setAditionalProperties($this->collectProperties(null, $additional_prop_raw_data));
        
                    $this->newTasks[] = $task;
                    //Movemos el Mail a proccesed
                    if(!AI::getInstance()->getSetup()->development){
                        rename($mail, AppleLiox::$mail_success_directory . $mail_name);
                    }
                    unset($apple_mail);
                    
                } else {
                    Functions::addLog("Cannot find WS ID in mail: $mail", Functions::WARNING, __CLASS__);
                    if ($times_processed >= self::TIMES_TO_REPROCCESS_MAIL) {
                        rename($mail, AppleLiox::$mail_damaged_directory . $mail_name);
                    } else {
                        $times_processed += 1;
                        rename($mail, AppleLiox::$appleliox_aps_mail_path . $mail_data_without_extension . "--" . $times_processed . "--.msg");
                    }
                }
            } catch(Throwable $t){
                Functions::logException($t->getMessage(), Functions::ERROR, __CLASS__, AIException::createInstanceFromThrowable($t));
                Functions::console("Mail process could not been completed. Mail file: $mail");
            }
        }
        parent::collectTask();

        return (count($this->newTasks) > 0);
    }
    /**
     * Tries to collect the task by reading the mails form the mail_path
     * @return bool|void
     */
    public function collectTask()
    {
        //controllar si el source tiene el download en 1
        $incoming_mails = glob(AppleLiox::$appleliox_aps_mail_path . "*.msg");
        foreach ($incoming_mails as $mail) {
            $projectTeam = "";
            try{
                $mail_name = explode("/", $mail);
                $mail_name = end($mail_name);
                $mail_data_without_extension = explode(".", $mail_name)[0];

                $nameParts = explode("--", "$mail_name");
                $times_processed = 0;
                if (count($nameParts) == 3) {
                    $times_processed = intval($nameParts[1]);
                    $mail_data_without_extension = explode("--", "$mail_name")[0];
                }
                try {
                    $apple_mail = new AppleLioxMail($mail);
                } catch (MailException $e) {
                    rename($mail, AppleLiox::$mail_damaged_directory . $mail_name);
                    Functions::logErrorToDB($e->getMessage() . " en mail: $mail", Functions::INFO, __CLASS__);
                    continue;
                } catch (ProjectIdNotFoundException $ex) {
                    rename($mail, AppleLiox::$mail_damaged_directory . $mail_name);
                    Functions::logErrorToDB("Cannot find WS project ID en mail: $mail. " . $ex->getMessage(), Functions::WARNING, __CLASS__);
                    continue;
                } catch (Throwable $ex2) {
                    $ex2 = (AIException::createInstanceFromThrowable($ex2))->construct(__FUNCTION__, __NAMESPACE__, $args = func_get_args());
                    rename($mail, AppleLiox::$mail_damaged_directory . $mail_name);
                    Functions::logException("Cannot parse mail, reason:" . $ex2->getMessage() . " en mail: $mail", Functions::ERROR, __CLASS__, $ex2);
                    continue;
                }
                try {
                    $mail_data = $apple_mail->analyzeMail();
                } catch (MailException $e) {
                    rename($mail, AppleLiox::$mail_damaged_directory . $mail_name);
                    Functions::logErrorToDB($e->getMessage() . " en mail: $mail", Functions::INFO, __CLASS__);
                    continue;
                }

                if ($this->hasProjectId($mail_data)) {
                    $projects_info = array();
                    try {
                        $projects_info = $this->appleConnectService->downloadProjects($mail_data["project_id"]);
                    }
                    catch (WorldServerException $e) {
                        if ($times_processed >= self::TIMES_TO_REPROCCESS_MAIL) {
                            rename($mail, AppleLiox::$mail_damaged_directory . $mail_name);
                        } else {
                            $times_processed += 1;
                            rename($mail, AppleLiox::$appleliox_aps_mail_path . $mail_data_without_extension . "--" . $times_processed . "--.msg");
                        }
                        Functions::logException($e->getMessage() . " en mail: $mail", Functions::WARNING, __CLASS__, $e);
                        continue;
                    }

                    $analysis_list = array();
                    $additional_instructions = "";
                    $targetLanguages_list = array();
                    $filesForReference = array();

                    //$instructions = "";
                    $project_titles = array();
                    $project_ids_todo =array();
                    for ($i = 0; $i < count($projects_info); $i++) {
                        $project_info = $projects_info[$i];
                        $project_titles[] = "[" . $project_info["idWS"]. "]" . $project_info["basic_info"]->detail[1]->value;
                        $project_ids_todo[] =  $project_info["idWS"];
                        if (is_array($project_info["extended_info"]["transinstructions"])) {
                            foreach ($project_info["extended_info"]["transinstructions"] as $instruction) {
                                $additional_instructions .= $instruction . " ";
                            }
                        }
                        if ($project_info == null) {
                            Functions::addLog("Mail $mail: Project not found (" . $mail_data["project_id"][$i] . ") in the WS", Functions::WARNING, __CLASS__);
                            continue;
                        }
                        $analysis = $this->createAnalysisFromJson($project_info["wwc"]);
                        if ($analysis == null) {
                            continue;
                        }
                        $analysis_list[] = $analysis;


                        if (isset($project_info["files"]["REFERENCE"])) {
                            foreach ($project_info["files"]["REFERENCE"] as $sourceFile) {
                                $filesForReference[] = $sourceFile["path"];
                            }
                        }

                        if (!in_array($project_info["wwc"]["target_language"], $targetLanguages_list) && $project_info["wwc"]["target_language"] != null) {
                            $targetLanguages_list[] = $project_info["wwc"]["target_language"];
                        }

                        $projectTeam = $project_info["basic_info"]->detail[3]->value;
                    }
                    if (empty($analysis_list)) {
                        $logData = array("project_id" => $mail_data["project_id"], "file" => $mail_data["file"]);
                        $ex = (new AIException("Cannot find WS projects info"))->construct(__METHOD__, __NAMESPACE__, $args = func_get_args(), $logData);
                        Functions::logException("Cannot find WS projects info", Functions::WARNING, __CLASS__, $ex);
                        if ($times_processed >= self::TIMES_TO_REPROCCESS_MAIL) {
                            rename($mail, AppleLiox::$mail_damaged_directory . $mail_name);
                        } else {
                            $times_processed += 1;
                            rename($mail, AppleLiox::$appleliox_aps_mail_path . $mail_data_without_extension . "--" . $times_processed . "--.msg");
                        }
                        continue;
                    }

                    $idClientTask = $this->generateClientId($mail_data["project_id"]);

                    if(!$this->isNewClientId($idClientTask)){
                        $ex = new AIException("Task already registered");
                        Functions::logException("Task already registered: $idClientTask", Functions::INFO, __CLASS__, $ex);
                        rename($mail, AppleLiox::$mail_damaged_directory . $mail_name);
                        continue;
                    }

                    $task = new Task();
                    $title = $this->createTaskTitle($mail_data["subject"], $mail_data["project_id"]);

                    //Mail files
                    if(is_array($mail_data["attachments"])){
                        foreach ($mail_data["attachments"] as $attachment){
                            $filesForReference[] = $attachment;
                        }
                    }
                    //LTB File
                    try {
                        if ($mail_data["ltb_configurations_link"] != "") {
                            $destinationPath = self::$repository_path . "ltb/";

                            $ltbLink = $mail_data["ltb_configurations_link"];
                            $ltbLink = str_replace("\n", "", $ltbLink);
                            $ltbLink = htmlentities($ltbLink, ENT_QUOTES | ENT_IGNORE, "UTF-8");
                            $ltbLink = str_replace('&nbsp;','',$ltbLink);

                            $ltbDownloader = new LTBDownloader($ltbLink, $destinationPath);
                            $files = $ltbDownloader->downloadLTB();
                            foreach ($files as $file) {
                                $filesForReference[] = AI::getInstance()->getSetup()->repository . $file["path"];
                            }
                        }
                    } catch (LTBDownloaderException $e) {
                        if ($times_processed >= self::TIMES_TO_REPROCCESS_MAIL) {
                            Functions::logException("LTB File not found. Task will be collected without this file", Functions::WARNING, __CLASS__, $e);
                        } else {
                            $times_processed += 1;
                            rename($mail, AppleLiox::$appleliox_aps_mail_path . $mail_data_without_extension . "--" . $times_processed . "--.msg");
                            Functions::logException("LTB File not found. Mail will be processed on next execution", Functions::INFO, __CLASS__, $e);
                            continue;
                        }
                    }



                    // Creating zip file
                    $destinationFolder = AI::getInstance()->getSetup()->repository . self::$repository_path . uniqid() . "/";
                    mkdir($destinationFolder);

                    $destinationHTMLFile = $destinationFolder . "instructions.html";
                    $htmlMail = $apple_mail->getBodyHTML();
                    $htmlMail = str_replace($mail_data["deadline"], "", $htmlMail);
                    $htmlMail = str_replace($mail_data["project_name"], implode("\n", $project_titles), $htmlMail);
                    $htmlMail = preg_replace('/[[:cntrl:]]/', '', $htmlMail);

                    if ($this->createInstructionsFile($destinationHTMLFile, $htmlMail)) {
                        $filesForReference[] = $destinationHTMLFile;
                    } else {
                        Functions::addLog("Cannot create instructions file", Functions::WARNING, __class__);
                    }

                    $zipFileName = "taskFiles.zip";
                    $destinationZipFile = $destinationFolder . $zipFileName;
                    try {
                        ZipFile::create_zip($filesForReference, "" . $destinationZipFile);
                    } catch (AIException $e){
                        Functions::logException("Cannot create zip file in" . " mail: $mail", Functions::WARNING, __CLASS__, $e);
                        continue;
                    }

                    $file = new File();
                    $file->construct(
                        "" . $zipFileName,
                        "zip",
                        null,
                        null,
                        Functions::currentDate(),
                        null,
                        null,
                        "AppleWorldServer",
                        File::REFERENCE,
                        true,
                        filesize($destinationZipFile)
                    );
                    $file->setPath($destinationZipFile);
                    $task->addFileForReference($file);

                    $message = ($mail_data["additional_instructions"] != null && $mail_data["additional_instructions"] !== "") ? $mail_data["additional_instructions"] : " ";
                    try{
                        $dueDate = $this->parseDeadLine($mail_data["deadline"]);
                    } catch (ParseDateException $e){
                        $dueDate = $this->getTodayEOBDate();
                        $message = $this->appendDueDateParseError($message);
                    }

                    $task->construct(
                        Functions::currentDate(),
                        $idClientTask,
                        $title,
                        $message,
                        Functions::currentDateAsObject(),
                        $dueDate,
                        ""
                    );
                    $task->setMenssage($task->getMenssage() . $additional_instructions); //mail instructions + WS instructions

                    $sourceLanguage = parent::identifySourceLanguage($this->clearSourceLanguage("English (US)"));
                    if ($sourceLanguage != null) {
                        $task->setSourceLanguage($sourceLanguage);
                    }
                    if ($targetLanguages_list == null || empty($targetLanguages_list)) {
                        $targets = parent::identifyTargetLanguage(array("Spanish (Spain)"));
                    } else {
                        $targets = parent::identifyTargetLanguage($targetLanguages_list);
                    }
                    if (isset($targets[0])) {
                        $targets[0]->setAnalysis($this->joinAnalysis($analysis_list));
                    }
                    $task->setTargetsLanguage($targets);

                    $dataSource = new DataSource();

                    $dataSource->construct(
                        $this->source->getSourceName(),
                        AI::getInstance()->getSetup()->execution_type,
                        Functions::currentDate(),
                        self::HARVESMETHOD,
                        WorldServerConnect::BASE_URL,
                        "",
                        file_get_contents($mail),
                        $this->source->getIdSource(),
                        $this->users[0]->getIdApiUser()
                    );
                    //Removing he original data on development to improve reading process
                    if(AI::getInstance()->getSetup()->development){
                        $dataSource->setOriginalData("");
                    }
                    $task->setDataSource($dataSource);
                    $task->setState(new State(State::COLLECTED));

                    $task->generateUniqueTaskId();
                    $task->setStartDate(Functions::currentDateAsObject());
                    $task->setTimeStamp(Functions::currentDate());
                    $task->generateUniqueSourceId();

                    $project_titles = implode("<br>", $project_titles);
                    $edditing_scope = $mail_data["editting_scope"];
                    $taskType = $mail_data["task"];

                    $additional_prop_raw_data = array(
                        AppleLioxAddittionalProperties::PROJECT_TEAM => $projectTeam,
                        AppleLioxAddittionalProperties::EDDITING_SCOPE => $edditing_scope,
                        AppleLioxAddittionalProperties::PROJECTS_TITLE => $project_titles,
                        AppleLioxAddittionalProperties::TASK_TYPE => $taskType,
                        AppleLioxAddittionalProperties::PROJECT_IDS_TODO => implode("-", $project_ids_todo),
                    );

                    $task->setAditionalProperties($this->collectProperties(null, $additional_prop_raw_data));

                    $this->newTasks[] = $task;
                    //Movemos el Mail a proccesed
                    if(!AI::getInstance()->getSetup()->development){
                        rename($mail, AppleLiox::$mail_success_directory . $mail_name);
                    }
                    unset($apple_mail);
                }
                else {
                    Functions::addLog("Cannot find WS ID in mail: $mail", Functions::WARNING, __CLASS__);
                    if ($times_processed >= self::TIMES_TO_REPROCCESS_MAIL) {
                        rename($mail, AppleLiox::$mail_damaged_directory . $mail_name);
                    } else {
                        $times_processed += 1;
                        rename($mail, AppleLiox::$appleliox_aps_mail_path . $mail_data_without_extension . "--" . $times_processed . "--.msg");
                    }
                }


            } catch(Throwable $t){
                Functions::logException($t->getMessage(), Functions::ERROR, __CLASS__, AIException::createInstanceFromThrowable($t));
                Functions::console("Mail process could not been completed. Mail file: $mail");
                continue;
            }
        }
        parent::collectTask();

        return (count($this->newTasks) > 0);
    }

    public function getAvailableTask(){
        $incoming_mails = glob(AppleLiox::$appleliox_aps_mail_path . "*.msg");

        foreach ($incoming_mails as $mail) {
            try {
                $apple_mail = new AppleLioxMail($mail);
                $mail_data = $apple_mail->analyzeMail();
            } catch (Throwable $ex2) {
                continue;
            }
            if (!$this->hasProjectId($mail_data)) continue;


            $projects_info = array();
            try {
                $projects_info = $this->appleConnectService->downloadProjects($mail_data["project_id"]);
            } catch (Throwable $e) {
                continue;
            }
            $analysis_list = array();
            $additional_instructions = "";
            $project_titles = array();
            $project_ids_todo = array();
            for ($i = 0; $i < count($projects_info); $i++) {
                $project_info = $projects_info[$i];
                $project_titles[] = "[" . $project_info["idWS"]. "]" . $project_info["basic_info"]->detail[1]->value;
                $project_ids_todo[] = $project_info["idWS"];
                if ($project_info == null) {
                    continue;
                }

                if (is_array($project_info["extended_info"]["transinstructions"])) {
                    foreach ($project_info["extended_info"]["transinstructions"] as $instruction) {
                        $additional_instructions .= $instruction . " ";
                    }
                }
                $analysis = $this->createAnalysisFromJson($project_info["wwc"]);
                if ($analysis == null) {
                    continue;
                }
                $analysis_list[] = $analysis;
            }



            $title = $title = $this->createTaskTitle($mail_data["subject"], $mail_data["project_id"]);

            $message = ($mail_data["additional_instructions"] != null && $mail_data["additional_instructions"] !== "") ? $mail_data["additional_instructions"] : " ";

            $idClientTask = $this->generateClientId($mail_data["project_id"]);

            try{
                $dueDate = $this->parseDeadLine($mail_data["deadline"]);
            } catch (ParseDateException $e){
                $dueDate = $this->getTodayEOBDate();
                $message = $this->appendDueDateParseError($message);
            }



            $project_titles = implode("\n", $project_titles);
            $edditing_scope = $mail_data["editting_scope"];
            $projectTeam = $mail_data["projectTeam"];
            $taskType = $mail_data["task"];

            $additional_prop_raw_data = array(
                AppleLioxAddittionalProperties::PROJECT_TEAM => $projectTeam,
                AppleLioxAddittionalProperties::EDDITING_SCOPE => $edditing_scope,
                AppleLioxAddittionalProperties::PROJECTS_TITLE => $project_titles,
                AppleLioxAddittionalProperties::TASK_TYPE => $taskType,
                AppleLioxAddittionalProperties::PROJECT_IDS_TODO => implode("-", $project_ids_todo),
            );

            $availableTask = new AvailableTask();
            $availableTask
                ->setIdClientTask($idClientTask)
                ->setDueDate($dueDate->format("Y-m-d H:i:s"))
                ->setTitle($title)
                ->setMessage($message)
                ->setAdditionalProperties($this->collectProperties(null, $additional_prop_raw_data));

            $sourceLanguage = parent::identifySourceLanguage($this->clearSourceLanguage("English (US)"));
            if ($sourceLanguage != null) {
                $availableTask->setSourceLanguage($sourceLanguage);
            }
            $targets = parent::identifyTargetLanguage(array("Spanish (Spain)"));
            if (isset($targets[0])) {
                $targets[0]->setAnalysis($this->joinAnalysis($analysis_list));
            }

            $availableTask->setSourceLanguage($sourceLanguage)
                ->setTargetsLanguage($targets);
            $this->availableTasks[] = $availableTask;

        }


        return parent::getAvailableTask();
    }

    /**
     * Check if a project is defined in the array
     * @param $mail_data
     * @return bool When it find at least one project id
     */
    private function hasProjectId($mail_data)
    {
        if (!isset($mail_data["project_id"]) || !is_array($mail_data["project_id"]))
            return false;
        return count($mail_data["project_id"]) > 0;
    }

    /**
     * Tries to create an instance of Analysis from given array
     * @param $json
     * @return Analysis|null return null if $json is null
     */
    protected function createAnalysisFromJson($json)
    {
        if (!is_array($json)) {
            return null;
        }


        $analysis = new Analysis();
        //$analysis->setPercentage_75($json["w75"]);
        $analysis->setNotMatch($json["w75"]);
        $analysis->setPercentage_85($json["w85"]);
        $analysis->setPercentage_95($json["w95"]);
        $analysis->setPercentage_100($json["w100"]);
        $analysis->setPercentage_101($json["w101"]);
        $analysis->setRepetition($json["repeat"]);
        $analysis->setTotalWords($json["total"]);
        $analysis->setWeightedWord($this->calculateWeightedWords($json));
        return $analysis;
    }

    /**
     * Calculates the WeightedWords. Formula got from PauHidalgo's mail
     * @param $json
     * @return int
     */
    protected function calculateWeightedWords($json)
    {
        $noMatch = 0;
        $repetitions = $json["repeat"];
        $w100 = $json["w100"];
        $w95 = $json["w95"];
        $w85 = $json["w85"];
        $w75 = $json["w75"];
        $WW = $noMatch * 1
            + ($repetitions + $w100) * 6 / 100
            + $w95 * 25 / 100
            + $w85 * 45 / 100
            + $w75 * 50 / 100;
        return $WW;
    }

    /**
     * Summarize all the analysis and return one
     * @param array $analysisArray
     * @return Analysis
     */
    private function joinAnalysis(array $analysisArray)
    {
        $analysis = new Analysis();
        foreach ($analysisArray as $wwc) {
            $analysis->setNotMatch($analysis->getNotMatch() + $wwc->getNotMatch());
            $analysis->setPercentage_75($analysis->getPercentage_75() + $wwc->getPercentage_75());
            $analysis->setPercentage_85($analysis->getPercentage_85() + $wwc->getPercentage_85());
            $analysis->setPercentage_95($analysis->getPercentage_95() + $wwc->getPercentage_95());
            $analysis->setPercentage_100($analysis->getPercentage_100() + $wwc->getPercentage_100());
            $analysis->setPercentage_101($analysis->getPercentage_101() + $wwc->getPercentage_101());
            $analysis->setRepetition($analysis->getRepetition() + $wwc->getRepetition());
            $analysis->setWeightedWord($analysis->getWeightedWord() + $wwc->getWeightedWord());
            $analysis->calculateTotalWords();
        }
        return $analysis;
    }

    /**
     * Generate UniqueClientId from the WorldServer project ids
     * @param array $list_ids
     * @return string
     */
    private function generateClientId(array $list_ids)
    {
        if (!is_array($list_ids)) {
            return uniqid("unknown_");
        }
        $res = "";
        for ($i = 0; $i < count($list_ids); $i++) {
            if ($i == count($list_ids) - 1) {
                $res .= $list_ids[$i];
            } else {
                $res = $res . $list_ids[$i] . "-";
            }
        }
        return $res;
    }

    /**
     * Tries to parse the deadline into DateTime object. The exception is handled but the error can be retrieved by the shutdown function.
     * @param string $deadline
     * @return String
     * @throws ParseDateException
     */
    protected function parseDeadLine(string $deadline)
    {
        if ($deadline == null) {
            throw new ParseDateException();
        }
        $date = $deadline;
        $date = htmlentities($date, null, 'utf-8');
        $date = str_replace("&nbsp;", "", $date);
        if (strpos($date, "CET") === false && strpos($date, "CEST") === false) {
            $date = $date . " Europe/Madrid";
        }
        $date = str_replace("EOB", "18:00", $date);
        if(strpos($date,"if not possible") != false){
            $re1='((?:Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday|Tues|Thur|Thurs|Sun|Mon|Tue|Wed|Thu|Fri|Sat))';	# Day Of Week 1
            $re2='.*?';	# Non-greedy match on filler
            $re3='((?:(?:[0-2]?\\d{1})|(?:[3][01]{1})))(?![\\d])';	# Day 1
            $re4='((?:[a-z][a-z]+))';	# Word 1
            $re5='.*?';	# Non-greedy match on filler
            $re6='((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))';	# Month 1
            $re7='.*?';	# Non-greedy match on filler
            $re8='(\\d+)';	# Integer Number 1
            $re9='((?:[a-z][a-z]+))';	# Word 2

            if ($c=preg_match_all ("/".$re1.$re2.$re3.$re4.$re5.$re6.$re7.$re8.$re9."/is", $date, $matches))
            {
                $dayofweek1=$matches[1][0];
                $day1=$matches[2][0];
                $word1=$matches[3][0];
                $month1=$matches[4][0];
                $int1=$matches[5][0];
                $word2=$matches[6][0];
                $date = "$dayofweek1, $day1$word1 $month1, $int1$word2";
            }else{
                $re6='(\\d+)';	# Integer Number 1
                $re7='((?:[a-z][a-z]+))';	# Word 2

                if ($c=preg_match_all ("/".$re1.$re2.$re3.$re4.$re5.$re6.$re7."/is", $date, $matches))
                {
                    $dayofweek1=$matches[1][0];
                    $day1=$matches[2][0];
                    $word1=$matches[3][0];
                    $month1 = date("F");
                    $int1=$matches[4][0];
                    $word2=$matches[5][0];
                    $date = "$dayofweek1, $day1$word1 $month1, $int1$word2";
                }
            }
        }

        try {
            $date = new DateTime($date);
            $date->setTimeZone(new DateTimeZone("UTC"));
            if ($date == null || $date == false) {
                throw new ParseDateException();
            }
        } catch (Throwable $e) { // Entraría aquí si es que encuentra un "Provide best TAT"
            error_clear_last();
            throw new ParseDateException();
        }
        return $date;//->format("Y-m-d H:i:s");
    }

    private function getTodayEOBDate(){
        try{
            $date = Functions::createFormatDate("today 18:00", AI::getInstance()->getSetup()->timeZone_TMS);
            return $date;
        }catch (Throwable $e){
            return (new DateTime());//->format("Y-m-d H:i:s");
        }
    }

    /**
     * Convert DateTimeObjet into a readable String
     * @param DateTime $date
     * @return string
     */
    protected function dateTimeToString(DateTime $date)
    {
        return $date->format("Y-m-d H:i:s");
    }

    /**
     * Tries to clean the SourceLanguage string found in mail but there are many scenarios and is no 100% bullet proof
     * @param string $source_language
     * @return string
     */
    protected function clearSourceLanguage(string $source_language)
    {
        $res = $source_language;
        $res = str_replace(" + Change report", "", $res);
        if ($res === "") {
            return "English (US)";
        }
        return $res;
    }

    /**
     * Create the AdditionalPropertie instances for every task
     * @param array $values
     * @param array $data
     * @return array|TaskAditionalProperties[]
     */
    protected function collectProperties($values, &$data)
    {
        $properties = array();
        foreach ($this->source->getAdditionalProperties() as $sourceProp) {
            try {
                if(isset($data[$sourceProp->getPropertyName()])){
                    $propiedad = $data[$sourceProp->getPropertyName()];
                    if($propiedad != null){
                        $value = (isset($data[$sourceProp->getPropertyName()]) ? $data[$sourceProp->getPropertyName()] : "-");
                        $tProp = new TaskAditionalProperties();
                        $tProp->setPropertyName($sourceProp->getPropertyName());
                        $tProp->setPropertyValue(trim($value));
                        $properties[] = $tProp;
                    }
                }
            } catch (Exception $e) {
                parent::addLog($e->getMessage(), Functions::WARNING, json_encode($e->getTrace()));
            }
        }
        return $properties;
    }

    private function createTaskTitle($emailSubject, $project_id_list){
        $title = str_replace("/", "-", $emailSubject);
        $title = str_ireplace("re:", "", $title);
        $title = str_replace(":", "", $title);
        $title = str_ireplace("_hand_off_", "", $title);
        $title = str_ireplace("_hand_off", "", $title);
        $title = str_ireplace("hand_off_", "", $title);
        $title = str_ireplace("hand_off", "", $title);
        $title = str_ireplace("_handoff_", "", $title);
        $title = str_ireplace("handoff_", "", $title);
        $title = str_ireplace("_handoff", "", $title);
        $title = str_ireplace("handoff", "", $title);
        $title = str_ireplace("due:", "", $title);
        $title = str_ireplace("due", "", $title);
        $title = str_ireplace("[", "", $title);
        $title = str_ireplace("]", "", $title);

        $titleIds = "";
        if (is_array($project_id_list)) {
            foreach ($project_id_list as $id) {
                $titleIds .= $id . "_";
                $title = str_replace($id, "", $title);//removing the ID from subject
            }
            $titleIds = rtrim($titleIds, '_');
        }
        $title = substr($title, 0, 44 - strlen($titleIds));
        return $title . "-" . $titleIds;
    }

    
    /**
     * Generates a FS file with the instructions.
     * Returns true if success, false otherwise.
     * @param string $fileName
     * @param string $instructions
     * @return boolean
     */
    private function createInstructionsFile(string $fileName, string $instructions){
        $string = substr($instructions, 0, strpos($instructions, '</html>') + strlen('</html>'));

        $file = fopen($fileName, "w");
        if ($file !== false){
        	fwrite($file, $string);
        	fclose($file);
        	return true;
        }
        
        return false;
    }
    protected function isNewClientId($id_task){
        $taskDao = new TaskDAO();
        try {
            $isNoTInDB = $taskDao->validateTask($id_task);
        } catch (AIException $e) {
            return true;
        }
        $isInNewTask = $this->isInNewTask($id_task);
        return $isNoTInDB && !$isInNewTask;
    }

    private function isInNewTask($id_task){
        foreach ($this->newTasks as $task){
            if($task->getIdClientetask() == $id_task){
                return true;
            }
        }
        return false;
    }

    private function appendDueDateParseError(string $text){
        $dateNotFoundString = "DEADLINE NOT FOUND, CHECK DEADLINE";
        $newText = $text;
        return trim($newText) . "-" . $dateNotFoundString;
    }
}