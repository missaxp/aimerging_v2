<?php

/**
 * @author mhernandez
 * @version 1.0
 * created on 8 mar 2019
 */

namespace harvestModule\sources;

use common\exceptions\AIException;
use common\exceptions\ParseDateException;
use common\exceptions\sources\apple_liox\ProjectIdNotFoundException;
use common\exceptions\sources\apple_liox\WorldServerException;
use common\exceptions\sources\LTBDownloaderException;
use common\ZipFile;
use core\AI;
use dataAccess\dao\TaskDAO;
use DateTime;
use DateTimeZone;
use Exception;
use Functions;
use harvestModule\sources\connectService\AppleLioxConnect;
use harvestModule\sources\connectService\AppleLioxMail;
use harvestModule\sources\MailProperties;
use harvestModule\sources\sourceComplements\LTBDownloader;
use harvestModule\sources\sourceComplements\WorldServerConnect;
use common\exceptions\sources\apple_liox\MailException;
use model\Analysis;
use model\AvailableTask;
use model\File;
use model\Source;
use model\State;
use model\Task;
use model\DataSource;
use model\TaskAditionalProperties;
use TaskError;
use TaskPredefinedError;
use Throwable;

include_once BaseDir . '/dataAccess/dao/LanguageDAO.php';
include_once BaseDir . '/harvestModule/sources/StructureSource.php';
include_once BaseDir . '/harvestModule/sources/connectService/AppleLioxMail.php';
include_once BaseDir . '/harvestModule/sources/AppleLiox.php';
include_once BaseDir . '/harvestModule/sources/AppleWSUtils.php';
include_once BaseDir . '/harvestModule/sources/connectService/AppleLioxConnect.php';
include_once BaseDir . '/model/Task.php';
include_once BaseDir . '/Functions.php';
include_once BaseDir . '/common/ZipFile.php';
include_once BaseDir . '/harvestModule/sources/sourceComplements/LTBDownloader.php';
include_once BaseDir . "/common/exceptions/sources/LTBDownloaderException.php";
include_once BaseDir . "/common/exceptions/AIException.php";
include_once BaseDir . "/common/exceptions/ParseDateException.php";
include_once BaseDir . '/dataAccess/dao/TaskDAO.php';
include_once BaseDir . "/model/AvailableTask.php";

class AppleWS extends StructureSource{
    public static $repository_path = "AppleLioxWS/";
    /**
     * @var String Mails path
     */
    public static $source_mails_path;
    /**
     * @var WorldServerConnect|null Should have an instance to share the cookies and authorization token
     */
    protected $appleConnectService = null;
    /**
     * Type of harvestMethod to define in the task creation
     */
    private const HARVESMETHOD = 'Email';
    /**
     * @var String path to save the mails that where parsed correctly
     */
    private static $mail_success_directory;
    /**
     * @var String path to save the mails that where parsed incorrectly
     */
    private static $mail_damaged_directory;
    /**
     * Define how many times a mail must be proccess to be moved to damaged
     */
    private const TIMES_TO_REPROCCESS_MAIL = 5;

    /**
     * AppleLiox constructor.
     * @param array $users User list to connect to the world server. In this stage only one user is used.
     * @param Source $source The source
     * @throws Exception When it can't log in the WorldServer
     */
    public function __construct(array $users, Source $source)
    {
        parent::__construct($users, $source);

        self::$source_mails_path = $this->development ? '/webs/ai/apple_mails/' : '/ai/sources/apple_lionbridge/';
        self::$mail_success_directory = $this->development ? '/webs/ai/apple_mails/processed/' : '/ai/sources/apple_lionbridge/processed/';
        self::$mail_damaged_directory = $this->development ? '/webs/ai/apple_mails/damaged/' : '/ai/sources/apple_lionbridge/damaged/';

        //We get the first element because we only have one user and we don't know how to detect diferents mail addressee
        if (count($users) != 1) {
            throw (new AIException("Unsupported number of user for AppleLiox"))
                ->construct(__FUNCTION__, __NAMESPACE__, $ARGS = func_get_args());
        }
        try {
            $this->appleConnectService = new AppleLioxConnect($users[0]);
            $this->appleConnectService->startConnection();
        } catch(AIException $e){
            throw $e;
        } catch (Throwable $e) {
            throw (new AIException("Imposible crear la instancia del conector. Mensaje de error: " . $e->getMessage()))
                ->construct(__FUNCTION__, __NAMESPACE__, $args = func_get_args());
        }
    }

    public function collectTask(){
        $incoming_mails = glob(self::$source_mails_path . "*.msg");
        foreach ($incoming_mails as $mail) {
            $this->collectTaskFromMail($mail);
        } // end of foreach processing mail
        parent::collectTask();
        return (count($this->newTasks) > 0);
    }

    public function collectTaskByIdClientTask($idTask)
    {
        $mail = $this->getMailFromIdClientTask($idTask);
        if ($mail === false) return (count($this->newTasks) > 0);

        $this->collectTaskFromMail($mail);

        parent::collectTask();
        return (count($this->newTasks) > 0);
    }

    public function getAvailableTask(){
        $incoming_mails = glob(self::$source_mails_path . "*.msg");
        foreach ($incoming_mails as $mail) {
            $mailProperties = MailProperties::getMailProperiesFromMailFile($mail);
            $appleMail = $this->getAppleMailInstance($mailProperties, true);
            if($appleMail === null) continue;

            try {
                $mail_data = $appleMail->analyzeMail();
            } catch (MailException $e) {
                $this->moveFile($mailProperties, true);
                Functions::logErrorToDB($e->getMessage() . " en mail: " . $mailProperties->getFileName(), Functions::INFO, __CLASS__);
                continue;
            }

            if(!$this->hasProjectId($mail_data)){
                Functions::logErrorToDB("Cannot find WS ID in mail: " . $mailProperties->getFileName(), Functions::WARNING, __CLASS__);
                $this->moveFile($mailProperties, true);
                continue;
            }

            try {
                $projects_info = $this->appleConnectService->getProjectInfo($mail_data["project_id"]);
            } catch (AIException $e) {
                Functions::logException($e->getMessage(), Functions::ERROR, __CLASS__, $e);
                $this->tryToReprocessMail($mailProperties);
                continue;
            }

            $taskInfoStructured = $this->getTaskStructuredInfoFromWSProjectInfo($projects_info);

            $targetLanguagesFinded = array_keys($taskInfoStructured);
            foreach ($targetLanguagesFinded as $targetLanguage){
                //Creating TASK Object
                $task = new AvailableTask();

                $message = ($mail_data["additional_instructions"] != null && $mail_data["additional_instructions"] !== "") ? $mail_data["additional_instructions"] : " ";
                $message .= $taskInfoStructured[$targetLanguage]["instructions"];

                try{
                    $dueDate = $this->parseDeadLine($mail_data["deadline"]);
                } catch (ParseDateException $e){
                    $dueDate = $this->getTodayEOBDate();
                    $message = $this->appendDueDateParseError($message);
                }

                $title = $this->createTaskTitle($mail_data["subject"], $mail_data["project_id"], $targetLanguage);
                $idClientTask = $this->generateClientId($mail_data["project_id"], $targetLanguage);
                if(!$this->isNewClientId($idClientTask)){
                    Functions::logErrorToDB("Task already registered: $idClientTask", Functions::INFO, __CLASS__);
                    $this->moveFile($mailProperties, true);
                    continue;
                }

                $task->setSourceLanguage(parent::identifySourceLanguage("English (US)"));
                $targets = parent::identifyTargetLanguage(array($targetLanguage));
                if (isset($targets[0])) {
                    $targets[0]->setAnalysis($this->joinAnalysis($taskInfoStructured[$targetLanguage]['wwc']));
                }
                $task->setTargetsLanguage($targets);

                $project_titles = implode("<br>", $taskInfoStructured[$targetLanguage]['project_titles']);
                $projectTeam = $taskInfoStructured[$targetLanguage]['project_team'];
                $project_ids_todo = $taskInfoStructured[$targetLanguage]['project_ids'];
                $edditing_scope = $mail_data["editting_scope"];
                $taskType = $mail_data["task"];

                $additional_prop_raw_data = array(
                    AppleLioxProperties::PROJECT_TEAM => $projectTeam,
                    AppleLioxProperties::EDDITING_SCOPE => $edditing_scope,
                    AppleLioxProperties::PROJECTS_TITLE => $project_titles,
                    AppleLioxProperties::TASK_TYPE => $taskType,
                    AppleLioxProperties::PROJECT_IDS_TODO => implode("-", $project_ids_todo),
                );

                $task
                    ->setIdClientTask($idClientTask)
                    ->setDueDate($dueDate->format("Y-m-d H:i:s"))
                    ->setTitle($title)
                    ->setMessage($message)
                    ->setAdditionalProperties($this->collectProperties(null, $additional_prop_raw_data));
                $this->availableTasks[] = $task;
            }

        } // end of foreach processing mail


        return parent::getAvailableTask();
    }

    private function collectTaskFromMail($mailPath){
        $mailProperties = MailProperties::getMailProperiesFromMailFile($mailPath);
        $appleMail = $this->getAppleMailInstance($mailProperties, true);
        $taskWarnings = [];
        if($appleMail === null) return;

        try {
            $mail_data = $appleMail->analyzeMail();
        } catch (MailException $e) {
            $this->moveFile($mailProperties, true);
            Functions::logErrorToDB($e->getMessage() . " en mail: " . $mailProperties->getFileName(), Functions::INFO, __CLASS__);
            return;
        }

        if(!$this->hasProjectId($mail_data)){
            Functions::logErrorToDB("Cannot find WS ID in mail: " . $mailProperties->getFileName(), Functions::WARNING, __CLASS__);
            $this->moveFile($mailProperties, true);
            return;
        }

        try {
            $projects_info = $this->appleConnectService->downloadProjects($mail_data["project_id"]);
        } catch (AIException $e) {
            Functions::logException($e->getMessage(), Functions::ERROR, __CLASS__, $e);
            $this->tryToReprocessMail($mailProperties);
            return;
        }

        $taskInfoStructured = $this->getTaskStructuredInfoFromWSProjectInfo($projects_info);


        $ltbFile = [];
        try {
            $ltbFile = $this->getLTBFile($mail_data["ltb_configurations_link"]);
        } catch (LTBDownloaderException $e) {
            if ($mailProperties->getTimesProcessed() >= self::TIMES_TO_REPROCCESS_MAIL) {
                Functions::logException("LTB File not found. Task will be collected without this file", Functions::WARNING, __CLASS__, $e);
                $taskWarnings[] = $this->createTaskErrorInstanceByPredefinedErrorID(2);
            } else {
                $this->tryToReprocessMail($mailProperties);
                return;
            }
        }

        $mail_attachments = $this->getMailAttachments($mail_data);


        $instructionsHTML = $this->getHTMLFile($mailProperties, $appleMail, $mail_data);
        if($instructionsHTML === null){
            $taskWarnings[] = $this->createTaskErrorInstanceByPredefinedErrorID(3);
        }

        $targetLanguagesFinded = array_keys($taskInfoStructured);
        foreach ($targetLanguagesFinded as $targetLanguage){
            $filesToCompress = [];
            $filesToCompress[] = $instructionsHTML;
            foreach ($mail_attachments as $file){
                $filesToCompress[] = $file;
            }
            foreach ($ltbFile as $file){
                $filesToCompress[] = $file;
            }
            foreach ($taskInfoStructured[$targetLanguage]["reference_files"] as $file){
                $filesToCompress[] = $file["path"];
            }
            foreach ($taskInfoStructured[$targetLanguage]["filesToTranslate"] as $file){
                $filesToCompress[] = $file["path"];
            }

            $destinationFolder = AI::getInstance()->getSetup()->repository . self::$repository_path . uniqid() . "/";
            if (!file_exists($destinationFolder)) {
                mkdir($destinationFolder);
            }
            $zipFileName = "taskFiles.zip";
            $destinationZipFile = $destinationFolder . $zipFileName;
            $zipFilewasCreated = false;
            try {
                $this->createZipFile($filesToCompress, $destinationZipFile);
                $zipFilewasCreated = true;
            } catch (AIException $e) {
                Functions::logException($e->getMessage(), Functions::ERROR, __CLASS__, $e);
                $taskWarnings[] = $this->createTaskErrorInstanceByPredefinedErrorID(4);
            }

            //Creating TASK Object
            $task = new Task();
            if($zipFilewasCreated){
                $file = new File();
                $file->construct(
                    "" . $zipFileName,
                    "zip",
                    null,
                    null,
                    Functions::currentDate(),
                    null,
                    null,
                    "AppleWorldServer",
                    File::REFERENCE,
                    true,
                    filesize($destinationZipFile)
                );
                $file->setPath($destinationZipFile);
                $task->addFileForReference($file);
            }

            $message = ($mail_data["additional_instructions"] != null && $mail_data["additional_instructions"] !== "") ? $mail_data["additional_instructions"] : " ";
            $message .= $taskInfoStructured[$targetLanguage]["instructions"];

            try{
                $dueDate = $this->parseDeadLine($mail_data["deadline"]);
            } catch (ParseDateException $e){
                $dueDate = $this->getTodayEOBDate();
                $message = $this->appendDueDateParseError($message);
            }
            $task->setMenssage($message);

            $title = $this->createTaskTitle($mail_data["subject"], $mail_data["project_id"], $targetLanguage);
            $idClientTask = $this->generateClientId($mail_data["project_id"], $targetLanguage);
            if(!$this->isNewClientId($idClientTask)){
                Functions::logErrorToDB("Task already registered: $idClientTask", Functions::INFO, __CLASS__);
                $this->moveFile($mailProperties, true);
                continue;
            }
            $task->construct(
                Functions::currentDate(),
                $idClientTask,
                $title,
                $message,
                Functions::currentDateAsObject(),
                $dueDate,
                ""
            );
            $task->setSourceLanguage(parent::identifySourceLanguage("English (US)"));
            $targets = parent::identifyTargetLanguage(array($targetLanguage));
            if (isset($targets[0])) {
                $targets[0]->setAnalysis($this->joinAnalysis($taskInfoStructured[$targetLanguage]['wwc']));
            }
            $task->setTargetsLanguage($targets);

            $dataSource = new DataSource();
            $dataSource->construct(
                $this->source->getSourceName(),
                AI::getInstance()->getSetup()->execution_type,
                Functions::currentDate(),
                self::HARVESMETHOD,
                WorldServerConnect::BASE_URL,
                "",
                file_get_contents($mailPath),
                $this->source->getIdSource(),
                $this->users[0]->getIdApiUser()
            );
            //Removing he original data on development to improve reading process
            if(AI::getInstance()->getSetup()->development){
                $dataSource->setOriginalData("");
            }
            $task->setDataSource($dataSource);
            $task->setState(new State(State::COLLECTED));

            $task->generateUniqueTaskId();
            $task->setStartDate(Functions::currentDateAsObject());
            $task->setTimeStamp(Functions::currentDate());
            $task->generateUniqueSourceId();
            $task->setTaskErrors($taskWarnings);

            $project_titles = implode("<br>", $taskInfoStructured[$targetLanguage]['project_titles']);
            $projectTeam = $taskInfoStructured[$targetLanguage]['project_team'];
            $project_ids_todo = $taskInfoStructured[$targetLanguage]['project_ids'];
            $edditing_scope = $mail_data["editting_scope"];
            $taskType = $mail_data["task"];

            $additional_prop_raw_data = array(
                AppleLioxProperties::PROJECT_TEAM => $projectTeam,
                AppleLioxProperties::EDDITING_SCOPE => $edditing_scope,
                AppleLioxProperties::PROJECTS_TITLE => $project_titles,
                AppleLioxProperties::TASK_TYPE => $taskType,
                AppleLioxProperties::PROJECT_IDS_TODO => implode("-", $project_ids_todo),
            );

            $task->setAditionalProperties($this->collectProperties(null, $additional_prop_raw_data));
            $this->newTasks[] = $task;
        }

        //Movemos el Mail a proccesed
        if(!AI::getInstance()->isDev()){
            $this->moveFile($mailProperties);
        }
        unset($apple_mail);
    }

    private function getTaskStructuredInfoFromWSProjectInfo($projects_info){
        $taskInfoStructured = [];
        for ($i = 0; $i < count($projects_info); $i++) {
            $project_info = $projects_info[$i];

            foreach ($project_info["wwc"] as $wwc){
                $targetLanguage = $wwc["target_language"];
                if(empty($taskInfoStructured[$targetLanguage])){
                    $taskInfoStructured[$targetLanguage ] = [
                        "reference_files" => [],
                        "filesToTranslate" => [],
                        "instructions" => "",
                        "project_ids" => [],
                        "project_titles" => [],
                        "wwc" => [],
                        "project_team" => "",
                    ];
                }

                $taskInfoStructured[$targetLanguage]["project_ids"][] = $project_info["idWS"];
                $taskInfoStructured[$targetLanguage]["project_titles"][] = "[" . $project_info["idWS"]. "]" . $project_info["basic_info"]->detail[1]->value;
                $taskInfoStructured[$targetLanguage]["project_team"] = $project_info["basic_info"]->detail[3]->value;
                $taskInfoStructured[$targetLanguage]["wwc"][] = $wwc;
                foreach ($project_info["files"]["REFERENCE"] as $referenceFile){
                    $taskInfoStructured[$targetLanguage]["reference_files"][] = $referenceFile;
                }

                if (is_array($project_info["extended_info"]["transinstructions"])) {
                    foreach ($project_info["extended_info"]["transinstructions"] as $instruction) {
                        $taskInfoStructured[$targetLanguage]["instructions"] .= $instruction . " ";
                    }
                }

                foreach ($project_info["files"]["files_to_translate"] as $fileToTranslate){
                    switch ($targetLanguage){
                        case "Spanish (Spain)":
                            if (stripos($fileToTranslate["file_name"], "es_ES") !== false){
                                $taskInfoStructured[$targetLanguage]["filesToTranslate"][] = $fileToTranslate;
                            }
                            break;
                        case "Catalan":
                            if (stripos($fileToTranslate["file_name"], "ca_ES") !== false){
                                $taskInfoStructured[$targetLanguage]["filesToTranslate"][] = $fileToTranslate;
                            }
                            break;
                    }
                }
            }
        }
        return $taskInfoStructured;
    }

    private function tryToReprocessMail(MailProperties $mailProperties){
        if($mailProperties->getTimesProcessed() > self::TIMES_TO_REPROCCESS_MAIL){
            $this->moveFile($mailProperties, true);
        } else {
            $times_processed = $mailProperties->getTimesProcessed() + 1;
            rename($mailProperties->getFullFileName(), self::$source_mails_path. $mailProperties->getFileWithoutExtension() . "--" . $times_processed . "--.msg");
        }
    }

    private function moveFile(MailProperties $mailProperties, bool $isDamaged = false){
        if(AI::getInstance()->isDev()) {
            Functions::echo("Moving file " . $mailProperties->getFileName() . " as " . $isDamaged ? "Damaged" : "Processed");
            return;
        }
        if($isDamaged){
            rename($mailProperties->getFullFileName(), self::$mail_damaged_directory . $mailProperties->getFileName());
        } else {
            rename($mailProperties->getFullFileName(), self::$mail_success_directory. $mailProperties->getFileName());
        }
    }

    private function getAppleMailInstance(MailProperties $mailProperties, bool $isAppleLiox = true){
        $result = null;
        try {
            $result = new AppleLioxMail($mailProperties->getFullFileName());
        } catch (MailException $e) {
            $this->moveFile($mailProperties, true);
            Functions::logErrorToDB($e->getMessage() . " en mail: " . $mailProperties->getFileName(), Functions::INFO, __CLASS__);
        } catch (ProjectIdNotFoundException $ex) {
            $this->moveFile($mailProperties, true);
            Functions::logErrorToDB("Cannot find WS project ID en mail:" . $mailProperties->getFileName() . $ex->getMessage(), Functions::WARNING, __CLASS__);
        } catch (Throwable $ex2) {
            $ex2 = (AIException::createInstanceFromThrowable($ex2))->construct(__FUNCTION__, __NAMESPACE__, $args = func_get_args());
            $this->moveFile($mailProperties, true);
            Functions::logException("Cannot parse mail, reason:" . $ex2->getMessage() . " en mail: " . $mailProperties->getFileName(), Functions::ERROR, __CLASS__, $ex2);
        }
        return $result;
    }

    private function hasProjectId($mail_data) {
        if (!isset($mail_data["project_id"]) || !is_array($mail_data["project_id"]))
            return false;
        return count($mail_data["project_id"]) > 0;
    }

    /**
     * @param $ltb_configurations_link
     * @return array
     * @throws LTBDownloaderException
     */
    private function getLTBFile($ltb_configurations_link){
        $ltbFiles = [];

        if ($ltb_configurations_link != "") {
            $destinationPath = self::$repository_path . "ltb/";

            $ltbLink = $ltb_configurations_link;
            $ltbLink = str_replace("\n", "", $ltbLink);
            $ltbLink = htmlentities($ltbLink, ENT_QUOTES | ENT_IGNORE, "UTF-8");
            $ltbLink = str_replace('&nbsp;','',$ltbLink);

            $ltbDownloader = new LTBDownloader($ltbLink, $destinationPath);
            $files = $ltbDownloader->downloadLTB();
            foreach ($files as $file) {
                $ltbFiles[] = AI::getInstance()->getSetup()->repository . $file["path"];
            }
        }
        return $ltbFiles;
    }

    private function getHTMLFile(MailProperties $mailProperties, AppleLioxMail $apple_mail, array $mail_data){
        $uniqid = uniqid();
        $destinationFolder = AI::getInstance()->getSetup()->repository . self::$repository_path . $uniqid . "/";
        if (!file_exists($destinationFolder)) {
            mkdir($destinationFolder);
        }
        $destinationHTMLFile = $destinationFolder . "instructions.html";
        $htmlMail = $apple_mail->getBodyHTML();
        $htmlMail = str_replace($mail_data["deadline"], "", $htmlMail);
        $htmlMail = preg_replace('/[[:cntrl:]]/', '', $htmlMail);

        if ($this->createInstructionsFile($destinationHTMLFile, $htmlMail)) {
            return $destinationHTMLFile;
        } else {
            $aiex = (new AIException("Cannot create HTML File"))->construct(__METHOD__, __NAMESPACE__, $f = func_get_args());
            Functions::logException($aiex->getMessage(), Functions::ERROR, __CLASS__, $aiex);
        }
        return null;
    }

    private function createInstructionsFile(string $fileName, string $instructions){
        $string = substr($instructions, 0, strpos($instructions, '</html>') + strlen('</html>'));
        $file = fopen($fileName, "w");
        if ($file !== false){
            fwrite($file, $string);
            fclose($file);
            return true;
        }
        return false;
    }


    private function createTaskErrorInstanceByPredefinedErrorID(int $idPredefinedError): TaskError{
        $taskPredefinedError = new TaskPredefinedError();
        $taskPredefinedError->setIdPredefinedError($idPredefinedError);
        $taskError = new TaskError();
        $taskError->setLocation(__CLASS__);
        $taskError->setTimeStamp(new DateTime());
        $taskError->setPredefinedError($taskPredefinedError);
        return $taskError;
    }

    private function getMailAttachments(array $mail_data){
        $mail_attachments = [];
        if(is_array($mail_data["attachments"])){
            foreach ($mail_data["attachments"] as $attachment){
                $mail_attachments[] = $attachment;
            }
        }
        return $mail_attachments;
    }

    /**
     * @param array $filesToCompress
     * @throws AIException
     */
    private function createZipFile(array $filesToCompress, $destinationFileName){
        ZipFile::create_zip($filesToCompress, "" . $destinationFileName);
    }

    /**
     * @param $deadline
     * @return mixed|string
     * @throws ParseDateException
     */
    private function parseDeadLine($deadline){
        if ($deadline == null) {
            throw new ParseDateException();
        }
        $date = $deadline;
        $date = htmlentities($date, null, 'utf-8');
        $date = str_replace("&nbsp;", "", $date);
        if (strpos($date, "CET") === false && strpos($date, "CEST") === false) {
            $date = $date . " Europe/Madrid";
        }
        $date = str_replace("EOB", "18:00", $date);
        if(strpos($date,"if not possible") != false){
            $re1='((?:Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday|Tues|Thur|Thurs|Sun|Mon|Tue|Wed|Thu|Fri|Sat))';	# Day Of Week 1
            $re2='.*?';	# Non-greedy match on filler
            $re3='((?:(?:[0-2]?\\d{1})|(?:[3][01]{1})))(?![\\d])';	# Day 1
            $re4='((?:[a-z][a-z]+))';	# Word 1
            $re5='.*?';	# Non-greedy match on filler
            $re6='((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))';	# Month 1
            $re7='.*?';	# Non-greedy match on filler
            $re8='(\\d+)';	# Integer Number 1
            $re9='((?:[a-z][a-z]+))';	# Word 2

            if ($c=preg_match_all ("/".$re1.$re2.$re3.$re4.$re5.$re6.$re7.$re8.$re9."/is", $date, $matches))
            {
                $dayofweek1=$matches[1][0];
                $day1=$matches[2][0];
                $word1=$matches[3][0];
                $month1=$matches[4][0];
                $int1=$matches[5][0];
                $word2=$matches[6][0];
                $date = "$dayofweek1, $day1$word1 $month1, $int1$word2";
            }else{
                $re6='(\\d+)';	# Integer Number 1
                $re7='((?:[a-z][a-z]+))';	# Word 2

                if ($c=preg_match_all ("/".$re1.$re2.$re3.$re4.$re5.$re6.$re7."/is", $date, $matches))
                {
                    $dayofweek1=$matches[1][0];
                    $day1=$matches[2][0];
                    $word1=$matches[3][0];
                    $month1 = date("F");
                    $int1=$matches[4][0];
                    $word2=$matches[5][0];
                    $date = "$dayofweek1, $day1$word1 $month1, $int1$word2";
                }
            }
        }

        try {
            $date = new DateTime($date);
            $date->setTimeZone(new DateTimeZone("UTC"));
            if ($date == null || $date == false) {
                throw new ParseDateException();
            }
        } catch (Throwable $e) { // Entraría aquí si es que encuentra un "Provide best TAT"
            error_clear_last();
            throw new ParseDateException();
        }
        return $date;
    }

    private function getTodayEOBDate(){
        try{
            $date = Functions::createFormatDate("today 18:00", AI::getInstance()->getSetup()->timeZone_TMS);
            return $date;
        }catch (Throwable $e){
            return (new DateTime());
        }
    }

    private function appendDueDateParseError(string $message){
        $dateNotFoundString = "DEADLINE NOT FOUND, CHECK DEADLINE";
        $newText = $message;
        return trim($newText) . "-" . $dateNotFoundString;
    }

    private function createTaskTitle($subject, $project_id_list, $targetLang){
        $title = str_replace("/", "-", $subject);
        $title = str_ireplace("re:", "", $title);
        $title = str_replace(":", "", $title);
        $title = str_ireplace("_hand_off_", "", $title);
        $title = str_ireplace("_hand_off", "", $title);
        $title = str_ireplace("hand_off_", "", $title);
        $title = str_ireplace("hand_off", "", $title);
        $title = str_ireplace("_handoff_", "", $title);
        $title = str_ireplace("handoff_", "", $title);
        $title = str_ireplace("_handoff", "", $title);
        $title = str_ireplace("handoff", "", $title);
        $title = str_ireplace("due:", "", $title);
        $title = str_ireplace("due", "", $title);
        $title = str_ireplace("[", "", $title);
        $title = str_ireplace("]", "", $title);

        $titleIds = "";
        $lang = "es_ES";
        switch ($targetLang){
            case "Spanish (Spain)":
                $lang = "es_ES";
                break;
            case "Catalan":
                $lang = "ca_ES";
                break;
        }
        if (is_array($project_id_list)) {
            foreach ($project_id_list as $id) {
                $titleIds .= $id . "_";
                $title = str_replace($id, "", $title);//removing the ID from subject
            }
            $titleIds = rtrim($titleIds, '_');
        }

        $title = substr($title, 0, 44 - strlen($lang. $titleIds));
        return $title . "-" . $titleIds . $lang;
    }

    private function generateClientId($list_ids, $targetLanguage = "Spanish (Spain)"){
        if (!is_array($list_ids)) {
            return uniqid("unknown_");
        }
        $res = "";
        for ($i = 0; $i < count($list_ids); $i++) {
            if ($i == count($list_ids) - 1) {
                $res .= $list_ids[$i];
            } else {
                $res = $res . $list_ids[$i] . "-";
            }
        }
        $lang = "es_ES";
        switch ($targetLanguage){
            case "Spanish (Spain)":
                $lang = "es_ES";
                break;
            case "Catalan":
                $lang = "ca_ES";
                break;
        }
        return $res." ".$lang;
    }

    private function isNewClientId(string $idClientTask){
        $taskDao = new TaskDAO();
        try {
            $isNoTInDB = $taskDao->validateTask($idClientTask);
        } catch (AIException $e) {
            return true;
        }
        $isInNewTask = $this->isInNewTask($idClientTask);
        return $isNoTInDB && !$isInNewTask;
    }

    private function isInNewTask($id_task){
        foreach ($this->newTasks as $task){
            if($task->getIdClientetask() == $id_task){
                return true;
            }
        }
        return false;
    }

    private function joinAnalysis($wwc){
        $res = [
            "w101" => 0,
            "w100" => 0,
            "w95" => 0,
            "w85" => 0,
            "w75" => 0,
            "repeat" => 0,
            "total" => 0
        ];

        foreach ($wwc as $item){
            $res["w101"] += $item["w101"];
            $res["w100"] += $item["w100"];
            $res["w95"] += $item["w95"];
            $res["w85"] += $item["w85"];
            $res["w75"] += $item["w75"];
            $res["repeat"] += $item["repeat"];
            $res["total"] += $item["total"];
        }

        return $this->WSwwcToAnalysis($res);
    }

    private function WSwwcToAnalysis(array $wwc){
        if (!is_array($wwc)) {
            return null;
        }
        $analysis = new Analysis();
        $analysis->setNotMatch($wwc["w75"]);
        $analysis->setPercentage_85($wwc["w85"]);
        $analysis->setPercentage_95($wwc["w95"]);
        $analysis->setPercentage_100($wwc["w100"]);
        $analysis->setPercentage_101($wwc["w101"]);
        $analysis->setRepetition($wwc["repeat"]);
        $analysis->setTotalWords($wwc["total"]);
        $analysis->calculateWeightedWords();
        return $analysis;
    }

    private function getMailFromIdClientTask($idTask){
        $incoming_mails = glob(self::$source_mails_path . "*.msg");
        foreach ($incoming_mails as $mail) {
            try{
                $apple_mail = new AppleLioxMail($mail);
                $mail_data = $apple_mail->analyzeMail();
                $idClientTaskMail = $this->generateClientId($mail_data["project_id"]);
                /*
                 * We delete the char of the anguage because we get it from WS
                 * */
                $idClientTaskMailSub = substr($idClientTaskMail, 0, strlen($idClientTaskMail)-4);
                $idTaskSub = substr($idTask, 0, strlen($idTask)-4);
                if($idClientTaskMailSub == $idTaskSub){
                    return $mail;
                }
            } catch(Throwable $t){
                continue;
            }
        }
        return false;
    }
}