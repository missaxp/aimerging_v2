<?php

/**
 * @author ljimenez
 * @version 1.0
 * created on 2 jul. 2018
 */
namespace harvestModule\sources;

use DateTime;
use model\AvailableTask;
use model\File;
use model\Task;
use model\Source;
use model\State;
use core\AI;
use model\Analysis;
use model\DataSource;
use Functions;
use harvestModule\sources\connectService\WebServiceMoravia_V4;
use model\TaskAditionalProperties;
use Throwable;

include_once BaseDir. '/model/Task.php';
include_once BaseDir. '/model/Source.php';
include_once BaseDir. '/model/APIUser.php';
include_once BaseDir. '/model/State.php';
include_once BaseDir. '/model/Analysis.php';
include_once BaseDir. '/model/DataSource.php';
include_once BaseDir. '/model/File.php';
include_once BaseDir. '/harvestModule/sources/connectService/basicHttpClient.php';
include_once BaseDir. '/harvestModule/sources/connectService/WebServiceMoravia_V4.php';
include_once BaseDir. '/Functions.php';

class SymfonieMoravia extends StructureSource{
	
	const URL = 'https://projects.moravia.com/';
	const HARVESTMETHOD = 'API';
	
	/**
	 *
	 * @var TaskAditionalProperties[]
	 */
	private $properties;
	
	
	private $moraviaAPI = null;
	
	/**
	 *
	 * @var int
	 */
	private $currentUser;
	
	private $isAmazonTask = false;
	
	public function __construct(array $users, Source $source){
		parent::__construct($users, $source);
		$this->properties = array();
	}
	
	public function acceptTask(Task $task) {
		$successfull = false;
		if(count($this->users) == 1){
			$moravia = new WebServiceMoravia_V4($this->users[0]);
			$successfull = $moravia->acceptTask($task->getIdClientetask());
		}
		return $successfull;
	}
	
	public function collectTask(){
		/**
             * Moravia API has what it is called service account.
             * Service account has access to all company tasks.
		 */
		foreach ($this->users as $user){
			
			\Functions::console("HM::".$this->source->getSourceName()." - Collecting tasks for user: ".$user->getUserName());
			$this->currentUser++;
			
			$clientId = $user->getUserName(); //DB.APIUser.userName
			$clientSecret = $user->getClientSecret(); //DB.APIUser.clientSecret
			$serviceAccount = $user->getPassWord(); //DB.APIUser.password
			
			$this->moraviaAPI = new WebServiceMoravia_V4($clientId, $clientSecret, $serviceAccount);
			//$this->moraviaAPI->setUser($user);
			$value = new DateTime($user->getTimeLife());
			$currentValue = new DateTime();
			$validation = $value < $currentValue;
			if($validation){
				$this->moraviaAPI->getAuthentication();
			}
			
			$tasks = $this->moraviaAPI->getTasks();
			$tasks = (isset($tasks["value"])?$tasks["value"]:array());
			
			//$tasks = $this->moraviaAPI->getTaskById("9544870
            //$tasks = $this->moraviaAPI->getTaskById("3518621");
			//$tasks = $this->moraviaAPI->getTaskById("9577660");
			//$tasks = $this->moraviaAPI->getTaskById("9705973");
			
			
			foreach ($tasks as $task){
				$newTask = new Task();
				$newTask->setAssignedUser($user->getIdApiUser());
				$newTask->construct(
						Functions::currentDate(),
						isset($task["Id"])? $task["Id"]:"",
						isset($task["JobName"])? $task["JobName"]:"",
						(isset($task["Description"]) && $task["Description"] != "")?$task["Description"]:"",
						Functions::createFormatDate(isset($task["StartDate"])? $task["StartDate"]:""),
						Functions::createFormatDate(isset($task["DueDate"])? $task["DueDate"]:""),
						isset($task["Project"]["Id"])?$task["Project"]["Id"]:""
						);
				
				
				
				$language = parent::identifySourceLanguage((isset($task["SourceLanguageCode"])? $task["SourceLanguageCode"]:""));
				if($language != null){
					$newTask->setSourceLanguage($language);
				}
				
				$target = parent::identifyTargetLanguage(array(isset($task["TargetLanguageCode"])? $task["TargetLanguageCode"]:""));
				$newTask->setTargetsLanguage($target);
				
				//If source attachment has Memsource TMS, here will be the TMS Autlogin Link
				$tmsAutoLoginLinks = array();
				
				$this->isAmazonTask = false;
				if(isset($task["Project"]) && isset($task["Project"]["Name"]) && $task["Project"]["Name"]!=""){
					$this->isAmazonTask = (strpos(strtoupper($task["Project"]["Name"]), "AMAZON")!==false);
				}
				
				if($this->isAmazonTask){
					$memsourceValues = explode("#", $task["JobName"]);
					if(count($memsourceValues)==2){
						$memsourceValues = $memsourceValues[1];
						$memsourceValues = explode("_",$memsourceValues, 2);
						if(count($memsourceValues)==2){
							$property = new TaskAditionalProperties();
							$property->setPropertyName("memsource_project_code");
							$property->setPropertyValue($memsourceValues[0]);
							$this->properties[] = $property;
							
							$property = new TaskAditionalProperties();
							$property->setPropertyName("memsource_project_name");
							$property->setPropertyValue($memsourceValues[1]);
							$this->properties[] = $property;
						}
						else{
							\Functions::console($this->source->getSourceName()." - Amazon task. No he podido obtener Memsource Project values. Falta _: ".$task["JobName"]);
							//Esto es un WARNING en RECOLECCIÓN. Notificar de error (No he podido obtener Memsource Project values. Falta _).
						}
					}
					else{
						\Functions::console($this->source->getSourceName()." - Amazon task. No he podido obtener Memsource Project values. Falta #: ".$task["JobName"]);
						//Esto es un WARNING en RECOLECCIÓN. Notificar de error (No he podido obtener Memsource Project values. Falta #).
					}
				}
				
				$attachments = $this->moraviaAPI->getUserattachmentsById($task["Id"]);
				if (is_array($attachments)) {
					foreach ($attachments as $attachment){
						$extension = explode(".",$attachment["Name"]);
						
						$fileExt = "";
						if(count($extension) > 1){
							$fileExt = $extension[count($extension)-1];
						}
						
						$attachmentId = $attachment["Id"];
						
						//Only if the TMS is MemSource we get the TMS access data.
						if(isset($attachment["FileType"]) && $attachment["FileType"]==="Source" && isset($attachment["TmsFiles"])){
							foreach($attachment["TmsFiles"] as $tmsFile){
								if($tmsFile["TmsName"]==="MemSource"){
									$tmsAccess = $this->moraviaAPI->getTMSAccess($attachmentId);
									if(isset($tmsAccess["AutoLoginUrl"])){
										$tmsAutoLoginLinks[] = $tmsAccess["AutoLoginUrl"];
									}
									else{
										\Functions::console($this->source->getSourceName()." - MemSource Source file does not have AutoLoginUrl. File ID: $attachmentId");
										//Esto es un WARNING en RECOLECCIÓN. Notificar de error (API devuelve un message con el motivo).
									}
								}
								else{
									\Functions::console($this->source->getSourceName()." - TMSFile but is not a memSource File. TMS : ".$tmsFile["TmsName"].", File ID: $attachmentId");
									//Esto es un INFO en RECOLECCIÓN. Notificar del nombre de TMS que se debe usar para este attachment.
								}
							}
						}
						
						//De moment només posos aquests tres. He detectat 4 tipus diferents. EL quart és Analysis, que el descarto.
						$availableFileTypes = array("Source", "Reference", "Other");
						
						//20190619 skype mhidalgo, de moment només baixar referncia
						//$availableFileTypes = array("Reference");
						
						//Si es de un tipo de archivo valido...
						if(isset($attachment["FileType"]) && in_array($attachment["FileType"], $availableFileTypes)){
							$file = new File();
							$file->construct(
									isset($attachment["Name"])?$attachment["Name"]:"file.zip",
									$fileExt,
									isset($attachment["MimeType"])?$attachment["MimeType"]:"",
									"",
									Functions::currentDate(),
									isset($attachment["DownloadUrl"])?WebServiceMoravia_V4::API."V4/".$attachment["DownloadUrl"]:"",
									isset($attachment["CheckSum"])?$attachment["CheckSum"]:"",
									Moravia_V2::HARVESTMETHOD,
									($attachment["FileType"]==="Source" ? File::SOURCE:File::REFERENCE),
									false,
									isset($attachment["Size"])?$attachment["Size"]:1
									);
							
							if($file->getFileType()==File::SOURCE){
								$newTask->addFileToTranslate($file);
							}else{
								$newTask->addFileForReference($file);
							}
						}
					}
				}
				
				
				if(count($tmsAutoLoginLinks)>0){
					$property = new TaskAditionalProperties();
					$property->setPropertyName("memsource_autologinurls");
					$property->setPropertyValue(implode("<br />", $tmsAutoLoginLinks));
					$this->properties[] = $property;
				}
				
				$wordCounts = $this->moraviaAPI->getWordCountAnalyses($task["Id"]);
				
				$analysis = $this->generateAnalsis($wordCounts);
				
				if (!empty($newTask->getTargetsLanguage())){
					$newTask->getTargetsLanguage()[0]->setAnalysis($analysis);
				}
				
				
				$dataSource = new DataSource();
				$dataSource->construct(
						$this->source->getSourceName(),
						AI::getInstance()->getSetup()->execution_type,
						Functions::currentDate(),
						Moravia_V2::HARVESTMETHOD,
						Moravia_V2::URL,
						Moravia_V2::URL.'task/'.$task["Id"].'/detail',
						json_encode($task),
						$this->source->getIdSource(),
						$user->getIdApiUser()
						);
				$newTask->setDataSource($dataSource);
				
				$aditionalProperties = parent::collectProperties($this->properties, $task);
				//Merge manual properties with collected properties.
				$aditionalProperties = array_merge($this->properties, $aditionalProperties);
				
				$newTask->setAditionalProperties($aditionalProperties);
				
				$newTask->setState(new State(State::COLLECTED));
				$newTask->generateUniqueTaskId();
				$newTask->setTimeStamp(\Functions::currentDate());
				$newTask->generateUniqueSourceId();
				
				if($this->source->getDownload()){
					//TODO:REVISAR
					foreach ($newTask->getFilesForReference() as &$file){
						$pathForDownload = parent::generatePathForDownload($task["Id"], $this->source->getSourceName(), $newTask->getUniqueTaskId());
						$file->setPath($pathForDownload.$file->getFileName());
						$successfullDownload = $this->downloadFile($file, $newTask);
						$file->setSuccessfulDownload($successfullDownload);
					}
					
					foreach ($newTask->getFilesToTranslate() as &$file){
						$pathForDownload = parent::generatePathForDownload($task["Id"], $this->source->getSourceName(), $newTask->getUniqueTaskId());
						$file->setPath($pathForDownload.$file->getFileName());
						$successfullDownload = $this->downloadFile($file, $newTask);
						$file->setSuccessfulDownload($successfullDownload);
					}
				}
				
				$this->newTasks[] = $newTask;
				
				$this->properties = array();
				
			}
		}
		parent::collectTask();
		return (count($this->newTasks) > 0);
	}

	public function getAvailableTask(){
        foreach ($this->users as $user){
            $this->currentUser++;

            $clientId = $user->getUserName(); //DB.APIUser.userName
            $clientSecret = $user->getClientSecret(); //DB.APIUser.clientSecret
            $serviceAccount = $user->getPassWord(); //DB.APIUser.password

            $this->moraviaAPI = new WebServiceMoravia_V4($clientId, $clientSecret, $serviceAccount);
            //$this->moraviaAPI->setUser($user);
            try {
                $value = new DateTime($user->getTimeLife());
            } catch (Throwable $e) {
                $value = new DateTime();
            }
            $currentValue = new DateTime();
            $validation = $value < $currentValue;
            if($validation){
                $this->moraviaAPI->getAuthentication();
            }

            $tasks = $this->moraviaAPI->getTasks();
            $tasks = (isset($tasks["value"])?$tasks["value"]:array());

            foreach ($tasks as $task){
                $newTask = new AvailableTask();

                $newTask
                    ->setIdClientTask(isset($task["Id"])? $task["Id"]:"")
                    ->setTitle(isset($task["JobName"])? $task["JobName"]:"")
                    ->setMessage((isset($task["Description"]) && $task["Description"] != "")?$task["Description"]:"")
                    ->setDueDate(Functions::createFormatDate(isset($task["DueDate"])? $task["DueDate"]:"")->format("Y-m-d H:i:s"));



                $language = parent::identifySourceLanguage((isset($task["SourceLanguageCode"])? $task["SourceLanguageCode"]:""));
                if($language != null){
                    $newTask->setSourceLanguage($language);
                }

                $target = parent::identifyTargetLanguage(array(isset($task["TargetLanguageCode"])? $task["TargetLanguageCode"]:""));
                $newTask->setTargetsLanguage($target);

                //If source attachment has Memsource TMS, here will be the TMS Autlogin Link
                $tmsAutoLoginLinks = array();

                $this->isAmazonTask = false;
                if(isset($task["Project"]) && isset($task["Project"]["Name"]) && $task["Project"]["Name"]!=""){
                    $this->isAmazonTask = (strpos(strtoupper($task["Project"]["Name"]), "AMAZON")!==false);
                }

                if($this->isAmazonTask){
                    $memsourceValues = explode("#", $task["JobName"]);
                    if(count($memsourceValues)==2){
                        $memsourceValues = $memsourceValues[1];
                        $memsourceValues = explode("_",$memsourceValues, 2);
                        if(count($memsourceValues)==2){
                            $property = new TaskAditionalProperties();
                            $property->setPropertyName("memsource_project_code");
                            $property->setPropertyValue($memsourceValues[0]);
                            $this->properties[] = $property;

                            $property = new TaskAditionalProperties();
                            $property->setPropertyName("memsource_project_name");
                            $property->setPropertyValue($memsourceValues[1]);
                            $this->properties[] = $property;
                        }
                    }
                }


                if(count($tmsAutoLoginLinks)>0){
                    $property = new TaskAditionalProperties();
                    $property->setPropertyName("memsource_autologinurls");
                    $property->setPropertyValue(implode("<br />", $tmsAutoLoginLinks));
                    $this->properties[] = $property;
                }

                $wordCounts = $this->moraviaAPI->getWordCountAnalyses($task["Id"]);

                $analysis = $this->generateAnalsis($wordCounts);

                if (!empty($newTask->getTargetsLanguage())){
                    $newTask->getTargetsLanguage()[0]->setAnalysis($analysis);
                }


                $aditionalProperties = parent::collectProperties($this->properties, $task);
                //Merge manual properties with collected properties.
                $aditionalProperties = array_merge($this->properties, $aditionalProperties);

                $newTask->setAdditionalProperties($aditionalProperties);

                $this->availableTasks[] = $newTask;

                $this->properties = array();

            }
        }
        return $this->availableTasks;
    }

    /**
	 *
	 * {@inheritDoc}
	 * @see \harvestModule\sources\StructureSource::generateAnalsis()
	 */
	protected function generateAnalsis(&$wordCounts){
		if(!isset($wordCounts[0]) || !isset($wordCounts[0]["Parser"])){
			//Esto es un WARNING en RECOLECCIÓN. Notificar que no se puede obtener contage o el parser que debe usarse/Revisar que devuelve.
			return new Analysis();
		}
		
		$parser = strtoupper($wordCounts[0]["Parser"]);
		if($this->isAmazonTask && $parser==="MEMSOURCE"){
			$parser = "AMAZON-MEMSOURCE";
		}
		
		switch($parser){
			case "XTM":
				return self::getXTMWordCount($wordCounts[0]["WordCounts"]);
			case "MEMOQ":
			case "MEMSOURCE":
				return self::getStandardWordCount($wordCounts[0]["WordCounts"]);
				break;
			case "AMAZON-MEMSOURCE":
				return self::getFORAMAZONMemSourceWordCount($wordCounts[0]["WordCounts"]);
			default:
				//return self::getUnknownParserWordCount($wordCounts[0]["WordCounts"]);
				//Esto es un WARNING en RECOLECCIÓN. Notificar que no se conoce el parser (notificar el nombre del parser).
				return new Analysis();
		}
		
		return new Analysis();
	}
	
	public function downloadFile(File &$file, Task &$task = null){
		$result = true;
		if(!file_exists($file->getPath())){
			set_time_limit(0);
			$file->setPathForDownload($file->getSourcePath());
			$pathForDownload = parent::generatePathForDownload($task->getIdClientetask(), $this->source->getSourceName(), $task->getUniqueTaskId());
			$result = $this->moraviaAPI->downloadAttachment($file->getSourcePath(),$file->getFileName(),$pathForDownload);
		}
		
		$file->setPathForDownload($file->getSourcePath());
		return $result;
	}
	
	/**
	 * El parser que hi havia genèric per a Moravia, el deixo aqui perque pudé en un futur correspón amb algun parser en concret
	 *
	 * @param array $wc
	 * @return \model\Analysis
	 */
	public static function getUnknownParserWordCount($wc){
		$analysis = new Analysis();
		
		foreach ($wc as $value){
			switch ($value["Name"]){
				case "autotrans 100% (Words)":
					$analysis->setPercentage_100($analysis->getPercentage_100() + $value["Value"]);
					break;
				case "autotrans 95% - 99% (Words)":
					$analysis->setPercentage_95($analysis->getPercentage_95() + $value["Value"]);
					break;
				case "autotrans 85% - 94% (Words)":
					$analysis->setPercentage_85($analysis->getPercentage_85() + $value["Value"]);
					break;
				case "autotrans 75% - 84% (Words)":
					$analysis->setPercentage_75($analysis->getPercentage_75() + $value["Value"]);
					break;
				case "autotrans 30% - 74% (Words)":
					$analysis->setPercentage_50($analysis->getPercentage_50() + $value["Value"]);
					break;
				case "forreview (Words)":
					$analysis->setPercentage_100($analysis->getPercentage_100() + $value["Value"]);
					break;
				case "validated (Words)":
					$analysis->setNotMatch($analysis->getNotMatch() + $value["Value"]);
					break;
				case "untranslated repeated (Words)":
					$analysis->setRepetition($analysis->getRepetition() + $value["Value"]);
					break;
				case "untranslated (Words)":
					$analysis->setNotMatch($analysis->getNotMatch() + $value["Value"]);
					break;
				case "total (Words)":
					$analysis->setTotalWords($value["Value"]);
					break;
				default:
					break;
			}
		}
		
		//$analysis->calculateTotalWords();
		$analysis->calculateWeightedWords();
		
		return $analysis;
	}
	
	/**
	 * XTM parser
	 *
	 * @param array $wc
	 * @return \model\Analysis
	 */
	public static function getXTMWordCount($wc){
		/*
		 * 	Total = Total Words
			To be corrected (Words) = Ignorar
			To be done (Words) = Ignorar
			Done (Words) = Ignorar
			Repeat (Words) = Repetitions
			No matching (Words) = New Words
			Machine translation (Words) = MT
			75-84 Fuzzy match (Words) + 75-84 Fuzzy repeat (Words) = 75-84
			85-94 Fuzzy match (Words) + 85-94 Fuzzy repeat (Words) = 85-94
			95-99 Fuzzy match (Words) + 95-99 Fuzzy repeat (Words)  = 95-99
			Leveraged match (Words)  = 100%
			ICE match (Words) = Ignorar (serien ICE)
			Non-translatable (Words) New Words
		*/
		
		/*
		 * @var \model\Analysis $analysis
		 */
		$analysis = new Analysis();
		
		foreach($wc as $value){
			switch ($value["Name"]){
				case "ICE match (Words)":
					$analysis->setPercentage_101($analysis->getPercentage_101() + $value["Value"]);
					break;
				case "Repeat (Words)":
					$analysis->setRepetition($analysis->getRepetition() + $value["Value"]);
					break;
				case "Leveraged match (Words)":
					$analysis->setPercentage_100($analysis->getPercentage_100() + $value["Value"]);
					break;
				case "95-99 Fuzzy match (Words)":
				case "95-99 Fuzzy repeat (Words)":
					$analysis->setPercentage_95($analysis->getPercentage_95() + $value["Value"]);
					break;
				case "85-94 Fuzzy match (Words)":
				case "85-94 Fuzzy repeat (Words)":
					$analysis->setPercentage_85($analysis->getPercentage_85() + $value["Value"]);
					break;
				case "75-84 Fuzzy match (Words)":
				case "75-84 Fuzzy repeat (Words)":
					$analysis->setPercentage_75($analysis->getPercentage_75() + $value["Value"]);
					break;
				case "50% - 74% (Words)":
					$analysis->setPercentage_50($analysis->getPercentage_50() + $value["Value"]);
					break;
				case "No matching (Words)":
				case "Non-translatable (Words)":
					$analysis->setNotMatch($analysis->getNotMatch() + $value["Value"]);
					break;
				case "Machine translation (Words)":
					$analysis->setMachineTanslation($analysis->getMachineTanslation() + $value["Value"]);
					break;
			}
		}
		
		$analysis->calculateTotalWords();
		$analysis->calculateWeightedWords();
		
		return $analysis;
	}
	
	/**
	 * Standard parser
	 *
	 * @param array $wc
	 * @return \model\Analysis
	 */
	public static function getStandardWordCount($wc){
		$analysis = new Analysis();
		
		foreach($wc as $value){
			switch ($value["Name"]){
				case "X-translated (Words)":
				case "101% (Words)":
				case "Context TM (Words)":
					$analysis->setPercentage_101($analysis->getPercentage_101() + $value["Value"]);
					break;
				case "Repetitions (Words)":
					$analysis->setRepetition($analysis->getRepetition() + $value["Value"]);
					break;
				case "100% (Words)":
					$analysis->setPercentage_100($analysis->getPercentage_100() + $value["Value"]);
					break;
				case "95% - 99% (Words)":
					$analysis->setPercentage_95($analysis->getPercentage_95() + $value["Value"]);
					break;
				case "85% - 94% (Words)":
					$analysis->setPercentage_85($analysis->getPercentage_85() + $value["Value"]);
					break;
				case "75% - 84% (Words)":
					$analysis->setPercentage_75($analysis->getPercentage_75() + $value["Value"]);
					break;
				case "50% - 74% (Words)":
					$analysis->setPercentage_50($analysis->getPercentage_50() + $value["Value"]);
					break;
				case "No Match (Words)":
				case "Fragments (Words)":
				case "No match (Words)":
					$analysis->setNotMatch($analysis->getNotMatch() + $value["Value"]);
					break;
			}
		}
		
		$analysis->calculateTotalWords();
		$analysis->calculateWeightedWords();
		
		return $analysis;
	}
	
	/**
	 * MemSource parser SPECIFIC AMAZON.
	 *
	 * @param array $wc
	 * @return \model\Analysis
	 */
	public static function getFORAMAZONMemSourceWordCount($wc){
		$analysis = new Analysis();
		
	
		
		foreach($wc as $value){
			switch ($value["Name"]){
				case "Context TM (Words)":
					$analysis->setPercentage_101(0);
					break;
				case "Repetitions (Words)":
					$analysis->setRepetition($analysis->getRepetition() + $value["Value"]);
					break;
				case "100% (Words)":
				case "95% - 99% (Words)":
					$analysis->setPercentage_95($analysis->getPercentage_95() + $value["Value"]);
					break;
				case "85% - 94% (Words)":
					$analysis->setPercentage_85($analysis->getPercentage_85() + $value["Value"]);
					break;
				case "75% - 84% (Words)":
				case "50% - 74% (Words)":
				case "No Match (Words)":
					$analysis->setPercentage_75($analysis->getPercentage_75() + $value["Value"]);
					break;
			}
		}
		
		$analysis->calculateTotalWords();
		$analysis->calculateWeightedWords();
		
		return $analysis;
	}

    public function collectTaskByIdClientTask($idTask)
    {
        $task = null;
        $taskFound = false;
        foreach ($this->users as $user) {

            \Functions::console("HM::" . $this->source->getSourceName() . " - Collecting tasks for user: " . $user->getUserName());
            $this->currentUser++;

            $clientId = $user->getUserName(); //DB.APIUser.userName
            $clientSecret = $user->getClientSecret(); //DB.APIUser.clientSecret
            $serviceAccount = $user->getPassWord(); //DB.APIUser.password

            $this->moraviaAPI = new WebServiceMoravia_V4($clientId, $clientSecret, $serviceAccount);
            //$this->moraviaAPI->setUser($user);
            $value = new DateTime($user->getTimeLife());
            $currentValue = new DateTime();
            $validation = $value < $currentValue;
            if ($validation) {
                $this->moraviaAPI->getAuthentication();
            }

            $tasks = $this->moraviaAPI->getTasks();
            $tasks = (isset($tasks["value"]) ? $tasks["value"] : array());

            foreach ($tasks as $taskData){
                if(isset($taskData["Id"]) && $taskData["Id"] == $idTask){
                    $task = $taskData;
                    $taskFound = true;
                    break;
                }
            }

            if($taskFound){
                break;
            }
        }
        if($taskFound){
            $newTask = new Task();
            $newTask->setAssignedUser($user->getIdApiUser());
            $newTask->construct(
                Functions::currentDate(),
                isset($task["Id"])? $task["Id"]:"",
                isset($task["JobName"])? $task["JobName"]:"",
                (isset($task["Description"]) && $task["Description"] != "")?$task["Description"]:"",
                Functions::createFormatDate(isset($task["StartDate"])? $task["StartDate"]:""),
                Functions::createFormatDate(isset($task["DueDate"])? $task["DueDate"]:""),
                isset($task["Project"]["Id"])?$task["Project"]["Id"]:""
            );



            $language = parent::identifySourceLanguage((isset($task["SourceLanguageCode"])? $task["SourceLanguageCode"]:""));
            if($language != null){
                $newTask->setSourceLanguage($language);
            }

            $target = parent::identifyTargetLanguage(array(isset($task["TargetLanguageCode"])? $task["TargetLanguageCode"]:""));
            $newTask->setTargetsLanguage($target);

            //If source attachment has Memsource TMS, here will be the TMS Autlogin Link
            $tmsAutoLoginLinks = array();

            $this->isAmazonTask = false;
            if(isset($task["Project"]) && isset($task["Project"]["Name"]) && $task["Project"]["Name"]!=""){
                $this->isAmazonTask = (strpos(strtoupper($task["Project"]["Name"]), "AMAZON")!==false);
            }

            if($this->isAmazonTask){
                $memsourceValues = explode("#", $task["JobName"]);
                if(count($memsourceValues)==2){
                    $memsourceValues = $memsourceValues[1];
                    $memsourceValues = explode("_",$memsourceValues, 2);
                    if(count($memsourceValues)==2){
                        $property = new TaskAditionalProperties();
                        $property->setPropertyName("memsource_project_code");
                        $property->setPropertyValue($memsourceValues[0]);
                        $this->properties[] = $property;

                        $property = new TaskAditionalProperties();
                        $property->setPropertyName("memsource_project_name");
                        $property->setPropertyValue($memsourceValues[1]);
                        $this->properties[] = $property;
                    }
                    else{
                        \Functions::console($this->source->getSourceName()." - Amazon task. No he podido obtener Memsource Project values. Falta _: ".$task["JobName"]);
                        //Esto es un WARNING en RECOLECCIÓN. Notificar de error (No he podido obtener Memsource Project values. Falta _).
                    }
                }
                else{
                    \Functions::console($this->source->getSourceName()." - Amazon task. No he podido obtener Memsource Project values. Falta #: ".$task["JobName"]);
                    //Esto es un WARNING en RECOLECCIÓN. Notificar de error (No he podido obtener Memsource Project values. Falta #).
                }
            }

            $attachments = $this->moraviaAPI->getUserattachmentsById($task["Id"]);
            if (is_array($attachments)) {
                foreach ($attachments as $attachment){
                    $extension = explode(".",$attachment["Name"]);

                    $fileExt = "";
                    if(count($extension) > 1){
                        $fileExt = $extension[count($extension)-1];
                    }

                    $attachmentId = $attachment["Id"];

                    //Only if the TMS is MemSource we get the TMS access data.
                    if(isset($attachment["FileType"]) && $attachment["FileType"]==="Source" && isset($attachment["TmsFiles"])){
                        foreach($attachment["TmsFiles"] as $tmsFile){
                            if($tmsFile["TmsName"]==="MemSource"){
                                $tmsAccess = $this->moraviaAPI->getTMSAccess($attachmentId);
                                if(isset($tmsAccess["AutoLoginUrl"])){
                                    $tmsAutoLoginLinks[] = $tmsAccess["AutoLoginUrl"];
                                }
                                else{
                                    \Functions::console($this->source->getSourceName()." - MemSource Source file does not have AutoLoginUrl. File ID: $attachmentId");
                                    //Esto es un WARNING en RECOLECCIÓN. Notificar de error (API devuelve un message con el motivo).
                                }
                            }
                            else{
                                \Functions::console($this->source->getSourceName()." - TMSFile but is not a memSource File. TMS : ".$tmsFile["TmsName"].", File ID: $attachmentId");
                                //Esto es un INFO en RECOLECCIÓN. Notificar del nombre de TMS que se debe usar para este attachment.
                            }
                        }
                    }

                    //De moment només posos aquests tres. He detectat 4 tipus diferents. EL quart és Analysis, que el descarto.
                    //$availableFileTypes = array("Source", "Reference", "Other", "Analysis");

                    //20190619 skype mhidalgo, de moment només baixar referncia
                    $availableFileTypes = array("Reference");

                    //Si es de un tipo de archivo valido...
                    if(isset($attachment["FileType"]) && in_array($attachment["FileType"], $availableFileTypes)){
                        $file = new File();
                        $file->construct(
                            isset($attachment["Name"])?$attachment["Name"]:"file.zip",
                            $fileExt,
                            isset($attachment["MimeType"])?$attachment["MimeType"]:"",
                            "",
                            Functions::currentDate(),
                            isset($attachment["DownloadUrl"])?WebServiceMoravia_V4::API."V4/".$attachment["DownloadUrl"]:"",
                            isset($attachment["CheckSum"])?$attachment["CheckSum"]:"",
                            Moravia_V2::HARVESTMETHOD,
                            ($attachment["FileType"]==="Source" ? File::SOURCE:File::REFERENCE),
                            false,
                            isset($attachment["Size"])?$attachment["Size"]:1
                        );

                        if($file->getFileType()==File::SOURCE){
                            $newTask->addFileToTranslate($file);
                        }else{
                            $newTask->addFileForReference($file);
                        }
                    }
                }
            }


            if(count($tmsAutoLoginLinks)>0){
                $property = new TaskAditionalProperties();
                $property->setPropertyName("memsource_autologinurls");
                $property->setPropertyValue(implode("<br />", $tmsAutoLoginLinks));
                $this->properties[] = $property;
            }

            $wordCounts = $this->moraviaAPI->getWordCountAnalyses($task["Id"]);

            $analysis = $this->generateAnalsis($wordCounts);

            if (!empty($newTask->getTargetsLanguage())){
                $newTask->getTargetsLanguage()[0]->setAnalysis($analysis);
            }


            $dataSource = new DataSource();
            $dataSource->construct(
                $this->source->getSourceName(),
                "IN",
                Functions::currentDate(),
                Moravia_V2::HARVESTMETHOD,
                Moravia_V2::URL,
                Moravia_V2::URL.'task/'.$task["Id"].'/detail',
                json_encode($task),
                $this->source->getIdSource(),
                $user->getIdApiUser()
            );
            $newTask->setDataSource($dataSource);

            $aditionalProperties = parent::collectProperties($this->properties, $task);
            //Merge manual properties with collected properties.
            $aditionalProperties = array_merge($this->properties, $aditionalProperties);

            $newTask->setAditionalProperties($aditionalProperties);

            $newTask->setState(new State(State::COLLECTED));
            $newTask->generateUniqueTaskId();
            $newTask->setTimeStamp(\Functions::currentDate());
            $newTask->generateUniqueSourceId();

            if($this->source->getDownload()){
                foreach ($newTask->getFilesForReference() as &$file){
                    $pathForDownload = parent::generatePathForDownload($task["Id"], $this->source->getSourceName(), $newTask->getUniqueTaskId());
                    $file->setPath($pathForDownload.$file->getFileName());
                    $successfullDownload = $this->downloadFile($file, $newTask);
                    $file->setSuccessfulDownload($successfullDownload);
                }

                foreach ($newTask->getFilesToTranslate() as &$file){
                    $pathForDownload = parent::generatePathForDownload($task["Id"], $this->source->getSourceName(), $newTask->getUniqueTaskId());
                    $file->setPath($pathForDownload.$file->getFileName());
                    $successfullDownload = $this->downloadFile($file, $newTask);
                    $file->setSuccessfulDownload($successfullDownload);
                }
            }

            $this->newTasks[] = $newTask;
        }
        parent::collectTask();
        return (count($this->newTasks) > 0);
    }
}
?>