<?php

/**
 * @author Ljimenez
 * @version 1.0
 * created on 10 jul. 2018
 */
namespace harvestModule\sources;

use Functions;
use model\AvailableTask;
use model\Source;
use harvestModule\sources\connectService\WelocalizeConnect;
use model\Task;
use core\AI;
use model\Analysis;
use model\DataSource;
use model\State;

include_once BaseDir. '/harvestModule/sources/connectService/WelocalizeConnect.php';
include_once BaseDir. '/harvestModule/sources/StructureSource.php';
include_once BaseDir. '/model/Analysis.php';
include_once BaseDir. '/model/Task.php';
include_once BaseDir. '/model/DataSource.php';
include_once BaseDir. '/model/Language.php';
include_once BaseDir. '/dataAccess/dao/LanguageDAO.php';
include_once BaseDir. '/model/State.php';
include_once BaseDir. '/Functions.php';
include_once BaseDir. '/dataAccess/dao/TaskDAO.php';

class Welocalize extends StructureSource{

    public const STR_SERVER = 'https://falcon.welocalize.com:7017';
    public const HARVESTMETHOD = 'API';
    private const URL_TASKS = 'https://is.welocalize.com/group/vendorportal';
    public const GOOGLE_WELOC_USER = "POC for Google Google projects";

    /**
     *
     * @var string[]
     */
    private $properties;
    public  function __construct(array $users, Source $source){
        parent::__construct($users,$source);
        $this->properties = array(
            "platform",
            "clientId"
        );
    }


    /**
     * Acepta una tarea de Welocalize
     * Devuelve true si éxito, false falla.
     *
     * @return boolean
     * {@inheritDoc}
     * @see \harvestModule\sources\StructureSource::acceptTask()
     */
    public function acceptTask(Task $task){
        //Inicializamos el estado de la aceptación a false.
        $validation = false;


        //Obtenemos el usuario que ha recolectado la tarea.
        $APITaskCollectorId = $task->getDataSource()->getTmsUserCollector();

        //Declaramos las variables $APIUname y $APIUpasswd y las inicializamos con las credenciales del 1er usuario del array de usuarios para este source.
        $APIUname = $this->users[0]->getUserName();
        $APIUpasswd = $this->users[0]->getPassWord();

        //Del array de usuarios, buscamos el objeto del usuario que ha recoelctado esta tarea.
        foreach($this->users as $apiUser){
            if($apiUser->getIdApiUser()==$APITaskCollectorId){
                $APIUname = $apiUser->getUserName();
                $APIUpasswd = $apiUser->getPassWord();
                break;
            }
        }

        //Obtenemos el token de autenticación a la API con el usuario que recolectó (y por tanto encontró) la tarea
        $welocalize = new WelocalizeConnect($APIUname, $APIUpasswd);
        $ts = $welocalize->getTask($task->getIdClientetask());
        if($ts === false){
            return false;
        }

        //Obtenemos el id de usuario que tiene la tarea asignada.
        $idUser = $task->getAssignedUser();

        if($idUser != null){

            //Generamos el array post con los datos que enviaremos a la API del cliente.
            $post = array('taskIds' => $task->getIdClientetask(), 'timestamps' => $ts,'vendorProfileUserId' => $idUser);

            $url = Welocalize::STR_SERVER."/Vendor/VendorTaskStatusTracking/AcceptTasks.".WelocalizeConnect::RESPONSE_FORMAT;

            $validation = $welocalize->request($url,'POST',$post);

            //Resultado.
            $validation = ($validation!==false && isset($validation["estat"]) && $validation["estat"] == "OK");
        }
        return $validation;
    }

    public function collectTask(){
        foreach ($this->users as $user){
            $connect = new WelocalizeConnect($user->getUserName(),$user->getPassWord());
            $response = $connect->request();
            $tasks = isset($response["json"]["FalconTasks"])?$response["json"]["FalconTasks"]:array();
// 			$string = '';
// 			$tasks = array();
// 			$t = json_decode($string,true);
// 			$tasks[] = $t;
            if(is_array($tasks)){
                foreach ($tasks as $task){
                    $newTask = new Task();
                    $title = "";
                    if(isset($task["requestName"]) && isset($task["clientName"])){
                        $title = $task["requestName"].' - '.$task["clientName"];
                    }else if (isset($task["clientName"])){
                        $title = $task["clientName"];
                    }
                    $newTask->setAssignedUser(isset($task["assigneeId"])?$task["assigneeId"]:100);
                    $newTask->construct(
                        Functions::currentDate(),
                        isset($task["taskId"])?$task["taskId"]:"",
                        $title,
                        isset($task["message"]) && $task["message"] != null?$task["message"]:"",
                        Functions::createFormatDate(isset($task["startDate"])?$task["startDate"]:""),
                        Functions::createFormatDate(isset($task["dueDate"])?$task["dueDate"]:""),
                        isset($task["projectNumber"])?$task["projectNumber"]:""
                    );

                    $sourceLanguage = parent::identifySourceLanguage(isset($task["sourceLocale"])?$task["sourceLocale"]:'');
                    if($sourceLanguage != null){
                        $newTask->setSourceLanguage($sourceLanguage);
                    }

                    $targetLanguage = parent::identifyTargetLanguage(array(isset($task["targetLocale"])?$task["targetLocale"]:''));
                    $newTask->setTargetsLanguage($targetLanguage);

                    $analysis = $this->generateAnalsis($task);
                    if(!empty($newTask->getTargetsLanguage())){
                        $newTask->getTargetsLanguage()[0]->setAnalysis($analysis);
                    }

                    $dataSource = new DataSource();
                    $dataSource->construct(
                        $this->source->getSourceName(),
                        AI::getInstance()->getSetup()->execution_type,
                        Functions::currentDate(),
                        Welocalize::HARVESTMETHOD,
                        Welocalize::STR_SERVER,
                        Welocalize::URL_TASKS,
                        json_encode($task),
                        $this->source->getIdSource(),
                        $user->getIdApiUser()
                    );

                    $newTask->setDataSource($dataSource);
                    $properties = parent::collectProperties($this->properties, $task);
                    $newTask->setAditionalProperties($properties);

                    $newTask->generateUniqueTaskId();
                    $newTask->setTimeStamp(Functions::currentDate());
                    $newTask->generateUniqueSourceId();
                    $newTask->setState(new State(State::COLLECTED));
                    if($newTask->getTitle() === ""){
                        $newTask->setTitle($newTask->getUniqueSourceId());
                    }
                    $this->newTasks[] = $newTask;
                }
            }
        }

        parent::collectTask();
        return (count($this->newTasks) > 0);
    }

    public function collectTaskByIdClientTask($idTask)
    {
        $taskData = null;
        $taskExist = false;
        $currentUser = null;
        $newTask = null;
        foreach ($this->users as $user) {
            $connect = new WelocalizeConnect($user->getUserName(), $user->getPassWord());
            $response = $connect->request();
            $tasks = isset($response["json"]["FalconTasks"]) ? $response["json"]["FalconTasks"] : array();
            if (is_array($tasks)) {
                foreach ($tasks as $task) {
                    if($task["taskId"] == $idTask){
                        $taskData = $task;
                        $currentUser = $user;
                        $taskExist = true;
                        break;
                    }
                }
            }
            if($taskExist){
                break;
            }
        }

        if($taskExist){
            $newTask = $this->createTask($taskData,null,$currentUser);
            $newTask->generateUniqueTaskId();
            $newTask->setTimeStamp(\Functions::currentDate());
            $newTask->generateUniqueSourceId();
            $newTask->setState(new State(State::COLLECTED));

            $this->newTasks[] = $newTask;
        }else{
            throw new \Exception("Any task was found with this idClientTask = $idTask");
        }
        parent::collectTask();
        return (count($this->newTasks) > 0);
    }

    public function getAvailableTask()
    {
        foreach ($this->users as $user) {
            $connect = new WelocalizeConnect($user->getUserName(), $user->getPassWord());
            $response = $connect->request();
            $tasks = isset($response["json"]["FalconTasks"]) ? $response["json"]["FalconTasks"] : array();
            if (is_array($tasks)) {
                foreach ($tasks as $task) {
                    $availableTask = new AvailableTask();
                    $title = "";
                    if (isset($task["requestName"]) && isset($task["clientName"])) {
                        $title = $task["requestName"] . ' - ' . $task["clientName"];
                    } else if (isset($task["clientName"])) {
                        $title = $task["clientName"];
                    }

                    $availableTask
                        ->setIdClientTask(isset($task["taskId"]) ? $task["taskId"] : "")
                        ->setTitle($title)
                        ->setMessage(isset($task["message"]) && $task["message"] != null ? $task["message"] : "")
                        ->setDueDate(Functions::createFormatDate(isset($task["dueDate"]) ? $task["dueDate"] : "")->format("Y-m-d H:i:s"));

                    $sourceLanguage = parent::identifySourceLanguage(isset($task["sourceLocale"]) ? $task["sourceLocale"] : '');
                    if ($sourceLanguage != null) {
                        $availableTask->setSourceLanguage($sourceLanguage);
                    }

                    $targetLanguage = parent::identifyTargetLanguage(array(isset($task["targetLocale"]) ? $task["targetLocale"] : ''));
                    $availableTask->setTargetsLanguage($targetLanguage);

                    $analysis = $this->generateAnalsis($task);
                    if (!empty($availableTask->getTargetsLanguage())) {
                        $availableTask->getTargetsLanguage()[0]->setAnalysis($analysis);
                    }


                    $properties = parent::collectProperties($this->properties, $task);
                    $availableTask->setAdditionalProperties($properties);

                    if ($availableTask->getTitle() === "") {
                        $availableTask->setTitle("Title not found. In harvesting the USID will be used.");
                    }
                    $this->availableTasks[] = $availableTask;
                }
            }
        }
        return $this->availableTasks;
    }


    public function createTask($taskData, $tms = null,$user = null)
    {
        $newTask = new Task();
        $title = "";
        if (isset($taskData["requestName"]) && isset($taskData["clientName"])) {
            $title = $taskData["requestName"] . ' - ' . $taskData["clientName"];
        } else if (isset($taskData["clientName"])) {
            $title = $taskData["clientName"];
        }
        $newTask->setAssignedUser(isset($taskData["assigneeId"]) ? $taskData["assigneeId"] : 100);
        $newTask->construct(
            \Functions::currentDate(),
            isset($taskData["taskId"]) ? $taskData["taskId"] : "",
            $title,
            isset($taskData["message"]) && $taskData["message"] != null ? $taskData["message"] : "",
            \Functions::createFormatDate(isset($taskData["startDate"]) ? $taskData["startDate"] : ""),
            \Functions::createFormatDate(isset($taskData["dueDate"]) ? $taskData["dueDate"] : ""),
            isset($taskData["projectNumber"]) ? $taskData["projectNumber"] : ""
        );

        $sourceLanguage = parent::identifySourceLanguage(isset($taskData["sourceLocale"]) ? $taskData["sourceLocale"] : '');
        if ($sourceLanguage != null) {
            $newTask->setSourceLanguage($sourceLanguage);
        }

        $targetLanguage = parent::identifyTargetLanguage(array(isset($taskData["targetLocale"]) ? $taskData["targetLocale"] : ''));
        $newTask->setTargetsLanguage($targetLanguage);

        $analysis = $this->generateAnalsis($taskData);
        if (!empty($newTask->getTargetsLanguage())) {
            $newTask->getTargetsLanguage()[0]->setAnalysis($analysis);
        }

        $dataSource = new DataSource();
        $dataSource->construct(
            $this->source->getSourceName(),
            "IN",
            \Functions::currentDate(),
            Welocalize::HARVESTMETHOD,
            Welocalize::STR_SERVER,
            Welocalize::URL_TASKS,
            json_encode($taskData),
            $this->source->getIdSource(),
            $user->getIdApiUser()
        );

        $newTask->setDataSource($dataSource);
        $properties = parent::collectProperties($this->properties, $taskData);
        $newTask->setAditionalProperties($properties);
        if ($newTask->getTitle() === "") {
            $newTask->setTitle($newTask->getUniqueSourceId());
        }
        return $newTask;
    }



    protected function generateAnalsis(&$wordCount){
        $analysis = new Analysis();
        if(isset($wordCount["unitItems"])){
            foreach($wordCount["unitItems"] as $data){
                switch ($data["key"]){
                    case "New":
                        $analysis->setNotMatch($data["value"]);
                        break;
                    case "95%":
                        $analysis->setPercentage_95($data["value"]);
                        break;
                    case "85%":
                        $analysis->setPercentage_85($data["value"]);
                        break;
                    case "75%":
                        $analysis->setPercentage_75($data["value"]);
                        break;
                    case "Rep":
                        $analysis->setRepetition($data["value"]);
                        break;
                    case "100%":
                        $analysis->setPercentage_100($data["value"]);
                        break;
                    case "ICE":
                        $analysis->setPercentage_101($data["value"]);
                        break;
                    case "MT Full":
                        $analysis->setMachineTanslation($data["value"]);
                        break;
                    case "Review":
                        break;
                    default:
                        break;
                }
            }
            if(isset($wordCount["unitOfMeasureDesc"]) && trim($wordCount["unitOfMeasureDesc"]) == "Words") {
                $analysis->setWeightedWord(isset($wordCount["weightedUnits"]) ? $wordCount["weightedUnits"] : 0.0);
            }else{
                $analysis->setMinute(isset($wordCount["weightedUnits"]) ? $wordCount["weightedUnits"] * 60 : 0);
            }
            if(empty($analysis->getWeightedWord())){
                $analysis->calculateWeightedWords();
            }
            $analysis->calculateTotalWords();
        }
        return $analysis;
    }

}
?>