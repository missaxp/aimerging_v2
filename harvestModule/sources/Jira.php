<?php

/**
 * @author Ljimenez
 * @version 1.0
 * created on 5 jul. 2018
 */
namespace harvestModule\sources;

use DateTime;
use Functions;
use harvestModule\sources\connectService\JiraMail;
use model\Analysis;
use model\AvailableTask;
use model\Source;
use model\State;
use core\AI;
use model\Task;
use model\DataSource;
use harvestModule\sources\connectService\JiraConnect;
use Throwable;

include_once BaseDir. '/dataAccess/dao/LanguageDAO.php';
include_once BaseDir. '/harvestModule/sources/StructureSource.php';
include_once BaseDir. '/harvestModule/sources/connectService/JiraMail.php';
include_once BaseDir. '/harvestModule/sources/connectService/JiraConnect.php';
include_once BaseDir. '/model/Task.php';
include_once BaseDir. '/Functions.php';


class Jira extends StructureSource{
    public static $jira_aps_mail_path;
    private const HARVESMETHOD = 'Email';
    public const APS_URL = 'https://aps.lionbridge.com';
    private $properties;

    public function __construct(array $users,Source $source){
        parent::__construct($users,$source);
        $this->properties = array(

        );

        if($this->development){
            Jira::$jira_aps_mail_path = '/webs/ai/testTMSData/';
        }else{
            Jira::$jira_aps_mail_path = '/ai/sources/jira/';
        }
    }

    public function collectTask(){
        $messages = glob(Jira::$jira_aps_mail_path."*.msg");
        $jiraConnect = null;
        if(!empty($messages)){
            $jiraConnect = new JiraConnect($this->users[0]);
        }
        foreach ($messages as $message){
            $messageName = str_replace(Jira::$jira_aps_mail_path,"",$message);

            $mail = new JiraMail($messageName);
            $isSuccessfull = $mail->readMail();

            if($isSuccessfull){
                $newTask = new Task();
                $instructions = "<b>TP + TRANSLATION COMPLETE + <br />Completar la tarea en JIRA</b><br />Link de JIRA:<br />".Jira::APS_URL."/".$mail->getDestination()."<br />
							<b>Comprueba que el número de links en JIRA y en GTT coincida. Si no es así, avisa al PM. ¡Gracias! </b>";
                try{
                    $dueDate = new \DateTime(str_replace("/","-", $mail->getDueDate())." Europe/Madrid");
                    $dueDate->setTimezone(new \DateTimeZone("UTC"));
                    $dueDate = $dueDate;//->format("Y-m-d H:i:s");
                }catch (\Exception $e){
                    $dueDate = Functions::currentDateAsObject();
                }

                $newTask->construct(
                    \Functions::currentDate(),
                    $mail->getDestinationId(),
                    $mail->getTitleTask(),
                    $instructions,
                    \Functions::currentDateAsObject(), //TODO: Los correos de jira no traen la fecha de inicio asi que se coloco una fecha cualquiera para que no altere al momento de crear el uniqueTaskId
                    $dueDate,
                    ""
                );

                $sourceLanguage = parent::identifySourceLanguage($mail->getSourceLanguage());
                if($sourceLanguage != null){
                    $newTask->setSourceLanguage($sourceLanguage);
                }

                $targets = parent::identifyTargetLanguage(array($mail->getTargetLanguage()));
                $newTask->setTargetsLanguage($targets);

                $dataSource = new DataSource();
                $dataSource->construct(
                    $this->source->getSourceName(),
                    AI::getInstance()->getSetup()->execution_type,
                    \Functions::currentDate(),
                    Jira::HARVESMETHOD,
                    Jira::APS_URL,
                    Jira::APS_URL.$mail->getDestination(),
                    /*$mail->getMailContent()*/"",
                    $this->source->getIdSource(),
                    $this->users[0]->getIdApiUser()
                );
                $newTask->setDataSource($dataSource);

                $taskData = $jiraConnect->getTaskData($dataSource->getTmsTaskUrl());
                $analysis = $this->generateAnalsis($taskData["analysis"]);
                $taskData["contentType"] = $mail->getContentType();
                if(!empty($newTask->getTargetsLanguage())){
                    $newTask->getTargetsLanguage()[0]->setAnalysis($analysis);
                }

                $isSuccessfull = $mail->analyzeDataEmail();
                if($isSuccessfull)
                    $newTask->setState(new State(State::COLLECTED));
                else{
                    continue;
                }


                $properties = parent::collectProperties($this->properties, $taskData);
                $newTask->setAditionalProperties($properties);

                $newTask->generateUniqueTaskId();
                $newTask->setTimeStamp(\Functions::currentDate());
                $newTask->setStartDate(\Functions::currentDateAsObject());
                $newTask->generateUniqueSourceId();
                $this->newTasks[] = $newTask;
            }else{
                $mail->moveEmailFile();
            }
        }
        parent::collectTask();
        return (count($this->newTasks) > 0);
    }

    public function getAvailableTask(){
        $messages = glob(Jira::$jira_aps_mail_path."*.msg");
        $jiraConnect = null;
        if(!empty($messages)){
            $jiraConnect = new JiraConnect($this->users[0]);
        }
        foreach ($messages as $message){
            $messageName = str_replace(Jira::$jira_aps_mail_path,"",$message);

            $mail = new JiraMail($messageName);
            $isSuccessfull = $mail->readMail(false);

            if(!$isSuccessfull) continue;
            $instructions = "<b>TP + TRANSLATION COMPLETE + <br />Completar la tarea en JIRA</b><br />Link de JIRA:<br />".Jira::APS_URL."/".$mail->getDestination()."<br />
                        <b>Comprueba que el número de links en JIRA y en GTT coincida. Si no es así, avisa al PM. ¡Gracias! </b>";
            try{
                $dueDate = new DateTime(str_replace("/","-", $mail->getDueDate())." Europe/Madrid");
                $dueDate->setTimezone(new \DateTimeZone("UTC"));
                $dueDate = $dueDate->format("Y-m-d H:i:s");
            }catch (Throwable $e){
                $dueDate = str_replace("/","-", \Functions::currentDate());
            }

            $newTask = new AvailableTask();
            $newTask->setDueDate($dueDate)
                ->setMessage($instructions)
                ->setIdClientTask($mail->getDestinationId())
                ->setTitle($mail->getTitleTask());

            $sourceLanguage = parent::identifySourceLanguage($mail->getSourceLanguage());
            if($sourceLanguage != null){
                $newTask->setSourceLanguage($sourceLanguage);
            }

            $targets = parent::identifyTargetLanguage(array($mail->getTargetLanguage()));
            $newTask->setTargetsLanguage($targets);

            $taskData = $jiraConnect->getTaskData(Jira::APS_URL.$mail->getDestination());
            $analysis = $this->generateAnalsis($taskData["analysis"]);
            $taskData["contentType"] = $mail->getContentType();
            if(!empty($newTask->getTargetsLanguage())){
                $newTask->getTargetsLanguage()[0]->setAnalysis($analysis);
            }


            $properties = parent::collectProperties($this->properties, $taskData);
            $newTask->setAdditionalProperties($properties);

            $this->availableTasks[] = $newTask;
        }
        return $this->availableTasks;
    }

    protected function generateAnalsis(&$wordCount){
        $analysis = new Analysis();

        if($wordCount != null){
            foreach ($wordCount as $key => $value){
                switch ($key){
                    case "ContextMatch":
                        break;
                    case "Repetitions":
                        $analysis->setRepetition($value);
                        break;
                    case "Translated":
                        break;
                    case "100%":
                        $analysis->setPercentage_100($value);
                        break;
                    case "95-99%":
                        $analysis->setPercentage_95($value);
                        break;
                    case "85-94%":
                        $analysis->setPercentage_85($value);
                        break;
                    case "75-84%":
                        $analysis->setPercentage_75($value + $analysis->getPercentage_75());
                        break;
                    case "50-74%":
                        $analysis->setPercentage_50($value);
                        break;
                    case "NoMatch":
                        $analysis->setNotMatch($value);
                        break;
                    case "MT":
                        $analysis->setPercentage_75($value + $analysis->getPercentage_75());
                        break;
                    default:
                        break;
                }
            }
            $analysis->calculateTotalWords();
            $analysis->calculateWeightedWords();
        }

        return $analysis;
    }

    /*protected function collectProperties($values, &$data){
        $properties = array();
        foreach ($this->source->getAdditionalProperties() as $property){
            try {
                $method = 'get'.ucfirst($property->getPropertyName());
                $value = $data->$method();
                $property = new TaskAditionalProperties();
                $property->setPropertyName("contentType");
                $addPropValue = trim($data->getContentType());
                $property->setPropertyValue(($addPropValue==""? "-":$addPropValue));
                $properties[] = $property;
            } catch (\Exception $e) {
                parent::addLog($e->getMessage(), \Functions::WARNING, json_encode($e->getTrace()));
            }
        }
        return $properties;
    }*/

    public function acceptTask(Task $task){
        $success = false;
        if(count($this->users) == 1){
            $connect = new JiraConnect($this->users[0]);
            if($connect->getDestinationAPI("/browse/".$task->getIdClientetask())){
                $negotiation_id = $connect->getNegotiationId();
                $success = $connect->accept($task->getIdClientetask(), $negotiation_id);

            }else{
                parent::addLog($connect->getErrorMessage(), \Functions::ERROR, "File => ".__FILE__."    Method => ".__METHOD__."    Line => ".__LINE__);
            }
        }
        return $success;
    }

    private function notifyErrorInMail(&$dataMail){

    }

    public function createTask($taskData, $tms = null, $user = null)
    {
        $newTask = new Task();
        $instructions = "<b>TP + TRANSLATION COMPLETE + <br />Completar la tarea en JIRA</b><br />Link de JIRA:<br />" . Jira::APS_URL . "/" . $taskData->getDestination() . "<br />
							<b>Comprueba que el número de links en JIRA y en GTT coincida. Si no es así, avisa al PM. ¡Gracias! </b>";
        try {
            $dueDate = new \DateTime(str_replace("/", "-", $taskData->getDueDate()) . " Europe/Madrid");
            $dueDate->setTimezone(new \DateTimeZone("UTC"));
            $dueDate = $dueDate->format("Y-m-d H:i:s");
        } catch (\Exception $e) {
            $dueDate = str_replace("/", "-", \Functions::currentDate());
        }

        $newTask->construct(
            \Functions::currentDate(),
            $taskData->getDestinationId(),
            $taskData->getTitleTask(),
            $instructions,
            \Functions::currentDateAsObject(), //TODO: Los correos de jira no traen la fecha de inicio asi que se coloco una fecha cualquiera para que no altere al momento de crear el uniqueTaskId
            $dueDate,
            ""
        );

        $sourceLanguage = parent::identifySourceLanguage($taskData->getSourceLanguage());
        if ($sourceLanguage != null) {
            $newTask->setSourceLanguage($sourceLanguage);
        }

        $targets = parent::identifyTargetLanguage(array($taskData->getTargetLanguage()));
        $newTask->setTargetsLanguage($targets);

        $dataSource = new DataSource();
        $dataSource->construct(
            $this->source->getSourceName(),
            "ON",
            \Functions::currentDate(),
            Jira::HARVESMETHOD,
            Jira::APS_URL,
            Jira::APS_URL . $taskData->getDestination(),
            /*$taskData->getMailContent()*/
            "",
            $this->source->getIdSource(),
            $this->users[0]->getIdApiUser()
        );
        $newTask->setDataSource($dataSource);

        $data = $tms->getTaskData($dataSource->getTmsTaskUrl());
        $analysis = $this->generateAnalsis($data["analysis"]);
        $data["contentType"] = $taskData->getContentType();
        if (!empty($newTask->getTargetsLanguage())) {
            $newTask->getTargetsLanguage()[0]->setAnalysis($analysis);
        }

        $newTask->setState(new State(State::COLLECTED));
        $properties = parent::collectProperties($this->properties, $data);
        $newTask->setAditionalProperties($properties);


        return $newTask;
    }

    public function collectTaskByIdClientTask($idTask)
    {
        $messages = glob(Jira::$jira_aps_mail_path . "*.msg");
        $jiraConnect = null;
        $taskExist = false;
        $mail = null;
        foreach ($messages as $message) {
            $messageName = str_replace(Jira::$jira_aps_mail_path, "", $message);

            $mail = new JiraMail($messageName);
            $mail->readMail();

            if($mail->getDestinationId() === $idTask){
                $taskExist = true;
                break;
            }
        }

        if($taskExist){
            $jiraConnect = new JiraConnect($this->users[0]);
            $newTask = $this->createTask($mail,$jiraConnect);
            $newTask->generateUniqueTaskId();
            $newTask->setTimeStamp(\Functions::currentDate());
            $newTask->setStartDate(\Functions::currentDate());
            $newTask->generateUniqueSourceId();
            $this->newTasks[] = $newTask;
            parent::collectTask();
        }else{
            throw new \Exception("Any task was found with this idClientTask = $idTask");
        }

        return (count($this->newTasks) > 0);
    }
}
?>