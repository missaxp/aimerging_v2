<?php
namespace harvestModule\sources\sourceComplements;


use common\downloadManager;
use common\exceptions\CurlErrorException;
use common\exceptions\sources\LTBDownloaderException;
use core\AI;
use Exception;
use Functions;

include_once BaseDir . "/common/exceptions/sources/apple_liox/WorldServerException.php";
include_once BaseDir . '/common/exceptions/CurlErrorException.php';
include_once BaseDir . "/common/exceptions/sources/LTBDownloaderException.php";

class LTBDownloader{
    private $username = 'LIOXNET\Olga.Lopez';
    private $password = 'L10nbridgeTMS@2014';
    private $linkPrefix = "https://glt.extranet.lionbridge.com/";
    private $download_path;
    private $url = "";

    private $cookieFile;


    public function __construct($url_to_download, $download_path){
        $this->url = $url_to_download;
        $this->cookieFile = tempnam(sys_get_temp_dir(), 'AI');
        $this->download_path = $download_path;
    }
    function __destruct() {
        if (file_exists($this->cookieFile)) {
            unlink($this->cookieFile);
        }
    }

    private function getHTMLLinks(){
        $ch = curl_init();

        curl_setopt_array($ch, array(
            CURLOPT_COOKIEJAR => $this->cookieFile,
            CURLOPT_COOKIEFILE => $this->cookieFile,
            CURLOPT_URL =>$this->url,
            CURLOPT_TIMEOUT => 30, //timeout after 30 secon
            CURLOPT_RETURNTRANSFER =>1,
            CURLOPT_HTTPAUTH => CURLAUTH_ANY,
            CURLOPT_USERPWD => "$this->username:$this->password"
        ));

        $result=curl_exec ($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code

        curl_close ($ch);

        if($status_code != 200){
            return false;
        }
        return $result;
    }

    private function getHTMLLinksLTB($html){
        $html = str_get_html($html);
        if($html === false) return false;
        $links = array();
        foreach($html->find('.itx a') as $element) {
            if($this->endsWith($element->plaintext, "ES")){
                $links[] = $element;
            }
        }
        return $links;
    }

    private function endsWith($haystack, $needle){
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    private function downloadLTBFile($link, $fileName){
        $fileUrl = $this->linkPrefix . $link;
        $fileUrl = str_replace(" ", "%20", $fileUrl);


        $curlCustomOptions = array(
            array(CURLOPT_RETURNTRANSFER, 1),
            array(CURLOPT_HTTPAUTH , CURLAUTH_ANY),
            array(CURLOPT_USERPWD, "$this->username:$this->password"),
        );

        $dm = new downloadManager();

        $rsp = $dm->setFileUrl($fileUrl)
            ->setAsync(false)
            ->setFileName("$fileName")
            ->setDownloadPath("$this->download_path")
            ->setSyncCurlOptions($curlCustomOptions)
            ->run();
        if($rsp){
            $file = array(
                "file_name" => $fileName,
                "file_name_extension" => "zip",
                "path" => $this->download_path . $fileName,
                "source" => "LTB Server",
                "source_path" => ""
            );

            return $file;
        }
        return false;
    }

    /**
     * @throws LTBDownloaderException
     */
    public function downloadLTB(){
        $ltb_html = $this->getHTMLLinks();
        if($ltb_html === false){
            throw (new LTBDownloaderException("Cannot get HTML Code. Check the credentials."))
            ->construct(__FUNCTION__, __NAMESPACE__, $args = func_get_args(), array(
                "url" => $this->url
            ));
        }
        $ltb_links = $this->getHTMLLinksLTB($ltb_html);
        if($ltb_links === false){
            throw (new LTBDownloaderException("Cannot get links in HTML code. Check the HTML code."))
            ->construct(__FUNCTION__, __NAMESPACE__, $args = func_get_args(), array(
                "htmlCode" => $ltb_html
            ));
        }

        $ltbFiles = array();

        foreach($ltb_links as $link){
            $fileName = explode("/" , $link->href);
            $fileName = end($fileName);
            $has_been_downloaded = $this->downloadLTBFile($link->href, $fileName);
            if($has_been_downloaded !== false){
                $ltbFiles[] = $has_been_downloaded;
            }
        }

        return $ltbFiles;

    }

}