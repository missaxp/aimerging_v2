<?php
namespace harvestModule\sources\sourceComplements;
use common\downloadManager;
use common\exceptions\AIException;
use Functions;

include_once BaseDir . '/common/downloadManager.php';
include_once BaseDir . '/simple_html_dom.php';

/**
 * This class is in charge of connecting and collecting the priorities of the tasks within the plunet portal.
 * Currently, only some properties found in the task are collected and the attached zip is downloaded to the task.
 * A word count was not found within the platform.
 *
 * @version 1.1 27/10/18
 * @author Ljimenez
 *
 */
class PlunetConnector {
	/**
	 * Authorization token on the platform
	 *
	 * @var string
	 */
	private $token;
	
	/**
	 * User name
	 *
	 * @var string
	 */
	private $user;
	
	/**
	 * user password
	 *
	 * @var string
	 */
	private $password;
	
	/**
	 * Key input[type="hidden"]
	 *
	 * @var string
	 */
	private $CSRF;
	
	/**
	 * State connection
	 *
	 * @var boolean
	 */
	private $isConnected;
	
	/**
	 * Task links
	 * @var array
	 */
	private $links;
	
	public const URLFILE = "/pagesUTF8/Sys_DirAnzeige.jsp?AnzeigeText=&Pfad=";
	
	public const COMPLEMENTURL = "&UPfade=true";
	
	private $url;
	/**
	 * State Task
	 * @var string
	 */
	public const INPROCESS = 'FDJob015';
	/**
	 * State Task
	 *
	 * @var string
	 */
	public const REQUESTED = 'FDJob012';
	/**
	 * State Task
	 * @var string
	 */
	public const ASSIGNED = 'FDJob014';

	private $idUser;

	public function getIdUser(){
	    return $this->idUser;
    }

	public function __construct($user = null, $password = null, $url = "https://bm.oettli.com"){
		$this->password = $password;
		$this->url = $url;
		$this->user = $user;
		$this->isConnected = $this->connect();
		if ($this->isConnected) {
			$this->getCSRF();
		}
	}
	
	/**
	 * Return state connection
	 *
	 * @return boolean True if the connection with the portal was established and false else
	 */
	 public function isConnect(){
	 	return $this->isConnected;
	 }
	 
	 /**
	  * This method looks for the CSRF property inside the form to be sent in the requests to filter the tasks by
	  * the state of the tasks.
	  *
	  * @return boolean True if you find the property or false if you can not find it
	  */
	 private function getCSRF(){
	 	
	 	$request = curl_init();
	 	curl_setopt($request, CURLOPT_URL, $this->url."/pagesUTF8/buero_termine.jsp");
	 	curl_setopt($request, CURLOPT_CUSTOMREQUEST, "GET");
	 	curl_setopt($request, CURLOPT_HEADER, true);
	 	curl_setopt($request, CURLOPT_ENCODING, "gzip");
	 	curl_setopt($request, CURLOPT_TIMEOUT, '10');
	 	
	 	$headers = array(
	 			'Content-Type: application/x-www-form-urlencoded',
	 			$this->token
	 	);
	 	curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
	 	curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
	 	
	 	try {
	 		$response = curl_exec($request);
	 		curl_close($request);
	 		$start = strpos($response, 'CSRF_NONCE');
	 		$string = substr($response, $start, (strlen($response) - $start));
	 		$start = strpos($string, 'value=');
	 		$start += 7;
	 		$string = substr($string, $start, 32);
	 		$this->CSRF = $string;
	 		return true;
	 	} catch(\Exception $e) {
	 		return false;
	 	}
	 }
	 
	 /**
	  * This method connects to the platform and filters the respect to find the token of the session.
	  *
	  * @return boolean
	  */
	 public function connect(){
	 	$isConnect = false;
	 	if($this->user != null && $this->password != null){
	 		$request = curl_init();
	 		curl_setopt($request, CURLOPT_URL, $this->url.'/pagesUTF8/login.jsp');
	 		curl_setopt($request, CURLOPT_POST, true);
	 		curl_setopt($request, CURLOPT_HEADER, true);
	 		$body = array(
	 				'+inp+LoginName' => $this->user,
	 				'PW' => $this->password
	 		);
	 		$headers = array(
	 				'Content-Type: application/x-www-form-urlencoded'
	 		);
	 		
	 		curl_setopt($request, CURLOPT_POSTFIELDS, http_build_query($body));
	 		curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
	 		curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
	 		
	 		try {
	 			$reponse = curl_exec($request);
	 			curl_close($request);
	 			
	 			$cookies = array();
	 			$match = array();
	 			$headers = explode("\r\n", preg_replace('/\x0D\x0A[\x09\x20]+/', ' ', $reponse));
	 			foreach($headers as $header) {
	 				if (preg_match('/([^:]+): (.+)/m', $header, $match)) {
	 					$match[0] = explode(":", $match[0])[0];
	 					if (isset($cookies[$match[0]])) {
	 						$cookies[$match[0]] = array(
	 								$cookies[$match[0]],
	 								$match[2]
	 						);
	 					} else {
	 						$cookies[$match[0]] = trim($match[2]);
	 					}
	 				}
	 			}
	 			
	 			if (isset($cookies["Set-Cookie"])) {
	 				$this->token = "Cookie: " . str_replace("; Path=/; Secure; HttpOnly", "", $cookies["Set-Cookie"]);
	 				$isConnect = true;
	 			}else{
                    \Functions::console("Plunet::Peticion para iniciar se sesion fallida");
	 				$isConnect = false;
	 			}
	 		} catch(\Exception $e) {
	 			$isConnect = false;
	 		}
	 	}
	 	return $isConnect;
	 }
	 
	 /**
	  * Search the links of the tasks that are with a state, inside the table at the bottom of the main page.
	  * The links are used to make the requests to obtain the properties of the tasks. The links are stored within
	  * the links attribute.
	  *
	  * @param string $state State of task to find
	  * @return array task links
	  */
	 public function filterTaskLinks($state,$idsTasks = null){
	 	
	 	$request = curl_init();
	 	$body = array(
	 			'SelectTab' => $state,
	 			'anchor' => 'AnchDash',
	 			'OutTab1OTP4' => '250',
	 			'CSRF_NONCE' => $this->CSRF
	 	);
	 	curl_setopt($request, CURLOPT_URL, $this->url.'/pagesUTF8/buero_termine.jsp');
	 	curl_setopt($request, CURLOPT_POST, true);
	 	curl_setopt($request, CURLOPT_HEADER, true);
	 	curl_setopt($request, CURLOPT_ENCODING, "gzip");
	 	curl_setopt($request, CURLOPT_TIMEOUT, '10');
	 	
	 	$headers = array(
	 			'Accept: text/html,application/xhtml+xml,application/xml;q=0.8',
	 			'Accept-Encoding: gzip, deflate, br',
	 			'Accept-Language: es-ES,es;q=0.9',
	 			'Cache-Control: max-age=0',
	 			'Connection: keep-alive',
	 			'Content-Type: application/x-www-form-urlencoded',
	 			$this->token,
	 			'Upgrade-Insecure-Requests: 1',
	 			'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36'
	 	);
	 	
	 	curl_setopt($request, CURLOPT_POSTFIELDS, http_build_query($body));
	 	curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
	 	curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
	 	try {
	 		$response = curl_exec($request);
            $httpCode = curl_getinfo($request, CURLINFO_HTTP_CODE);
			curl_close($request);
	 		if($httpCode == 200){
	 			$start = strpos($response, "tableScrollWrapper");
	 			$string = substr($response, $start, (strlen($response) - $start));
	 			$start = strpos($string, "</tr>");
	 			$end = strpos($string, "</table>");
	 			$taskList = substr($string, $start, ($end - $start));
	 			$taskList = explode("<tr", $taskList);

	 			if($idsTasks != null){
	 			    $linkFilter = array();
                    foreach ($idsTasks as $id){
                        foreach ($taskList as $value){
                            if(strpos($value,$id["id"]) !== false){
                                $linkFilter[] = $value;
                                break;
                            }
                        }
                    }
                    $taskList = $linkFilter;
                }

	 			$links = array();
	 			foreach($taskList as $task) {
	 				$start = strpos($task, "modal-params=");
	 				if ($start !== false) {
	 					$end = strpos($task, "modal-size=");
	 					$string = substr($task, $start, ($end - $start));
	 					$end = strpos($string, "class=");
	 					$start += 14;
	 					$end -= 14;
	 					$string = substr($string, 14, $end);
	 					$links[] = $string;
	 				}
	 			}
	 			$this->links = $links;
	 		}else{
                $ex =  (new AIException("NLG:: the session has been interrupted - Plunet::filerTaskLink response code:" . $httpCode))->construct(__METHOD__, __NAMESPACE__, $args = func_get_args(), array(
                    "response_code" => $httpCode,
                    "response" => $response
                ));
                Functions::logException($ex->getMessage(), Functions::WARNING, __CLASS__, $ex);
	 			$this->links = null;
	 		}
	 		
	 	} catch(\Exception $e) {
	 		return false;
	 	}
	 	return $this->links;
	 }
	 
	 /**
	  * Make the request to the platform to obtain the properties of the task.
	  *
	  * @param string $modalLink Task Link
	  * @return array|bool
	  */
	 public function getTaskData($modalLink = null){
	 	$task = array();
	 	if($modalLink != null){
	 		$request = curl_init();
	 		curl_setopt($request, CURLOPT_URL, $this->url."/rs/modal/job_modal?modal-params=".$modalLink);
	 		curl_setopt($request, CURLOPT_CUSTOMREQUEST, "GET");
	 		curl_setopt($request, CURLOPT_HEADER, true);
	 		curl_setopt($request, CURLOPT_ENCODING, "gzip");
	 		curl_setopt($request, CURLOPT_TIMEOUT, '10');
	 		
	 		$headers = array(
	 				'Content-Type: application/x-www-form-urlencoded',
	 				$this->token
	 		);
	 		curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
	 		curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
	 		
	 		try {
	 			$response = curl_exec($request);
                $httpCode = curl_getinfo($request, CURLINFO_HTTP_CODE);
	 			curl_close($request);
	 			if($httpCode == 200){
                    $modal = \str_get_html($response);
                    $span = $modal->find('h1 span');
                    $task['taskName'] = trim($span[0]->plaintext);
                    unset($span);

//                    $span = $modal->find('span[id="modal_JobAssistent"]');
//                    $task['contact'] = trim($span[0]->plaintext);
//                    unset($span);

                    /*$start = strpos($response, "instructionlistchecklist");
                    $start -= 11;
                    $string = substr($response, $start, $length - $start);
                    $end = strpos($string, "</tbody>");
                    $string = substr($string, 0, $end);
                    $instructions = $modal->find('table[id="instructionlistchecklist"]');
                    $table = \str_get_html($instructions[0]);
                    $trs = $table->find("tbody tr");
                    unset($instructions);
                    unset($table);
                    $instructions = "";
                    $count = 1;
                    foreach ($trs as $tr){
                        $row = \str_get_html($tr);
                        $colums = $row->find("td");
                        $colum = $colums[1]->plaintext;
                        $instructions .= "<p>".$count.".- ".$colum."</p>";
                        $count++;
                    }
                    $task['instruction'] = $instructions;*/

                    $comment = $modal->find('input[id="+inp+memo"]',0);
                    if($comment != null){
                        $comment = $comment->parent();
                        $comment = \str_get_html($comment);
                        $comment = $comment->find("span");
                        $task['instruction'] = $comment[0]->innertext;
                    }else{
                        $task['instruction'] = "";
                    }

                    unset($comment);

                    unset($modal);
                    //$task['originalData'] = $response;
                    $task['originalData'] = "";

                    $length = strlen($response);
                    $start = strpos($response, "modalJbLckAjxCntr");
                    $end = strpos($response, 'state-mark');
                    $string = substr($response, $start, ($end - $start));
                    $start = strpos($string, "<span>");
                    $end = strpos($string, "</span>");
                    $string = substr($string, $start + 6, ((strlen($string) - 19) - $start));
                    $task['idClient'] = trim($string);
                    unset($string);

                    $start = strpos($response, "Start date");
                    $string = substr($response, $start, $length - $start);
                    $start = strpos($string, "form-text");
                    $string = substr($string, $start + 11, 16);
                    $task['startDate'] = trim($string);
                    unset($string);

                    $start = strpos($response, "Job types");
                    $string = substr($response, $start, $length - $start);
                    $start = strpos($string, "<span");
                    $end = strpos($string, "</span>");
                    $string = substr($string, $start, $end);
                    $string = strip_tags($string);
                    $task['jobTypes'] = trim($string);
                    unset($string);

                    $start = strpos($response, "DoDoku");
                    $start += 6;
                    $string = substr($response, $start, $length - $start);
                    $end = strpos($string, "Out");
                    $end += 3;
                    $start = strpos($string,'%');
                    $string = substr($string, $start, $end - $start);
                    $task['linkOptional'] = $this->url.PlunetConnector::URLFILE.$string;
                    unset($string);

                    $start = strpos($response, "Mandatory field");
                    if ($start == false){
                        $start = strpos($response, "NgInputmodal_JobTerminBis");
                    }
                    $string = substr($response, $start, $length - $start);
                    $start = strpos($string, "form-text");
                    $string = substr($string, $start + 11, 16);
                    $task['dueDate'] = trim($string);
                    unset($string);

                    $start = strpos($response, "Prices");
                    $end = strpos($response, "EUR");
                    $string = substr($response, $start + 6, $end - ($start + 6));
                    $string = strip_tags($string);
                    $task['price'] = trim($string) . " EUR";
                    unset($string);

                    $start = strpos($response, "Gross quantity");
                    $string = substr($response, $start, $length - $start);
                    $end = strpos($string, "</span>");
                    $string = substr($string, 14, $end);
                    $task['wordCount'] = strip_tags($string);

                    $start = strpos($response, "Net quantity");
                    $string = substr($response, $start, $length - $start);
                    $end = strpos($string, "</span>");
                    $string = substr($string, 12, $end);
                    $task['weigthWord'] = strip_tags($string);

                    /*$start = strpos($response, "alkPrefixEigenschaftWert0");
                    $string = substr($response, $start, $length - $start);
                    $end = strpos($string, "</span>");
                    $string = substr($string, 27, $end);
                    $task['sourceLanguage'] = $this->filterLanguage(trim(strip_tags($string)));

                    $start = strpos($response, "alkPrefixEigenschaftWert1");
                    $string = substr($response, $start, $length - $start);
                    $end = strpos($string, "</span>");
                    $string = substr($string, 27, $end);
                    $task['targetLanguage'] = $this->filterLanguage(trim(strip_tags($string)));*/



                    $start = strpos($response, "jobworkzip");
                    $start --;
                    $string = substr($response, $start, $length - $start);
                    $end = strpos($string, "class");
                    $end -= 2;
                    $string = substr($string, 0, $end);
                    $task['link'] = strip_tags($string);
                }else{
                    $ex =  (new AIException("NLG:: the session has been interrupted"))->construct(__METHOD__, __NAMESPACE__, $args = func_get_args(), array(
                        "message" => "Plunet::Peticion para buscar propiedades de una Tarea con codigo: ".$httpCode,
                        "response_code" => $httpCode,
                        "response" => $response
                    ));
                    Functions::logException($ex->getMessage(), Functions::WARNING, __CLASS__, $ex);
	 			    return false;
                }
	 		} catch(\Exception $e) {
	 			return array();
	 		}
	 	}
	 	return $task;
	 }
	 
	 /**
	  * This method download to file .zip
	  *
	  * @param string $fileName file name with which it will be saved
	  * @param string $url task link
	  * @return string download token
	  */
	 public function downloadFile($fileName, $url, $downloadPath = ""){
	 	$asyncHash = null;
	 	$dm = new downloadManager();
	 	$rsp = $dm->setFileUrl($url)
	 	->setAsync(true)
	 	->setFileName($fileName)
	 	->setDownloadPath("NLG/Plunet/".$downloadPath)
	 	->addHeader(array(
	 			'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0',
	 			'Accept-Encoding: gzip, deflate, br',
	 			$this->token
	 	))
	 	->run();
	 	
	 	if(isset($rsp['success']) && $rsp['success']){
	 		$asyncHash = $rsp['data']['asyncHash'];
	 	}
	 	
	 	return $asyncHash;
	 }
	 
	 public function singleFileDownload($link){
	 	$request = curl_init();
	 	curl_setopt($request, CURLOPT_URL, $link."&UPfade=true");
	 	curl_setopt($request, CURLOPT_CUSTOMREQUEST, "GET");
	 	curl_setopt($request, CURLOPT_HEADER, true);
	 	curl_setopt($request, CURLOPT_ENCODING, "gzip");
	 	curl_setopt($request, CURLOPT_TIMEOUT, '10');
	 	
	 	$headers = array(
	 			'Content-Type: application/x-www-form-urlencoded',
	 			$this->token
	 	);
	 	curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
	 	curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
	 	$files = array();
	 	$files["source"] = array();
	 	try{
	 		$response = curl_exec($request);
            $httpCode = curl_getinfo($request, CURLINFO_HTTP_CODE);
	 		curl_close($request);
	 		if($httpCode == 200){
                $modal = \str_get_html($response);

                $tables = $modal->find('div[role="grid"]');
                // 	foreach ($tables as $table){ csrf-nonce
                if(is_array($tables) && !empty($tables)){
                    $table = $tables[0];
                    $tableDOM = \str_get_html($table);
                    if(!is_bool($tableDOM)){
                        $rows = $tableDOM->find('div[class="filebrowser-row"]');
                        foreach ($rows as $row){
                            $rowDOM = \str_get_html($row);
                            if($rowDOM->root->children[0]->class == "filebrowser-row"){
                                $file = array();
                                $file["name"] = trim($rowDOM->find('div[class="filebrowser-col filebrowser-name ellipsis"] a span')[0]->plaintext);
                                $file["link"] = $this->url. trim($rowDOM->find('div[class="filebrowser-col filebrowser-name ellipsis"] a')[0]->href);
                                $file["type"] = strtolower(trim($rowDOM->find('div[class="filebrowser-col filebrowser-type"] span')[0]->plaintext));
                                $file["size"] = trim($rowDOM->find('div[class="filebrowser-col filebrowser-size"]')[0]->plaintext);
                                $file["size"] = \Functions::parseToBytes($file["size"]);
                                $files["source"][] = $file;
                            }
                            if($rowDOM->root->children[0]->class == "filebrowser-row filebrowser-dir"){
                                $input = $modal->find('input[id="csrf-nonce"]');
                                $id = $input[0]->value;
                                $selectTable = trim($rowDOM->find('div[class="filebrowser-col filebrowser-name ellipsis"] a')[0]->href);
                                $selectTable = str_replace('javascript:SelectTabelle("',"",$selectTable);
                                $selectTable = str_replace('");',"",$selectTable);
                                $filesInDir = $this->searchFilesInDir($selectTable,$id);
                                $files["source"] = array_merge($files["source"],$filesInDir);
                            }
                            unset($rowDOM);
                        }
                        unset($tableDOM);
                        unset($rows);
                        unset($table);

                        $table = $tables[1];
                        $tableDOM = \str_get_html($table);
                        $rows = $tableDOM->find('div[class="filebrowser-row"]');
                        $folder = "";
                        foreach ($rows as $row){
                            $rowDOM = \str_get_html($row);
                            if($rowDOM->root->children[0]->class == "filebrowser-row filebrowser-dir"){
                                if(strpos(strtoupper($rowDOM->innertext), strtoupper("This folder does not contain any files"))===false){
                                    $folder = strip_tags($rowDOM->innertext);
                                    $folder = str_replace(" ","",$folder);
                                }
                            }

                            if($rowDOM->root->children[0]->class == "filebrowser-row"){
                                $file = array();
                                $file["name"] = trim($rowDOM->find('div[class="filebrowser-col filebrowser-name ellipsis"] a span')[0]->plaintext);
                                $file["link"] = $this->url. trim($rowDOM->find('div[class="filebrowser-col filebrowser-name ellipsis"] a')[0]->href);
                                $file["type"] = strtolower(trim($rowDOM->find('div[class="filebrowser-col filebrowser-type"] span')[0]->plaintext));
                                $file["size"] = trim($rowDOM->find('div[class="filebrowser-col filebrowser-size"]')[0]->plaintext);
                                $file["size"] = \Functions::parseToBytes($file["size"]);
                                $files["reference"][$folder][] = $file;
                            }
                            unset($rowDOM);
                        }

                        unset($tableDOM);
                        unset($rows);
                        unset($table);
                        unset($tables);
                    }
                }


            }else{
	 		    $files = null;
                $ex =  (new AIException("NLG:: the session has been interrupted"))->construct(__METHOD__, __NAMESPACE__, $args = func_get_args(), array(
                    "message" => "Plunet::Peticion para buscar archivos con codigo ".$httpCode,
                    "response_code" => $httpCode,
                    "response" => $response
                ));
                Functions::logException($ex->getMessage(), Functions::WARNING, __CLASS__, $ex);
            }

	 	}catch(\Exception $e){
	 		$files = null;
	 	}
	 	return $files;
	 }

	 private function searchFilesInDir($selection,$CSRF = null){
         $request = curl_init();
         curl_setopt($request, CURLOPT_URL, 'https://bm.oettli.com/pagesUTF8/Sys_DirAnzeige.jsp');
         curl_setopt($request, CURLOPT_POST, true);
         curl_setopt($request, CURLOPT_HEADER, true);
         $body = array(
             'SelectTab' => $selection,
             'CSRF_NONCE' => $CSRF,
             'OJDAU01' => 0,
             '+inp+OUTDIR19' => 60,
             'CheckXYZ' => 'Send',
             'AnzeigeTextPOST' => '',
             '+inp+SuchBoxForBox' => '',
             'lastFocusedElementID' => '',
             'LangFlag' => '',
             'exit' => '',
             'ContentBox' => '',
             'OpenContentBox' => '',
             'LoginPressed' => '',
             'SaveButton' => '',
             'yOffsetScroll' => 0,
             'xOffsetScroll' => 0,
             'HiddenWindowWidth' => 1012,
             'HiddenContentWidth' => 1000
         );
         $headers = array(
             'Content-Type: application/x-www-form-urlencoded',
             $this->token
         );

         curl_setopt($request, CURLOPT_POSTFIELDS, http_build_query($body));
         curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
         curl_setopt($request, CURLOPT_RETURNTRANSFER, true);

         $response = curl_exec($request);
         $httpCode = curl_getinfo($request, CURLINFO_HTTP_CODE);
         curl_close($request);
         $files = array();
         if($httpCode == 200){
             $modal = \str_get_html($response);

             $tables = $modal->find('div[role="grid"]');
             if(is_array($tables) && !empty($tables)) {
                 $table = $tables[0];
                 $tableDOM = \str_get_html($table);
                 if (!is_bool($tableDOM)) {
                     $rows = $tableDOM->find('div[class="filebrowser-row"]');
                     foreach ($rows as $row) {
                         $rowDOM = \str_get_html($row);
                         if ($rowDOM->root->children[0]->class == "filebrowser-row") {
                             if(empty($rowDOM->find('div[class="filebrowser-col filebrowser-name ellipsis"] a span'))){
                                 continue;
                             }
                             $file = array();
                             $file["name"] = trim($rowDOM->find('div[class="filebrowser-col filebrowser-name ellipsis"] a span')[0]->plaintext);
                             $file["link"] = $this->url . trim($rowDOM->find('div[class="filebrowser-col filebrowser-name ellipsis"] a')[0]->href);
                             $file["type"] = strtolower(trim($rowDOM->find('div[class="filebrowser-col filebrowser-type"] span')[0]->plaintext));
                             $file["size"] = trim($rowDOM->find('div[class="filebrowser-col filebrowser-size"]')[0]->plaintext);
                             $file["size"] = \Functions::parseToBytes($file["size"]);
                             $files[] = $file;
                         }
                     }
                 }
             }
         }
         return $files;
     }
}

?>