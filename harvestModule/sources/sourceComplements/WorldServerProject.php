<?php
namespace harvestModule\sources\sourceComplements;
use common\downloadManager;
use common\exceptions\AIException;
use common\exceptions\sources\apple_liox\WorldServerException;
use common\ZipFile;
use core\AI;
use Exception;
use Functions;

include_once BaseDir . "/common/exceptions/sources/apple_liox/WorldServerException.php";

class WorldServerProject{
    protected $worldServerOrigin = "https://ws01.apple.com";
    protected $baseUrl = "https://ws01.apple.com/ws-legacy";

    //const WORLD_SERVER_ORIGIN = "";

    protected $project_id = 0;
    protected $token;
    protected $cookieFile;
    protected $apiuser_id;

    protected $download_path_in_repository = "AppleLioxWS/";

    protected $xlzData;
    protected $xlzCheckbox;

    protected $files = array();

    protected $wordCount = array();

    public function getWordCount(){
        if(empty($this->wordCount))
            return $this->getConteoDePalabras();
        return $this->wordCount;
    }

    public function __construct($project_id, $token, $cookieFile, $apiuser_id){
        $this->project_id = $project_id;
        $this->token = $token;
        $this->cookieFile = $cookieFile;
        $this->apiuser_id = $apiuser_id;
    }

    /**
     * Gets the basic information of the project.
     */
    public function getProjectBasicInfo(){
        $curl = curl_init();

        $default_headers = array(
            "Accept: application/json, text/javascript, */*; q=0.01",
            "Content-Type: application/x-www-form-urlencoded",
            "Origin: " . $this->worldServerOrigin,
            "Postman-Token: 7358f6c2-dee2-46a5-9653-4d689a5cf97f",
            "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36",
            "X-Requested-With: XMLHttpRequest",
            "cache-control: no-cache"
        );



        $url = "$this->baseUrl/ws_ext?servlet=ListingServlet&type=projectgroup&start=0&count=100&token=$this->token";


        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_COOKIEJAR => $this->cookieFile,
            CURLOPT_COOKIEFILE => $this->cookieFile,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "folderid=allFilter&key=$this->project_id",
            CURLOPT_HTTPHEADER => $default_headers,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if($err){
            return false;
        }else{
            $data = json_decode($response);
            foreach ($data as $project_info){
                if($project_info->projectgroupid == $this->project_id){
                    return $project_info;
                }
            }
            return null;
        }

    }

    /**
     * Gets the extended information of the project. This is used for getting the additional instructions of the task.
     */
    public function getProjectExtendedInfo(){
        $curl = curl_init();

        $default_headers = array(
            "Accept: application/json, text/javascript, */*; q=0.01",
            "Accept-Encoding: gzip, deflate, br",
            "Accept-Language: es-ES,es;q=0.9",
            "Content-Type: application/x-www-form-urlencoded",
            "Connection: keep-alive",
            "Origin: " . $this->worldServerOrigin,
            "Postman-Token: 7358f6c2-dee2-46a5-9653-4d689a5cf97f",
            "Referer: $this->baseUrl/assignments_projects/?&token=$this->token",
            "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36",
            "X-Requested-With: XMLHttpRequest",
            "cache-control: no-cache",
        );


        $url = "$this->baseUrl/ws_ext?servlet=InstructionServlet&token=$this->token";

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_COOKIEJAR => $this->cookieFile,
            CURLOPT_COOKIEFILE => $this->cookieFile,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "projectgroupid=$this->project_id&folderid=allFilter&requestId=2",
            CURLOPT_HTTPHEADER => $default_headers,
        ));

        $response = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $err = curl_error($curl);

        curl_close($curl);

        if($err) {
            return false;
        } else {
            if (($statusCode == 200)) {
                return $response;
            } else {
                return false;
            }
        }
    }

    /**
     * This method downloads all the project files.
     * @throws WorldServerException
     */
    public function downloadFiles(){
        try{
            $setup = AI::getInstance()->getSetup();
        } catch(Exception $e){
            Functions::console("AppleLiox: Cannot access to setup variable");
            return false;
        }
        if (!file_exists($setup->repository . $this->download_path_in_repository . "$this->project_id")) {
            $path = $setup->repository . $this->download_path_in_repository . "$this->project_id";
            mkdir($path);
        }
        if (!file_exists($setup->repository . $this->download_path_in_repository . "$this->project_id/reference_files")) {
            mkdir($setup->repository. $this->download_path_in_repository . "$this->project_id/reference_files");
        }
        if (!file_exists($setup->repository  . $this->download_path_in_repository. "$this->project_id/source")) {
            mkdir($setup->repository . $this->download_path_in_repository . "$this->project_id/source");
        }
        if (!file_exists($setup->repository. $this->download_path_in_repository . "$this->project_id/xlz")) {
            mkdir($setup->repository. $this->download_path_in_repository . "$this->project_id/xlz");
        }
        $conteo = false;
        try{
            $conteo = $this->getConteoDePalabras();
        } catch(Exception $e){
            throw (new WorldServerException(WorldServerException::CANNOT_DOWNLOAD_CSV))->construct(__METHOD__, __NAMESPACE__, $func = func_get_args(), array("project_id" => $this->project_id));
        }
        if($conteo === false){
            Functions::console("AppleLiox: Cannot get the Word Count from project id: " . "$this->project_id");
            throw (new WorldServerException(WorldServerException::CANNOT_DOWNLOAD_CSV))->construct(__METHOD__, __NAMESPACE__, $func = func_get_args(), array("project_id" => $this->project_id));
        }
        if(!$this->downloadReferenceFiles()){
            throw (new WorldServerException(WorldServerException::CANNOT_DOWNLOAD_REFERENCE_FILES))->construct(__METHOD__, __NAMESPACE__, $func = func_get_args(), array("project_id" => $this->project_id));
        }
        if(!$this->downloadSourceFiles()){
            throw (new WorldServerException(WorldServerException::CANNOT_DOWNLOAD_SOURCE_FILES))->construct(__METHOD__, __NAMESPACE__, $func = func_get_args(), array("project_id" => $this->project_id));
        }

        $json_xlz = $this->getXLZJson();
        if($json_xlz === false){
            throw (new WorldServerException(WorldServerException::CANNOT_ANALYZE_XLZ_JSON))->construct(__METHOD__, __NAMESPACE__, $func = func_get_args(), array("project_id" => $this->project_id));
        }
        $response_iframe = $this->postToIframe($json_xlz->url , $json_xlz->projectids[0]);
        if($response_iframe === false){
            throw (new WorldServerException(WorldServerException::CANNOT_ANALYZE_XLZ_IFRAME))->construct(__METHOD__, __NAMESPACE__, $func = func_get_args(), array("project_id" => $this->project_id));
        }

        $data = $this->analizarIframe($response_iframe);
        if (!$data){
            throw (new WorldServerException(WorldServerException::CANNOT_ANALYZE_XLZ_IFRAME))->construct(__METHOD__, __NAMESPACE__, $func = func_get_args(), array("project_id" => $this->project_id));
        }


        $response_postDownloadXlZ = $this->postXLZIframe();
        if (!$response_postDownloadXlZ){
            throw (new WorldServerException(WorldServerException::CANNOT_ANALYZE_XLZ_IFRAME))->construct(__METHOD__, __NAMESPACE__, $func = func_get_args(), array("project_id" => $this->project_id));
        }
        $data = $this->analizarpostXLZIframeResponse($response_postDownloadXlZ);
        if (!$data){
            throw (new WorldServerException(WorldServerException::CANNOT_ANALYZE_XLZ_DOWNLOAD_POST))->construct(__METHOD__, __NAMESPACE__, $func = func_get_args(), array("project_id" => $this->project_id));
        }
        try{
            $response_download_xlz = $this->postPostXLZIframe($data);
        } catch(Exception $e){
            Functions::console("Cannot download xlz for project id: $this->project_id") ;
            throw (new WorldServerException(WorldServerException::CANNOT_ANALYZE_XLZ_POST))->construct(__METHOD__, __NAMESPACE__, $func = func_get_args(), array("project_id" => $this->project_id));
        }
        if (!$response_download_xlz){
            Functions::console("Cannot download xlz for project id: $this->project_id") ;
            throw (new WorldServerException(WorldServerException::CANNOT_DOWNLOAD_XLZ))->construct(__METHOD__, __NAMESPACE__, $func = func_get_args(), array("project_id" => $this->project_id));
        }

        return $this->files;
    }

    /**
     * Downloads the CSV that cointain the words count.
     * @throws Exception If it cannot write to the specified file
     */
    public function getConteoDePalabras(){
        try{
            $setup = AI::getInstance()->getSetup();
        } catch(Exception $e){
            return false;
        }

        $curl = curl_init();

        $url = "https://ws01.apple.com/ws-legacy/ws_ext?servlet=ViewScopingServlet&projectgroupid=$this->project_id&csv=1&token=$this->token";

        $default_headers = array(
            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
            "Accept-Encoding: gzip, deflate, br",
            "Accept-Language: es-ES,es;q=0.9",
            "Content-Type: application/x-www-form-urlencoded",
            "Connection: keep-alive",
            "Origin: https://ws01.apple.com",
            "Postman-Token: 7358f6c2-dee2-46a5-9653-4d689a5cf97f",
            "Referer: https://ws01.apple.com/ws-legacy/assignments_projects/?^&token=$this->token",
            "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36",
            "X-Requested-With: XMLHttpRequest",
            "cache-control: no-cache",
            "Upgrade-Insecure-Requests: 1"
        );


        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_COOKIEJAR => $this->cookieFile,
            CURLOPT_COOKIEFILE => $this->cookieFile,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => $default_headers,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if($err) {
            return false;
        } else {
            if($statusCode == 200){
                return $this->getWordCountFromCsv($response);
            } else {
                return false;
            }
        }

    }

    /**
     * Gets the word count from the downloaded csv
     * @param String $response The CSV in a String
     * @return array returns the WWC from the CSV file
     */
    protected function getWordCountFromCsv($response){

        $array = explode("\n", $response);
        $line_count = explode(",", $array[1]);
        
        $this->wordCount = array(
            "w101" => $line_count[4],
            "w100" => $line_count[5],
            "w95" => $line_count[6],
            "w85" => $line_count[7],
            "w75" => $line_count[8],
            "repeat" => $line_count[9],
            "total" => $line_count[10],
            "target_language" => $line_count[1],
        );
        return $this->wordCount;
    }

    /**
     * Gets all the URLs of the reference files and pass it to downloadSingleFile.
     */
    public function downloadReferenceFiles(){
        try{
            $setup = AI::getInstance()->getSetup();
        } catch(Exception $e){
            return false;
        }
        $data = json_decode ($this->getProjectExtendedInfo());
        foreach($data->references as $archivo){
            $download_result = false;
            try{
                $download_result = $this->downloadSingleFile("$this->worldServerOrigin" . $archivo->url, "$this->project_id/reference_files/" , $this->project_id."-".$archivo->file, "REFERENCE");
            } catch(Exception $e){
                $aiex = (new AIException($e->getMessage()))->construct(__METHOD__,__NAMESPACE__, $f = func_get_args());
                Functions::logException($e->getMessage(), Functions::ERROR, __CLASS__, $aiex);
                return false;
            }
            if( $download_result === false){
                //cho "Unreachable file: $archivo->url";
                return false;
            }

        }

        return true;
    }

    /**
     * This function download every reference file included in the task.
     * @param $url String Url to consult
     * @param $file_path String Path to save the file
     * @param $file_name String name of the file
     * @param $typeOfFile String type of file (SOURCE or REFERENCE)
     * @return bool Returns true if the download was completed successfully
     * @throws Exception On Curl Error
     */
    protected function downloadSingleFile($url, $file_path, $file_name, $typeOfFile = "SOURCE"){

        $default_headers = array(
            "Accept: application/json, text/javascript, */*; q=0.01",
            "Accept-Encoding: gzip, deflate, br",
            "Accept-Language: es-ES,es;q=0.9",
            "Content-Type: application/x-www-form-urlencoded",
            "Connection: keep-alive",
            "Origin: " . $this->worldServerOrigin,
            "Postman-Token: 7358f6c2-dee2-46a5-9653-4d689a5cf97f",
            "Referer: $this->baseUrl/assignments_projects/?&token=$this->token",
            "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36",
            "X-Requested-With: XMLHttpRequest",
            "cache-control: no-cache"
        );
        $curlCustomOptions = array(
            array(CURLOPT_COOKIEJAR, $this->cookieFile),
            array(CURLOPT_COOKIEFILE, $this->cookieFile),
            array(CURLOPT_TIMEOUT, 200),
        );

        $dm = new downloadManager();

        $rsp = $dm->setFileUrl($url)
            ->setAsync(false)
            ->setFileName("$file_name")
            ->setDownloadPath($this->download_path_in_repository . $file_path)
            ->setSyncCurlOptions($curlCustomOptions)
            ->addHeader($default_headers)
            ->run();
        if($rsp){
            if (!isset($this->files[$typeOfFile])){
                $this->files[$typeOfFile] = array();
            }
            $extensionArchivo = explode(".", $file_name);
            $extensionArchivo = "." . end($extensionArchivo);
            $this->files[$typeOfFile][] = array(
                "file_name" => $file_name,
                "file_name_extension" => $extensionArchivo,
                "path" => AI::getInstance()->getSetup()->repository . $this->download_path_in_repository . $file_path . $file_name,
                "source" => "AppleWorldServer",
                "source_path" => ""
            );
        }
        return $rsp;

    }
    /**
     * Downloads all sources files.
     */
    public function downloadSourceFiles(){
        try{
            $setup = AI::getInstance()->getSetup();
            $this->downloadSingleFile(
                "$this->baseUrl/ws_ext?servlet=DownloadServlet&op=onedownload&sourcefile=true&folderid=allFilter&projectgroupid=$this->project_id&token=$this->token",
                "$this->project_id/source/", "$this->project_id-source.zip", "REFERENCE");

        } catch(Exception $e){
            return false;
        }
        return true;
    }
    /**
     * Downloads and parse a JSON that contains the links and token for download the xlz file.
     */
    protected function getXLZJson(){
        //

        $curl = curl_init();

        $default_headers = array(
            "Accept:application/json, text/javascript, */*; q=0.01",
            "X-Requested-With: XMLHttpRequest",
            "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36"
        );



        curl_setopt_array($curl, array(
            CURLOPT_URL => "$this->baseUrl/ws_ext?servlet=NewToOldUIInterceptor&requestType=Export&token=$this->token&folderid=allFilter&projectgroupid=$this->project_id&token=$this->token",
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_COOKIEJAR => $this->cookieFile,
            CURLOPT_COOKIEFILE => $this->cookieFile,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => $default_headers,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if($err) {
            return false;
        } else {
            if($statusCode == 200){
                $json = json_decode($response);
                return $json;
            } else {
                return false;
            }
        }

    }

    /**
     * Makes a request for retrive the HTML that contains the URL of the xlz file.
     * @param $url string URL to post
     * @param $project_json_id String The checkbox parameter
     * @return bool|string False on error HTTP response o success
     */
    protected function postToIframe($url, $project_json_id){
        $this->xlzCheckbox = $project_json_id;
        $curl = curl_init();

        $default_headers = array(
            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
            "Content-Type: application/x-www-form-urlencoded",
            "Origin: $this->worldServerOrigin",
            "Postman-Token: e99edbc7-5e3c-4348-9233-d7ce910e6558",
            "Upgrade-Insecure-Requests: 1",
            "Referer: $this->baseUrl/assignments_projects/iframe.html?type=export&token=$this->token&folderid=allFilter&projectgroupid=$this->project_id"
        );

        curl_setopt_array($curl, array(
            CURLOPT_URL => "$this->baseUrl/$url",
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_COOKIEJAR => $this->cookieFile,
            CURLOPT_COOKIEFILE => $this->cookieFile,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => "checkbox=$project_json_id",
            CURLOPT_HTTPHEADER => $default_headers,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if ($err) {
            //echo "cURL Error #:" . $err;
            return false;
        } else {
            if($statusCode == 200)
                return $response;
            else
                return false;
        }
    }

    /**
     * Analizes the HTML retrived by PostToIframe function and return the pagellocation, token and random keys.
     * @param $html_str String The HTML code in a string
     * @return array containing de pagelocation, random data, pagetoken and exporttoken
     */
    protected function analizarIframe($html_str){
        $lines_of_code = explode(" ", $html_str);
        $res = array();
        for($i = count($lines_of_code) - 1; $i >= 0; $i--){
            if (strpos($lines_of_code[$i], 'pagelocation') === 0) {
                $res["pagelocation"] = $lines_of_code[$i];
                $res["pagetoken"] = $lines_of_code[$i-2];
                return $this->procesarDatosIframe($res);
            }
        }
        return $res;
    }

    /**
     * This function sanitize the xlz keys.
     * @param $data array Array containing the pagelocation and pagetoken in raw format
     * @return array|bool That contain the data to download the XLZ file or False on error
     */
    protected function procesarDatosIframe($data){
        $res = array();
        try{
            $res["pagelocation"] = explode("pagelocation='", $data["pagelocation"])[1];
            $res["pagelocation"] = str_replace("'", "", $res["pagelocation"]);
            $res["pagelocation"] = str_replace("\n", "", $res["pagelocation"]);
            $res["pagelocation"] = str_replace(";", "", $res["pagelocation"]);
            $res["pagelocation"] = str_replace("</script></html>", "", $res["pagelocation"]);

            $res["random"] = explode("random=", $data["pagelocation"])[1];
            $res["random"] = str_replace("';", "", $res["random"]);
            $res["random"] = str_replace("\n", "", $res["random"]);

            $res["pagetoken"] = explode("pagetoken=", $data["pagetoken"])[1];
            $res["pagetoken"] = str_replace("';", "", $res["pagetoken"]);
            $res["pagetoken"] = str_replace("'", "", $res["pagetoken"]);
            $res["pagetoken"] = str_replace("\n", "", $res["pagetoken"]);

            $res["exporttoken"] = explode("&", $res["pagelocation"])[1];
            $res["exporttoken"] = explode("token=", $res["exporttoken"])[1];

            $this->xlzData = $res;
            return $this->xlzData;
        } catch(Exception $e){
            return false;
        }

    }

    /**
     * This function makes a request to retrive HTML that contains the URL of the xlz file.
     */
    protected function postXLZIframe(){

        $curl = curl_init();

        $url = "$this->baseUrl/" . $this->xlzData["pagelocation"];
        $pagelocation = $this->xlzData["pagelocation"];
        $formlocation = urlencode($pagelocation);

        $default_headers = array(
            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
            "Accept-Language: es-ES,es;q=0.9",
            "Cache-Control: max-age=0",
            "Content-Type: application/x-www-form-urlencoded",
            "Upgrade-Insecure-Requests: 1",
            "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36",
        );


        curl_setopt_array($curl, array(
            CURLOPT_URL => "$url",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_COOKIEJAR => $this->cookieFile,
            CURLOPT_COOKIEFILE => $this->cookieFile,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 300,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "scheme=desktopWorkbench&url_of_origin=&SegmentExclusionSelect=0&tmFilterId=-1&tdFilterId=-1&checkbox=$this->xlzCheckbox&cur_step=assetExportStep2&mode=projects&formAction=$formlocation&submittedBy=ok&methodUsed=POST",
            CURLOPT_HTTPHEADER => $default_headers,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if ($err) {
            //echo "cURL Error #:" . $err;
            return false;
        } else {
            if($statusCode != 200){
                return false;
            }
            return ($response);
        }
    }

    /**
     * Analyzes the response of the postXLZIframe in order to get the URL of the xlz file.
     * Returns false if doesn't exists the wwbfile. URL if is found.
     */
    protected function analizarpostXLZIframeResponse($data){
        $html = str_get_html($data);
        $url_archivo = "";
        foreach($html->find('input') as $input) {
            $name = $input->name;
            if ($name == "wwbfile") {
                $url_archivo = $input->value;
            }
        }

        if($url_archivo === ""){
            return false;
        }

        return $url_archivo;
    }

    /**
     * This is the function that downloads the xlz file.
     * @param $url_archivo
     * @return bool True if successful
     * @throws Exception On error
     */
    protected function postPostXLZIframe($url_archivo){
        $url = "$this->baseUrl/" . $this->xlzData["pagelocation"];
        $default_headers = array(
            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
            "Accept-Language: es-ES,es;q=0.9",
            "Cache-Control: max-age=0",
            "Content-Type: application/x-www-form-urlencoded",
            "Upgrade-Insecure-Requests: 1",
            "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36",
        );

        $curlCustomOptions = array(
            array(CURLOPT_COOKIEJAR, $this->cookieFile),
            array(CURLOPT_COOKIEFILE, $this->cookieFile),
        );

        $body = array(
            "download" => "yes",
            "wwbfile" => "$url_archivo",
            "suggestedname"=>"xliff_projects.zip"
        );

        $dm = new downloadManager();

        $rsp = $dm->setFileUrl($url)
            ->setAsync(false)
            ->setMethod("post")
            ->setFileName("$this->project_id-xlz.zip")
            ->setDownloadPath($this->download_path_in_repository . "$this->project_id/xlz/")
            ->setSyncCurlOptions($curlCustomOptions)
            ->addBody($body)
            ->addHeader($default_headers)
            ->run();
        if($rsp){
            $path = AI::getInstance()->getSetup()->repository . $this->download_path_in_repository . "$this->project_id/xlz/";
            $filePath = $path . "$this->project_id-xlz.zip";

            $has_been_extracted = ZipFile::unZipFile($filePath, $path);
            if($has_been_extracted){
                unlink($filePath);
                $xlzFiles = glob($path . "*.xlz");
                foreach ($xlzFiles as $file){
                    $name = explode("/", "$file");
                    $name = end($name);
                    $this->files["REFERENCE"][] = array(
                        "file_name" => "$name",
                        "file_name_extension" => "xlz",
                        "path" => $file,
                        "source" => "AppleWorldServer",
                        "source_path" => ""
                    );
                }
            } else {
                Functions::addLog("Cannot unzip file $filePath", Functions::ERROR, __CLASS__);
            }
        }
        return $rsp;
    }

}