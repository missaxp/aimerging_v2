<?php


namespace harvestModule\sources\sourceComplements;

use common\downloadManager;
use common\exceptions\AIException;
use common\ZipFile;
use core\AI;
use Throwable;


include_once BaseDir . '/simple_html_dom.php';
include_once BaseDir . '/common/ZipFile.php';


/**
 * Class ArgosConnector
 * @package harvestModule\sources\sourceComplements
 * @author ljimenez
 */
class ArgosConnector
{
    private $user;
    private $password;
    private $url;
    private $cookieFile;
    public const ASSIGNMENT = "Status_Submited";
    public const INPROGRESS = "Status_InProgress";


    /**
     * Deletes cookieFiles when the execution finish
     */
    function __destruct() {
        if (file_exists($this->cookieFile)) {
            unlink($this->cookieFile);
        }
    }

    public function __construct($user = null,$password = null ,$url = "https://supplier.argosmultilingual.com")
    {
        $this->user = $user;
        $this->password = $password;
        $this->url = $url;
        $this->cookieFile = tempnam(sys_get_temp_dir(), 'AI');
        try{
            $this->connect();
        }catch (ArgosException $ex){
            throw  $ex;
        }
    }

    /**
     * Login on TMS Argos
     *
     * @return bool true if connection success or fale else
     * @throws ArgosException
     */
    private function connect()
    {
        $validation = false;
        $request = curl_init();
        curl_setopt($request, CURLOPT_URL, $this->url.'/Account/Login?ReturnUrl=%2fHome');
        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt($request, CURLOPT_HEADER, true);
        $body = array(
            'UserName' => $this->user,
            'Password' => $this->password,
            'RememberMe' => false,
            'BrowserCTZ' => '+01:00,1',
            'NumberDecimalSeparator' => ',',
            'NumberGroupSeparator' => '.',
            'ScreenWidth' => '900',
            'ScreenHeight' => '1600',
            'button' => '',
            'DXScript' => '1_233,1_169,1_135,1_132,1_208,1_221,1_215,1_218,1_134,17_39,17_3,1_214,1_138,17_8,1_226,1_151,17_10,1_216,1_153,1_152,17_11,1_167,1_175,1_231,1_193,1_195,1_197,1_232,1_179,17_12,1_225,1_224,1_207,17_38,1_139,1_180,1_219,1_217,1_154,1_228,1_194,1_191,1_198,17_15,17_17,1_200,1_201,17_19,1_202,1_203,17_20,17_21,1_181,17_14,1_205,1_209,17_24,1_222,17_26,1_220,1_223,17_30,1_227,17_34,17_37,4_105,1_150,5_5,5_4,4_114,4_99,4_100,4_102,4_103,4_101,4_92,4_93,4_94,4_96,4_97,4_91,4_98,4_112,4_113,4_60,4_124,4_108,4_123,4_119,4_126,4_122,4_110,4_26,4_35,4_29,4_83,4_59,4_38,4_58,4_30,4_27,4_31,4_32,4_33,4_34,4_37,4_39,4_41,4_43,4_28,4_44,4_45,4_46,4_47,4_49,4_54,4_55,4_56,4_57,4_61,4_66,4_67,4_68,4_69,4_70,4_72,4_73,4_74,4_75,4_80,4_62,4_63,4_64,4_65,4_81,4_82,4_71,4_42,4_48,4_36,4_51,4_52,4_50,4_40,4_78,4_77,4_79,4_76,4_84,4_85,4_86,4_87,4_53,4_25,4_109,4_88,4_107,4_127,4_130,4_131,4_132,4_128,4_118,4_106,4_111,4_104,17_18,4_24,4_115,1_166,1_164,1_170,4_116,4_117,1_161,1_163,4_120,1_156,1_159,17_1,1_196,17_16,1_171,1_155,17_0,1_157,17_2,1_158,17_4,1_160,1_177,17_7,1_165,17_9,1_204,1_173,1_174,17_22,17_23,1_172,1_176,17_35,1_178,6_12,17_36',
            'DXCss' => '/favicon.ico,/Content/bootstrap.min.css,/Content/font-awesome.css,/Content/ie.css,102_331,1_33,1_35,102_333,1_29,1_3,102_143,1_18,102_145,102_148,102_150,1_19,1_17,1_16,102_152,4_7,4_1,4_2,102_154,5_1,102_292,1_4,1_9,1_5,9_11,9_13,9_17,102_357,9_1,102_359,9_3,102_294,6_2,102_296,/Content/css?v=67vttVjXMPHyL6lWNwO3VccW2a3HXB9krNg8NUHqrOk1'
        );
        $headers = array(
            'Content-Type: application/x-www-form-urlencoded'
        );

        curl_setopt($request, CURLOPT_POSTFIELDS, http_build_query($body));
        curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_COOKIEJAR, $this->cookieFile);
        curl_setopt($request, CURLOPT_COOKIEFILE, $this->cookieFile);

        curl_exec($request);
        $httpCode = curl_getinfo($request, CURLINFO_HTTP_CODE);
        curl_close($request);


        if($httpCode === 302){
            $validation = true;
        }else{
            throw new ArgosException("Login fail");
        }
        return $validation;
    }

    /**
     * Listed Tasks
     *
     * @param string $state State of task
     * @return array All properties tasks
     * @throws ArgosException
     */
    public function getTasks($state = "Status_Submited", $ids = array()){
        $request = curl_init();
        curl_setopt($request, CURLOPT_URL, $this->url.'/Assignment/Assignments?filter='.$state);
        curl_setopt($request, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($request, CURLOPT_HEADER, true);

        $headers = array(
            'Content-Type: application/x-www-form-urlencoded'
        );

        curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_COOKIEJAR, $this->cookieFile);
        curl_setopt($request, CURLOPT_COOKIEFILE, $this->cookieFile);

        $response = curl_exec($request);
        $httpCode = curl_getinfo($request, CURLINFO_HTTP_CODE);
        curl_close($request);
        if($httpCode === 200){
            $tasks = array();
            $page = \str_get_html($response);
            $tbody = $page->find('table[id="gvAssignmentsList_DXMainTable"] tbody');
            $tbodyDOM = \str_get_html($tbody[0]);
            $rows = $tbodyDOM->find('tr[class="dxgvDataRow_TMSArgosBlue"]');
            $count = 0;
            foreach($rows as $row){
                $task = array();
                $rowDOM = \str_get_html($row);
                $task["id"] = trim($rowDOM->find('td[id="gvAssignmentsList_tccell'.$count.'_0"]')[0]->plaintext);
                if(!isset($ids[$task["id"]])){
                    $count++;
                    unset($rowDOM);
                    continue;
                }
                $task["taskLink"] = $this->url.trim($rowDOM->find('td[id="gvAssignmentsList_tccell'.$count.'_0"] a')[0]->href);
                $task["MT"] = isset($rowDOM->find('span.dxWeb_edtCheckBoxChecked_TMSArgosBlue')[0])?true:false;
                $task["target"] = $rowDOM->find('td[id="gvAssignmentsList_tccell'.$count.'_7"]')[0]->plaintext;
                $dueDate = $rowDOM->find('td[id="gvAssignmentsList_tccell'.$count.'_10"]')[0];
                $task["dueDate"] = str_replace("-","",$dueDate->plaintext);
                try{
                    $details = $this->getDetailsTasks($task["taskLink"]);
                }catch (ArgosException $ex){
                    throw $ex;
                }
                $task = array_merge($details,$task);
                $tasks[$task["id"]] = $task;
                $count++;
                unset($rowDOM);
            }
        }else{
            throw new ArgosException("Has not recollected tasks list in state ".$state);
        }
        unset($response);

        return $tasks;
    }

    /**
     * Gets HTML with all properties tasks and option by accept
     *
     * @param string $url task link in TMS Argos
     * @return array properties of task
     * @throws ArgosException
     */
    public function getDetailsTasks($url){
        $task = array();
        $request = curl_init();
        curl_setopt($request, CURLOPT_URL, $url);
        curl_setopt($request, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($request, CURLOPT_HEADER, true);

        $headers = array(
            'Content-Type: application/x-www-form-urlencoded'
        );
        curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_COOKIEJAR, $this->cookieFile);
        curl_setopt($request, CURLOPT_COOKIEFILE, $this->cookieFile);


        $response = curl_exec($request);
        $httpCode = curl_getinfo($request, CURLINFO_HTTP_CODE);
        curl_close($request);
        if($httpCode === 200) {
            $length = strlen($response);
            $start = strpos($response, "Instructions");
            $string = substr($response, $start, $length - $start);
            $start = strpos($string, "<textarea");
            $string = substr($string, $start, strlen($string) - $start);
            $end = strpos($string, "</div");
            $string = substr($string, 0, $end);
            $string = strip_tags($string);
            $task["instructions"] = nl2br(trim($string));
            unset($string);

            $property = $this->searchProperty("Task Name",$response);
            if($property !== null){
                $task["name"] = $property;
            }

            $property = $this->searchProperty("Source Language",$response);
            if($property !== null){
                $task["source"] = $property;
                $source = strrchr($task["source"],"(");
                if($source !== false){
                    $source = str_replace("(","",$source);
                    $source = str_replace(")","",$source);
                    $task["source"] = $source;
                }
            }

            $property = $this->searchProperty("LanguagePairServiceTask_LanguagePairService_BatchLanguagePair_ProjectBatch_Project_ClientId",$response);
            if($property !== null){
                $task["client"] = $property;
            }

            $property = $this->searchProperty("Batch Name",$response);
            if($property !== null){
                $task["batchName"] = $property;
            }

            $property = $this->searchProperty("Project Number",$response);
            if($property !== null){
                $task["projectNumber"] = $property;
            }

            $property = $this->searchProperty("Workload",$response);
            if($property !== null){
                $property = str_replace("Hour(s)","",$property);
                $task["Workload"] = floatval(trim($property))*60;
            }

            $details = \str_get_html($response);
            $rows = $details->find('table[id="logBreakdownTable"] tbody tr');

            if(is_array($rows)){
                foreach ($rows as $row) {
                    $row = \str_get_html($row);
                    $columns = $row->find("td");
                    $unitPrice = floatval($columns[3]->plaintext);
                    if($unitPrice > 0){
                        $task["wordCount"][trim($columns[0]->plaintext)] = trim($columns[1]->plaintext);
                    }

                    unset($row);
                    unset($columns);
                }
            }

            $linkFiles = $details->find('a[id="lnkDownloadAllAsZip"]');
            if($linkFiles != null){
                $task["linkFiles"] = trim(str_replace("&amp;", "&", $this->url.$linkFiles[0]->href));//trim($this->url.$linkFiles[0]->href);
            }

            unset($details);
            unset($response);
        }else{
            throw new ArgosException("Fail on request to ".$url);
        }
        return $task;
    }

    /**
     * Searches task properties in Details Task
     *
     * @param string $propertyName Unique value of property
     * @param string $response Details HTML
     * @return string|null property if found or null else
     *
     */
    private function searchProperty($propertyName,&$response){
        $property = null;
        $start = strpos($response, $propertyName);
        if($start !== false){
            $length = strlen($response);
            $string = substr($response, $start, $length - $start);
            $start = strpos($string, "<div");
            $string = substr($string,$start,strlen($string) - $start);
            $end = strpos($string, "</span");
            $string = substr($string,0,$end);
            $string = strip_tags($string);
            $property = trim($string);
        }
        return $property;
    }

    /**
     * Download File
     *
     * @param string $url file link
     * @param string $filePath Path where will be downloaded
     * @param string $fileName File name with extesion
     * @param string $targetLanguage code of target language
     *
     * @return bool
     * @throws \Exception
     */
    public function downloadFile($url, $filePath,$fileName,$targetLanguage){
        $default_headers = array(
            'Content-Type: application/x-www-form-urlencoded'
        );
        $curlCustomOptions = array(
            array(CURLOPT_COOKIEJAR, $this->cookieFile),
            array(CURLOPT_COOKIEFILE, $this->cookieFile)
        );

        $dm = new downloadManager();

        $rsp = $dm->setFileUrl($url)
            ->setAsync(false)
            ->setFileName($fileName)
            ->setDownloadPath($filePath)
            ->setSyncCurlOptions($curlCustomOptions)
            ->addHeader($default_headers)
            ->run();

        if($rsp){
            $this->deleteFilesNotRequired(AI::getInstance()->getSetup()->repository.$filePath.$fileName,$targetLanguage);
        }

        return $rsp;
    }

    /**
     * Deleted all files that is not contains the code target language of task. If any file contains the code target
     * language none file will be deleted.
     *
     * @param string $path path of zip file
     * @param string $targetLanguage code target language
     */
    public function deleteFilesNotRequired($path,$targetLanguage){
        $files = ZipFile::listFiles($path);
        $filesToDelete = array();
        if(!empty($files)){
            foreach ($files as $file){
                if(strpos(strtoupper($file),strtoupper($targetLanguage)) === false && preg_match('#\.(sdlppx)$#i', $file)){
                    /*$language = explode("-",$targetLanguage);
                    if(count($language) === 2){
                        foreach ($files as $file){
                            if(strpos(strtoupper($file),strtoupper($language[0])) === false && preg_match('#\.(sdlppx)$#i', $file)){
                                $filesToDelete[] = $file;
                            }
                        }
                    }*/
                    $languages = $this->getLanguagesOnFiles($targetLanguage);
                    $fileToDelete = !empty($languages);
                    foreach ($languages as $language){
                        if(strpos(strtoupper($file),strtoupper($language)) !== false && preg_match('#\.(sdlppx)$#i', $file)){
                            $fileToDelete = false;
                            break;
                        }
                    }
                    if($fileToDelete){
                        $filesToDelete[] = $file;
                    }
                }
            }
            /*if(count($filesToDelete)===count($files)){
                $filesToDelete = array();
                $language = explode("-",$targetLanguage);
                if(count($language) === 2){
                    foreach ($files as $file){
                        if(strpos(strtoupper($file),strtoupper($language[0])) === false && preg_match('#\.(sdlppx)$#i', $file)){
                            $filesToDelete[] = $file;
                        }
                    }
                }
            }*/
            if(count($filesToDelete)===count($files)){
                $filesToDelete = array();
            }
        }
        if(!empty($filesToDelete)){
            ZipFile::deleteFilesOnZip($path,$filesToDelete);
        }
    }

    public function getLanguagesOnFiles($target){
        $languages = array();
        switch ($target){
            case 'es-UN':
                $languages = array("es-x-int","es-MX");
                break;
            case 'es-ES':
                break;
            case 'es-LA':
                $languages = array("es-");
                break;
            default:
                break;
        }

        return $languages;
    }

    /**
     * Does POST request to url https://supplier.argosmultilingual.com/Assignment/AssignmentDetail/idTask for accept
     * task.
     *
     * @param string $url
     * @return bool
     */
    public function acceptTask($url){
        $taskAccepted = false;
        $body = $this->getParamsForAcceptTask($url);

        $request = curl_init();
        curl_setopt($request, CURLOPT_URL, $url);
        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt($request, CURLOPT_HEADER, true);
        $headers = array(
            'Content-Type: application/x-www-form-urlencoded'
        );

        curl_setopt($request, CURLOPT_POSTFIELDS, http_build_query($body));
        curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_COOKIEJAR, $this->cookieFile);
        curl_setopt($request, CURLOPT_COOKIEFILE, $this->cookieFile);

        try{
            curl_exec($request);
            $httpCode = curl_getinfo($request, CURLINFO_HTTP_CODE);
            curl_close($request);
            $taskAccepted = $httpCode === 200;
        }catch (\Exception $ex){

        }

        return $taskAccepted;
    }

    /**
     * Collect all value´s inputs in form#form0 in page task details
     *
     * @param string $url
     * @return array
     */
    public function getParamsForAcceptTask($url){
        $request = curl_init();
        curl_setopt($request, CURLOPT_URL, $url);
        curl_setopt($request, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($request, CURLOPT_HEADER, true);

        $headers = array(
            'Content-Type: application/x-www-form-urlencoded'
        );
        curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_COOKIEJAR, $this->cookieFile);
        curl_setopt($request, CURLOPT_COOKIEFILE, $this->cookieFile);

        $params = array();
        try{
            $response = curl_exec($request);
            $httpCode = curl_getinfo($request, CURLINFO_HTTP_CODE);
            curl_close($request);

            if($httpCode === 200){
                $page = \str_get_html($response);
                $formDOM = $page->find('form[id="form0"]');
                if($formDOM != null){
                    $form = \str_get_html($formDOM[0]);
                    if($form != null){
                        $inputs = $form->find('input[type="hidden"]');
                        foreach ($inputs as $input){
                            $params[$input->name] = $input->value;
                        }
                    }
                }
            }
        }catch (\Exception $ex){

        }

        return $params;
    }
}

class ArgosException extends \Exception{

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}
