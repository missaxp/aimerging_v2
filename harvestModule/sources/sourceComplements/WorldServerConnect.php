<?php

/**
* @author mhernandez
* @version 1.0
* created on 5 jul. 2018
*/

namespace harvestModule\sources\sourceComplements;


use common\exceptions\AIException;
use common\exceptions\sources\apple_liox\WorldServerException;
use common\exceptions\CurlErrorException;
use Exception;
use Functions;
use model\APIUser;
use core\AI;
use model\Task;
use model\DataSource;

include_once BaseDir . '/Functions.php';
include_once BaseDir . '/simple_html_dom.php';
include_once BaseDir . '/AI.php';
include_once BaseDir . '/model/APIUser.php';
include_once BaseDir . '/harvestModule/sources/sourceComplements/WorldServerProject.php';
include_once BaseDir . '/common/exceptions/CurlErrorException.php';
include_once BaseDir . '/common/exceptions/AIException.php';
include_once BaseDir . '/Functions.php';



class WorldServerConnect {
    //This constants must be override on every subclass
    const LOGIN_URL = "https://idmsa.apple.com/authenticate";
    const BASE_URL = "https://ws01.apple.com/ws-legacy";
    const APP_ID_KEY = "f63dc42a63424f0876a5d56bcc1f3e8952e0b9c3ecef205d4f46bd99dc33c615";
    const LOGIN_REFERENCE = "https://idmsa.apple.com";

    protected $cookieFile;


    protected $user;// = "rmteam@idisc.com": "Apple@2017";

    /**
	 * This token provides access to read some data (not all).
	 * @var String
	 */
    protected $access_token;

    /**
	 * Contains all the cookies saved across all the requests
	 */
	//private $cookies;

    protected $all_projects = null;

	
	public function __construct(APIUser $apiUser){
		$this->user = $apiUser;
		$this->cookieFile = tempnam(sys_get_temp_dir(), 'AI');
	}

    function __destruct() {
        if (file_exists($this->cookieFile)) {
            unlink($this->cookieFile);
        }
    }

    /**
     * @throws AIException
     */
    public function startConnection(){
        $login_response = $this->login();
        $this->findToken($login_response);
    }


    /**
     * Performs a login request with the default credentials. This must be called in constructor in order to save the cookies.
     * This function must be override on every Source. This is an example from AppleLiox
     * @throws AIException
     */
	public function login(){
		$user = $this->user->getUserName();
		$password = $this->user->getPassWord();

		$curl = curl_init();
		
		curl_setopt_array($curl, array(
            CURLOPT_URL => self::LOGIN_URL,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_COOKIEJAR => $this->cookieFile,
            CURLOPT_COOKIEFILE => $this->cookieFile,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "Env=PROD&accNameLocked=false&accountPassword=$password&appIdKey=".self::APP_ID_KEY."&appleId=$user&clientInfo=%7B%22U%22%3A%22Mozilla%2F5.0%20(Windows%20NT%2010.0%3B%20Win64%3B%20x64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F72.0.3626.109%20Safari%2F537.36%22%2C%22L%22%3A%22es-ES%22%2C%22Z%22%3A%22GMT-06%3A00%22%2C%22V%22%3A%221.1%22%2C%22F%22%3A%22c0a44j1e3NlY5BSo9z4ofjb75PaK4Vpjt.gEngMQEjZr_WhXTA2s.XTVV26y8GGEDd5ihORoVyFGh8cmvSuCKzIlnY6xljQlpRD_KQnH0FAD6hvLG9mhORoVidPZW2AUMnGWVQdgMVQdg1kzoMpwoNJ9z4oYYLzZ1kzDlSgyyITL5q8sgEV18u1.BUs_43wuZPup_nH2t05oaYAhrcpMxE6DBUr5xj6KkuKKjC74.gsTrhO3f9p_nH1z5R2leyjaY2hDpBtOtJJIqSI6KUMnGWpwoNSUC56MnGWVQdg3ZLQ0I59L6ed7Tk.Mk.j9R.tHore9zH_y3CmVWNvAGYi5uFB8WNJNtGmX6QynxHMtVrJ5tFFgSFBBFY5BNlrAp5BNlan0Os5Apw.6PR%22%7D&fdcBrowserData=%7B%22U%22%3A%22Mozilla%2F5.0%20(Windows%20NT%2010.0%3B%20Win64%3B%20x64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F72.0.3626.109%20Safari%2F537.36%22%2C%22L%22%3A%22es-ES%22%2C%22Z%22%3A%22GMT-06%3A00%22%2C%22V%22%3A%221.1%22%2C%22F%22%3A%22c0a44j1e3NlY5BSo9z4ofjb75PaK4Vpjt.gEngMQEjZr_WhXTA2s.XTVV26y8GGEDd5ihORoVyFGh8cmvSuCKzIlnY6xljQlpRD_KQnH0FAD6hvLG9mhORoVidPZW2AUMnGWVQdgMVQdg1kzoMpwoNJ9z4oYYLzZ1kzDlSgyyITL5q8sgEV18u1.BUs_43wuZPup_nH2t05oaYAhrcpMxE6DBUr5xj6KkuKKjC74.gsTrhO3f9p_nH1z5R2leyjaY2hDpBtOtJJIqSI6KUMnGWpwoNSUC56MnGWVQdg3ZLQ0I59L6ed7Tk1rk.j9R.tHore9zH_y3CmVWNvAGYjpiZ21NL45DJRcWqzgzJ_yNCp0iJ3A6J2237lY5BSmxGY5BOgkLT0XxU..AtD%22%7D&language=ES-ES&openiForgotInNewWindow=false&referer=https%3A%2F%2Fws01.apple.com%2Fws-legacy%2Fnon_ac_login%3F%26urlToken%3D7667455f352cfdaf6f41633af84e2adef5476&requestUri=%2Flogin.html&scnt=8bc25555f7da69347b27d30c0891b201&undefined=",
            CURLOPT_HTTPHEADER => array(
                    "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,* /*;q=0.8",
                    "Content-Type: application/x-www-form-urlencoded",
                    "Origin: " . self::LOGIN_REFERENCE,
                    "Upgrade-Insecure-Requests: 1",
                    "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36",
            ),
		));
		
		$response = curl_exec($curl);
		$err = curl_error($curl);
		
		curl_close($curl);
		if ($err) {
			throw (new CurlErrorException())
                ->construct(__FUNCTION__, __NAMESPACE__, $ARGS=func_get_args());
		} else {
			return $response;
		}
	}

    /**
     * Find the cookie from the login response.
     * @param $response String HTML Code from the login request
     * @throws AIException On token not found
     */
    protected function findToken($response){
		//We search in all the a tags in the HTML. In this way we prevent changes made in the front-end
		$html = str_get_html($response);
		if($html === false){
		    throw (new AIException("Could not parse login HTML"))
                ->construct(__FUNCTION__, __NAMESPACE__, $ARGS = func_get_args(), array("response" => $response));
        }
		$href_link = "";
		foreach($html->find('a') as $input) {
			$link = $input->href;
			if (strpos($link, 'assignments_projects') === 0) {
				$href_link = $link;
			}
		}
		
		if($href_link === ""){
			throw (new AIException("Token not found in login HTML"))
                ->construct(__FUNCTION__, __NAMESPACE__, $ARGS = func_get_args());
		}
		$token = explode("=", $href_link);
		$this->access_token = $token[count($token) -1];
	}

    /**
     * Makes a POST request to the server in order to download the JSON that contains the projects data
     * @throws CurlErrorException
     */
	public function getAllProject(){
	    if($this->all_projects != null){
	        return $this->all_projects;
        }
		$curl = curl_init();

		$default_headers = array(
            "Accept:application/json, text/javascript, */*; q=0.01",
            "X-Requested-With: XMLHttpRequest",
            "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36",
		);

		$url = self::BASE_URL . "/ws_ext?servlet=ListingServlet&type=projectgroup&start=0&count=100&token=$this->access_token";

		curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_COOKIEJAR => $this->cookieFile,
            CURLOPT_COOKIEFILE => $this->cookieFile,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "folderid=allFilter",
            CURLOPT_HTTPHEADER => $default_headers,
		));
		
		$response = curl_exec($curl);
		$err = curl_error($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		
		curl_close($curl);
		
		if($err) {
			throw (new CurlErrorException($err))->construct(__FUNCTION__, __NAMESPACE__, $args = func_get_args(), array("curl_response" => $response));
		} else {
			$json = json_decode($response);
			$this->all_projects = $json;
			return $json;
		}
		
	}

	/**
	 * Gets all the projects unclaimed by the user from world server
	 */
	public function getProjectsUnclaimed(){
        try {
            $assigned_projects = $this->getAllProject();
        } catch (CurlErrorException $e) {
            Functions::logException($e->getMessage(), Functions::ERROR, __CLASS__, $e);
            return array();
        }
        return array_filter($assigned_projects, function($project){
			return $project->detail[4]->value == 0;
		});
	}

    /**
     * @param array $id_projects Project IDs that need to be downloaded
     * @return array
     * @throws WorldServerException When it cannot find the project id in world server
     * @throws AIException When the $id_projects is not an array
     */
    public function downloadProjects($id_projects){
		if(!is_array($id_projects)){
			throw new AIException("ID list must be an array");
		}
		$projects_info = array();
		$i = 0;
		foreach ($id_projects as $id){
            if(!$this->idProjectIsAvailable($id)){
                $ex = (new AIException("The project $id was not found in WS"))->construct(__METHOD__, __NAMESPACE__, $args = func_get_args(), array(
                    "id_ws" => $id
                ));
                Functions::logException($ex->getMessage(), Functions::WARNING, __CLASS__, $ex);
                continue;
            }
            //Si no se encuentra el proyecto se borra
            $apple_project = new WorldServerProject($id, $this->access_token, $this->cookieFile, $this->user->getIdApiUser());
            $idWS = $id;
            $basic_info = $apple_project->getProjectBasicInfo();
            $extended_info = json_decode($apple_project->getProjectExtendedInfo(), true);


            $projects_info[$i]["idWS"] = $idWS;
            $projects_info[$i]["basic_info"]  = $basic_info;
            $projects_info[$i]["extended_info"]  =$extended_info;
            $projects_info[$i]["files"] = $apple_project->downloadFiles();
            $projects_info[$i]["wwc"]  = $apple_project->getWordCount();
            $i++;
        }
		
		return $projects_info;
	}

    public function getProjectInfo($id_projects){
        if(!is_array($id_projects)){
            throw new AIException("ID list must be an array");
        }
        $projects_info = array();
        $i = 0;
        foreach ($id_projects as $id){
            if(!$this->idProjectIsAvailable($id)){
                $ex = (new AIException("The project $id was not found in WS"))->construct(__METHOD__, __NAMESPACE__, $args = func_get_args(), array(
                    "id_ws" => $id
                ));
                Functions::logException($ex->getMessage(), Functions::WARNING, __CLASS__, $ex);
                continue;
            }
            //Si no se encuentra el proyecto se borra
            $apple_project = new WorldServerProject($id, $this->access_token, $this->cookieFile, $this->user->getIdApiUser());
            $idWS = $id;
            $basic_info = $apple_project->getProjectBasicInfo();
            $extended_info = json_decode($apple_project->getProjectExtendedInfo(), true);


            $projects_info[$i]["idWS"] = $idWS;
            $projects_info[$i]["basic_info"]  = $basic_info;
            $projects_info[$i]["extended_info"]  =$extended_info;
            $projects_info[$i]["wwc"]  = $apple_project->getWordCount();
            $i++;
        }

        return $projects_info;
    }

    protected function idProjectIsAvailable($project_id){
        try {
            $projects_list = $this->getAllProject();
        } catch (CurlErrorException $e) {
            $projects_list = array();
        }
        foreach ($projects_list as $project){
            //echo $project->projectgroupid . ", ";
            if($project->projectgroupid == $project_id)
                return true;
        }
        return false;
    }
}



