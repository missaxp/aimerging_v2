<?php

namespace harvestModule\sources\sourceComplements;

use common\downloadManager;
use common\exceptions\AIException;
use Exception;
use Functions;
use model\Analysis;
use model\File;
use PharData;
use RecursiveIteratorIterator;
use Throwable;

include_once BaseDir . '/simple_html_dom.php';

class GlobalSightService {
	// private const SERVER_URL = "/globalsight/services/AmbassadorWebService?wsdl";
	private $globalSightPortal;
	private $userName;
	private $password;
	private $accessToken;
	private $taskList = null;
	private $tokenLogin = null;
	private $cookies = null;
	private $cookieFile;
	private $loggedIn = false;
	public const TASK_STATUS_AVAILABLE = 3;
	public const TASK_STATUS_IN_PROGRESS = 8;
	private const REQUEST_ERROR = "Unexpected error while sendind request";
	private const WSDL_URL = "/globalsight/services/AmbassadorWebService?wsdl";
	private const LOGIN_PAGE_URL = "/globalsight/ControlServlet?activityName=login";
	private const LOGIN_SERVICE_URL = "/globalsight/ControlServlet?linkName=dummyLink&pageName=LOG1";
	private const TASK_LIST_SERVICE = "/globalsight/TaskListServlet?action=getTaskList&state=#TASKSTATE#&pageNumber=1&rowsPerPage=50&sortColumn=jobId&sortType=desc&advancedSearch=true&acceptanceStartFilter=&acceptanceStartOptionsFilter=&acceptanceEndFilter=&acceptanceEndOptionsFilter=&completionStartFilter=&completionStartOptionsFilter=&completionEndFilter=&completionEndOptionsFilter=&priorityFilter=&jobIdOption=EQ&jobIdFilter=&jobNameFilter=&activityNameFilter=&assigneesNameFilter=&sourceLocaleFilter=0&targetLocaleFilter=0&companyFilter=0&getFilterFromRequest=true&random=0.027207656472225006";
	private const SELF_URL = "/globalsight/ControlServlet?linkName=self&pageName=TK1A";
	private const WORDCOUNT_LIST_URL = "/globalsight/ControlServlet?linkName=wordcountlist&pageName=TK1A";
	private const DOWNLOAD_SOURCE_FILES = "/globalsight/ControlServlet?linkName=detail&pageName=TK2&taskId=#TASKID#&taskAction=downloadSourcePages";
	private const DOWNLOAD_OFFLINE_FILES = "/globalsight/ControlServlet?linkName=self&pageName=TK1A&taskAction=downloadALLOfflineFiles&taskParam=";
	private const DOWNLOAD_COMBINED_TASK_FILES = "/globalsight/ControlServlet?linkName=self&pageName=TK1A&taskAction=downloadALLOfflineFilesCombined&taskParam=";
	private const DOWNLOAD_COMBINED = "/globalsight/ControlServlet?linkName=self&pageName=TK1A&taskAction=DownloadCombined&taskParam=";
	private const DOWNLOAD_REFERENCE_FROM_COMMENTS = "/globalsight/ControlServlet?linkName=downloadcomment&pageName=TK5&action=downloadFiles&jobId=#JOBID#&taskId=#TASKID#";
	private  const DOWNLOAD_FROM_COMMENTS_PAGE = "/globalsight/ControlServlet?linkName=comment&pageName=TK2&taskAction=getTask&state=8&taskId=";
    private $xlfFile = "";
    private $rtfFile = "";
    private const ACCEPT_TASK = "/globalsight/ControlServlet?linkName=accept&pageName=TK2&taskAction=acceptTask&taskId=#TASKID#";
    private const WORKOFFLINE_PAGE = "/globalsight/ControlServlet?linkName=download&pageName=TK2&taskId=#TASKID#&state=8";
    private const START_DOWNLOAD_RTF = "/globalsight/ControlServlet?linkName=startdownload&pageName=TK6&downloadAction=startDownload&state=8&taskId=#TASKID#&format=rtfTradosOptimized&placeholder=compact&resInsSelector=resInsTmx14b&editExact=yes&TMEditType=3&termSelector=tbx";
    private const DOWNLOAD_RTF = "/globalsight/ControlServlet?linkName=senddownloadfile&pageName=TK7&downloadAction=downloadDone&taskId=#TASKID#&state=8";
	/**
	 *
	 * @param string $globalSightPortal
	 * @param string $userName
	 * @param string $password
	 */
	public function __construct(string $globalSightPortal, string $userName, string $password){

		$this->globalSightPortal = rtrim($globalSightPortal, "/");
		$this->userName = $userName;
		$this->password = $password;
        $this->cookieFile = tempnam(sys_get_temp_dir(), 'GSC');
	}

	/**
	 * Will try to log in to Global Sight portal with the credentials provided in costructor
	 *
	 * @return boolean
	 * @return
	 */
	public function login(){

		$handler = curl_init();
		curl_setopt($handler, CURLOPT_URL, $this->globalSightPortal . GlobalSightService::LOGIN_PAGE_URL);
		curl_setopt($handler, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($handler, CURLOPT_COOKIEJAR, $this->cookieFile);
        curl_setopt($handler, CURLOPT_COOKIEFILE, $this->cookieFile);
		$success = null;
		try {
			$response = curl_exec($handler);
			// extract header
			$headerSize = curl_getinfo($handler, CURLINFO_HEADER_SIZE);
			$header = substr($response, 0, $headerSize);
			// extract body
			$body = substr($response, $headerSize);
			curl_close($handler);
			// get token login from html body response
			$htmlBody = \str_get_html($body);
			$input = $htmlBody->find('input');
			$this->tokenLogin = $input[9]->value;

			// Now that we have the token.login we can try to actually login to global sight
			$handler = curl_init();
			curl_setopt($handler, CURLOPT_URL, $this->globalSightPortal . GlobalSightService::LOGIN_SERVICE_URL);
			curl_setopt($handler, CURLOPT_HEADER, true);
            curl_setopt($handler, CURLOPT_COOKIEJAR, $this->cookieFile);
            curl_setopt($handler, CURLOPT_COOKIEFILE, $this->cookieFile);
			$headers = array(
					'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0',
					'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
					'Accept-Encoding: gzip, deflate, br',
					'Content-Type: application/x-www-form-urlencoded'
			);
			curl_setopt($handler, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($handler, CURLOPT_POST, true);
			$body = array(
					'nameField' => $this->userName,
					'passwordField' => $this->password,
					'token.login' => $this->tokenLogin
			);
			curl_setopt($handler, CURLOPT_POSTFIELDS, http_build_query($body));
			$response = curl_exec($handler);
			// get cookies
			preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $response, $matches);
			$cookies = array();
			foreach($matches[1] as $item) {
				parse_str($item, $cookie);
				$cookies = array_merge($cookies, $cookie);
			}
			$httpCode = curl_getinfo($handler, CURLINFO_HTTP_CODE);
			if ($httpCode == 200 && (count($cookies) > 0)) {
				$success = true;
				$this->loggedIn = true;
				$this->cookies = $cookies;
			} else {
				$success = false;
			}
		} catch(Exception $ex) {
			$success = false;
			Functions::addLog(GlobalSightService::REQUEST_ERROR, Functions::WARNING, json_encode($ex->getTrace()));
		}
		curl_close($handler);
		return $success;
	}

	/**
	 * Gets a list of tasks in the given state and returns an array, if there is an error the array will be null
	 *
	 * @param string $taskState
	 * @return array
	 * @return
	 */
	public function getTaskList(string $taskState){

		try {
			if ($this->loggedIn) {
				$handler = curl_init();
				$parsedTaskService = str_replace("#TASKSTATE#", $taskState, $this->globalSightPortal . GlobalSightService::TASK_LIST_SERVICE);
				curl_setopt($handler, CURLOPT_URL, $parsedTaskService);
                curl_setopt($handler, CURLOPT_COOKIEJAR, $this->cookieFile);
                curl_setopt($handler, CURLOPT_COOKIEFILE, $this->cookieFile);
				curl_setopt($handler, CURLOPT_HEADER, true);
				$headers = array(
						'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0',
						'Accept: application/json, text/javascript, */*; q=0.01',
						'Accept-Encoding: gzip, deflate, br'
				);
				curl_setopt($handler, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);

				$response = curl_exec($handler);
				$headerSize = curl_getinfo($handler, CURLINFO_HEADER_SIZE);
				$header = substr($response, 0, $headerSize);
				// extract body
				$body = substr($response, $headerSize);
				$responseJson = json_decode($body, true);
				if (isset($responseJson['tasks'])) {
					$this->taskList = $responseJson['tasks'];
				}
			}
		} catch(Throwable $ex) {
		    $aiex = AIException::createInstanceFromThrowable($ex)->construct(__METHOD__, __NAMESPACE__, $f = func_get_args());
			Functions::logException(GlobalSightService::REQUEST_ERROR, Functions::WARNING, __CLASS__, $aiex);
		}
		curl_close($handler);
		return $this->taskList;
	}

	/**
	 * gets the wordcount for a task given its id and the target language in the form xx_XX
	 *
	 * @param int $taskId
	 * @param string $targetLanguage
	 * @return array asociative array with the GS fuzzies as keys
	 * @return
	 */
	public function getTaskWordCount(int $taskId, string $targetLanguage){

		if ($this->loggedIn) {
            $wordCount = array(
                "100" => 0,
                "95" => 0,
                "85" => 0,
                "75" => 0,
                "noMatch" => 0,
                "repetitions" => 0,
                "inContextMatch" => 0
            );
			try {
				$handler = curl_init();
				curl_setopt($handler, CURLOPT_URL, $this->globalSightPortal . GlobalSightService::WORDCOUNT_LIST_URL . "&action=one&taskid=" . $taskId);
                curl_setopt($handler, CURLOPT_COOKIEJAR, $this->cookieFile);
                curl_setopt($handler, CURLOPT_COOKIEFILE, $this->cookieFile);
				curl_setopt($handler, CURLOPT_HEADER, true);
				$headers = array(
						'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0',
						'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
						'Accept-Encoding: gzip, deflate, br',
						'Content-Type: application/x-www-form-urlencoded',
				);
				curl_setopt($handler, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($handler, CURLOPT_POST, true);
				$body = array(
						'activityNameFilter' => '',
						'assigneesNameFilter' => '',
						'jobIdFilter' => '',
						'jobNameFilter' => '',
						'languageSet' => $targetLanguage
				);
				curl_setopt($handler, CURLOPT_POSTFIELDS, http_build_query($body));
				$response = curl_exec($handler);

				$headerSize = curl_getinfo($handler, CURLINFO_HEADER_SIZE);
				// extract body
				$body = substr($response, $headerSize);
				curl_close($handler);
				$htmlBody = \str_get_html($body);
				if($htmlBody === false){
                    $e = (new AIException("Cannot parse data as HTML content"))->construct(__METHOD__, __NAMESPACE__, $args = func_get_args());
                    Functions::logException($e->getMessage(), Functions::ERROR, __CLASS__, $e);
                    return $wordCount;
                }
				$tables = $htmlBody->find('tr[class=tableRowOddTM]');
				$wordCountTable = \str_get_html($tables[1]);
                if($wordCountTable === false){
                    $e = (new AIException("Cannot parse wordCountTable as HTML content"))->construct(__METHOD__, __NAMESPACE__, $args = func_get_args());
                    Functions::logException($e->getMessage(), Functions::ERROR, __CLASS__, $e);
                    return $wordCount;
                }
				$plainWC = array();
				foreach($wordCountTable->find('td') as $td) {
					$plainWC[] = trim($td->plaintext);
				}
				$wordCount = array();
				$wordCount['100'] = (int)$plainWC[4];
				$wordCount['95'] = (int)$plainWC[5];
				$wordCount['85'] = (int)$plainWC[6];
				$wordCount['75'] = (int)$plainWC[7];
				$wordCount['noMatch'] = (int)$plainWC[8];
				$wordCount['repetitions'] = (int)$plainWC[9];
				$wordCount['inContextMatch'] = (int)$plainWC[10];
				//check if is machine translation
				if(count($plainWC) === 14){
					$wordCount['MT'] = (int)$plainWC[11];
				}
			} catch(Throwable $th){
                $ex = AIException::createInstanceFromThrowable($th)->construct(__METHOD__, __NAMESPACE__, $args = func_get_args());
                Functions::logException("Cannot get wordcount. Check details in stack trace.", Functions::WARNING, __CLASS__, $ex);
            }
			return $wordCount;
		}
	}

	/**
	 * Gets a single task given its jobId and state.
	 * Will return and array with all the properties of the task, may be null the task is not found
	 *
	 * @param int $taskId
	 * @param string $taskState
	 * @return
	 */
	public function getSingleTask(int $taskId, string $taskState){

		$resultTask = null;
		if ($this->taskList == null) {
            $this->taskList = $this->getTaskList($taskState);
//            $inProgress = $this->getTaskList(GlobalSightService::TASK_STATUS_IN_PROGRESS);
//            $this->taskList = array_merge($available, $inProgress);
		}
		foreach($this->taskList as $task) {
			if ($task['jobId'] === $taskId) {
				$resultTask = $task;
				break;
			}
		}
		return $resultTask;
	}

    /**
     * Gets the source files for a task given its id.
     * The files will be downloaded in the filePath provided.
     * The dowloaded file will be in zip format
     * Return true if the files were downloaded succesfully
     *
     * @param int $taskId
     * @param string $filePath
     * @param string $fileName
     * @return array|null
     */
	public function getTaskSourceFiles(int $taskId, string $filePath, string $fileName){

		try {
			$success = false;
			$file = null;
			$newFile = fopen($filePath.$fileName, "w+");
			if ($newFile != false) {
				$handler = curl_init();
				$parsedUrl = str_replace("#TASKID#", $taskId, $this->globalSightPortal . GlobalSightService::DOWNLOAD_SOURCE_FILES);
				curl_setopt($handler, CURLOPT_URL, $parsedUrl);
				curl_setopt($handler, CURLOPT_FILE, $newFile);
				curl_setopt($handler, CURLOPT_CUSTOMREQUEST, "GET");
                curl_setopt($handler, CURLOPT_COOKIEJAR, $this->cookieFile);
                curl_setopt($handler, CURLOPT_COOKIEFILE, $this->cookieFile);
				$headers = array(
						'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0',
						'Accept: application/json, text/javascript, */*; q=0.01',
						'Accept-Encoding: gzip, deflate, br',
				);
				curl_setopt($handler, CURLOPT_HTTPHEADER, $headers);
				$response = curl_exec($handler);
				$httpCode = curl_getinfo($handler, CURLINFO_HTTP_CODE);
				
				$zip = new \ZipArchive();
				$result = $zip->open($filePath.$fileName);
				
				if ($result === true) {
					$success = true;
					$file = array();
					$file['fileName'] = $fileName;
					$file['extension'] = "zip";
					$file['mime'] = "application/zip";
					$file['size'] = filesize($filePath.$fileName);
					$file['path'] = $filePath.$fileName;
					$file['sourcePath'] = $parsedUrl;
					//buscar XLF
                    $ar = new PharData($filePath.$fileName);
                    foreach (new RecursiveIteratorIterator($ar) as $f) {
                        if(Functions::endsWith($f, "xlf")){
                            $name = explode("/", $f);
                            $name = end($name);
                            $this->xlfFile = $name;
                            break;
                        }
                    }
				}else{
					$file = null;
					@unlink($filePath.$fileName);
				}

				curl_close($handler);
			}
		} catch(Exception $ex) {
			Functions::addLog(GlobalSightService::REQUEST_ERROR, Functions::WARNING, json_encode($ex->getTrace()));
		}
		return $file;
	}

	public function getXlfFile(){
	    return $this->xlfFile;
    }

    public function getRTFFile(){
        return $this->rtfFile;
    }

    /**
     * Gets the post parameters to download reference files from comments section
     * Return a string with the name of the files to download
     * Return an empty string if the job have no files in the comments section
     *
     * @param int $taskId
     * @return
     */
	public function getPostFields(string $taskID){
        $options = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>'Cookie: autoLogin=' . $this->cookies['autoLogin'] . '; JSESSIONID=' . $this->cookies['JSESSIONID'] . ';',
            )
        );
        $context = stream_context_create($options);
        $url = $this->globalSightPortal.GlobalSightService::DOWNLOAD_FROM_COMMENTS_PAGE.$taskID;
        $file_globalsight = file_get_contents($url, false, $context);
        if($file_globalsight != false){
            $html = str_get_html($file_globalsight);
            $inputs = $html->find("input[name=checkboxBtn]");
            $files_to_download = array();
            foreach($inputs as $input){
                array_push($files_to_download,$input->value);
            }
            $opt_postfields = "";
            foreach ($files_to_download as $file_to_download){
                $opt_postfields .= "checkboxBtn=".$file_to_download.'&';
            }
            return $opt_postfields;
        }else{
            return "";
        }

    }

    /**
     * Download the reference files from the comment section
     * succesfully
     *
     * @param string $taskID
     * @param string $jobID
     * @param string $filePath
     * @param string $fileName
     * @return file
     */
	public function getReferenceFilesFromComments(string $taskID,string $jobID, string $filePath, string $absolute_path, string $fileName){
        try {
                $url = $this->globalSightPortal.GlobalSightService::DOWNLOAD_REFERENCE_FROM_COMMENTS;
                $url = str_replace("#TASKID#",$taskID,$url);
                $url = str_replace("#JOBID#",$jobID,$url);
                $default_headers = array(
                    'Cookie: autoLogin=' . $this->cookies['autoLogin'] . '; JSESSIONID=' . $this->cookies['JSESSIONID'] . ';',
                );
                $dm = new downloadManager();
                $optional_fields = $this->getPostFields($taskID);
                if($optional_fields == ""){
                    return null;
                }

                $curlCustomOptions = array(
                    array(CURLOPT_CUSTOMREQUEST, "POST"),
                    array(CURLOPT_POSTFIELDS, $optional_fields),
                );
                $rsp = $dm->setFileUrl($url)
                    ->setAsync(false)
                    ->setFileName($fileName)
                    ->setDownloadPath($filePath)
                    ->setSyncCurlOptions($curlCustomOptions)
                    ->addHeader($default_headers)
                    ->run();
                if($rsp){
                    try{
                        $file = array();
                        $file['fileName'] = $fileName;
                        $file['extension'] = "zip";
                        $file['mime'] = "application/zip";
                        $file['size'] = filesize($absolute_path.$fileName);
                        $file['path'] = $absolute_path.$fileName;
                        $file['sourcePath'] = $this->globalSightPortal . GlobalSightService::DOWNLOAD_REFERENCE_FROM_COMMENTS . $taskID;
                    }catch (Exception $ex){
                        Functions::addLog(GlobalSightService::REQUEST_ERROR, Functions::WARNING, json_encode($ex->getTrace()));
                    }
                }else{
                    $exception = (new AIException("Cannot download file $fileName"))->construct(__METHOD__,__NAMESPACE__,$func = func_get_args());
                    Functions::logException($exception->getMessage(), Functions::ERROR, __CLASS__,$exception);
                }

        } catch(Throwable $ex) {
            $exception = AIException::createInstanceFromThrowable($ex)->construct(__METHOD__,__NAMESPACE__,$func = func_get_args());
            Functions::logException($exception->getMessage(), Functions::ERROR, __CLASS__,$exception);
        }
        return $file;
    }

	/**
	 * Finds and downloads the combined files for the task id provided in zip format.
	 * Will return true if the files were downloaded
	 * succesfully
	 *
	 * @param array $taskIds
	 * @param string $filePath
	 * @param string $targetLanguage
	 *        	In the form xx_XX
	 * @return boolean
	 */
	public function getTasksFilesCombined(array $taskIds, string $filePath, string $fileName, string $targetLanguage){

		try {
			$success = false;
			$file = null;
			$newFile = fopen($filePath.$fileName, "w+");
			if ($newFile != false) {
				$handler = curl_init();
				$taskIdString = "";
				$taskIdPost = "";
				foreach($taskIds as $taskId) {
					$taskIdString .= $taskId . " ";
					$taskIdPost .= "taskId=" . $taskId . "&";
				}
				$url = $this->globalSightPortal.GlobalSightService::DOWNLOAD_COMBINED.urlencode(trim($taskIdString));
				//$url = $this->globalSightPortal . GlobalSightService::DOWNLOAD_COMBINED_TASK_FILES . urlencode(trim($taskIdString));
				curl_setopt($handler, CURLOPT_URL, $url);
                curl_setopt($handler, CURLOPT_COOKIEJAR, $this->cookieFile);
                curl_setopt($handler, CURLOPT_COOKIEFILE, $this->cookieFile);
				//curl_setopt($handler, CURLOPT_FILE, $newFile);
				
				
// 				try{
// 					\Functions::addLog("GSDOWN $taskIdString - $url", \Functions::INFO, "GlobalSightService");
// 				}
// 				catch(\Exception $e){}
				
				$headers = array(
						'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0',
						'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
						'Accept-Encoding: gzip, deflate, br',
						'Content-Type: application/x-www-form-urlencoded',
				);
				curl_setopt($handler, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($handler, CURLOPT_POST, true);
				$body = array(
						'jobIdFilter' => '',
						'jobNameFilter' => '',
						'activityNameFilter' => '',
						'languageSet' => $targetLanguage,
						'assigneesNameFilter' => ''
				);
				curl_setopt($handler, CURLOPT_POSTFIELDS, http_build_query($body) . "&" . $taskIdPost . "languageSet=" . $targetLanguage);
				curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
				$success = curl_exec($handler);
				$httpCode = curl_getinfo($handler, CURLINFO_HTTP_CODE);
				curl_close($handler);
				
				$handler = curl_init();
				$url2 = $this->globalSightPortal . GlobalSightService::DOWNLOAD_COMBINED_TASK_FILES . urlencode(trim($taskIdString));
				curl_setopt($handler, CURLOPT_URL, $url2);
				curl_setopt($handler, CURLOPT_FILE, $newFile);
                curl_setopt($handler, CURLOPT_COOKIEJAR, $this->cookieFile);
                curl_setopt($handler, CURLOPT_COOKIEFILE, $this->cookieFile);
				curl_setopt($handler, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($handler, CURLOPT_POST, true);
				curl_setopt($handler, CURLOPT_POSTFIELDS, http_build_query($body) . "&" . $taskIdPost . "languageSet=" . $targetLanguage);
				curl_exec($handler);
				$httpCode = curl_getinfo($handler, CURLINFO_HTTP_CODE);
				
				$zip = new \ZipArchive();
				$result = $zip->open($filePath.$fileName);
				if($result === true){
					$success = true;
					$file = array();
					$file['fileName'] = $fileName;
					$file['extension'] = "zip";
					$file['mime'] = "application/zip";
					$file['size'] = filesize($filePath.$fileName);
					$file['path'] = $filePath.$fileName;
					$file['sourcePath'] = $url2;
                    $ar = new PharData($filePath.$fileName);
                    foreach (new RecursiveIteratorIterator($ar) as $f) {
                        if(Functions::endsWith($f, "xlf")){
                            $name = explode("/", $f);
                            $name = end($name);
                            $this->xlfFile = $name;
                            break;
                        }
                    }
				}else{
					$file = null;
					@unlink($filePath.$fileName);
				}
				curl_close($handler);
			}
		} catch(Exception $ex) {
			Functions::addLog(GlobalSightService::REQUEST_ERROR, Functions::WARNING, json_encode($ex->getTrace()));
		}
		return $file;
	}

	/**
	 * Gets the complete set of files for offline work in zip format.
	 * Returns true if the files were downloaded succesfully
	 *
	 * @param int $taskId
	 * @param string $filePath
	 * @param string $targetLanguage
	 * @return
	 */
	public function getTaskOfflineFiles(int $taskId, string $filePath, string $fileName, string $targetLanguage){

		try {
			$success = false;
			$file = null;
			$newFile = fopen($filePath.$fileName, "w+");
			if ($newFile != false) {
				$handler = curl_init();
				// $parsedUrl = str_replace("#TASKID#", $taskId, $this->globalSightPortal.GlobalSightService::DOWNLOAD_SOURCE_FILES);
				curl_setopt($handler, CURLOPT_URL, $this->globalSightPortal . GlobalSightService::DOWNLOAD_OFFLINE_FILES . $taskId);
				curl_setopt($handler, CURLOPT_FILE, $newFile);
                curl_setopt($handler, CURLOPT_COOKIEJAR, $this->cookieFile);
                curl_setopt($handler, CURLOPT_COOKIEFILE, $this->cookieFile);
				$headers = array(
						'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0',
						'Accept: application/json, text/javascript, */*; q=0.01',
						'Accept-Encoding: gzip, deflate, br',
				);
				curl_setopt($handler, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($handler, CURLOPT_POST, true);
				$body = array(
						'activityNameFilter' => '',
						'assigneesNameFilter' => '',
						'jobIdFilter' => '',
						'jobNameFilter' => '',
						'languageSet' => $targetLanguage,
						'taskId' => $taskId
				);
				curl_setopt($handler, CURLOPT_POSTFIELDS, http_build_query($body));
				$response = curl_exec($handler);
				$httpCode = curl_getinfo($handler, CURLINFO_HTTP_CODE);
				
				$zip = new \ZipArchive();
				$result = $zip->open($filePath.$fileName);
				
				if ($result === true) {
					$success = true;
					
					$file = array();
					$file['fileName'] = $fileName;
					$file['extension'] = "zip";
					$file['mime'] = "application/zip";
					$file['size'] = filesize($filePath.$fileName);
					$file['path'] = $filePath.$fileName;
					$file['sourcePath'] = $this->globalSightPortal . GlobalSightService::DOWNLOAD_OFFLINE_FILES . $taskId;
					
				}else{
					$file = null;
					@unlink($filePath.$fileName);
				}
				curl_close($handler);
			}
		} catch(Exception $ex) {
			Functions::addLog(GlobalSightService::REQUEST_ERROR, Functions::WARNING, json_encode($ex->getTrace()));
		}
		return $file;
	}

    /**
     * Download files in RTF trados format
     * @param int $taskId
     * @param string $downloadPath
     * @param string $fileName
     * @return array
     */
	public function downloadRTFFiles(int $taskId, string $downloadPath, string $fileName){
        $handler = curl_init();

        //Get task details, we need to get a cookie and a value from a checkbox
        $url = $this->globalSightPortal.self::WORKOFFLINE_PAGE;
        $url = str_replace("#TASKID#", $taskId, $url);
        curl_setopt($handler, CURLOPT_URL, $url);
        curl_setopt($handler, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($handler, CURLOPT_COOKIEJAR, $this->cookieFile);
        curl_setopt($handler, CURLOPT_COOKIEFILE, $this->cookieFile);
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
        $headers = array(
            'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0',
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Encoding: gzip, deflate, br',
        );
        curl_setopt($handler, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($handler);
        curl_close($handler);
        $html = \str_get_html($response);
        //Get the element by its id, then get its value
        $checkBox = $html->find('#page');
        $pageCheckBoxes = $checkBox[0]->value;


        //Prepare the files on GlobalSight
        $url = $this->globalSightPortal.self::START_DOWNLOAD_RTF;
        $url = str_replace("#TASKID#", $taskId, $url);
        $handler = curl_init();
        curl_setopt($handler, CURLOPT_URL, $url);
        curl_setopt($handler, CURLOPT_COOKIEJAR, $this->cookieFile);
        curl_setopt($handler, CURLOPT_COOKIEFILE, $this->cookieFile);
        curl_setopt($handler, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($handler, CURLOPT_POST, true);
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
        $body = array(
            'pageCheckBoxes' => $pageCheckBoxes,
            'formatSelector' => 'rtfTradosOptimized',
            'ptagSelector' => 'compact',
            'resInsertionSelector' => 'resInsTmx14b',
            'changeCreationIdForMT' => 'on',
            'separateTMfile' => 'on',
            'penalizedReferenceTmPre' => 'on',
            'termSelector' => 'tbx',
            'TMEditType'=>'3',
            'populate100'=>true,
            'populatemt'=>true,
            'consolidateFileType'=>'consolidate',
            'wordCountForDownload'=>2000
        );
        curl_setopt($handler, CURLOPT_POSTFIELDS, http_build_query($body));
        $response = curl_exec($handler);
        curl_close($handler);

        //We need to wait while the files are being generated in order to download them successfully
        sleep(7);

        //Download the actual file
        $url = $this->globalSightPortal.self::DOWNLOAD_RTF;
        $url = str_replace("#TASKID#",$taskId,$url);
        $handler = curl_init();
        curl_setopt($handler, CURLOPT_URL, $url);
        curl_setopt($handler, CURLOPT_COOKIEJAR, $this->cookieFile);
        curl_setopt($handler, CURLOPT_COOKIEFILE, $this->cookieFile);
        curl_setopt($handler, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($handler, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
        $newFile = fopen($downloadPath.$fileName, "w+");
        curl_setopt($handler, CURLOPT_FILE, $newFile);
        $response = curl_exec($handler);

        //open the file to check if it is ok
        $zip = new \ZipArchive();
        $result = $zip->open($downloadPath.$fileName);
        $fileSize = filesize($downloadPath.$fileName);
        $file = array();
        if ($result === true && $fileSize > 0) {

            $file['fileName'] = $fileName;
            $file['extension'] = "zip";
            $file['mime'] = "application/zip";
            $file['size'] = filesize($downloadPath.$fileName);
            $file['path'] = $downloadPath.$fileName;
            $file['sourcePath'] = $url;
//            $filesToTrans[] = $file;
            $numFiles = $zip->numFiles;
            for($i = 0; $i < $numFiles; $i ++) {
                $fileName = $zip->getNameIndex($i);
                if (preg_match('#\.(rtf)$#i', $fileName)) {
                    $fileInfo = pathinfo($fileName);
                    $this->rtfFile = $fileInfo['basename'];
                }
            }
            $zip->close();
        }else{
            $file = null;
            @unlink($downloadPath.$fileName);
        }
        return $file;
    }
}

