<?php

/**
 * @author ljimenez
 * @version 1.0
 * created on 2 jul. 2018
 */
namespace harvestModule\sources;

use model\File;
use model\Task;
use harvestModule\sources\connectService\WebServiceMoravia_V2;
use model\Source;
use model\State;
use model\Analysis;
use harvestModule\sources\connectService\Rest;
use model\DataSource;
use Functions;

include_once BaseDir. '/model/Task.php';
include_once BaseDir. '/model/Source.php';
include_once BaseDir. '/model/APIUser.php';
include_once BaseDir. '/model/State.php';
include_once BaseDir. '/model/Analysis.php';
include_once BaseDir. '/model/DataSource.php';
include_once BaseDir. '/model/File.php';
include_once BaseDir. '/harvestModule/sources/connectService/Rest.php';
include_once BaseDir. '/harvestModule/sources/connectService/WebServiceMoravia_V2.php';
include_once BaseDir. '/Functions.php';

class Moravia_V2 extends StructureSource{
	
	const URL = 'https://projects.moravia.com/';
	const HARVESTMETHOD = 'API';
	private $properties;
	
	/**
	 *
	 * @var int
	 */
	private $currentUser;
	
	public function __construct(array $users, Source $source){
		parent::__construct($users, $source);
		$this->properties = array(
		);
	}
	
	public function acceptTask(Task $task) {
		$successfull = false;
		if(count($this->users) == 1){
			$moravia = new WebServiceMoravia_V2($this->users[0]);
			$successfull = $moravia->acceptTask($task->getIdClientetask());
		}
		return $successfull;
	}
	
	public function collectTask(){
		// 		foreach ($this->users as $user){
		$user = $this->users[0];
		\Functions::console("HM::".$this->source->getSourceName()." - Collecting tasks for user: ".$user->getUserName());
		$this->currentUser++;
		$moravia = new WebServiceMoravia_V2();
		$moravia->setUser($user);
		$value = new \DateTime($user->getTimeLife());
		$currentValue = new \DateTime();
		$validation = $value < $currentValue;
		if($validation){
			$moravia->getAuthentication();
		}
		$tasks = $moravia->getTasks();
		$tasks = isset($tasks["value"])?$tasks["value"]:array();
		foreach ($tasks as $task){
			$newTask = new Task();
			$newTask->setAssignedUser($user->getIdApiUser());
			$newTask->construct(
					Functions::currentDate(),
					isset($task["Id"])? $task["Id"]:"",
					isset($task["Name"])? $task["Name"]."_".$task["Id"]."_".$task["TargetLanguageCode"]:"",
					(isset($task["Description"]) && $task["Description"] != "")?$task["Description"]:"",
					Functions::createFormatDate(isset($task["StartDate"])? $task["StartDate"]:""),
					Functions::createFormatDate(isset($task["DueDate"])? $task["DueDate"]:""),
					isset($task["Project"]["Id"])?$task["Project"]["Id"]:""
					);
			
			
			
			$language = parent::identifySourceLanguage((isset($task["SourceLanguageCode"])? $task["SourceLanguageCode"]:""));
			if($language != null){
				$newTask->setSourceLanguage($language);
			}
			
			$target = parent::identifyTargetLanguage(array(isset($task["TargetLanguageCode"])? $task["TargetLanguageCode"]:""));
			$newTask->setTargetsLanguage($target);
			
			$attachments = $moravia->getUserattachmentsById($task["Id"]);
			$attachments = $attachments["value"];
			if (is_array($attachments)) {
				foreach ($attachments as $attachment){
					$extention = explode(".",$attachment["Name"]);
					
					if(count($extention) > 1)
						$extention = $extention[count($extention)-1];
						else
							$extention = " ";
							$file = new File();
							$file->construct(
									isset($attachment["Name"])?$attachment["Name"]:"file.zip",
									$extention,
									isset($attachment["MimeType"])?$attachment["MimeType"]:"",
									"",
									Functions::currentDate(),
									isset($attachment["DownloadUrl"])?WebServiceMoravia_V2::API.$attachment["DownloadUrl"]:"",
									isset($attachment["CheckSum"])?$attachment["CheckSum"]:"",
									Moravia_V2::HARVESTMETHOD,
									isset($attachment["FileType"])?$attachment["FileType"]:"zip",
									false,
									isset($attachment["Size"])?$attachment["Size"]:1
									);
							
							if($file->getFileType()==File::SOURCE){
								$newTask->addFileToTranslate($file);
							}else{
								$newTask->addFileForReference($file);
							}
				}
			}
			
			
			$wordCounts = $moravia->getWordCountAnalyses($task["Id"]);
			$analysis = $this->generateAnalsis($wordCounts["value"]);
			if (!empty($newTask->getTargetsLanguage())){
				$newTask->getTargetsLanguage()[0]->setAnalysis($analysis);
			}
			
			$dataSource = new DataSource();
			$dataSource->construct(
					$this->source->getSourceName(),
					"IN",
					Functions::currentDate(),
					Moravia_V2::HARVESTMETHOD,
					Moravia_V2::URL,
					Moravia_V2::URL.'task/'.$task["Id"].'/detail',
					json_encode($task),
					$this->source->getIdSource(),
					$user->getIdApiUser()
					);
			$newTask->setDataSource($dataSource);
			
			$aditionalProperties = parent::collectProperties($this->properties, $task);
			$newTask->setAditionalProperties($aditionalProperties);
			
			$newTask->setState(new State(State::COLLECTED));
			$newTask->generateUniqueTaskId();
			$newTask->setTimeStamp(\Functions::currentDate());
			$newTask->generateUniqueSourceId();
			
			if($this->source->getDownload()){
				foreach ($newTask->getFilesForReference() as &$file){
					$pathForDownload = parent::generatePathForDownload($task["Id"], $this->source->getSourceName(), $newTask->getUniqueTaskId());
					$file->setPath($pathForDownload.$file->getFileName());
					$successfullDownload = $this->downloadFile($file);
					$file->setSuccessfulDownload($successfullDownload);
				}
				
				foreach ($newTask->getFilesToTranslate() as &$file){
					$pathForDownload = parent::generatePathForDownload($task["Id"], $this->source->getSourceName(), $newTask->getUniqueTaskId());
					$file->setPath($pathForDownload.$file->getFileName());
					$successfullDownload = $this->downloadFile($file);
					$file->setSuccessfulDownload($successfullDownload);
				}
			}
			
			$this->newTasks[] = $newTask;
		}
		// 		}
		// 		die(var_dump($this->getTasks()));
		parent::collectTask();
		return (count($this->newTasks) > 0);
	}
	
	protected function generateAnalsis(&$wordCounts){
		$analysis = null;
		//Si no hay contage des de la tarea, se inicializa a todo vacio. Modificado por phidalgo 20180803.
		$analysis = new Analysis();
		if (isset($wordCounts[0]["WordCounts"])){
			foreach ($wordCounts[0]["WordCounts"] as $value){
				switch ($value["Name"]){
					case "autotrans 100% (Words)":
						$analysis->setPercentage_100($analysis->getPercentage_100() + $value["Value"]);
						break;
					case "autotrans 95% - 99% (Words)":
						$analysis->setPercentage_95($analysis->getPercentage_100() + $value["Value"]);
						break;
					case "autotrans 85% - 94% (Words)":
						$analysis->setPercentage_85($analysis->getPercentage_100() + $value["Value"]);
						break;
					case "autotrans 75% - 84% (Words)":
						$analysis->setPercentage_75($analysis->getPercentage_100() + $value["Value"]);
						break;
					case "autotrans 30% - 74% (Words)":
						$analysis->setPercentage_50($analysis->getPercentage_100() + $value["Value"]);
						break;
					case "forreview (Words)":
						$analysis->setPercentage_100($analysis->getPercentage_100() + $value["Value"]);
						break;
					case "validated (Words)":
						$analysis->setNotMatch($analysis->getNotMatch() + $value["Value"]);//TODO: Pregunta a Pau
						break;
					case "untranslated repeated (Words)":
						$analysis->setRepetition($analysis->getRepetition() + $value["Value"]);
						break;
					case "untranslated (Words)":
						$analysis->setNotMatch($analysis->getNotMatch() + $value["Value"]);
						break;
					case "total (Words)":
						$analysis->setTotalWords($value["Value"]);
						break;
					default:
						break;
				}
			}
		}
		return $analysis;
	}
	
	public function downloadFile(File &$file, Task &$task = null){
		$result = true;
		if(!file_exists($file->getPath())){
			set_time_limit(0);
			$file->setPathForDownload($file->getSourcePath());
			$rest = new Rest($file->getSourcePath());
			$rest->setHeader('Authorization: Bearer '.$this->users[$this->currentUser-1]->getToken());
			$data = $rest->createRequest();
			$pathForDownload = parent::generatePathForDownload($task->getIdClientetask(), $this->source->getSourceName(), $task->getUniqueTaskId());
			$result = $this->saveDownload($file, $pathForDownload, $data);
			
		}
		$file->setPathForDownload($file->getSourcePath());
		return $result;
	}
}
?>