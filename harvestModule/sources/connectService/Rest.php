<?php

namespace harvestModule\sources\connectService;

class Rest{
	
	const _POST = "POST";
	const _GET = "GET";
	
	public static $COOKIE = "COOKIE";
	public static $HEADER = "HEADER";
	
	private $options;
	
	private $method;
	
	private $url;
	
	private $auth;
	
	private $request;
	
	private $headers = array();
	
	public static $authenticationMethod = null;
	
	/**
	 * Deprecated
	 * @var string
	 */
	public static function setAuthenticationMethod($method){
		self::$authenticationMethod = $method;
	}
	
	public function __construct($url,$auth = null,$method = self::_GET){
		if(self::$authenticationMethod==null){
			self::$authenticationMethod = self::$COOKIE;
		}
		$this->options = array("timeout"=>30,"useragent"=>'IDISC_CONNECTOR',"connecttimeout"=>5);
		$this->method = $method;
		$this->url = $url;
		$this->auth = $auth;
		$this->request = curl_init();
		
		curl_setopt($this->request,CURLOPT_URL,$url);
		curl_setopt($this->request,CURLOPT_USERAGENT,"iDISC Connector");
		curl_setopt($this->request,CURLOPT_TIMEOUT,"30");
		curl_setopt($this->request,CURLOPT_RETURNTRANSFER,true);
		
		
		if(self::$authenticationMethod==self::$COOKIE && $this->auth!= null){
			$cookiestr = "";
			foreach($this->auth as $name => $value){
				$cookiestr.= $name."=".$value."; ";
			}
			$this->headers[] = "Cookie: ".$cookiestr;
		}
		else if(self::$authenticationMethod==self::$HEADER && $this->auth!=null){
			$this->headers[] = 'Authorization: Bearer '.$this->auth;
		}
		
		if($this->method == 'POST'){
			$this->headers[] = 'Content-Length: 0';
			curl_setopt($this->request,CURLOPT_POST,true);
		}
	}
	
	public function setHeader($header){
		$this->headers[] = $header;
	}
	
	public function setContenttype($ct){
		$this->headers[] = 'Content-Type: '.$ct;
	}
	
	public function setData($data){
		if($data==null) return;
		if($this->method == 'POST'){
			curl_setopt($this->request,CURLOPT_POSTFIELDS,http_build_query($data));
		}
	}
	
	public function setBody($data){
		if($data==null) return;
		$this->headers[] = 'Content-Length: ' . strlen($data);
		curl_setopt($this->request, CURLOPT_POSTFIELDS, $data);
	}
	
	public function createRequest(){
		try{
			if(count($this->headers)>0){
				curl_setopt($this->request, CURLOPT_HTTPHEADER, $this->headers);
			}
			$output = curl_exec($this->request); //Enviem la consulta.
		}
		catch(\Exception $e){
			\Functions::addLog($e->getMessage(), \Functions::ERROR, json_encode($ex->getTrace()));
			return false;
		}
		return $output;
	}
	
	public function addFile($file){
		if($file==null){
			return;
		}
		if($this->method == self::_POST){
			
		}
	}
	
	public function getCookies(){
		if($this->auth==null){
			$ch = curl_init($this->url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, 1); // get headers too with this line
			$result = curl_exec($ch);
			// get cookie
			// multi-cookie variant contributed by @Combuster in comments
			preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result, $matches);
			$cookies = array();
			foreach($matches[1] as $item) {
				//parse_str($item, $cookie);
				$c = explode("=",$item);
				$cookie = array($c[0] => $c[1]);
				$cookies = array_merge($cookies, $cookie);
			}
			return $cookies;
		}
		else{
			return $this->auth;
		}
	}
	
	public function __destruct(){
		curl_close($this->request);
	}
}
?>