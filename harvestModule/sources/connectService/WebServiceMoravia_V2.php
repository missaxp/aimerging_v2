<?php

namespace harvestModule\sources\connectService;

use model\APIUser;
use dataAccess\dao\SourceDAO;

class WebServiceMoravia_V2 {
	/**
	 * 
	 * @var APIUser
	 */
	private $user;
	private $auth_token;
	
	const TASK_LIST_NUMBER = 100; 
	const API = 'https://projects.moravia.com/Api/';
	
	public function __construct(){
		Rest::setAuthenticationMethod(Rest::$HEADER);
	}
	
	public function setUser($cred){
		$this->user = $cred;
	}
	
	public function getAuthentication(){
		$post = 'grant_type=service&client_id='.$this->user->getUserName().'&client_secret=xiea2QDxt4TO3UYc0WEAaVnnjq6T6B6Tglgo0dSsVwOX56fb2n5Q8y5c411MUt1r&scope=symfonie2-api&service_account=srv_global_idisc@moravia.com';
		$ch = curl_init('https://login.moravia.com/connect/token');
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); // **Inject Token into Header**
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$post);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		$result = curl_exec($ch);
		curl_close($ch);
		$response = json_decode($result,true);
		
		if(!isset($response["error"])){
			$sourceDAO = new SourceDAO();
			$sourceDAO->updateToken($this->user->getIdApiUser(), $response["access_token"], "+50 minute");
			$this->user->setToken($response["access_token"]);
		}
		
		return $response;
	}
	
	public function getTaskAmounts($tid){
		$url = WebServiceMoravia_V2::API."TaskAmounts?\$filter=(TaskId eq ".$tid.")";
		$rest = new Rest($url,$this->user->getToken());
		return json_decode($rest->createRequest(),true);
	}
	
	public function getWordCountAnalyses($taskId){
		$url = WebServiceMoravia_V2::API.'V3/WordCountAnalyses?$filter=TaskId%20eq%20'.$taskId;
		$rest = new Rest($url,$this->user->getToken());
		return json_decode($rest->createRequest(),true);
	}
	
	public function getTasks(){
		$url = WebServiceMoravia_V2::API.'V3/Tasks?$filter=(state%20eq%20Moravia.Symfonie.Data.TaskStates%27Order%27)%20and%20isArchived%20eq%20false&$count=true&subscribed=true&$orderby=OrderDate%20desc&$top='.self::TASK_LIST_NUMBER;
		$rest = new Rest($url,$this->user->getToken());
		return json_decode($rest->createRequest(),true);
	}

	public function getTasksv2($status){
		$url = "https://projects.moravia.com/odata/tasks?\$orderby=id%20desc&\$filter=(isArchived%20eq%20false)%20and%20(status%20eq%20Moravia.Symfonie.Data.TaskStates%27Order%27)&\$format=application/json;odata.metadata=minimal&\$expand=creator,%20projectLink,%20assignees,%20workitemLink,%20customFields&\$select=id,linkName,jobLink,targetLanguageName,workitemLink,status,plannedEnd,assignees,hasChildren,isLocked&\$count=true&subscribed=true&_=1458302996223";
		$rest = new Rest($url,$this->user->getToken());
		return json_decode($rest->createRequest(),true);
	}
	
	public function getTaskById($id){
		$url = WebServiceMoravia_V2::API."Tasks?\$filter=(Id%20eq%20$id)";
		$rest = new Rest($url,$this->user->getToken());
		return json_decode($rest->createRequest(),true);
	}
	
	public function getProjects(){
		$url = WebServiceMoravia_V2::API."/Api/Projects";
		$rest = new Rest($url,$this->user->getToken());
		return json_decode($rest->createRequest(),true);
	}
	
	public function getUserattachmentsById($id){
		$url = WebServiceMoravia_V2::API."/TaskAttachments?\$filter=(TaskId%20eq%20$id)";
		//$url = self::$api."/TaskAttachments?\$filter=(TaskId%20eq%201016203)";
		$rest = new Rest($url,$this->user->getToken());
		return json_decode($rest->createRequest(),true);
	}
	
	public function acceptTask($id){
		$url = WebServiceMoravia_V2::API."Tasks($id)/Default.ExecuteTaskCommand";
		$rb = '{"taskCommand" : "Accept"}';
		$rest = new Rest($url,$this->user->getToken(),Rest::_POST);
		$rest->setContenttype("application/json");
		$rest->setBody($rb);
		$rsp = $rest->createRequest();
		
		if($rsp===false){
			return false;
		}
		else{
			return true;
		}
	}
}
?>