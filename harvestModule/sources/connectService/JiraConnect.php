<?php

/**
* @author Ljimenez
* @version 1.0
* created on 5 jul. 2018
*/
namespace harvestModule\sources\connectService;

use model\APIUser;
use Functions;
use harvestModule\sources\Jira;

include_once BaseDir. '/Functions.php';

class JiraConnect {
	
	/**
	 * 
	 * @var APIUser
	 */
	private $user;
	private $postUrl = '/login.jsp';
	private $destination;
	private $authentificationCookie;
	private $destination_html;
	private $errorMessage;
    /**
     * @var Authentication token
     */
	private $token;
	
	public const _NO_ERROR = 'No error found';
	public const _AUTHENTICATION_ERROR = 'Can not get the Set-Cookie Header';
	public const _CAN_NOT_ACCEPT_TASK = 'POST HTTP STATUS != 200 Response from JIRA-APS Server';
	public const _UNEXPECTED_CURL_ERROR = 'Unexpected Curl error found';
	public const _CAN_NOT_GET_NEGOTIATION_ID = 'Can not get Negotiation ID';
	
	public function __construct(APIUser $user){
		$this->user = $user;
		$this->authentificationCookie = $this->connect();
	}

    /**
     * @return Authentication
     */
    public function getToken(): Authentication
    {
        return $this->token;
    }

    /**
     * @param Authentication $token
     */
    public function setToken(Authentication $token): void
    {
        $this->token = $token;
    }
	
	/**
	 * @return \model\APIUser
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * @return string
	 */
	public function getPostUrl() {
		return $this->postUrl;
	}

	/**
	 * @return string
	 */
	public function getDestination() {
		return $this->destination;
	}

	/**
	 * @return string
	 */
	public function getAuthentificationCookie() {
		return $this->authentificationCookie;
	}

	/**
	 * @return mixed
	 */
	public function getDestination_html() {
		return $this->destination_html;
	}

	/**
	 * @return string
	 */
	public function getErrorMessage() {
		return $this->errorMessage;
	}

	/**
	 * @param \model\APIUser $user
	 */
	public function setUser($user) {
		$this->user = $user;
	}

	/**
	 * @param string $postUrl
	 */
	public function setPostUrl($postUrl) {
		$this->postUrl = $postUrl;
	}

	/**
	 * @param Ambigous <\sources\webServices\string, string, multitype:mixed unknown > $destination
	 */
	public function setDestination($destination) {
		$this->destination = $destination;
	}

	/**
	 * @param Ambigous <boolean, string> $authentificationCookie
	 */
	public function setAuthentificationCookie($authentificationCookie) {
		$this->authentificationCookie = $authentificationCookie;
	}

	/**
	 * @param mixed $destination_html
	 */
	public function setDestination_html($destination_html) {
		$this->destination_html = $destination_html;
	}

	/**
	 * @param string $errorMessage
	 */
	public function setErrorMessage($errorMessage) {
		$this->errorMessage = $errorMessage;
	}

	public function connect(){
		$authentication_cookie = false;
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, Jira::APS_URL.$this->postUrl);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		
		$fields = array(
				'os_username'   => $this->user->getUserName(),
				'os_password'   => $this->user->getPassWord(),
				'user_role'     => "",
				'atl_token'     => "",
				'login'         => "Log In"
		);
		
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
		
		
		// receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		try {
			$server_output = curl_exec ($ch);
			
			curl_close ($ch);
			$retVal = array();
			$match = array();
			$headers = explode("\r\n", preg_replace('/\x0D\x0A[\x09\x20]+/', ' ', $server_output));
			foreach( $headers as $header ) {
				if( preg_match('/([^:]+): (.+)/m', $header, $match) ) {
					$match[0] = explode(":",$match[0])[0];
					if( isset($retVal[$match[0]]) ) {
						$retVal[$match[0]] = array($retVal[$match[0]], $match[2]);
					} else {
						$retVal[$match[0]] = trim($match[2]);
					}
				}
			}
			
			if(isset($retVal["Set-Cookie"]) && isset($retVal["Set-Cookie"][0]) && isset($retVal["Set-Cookie"][1])){
				$jsessionid = str_replace(";path=/", "", $retVal["Set-Cookie"][0]);
				$jsessionid = str_replace(";HttpOnly", "", $jsessionid);
				
				$atlassian = str_replace(";path=/", "", $retVal["Set-Cookie"][1]);
				
				$authentication_cookie = "Cookie: ".$jsessionid."; ".$atlassian;
				$this->token = $authentication_cookie;
				
				if(isset($retVal["Location"]) && $retVal["Location"]!=""){
					$this->destination = $retVal["Location"];
				}
			}
		} catch (\Exception $e) {
			Functions::addLog(JiraConnect::_AUTHENTICATION_ERROR, Functions::WARNING,json_encode($e->getTrace()));
		}
		
		return $authentication_cookie;
	}
	
	public function getDestinationAPI(string $destination = null){
		
		if($this->authentificationCookie===false){
			return false;
		}
		
		if($destination!=null){
			$this->destination = $destination;
		}
		
		if($this->destination===null){
			return false;
		}
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL,Jira::APS_URL.$destination);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->authentificationCookie));

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$server_output = curl_exec ($ch);
		
		$http_status = curl_getinfo ($ch, CURLINFO_HTTP_CODE);
		
		curl_close ($ch);
		$this->destination_html = $server_output;
		
		return true;
	}
	
	public function getNegotiationId(){
		$inittrIDPos = strpos($this->destination_html, "<tr id=\"", 0);
		if($inittrIDPos===false){
			$this->errorMessage = JiraConnect::_CAN_NOT_GET_NEGOTIATION_ID;
			return false;
		}
		
		$inittrIDPos += 8;
		
		$endtrIDPos = strpos($this->destination_html, "\">", $inittrIDPos);
		
		$negotiation_id = substr($this->destination_html, $inittrIDPos, ($endtrIDPos-$inittrIDPos));
		
		return $negotiation_id;
	}
	
	public function accept($destination_id, $negotiation_id){
		if($this->authentificationCookie===false){
			$this->errorMessage = JiraConnect::_AUTHENTICATION_ERROR;
			return false;
		}
		
		$fields = array(
				"issueKey"      => $destination_id,
				"negotiationId" => $negotiation_id,
				"transition"    => "ACCEPT",
		);
		
		$payload = '{"issueKey":"'.$destination_id.'","negotiationId":"'.$negotiation_id.'","transition":"ACCEPT"}';
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL,Jira::APS_URL.'/rest/negotiations/1.0/actions/transition');
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		
		$headers = array(
				'Accept: application/json, text/javascript, */*; q=0.01',
				'Accept-Encoding: gzip, deflate, br',
				'Accept-Language: ca,nl;q=0.9',
				'Connection: keep-alive',
				'Content-Length: '.strlen($payload),
				'Content-Type: application/json',
				$this->authentificationCookie,
				'Host: aps.lionbridge.com',
				'Origin: https://aps.lionbridge.com',
				'Referer: https://aps.lionbridge.com/browse/'.$destination_id,
				'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36',
				'X-Requested-With: XMLHttpRequest'
		);
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$server_output = curl_exec ($ch);
		
		if($server_output===false){
			$this->errorMessage = JiraConnect::_UNEXPECTED_CURL_ERROR;
			return false;
		}
		
		$http_status = curl_getinfo ($ch, CURLINFO_HTTP_CODE);
		
		curl_close($ch);
		
		if($http_status!=200){
			$this->errorMessage = JiraConnect::_CAN_NOT_ACCEPT_TASK;
			return false;
		}
		return true;
	}

    /**
     * @param $taskLink
     * @return array
     */
	public function getTaskData($taskLink){
	    $taskData = array();
        $request = curl_init();
        curl_setopt($request, CURLOPT_URL, $taskLink);
        curl_setopt($request, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($request, CURLOPT_HEADER, true);
        curl_setopt($request, CURLOPT_TIMEOUT, '10');

        $headers = array(
            'Content-Type: application/x-www-form-urlencoded',
            $this->token
        );


        curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);

        try{
            $response = curl_exec($request);
            $httpCode = curl_getinfo($request, CURLINFO_HTTP_CODE);
            curl_close($request);

            if($httpCode == 200){
                $page = \str_get_html($response);
                $section = $page->find('div[id="customfield-panel-3"]',0);
                if($section != null){
                    $list = \str_get_html($section);
                    $lis = $list->find('ul li');
                    if($lis != null){
                        foreach ($lis as $li){
                            $row = \str_get_html($li);
                            $key = $row->find("div strong")[0]->plaintext;
                            $value = $row->find("div div")[0]->plaintext;
                            $key = str_replace(":","",$key);
                            $taskData["analysis"][$key] =  str_replace(",","",trim($value));
                        }
                        unset($lis);
                    }

                    unset($section);
                    unset($page);
                }
            }

            $typeTask = $this->getTaskPropertyByIdHTML('span[id="type-val"]',$response);

            if($typeTask != null){
                $taskData["typeTask"] = $typeTask;
            }

            $packageLinks = $this->getPackgeLinks($response);

            if($packageLinks['links'] != null){
                /*$taskData["PACKAGE_URLS"] = "<br>";
                foreach ($packageLinks['links'] as $link){
                    $taskData["PACKAGE_URLS"] .= $link."<br>";
                }*/
                $taskData["PACKAGE_URLS"] = implode("<br>",$packageLinks['links']);

            }

            if($packageLinks['ids'] != null){
                $taskData["PACKAGES_IDS"] = implode(",",$packageLinks['ids']);
            }

        }catch (\Exception $ex){
            $taskData = array();
        }
        return $taskData;
    }

    public function getPackgeLinks(&$html){
	    $package = array();
        $package['links'] = array();
        $package['ids'] = array();
        $page = \str_get_html($html);
        $rows = $page->find('table[id="package-items-table"] tbody tr');

        if($rows != null){
            foreach ($rows as $row){
                $rowDOM = \str_get_html($row);
                $columns = $rowDOM->find("td");
                $link = $rowDOM->find("td a");
                if($columns != null){
                    $data = array();
                    foreach ($columns as $column){
                        $data[] = trim($column->plaintext);
                    }
                    if(isset($data[3]) && count($data) === 5){
                        $id = explode("-",$data[3]);
                        $package['ids'][] = trim($id[0]);
                    }
                    if($link != null){
                        $package['links'][] = trim($link[0]->href);
                    }
                }
                unset($rowDOM);
                unset($columns);
            }
        }
        unset($page);
        return $package;
    }

    public function getTaskPropertyByIdHTML($id,&$html){
        $propertyValue = null;
        $page = \str_get_html($html);
        $property = $page->find($id);
        if($property != null){
            $propertyValue = trim($property[0]->plaintext);
        }
        unset($page);
        return $propertyValue;
    }
}
?>