<?php

/**
* @author Ljimenez
* @version 1.0
* created on 5 jul. 2018
*/
namespace harvestModule\sources\connectService;

use harvestModule\sources\Jira;
use Functions;

include_once BaseDir. '/Functions.php';

class JiraMail {
	private $errorMessage;
	private $fileName;
	private $mailContent;
	private $titleTask;
	private $destination;
	private $destinationId;
	private $sourceLanguage;
	private $targetLanguage;
	private $weightedWords;
	private $dueDate;
	private $contentType;
	private $mailParsedOk;
	private $breakPoints;
	private $totalWords;
	private $data;
	
	public const _NO_ERROR = 'No error found';
	public const _EMAIL_NO_TASK = 'This email is not a task email.';
	public const _MAIL_FILE_NOT_FOUND = 'The mail file does not exist.';
	public const _REQUIRED_STRING_NOT_FOUND = 'A needed string to parse the content was not found in this email';
	public const _INIT_HREF_NOT_FOUND = 'init href anchor not found';
	public const _END_HREF_NOT_FOUND = 'end href anchor not found';
	
	public function __construct(string $fileName = ""){
		$this->breakPoints = array(
				"Estimated weighted words:",
				"Total words:",
				"Translation due date:",
				"Content type:",
				"Product:",
				"Source language",
				"Target language:"
		);
		
		$this->fileName = $fileName;
	}
	
	public function __call($methodName, $args){
		if(substr($methodName, 0, 3) =="get" && count($args)==0){
			$property = lcfirst (preg_replace("/get/","" , $methodName, 1));
			if(property_exists($this, $property)){
				return $this->$property;
			}
			throw new \Exception("TMS basic Property $property not exist", 500);
		}
	}
	
	/**
	 * @return mixed
	 */
	public function getTotalWords() {
		return $this->totalWords;
	}

	/**
	 * @param mixed $totalWords
	 */
	public function setTotalWords($totalWords) {
		$this->totalWords = $totalWords;
	}

	/**
	 * @return string
	 */
	public function getTitleTask() {
		return $this->titleTask;
	}

	/**
	 * @return string
	 */
	public function getDestination() {
		return $this->destination;
	}

	/**
	 * @return mixed
	 */
	public function getDestinationId() {
		return $this->destinationId;
	}

	/**
	 * @return string
	 */
	public function getSourceLanguage() {
		return $this->sourceLanguage;
	}

	/**
	 * @return string
	 */
	public function getTargetLanguage() {
		return $this->targetLanguage;
	}

	/**
	 * @return number
	 */
	public function getWeightedWords() {
		return $this->weightedWords;
	}

	/**
	 * @return string
	 */
	public function getDueDate() {
		return $this->dueDate;
	}

	/**
	 * @return string
	 */
	public function getContentType() {
		return $this->contentType;
	}

	/**
	 * @return boolean
	 */
	public function getMailParsedOk() {
		return $this->mailParsedOk;
	}

	/**
	 * @param string $titleTask
	 */
	public function setTitleTask($titleTask) {
		$this->titleTask = $titleTask;
	}

	/**
	 * @param Ambigous <string, mixed> $destination
	 */
	public function setDestination($destination) {
		$this->destination = $destination;
	}

	/**
	 * @param mixed $destinationId
	 */
	public function setDestinationId($destinationId) {
		$this->destinationId = $destinationId;
	}

	/**
	 * @param mixed $sourceLanguage
	 */
	public function setSourceLanguage($sourceLanguage) {
		$this->sourceLanguage = $sourceLanguage;
	}

	/**
	 * @param mixed $targetLanguage
	 */
	public function setTargetLanguage($targetLanguage) {
		$this->targetLanguage = $targetLanguage;
	}

	/**
	 * @param number $weightedWords
	 */
	public function setWeightedWords($weightedWords) {
		$this->weightedWords = $weightedWords;
	}

	/**
	 * @param string $dueDate
	 */
	public function setDueDate($dueDate) {
		$this->dueDate = $dueDate;
	}

	/**
	 * @param mixed $contentType
	 */
	public function setContentType($contentType) {
		$this->contentType = $contentType;
	}

	/**
	 * @param boolean $mailParsedOk
	 */
	public function setMailParsedOk($mailParsedOk) {
		$this->mailParsedOk = $mailParsedOk;
	}

	/**
	 * @return string
	 */
	public function getErrorMessage() {
		return $this->errorMessage;
	}

	/**
	 * @return string
	 */
	public function getFileName() {
		return $this->fileName;
	}

	/**
	 * @return string
	 */
	public function getMailContent() {
		return $this->mailContent;
	}

	/**
	 * @param string $errorMessage
	 */
	public function setErrorMessage($errorMessage) {
		$this->errorMessage = $errorMessage;
	}

	/**
	 * @param string $fileName
	 */
	public function setFileName($fileName) {
		$this->fileName = $fileName;
	}

	/**
	 * @param string $mailContent
	 */
	public function setMailContent($mailContent) {
		$this->mailContent = $mailContent;
	}

	public function readMail($moveFile = true){
		if($this->fileName == false){
			$this->errorMessage = JiraMail::_MAIL_FILE_NOT_FOUND;
            return false;
        }


        $stringStart = "Content-Transfer-Encoding: 7bit";
        if(!file_exists(Jira::$jira_aps_mail_path.$this->fileName)){
            $this->errorMessage = JiraMail::_MAIL_FILE_NOT_FOUND;
            return false;
        }

        $mail = file_get_contents(Jira::$jira_aps_mail_path.$this->fileName);

        if($mail === false){
            $this->errorMessage = JiraMail::_MAIL_FILE_NOT_FOUND;
            return false;
        }

        $this->mailContent = $mail;

        $inStart = strpos($mail, $stringStart, 0);

        if($inStart===false){
            return false;
        }

        $stringMailOk = "Subject: [APS] New translation work available:";
        if(strpos($mail,$stringMailOk,0) === false){
            $this->errorMessage = JiraMail::_EMAIL_NO_TASK;
            return false;
        }

        $inStart += strlen($stringStart);

        $mail = substr($mail, $inStart,strlen($mail)-$inStart);

        $stringStart = "A new translation request has been shared with you:";

        $inStart = strpos($mail, $stringStart, 0);

        if($inStart===false){
            $this->errorMessage = JiraMail::_REQUIRED_STRING_NOT_FOUND;
            return false;
        }

        $inStart += strlen($stringStart);
        $mail = substr($mail, $inStart, strlen($mail)-$inStart);

        $inStrongPos = strpos($mail, "<strong>", 0);
        if($inStart===false){
            $this->errorMessage = JiraMail::_REQUIRED_STRING_NOT_FOUND;
            return false;
        }

        $inStrongPos += strlen("<strong>");

        $endStrongPos = strpos($mail, "</strong>", $inStrongPos);
        if($endStrongPos===false){
            $this->errorMessage = JiraMail::_REQUIRED_STRING_NOT_FOUND;
            return false;
        }

        $description = subStr($mail, $inStrongPos, ($endStrongPos-$inStrongPos));
        $this->titleTask = $description;

        $ahrefPos = strpos($mail, "href=\"", 0);
        if($ahrefPos===false){
            $this->errorMessage = JiraMail::_INIT_HREF_NOT_FOUND;
            return false;
        }

        $ahrefPos +=6;

        $endHrefPos = strpos($mail, "\"", $ahrefPos);

        if($endHrefPos===false){
            $this->errorMessage = JiraMail::_END_HREF_NOT_FOUND;
            return false;
        }
        $this->destination = substr($mail, $ahrefPos, ($endHrefPos-$ahrefPos));

        $this->destination = str_replace(Jira::APS_URL, "", $this->destination);
        $this->destinationId = str_replace("/browse/","", $this->destination);
        $mail =  preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i",'<$1$2>', $mail);
        $endPos = strpos($mail, "</table>", 0);

        if($endPos===false){
            $this->errorMessage = JiraMail::_REQUIRED_STRING_NOT_FOUND;
            return false;
        }

        $mail = substr($mail, 0, $endPos);
        $startTablePos = strpos($mail, "<table>", 0 );
        if($startTablePos===false){
			$this->errorMessage = JiraMail::_REQUIRED_STRING_NOT_FOUND;
			return false;
		}
		
		$mail = substr($mail, $startTablePos, ($endPos-$startTablePos));
		$tableElements = array();
		$trKey = 0;
		$trPos = strpos($mail,"<tr>");
		while($trPos!==false){
			$trPos+= 4;
			$initTdPos = strpos($mail, "<td>", $trPos);
			$endTrPos = strpos($mail, "</tr>",$trPos);
			$text =  trim(strip_tags (substr($mail, $trPos, ($endTrPos-$trPos))));
			
			$rsp = $this->texteBreakPoints($text);
			if($rsp[0]!==false){
				$tableElements[$rsp[1]] = $rsp[0];
				
				switch($rsp[1]){
					case "WW":
						$this->weightedWords = (float) $rsp[0];
						break;
					case "TL":
						$this->targetLanguage = $rsp[0];
						break;
					case "SL":
						$this->sourceLanguage = $rsp[0];
						break;
					case "DD":
						$this->dueDate = $rsp[0];
						break;
					case "CT":
						$this->contentType = $rsp[0];
						break;
					case "TW":
						$this->totalWords = (int) $rsp[0];
						break;
				}
			}
			
			$trPos = strpos($mail,"<tr>", $trPos);
			$trKey++;
		}
        $this->mailParsedOk = $this->analyzeDataEmail();
        if($moveFile){
            $this->moveEmailFile();
        }
        return $this->mailParsedOk;
	}
	
	public function analyzeDataEmail(){
		$this->data = array();
		
		$this->data["weight"] = is_numeric($this->weightedWords);
		$this->data["target"] = is_string($this->targetLanguage);
		$this->data["source"] = is_string($this->sourceLanguage);
		$this->data["dueDate"] = \Functions::is_date($this->dueDate);
		$this->data["totalWord"] = is_numeric($this->totalWords);
		$this->data["content"] = is_string($this->contentType);
		$this->data["destination"] = is_string($this->destination);
		$this->data["idTask"] = is_string($this->destinationId);
		
		$validation =
            is_string($this->targetLanguage) &&
            is_string($this->sourceLanguage) &&
            Functions::is_date($this->dueDate) &&
            is_string($this->contentType) &&
            is_string($this->destination) &&
            is_string($this->destinationId);

		return $validation;
	}
	
	public function getAnalysis(){
		return $this->data;
	}
	
	private function texteBreakPoints($string){
		if (strpos($string, 'Estimated') !== false) {
			$string = str_replace("&nbsp;", " ",$string);
		}
		foreach($this->breakPoints as $key => $bp){
			if(strpos($string, $bp)!==false){
				return array(trim(str_replace($bp, "",$string)), $this->getValueTypeFromKey($key));
			}
		}
		
		return array(false, false);
	}
	
	private function getValueTypeFromKey($key){
		switch($key){
			case 0:
				return "WW";
			case 1:
				return "TW";
			case 2:
				return "DD";
			case 3:
				return "CT";
			case 4:
				return "PR";
			case 5:
				return "SL";
			case 6:
				return "TL";
		}
	}
	
	/**
	 * Delete
	 *
	 * @return boolean
	 */
	public function moveEmailFile(){
		try{
			if($this->mailParsedOk){
				if(@copy(Jira::$jira_aps_mail_path.$this->fileName,Jira::$jira_aps_mail_path."processed/".$this->fileName)){
					@unlink(Jira::$jira_aps_mail_path.$this->fileName);
				}
				
			}else{
				if(@copy(Jira::$jira_aps_mail_path.$this->fileName,Jira::$jira_aps_mail_path."damaged/".$this->fileName)){
					@unlink(Jira::$jira_aps_mail_path.$this->fileName);
				}
			}
			
		}
		catch(\Exception $e){
			Functions::addLog('Error to delete email => '.$this->fileName.' Message => '.$e->getMessage(), \Functions::ERROR,"File => ".__FILE__."    Method => ".__METHOD__."    Line => ".__LINE__);
			return false;
		}
		return true;
	}
}
?>