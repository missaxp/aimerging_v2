<?php

/**
* @author Ljimenez
* @version 1.0
* created on 10 jul. 2018
*/
namespace harvestModule\sources\connectService;

use harvestModule\sources\Welocalize;

include_once BaseDir. '/harvestModule/sources/connectService/AuthenticationWelocalize.php';

class WelocalizeConnect {
	
	public const RESPONSE_FORMAT = 'json';
	public const PHASE = 'PROPOSED';
	public const STATUS = '';
	public const TIMEOUT = '30';
	/**
	 * 
	 * @var AuthenticationWelocalize
	 */
	private $authentification;
	
	/**
	 * 
	 * @return string
	 */
	public function getAuthentification(){
		return $this->authentification->getToken();
	}
	
	public function __construct(string $user, string $password){
		$this->authentification = new AuthenticationWelocalize($user,$password);
	}
	
	public function request($url = null,$type = 'GET',$body = null){
		$s = curl_init();
		$ts =  time();
		
		if($url == null){
			$url = Welocalize::STR_SERVER."/Vendor/VendorTaskStatusTracking/UnapprovedTasks.".WelocalizeConnect::RESPONSE_FORMAT;
			$url.= "?phase=".WelocalizeConnect::PHASE."&status=".WelocalizeConnect::STATUS."&timestamp=".$ts;
		}
		
		curl_setopt($s,CURLOPT_URL,$url);
		curl_setopt($s,CURLOPT_USERAGENT,"iDISC Connector");
		curl_setopt($s,CURLOPT_HTTPHEADER,array('xciAuthenticationToken: '.$this->authentification->getToken()));
		curl_setopt($s,CURLOPT_TIMEOUT,WelocalizeConnect::TIMEOUT);
		curl_setopt($s,CURLOPT_RETURNTRANSFER,true);
		
		if($type == 'POST'){
			curl_setopt($s,CURLOPT_POST,true);
			$field_string = http_build_query($body);
			curl_setopt($s,CURLOPT_POSTFIELDS,$field_string);
		}
		
		try{
			$output = curl_exec($s);
		}
		catch(\Exception $e){
			return false;
		}
		
		$json_decode = json_decode($output,true);
		
		return $this->checkJSON($json_decode,$output);
	}
	
	
	public function getTask($idTask){
		$url = Welocalize::STR_SERVER."/Vendor/VendorTaskStatusTracking/TaskStatusTimestamp.".WelocalizeConnect::RESPONSE_FORMAT."?taskId=".$idTask;
		
		$s = curl_init();
		
		curl_setopt($s,CURLOPT_URL,$url);
		curl_setopt($s,CURLOPT_USERAGENT,"iDISC Connector");
		curl_setopt($s,CURLOPT_HTTPHEADER,array('xciAuthenticationToken: '.$this->authentification->getToken()));
		curl_setopt($s,CURLOPT_TIMEOUT,WelocalizeConnect::TIMEOUT);
		curl_setopt($s,CURLOPT_RETURNTRANSFER,true);
		
		try{
			$output = curl_exec($s);
		}
		catch(\Exception $e){
			curl_close($s);
			return false;
		}
		curl_close($s);
		
		$ts = str_replace("\"","",$output);
		return $ts;
	}
	
	public function getAssignableContacts($userName){
		$ts =  time();
		$url = Welocalize::STR_SERVER."/Vendor/VendorProfile/AssignableContacts.".WelocalizeConnect::RESPONSE_FORMAT."?timestamp=".$ts;
		$welocalizeUsers = null;
		$resposta = $this->request($url);
		if($resposta != null){
			\Functions::addLog(json_encode($resposta), "INFO", "Weolocalize");
		}
		
		if($resposta == false){
			return null;
		}
		
		$welocalizeUsers = array();
		foreach ($resposta["json"]["vendorContacts"] as $key => $value){
			$welocalizeUsers[count($welocalizeUsers)] = $value;
		}
		
		$i = 0;
		while($i<count($welocalizeUsers)){
			if($welocalizeUsers[$i]["fullName"]==$userName){
				return $welocalizeUsers[$i]["vendorProfileUserId"];
			}
			$i++;
		}
		return null;
	}
	
	private function checkJSON($json_decode,$original){
		$resposta = array();
		if(empty($json_decode)){
			$obj = @simplexml_load_string($original); // Parse XML
			$json_decode = json_decode(json_encode($obj), true); // Convert to array
			if(empty($json_decode)) //Si no es JSON ni XML llancem excepció
				$resposta["estat"] = "ERROR";
				$resposta["missatge"] = "El contingut no és JSON ni XML";
				return $resposta;
		}
		foreach ($json_decode as $key => $value) {
			if($key == "exception"){ //Exception Handling for REST web services
				$resposta["estat"] = "ERROR";
				$resposta["missatge"] = "Exception: ".$json_decode["exception"]["exceptionMessage"];
				return $resposta;
			}
			if($key == "errors"){ 
				$resposta["estat"] = "ERROR";
				$resposta["missatge"] = "Validation: ".$json_decode["errors"][0]["errorCode"]." : ".$json_decode["errors"][0]["errorField"];
				return $resposta;
			}
		}
		$resposta["estat"] = "OK";
		$resposta["missatge"] = "";
		$resposta["json"] = $json_decode;
		return $resposta;
	}
}