<?php

/**
* @author Ljimenez
* @version 1.0
* created on 6 jul. 2018
*/
namespace harvestModule\sources\connectService;

class FalconSoapHeader extends \SoapHeader{
	public function __construct($strUser, $strPass){
		$strNs = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
		
		$oAuth = new \stdClass();
		$oAuth->Username = new \SoapVar($strUser, XSD_STRING, NULL, $strNs, NULL, $strNs);
		$oAuth->Password = new \SoapVar($strPass, XSD_STRING, NULL, $strNs, NULL, $strNs);
		$oUsernameToken = new \stdClass();
		
		$oUsernameToken->UsernameToken = new \SoapVar($oAuth, SOAP_ENC_OBJECT, NULL, $strNs, 'UsernameToken', $strNs);
		$oSecurity = new \SoapVar(new \SoapVar($oUsernameToken, SOAP_ENC_OBJECT, NULL, $strNs, 'UsernameToken', $strNs), SOAP_ENC_OBJECT, NULL, $strNs, 'Security', $strNs);
		parent::__construct($strNs, 'Security', $oSecurity, true);
	}
}
?>