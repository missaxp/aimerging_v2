<?php

namespace harvestModule\sources\connectService;


class WebServiceMoravia_V1 {
	private $api = "https://symfonie.moravia.com";
	
	private $auth_cookie;

	private $credentials;
	
	public function __construct($cred){
		Rest::setAuthenticationMethod(Rest::$COOKIE);
		$this->credentials = $cred;
		$this->auth_cookie = $this->getAuthCookie();
	}
	
	private function getAuthCookie(){
		$rest = new Rest($this->api."/account/auth_token/".$this->credentials->getToken());
		$c = $rest->getCookies();
		if(isset($c["ASP.NET_SessionId"])){
			return $c;
		}
		else{
			return null;
		}
	}
	
	public function getTaskById($id){
		$url = $this->api."/api/tasks/".$id;
		$rest = new Rest($url,$this->auth_cookie);
		return json_decode($rest->createRequest(),true);
	}
	
	public function getTasks($opt = null){
		$stringify = null;
		if($opt != null){
			$stringify = "?";
			foreach ($opt as $key => $value){
				$stringify .= $key."=".$value."&";
			}
			$stringify = substr($stringify,0,strlen($stringify)-1);
			//die($stringify);
		}
		$url = $this->api."/api/tasks".(($stringify!=null)? $stringify:'');
		$rest = new Rest($url,$this->auth_cookie);
		return json_decode($rest->createRequest(),true);
	}
	
	public function getTaskamounts(){
		$url = $this->api."/api/taskamounts?limit=1";
		$rest = new Rest($url,$this->auth_cookie);
		return json_decode($rest->createRequest(),true);
	}
	
	public function getLanguageById($id){
		$url = $this->api."/api/languages/".$id;
		$rest = new Rest($url,$this->auth_cookie);
		return json_decode($rest->createRequest(),true);
	}
	
	public function getUserattachmentsById($id){
		$url = $this->api."/api/taskattachments?filter=task_id==$id";
		$rest = new Rest($url,$this->auth_cookie);
		return json_decode($rest->createRequest(),true);
	}
	
	public function getWordCountById($id){
		$url = $this->api."/api/taskamounts?filter=task_id==$id";
		$rest = new Rest($url,$this->auth_cookie);
		return json_decode($rest->createRequest(),true);
	}
	
	public function getProjects(){
		$url = $this->api."/api/projects";
		$rest = new Rest($url,$this->auth_cookie);
		return json_decode($rest->createRequest(),true);
	}
	
	public function getProjectInfoById($id){
		$url = $this->api."/api/projects/".$id;
		
		$rest = new Rest($url,$this->auth_cookie);
		return json_decode($rest->createRequest(),true);
	}
	
	public function updateTask($opt = null){
		$stringify = null;
		if($opt != null){
			$stringify = "?";
			foreach ($opt as $key => $value){
				$stringify .= $key."=".$value."&";
			}
			$stringify = substr($stringify,0,strlen($stringify)-1);
		}
		$url = $this->api."/api/task/actions".(($stringify!=null)? $stringify:'');
		$rest = new Rest($url,$this->auth_cookie,Rest::_POST);
		$rsp = json_decode($rest->createRequest(),true);
		if($rsp["success"]){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function uploadAttachment($opt,$file){
		$stringify = null;
		if($opt != null){
			$stringify = "?";
			foreach ($opt as $key => $value){
				$stringify .= $key."=".$value."&";
			}
			$stringify = substr($stringify,0,strlen($stringify)-1);
		}
		$url = $this->api."/api/taskattachment/Upload".(($stringify!=null)? $stringify:'');
		$rest = new Rest($url,$this->auth_cookie,Rest::_POST);
		$rest->addFile($file);
		$rsp = json_decode($rest->createRequest(),true);
		return $rsp["success"];
	}
}
?>