<?php


namespace harvestModule\sources\connectService;

use common\exceptions\AIException;
use core\PlancakeEmailParser;
use Exception;
use Functions;
use GoogleSDLMailException;

include_once BaseDir. '/PlancakeEmailParser.php';
include_once BaseDir. '/simple_html_dom.php';


class GoogleSDLMail {
    private $mail_file;

    private $data = array(
        "subject" => null,
        "project" => null,
        "cat_tool" => null,
        "task" => null,
        "deadline" => null,
        "product" => null,
        "component" => null,
        "source_lang" => null,
        "target_lang" => null,
        "weighted_words" => null,
        "total_words" => null,
        "instructions" => null,
    );

    private $body;



    private $posibleBreakPoints = array(
        "Google project: ",
        "CAT Tool: ",
        "Service Type: ",
        "Due Date: ",
        "Product: ",
        "Source Language:",
        "Component: ",
        "Target Language:",
        "Total Weighted Word Count:",
        "Total Word Count:",
        "Client Instructions:",
        "Google Project Management Tool"
    );

    public function __construct(String $mail){
        $this->mail_file = $mail;
    }

    /**
     * @throws AIException
     */
    public function processMail(){
        if(!file_exists($this->mail_file)){
            throw (new GoogleSDLMailException("Cannot find email file"))->construct(__METHOD__, __NAMESPACE__, $f = func_get_args(), array("file" => $this->mail_file));
        }
        $mail_parser = new PlancakeEmailParser( file_get_contents($this->mail_file) );

        try {
            $this->data["subject"] = trim(str_replace("A project has been shared with you. ", "" , $mail_parser->getSubject()));
        } catch (Exception $e) {
            throw AIException::createInstanceFromThrowable($e)->construct(__METHOD__, __NAMESPACE__, $f = func_get_args());
        }

        $this->body = $mail_parser->getPlainBody();

        $lines = explode("\n", $this->body );
        $lines = $this->cleanLines($lines);
        $this->processDataFromLines($lines);
        return $this->data;
    }
    private function cleanLines(array $lines){
        $arra = array();
        foreach ($lines as $line) {
            $arra[] = preg_replace('/[[:cntrl:]]/', '', $line);
        }
        return $arra;
    }
    private function containsBreakpoint($line){
        $line = trim($line);
        if(empty($line)) return false;

        foreach($this->posibleBreakPoints as $breakpoint){
            //Functions::console($line . "====>" . $breakpoint);
            if(stripos($line, $breakpoint) !== false) {
                return $breakpoint;
            }
        }
        return false;
    }
    private function processDataFromLines(array $lines) {
        //Functions::print($lines);
        $propertyValue = "";
        for($i = 0; $i< count($lines); $i++){
            $line = $lines[$i];
            $breakpoint = $this->containsBreakpoint($line);
            if($breakpoint === false){
                $propertyValue .= $line;
                continue;
            } else {
                switch($breakpoint){
                    case "Google project: ":
                        $propertyValue = "";
                        $this->data["project"] = trim(str_replace("Google project: ", "", $line));
                        break;
                    case "CAT Tool: ":
                        $propertyValue = "";
                        $this->data["cat_tool"] = trim(str_replace("CAT Tool: ", "", $line));
                        break;
                    case "Service Type: ":
                        $propertyValue = "";
                        $this->data["task"] = trim(str_replace("Service Type: ", "", $line));
                        break;
                    case "Due Date: ":
                        $propertyValue = "";
                        $this->data["deadline"] = trim(str_replace("Due Date: ", "", $line));
                        break;
                    case "Product: ":
                        $propertyValue = "";
                        $this->data["product"] = trim(str_replace("Product: ", "", $line));
                        break;
                    case "Component: ":
                        $propertyValue = "";
                        $this->data["component"] = trim(str_replace("Component: ", "", $line));
                        break;
                    case "Source Language:":
                        $propertyValue = "";
                        $this->data["source_lang"] = trim(str_replace("Source Language:", "", $line));
                        break;
                    case "Target Language:":
                        $propertyValue = "";
                        $this->data["target_lang"] = trim(str_replace("Target Language:", "", $line));
                        break;
                    case "Total Weighted Word Count:":
                        $propertyValue = "";
                        $this->data["weighted_words"] = trim(str_replace("Total Weighted Word Count:", "", $line));
                        break;
                    case "Total Word Count:":
                        $propertyValue = "";
                        $this->data["total_words"] = trim(str_replace("Total Word Count:", "", $line));
                        break;
                    case "Client Instructions:":
                        $propertyValue = "";
                        break;
                    case "Google Project Management Tool":
                        $this->data["instructions"] = trim($propertyValue);
                        break;
                }
            }
        }

    }
}