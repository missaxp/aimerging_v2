<?php
namespace harvestModule\sources\connectService;


use common\exceptions\AIException;
use common\exceptions\sources\apple_liox\ProjectIdNotFoundException;
use core\AI;
use Functions;
use harvestModule\sources\AppleLioxTeams;
use Exception;
use common\exceptions\sources\apple_liox\MailException;
use core\PlancakeEmailParser;
include_once BaseDir. '/PlancakeEmailParser.php';
include_once BaseDir. '/simple_html_dom.php';
include_once BaseDir. '/Functions.php';
include_once BaseDir. '/common/exceptions/sources/apple_liox/MailException.php';
include_once BaseDir. '/common/exceptions/AIException.php';
include_once BaseDir. '/common/exceptions/sources/apple_liox/ProjectIdNotFoundException.php';
include_once BaseDir. '/harvestModule/sources/AppleLiox.php';
class AppleWelocalizeMail
{
    private $attachments_folder = "";
    /**
     * @var String Email file path
     */
    private $email_file_path;
    /**
     * The plancakeEmailParser intance
     * @var PlancakeEmailParser
     */
    private $email_parser = null;
    /**
     * @var string Email subject
     */
    private $emailSubject = "";
    /**
     * Holds the body in HTML format
     * @var string
     */
    private $emailBodyHTML = "";
    /**
     * @var string Holds the body in plan text format
     */
    private $emailBodyTXT = "";
    /**
     * All the mail contents
     * @var false|string
     */
    private $email_raw_data = "";

    /**
     * Returns the email file path
     * @return String
     */
    public function getEmailFilePath()
    {
        return $this->email_file_path;
    }

    /**
     * Returns the email subject
     * @return string
     */
    public function getEmailSubject()
    {
        return $this->emailSubject;
    }

    /**
     * Returns the email body in plain text
     * @return string
     */
    public function getBodyTXT()
    {
        return $this->emailBodyTXT;
    }

    public function getBodyHTML()
    {
        return $this->emailBodyHTML;
    }

    /**
     * Returns the array that contains the email data
     * @return array
     */
    public function getEmailContents()
    {
        return $this->email_contents;
    }

    /**
     * This helps to split the HTML cells in a map like array
     * @var array
     */
    private $posibleBreakPoints = array(
        "Client",
        "Name",

    );
    /**
     * Holds all the email data
     * @var array
     */
    private $email_contents = array(
        "file" => "",
        "subject" => "",
        "source_language" => "",
        "target_laguages" => array(
            // array(),
            // array(),
        ),
        //tasks => "",
        "wwc" => "", //weighted words count
        "deadline" => "",
        "genre" => "", //genero
        "additional_instructions" => "",
        "editting_scope" => "",
        "references" => "",
        "localization_info" => "",
        "ltb_configurations_link" => "",
        "project_name" => array(),
        "project_id" => array(),
        "extra_info" => "",
        "projectTeam" => "",
        "task" => "",
        "attachments" => array(),
    );

    /**
     * AppleLioxMail constructor.
     * @param $email_file_path
     * @throws MailException When it find an unknown mail format
     * @throws ProjectIdNotFoundException When it cannot get the project ID
     * @throws AIException
     */
    public function __construct($email_file_path)
    {
        $attachmentsDir = AI::getInstance()->getSetup()->repository . "AppleWelocalizeWS/attachments/";
        echo($attachmentsDir);
        $this->attachments_folder = $attachmentsDir;
        if (!file_exists($attachmentsDir)) {
            mkdir($attachmentsDir, 0760, true);
        }
        $this->email_file_path = $email_file_path;
        $email_parts = explode("/", $email_file_path);
        $this->email_contents["file"] = end($email_parts);
        $this->parseMessage();
        $this->getProjectId();
        $this->email_raw_data = file_get_contents($email_file_path);
    }

    /**
     * Tries to get the basic data of the email
     * @throws AIException
     */
    private function parseMessage()
    {
        $this->email_parser = new PlancakeEmailParser(file_get_contents($this->email_file_path));
        try {
            $this->emailSubject = $this->email_parser->getSubject();
        } catch (Exception $e) {
            throw (AIException::createInstanceFromThrowable($e))
                ->construct(__FUNCTION__, __NAMESPACE__, $args = func_get_args());
        }
        $this->email_contents["subject"] = $this->emailSubject;
        //$this->email_contents["projectTeam"] = $this->getProjectTeam($this->emailSubject);
        $attachments = $this->email_parser->getAttachmentArray();
        foreach ($attachments as $attachment) {
            if (isset($attachment["file_d_type"]) && $attachment["file_d_type"] === "attachment") {
                if ($attachment["file_encoding"] === "base64") {
                    $output_file = $this->attachments_folder . uniqid() . explode(";", $attachment["file_name"])[0];
                    if ($this->base64ToFile($attachment["file_content"], $output_file)) {
                        $this->email_contents["attachments"][] = $output_file;
                    }
                }
            }
        }
        $HTMLbody = $this->email_parser->getHTMLBody();
        $this->emailBodyHTML = preg_replace('/[[:cntrl:]]/', '', $HTMLbody);
        $this->emailBodyTXT = $this->email_parser->getBody();

    }

    /**
     * @param $base64_string
     * @param $output_file
     * @return bool
     */
    private function base64ToFile($base64_string, $output_file)
    {
        $ifp = fopen($output_file, 'wb');
        if ($ifp === false) {
            return false;
        }
        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);
        return true;
    }

    /**
     * Find the project team in the email subject. The project team is an additional propertie.
     * @param $data String The email subject [][]
     * @return string The project team
     */
    public function getProjectTeam($data)
    {
        $subject = strtolower($data);
        if (stripos("$subject", "Apple_Sales_Training") !== false) {
            return AppleLioxTeams::SalesTraining;
        } else if (stripos("$subject", "shared") !== false) {
            return AppleLioxTeams::SharedServices;
        } else if (stripos("$subject", "AppleSalesTraining") !== false) {
            return AppleLioxTeams::SalesTraining;
        } else if (stripos("subject", "Hand_Off_AppleCare_Inquira") !== false) {
            return AppleLioxTeams::AppleCareInquira;
        } else if (stripos("$subject", "Hand_off_LS_") !== false) {
            return AppleLioxTeams::AppleHeart;
        } else if (stripos("$subject", "Hand_Off_AppleCare_kBase") !== false) {
            return AppleLioxTeams::AppleCareKbase;
        } else if (stripos("$subject", "AppleCare") !== false) {
            return AppleLioxTeams::AppleCareKbase;
        } else {
            return AppleLioxTeams::Unknown;
        }
    }

    /**
     * Tries to find the WorldServer ID in the subject or call to find in the email body
     * @throws MailException when it find an unknown mail format
     * @throws Exception when it find an unknown mail format
     * @throws ProjectIdNotFoundException When it cannot find the project id
     */
    public function getProjectId()
    {
        $subject = $this->emailSubject;
        if (strpos(strtolower($subject), 're:') === 0) {
            throw new MailException(MailException::UNSUPPORTED_MAIL_TYPE_RE);
        }
        if (strpos(strtolower($subject), 'fw') === 0) {
            throw new MailException(MailException::UNSUPPORTED_MAIL_TYPE_FW);
        }
        if (strpos(strtolower($subject), 'q&a') === 0 || strpos(strtolower($subject), 'query answer') === 0) {
            throw new MailException(MailException::UNSUPPORTED_MAIL_TYPE_QA);
        }

        preg_match_all('/(?<!\d)\d{6}(?!\d)/', $subject, $matches);
        $matches = $this->flatten_array($matches);

        if (count($matches) == 0) {
            $encontrado_en_body = $this->findProjectIdInBody();
            if ($encontrado_en_body === false) {
                throw new ProjectIdNotFoundException("Cannot find project ID");
            }
        } else {
            $this->email_contents["project_id"] = $matches;
        }
    }

    /**
     * Tries to find the project ID in the email body
     * @return bool True if have been found
     */
    private function findProjectIdInBody()
    {

        $html = str_get_html($this->emailBodyHTML);
        if ($html === false) return false;
        $celdas = $html->find('td');

        for ($i = 0; $i < count($celdas); $i++) {
            $current_value = $celdas[$i]->plaintext;
            if (stripos($current_value, "WS number") !== false || stripos($current_value, "Project name") !== false) {

                $posible_id_string = $celdas[$i + 1]->plaintext;
                $matches = null;
                preg_match_all('/(?<!\d)\d{6}(?!\d)/', $posible_id_string, $matches);
                $matches = $this->flatten_array($matches);

                if (count($matches) == 0) continue;
                else {
                    $this->email_contents["project_id"] = $matches;
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Tries to analyze the mail structure to find the email data
     * @return array
     * @throws MailException Whe PlancakeMailParser cannot parse de HTML tags
     */
    public function analyzeMail()
    {
        $this->getMailInformation();
        $this->getInstructions();
        return $this->email_contents;
    }

    /**
     * From the structure of breakpoints, try to create a common generalized structure for all types of emails
     * @param $mail_map array This must contains a map with the email data
     * @return array That contains the email data in a generalized structure
     */
    private function generalizeMailFormat($mail_map)
    {

        foreach (array_keys($mail_map) as $key) {
            switch (trim($key)) {
                case "Additional Instructions:":
                case "Additional information:":
                case "Additional instruction:":
                case "Info:":
                case "Localization info":
                    $this->email_contents["additional_instructions"] = trim($mail_map[$key]);
                    break;
                case "SST LTB configuration:":
                case "SalesTraining LTB configuration:":
                case "AppleCare LTB configuration:":
                case "LTB configuration:":
                    $this->email_contents["ltb_configurations_link"] = trim($mail_map[$key]);
                    break;
                // case "Customer:":
                // case "Deliverables:":
                case "Translation deadline:":
                case "Deadline:":
                    $this->email_contents["deadline"] = trim($mail_map[$key]);
                    break;
                case "Editing Scope":
                    $this->email_contents["editting_scope"] = trim($mail_map[$key]);
                    break;
                // case "File format:":
                // case "Gemini no.":
                // case "Gemini no.:":
                case "Genre:":
                    $this->email_contents["genre"] = trim($mail_map[$key]);
                    break;
                case "Project name:":
                    $this->email_contents["project_name"] = trim($mail_map[$key]);
                    break;
                // case "Q&A deadline:":
                // case "Query management:":
                case "Reference:":
                    $this->email_contents["references"] = trim($mail_map[$key]);
                    break;
                case "Source language:":
                    $this->email_contents["source_language"] = trim($mail_map[$key]);
                    break;
                case "Target languages:":
                    $this->email_contents["target_laguages"] = trim($mail_map[$key]);
                    break;
                case "Tasks:":
                case "Tasks":
                case "tasks":
                case "tasks:":
                    $this->identifyTaskType(trim($mail_map[$key]));
                    break;
                // case "Tools:":
                // case "WS number:":
                case "WWC/total:":
                    $this->email_contents["wwc"] = trim($mail_map[$key]);
                    break;
                default:
                    $this->email_contents["extra_info"] .= $key . ": " . trim($mail_map[$key]) . " ";
            }
        }

        if ($this->email_contents["task"] === "") {
            $this->findTaskTypeInSubject();
        }

        return $this->email_contents;
    }

    /**
     * Remove all non breaking spaces from the string
     * @param $string String to be cleared
     * @return string
     */
    private function clearRowData($string)
    {
        //$result = str_replace("&nbsp;", "", $string);
        $result = html_entity_decode($string);
        $result = preg_replace("/\s|&nbsp;/", ' ', $result);
        $result = str_replace("\n\n", "\n", $result);
        return $result;
    }

    /**
     * Creates an unidimensional array from a multidimensional array
     * @param $arg array Multidimensional array
     * @return array Returns a unidimensional array
     */
    private function flatten_array($arg)
    {
        return is_array($arg) ? array_reduce($arg, function ($c, $a) {
            return array_merge($c, $this->flatten_array($a));
        }, []) : [$arg];
    }

    private function findTaskTypeInSubject()
    {
        $this->identifyTaskType($this->emailSubject);
    }

    private function identifyTaskType(string $text)
    {
        $subject = strtolower($text);
        if (stripos($subject, "isv") !== false) {
            $this->email_contents["task"] = AppleLioxTaskType::ISV;
        } else if (stripos($subject, "tep") !== false) {
            $this->email_contents["task"] = AppleLioxTaskType::TEP;
        } else if (stripos($subject, "icr") !== false) {
            $this->email_contents["task"] = AppleLioxTaskType::ICR;
        } else {
            $this->email_contents["task"] = AppleLioxTaskType::TEP;
        }
    }


    private function separateJobNameIdClientTask($jobName)
    {
        try {
            $idClientTask = substr($jobName, 0, strpos($jobName, ']'));
            $jobName = substr($jobName, strpos($jobName, ']') + 1);
            $idClientTask = str_replace('[', '', $idClientTask);
        } catch (Exception $exception) {
            return false;
        }
        return array("idClientTask" => $idClientTask, "jobName" => $jobName);
    }


    private function getMailInformation()
    {
        $html = str_get_html($this->emailBodyHTML);
        if($html === false) return false;
        $table_tasks = $html->find("table tr");
        $table_tasks = array_slice($table_tasks, 1);
        $tasks_table = array();
        foreach ($table_tasks as $row) {
            $information_task = $row->find("p");
            $task = array();
            foreach ($information_task as $item) {
                //echo($item->plaintext);
                $task[] = $item->plaintext;
            }
            $tasks_table[] = $task;
        }

        // var_dump(($tasks_table[1]));

        // $tasks = array();
        foreach ($tasks_table as $task) {
            $client = $task[0];
            $name = $task[1];
            $taskName = $this->separateJobNameIdClientTask($name);
            $idClientTask = $taskName["idClientTask"];
            $jobName = $taskName["jobName"];
            $this->email_contents["project_name"][] = $jobName;
            $this->email_contents["projectTeam"] = $client;
            $task = array("client"=>$client,"idClientTask"=>$idClientTask,"jobName"=>$jobName);
            $tasks[] = $task;
        }
        Functions::print($tasks);
    }

    private function getInstructions()
    {
        $mail_lines = explode("</p>", $this->emailBodyHTML);

        $count = 0;
        foreach ($mail_lines as $line) {
            if (strpos($line, "</table>") !== false) {
                break;
            }
            $count++;
        }
        $instructions = array_slice($mail_lines, $count + 1);

        $count = 0;
        foreach ($instructions as $instruction_line) {
            if (strpos($instruction_line, "Please kindly confirm") !== false) {
                break;
            }
            $count++;
        }
        $instructions = array_slice($instructions, 0, $count);
        $instructions = implode("", $instructions);
        $this->email_contents["additional_instructions"] = $instructions;

    }
}

abstract class AppleLWelocalizeTaskType{
    const ISV = "ISV";
    const TEP = "TEP";
    const ICR = "ICR";
}