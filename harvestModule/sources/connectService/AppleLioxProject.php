<?php

use common\downloadManager;
use common\exceptions\sources\apple_liox\WorldServerException;
use common\ZipFile;
use core\AI;
use harvestModule\sources\sourceComplements\WorldServerProject;
include_once BaseDir . "/harvestModule/sources/sourceComplements/WorldServerProject.php";

class AppleLioxProject extends WorldServerProject {
    public $sub_project_id;
    /**
     * This function makes a request to retrive HTML that contains the URL of the xlz file.
     * Overrited for making request to XWS
     */
    protected function postXLZIframe(){

        $curl = curl_init();

        $url = "$this->baseUrl/" . $this->xlzData["pagelocation"];
        $pagelocation = $this->xlzData["pagelocation"];
        $formlocation = urlencode($pagelocation);

        $default_headers = array(
            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
            "Accept-Language: es-ES,es;q=0.9",
            "Cache-Control: max-age=0",
            "Content-Type: application/x-www-form-urlencoded",
            "Upgrade-Insecure-Requests: 1",
            "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36",
        );

        $formAction = str_replace("https://ws01.apple.com/ws-legacy/", "", $url);
        curl_setopt_array($curl, array(
            CURLOPT_URL => "$url",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_COOKIEJAR => $this->cookieFile,
            CURLOPT_COOKIEFILE => $this->cookieFile,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 300,
            CURLOPT_CUSTOMREQUEST => "POST",
            //CURLOPT_POSTFIELDS => "scheme=desktopWorkbench&url_of_origin=&SegmentExclusionSelect=0&tmFilterId=-1&tdFilterId=-1&checkbox=$this->xlzCheckbox&cur_step=assetExportStep2&mode=projects&formAction=$formlocation&submittedBy=ok&methodUsed=POST",
            CURLOPT_POSTFIELDS => "scheme=trados_studio&url_of_origin=&tm_export_type=CONTENT&tmFilterId=-1&tdFilterId=-1&checkbox=$this->xlzCheckbox&cur_step=assetExportStep2&mode=projects&formAction=$formAction&submittedBy=ok&methodUsed=POST",
            CURLOPT_HTTPHEADER => $default_headers,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if ($err) {
            //echo "cURL Error #:" . $err;
            return false;
        } else {
            if($statusCode != 200){
                return false;
            }
            return ($response);
        }
    }


    /**
     * This is the function that downloads the xlz file.
     * @param $url_archivo
     * @return bool True if successful
     * @throws Exception On error
     */
    protected function postPostXLZIframe($url_archivo){
        $url = "$this->baseUrl/" . $this->xlzData["pagelocation"];
        $default_headers = array(
            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
            "Accept-Language: es-ES,es;q=0.9",
            "Cache-Control: max-age=0",
            "Content-Type: application/x-www-form-urlencoded",
            "Upgrade-Insecure-Requests: 1",
            "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36",
        );

        $curlCustomOptions = array(
            array(CURLOPT_COOKIEJAR, $this->cookieFile),
            array(CURLOPT_COOKIEFILE, $this->cookieFile),
        );

        $body = array(
            "download" => "yes",
            "wwbfile" => "$url_archivo",
            "suggestedname"=>"xliff_projects.zip"
        );

        $dm = new downloadManager();

        $rsp = $dm->setFileUrl($url)
            ->setAsync(false)
            ->setMethod("post")
            ->setFileName("$this->project_id-xlz.zip")
            ->setDownloadPath($this->download_path_in_repository . "$this->project_id/xlz/")
            ->setSyncCurlOptions($curlCustomOptions)
            ->addBody($body)
            ->addHeader($default_headers)
            ->run();
        if($rsp){
            $path = AI::getInstance()->getSetup()->repository . $this->download_path_in_repository . "$this->project_id/xlz/";
            $filePath = $path . "$this->project_id-xlz.zip";

            $has_been_extracted = ZipFile::unZipFile($filePath, $path);
            if($has_been_extracted){
                unlink($filePath);
                $xlzFiles = glob($path . "*.wsxz");
                foreach ($xlzFiles as $file){
                    $name = explode("/", "$file");
                    $name = end($name);
                    $this->files["files_to_translate"][] = array(
                        "file_name" => "$name",
                        "file_name_extension" => "wsxz",
                        "path" => $file,
                        "source" => "AppleWorldServer",
                        "source_path" => ""
                    );
                }
            } else {
                Functions::addLog("Cannot unzip file $filePath", Functions::ERROR, __CLASS__);
            }
        }
        return $rsp;
    }




    protected function getWordCountFromCsv($response){
        $array = explode("\n", $response);
        $array_wordCount = array();
        $info_wordcount = (array_slice($array,1,count($array)-3));
        foreach($info_wordcount as $line_info){
            $line_count = explode(",", $line_info);
            if($line_count[1] != ''){
                $this->wordCount = array(
                    "w101" => $line_count[4],
                    "w100" => $line_count[5],
                    "w95" => $line_count[6],
                    "w85" => $line_count[7],
                    "w75" => $line_count[8],
                    "repeat" => $line_count[9],
                    "total" => $line_count[10],
                    "target_language" => $line_count[1],
                );
                $array_wordCount[] =$this->wordCount;
            }
        }
        $this->wordCount =$array_wordCount;
        return $array_wordCount;
    }


    /**
     * This method downloads all the project files.
     * @throws WorldServerException
     */
    public function downloadFiles(){
        try{
            $setup = AI::getInstance()->getSetup();
        } catch(Exception $e){
            Functions::console("AppleLiox: Cannot access to setup variable");
            return false;
        }
        if (!file_exists($setup->repository . $this->download_path_in_repository . "$this->project_id")) {
            $path = $setup->repository . $this->download_path_in_repository . "$this->project_id";
            mkdir($path);
        }
        if (!file_exists($setup->repository . $this->download_path_in_repository . "$this->project_id/reference_files")) {
            mkdir($setup->repository. $this->download_path_in_repository . "$this->project_id/reference_files");
        }
        if (!file_exists($setup->repository  . $this->download_path_in_repository. "$this->project_id/source")) {
            mkdir($setup->repository . $this->download_path_in_repository . "$this->project_id/source");
        }
        if (!file_exists($setup->repository. $this->download_path_in_repository . "$this->project_id/xlz")) {
            mkdir($setup->repository. $this->download_path_in_repository . "$this->project_id/xlz");
        }
        $conteo = false;
        try{
            $conteo = $this->getConteoDePalabras();
        } catch(Exception $e){
            throw new WorldServerException(WorldServerException::CANNOT_DOWNLOAD_CSV);
        }
        if($conteo === false){
            Functions::console("AppleLiox: Cannot get the Word Count from project id: " . "$this->project_id");
            throw new WorldServerException(WorldServerException::CANNOT_DOWNLOAD_CSV);
        }
        if(!$this->downloadReferenceFiles()){
            throw new WorldServerException(WorldServerException::CANNOT_DOWNLOAD_REFERENCE_FILES);
        }
        if(!$this->downloadSourceFiles()){
            throw new WorldServerException(WorldServerException::CANNOT_DOWNLOAD_SOURCE_FILES);
        }

        $json_xlz = $this->getXLZJson();
        if($json_xlz === false){
            throw new WorldServerException(WorldServerException::CANNOT_ANALYZE_XLZ_JSON);
        }

        $projects_ids_length = count($json_xlz->projectids);
        for ($i = 0; $i < $projects_ids_length; $i++) {
            $this->sub_project_id = $json_xlz->projectids[$i];
            $response_iframe = $this->postToIframe($json_xlz->url , $json_xlz->projectids[$i]);
            if($response_iframe === false){
                throw new WorldServerException(WorldServerException::CANNOT_ANALYZE_XLZ_IFRAME);
            }

            $data = $this->analizarIframe($response_iframe);
            if (!$data){
                throw new WorldServerException(WorldServerException::CANNOT_ANALYZE_XLZ_IFRAME);
            }


            $response_postDownloadXlZ = $this->postXLZIframe();
            if (!$response_postDownloadXlZ){
                throw new WorldServerException(WorldServerException::CANNOT_ANALYZE_XLZ_IFRAME);
            }
            $data = $this->analizarpostXLZIframeResponse($response_postDownloadXlZ);
            if (!$data){
                throw new WorldServerException(WorldServerException::CANNOT_ANALYZE_XLZ_DOWNLOAD_POST);
            }
            try{
                $response_download_xlz = $this->postPostXLZIframe($data);
            } catch(Exception $e){
                Functions::console("Cannot download xlz for project id: $this->project_id") ;
                throw new WorldServerException(WorldServerException::CANNOT_ANALYZE_XLZ_POST);
            }
            if (!$response_download_xlz){
                Functions::console("Cannot download xlz for project id: $this->project_id") ;
                throw new WorldServerException(WorldServerException::CANNOT_DOWNLOAD_XLZ);
            }
        }
        $files_aux = array();
        foreach ($this->files["REFERENCE"] as $file){
            if (!in_array($file,$files_aux)){
                array_push($files_aux,$file);
            }
        }
        $this->files["REFERENCE"] = $files_aux;
        return $this->files;
    }
}