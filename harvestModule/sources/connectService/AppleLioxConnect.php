<?php
namespace harvestModule\sources\connectService;

use AppleLioxProject;
use common\exceptions\AIException;
use common\exceptions\sources\apple_liox\WorldServerException;
use Functions;
use \harvestModule\sources\sourceComplements\WorldServerConnect;
require_once BaseDir . "/harvestModule/sources/sourceComplements/WorldServerConnect.php";
require_once BaseDir . "/harvestModule/sources/connectService/AppleLioxProject.php";

//require_once BaseDir . "/model/APIUser.php";
class AppleLioxConnect extends WorldServerConnect{


    /**
     * @param array $id_projects Project IDs that need to be downloaded
     * @return array
     * @throws WorldServerException When it cannot find the project id in world server
     * @throws AIException When the $id_projects is not an array
     */
    public function downloadProjects($id_projects){
        if(!is_array($id_projects)){
            throw new AIException("ID list must be an array");
        }
        $projects_info = array();
        $i = 0;
        foreach ($id_projects as $id){
            if(!$this->idProjectIsAvailable($id)){
                $ex = (new AIException("The project $id was not found in WS"))->construct(__METHOD__, __NAMESPACE__, $args = func_get_args(), array(
                    "id_ws" => $id
                ));
                Functions::logException($ex->getMessage(), Functions::WARNING, __CLASS__, $ex);
                continue;
            }
            //Si no se encuentra el proyecto se borra
            $apple_project = new AppleLioxProject($id, $this->access_token, $this->cookieFile, $this->user->getIdApiUser());
            $idWS = $id;
            $basic_info = $apple_project->getProjectBasicInfo();
            $extended_info = json_decode($apple_project->getProjectExtendedInfo(), true);


            $projects_info[$i]["idWS"] = $idWS;
            $projects_info[$i]["basic_info"]  = $basic_info;
            $projects_info[$i]["extended_info"]  =$extended_info;
            $projects_info[$i]["files"] = $apple_project->downloadFiles();
            $projects_info[$i]["wwc"]  = $apple_project->getWordCount();
            $i++;
        }

        return $projects_info;
    }

    public function getProjectInfo($id_projects){
        if(!is_array($id_projects)){
            throw new AIException("ID list must be an array");
        }
        $projects_info = array();
        $i = 0;
        foreach ($id_projects as $id){
            if(!$this->idProjectIsAvailable($id)){
                $ex = (new AIException("The project $id was not found in WS"))->construct(__METHOD__, __NAMESPACE__, $args = func_get_args(), array(
                    "id_ws" => $id
                ));
                Functions::logException($ex->getMessage(), Functions::WARNING, __CLASS__, $ex);
                continue;
            }
            //Si no se encuentra el proyecto se borra
            $apple_project = new AppleLioxProject($id, $this->access_token, $this->cookieFile, $this->user->getIdApiUser());
            $idWS = $id;
            $basic_info = $apple_project->getProjectBasicInfo();
            $extended_info = json_decode($apple_project->getProjectExtendedInfo(), true);


            $projects_info[$i]["idWS"] = $idWS;
            $projects_info[$i]["basic_info"]  = $basic_info;
            $projects_info[$i]["extended_info"]  =$extended_info;
            $projects_info[$i]["files"] = [
                "REFERENCE" => [],
            ];
            $projects_info[$i]["wwc"]  = $apple_project->getWordCount();
            $i++;
        }

        return $projects_info;
    }
}