<?php

/**
* @author Ljimenez
* @version 1.0
* created on 6 jul. 2018
*/
namespace harvestModule\sources\connectService;

use harvestModule\sources\Welocalize;

include_once BaseDir. '/harvestModule/sources/Welocalize.php';
include_once BaseDir. '/harvestModule/sources/connectService/FalconSoapHeader.php';

ini_set("default_socket_timeout", '30');

class AuthenticationWelocalize {
	
	private const STR_USER = 'IDISC_POC.for.Google.Google.projects';
	private const STR_PASSWORD = 'bB_0St6s';
	
	private $strUser;
	private $strPassword;
	private $token;
	private $strServer;
	
	/**
	 * @return string
	 */
	public function getStrUser() {
		return $this->strUser;
	}

	/**
	 * @return string
	 */
	public function getStrPassword() {
		return $this->strPassword;
	}

	/**
	 * @return mixed
	 */
	public function getToken() {
		return $this->token;
	}

	/**
	 * @return string
	 */
	public function getStrServer() {
		return $this->strServer;
	}

	/**
	 * @param string $strUser
	 */
	public function setStrUser($strUser) {
		$this->strUser = $strUser;
	}

	/**
	 * @param string
	 */
	public function setStrPassword($strPassword) {
		$this->strPassword = $strPassword;
	}

	/**
	 * @param mixed $token
	 */
	public function setToken($token) {
		$this->token = $token;
	}

	/**
	 * @param string $strServer
	 */
	public function setStrServer($strServer) {
		$this->strServer = $strServer;
	}

	public function __construct($userName, $userPassword){
		$this->strUser = ($userName==null? AuthenticationWelocalize::STR_USER:$userName);
		$this->strPassword = ($userPassword==null? AuthenticationWelocalize::STR_PASSWORD:$userPassword);
		$this->strServer = Welocalize::STR_SERVER;
		$oHeader = new FalconSoapHeader('testUser', 'testPassword');
		try{
			$oClient = new \SoapClient($this->strServer."/Vendor/Authentication/Authentication.svc?wsdl",array("connection_timeout" => WelocalizeConnect::TIMEOUT));
			$oClient->__setSoapHeaders(array($oHeader));
			$oToken = $oClient->GetAuthenticationToken(array('loginId'=>$this->strUser, 'emailAddress'=>$this->strPassword,'userType' =>'Vendor'));
			$this->token = $oToken->GetAuthenticationTokenResult;
		}
		catch (\Exception $e){
			throw new \Exception ($e->getMessage());
		}
	}
}
?>