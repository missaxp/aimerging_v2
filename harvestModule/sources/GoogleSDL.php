<?php


namespace harvestModule\sources;
use common\exceptions\AIException;
use common\exceptions\ParseDateException;
use core\AI;
use DateTime;
use Exception;
use Functions;
use harvestModule\sources\connectService\GoogleSDLMail;
use harvestModule\sources\sourceComplements\WorldServerConnect;
use model\Analysis;
use model\AvailableTask;
use model\DataSource;
use model\Source;
use model\State;
use model\Task;
use model\TaskAditionalProperties;
use TaskError;
use Throwable;

include_once BaseDir . '/dataAccess/dao/LanguageDAO.php';
include_once BaseDir . '/harvestModule/sources/StructureSource.php';
include_once BaseDir . '/harvestModule/sources/connectService/AppleLioxMail.php';
include_once BaseDir . '/harvestModule/sources/sourceComplements/WorldServerConnect.php';
include_once BaseDir . '/model/Task.php';
include_once BaseDir . '/Functions.php';
include_once BaseDir . '/common/ZipFile.php';
include_once BaseDir . '/harvestModule/sources/sourceComplements/LTBDownloader.php';
include_once BaseDir . "/common/exceptions/sources/LTBDownloaderException.php";
include_once BaseDir . "/common/exceptions/AIException.php";
include_once BaseDir . "/common/exceptions/ParseDateException.php";
include_once BaseDir . '/dataAccess/dao/TaskDAO.php';
include_once BaseDir . "/harvestModule/sources/connectService/GoogleSDLMail.php";

abstract class GoogleSDLAdditionalProperties {
    const CAT_TOOL = "cat_tool";
    const SERVICE_TYPE = "service_type";
    const PRODUCT = "product";
    const COMPONENT = "component";
}
class GoogleSDL extends StructureSource {


    /**
     * @var String Mails path
     */
    public static $googlesdl_aps_mail_path;

    /**
     * Type of harvestMethod to define in the task creation
     */
    private const HARVESMETHOD = 'Email';
    /**
     * @var String path to save the mails that where parsed correctly
     */
    private static $mail_success_directory;
    /**
     * @var String path to save the mails that where parsed incorrectly
     */
    private static $mail_damaged_directory;
    /**
     * Define how many times a mail must be proccess to be moved to damaged
     */
    private const TIMES_TO_REPROCCESS_MAIL = 5;

    public function __construct(array $users, Source $source) {
        parent::__construct($users, $source);

        self::$googlesdl_aps_mail_path= $this->development ? '/webs/ai/google_mails/' : '/ai/sources/google_sdl/';
        self::$mail_success_directory= $this->development ? '/webs/ai/google_mails/processed/' : '/ai/sources/google_sdl/processed/';
        self::$mail_damaged_directory = $this->development ? '/webs/ai/google_mails/damaged/' : '/ai/sources/google_sdl/damaged/';
    }

    /**
     * @return AvailableTask[]
     */
    public function getAvailableTask(): array{
        $incoming_mails = glob(self::$googlesdl_aps_mail_path . "*.msg");
        foreach ($incoming_mails as $mail) {
            $googleMail = new GoogleSDLMail($mail);
            try {
                $googleMailData = $googleMail->processMail();

                $task = new AvailableTask();
                $deadline = $this->parseDeadline($googleMailData["deadline"]);

                $sourceLanguage = parent::identifySourceLanguage($googleMailData["source_lang"]);
                $targets = parent::identifyTargetLanguage(array($googleMailData["target_lang"]));
                if(!empty($targets)){
                    $targets[0]->setAnalysis($this->createAnalysis($googleMailData));
                }

                $additional_prop_raw_data = array(
                    GoogleSDLAdditionalProperties::CAT_TOOL => $googleMailData["cat_tool"],
                    GoogleSDLAdditionalProperties::COMPONENT => $googleMailData["component"],
                    GoogleSDLAdditionalProperties::PRODUCT=> $googleMailData["product"],
                    GoogleSDLAdditionalProperties::SERVICE_TYPE => $googleMailData["task"]
                );


                $task
                    ->setTitle($googleMailData["subject"])
                    ->setIdClientTask($googleMailData["project"])
                    ->setMessage($googleMailData["instructions"])
                    ->setDueDate($deadline->format("Y-m-d H:i:s"))
                    ->setSourceLanguage($sourceLanguage)
                    ->setTargetsLanguage($targets)
                    ->setAdditionalProperties($this->collectProperties(null, $additional_prop_raw_data));

                $this->availableTasks[] = $task;
            } catch (AIException $e) {
                echo $e->getMessage();
            }
        }
        return $this->availableTasks;
    }

    public function collectTask(){
        $incoming_mails = glob(self::$googlesdl_aps_mail_path . "*.msg");
        foreach ($incoming_mails as $mail) {
            $googleMail = new GoogleSDLMail($mail);
            try {
                $googleMailData = $googleMail->processMail();

                $task = new Task();
                $deadline = $this->parseDeadline($googleMailData["deadline"]);
                $task->construct(
                    "",//Functions::currentDateAsObject(), //seteado mas adelante
                    $googleMailData["project"],
                    $googleMailData["subject"],
                    $googleMailData["instructions"],
                    Functions::currentDateAsObject(),
                    $deadline,
                    ""
                );
                $dataSource = new DataSource();
                $dataSource->construct(
                    $this->source->getSourceName(),
                    AI::getInstance()->getSetup()->execution_type,
                    Functions::currentDate(),
                    self::HARVESMETHOD,
                    "",
                    "",
                    file_get_contents($mail),
                    $this->source->getIdSource(),
                    $this->users[0]->getIdApiUser()
                );
                //Removing he original data on development to improve reading process
                if(AI::getInstance()->getSetup()->development){
                    $dataSource->setOriginalData("");
                }

                $task->setDataSource($dataSource);

                $sourceLanguage = parent::identifySourceLanguage($googleMailData["source_lang"]);

                $task->setSourceLanguage($sourceLanguage);

                $targets = parent::identifyTargetLanguage(array($googleMailData["target_lang"]));
                if(!empty($targets)){
                    $targets[0]->setAnalysis($this->createAnalysis($googleMailData));
                } else {
                    $aiex = (new AIException("Cannot identify target language"))->construct(__METHOD__, __NAMESPACE__, $f = func_get_args(), array(
                        "mail_data" => $googleMailData,
                        "mail" => $mail
                    ));
                    Functions::logException($aiex, Functions::WARNING, __CLASS__, $aiex);
                    $this->markMailAsDamage($mail);
                    continue;
                }

                $task->setTargetsLanguage($targets);


                $task->setState(new State(State::COLLECTED));

                $additional_prop_raw_data = array(
                    GoogleSDLAdditionalProperties::CAT_TOOL => $googleMailData["cat_tool"],
                    GoogleSDLAdditionalProperties::COMPONENT => $googleMailData["component"],
                    GoogleSDLAdditionalProperties::PRODUCT=> $googleMailData["product"],
                    GoogleSDLAdditionalProperties::SERVICE_TYPE => $googleMailData["task"]
                );

                $task->setAditionalProperties($this->collectProperties(null, $additional_prop_raw_data));


                $task->generateUniqueTaskId();
                $task->setStartDate(Functions::currentDateAsObject());
                $task->setTimeStamp(Functions::currentDate());
                $task->generateUniqueSourceId();


                $this->markMailAsProcessed($mail);
                $this->newTasks[] = $task;
            } catch (AIException $e) {
                echo $e->getMessage();
            }
        }

        parent::collectTask();
        return (count($this->newTasks) > 0);
    }

    /**
     * @param $deadline
     * @return String
     */
    private function parseDeadline($deadline){
        $def_deadline = $deadline;
        $deadline = str_replace("[" , "", $deadline);
        $deadline = str_replace("]" , "", $deadline);
        try{
            $deadlineDateTime = new DateTime($deadline);
            return $deadlineDateTime;//->format("Y/m/d H:i:s");
        } catch(Throwable $th){
            error_clear_last();
            $ex = (new ParseDateException($th->getMessage()))->construct(__METHOD__, __NAMESPACE__, $f = func_get_args(), array("deadline_string" => $def_deadline));
            Functions::logException("Cannot parse deadline, using today EOB" . $ex->getMessage(), Functions::WARNING, __CLASS__, $ex);
            return $this->getTodayEOBDate();
        }
    }

    private function getTodayEOBDate(){
        try{
            $date = Functions::createFormatDate("today 18:00", AI::getInstance()->getSetup()->timeZone_TMS);
            return $date;
        }catch (Throwable $e){
            return (new DateTime())->format("Y/m/d H:i:s");
        }
    }

    private function markMailAsDamage($mail){
        Functions::console("Marcando correo como damaged");
        $this->moveEmail($mail, self::$mail_damaged_directory);
    }

    private function markMailAsProcessed($mail) {
        Functions::console("Marcando correo como procesado");
        $this->moveEmail($mail, self::$mail_success_directory);
    }

    private function moveEmail($mail, $path){
        if(!AI::getInstance()->isDev()){
            $mailName = explode("/", $mail);
            $mailName = end($mailName );
            rename($mail, $path . $mailName);
        }
    }

    //private functionMoveMail($mail)

    private function createAnalysis(array $googleMailData){
        $analysis = new Analysis();
        $analysis->setWeightedWord($googleMailData["weighted_words"]);
        $analysis->setTotalWords($googleMailData["total_words"]);
        $analysis->setNotMatch(round($analysis->getWeightedWord(), 0, PHP_ROUND_HALF_UP));
        return $analysis;
    }

    /**
     * Create the AdditionalPropertie instances for every task
     * @param array $values
     * @param array $data
     * @return array|TaskAditionalProperties[]
     */
    protected function collectProperties($values, &$data) {
        $properties = array();
        foreach ($this->source->getAdditionalProperties() as $sourceProp) {
            try {
                if(isset($data[$sourceProp->getPropertyName()])){
                    $propiedad = $data[$sourceProp->getPropertyName()];
                    if($propiedad != null){
                        $value = (isset($data[$sourceProp->getPropertyName()]) ? $data[$sourceProp->getPropertyName()] : "-");
                        $tProp = new TaskAditionalProperties();
                        $tProp->setPropertyName($sourceProp->getPropertyName());
                        $tProp->setPropertyValue(trim($value));
                        $properties[] = $tProp;
                    }
                }
            } catch (Throwable $e) {
                $aiex = AIException::createInstanceFromThrowable($e)->construct(__METHOD__, __NAMESPACE__, $f = func_get_args());
                Functions::logException($e->getMessage(), Functions::WARNING, json_encode(__CLASS__), $aiex);
            }
        }
        return $properties;
    }



    public function collectTaskByIdClientTask($idTask)
    {
        $incoming_mails = glob(self::$googlesdl_aps_mail_path . "*.msg");
        $taskFound = false;
        $googleMailData = null;
        $task = null;
        foreach ($incoming_mails as $mail) {
            $googleMail = new GoogleSDLMail($mail);
            try {
                $googleMailData = $googleMail->processMail();
                if(isset($googleMailData["project"]) && $googleMailData["project"] === $idTask){
                    $taskFound = true;
                    break;
                }
            } catch (AIException $e) {
                echo $e->getMessage();
            }
        }

        if($taskFound){
            $task = $this->createTask($googleMailData);
            if($task === null){
                $aiex = (new AIException("Cannot identify target language"))->construct(__METHOD__, __NAMESPACE__, $f = func_get_args(), array(
                    "mail_data" => $googleMailData,
                    "mail" => $mail
                ));
                Functions::logException($aiex, Functions::WARNING, __CLASS__, $aiex);
                $this->markMailAsDamage($mail);
                throw new \Exception("This task does not found target language in mail");
            }

            $task->getDataSource()->setOriginalData(file_get_contents($mail));

            $task->generateUniqueTaskId();
            $task->setStartDate(Functions::currentDate());
            $task->setTimeStamp(Functions::currentDate());
            $task->generateUniqueSourceId();


            $this->markMailAsProcessed($mail);
            $this->newTasks[] = $task;
        }

        parent::collectTask();
        return (count($this->newTasks) > 0);
    }

    public function createTask($taskData, $tms = null,$user = null){
        $googleMailData = $taskData;
        $task = new Task();
        $deadline = $this->parseDeadline($googleMailData["deadline"]);
        $task->construct(
            "",//Functions::currentDate(), //seteado mas adelante
            $googleMailData["project"],
            $googleMailData["subject"],
            $googleMailData["instructions"],
            Functions::currentDate(),
            $deadline,
            ""
        );
        $dataSource = new DataSource();
        $dataSource->construct(
            $this->source->getSourceName(),
            "IN",
            Functions::currentDate(),
            self::HARVESMETHOD,
            "",
            "",
            "",
            $this->source->getIdSource(),
            $this->users[0]->getIdApiUser()
        );
        //Removing he original data on development to improve reading process
        if(AI::getInstance()->getSetup()->development){
            $dataSource->setOriginalData("");
        }

        $task->setDataSource($dataSource);

        $sourceLanguage = parent::identifySourceLanguage($googleMailData["source_lang"]);

        $task->setSourceLanguage($sourceLanguage);

        $targets = parent::identifyTargetLanguage(array($googleMailData["target_lang"]));
        if(!empty($targets)){
            $targets[0]->setAnalysis($this->createAnalysis($googleMailData));
        } else {
            return null;
        }

        $task->setTargetsLanguage($targets);


        $task->setState(new State(State::COLLECTED));

        $additional_prop_raw_data = array(
            GoogleSDLAdditionalProperties::CAT_TOOL => $googleMailData["cat_tool"],
            GoogleSDLAdditionalProperties::COMPONENT => $googleMailData["component"],
            GoogleSDLAdditionalProperties::PRODUCT=> $googleMailData["product"],
            GoogleSDLAdditionalProperties::SERVICE_TYPE => $googleMailData["task"]
        );

        $task->setAditionalProperties($this->collectProperties(null, $additional_prop_raw_data));

        return $task;
    }
}