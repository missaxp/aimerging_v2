<?php

namespace harvestModule\sources;

use model\APIUser;
use model\File;
use model\Source;
use model\Task;
use model\TaskAditionalProperties;
use dataAccess\dao\LanguageDAO;
use Functions;
use core\AI;
use model\Analysis;
use model\Language;

include_once BaseDir. '/model/File.php';
include_once BaseDir. '/model/Task.php';
include_once BaseDir. '/model/DataSource.php';
include_once BaseDir. '/model/TaskAditionalProperties.php';
include_once BaseDir. '/dataAccess/dao/LanguageDAO.php';
include_once BaseDir. '/model/Language.php';
include_once BaseDir. '/Functions.php';
include_once BaseDir. '/dataAccess/dao/TaskDAO.php';
include_once BaseDir. '/AI.php';

class StructureSource {

    /**
     * Determines the environment
     *
     * @var boolean
     */
    protected $development;

    /**
     * Tasks found
     *
     * @var Task[]
     */
    protected $newTasks;

    /**
     * Users of the source
     *
     * @var APIUser[]
     */
    protected $users;

    /**
     * Data  of source
     *
     * @var Source
     */
    protected $source;
    /**
     * @var AvailableTask[] List of available tasks to harvest
     */
    protected $availableTasks;

    public function __construct(array $users = array(), Source $source = null){
        $this->users = $users;
        $this->source = $source;
        $this->newTasks = array();
        $this->availableTasks = array();
        $this->development = AI::getInstance()->isDev();
    }

    /**
     * Return the environment
     *
     * @return boolean
     */
    protected function getDevelopment(){
        return $this->development;
    }

    /**
     * Rejectes a task.
     *
     * @param Task $task
     * @return boolean
     */
    public function rejectTask(Task $task){
        return true;
    }

    /**
     * Search tasks in API's or emails of sources and save the tasks in the storage system.
     *
     * @return bool return true if tasks were found or false if tasks were not found
     */
    public function collectTask(){
        $aux = array();
        foreach ($this->newTasks as $task){
            if($task->taskCheck() && !AI::getInstance()->getBehavior()->checkIfTaskExist($task/*->getUniqueTaskId()*/)){
                $aux[] = $task;
            }
        }
        $this->newTasks = $aux;
    }

    /**
     * Returns tasks was found
     *
     * @return \model\Task[]
     */
    public function getTasks(){
        return $this->newTasks;
    }

    /**
     * Accept the task in the tms, api or mail to the client
     *
     * @param Task $task task to accept
     * @return boolean
     */
    public function acceptTask(Task $task){
        return true;
    }

    /**
     * Writes data on physical disk
     *
     * @param File $file
     * @param string $pathForDownload path where it will be write.
     * @param mixed $data bytes or data download of platform
     * @return boolean
     */
    protected function saveDownload(File &$file, string $pathForDownload, &$data){
        if(!file_exists($pathForDownload)){
            try{
                mkdir($pathForDownload,0760,true);
            }catch (\Exception $e){
                $this->addLog("Directory creation => ".$e->getMessage(), \Functions::WARNING,json_encode($e->getTrace()));
            }
        }

        $newFile = fopen($pathForDownload.$file->getFileName(),"w+");
        if($newFile !== false){
            fwrite($newFile, $data);
            fclose($newFile);
        }

        $filePath = $pathForDownload.$file->getFileName();

        return (file_exists($filePath) && filesize($filePath)>0);
    }

    /**
     * Download data from the network
     *
     * @param File $file
     * @param string $pathForDownload
     * @return boolean true if download was successfull
     */
    public function downloadFile(File &$file, Task &$task = null) {
        return true;
    }

    /**
     * Create path where files will be save.
     *
     * @param string $idTask
     * @param string $sourceName
     * @param string $uniqueSourceId
     * @return string
     */
    protected function generatePathForDownload(string $idTask, string $sourceName, string $uniqueSourceId){
        return FILEPATH.$sourceName.'/'.date("Ym").'/'.$idTask.'-'.$uniqueSourceId.'/';
    }

    /**
     * Searches to properties in data that was received.
     *
     * @param array $values properties name
     * @param array $data data received
     * @return \model\TaskAditionalProperties[]
     */
    protected function collectProperties($values, &$data){
        $properties = array();
        foreach ($this->source->getAdditionalProperties() as $value){
            $propertyValue = null;


            //Si la propiedad está contenida dentro de un array, hay que acceder al valor en concreto.
            //La propiedad adicional está definida para acceder al valor del espacio en concreto.
            //Ejemplo: Project.Name
            //Si es el espacio root, se asigna la propiedad directamente.
            $childNS = explode(".", $value->getPropertyName());

            $currentNSArr = $data;
            for($i=0,$j=count($childNS);$i<$j;$i++){
                $child = $childNS[$i];
                if(isset($currentNSArr[$child])){
                    if($i+1 == $j){
                        $propertyValue = $currentNSArr[$child];
                        break;
                    }
                    else{
                        $currentNSArr = $currentNSArr[$child];
                    }
                }
            }

            if($propertyValue!=null){
                $property = new TaskAditionalProperties();
                $property->setPropertyName($value->getPropertyName());
                $property->setPropertyValue($propertyValue);
                $properties[] = $property;
            }


        }
        return $properties;
    }

    /**
     * Get word count of task
     *
     * @param array $wordCount
     * @return Analysis
     */
    protected function generateAnalsis(&$wordCount){

    }

    /**
     * Called the method that saves the data in the log
     *
     * @param string $information message to save on log
     * @param string $level level of error or information.
     * @param string $originClass class name where it arose.
     */
    protected function addLog(string $information, string $level,string $originClass){
        Functions::addLog($information, $level,$originClass);
    }

    /**
     * Identify the Language
     *
     * @param string $idLanguage
     * @return \model\Language
     */
    protected function identifySourceLanguage(string $idLanguage){
        $languageDAO = new LanguageDAO();
        $language = $languageDAO->getLanguage(Language::filterLanguage($idLanguage));
        if($language == null){
            $this->addLog('identifier not match => '.$idLanguage, \Functions::WARNING,"File => ".__FILE__."    Method => ".__METHOD__."    Line => ".__LINE__);
        }

        return $language;
    }

    /**
     * Identify the Language
     *
     * @param array $idlanguages
     * @return \model\Language[]
     */
    protected function identifyTargetLanguage(array $idlanguages){
        $languageDAO = new LanguageDAO();
        $targets = array();
        foreach ($idlanguages as $idLanguage){
            $language = $languageDAO->getLanguage(Language::filterLanguage($idLanguage));
            if($language != null)
                $targets[] = $language;
            else
                $this->addLog('identifier not match => '.$idLanguage, \Functions::WARNING,"File => ".__FILE__."    Method => ".__METHOD__."    Line => ".__LINE__);
        }

        return $targets;
    }

    /**
     * @param $task Task
     * @return bool
     */
    public function structureTask($task){
        return true;
    }

    /**
     * @return AvailableTask[]
     */
    public function getAvailableTask(){
        return $this->availableTasks;
    }

    public function collectTaskByIdClientTask($idTask){
        return $this->newTasks;
    }

    /**
     * @param array $taskData
     * @param mixed $tms
     * @param APIUser $user
     * @return bool|Task
     * @throws \Exception
     */
    protected function createTask($taskData,$tms = null,$user = null){
        return true;
    }
}
?>