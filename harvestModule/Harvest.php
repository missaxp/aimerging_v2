<?php

/**
 * @author Ljimenez
 * @version 1.0
 * created on 21 jun. 2018
 */
namespace harvestModule;

use common\exceptions\AIException;
use core\AI;
use dataAccess\dao\TaskDAO;
use Exception;
use Functions;
use harvestModule\sources\AppleLiox;
use harvestModule\sources\AppleWelocalize;
use model\Source;
use dataAccess\dao\SourceDAO;
use model\State;
use model\Task;
use model\APIUser;
use harvestModule\sources\StructureSource;
use model\File;
use harvestModule\sources\Jira;
use harvestModule\sources\Moravia_V1;
use harvestModule\sources\Moravia_V2;
use harvestModule\sources\NLG;
use harvestModule\sources\Welocalize;
use Throwable;
use harvestModule\sources\SymfonieMoravia;

include_once BaseDir. '/dataAccess/dao/SourceDAO.php';
include_once BaseDir. '/harvestModule/sources/Jira.php';
include_once BaseDir. '/harvestModule/sources/Moravia_V1.php';
include_once BaseDir. '/model/Source.php';
include_once BaseDir. '/harvestModule/sources/Moravia_V2.php';
include_once BaseDir. '/model/Task.php';
include_once BaseDir. '/harvestModule/sources/Welocalize.php';
include_once BaseDir. '/harvestModule/sources/NLG.php';
include_once BaseDir. '/harvestModule/sources/AppleLiox.php';
include_once BaseDir. '/harvestModule/sources/AppleWelocalize.php';
include_once BaseDir. '/harvestModule/sources/SymfonieMoravia.php';

include_once BaseDir. '/Functions.php';
require_once BaseDir . "/common/exceptions/AIException.php";


class Harvest {

    /**
     * Task collected of Sources
     *
     * @var Task[]
     */
    private $tasks;

    /**
     * @var Task[]
     */
    private $tasksToReprocesses;

    public const API = 'API';
    public const EMAIL = 'Email';
    public const TMS = 'TMS';

    public function __construct(){
        $this->tasks = array();
        $this->tasksToReprocesses = array();
    }

    /**
     * Returns collected tasks
     *
     * @return Task[] tasks found. empty array if neither task was found
     */
    public function getTasks(){
        return $this->tasks;
    }

    public function getTasksToReprocess(){
        return $this->tasksToReprocesses;
    }

    /**
     * Searches tasks in sources.
     *
     * @var Source[] $sources
     * @return boolean
     */
    public function startSearch(array $sources){
        Functions::console("AI: Start Processing Sources");
        $sourceDAO = new SourceDAO();

        /**@var Source $source*/
        foreach ($sources as $source){
            Functions::console("HM::".$source->getSourceName()." - Start collecting");
            $users = $sourceDAO->getUsersSources($source->getIdSource());
            $sourceInstance = null;
            try{
                $sourceInstance = $this->createObjectSource($source,$users);
            } catch(Throwable $e){
                Functions::console("HM::".$source->getSourceName()." - Impossible to get instance file not found");
            }
            if($sourceInstance != null){
                try{
                    $sourceDAO->saveStartExecution($source->getIdSource(), Functions::currentDate());
                    $result = $sourceInstance->collectTask();
                } catch (AIException $e){
                    Functions::logException($e->getMessage(), Functions::SEVERE, __class__, $e);
                    $sourceDAO->saveEndExecution($source->getIdSource(), Functions::currentDate());
                    continue;
                }
                catch (Throwable $th){
                    Functions::logException($th->getMessage(), Functions::ERROR, __class__, AIException::createInstanceFromThrowable($th)->construct(__METHOD__, __NAMESPACE__, $f = func_get_args(), array(
                        "current_source" => $source->getSourceName()
                    )));
                    Functions::console("HM::" ."ERROR:" . $th->getMessage() . " ON SOURCE " . $source->getSourceName() . ". Processing next source");
                    $sourceDAO->saveEndExecution($source->getIdSource(), Functions::currentDate());
                    continue;
                }
                if($result){
                    $result = $sourceInstance->getTasks();
                    $this->tasks = array_merge($this->tasks,$result);
                    Functions::console("HM::".$source->getSourceName()." - Found ".count($this->tasks)." tasks");
                }
                else{
                    Functions::console("HM::".$source->getSourceName()." - No tasks has been found");
                }

                $sourceDAO->saveLastExecution($source->getIdSource(), Functions::currentDate());
                $sourceDAO->saveEndExecution($source->getIdSource(), Functions::currentDate());
            } else {
                $aiex = (new AIException("Cannot create source instance " . $source->getSourceName()))->construct(__METHOD__, __NAMESPACE__, $f = func_get_args(), ["Source" => $source]);
                Functions::logException($aiex ->getMessage(), Functions::SEVERE, __class__, $aiex);
            }
        }
        $tasksCollected = $this->checkCollectingStatus();

        $this->tasks = array_merge($this->tasks,$tasksCollected);

        return count($this->tasks) > 0;
    }

    /**
     * Validate if file task was download successful.
     *
     * @param $task task to process
     * @param $file file to validate
     *
     * @return boolean
     */
    public function processFiles(Task &$task,File $file) {
        $result = false;
        $taskDao = new TaskDAO();
        $sourceDAO = new SourceDAO();
        if($file->getAsyncHash() != null){
            $fileDownloading = $taskDao->getTaskFileAsyncDownload($file->getAsyncHash());
            if (!empty($fileDownloading) && $fileDownloading['isDownloaded']){
                $file->setPath($fileDownloading['filePath']);
                if($file->validateDownload()){
                    $file->setChecksum(md5_file($file->getPath()));
                    $file->setSize(filesize($file->getPath()));
                    $file->setDescription("File was download successful");
                    $file->setSuccessfulDownload(true);
                    $result = $taskDao->updateFile($file);
                }else{
                    $taskDao->saveDownloadFileFail($task);
                    try{
                        $ai = AI::getInstance();
                    } catch(Throwable $e){
                        return null;
                    }
                    $ai->getHarvest()->rebootDownload($file, $sourceDAO->getSourceByName($task->getDataSource()->getSourceName()), $sourceDAO->getApiUser($task->getAssignedUser()),$task);
                }
            }else{
                if (empty($fileDownloading) || !$fileDownloading['isDownloading']){
                    $taskDao->saveDownloadFileFail($task);
                    try{
                        $ai = AI::getInstance();
                    } catch(Throwable $e){
                        return null;
                    }
                    $ai->getHarvest()->rebootDownload($file, $sourceDAO->getSourceByName($task->getDataSource()->getSourceName()), $sourceDAO->getApiUser($task->getAssignedUser()),$task);
                }
            }
        }

        return $result;
    }

    public function checkCollectingStatus(){
        $taskDao = new TaskDAO();
        Functions::console('HM::Start Processing COLLECTING tasks');
        $tasks = $taskDao->getTasksByState(State::COLLECTING);
        $tasksCollected  = array();
        foreach ($tasks as &$task){
            $count = 0;
            $totalFile = count($task->getAttachments());
            foreach ($task->getAttachments() as $file){
                $result = $this->processFiles($task, $file);
                if($result == false){
                    break;
                }
                $count++;
            }
            if($count == $totalFile){
                if($task->getDataSource()->getSourceName() == Source::NLG){
//                    $task->compressFiles(File::REFERENCE);
                    $sourceDAO = new SourceDAO();
                    $source = $sourceDAO->getSourceByName(Source::NLG);
                    $instance = Harvest::createObjectSource($source,array());
                    $instance->structureTask($task);
                }
                $taskDao->updateTaskState($task,State::COLLECTED,"Task recollected");
                $task->setState(new State(State::COLLECTED));
                $tasksCollected[]=$task;
            }
        }
        Functions::console("HM::End Processing COLLECTING tasks");
        unset($tasks);
        return $tasksCollected;
    }

    /**
     * Instances a object of StructureSource by source name
     *
     * @param Source $source
     * @param APIUser[] $users
     * @return StructureSource
     */
    public static function createObjectSource(Source $source, array $users){
        $instance = null;
        /*switch ($source->getSourceName()){
            // case Source::SYMFONIEMORAVIA:
            // 	$instance = null;
            // 	try {
            // 		$instance = new SymfonieMoravia($users, $source);
            // 	}
            // 	catch(Throwable $e){
            // 		Functions::console("HM:Error::".$source->getSourceName()." " . $e->getMessage());
            // 	}
            // 	return $instance;

            // case Source::MORAVIA_V1:
            // 	return new Moravia_V1($users,$source);
            // 	break;
            // case Source::MORAVIA_V2:
            //     return new Moravia_V2($users, $source);
            // 	break;
            // case Source::JIRA:
            //     return new Jira($users, $source);
            // 	break;
            // case Source::WELOCALIZE:
            //     return new Welocalize($users,$source);
            // 	break;
            // case Source::NLG:
            //    return new NLG($users, $source);
            // case Source::APPLE_LIOX_WS:
            //     $instance = null;
            //     try{
            //         $instance = new AppleLiox($users, $source);
            //     } catch (Exception $e){
            //         Functions::console("HM:Error::".$source->getSourceName()." " . $e->getMessage());
            //     }
            // 	return $instance;
            case Source::APPLE_WELOCALIZE_WS:
                $instance = null;
                try{
                    $instance = new AppleWelocalize($users, $source);
                } catch (Exception $e){
                    Functions::console("HM:Error::".$source->getSourceName()." " . $e->getMessage());
                }
                return $instance;
            default:
                return null;
                break;
        }*/
        $class = "harvestModule\\sources\\".$source->getClassName();
        $classPath = "/harvestModule/sources/".$source->getClassName().".php";
        if(!class_exists($class)){
            if(!file_exists(BaseDir.$classPath)) {
                return null;
            }
            require_once BaseDir.$classPath;
        }
        $instance = new $class($users, $source);

        return $instance;
    }

    /**
     * Accept a task according to the source that belongs
     *
     * @param Task $task task to accept
     * @return bool true if action was successfull or false if action fails
     * @throws Exception
     */
    public static function acceptTask(Task $task){
        $sourceDAO = new SourceDAO();
        $sourceData = $sourceDAO->getSourceByName($task->getDataSource()->getSourceName());
        $user = $sourceDAO->getApiUser(($task->getDataSource()->getTmsUserCollector()!=null)?$task->getDataSource()->getTmsUserCollector():0);
        $users = isset($user)?array($user):array();
        if(count($users) > 0){
            $source = Harvest::createObjectSource($sourceData, $users);
            if($source == null){
                return false;
            }
            $successfull = $source->acceptTask($task);
        }else{
            $successfull = false;
        }

        return $successfull;
    }

    /**
     * Execute Source's code that ondemand process
     *
     * @param string $sourceName
     * @return boolean true if any task was found or false if else.
     * @throws Exception
     */
    public function ondemandSource(string $sourceName,$idTask = null){
        $sourceDAO = new SourceDAO();
        $taskDAO = new TaskDAO();
        $source = $sourceDAO->getSourceByName($sourceName);
        $users = $sourceDAO->getUsersSources($source->getIdSource());
        $sourceInstance = $this->createObjectSource($source, $users);
        if($sourceInstance != null){
            if($idTask === null){
                $result = $sourceInstance->collectTask();
                if($result){
                    $result = $sourceInstance->getTasks();
                    $this->tasks = array_merge($this->tasks,$result);
                }
            }else{
                foreach ($idTask as $id) {
                    if ($sourceInstance->collectTaskByIdClientTask($id)) {
                        $result = $sourceInstance->getTasks();
                        if (!empty($result)) {
                            $result[0]->getState()->setDescription("task recollected by " . AI::getInstance()->getBehavior()->getOndemandConfiguration()["link"]);
                        }
                        $this->tasks = array_merge($this->tasks, $result);
                    } else {
                        $task = $taskDAO->getTaskByClientId($sourceName, $id);
                        if ($task !== null && $task->getState()->getIdState() === State::REJECTED) {
                            $taskDAO->updateTaskState($task, State::COLLECTED, "Task recollected by " . AI::getInstance()->getBehavior()->getOndemandConfiguration()["link"]);
                            $state = new State(State::COLLECTED);
                            $state->setDescription("Task recollected by " . AI::getInstance()->getBehavior()->getOndemandConfiguration()["link"]);
                            $task->setState($state);
                            $this->tasksToReprocesses[] = $task;
                        }
                    }
                }
            }

            $sourceDAO->saveLastExecution($source->getIdSource(), Functions::currentDate());
        }
        return (count($this->tasks) > 0 || count($this->tasksToReprocesses) > 0);
    }

    /**
     * Reboot download file.
     *
     * @param File $file File to download
     * @param Source $source Source where file was recollected
     * @param APIUser $user User that recollected task
     * @param Task $task
     * @return boolean true if action was successful
     */
    public function rebootDownload(File &$file, Source $source, APIUser $user, Task &$task) {
        $sourceInstance = $this->createObjectSource($source, array($user));
        $result = false;
        if($sourceInstance != null){
            $result = $sourceInstance->downloadFile($file, $task);
        }

        return $result;
    }
}
?>