<?php

/**
 *
 * @author Ljimenez
 * @version 1.0
 * created on 21 jun. 2018
 */
namespace dataAccess;

use core\AI;

include_once BaseDir. '/dataAccess/dbConn.php';

class ConnectionDB {
	private static $dbconn = null;
	public const LASTEXECUTION = 'ExecutionLog';
	public const TASK = 'Task';
	public const ANALYSIS = 'TaskWordCounts';
	public const FILE = 'TaskFiles';
	public const LANGUAGE = 'Language';
	public const TARGETLANGUAGE = 'TaskTargetLanguages';
	public const SOURCELANGUAGE = 'SourceLanguage';
	public const PROCESS = 'Process';
	public const ERRORLOG = 'ErrorLog';
	public const LOGCONFIGURATION = 'LogConfiguration';
	public const SOURCE = 'Source';
	public const ACTIONCONFIRMEMAIL = 'ActionConfirmEmail';
	public const ACTIONCREATE = 'ActionCreate';
    public const ACTION_CREATE_TMS_RESPONSE = "ActionCreateTMSResponse";
	public const ACTIONCREATEADDITIONALPROPERTIES = 'ActionCreateAdditionalProperties';
	public const ACTIONCREATECUSTOMFUZZYRELATION = 'ActionCreateCustomFuzzyRelation';
	public const ACTIONCREATECUSTOMFILERELATION = 'ActionCreateCustomFileRelation';
	public const CONFIRMATIONTOKEN = 'ConfirmationToken';
	public const ACTIONEMAIL = 'ActionEmail';
	public const TASKSOURCE = 'TaskSource';
	public const PROCESSRULES = 'ProcessRules';
	public const PROCESSNOTIFICATIONS = 'ProcessNotifications';
	public const TASKFILESFORREFERENCE = 'TaskFilesForReference';
	public const TASKFILESTOTRANSLATE = 'TaskFilesToTranslate';
	public const TASKCHANGEHISTORY = 'TaskChangeHistory';
	public const ACTIONCREATEWCOVERWRITE = 'ActionCreateWCOverwrite';
	public const ACTIONCONFIRMEMAILFEEDBACK = 'ActionConfirmEmailFeedback';
	public const TASKSTATES = 'TaskStates';
	public const PROCESSSCHEDULES = 'ProcessSchedules';
	public const TASKFILESTM = 'TaskFilesTM';
	public const TMTARGETLANGUAGES = 'TaskFilesTMTargetLanguages';
	public const PREPROCESS = "Preprocess";
	public const PREPROCESSRULES = "PreprocessRules";
	public const TASKFILEASYNCDOWNLOAD = "TaskFileAsyncDownload";
	public const CATTOOLS = "CATTools";
	public const MEMOQTOOLSETUP = "MemoqToolSetup";
	public const CONFIGURATION = "Configuration";
	public const GLOBALWORKLOAD = "GlobalWorkLoad";

	public function __construct() {
	}
	
	/**
	 * Generates an instance of the dbConn class and return a dbConn object unique for all execution system
	 * 
	 * @return dbConn
	 */
	public function getConnection() {
		if(self::$dbconn==null){
			self::$dbconn = new dbConn (DBCONN_MYSQL, AI::getInstance()->getSetup()->db_user, AI::getInstance()->getSetup()->db_passwd, AI::getInstance()->getSetup()->db_host, AI::getInstance()->getSetup()->db_scheme);
		}
		return self::$dbconn;
	}
	
	/**
	 * Closes the connection to the database
	 * 
	 */
	public function closeConnection(){
		if(self::$dbconn != null){
			self::$dbconn->close();
		}
	}
}
?>