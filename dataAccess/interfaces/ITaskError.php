<?php
interface ITaskError{
    public function getErrorsByUniqueSourceId(string $uniqueSourceId);
    public function saveErrors(array $errors);
}