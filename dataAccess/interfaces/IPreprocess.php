<?php

namespace dataAccess\interfaces;

use model\Preprocess;
use model\PreprocessRule;

interface IPreprocess {
	
	public function savePreprocess();
	
	/***
	 * Gets all the enabled preprocesses for a given id source
	 * @param int $idSource
	 * @return Preprocess[]
	 */
	public function getPreprocessBySource(int $idSource);
	
	
	
	
}

