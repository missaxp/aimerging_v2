<?php

namespace dataAccess\interfaces;

interface IAI {
	
	/**
	 * Saves the start of a system execution on the storage media, the key of the execution will be generated as a md5 hash of the 
	 * startDate and the type of execution (Ondemand or Inqueue).
	 * @param string $startDate The date of execution in the following format: Y/m/d h:i:s 
	 * @param string $type
	 * @return string identifier The generated md5 hash
	 */
	public function saveExecution(string $startDate,string $type);
	
	/**
	 * Updates a system execution on the storage media, this method should be called at the end of the execution to save
	 * the date of the end and the status of the execution.
	 * 
	 * @param string $idExecution 
	 * @param string $endDate
	 * @return boolean
	 */
	public function saveEndExecution(string $idExecution,string $endDate, string $status);
	
	/**
	 * Saves a log record on the storage media, the method should be provided with the type of error and the description of the error
	 * 
	 * @param mixed $type
	 * @param string $error
	 * @return boolean
	 */
	public function saveError(string $type,string $error,string $class);
	
	/**
	 * Validate if the system is running.
	 * 
	 * @param string $executionType type execution INQUEUE or ONDEMAND
	 * @return bool true if system is execution or false else.
	 */
	public function isExecuting(string $executionType);
	
	/**
	 * Get configuration of data for send the log
	 * 
	 * @return array
	 */
	public function getConfigurationLog();
}
?>