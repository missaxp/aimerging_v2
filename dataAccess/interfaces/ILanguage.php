<?php
/**
* @author zroque
* @version 1.0
* created on 21 jun. 2018
*/
namespace dataAccess\interfaces;

use model\Language;

interface ILanguaje{
	/**
	 * Saves a language into the storage media
	 * @param Language $language A language object with all its properties initialized
	 * @return boolean True if the operation succeded, false otherwise
	 */
	public function saveLanguage(Language $language);
	
	/**
	 * Deletes a language from the storage media
	 * @param int $idLanguage the id of the language to be deleted
	 * @return boolean True if the operation succedes, false otherwise
	 */
	public function deleteLanguage(int $idLanguage);
	
	/**
	 * Updates a language on the storage media 
	 * @param Language $language A Language object with all its properties initialized
	 * @return boolean True if the operation succeded, false otherwise
	 */
	public function updateLanguage(Language $language);
	
	/**
	 * Gets the language asociated with the id provided
	 * @param string $idLanguage The id of the language
	 * @return Language A Language object if the id is found, null otherwise
	 */
	public function getLanguage(string $idLanguage);
	
	/**
	 * Gets all the Languages in the storage media
	 * 
	 * @return array A Language array containing all the Languages found
	 */
	public function getAll();
	
	public function getMoraviaV1Language(int $idLanguage);
	
	/**
	 * Searches language by name
	 * 
	 * @param string $languageName
	 * @return Language[]
	 */
	public function searchLanguageForName(string $languageName);
	
}
?>