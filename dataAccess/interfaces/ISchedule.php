<?php

namespace dataAccess\interfaces;

use model\Schedule;

interface ISchedule {
	
	/**
	 * Saves a Schedule to the storage media
	 * @param Schedule $schedule A Schedule object to be stored
	 * @return boolean True if the operation succeded, false otherwise
	 */
	public function saveSchedule($schedule);
	
	/**
	 * Updates a Schedule on the storage media
	 * @param Schedule $schedule A Schedule object to be updated
	 * @return boolean True if the operation succeded, false otherwise
	 */
	public function updateSchedule($schedule);
	
	/**
	 * Deletes a Schedule from the storage media
	 * @param int $idSchedule The id associated to the schedule to be deleted
	 * @return boolean True if the operation succeded, false otherwise
	 */
	public function deleteSchedule($idSchedule);
	
	/**
	 * Gets the schedule associated with the given id
	 * @param int $idSchedule The id associated with the Schedule you want to get
	 * @return Schedule A Schedule object if found, null otherwise
	 */
	public function getSchedule($idSchedule);
	
	/**
	 * Gets all the schedules found in the storage media
	 *
	 * @return array An array containing all the schedules found
	 */
	public function getAll();
	
	/**
	 * Gets the current Schedule of a certain process
	 * Schedule the object with the current Schedule configuration
	 * @param int $idProcess The id of thw process from wich retrieve the schedule
	 * @return Schedule	 
	 **/
	public function getScheduleByProcess(int $idProcess);
}
?>