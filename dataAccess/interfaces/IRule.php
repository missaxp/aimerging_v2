<?php

namespace dataAccess\interfaces;

use model\Rule;

interface IRule {
	
	/**
	 * Saves a rule to the storage media
	 * @param Rule $rule The Rule object to be stored
	 * @return boolean if the operation succeded, false otherwise
	 */
	
	public function saveRule($rule);
	
	/**
	 * Updates a Rule on the storage media
	 * @param Rule $rule The Rule object to be updated
	 * @return boolean if the operation succeded, false otherwise
	 */
	public function updateRule($rule);
	
	/**
	 * Deletes a rule from the storage media
	 * @param int $idRule The id associated with the rule to be deleted
	 * @return boolean if the operation succeded, false otherwise
	 */
	public function deleteRule($idRule);
	
	/**
	 * Gets a Rule given its id
	 * @param  int $idRule The id associated with the Rule you want to get
	 * @return Rule The Rule object if found, null otherwise
	 */
	public function getRule($idRule);
	
	/**
	 * Gets all the Rules found on the storage media
	 *
	 * @return array An array containing all the Rules found
	 */
	public function getAll();
	
	/**
	 * Gets the rules associated with the given idProcess
	 * @param int $idProcess
	 * @return array An array containing all the rules found
	 */
	public function getRulesByProcess($idProcess);
	
	/***
	 * Gets all the rules for a given preprocess
	 * @param int $idPreprocess
	 * @return Rule[]
	 */
	public function getRulesByPreprocess(int $idPreprocess);
}
?>