<?php

/**
* @author zroque
* @version 1.0
* created on 21 jun. 2018
*/
namespace dataAccess\interfaces;

use model\Action;
use model\Process;

interface IProcess {
	/**
	 * Saves a process to the storage media
	 * @param Process $process A Process object to be stored
	 * @return boolean True if the operation succeded, false otherwise
	 */	
	public function saveProcess($process);
	
	/**
	 * Updates a process on the storage media
	 * @param Process $process A Process object to be stored
	 * @return boolean True if the operation succeded, false otherwise
	 */
	public function updateProcess($process,$idProcess);
	
	/**
	 * Deletes a process on the storage media
	 * @param int $idProcess the id of the process to be deleted
	 * @return boolean True if the operation succeded, false otherwise
	 */
	public function deleteProcess($idProcess);
	
	/**
	 * Gets the process associated with the given id
	 * @param int $idProcess The id of the process you want to get
	 * @return Process A process object if found, null otherwise
	 */
	public function getProcess($idProcess);
	
	/**
	 * Gets all the processes found in the storage media
	 * 
	 * @return array An array with all the processes found in the storage media
	 */
	public function getAll();
	
	/**
	 * Returns an array containing all the processes related to a source, the processes found will also contain the actions, 
	 * rules, notifications and schedule configurations, the array might be empty if no processes are found on the storage media
	 * @param int $idSource The key of the source
	 * @return Process[]
	 */
	public function getProcessesBySource(int $idSource);
	
	/**
	 * 
	 * @param Action $actions
	 * @return
	 */
	public function saveActions($actions);
	
	public function updateActions($actions);
	
	public function getActionsByProcess(int $idProcess);
	
	public function getAllActions();
}
?>