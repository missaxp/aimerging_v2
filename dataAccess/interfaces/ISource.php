<?php

/**
* @author Ljimenez
* @version 1.0
* created on 21 jun. 2018
*/
namespace dataAccess\interfaces;

use model\SourceAditonalProperty;
use model\APIUser;
use model\Source;

interface ISource{
	/**
	 * This method search all source recorded within of storage system.
	 * 
	 * @return array Sources.
	 */
	public function getSources();
	
	/**
	 * Search all user accounts for a sources.
	 * 
	 * @param int $idSource identifier of sources.
	 * 
	 * @return APIUser[] Users or null if not found any user
	 */
	public function getUsersSources(int $idSource);
	
	/**
	 * Search to properties of a souce.
	 * 
	 * @param int $idSource Identifier of sources.
	 * @return SourceAditonalProperty[] array with all AditonalProperties of a Source.
	 */
	public function getAditionalProperties(int $idSource);
	
	/**
	 * Save an SourceAditionalProperty of a Source in the storage media.
	 *
	 * @param SourceAditonalProperty $aditionalProperty aditional property of a source.
	 * @return bool true if operation was successful or false if else.
	 */
	public function saveSourceAditionalProperty(SourceAditonalProperty $aditionalProperty);
	
	/**
	 * Edit an SourceAditionalProperty saved in the storage media.
	 * 
	 * @param SourceAditonalProperty $aditionalProperty aditional property of a source.
	 * @return bool true if operation was successful or false if else.
	 */
	public function editPropertyAction(SourceAditonalProperty $aditionalProperty);
	
	/**
	 * Delete an aditional property of a Source saved in the storage media.
	 * 
	 * @param int $idAditonalProperty Identifier of SourceAditionalPorperty.
	 * @return bool true if operation was successful or false if else.
	 */
	public function deleteAditionalPropertyAction(int $idAditonalProperty);
	
	/**
	 * Searches Sources by name. 
	 * 
	 * @param string $sourceName
	 * @return Source
	 */
	public function getSourceByName(string $sourceName);
	
	/**
	 * Gets the APIUser associated with the given id
	 * @param int $idApiUser The id associated with the APIUser you want to get
	 * @return APIUser An APIUser if found, null otherwise
	 */
	public function getApiUser($idApiUser);
	
	/**
	 * Save date of last execution of Sources
	 * 
	 * @param string $executeDate
	 * @return boolean
	 */
	public function saveLastExecution(int $idSource, string $executeDate);
	
	/**
	 * Searches the Sources that will be in this iteration
	 * 
	 * @return Source[]
	 */
	public function getSourceToExecute();
	
	public function updateToken($idUser,$token,$time);
}
?>