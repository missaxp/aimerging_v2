<?php


namespace dataAccess\interfaces;


use model\Task;

interface ActionCreateTMSResponseI{
    /**
     * @param Task $task The task to be searched
     */
    public function getTMSResponseFromTask(Task $task);
}