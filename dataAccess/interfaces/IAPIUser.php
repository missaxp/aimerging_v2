<?php

/**
* @author zroque
* @version 1.0
* created on 21 jun. 2018
*/
namespace dataAccess\interfaces;

use model\APIUser;

interface IAPIUser {
	
	/**
	 * Saves an API User to the storage media
	 * @param APIUser $apiUser An ApiUser object to be stored
	 * @return bool True if the operation succeded, false otherwise
	 */
	public function saveApiUser($apiUser);
	
	/**
	 * Updates an APIUser on the storage media
	 * @param APIUser $apiUser The object to be stored
	 * @return bool True if the operation succeded, false otherwise
	 */
	public function updateApiUser($apiUser);
	
	/**
	 * Deletes an APIUser to the storage media
	 * @param int $idApiUser The id associated with the APIUser to be deleted
	 * @return bool True if the operation succeded, false otherwise
	 */
	public function deleteApiUser($idApiUser);
	

	
	/**
	 * Gets all the APIUsers found on the storage media
	 * 
	 * @return APIUser[] An Array containing all the APIUsers found
	 */
	public function getAll();
	
	/**
	 * Gets all the users related to a source
	 * @param int $idSource The id of the source from wich get the users
	 * @return APIUser[] An array containing all the users for the source, will be empty if no users are found
	 */
	public function getAPIUsersBySource(int $idSource);
}
?>