<?php

/**
* @author Ljimenez
* @version 1.0
* created on 21 jun. 2018
*/
namespace dataAccess\interfaces;

use model\Analysis;
use model\DataSource;
use model\File;
use model\Task;
use model\State;
use model\TaskAditionalProperties;
use model\Language;

interface ITask {
	/**
	 * This method save a task with all within of storage system.
	 * 
	 * @param Task $task Task structure created from the analysis of the source.
	 * 
	 * @return boolean return true if the process was successful or false if process was fail.
	 */
	public function saveTask(Task $task);
	
	/**
	 * This metodo update a task that is already stored.
	 * 
	 * @param Task $task Task with new datas.
	 */
	public function updateTask(Task $task);
	
	/**
	 * This method change state of task.
	 * 
	 * @param string $idTask identifier of task has changed.
	 * @param State $state New state of task.
	 * 
	 * @return bool return true if the process was successful or false if process was fail.
	 */
	public function changeStateTask(string $idTask, int $state, string $description);
	
	/**
	 * This method delete to task of storage system.
	 * 
	 * @param string $idTask identifier of task to delete.
	 * 
	 * @return bool return true if the process was successful or false if process was fail.
	 */
	public function deleteTask(string $idTask);
	
	/**
	 * Search a task by identifier.
	 * 
	 * @param string $idTask Identifier of task to search.$this
	 * 
	 * @return Task if found, null otherwise.
	 */
	public function getTask(string $idTask);
	
	/**
	 * Saves the aditional properties of a task to the storage media
	 * @param TaskAditionalProperties $taskAdProp The object to be stored
	 * @return bool True if the operation succeded, false otherwise
	 */
	public function saveTaskAditionalProperties(TaskAditionalProperties $taskAdProp,string $uniqueSourceId);
	
	/**
	 * Updates the aditional properties of a task on the storage media
	 * @param TaskAditionalProperties $taskAdProp The object to be stored
	 * @return
	 */
	public function updateTaskAditionalProperties(TaskAditionalProperties $taskAdProp);
	
	/**
	 * Deletes the aditional properties of a task given its id
	 * @param int $idTaskAditionalProperties The id of the aditional properties of the task
	 * @return
	 */
	public function deleteTaskAditionalProperties(int $idTaskAditionalProperties);
	
	/**
	 * Gets a single TaskAditionalProperties given its id
	 * @param int $idTaskAditionalProperties The id of the aditional property you want to get
	 * @return TaskAditionalProperties[] An object with the aditional properties if found, null otherwise
	 */
	public function getTaskAditionalProperties(int $idTaskAditionalProperties);
	
	/**
	 * Gets all the TaskAditional properties related to a task
	 * @param int $uniqueSourceId The uniqueSourceId of the task
	 * @return TaskAditionalProperties[] An array containing all the aditional properties found, if no
	 * aditional properties are found, will return an empty array
	 */
	public function getAditionalPropertiesByTask(string $uniqueSourceId);
	
	/**
	 * This method should save a file to the desired storage system,
	 * to do so it receives a File object with all its properties initialized
	 * @param File $file the file object to store
	 * @param string $uniqueSourceId identifier of task.
	 * @return bool True if the operation succedes, false otherwise
	 */
	public function saveFile(File $file, string $uniqueSourceId);
	
	/**
	 * This method will return the file asociated with the idFile given as parameter
	 * @param int $idFile the key of the file you want to get
	 * @return File A File object if found, null otherwise
	 */
	public function getFile(int $idFile);
	
	/**
	 * This method will delete a file given its idFile
	 * @param int $idFile
	 * @return boolean True if the operation succedes, false otherwise
	 */
	public function deleteFile(int $idFile);
	
	/**
	 * This method will update a file, to do so, a File object with all the properties
	 * initialized must be given
	 * @param File $file the file to update
	 * @return boolean True if the operation succedes, false otherwise
	 */
	public function updateFile(File $file);
	
	/**
	 * This method will retrieve all the files stored
	 * @return array An array containing all the Files found on the storage media
	 */
	public function getAll();
	
	/**
	 * This method records an analysis of a task within the storage system.
	 *
	 * @param Analysis $analyze Word analysis of a task.
	 * @param string $idTask Identifier of the task to which the analysis belongs.
	 * @param Language[] $targetLanguage
	 *
	 * @return boolean return true if process was successful or false if process was fail.
	 */
	public function saveAnalysis(array $analysis,string $idTask, array $targetLanguage);
	
	/**
	 * This method update to analysis within the storage system.
	 *
	 * @param Analysis $analyze Analysis to update with a identifier.
	 *
	 * @return boolean return true if process was successful or false if process was fail.
	 */
	public function updateAnalysis(Analysis $analysis);
	
	/**
	 * This method delete to analisis within the storage system.
	 *
	 * @param int $idAnalyze identifier of analysis to delete.
	 *
	 * @return boolean return true if process was successful or false if process was fail.
	 */
	public function deleteAnalysis(int $idAnalysis);
	
	/**
	 * Gets the word analysis of a target language
	 *
	 * @param string $idTask identifier of task.
	 *
	 * @return array returns the array of analysis of the target of the task
	 */
	public function getAnalysis(int $idAnalysis);
	
	/**
	 * this method save a new Source
	 *
	 * @param string $uniqueSourceId
	 * @param DataSource $dataSource
	 */
	public function saveDataSource(DataSource $dataSource, string $uniqueSourceId);
	
	/**
	 * This method update a Data source of a task.
	 *
	 * @param DataSource $dataSource data source of a task.
	 */
	public function updateDataSource(DataSource $dataSource);
	
	/**
	 * This method delete to data source that is registered in storage system.
	 *
	 * @param int $idDataSource Identifier of data source.
	 */
	public function deleteDataSource(int $idDataSource);
	
	/**
	 * Searches a data source by identifier.
	 *
	 * @param string $uniqueSourceId Identifier of data source.
	 */
	public function findDataSource(string $uniqueSourceId);
	
	/**
	 * Updates the state of the task, this method will update the current state and save the change history
	 * If state is RETRY and numberOfTries is over than the set default numberTries value, the task is set to ERROR.
	 * @param Task $task
	 * @param int $idState
	 * @param string $description
	 * @param integer $idAction
	 * @param integer $idProcess
	 * @param bool $alter
	 * @param bool $idAction 
	 * @return boolean true is succeded, false otherwise
	 */
	public function updateTaskState(Task $task, int $idState, string $description, int $idAction=0, int $idProcess=0, bool $alter = true, bool $alterAction = true);
	
	
	/**
	 * Gets the tasks that are in the specified state, returns an array of Tasks, might be empty if no tasks are found
	 * @param int $idState
	 * @return Task[]
	 */
	public function getTasksByState(int $idState);
	
	/**
	 * Gets the target languages for a task
	 * @param int $uniqueSourceId
	 * @return Language[] An array containing the languages, may be empty if no languages are found
	 */
	public function getTargetLanguagesByTask(string $uniqueSourceId);
	
	/**
	 * Gets the files associated with a task, it will return the file type specified as parameter.
	 * @param string $fileType A string for the type of file: TRANSLATE or REFERENCE
	 * @param string $uniqueSourceId
	 * @return File[] An array containing the files, may be empty if no files are found
	 */
	public function getFilesByTask(string $fileType, string $uniqueSourceId);
	
	/**
	 * Look for the tasks which have passed the waiting time and change state to CANCEL
	 * 
	 * @return Task[]
	 */
	public function cancelExpiredTasks(int $interval, int $idSource = null);
	
	/**
	 * Return all task with state FILTERED, EXECUTINGACTIONS, RETRY.
	 * 
	 * @return Task[]
	 */
	public function getTaskToReprocess();
}
?>