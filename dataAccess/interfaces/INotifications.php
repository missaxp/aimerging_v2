<?php

namespace dataAccess\interfaces;

use model\Notification;

interface INotifications {
	
	/**
	 * Saves a notification to the storage media
	 * @param Notification $notification A Notification object to be stored
	 * @return boolean True if the operation succeded, false otherwise
	 */
	public function saveNotification($notification);
	
	/**
	 * Updates a notification on the storage media
	 * @param Notification $notification The Notification object to be stored
	 * @return boolean True if the operation succeded, false otherwise
	 */
	public function updateNotification($notification,$idProcess);
	
	/**
	 * Deletes a notification from the storage media
	 * @param int $idProcess The id of the process associated with the notification
	 * @return
	 */
	public function deleteNotificacion(int $idProcess);
	
	/**
	 * Gets all the notification associated with the given id process
	 * @param int $idProcess
	 * @return Notification
	 */
	public function getNotificationByProcess(int $idProcess);
	
	/**
	 * Gets all the notification found on the storage media
	 *
	 * @return array An array containing all the notifications found
	 */
	public function getAll();
}
?>