<?php

/**
 *
 * @author zroque
 * @version 1.0
 *          created on 21 jun. 2018
 */
namespace dataAccess\interfaces;

use model\Category;

interface ICategory {
	/**
	 * Saves a category to the storage media
	 * @param Category $category A category object to be stored, with all
	 * its properties initilized
	 * @return boolean True if the operation succeded, false otherwise
	 */
	public function saveCategory($category);
	
	/**
	 * Updates a category on the storage media
	 * @param Category $category The category to be updated
	 * @return boolean True if the operation succeded, false otherwise
	 */
	public function updatecategory($category);
	
	/**
	 * Deletes a category from the storage media
	 * @param int $idCategory the id of the category to be deleted
	 * @return boolean True if the operation succeded, false otherwise
	 */
	public function deleteCategory($idCategory);
	
	/**
	 * Gets the category associated with the given id
	 * @param int $idcategory The id of the category you want to get
	 * @return Category A Category object if found, null otherwise
	 */
	public function getCategory($idcategory);
	
	/**
	 * Gets all the categories found on the storage media
	 * 
	 * @return array An array containing all the categories found
	 */
	public function getAll();
	
}
?>