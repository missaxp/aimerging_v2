<?php
namespace dataAccess\dao;

use common\exceptions\AIException;
use dataAccess\interfaces\ActionCreateTMSResponseI;
use dataAccess\dbConn;
use dataAccess\ConnectionDB;
use dataAccess\SQLException;
use Functions;
use model\Task;
use Throwable;

include_once BaseDir. '/dataAccess/ConnectionDB.php';
include_once BaseDir. '/dataAccess/interfaces/ActionCreateTMSResponseI.php';
include_once BaseDir. '/common/exceptions/AIException.php';
include_once BaseDir. '/Functions.php';

class ActionCreateTMSResponseDAO implements ActionCreateTMSResponseI{

    /**
     *
     * @var dbConn
     */
    private $connection;

    public function __construct(){
        $dataBase = new ConnectionDB();
        $this->connection = $dataBase->getConnection();
    }

    public function closeConnection(){
        $dataBase = new ConnectionDB();
        $dataBase->closeConnection();
    }

    /**
     * @param Task $task The task to be searched
     * @return String TMSResponse from task
     * @throws AIException
     */
    public function getTMSResponseFromTask(Task $task)
    {
        $tmsResponse = "";
        $query = 'SELECT tmsResponse FROM ActionCreateTMSResponse where uniqueSourceId = :usi';
        try{
            $result = $this->connection->execute($query, array(
                "usi" => $task->getUniqueSourceId()
            ));
            if($result->fetch()){
                $tmsResponse = $result->getVal("TMSRESPONSE");
            }

            $result->close();
        } catch (SQLException $ex){
            throw AIException::createInstanceFromSQLException($ex)->construct(__METHOD__, __NAMESPACE__, $args = func_get_args());
        } catch (Throwable $t){
            throw AIException::createInstanceFromThrowable($t)->construct(__METHOD__, __NAMESPACE__, $args = func_get_args());
        }

        return $tmsResponse;
    }


    /**
     * @param $idCreateAction
     * @param $uniqueSourceId
     * @param $tmsResponse
     * @throws AIException
     */
    public function saveActionCreateTMSResponse($idCreateAction, $uniqueSourceId, $tmsResponse){
        $vars["idCreateAction"] = $idCreateAction;
        $vars["uniqueSourceId"] = $uniqueSourceId;
        $vars["tmsResponse"] = $tmsResponse;
        try{
            $this->connection->setValues(DBCONN_INSERT, "ActionCreateTMSResponse", $vars);
        }catch (SQLException $sqlE){
            $e = AIException::createInstanceFromSQLException($sqlE)->construct(__METHOD__, __NAMESPACE__, $args = func_get_args(), array(
                "sql_vars" => $vars
            ));
            throw $e;
        }
        catch(Throwable $e) {
            $ex = AIException::createInstanceFromThrowable($e)->construct(__METHOD__, __NAMESPACE__, $args = func_get_args(), array(
                "sql_vars" => $vars
            ));
            throw $ex;
        }
    }
}