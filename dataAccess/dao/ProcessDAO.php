<?php

/**
 *
 * @author zroque
 * @version 1.0
 *          created on 28 jun. 2018
 */
namespace dataAccess\dao;

use common\exceptions\AIException;
use dataAccess\interfaces\IProcess;
use Functions;
use model\Process;
use dataAccess\dbConn;
use dataAccess\ConnectionDB;
use dataAccess\SQLException;
use model\Action;
use model\ActionCreate;
use model\ConfirmEmailAction;
use model\PropertyAction;
use model\State;
use behaviorModule\actionsModule\EmailAction;
use model\Analysis;
use dataAccess\dbStm;
use behaviorModule\Behavior;
use model\Notification;
use model\MemoQToolSetup;
use core\AI;
use Throwable;

include_once BaseDir . '/dataAccess/interfaces/IProcess.php';
include_once BaseDir . '/model/Process.php';
include_once BaseDir . '/model/Notification.php';
include_once BaseDir . '/dataAccess/dao/NotificationDAO.php';
include_once BaseDir . '/dataAccess/dao/RuleDAO.php';
include_once BaseDir . '/dataAccess/dao/ScheduleDAO.php';
include_once BaseDir . '/model/Email.php';
include_once BaseDir . '/dataAccess/ConnectionDB.php';
include_once BaseDir . '/behaviorModule/actionsModule/EmailAction.php';
include_once BaseDir . '/behaviorModule/actionsModule/createAction.php';
include_once BaseDir . '/behaviorModule/actionsModule/AcceptTaskAction.php';
include_once BaseDir . '/model/PropertyAction.php';
include_once BaseDir . '/behaviorModule/actionsModule/ConfirmEmailAction.php';
include_once BaseDir . '/model/MemoQToolSetup.php';
class ProcessDAO implements IProcess {
	
	/**
	 *
	 * @var dbConn
	 */
	private $connection;

	/**
	* Validation for the source of the request if from the AI ​​or from the api
	*
	* @var boolean
	*/
	private $rest;
	
	private const MEMOQTOOL = 1;

	public function __construct($rest = false){
		$this->rest = $rest;
		$dataBase = new ConnectionDB();
		$this->connection = $dataBase->getConnection();
	}
	
// 	public function cloneProcess($idProcess){
// 		$process = $this->getProcess($idProcess);
// 		$process = $process->toArray();
// 		$process = array_change_key_case_recursive($process, CASE_LOWER);
// 		$process = (object) $process;
// 		$dataValues = array();
// 		$dataValues["processName"] = "Copy of ".$process->processname.\Functions::currentDate();
// 		$dataValues["enabled"] = !isset($process->enabled)?'0':($process->enabled == "Y")?'1':'0';
// 		$dataValues["description"] = isset($process->description)?$process->description:'empty';
// 		$dataValues["creator"] = isset($process->creator)?$process->creator:'AI:Creator';
// 		$dataValues["timeStamp"] = array(
// 				date('Y/m/d H:i:s'),
// 				DBCONN_DATATYPE_DATE
// 		);
// 		$dataValues["idSource"] = $process->idsource;
// 		// $dataValues["priority"] = 1;
// 		$idProcess = 0;
// 		$result = false;
// 		try {
// 			$this->connection->setDefaultExecMode(DBCONN_TRANSACTION);
// 			$action = "C";
// 			$table = "Process";
// 			$idProcess = $this->connection->setValues($action,$table, $dataValues);
// 			// $scheduleDao = new ScheduleDAO();
// 			// $schedules = $process['schedules'];
// 			// for($i = 0; $i < count($schedules); $i ++) {
// 			//     $schedules[$i]['idprocess'] = $idProcess;
// 			//     $scheduleDao->saveSchedule($schedules[$i]);
// 			// }
			
// 			// $ruleDao = new RuleDAO();
// 			// $rules = $process['rules'];
// 			// for($i = 0; $i < count($rules); $i ++) {
// 			//     $rules[$i]['idprocess'] = $idProcess;
// 			//     $ruleDao->saveRule($rules[$i]);
// 			// }
// 			$ruleDao = new RuleDAO();
// 			foreach ($process->rules as $rule){
// 				$rule->idProcess = $idProcess;
// 				$ruleDao->saveRule($rule);
// 			}
			
// 			foreach ($process->actions as $action){
// 				$action->idprocess = $idProcess;
// 				$this->saveActions($action);
// 			}
			
// 			$notification = array();
// 			$notification['email'] = $process->email;
			
// 			$notification['whenProcessed'] = isset($process->notification->whenprocessed)?($process->notification->whenprocessed == 'Y')?'1':'0':'0';
// 			$notification['whenWarnings'] = isset($process->notification->whenwarnings)?($process->notification->whenwarnings == 'Y')?'1':'0':'0';
// 			$notification['whenErrors'] = isset($process->notification->whenerrors)?($process->notification->whenerrors == 'Y')?'1':'0':'0';
// 			$notification['whenRejected'] = isset($process->notification->whenrejects)?($process->notification->whenrejects == 'Y')?'1':'0':'0';
// 			$notification['idProcess'] = $idProcess;
// 			$notification['addBasicTaskInfo'] = isset($process->notification->addbasicinformation)?($process->notification->addbasicinformation == 'Y')?'1':'0':'0';
// 			$notification['addSourceFile'] = isset($process->notification->addsourcefile)?($process->notification->addsourcefile == 'Y')?'1':'0':'0';
// 			$notification['whenChanged'] = isset($process->notification->whenchanges)?($process->notification->whenchanges == 'Y')?'1':'0':'0';
// 			$notificationDao = new NotificationDAO();
// 			$notificationDao->saveNotification($notification);
			
// 			// $actions = $process['actions'];
// 			// for($i = 0; $i < count($actions); $i ++) {
// 			//     $actions[$i]['idprocess'] = $idProcess;
// 			//     $this->saveActions($actions[$i]);
// 			// }
// 			$result = $this->connection->commit();
// 		} catch(SQLException $ex) {
// 			$result = false;
// 			$this->connection->rollback();
// 		} finally {
// 			$this->connection->setDefaultExecMode(DBCONN_COMMIT_ON_SUCCESS);
// 		}
		
// 		return  array('result' => $result);
// 	}
	
	public function enabledProcess($idProcess,$enabled){
		$values = array();
		$values['enabled'] = $enabled->enabled?'1':'0';
		
		$whereValues = array();
		$whereValues["idProcess"] = $idProcess;
		// return $dataValues;
		$whereCondition = "idProcess = :IDPROCESS";
		
		$result = $this->connection->setValues(DBCONN_UPDATE, ConnectionDB::PROCESS, $values, $whereCondition, $whereValues);
		
		return array('result' => $result);
	}
	
	public function generateProcess($process){
		$newProcess = new Process();
		
		$newProcess->setProcessName($process->processName);
		$newProcess->setEnabled((!isset($process->enabled)?'0':($process->enabled == "Y")?'1':'0'));
		$newProcess->setDescription(isset($process->description)?$process->description:'empty');
		$newProcess->setCreator(isset($process->creator)?$process->creator:'AI:Creator');
		$newProcess->setIdSource($process->idsource);
		
		$notification = new Notification();
		$notification->setEmail($process->email);
		$notification->setAddBasicTaskInfo(isset($process->addbasicinformation)?($process->addbasicinformation == 'Y')?'1':'0':'0');
		$notification->setAddSourceFile(isset($process->addsourcefile)?($process->addsourcefile == 'Y')?'1':'0':'0');
		$notification->setWhenChanged(isset($process->whenchanges)?($process->whenchanges == 'Y')?'1':'0':'0');
		$notification->setWhenErrors(isset($process->whenerrors)?($process->whenerrors == 'Y')?'1':'0':'0');
		$notification->setWhenProcess(isset($process->whenprocessed)?($process->whenprocessed == 'Y')?'1':'0':'0');
		$notification->setWhenRejected(isset($process->whenrejects)?($process->whenrejects == 'Y')?'1':'0':'0');
		$notification->setWhenWarnings(isset($process->whenwarnings)?($process->whenwarnings == 'Y')?'1':'0':'0');
		
		$newProcess->setNotification($notification);
		
		return $newProcess;
	}
	
	public function getProcess($idProcess){

		$dataValues = array();
		$dataValues["IDPROCESS"] = $idProcess;
		if ($this->rest) {
			$query = "SELECT * FROM Process WHERE idProcess = :IDPROCESS";
		}else{
			$query = "SELECT * FROM Process WHERE idProcess = :IDPROCESS AND enabled = 1";
		}
		
		$process = null;
		try {
			$result = $this->connection->execute($query, $dataValues);
			if ($result->fetch()) {
				$process = $this->dbresourceprocess($result);
			}
		} catch(SQLException $ex) {
			\Functions::addLog($ex->getMessage(), \Functions::SEVERE, json_encode($ex->getTrace()));
		} finally{
			$result->close();
		}
		
		return $process;
	}

	/**
	 * Creates a process object given a db resource
	 *
	 * @param dbStm $q
	 * @return \model\Process
	 */
	private function dbresourceprocess($result){

		$process = new Process();
		
		$process->setIdProcess($result->idProcess);
		$process->setProcessName($result->processName);
		$process->setEnabled($result->enabled == 1);
		$process->setDescription($result->description);
		$process->setCreator($result->creator);
		$process->setTimeStamp($result->timeStamp);
		$process->setIdSource($result->idSource);
		
		$ruleDao = new RuleDAO($this->rest);
		$rules = $ruleDao->getRulesByProcess($process->getIdProcess());
		$process->setRules($rules);
		
		$scheduleDao = new ScheduleDAO();
		$schedules = $scheduleDao->getScheduleByProcess($process->getIdProcess());
		$process->setSchedules($schedules);
		
		$actions = $this->getActionsByProcess($process->getIdProcess());
		$process->setActions($actions);
		
		$notificationDao = new NotificationDAO();
		$notification = $notificationDao->getNotificationByProcess($process->getIdProcess());
		
		$actionsIDName = array();
		foreach($actions as $action) {
			$actionsIDName[] = array(
					"id" => $action->getIdAction(),
					"name" => $action->getActionName()
			);
		}
		$notification->setActionsData($actionsIDName);
		
		$process->setNotification($notification);
		
		return $process;
	}

	public function getAll(){

		$query = "SELECT * FROM Process";
		
		$processesFound = array();
		
		try {
			$result = $this->connection->execute($query);
			while($result->fetch()) {
				$processesFound[] = $this->dbresourceprocess($result);
			}
		} catch(SQLException $ex) {
			echo $ex->getMessage();
		} finally{
			$result->close();
		}
		
		return $processesFound;
	}

	public function updateProcess($process,$idProcess){
		$whereValues = array();
		$whereValues["idProcess"] = $idProcess;
		
		$dataValues = array();
		if (isset($process->processName)) {
			$dataValues["processName"] = $process->processName;
		}
		
		if (isset($process->enabled)) {
			$dataValues["enabled"] = !isset($process->enabled)?'0':($process->enabled == "Y")?'1':'0';
		}
		
		if (isset($process->description)) {
			$dataValues["description"] = isset($process->description)?$process->description:'empty';
		}
		
		
		if (isset($process->creator)) {
			$dataValues["creator"] = isset($process->creator)?$process->creator:'AI:Creator';
		}
		// $dataValues["priority"] = 1;
		// $idProcess = 0;
		$result = 0;
		
		//         /**
		//          * Get the current process as stored in BD, compares the hash with the new process and if both are different
		//          * do the update and store the json and other useful data in the ProcessHistory
		//          *
		//          * @var \model\Process $currentProcess
		//          */
		$currentProcess = $this->getProcess($idProcess);
		$currentHash = md5(json_encode($currentProcess));
		$newHash = md5(json_encode(get_object_vars($process)));
		if ($currentHash != $newHash) {
			try {
				$this->connection->setDefaultExecMode(DBCONN_TRANSACTION);
				$action = "M";
				$table = "Process";
				$whereCondition = "idProcess = :IDPROCESS";

				
				if (!empty($dataValues)) {
					$result = $this->connection->setValues($action, $table, $dataValues, $whereCondition, $whereValues);
				}
				
				if (isset($process->actions)){
					$actions = $process->actions;
					$sequence = 1;
					foreach($actions as $action){
						$idAccion = ($action->id_accion);
						$dataValues["sequence"] = $sequence;
						$sequence++;
					
					$whereValues = array();
					$whereValues["idProcess"] = $idProcess;
					$whereValues["idActions"] = $idAccion;
					$action = "M";
					$table = "ProcessActions";
					$whereCondition = "idProcess=:idProcess and idActions=:idActions";
					$result2 = $this->connection->setValues($action, $table, $dataValues, $whereCondition, $whereValues);
					}
				}


				$notification = array();
				if (isset($process->email)) {
					$notification['email'] = $process->email;
				}
				
				if (isset($process->whenprocessed)) {
					$notification['whenProcessed'] = isset($process->whenprocessed)?($process->whenprocessed == 'Y')?'1':'0':'0';
				}
				
				
				if (isset($process->whenwarnings)) {
					$notification['whenWarnings'] = isset($process->whenwarnings)?($process->whenwarnings == 'Y')?'1':'0':'0';
				}
				
				if (isset($process->whenerrors)) {
					$notification['whenErrors'] = isset($process->whenerrors)?($process->whenerrors == 'Y')?'1':'0':'0';
				}
				
				if (isset($process->whenrejects)) {
					$notification['whenRejected'] = isset($process->whenrejects)?($process->whenrejects == 'Y')?'1':'0':'0';
				}
				
				if (isset($process->addbasicinformation)) {
					$notification['addBasicTaskInfo'] = isset($process->addbasicinformation)?($process->addbasicinformation == 'Y')?'1':'0':'0';
				}
				
				if (isset($process->addsourcefile)) {
					$notification['addSourceFile'] = isset($process->addsourcefile)?($process->addsourcefile == 'Y')?'1':'0':'0';
				}
				
				
				if (isset($process->whenchanges)) {
					$notification['whenChanged'] = isset($process->whenchanges)?($process->whenchanges == 'Y')?'1':'0':'0';
				}
				// $archivo = "/webs/ai/dataAccess/dao/archivo.txt";
				// file_put_contents($archivo,var_dump($notification['whenProcessed'],true));
				$notificationDao = new NotificationDAO();
				
				if (!empty($notification)) {
					// $notification['idProcess'] = $idProcess;
					$notificationDao->updateNotification($notification,$idProcess);
				}
				
				// $dataValues = array();
				// $dataValues["modifiedBy"] = "temp"; // TODO obtener quien modifica el proceso
				// $dataValues["modificationDate"] = date('Y/m/d H:i:s');
				// $dataValues["configuration"] = json_encode($currentProcess);
				// $dataValues["idProcess"] = $idProcess;
				// $dataValues["processName"] =
				
				// $actionType = "C";
				// $table = "ProcessHistory";
				// $this->connection->setValues($actionType, $table, $dataValues);
				
				$this->connection->commit();
			} catch(SQLException $ex) {
				$this->connection->rollback();
				Functions::logException($ex->getMessage(), Functions::SEVERE, __CLASS__, AIException::createInstanceFromSQLException($ex)->construct(__METHOD__,__NAMESPACE__, $f = func_get_args()));
			} finally {
				$this->connection->setDefaultExecMode(DBCONN_COMMIT_ON_SUCCESS);
			}
		}
		return $notification;
	}

	public function saveProcess($process){

		$dataValues = array();
        $dataValues["processName"] = $process->processName;
        $dataValues["enabled"] = !isset($process->enabled)?'0':($process->enabled == "Y")?'1':'0';
        $dataValues["description"] = isset($process->description)?$process->description:'empty';
        $dataValues["creator"] = isset($process->creator)?$process->creator:'AI:Creator';
        $dataValues["timeStamp"] = array(
            date('Y/m/d H:i:s'),
            DBCONN_DATATYPE_DATE
        );
        $dataValues["idSource"] = $process->idsource;
        $dataValues["idSource"] = $process->idsource;
        // $dataValues["priority"] = 1;
        $idProcess = 0;

        try {
            $this->connection->setDefaultExecMode(DBCONN_TRANSACTION);
            $action = "C";
            $table = "Process";
            $idProcess = $this->connection->setValues($action,$table, $dataValues);

            $notification = array();
            $notification['email'] = $process->email;
            
            $notification['whenProcessed'] = isset($process->whenprocessed)?($process->whenprocessed == 'Y')?'1':'0':'0';
            $notification['whenWarnings'] = isset($process->whenwarnings)?($process->whenwarnings == 'Y')?'1':'0':'0';
            $notification['whenErrors'] = isset($process->whenerrors)?($process->whenerrors == 'Y')?'1':'0':'0';
            $notification['whenRejected'] = isset($process->whenrejects)?($process->whenrejects == 'Y')?'1':'0':'0';
            $notification['idProcess'] = $idProcess;
            $notification['addBasicTaskInfo'] = isset($process->addbasicinformation)?($process->addbasicinformation == 'Y')?'1':'0':'0';
            $notification['addSourceFile'] = isset($process->addsourcefile)?($process->addsourcefile == 'Y')?'1':'0':'0';
            $notification['whenChanged'] = isset($process->whenchanges)?($process->whenchanges == 'Y')?'1':'0':'0';
            $notificationDao = new NotificationDAO();
            $notificationDao->saveNotification($notification);
            $this->connection->commit();
        } catch(SQLException $ex) {
            $this->connection->rollback();
        } finally {
            $this->connection->setDefaultExecMode(DBCONN_COMMIT_ON_SUCCESS);
        }

        $newProcess = $this->getProcess($idProcess);

        return  array('process' => $idProcess, 'data' => $newProcess);
	}
	
	public function cloneProcess($idProcess, $processName){
		$process = $this->getProcess($idProcess);
		$process = $process->toArray();
		$process = array_change_key_case_recursive($process, CASE_LOWER);
		$process = \Functions::arrayToObject($process);
		// return $process;
		$dataValues = array();
		$dataValues["processName"] = $processName;
		$dataValues["enabled"] = $process->enabled?'1':'0';
		$dataValues["description"] = isset($process->description)?$process->description:'empty';
		$dataValues["creator"] = isset($process->creator)?$process->creator:'AI:Creator';
		$dataValues["timeStamp"] = array(
				date('Y/m/d H:i:s'),
				DBCONN_DATATYPE_DATE
		);
		$dataValues["idSource"] = $process->idsource;
		// $dataValues["priority"] = 1;
		$idProcess = 0;
		$result = false;
		try {
			$this->connection->setDefaultExecMode(DBCONN_TRANSACTION);
			$action = "C";
			$table = "Process";
			$idProcess = $this->connection->setValues($action,$table, $dataValues);
			$ruleDao = new RuleDAO();
			foreach ($process->rules as $rule){
				$rule->idprocess = $idProcess;
				$rule->enabled = $rule->enabled.'';
				$ruleDao->saveRule($rule);
			}
			
			foreach ($process->actions as $action){
				$action->idprocess = $idProcess;
				$this->saveActions($action);
			}
			
			$notification = array();
			$notification['email'] = $process->notification->email;
			
			$notification['whenProcessed'] = $process->notification->whenprocess == 1?'1':'0';
			$notification['whenWarnings'] = $process->notification->whenwarnings == 1?'1':'0';
			$notification['whenErrors'] = $process->notification->whenerrors == 1?'1':'0';
			$notification['whenRejected'] = $process->notification->whenrejected == 1?'1':'0';
			$notification['idProcess'] = $idProcess;
			$notification['addBasicTaskInfo'] = $process->notification->addbasictaskinfo == 1?'1':'0';
			$notification['addSourceFile'] = $process->notification->addsourcefile == 1?'1':'0';
			$notification['whenChanged'] = $process->notification->whenchanged == 1?'1':'0';
			$notificationDao = new NotificationDAO();
			$notificationDao->saveNotification($notification);
			
			// $actions = $process['actions'];
			// for($i = 0; $i < count($actions); $i ++) {
			//     $actions[$i]['idprocess'] = $idProcess;
			//     $this->saveActions($actions[$i]);
			// }
			$result = $this->connection->commit();
		} catch(SQLException $ex) {
			$idProcess = 0;
			$result = false;
			$this->connection->rollback();
		} finally {
			$this->connection->setDefaultExecMode(DBCONN_COMMIT_ON_SUCCESS);
		}
		
		return  array('result' => $result, 'idprocess' => $idProcess);
	}

	public function deleteProcess($idProcess){
		$process = $this->getProcess($idProcess);
        $process = $process->toArray();
        $process = array_change_key_case_recursive($process, CASE_LOWER);
        // return $process;
        $result = false;
        try {
            $this->connection->setDefaultExecMode(DBCONN_TRANSACTION);
            

            if ($process == null) {
                return true;
            }
            $ruleDAO = new RuleDAO();
            foreach ($process['rules'] as $rule) {
                $ruleDAO->deleteRule($rule['idrule']);
            }

            foreach ($process['actions'] as $action) {
                $this->deleteAction($action);
            }

            $notificationDAO = new NotificationDAO();

            $notificationDAO->deleteNotificacion($idProcess);

            $values = array();
            $values['IDPROCESS'] = $idProcess;
            $query = "DELETE FROM ". ConnectionDB::PROCESS ." WHERE idProcess = :IDPROCESS";
            $this->connection->execute($query,$values);

            $result = $this->connection->commit();
        } catch (\Exception $e) {
            $this->connection->rollback();
            $result = false;
        }finally{
            $this->connection->setDefaultExecMode(DBCONN_COMMIT_ON_SUCCESS);
        }

        return $result;
    }

    public function deleteAction($action){
		if (gettype($action)!="array"){
			$action = get_object_vars($action);
		}
        switch ($action['actiontype']) {
            case "create" :
                $result = $this->deleteActionCreate($action);
                break;
            case "confirmEmail" :
                $result = $this->deleteActionConfirm($action);
                break;
            case 'sendMail':
                $result = $this->deleteSendMail($action);
                break;
            default :
                break;
        }

        $values = array();
        $values['IDPROCESS'] = $action['idprocess'];
        $values['IDACTIONS'] = $action['idaction'];
        $query = "DELETE FROM ProcessActions WHERE idProcess = :IDPROCESS AND idActions = :IDACTIONS";
        $result = $this->connection->execute($query,$values);

        return $result;

    }

    public function deleteActionCreate($action){//[][]
		//Functions::print($action);
		// if(isset($action['overwrite'])){
        //     $values = array();
        //     $values['IDCREATEACTION'] = $action['idcreate'];
        //     $query = "DELETE FROM ".ConnectionDB::ACTIONCREATEWCOVERWRITE." WHERE idCreateAction = :IDCREATEACTION";
        //     $this->connection->execute($query,$values);
        
        foreach ($action['propertyactions'] as $property) {
			$property = (object) $property;
            $values = array();
            $values['IDPROPERTYACTION'] = $property->idproperty;
            $query = "DELETE FROM ".ConnectionDB::ACTIONCREATEADDITIONALPROPERTIES." WHERE idPropertyAction = :IDPROPERTYACTION";
            $this->connection->execute($query,$values);
        }

        $values = array();
        $values['IDCREATEACTION'] = $action['idcreate'];
		$query_delete_TMS_response = "DELETE FROM ".ConnectionDB::ACTION_CREATE_TMS_RESPONSE." WHERE idCreateAction = :IDCREATEACTION";
		$result_TMS_response = $this->connection->execute($query_delete_TMS_response,$values);
		
        $query_delete_overwrite = "DELETE FROM ".ConnectionDB::ACTIONCREATEWCOVERWRITE." WHERE idCreateAction = :IDCREATEACTION";
        $result_overwrite = $this->connection->execute($query_delete_overwrite,$values);
		
		$query_delete_action_create = "DELETE FROM ".ConnectionDB::ACTIONCREATE." WHERE idCreateAction = :IDCREATEACTION";
        $result_action_create = $this->connection->execute($query_delete_action_create,$values);
        return ($result_TMS_response && $result_overwrite && $result_action_create);
    }

    public function deleteActionConfirm($action){
        $values = array();
		$values['IDCONFIRMEMAIL'] = $action['idconfirmemail'];
		try{
			$query_confirmation_token = "DELETE FROM ".ConnectionDB::CONFIRMATIONTOKEN." WHERE idConfirmEmail = :IDCONFIRMEMAIL";
			$result_confirmation = $this->connection->execute($query_confirmation_token,$values);
	
			$query_action_confirm_email_feedback = "DELETE FROM ".ConnectionDB::ACTIONCONFIRMEMAILFEEDBACK." WHERE idConfirmEmail = :IDCONFIRMEMAIL";
			$result = $this->connection->execute($query_action_confirm_email_feedback,$values);
		
			$query_action_confirm_email = "DELETE FROM ".ConnectionDB::ACTIONCONFIRMEMAIL." WHERE idConfirmEmail = :IDCONFIRMEMAIL";
			$result = $this->connection->execute($query_action_confirm_email,$values);
		}catch(SQLException $e){
			Functions::logException($ex->getMessage(), Functions::WARNING, __CLASS__, $ex);
			$result = false;
		}
        return $result;
    }

    public function deleteSendMail($action){
        $values = array();
        $values['IDCONFIRMEMAIL'] = $action['idemailaction'];
        $query = "DELETE FROM ".ConnectionDB::ACTIONEMAIL." WHERE idEmailAction = :IDEMAILACTION";
        $result = $this->connection->execute($query,$values);

        return $result;
    }

	public function getProcessesBySource(int $idSource){

		$dataValues = array();
		$dataValues["IDSOURCE"] = $idSource;
		$query = "SELECT * FROM Process WHERE idSource = :IDSOURCE AND enabled = 1 order by priority asc";
		$processesFound = array();
		
		try {
			$result = $this->connection->execute($query, $dataValues);
			while($result->fetch()) {
				$process = new Process();
				$process->setIdProcess($result->idProcess);
				$process->setProcessName($result->processName);
				$process->setEnabled($result->enabled);
				$process->setDescription($result->description);
				$process->setCreator($result->creator);
				$process->setTimeStamp($result->timeStamp);
				$process->setIdSource($result->idSource);
				$process->setIdCATTool($result->idCATTools);
				
				if($process->getIdCATTool() !== null){
					switch($process->getIdCATTool()){
						case self::MEMOQTOOL:
							$process->setCATToolSetup($this->getMemoqSetupByProcess($result->idProcess));
							break;
						default:
							
							break;
					}
				}
				
				$ruleDao = new RuleDAO();
				$rules = $ruleDao->getRulesByProcess($process->getIdProcess());
				$process->setRules($rules);
				
				$scheduleDao = new ScheduleDAO();
				$schedules = $scheduleDao->getScheduleByProcess($process->getIdProcess());
				$process->setSchedules($schedules);
				
				$actions = $this->getActionsByProcess($process->getIdProcess());
				$process->setActions($actions);
				
				$notificationDao = new NotificationDAO();
				$notification = $notificationDao->getNotificationByProcess($process->getIdProcess());
				$actionsIDName = array();
				foreach($actions as $action) {
					$actionsIDName[] = array(
							"id" => $action->getIdAction(),
							"name" => $action->getActionName()
					);
				}
				$notification->setActionsData($actionsIDName);
				$process->setNotification($notification);
				
				$processesFound[] = $process;
			}
		} catch(SQLException $ex) {
			\Functions::addLog($ex->getMessage(), \Functions::SEVERE, self::class);
		} finally{
			$result->close();
		}
		
		return $processesFound;
	}

	public function updateActions($actions){
		$dataValues = array();
		$dataValues["sequence"] = $actions->sequence;
		$dataValues["enabled"] = $actions->enabled?'1':'0';
		$dataValues["onFallBack"] = $actions->onfallback?'1':'0';
		$dataValues["actionName"] = $actions->actionname;
		
		$whereValues = array();
		$whereValues["idProcess"] = $actions->idprocess;
		$whereValues["idActions"] = $actions->idaction;
		// return $dataValues;
		$action = "M";
		$table = "ProcessActions";
		$whereCondition = "idProcess = :IDPROCESS AND idActions = :IDACTIONS";
		try {
			$this->connection->setDefaultExecMode(DBCONN_TRANSACTION);
			$result = $this->connection->setValues($action, $table, $dataValues, $whereCondition, $whereValues);
			
			switch ($actions->actiontype) {
				case "create" :
					$result = $this->updateCreateAction($actions);
					break;
				case "confirmEmail" :
					$result = $this->updateConfirmationEmail($actions);
					break;
				case 'sendMail':
					$result = $this->updateSendMailAction($actions);
					break;
				case 'accept':
					$result =  array('result' => $result, 'data' => $dataValues);
					break;
				default :
					break;
			}
			$this->connection->commit();
		} catch (\Exception $e) {
			$this->connection->rollback();
		}finally{
			$this->connection->setDefaultExecMode(DBCONN_COMMIT_ON_SUCCESS);
		}
		
		if($actions->actiontype == 'create' && $result['result']){
			$actionSave = $this->getActionCreate($actions->idprocess,$actions->idaction);
			$result['action'] = $actionSave;
		}
		
		return $result;
	}

	/**
	 * Gets all actions for this process.
	 * The actions are sorted by their sequence asc.
	 * 
	 * @return Action[]
	 * {@inheritDoc}
	 * @see \dataAccess\interfaces\IProcess::getActionsByProcess()
	 */
	public function getActionsByProcess(int $idProcess){
		$actionsFound = array();
		
		
		//First, we will get all the ProcessActions stores on the DATA BASE.
		//Then, for each Action, we will get their own specific setup.
		$sql = "SELECT * FROM ProcessActions WHERE idProcess=:IDP order by sequence asc";
		$q = $this->connection->execute($sql, array("IDP" => $idProcess));
		while($q->fetch()){
			$action = null;
			switch($q->idActions){
				case Action::_TMS_CREATE_TASK: //Case Action CREATE
					$sql = "SELECT * FROM ".ConnectionDB::ACTIONCREATE." WHERE idProcess=:IDP";
					$q2 = $this->connection->execute($sql, array("IDP" => $idProcess));
					if($q2->fetch()){
						$action = new ActionCreate();
						
						//Basic ProcessAction property.
						$action->setIdAction($q->idActions);
						$action->setActionType("create");
						$action->setIdProcess($idProcess);
						$action->setSequence($q->sequence);
						$action->setEnabled(($q->enabled==1));
						$action->setOnFallBack(($q->onFallBack == 1));
						$action->setActionName($q->actionName);
						
						//Action "Create-TMS" Specific Properties.
						$action->setIdCreate($q2->idCreateAction);
						$action->setNameTask($q2->nameTask);
						$action->setDescription($q2->description);
						$action->setDueDate($q2->dueDate);
						$action->setUploadOriginalMessageAsReferenceFile(($q2->uploadOriginalMessageAsReferenceFile==1));
						$action->setEnableOverWrite(($q2->enableOverWrite==1));
						if ($action->getEnableOverWrite()) { //if WC Overwrite is enabled, get the specific WC analysis
							$action->setOverWrite($this->getOverwriteByCreateAction($action->getIdCreate()));
							$action->setMaxWords($action->getOverWrite()->getWeightedWordsTresshold()); //Get the maxWords threeshold where the Overwrite WC will be set to the TMS task.
							
						}
						$fuzzy_relation = $this->getFuzzyRelation($q2->idCreateAction,$idProcess);
						$file_relation = $this->getFileRelation($q2->idCreateAction,$idProcess);
						$action->setFuzzyOverwrite($fuzzy_relation);
						$action->setFileOverwrite($file_relation);
						//Get the property to add time of proof to the task
						$action->setAppendWCProofForecast($q2->appendWCProofForecast);
						$action->setAppendMachineTransTo($q2->appendMachineTransTo);
						$action->setAddIceWC($q2->addIceWC == 1);
						//Get the TMS Specific Properties set values.
						$action->setPropertyActions($this->getPropertiesByCreateAction($q2->idCreateAction));
						
						$actionsFound[] = $action;
					}
					$q2->close();
					break;
				case Action::_ACCEPT: //Case Action ACCEPT.
					$action = new \AcceptTask();
					//Basic ProcessAction property.
					$action->setIdAction($q->idActions);
					$action->setIdProcess($idProcess);
					$action->setActionType("accept");
					$action->setSequence($q->sequence);
					$action->setEnabled(($q->enabled==1));
					$action->setOnFallBack(($q->onFallBack == 1));
					$action->setActionName($q->actionName);
					
					//Action Accept does not have specific setup.
					
					$actionsFound[] = $action;
					
					break;
				case Action::_CONFIRMEMAIL: //Case Action CONFIRM EMAIL
					$sql = "SELECT * FROM ".ConnectionDB::ACTIONCONFIRMEMAIL." WHERE idProcess=:IDP";
					$q2 = $this->connection->execute($sql, array("IDP" => $idProcess));
					if($q2->fetch()){
						$action = new ConfirmEmailAction();
						
						//Basic ProcessAction property.
						$action->setIdAction($q->idActions);
						$action->setActionType("confirmEmail");
						$action->setIdProcess($idProcess);
						$action->setSequence($q->sequence);
						$action->setEnabled(($q->enabled==1));
						$action->setOnFallBack(($q->onFallBack == 1));
						$action->setActionName($q->actionName);
						
						//Action "Confirmemail" Specific Properties.
						$action->setIdConfirmEmail($q2->idConfirmEmail);
						$action->setAddBasicTaskInfo(($q2->addBasicTaskInformation==1));
						$action->setAddSourceFile(($q2->addSourceFile==1));
						$action->setSendTo($q2->sendTo);
						$action->setSubject($q2->subject);
						$actionsFound[] = $action;
					}
					$q2->close();
					break;
				case Action::_SENDMAIL: //Case Action SEND EMAIL
					$sql = "SELECT * FROM ".ConnectionDB::ACTIONEMAIL." WHERE idProcess=:IDP";
					$q2 = $this->connection->execute($sql, array("IDP" => $idProcess));
					if($q2->fetch()){
						$action = new EmailAction();
						//Basic ProcessAction property.
						$action->setIdAction($q2->idActions);
						$action->setActionType("sendMail");
						$action->setIdProcess($idProcess);
						$action->setSequence($q->sequence);
						$action->setEnabled(($q->enabled==1));
						$action->setOnFallBack(($q->onFallBack==1));
						$action->setActionName($q->actionName);
						
						//Action "SendMail" Specific Properties.
						$action->setIdEmailAction($q2->idEmailAction);
						$action->setContent($q2->emailContent);
						$action->setSubject($q2->subject);
						$action->setSendTo($q2->sendTo);
						$action->setIdEmailAction($q2->idEmailAction);
						$action->setAddBasicTaskInfo(($q2->addBasicTaskInfo==1));
						$action->setAddSourceFile(($q2->addSourceFile==1));
						$actionsFound[] = $action;
					}
					$q2->close();
					
					break;
			}
		}
		$q->close();
		
		return $actionsFound;
	}


	public function getFuzzyRelation($idCreateAction, $idProcess){
		//[][]
		$dataValues = array();
		$dataValues["IDCREATEACTION"] = $idCreateAction;
		$dataValues["IDPROCESS"] = $idProcess;
		$query = "SELECT * FROM ActionCreateCustomFuzzyRelation WHERE idCreateAction=:IDCREATEACTION and idProcess=:IDPROCESS";
		$result = $this->connection->execute($query, $dataValues);
		$fuzzy_relation = array();
		if ($result->fetch()) {
			//$analysis->setIdAnalis($result->IDWORDANALYSIS);
			$fuzzy_relation["idCreateAction"] = $result->IDCREATEACTION;
			$fuzzy_relation["idProcess"] = $result->IDPROCESS;
			$fuzzy_relation["percentage_101"] = $result->PERCENTAGE_101;
			$fuzzy_relation["repetition"] = $result->REPETITIONS;
			$fuzzy_relation["percentage_100"] = $result->PERCENTAGE_100;
			$fuzzy_relation["percentage_95"] = $result->PERCENTAGE_95;
			$fuzzy_relation["percentage_85"] = $result->PERCENTAGE_85;
			$fuzzy_relation["percentage_75"] = $result->PERCENTAGE_75;
			$fuzzy_relation["percentage_50"] = $result->PERCENTAGE_50;
			$fuzzy_relation["machineTranslation"] = $result->MACHINETRANSLATION;
			$fuzzy_relation["notMatch"] = $result->NOMATCH;
			$fuzzy_relation["minute"] = $result->MINUTE;
			$result->close();
		}else{
			return null;
		}
		return $fuzzy_relation;

	}

	public function getFileRelation($idCreateAction, $idProcess){
		//[][]
		$dataValues = array();
		$dataValues["IDCREATEACTION"] = $idCreateAction;
		$dataValues["IDPROCESS"] = $idProcess;
		$query = "SELECT * FROM ActionCreateCustomFileRelation WHERE idCreateAction=:IDCREATEACTION and idProcess=:IDPROCESS";
		$result = $this->connection->execute($query, $dataValues);
		$file_relation = array();
		if ($result->fetch()) {
			//$analysis->setIdAnalis($result->IDWORDANALYSIS);
			$file_relation["idCreateAction"] = $result->IDCREATEACTION;
			$file_relation["idProcess"] = $result->IDPROCESS;
			$file_relation["source_files"] = $result->SOURCE_FILES;
			$file_relation["reference_files"] = $result->reference_files;
			$file_relation["tm_files"] = $result->TM_FILES;
			$file_relation["compress_source_files"] = $result->compress_source_files;
			$file_relation["compress_reference_files"] = $result->compress_reference_files;
			$result->close();
		}else{
			return null;
		}
		return $file_relation;

	}
	
	public function getAditionalPropertiesForActionCreate(){
		$properties = array();
		$properties[] = array("id" => "area","name" => "areas", "vartype" => "string", "type" => "S");
		$properties[] = array("id" => "inici_previst","name" => "Inicio previsto", "vartype" => "date", "type" => "D");
		$properties[] = array("id" => "translator","name" => "translator", "vartype" => "string", "type" => "S");
		$properties[] = array("id" => "proofer","name" => "proofer", "vartype" => "string", "type" => "S");
		$properties[] = array("id" => "area2","name" => "area2", "vartype" => "string", "type" => "S");
		$properties[] = array("id" => "origin","name" => "origen", "vartype" => "string", "type" => "S");
		$properties[] = array("id" => "projecte","name" => "projecte", "vartype" => "date", "type" => "D");
		$properties[] = array("id" => "tipus_tasca","name" => "tipus_tasca", "vartype" => "string", "type" => "S");
		$properties[] = array("id" => "tarifa","name" => "tarifa", "vartype" => "string", "type" => "S");
		$properties[] = array("id" => "estat","name" => "estat", "vartype" => "int", "type" => "I");
		$properties[] = array("id" => "id_grup","name" => "id_grup", "vartype" => "int", "type" => "I");
		$properties[] = array("id" => "folderType","name" => "folderType", "vartype" => "string", "type" => "S");
		$properties[] = array("id" => "folderCreationNames","name" => "folderCreationNames", "vartype" => "string", "type" => "S");
		
		return $properties;
	}
	
	public function getPropertyRules($idSource) {
		$properties = $this->getAditionalProperties($idSource);
		$properties[] = array("id" => "title","name" => "Title", "vartype" => "string", "type" => "S");
		$properties[] = array("id" => "dueDate","name" => "Due Date", "vartype" => "date", "type" => "S");
		$properties[] = array("id" => "sourceLanguage","name" => "Source Language", "vartype" => "string", "type" => "S");
		$properties[] = array("id" => "targetLanguages","name" => "Target Languages", "vartype" => "string", "type" => "S");
		$properties[] = array("id" => "totalWords", "name" => "Max Words", "vartype" => "int", "type" => "S");
		$properties[] = array("id" => "user", "name" => "User", "vartype" => "string", "type" => "S");
		$properties[] = array("id" => "workgroup_max_load", "name" => "Workgroup max load", "vartype" => "int", "type" => "S");
		$properties[] = array("id" => "execution_type", "name" => "Execution type", "vartype" => "string", "type" => "S");
		
		return $properties;
	}
	
	public function getAditionalProperties(int $idSource) {
		$query = "SELECT * FROM SourceAditionalProperties WHERE idSource = :IDSOURCE";
		$dataValues = array();
		$dataValues["IDSOURCE"]= $idSource;
		$result = $this->connection->execute($query, $dataValues);
		
		$properties = array();
		while($result->fetch()){
			$prop = array();
			$prop['name'] = $result->propertyName;
			$prop['id'] = $result->propertyName;
			$properties[] = $prop;
		}
		$result->close();
		
		return $properties;
	}

	public function saveActions($actions){

		$this->connection->setDefaultExecMode(DBCONN_TRANSACTION);
		$dataValues = array();
		$dataValues["idProcess"] = $actions->idprocess;
		$dataValues["idActions"] = $actions->idaction;
		$dataValues["sequence"] = $actions->sequence;
		$dataValues["enabled"] = $actions->enabled?'1':'0';
		$dataValues["onFallBack"] = $actions->onfallback?'1':'0';
		$dataValues["actionName"] = $actions->actionname;
		$dataValues["actionName"] = $actions->actionname;
		// return $dataValues;
		try {
			$action = "C";
			$table = "ProcessActions";
			$result = $this->connection->setValues($action, $table, $dataValues);
			
			switch ($actions->actiontype) {
				case "confirmEmail" :
					$result = $this->saveConfirmEmailAction($actions);
					break;
				case "create" :
					$result = $this->saveCreateAction($actions);
					break;
				case "sendMail" :
					$result = $this->saveSendMailAction($actions);
					break;
				case "accept":
					$result = array("result" => $result);
					break;
				default :
					
					break;
			}
			$this->connection->commit();
		} catch (\Exception $e) {
			$this->connection->rollback();
			$result = array("result" => false, "message" => $e->getMessage());
		}finally{
			$this->connection->setDefaultExecMode(DBCONN_COMMIT_ON_SUCCESS);
		}
		
		if($actions->actiontype == 'create' && $result['result']){
			$actionSave = $this->getActionCreate($actions->idprocess,$actions->idaction);
			$result['action'] = $actionSave;
		}
		
		return $result;
	}

	public function getAllActions(){

	}

	/**
	 * Saves a CreateAction to the storage media, if necessary, it will save the aditional properties and the overWrite of the Create action,
	 * if and only if the variables PropertyAction and Overwrite are set and the values are correct and of the type expected, any trouble with
	 * those variables will not stop the process transaction and only will be ignored
	 *
	 * @param \stdClass $action
	 * @return
	 */
	private function saveCreateAction($action){
		$dataValues = array();
		$dataValues["nameTask"] = $action->nametask;
		$dataValues["dueDate"] = $action->duedate;
		$dataValues["enableOverwrite"] = $action->enableoverwrite?'1':'0';
		$dataValues["idProcess"] = $action->idprocess;
		$dataValues["idActions"] = $action->idaction;
		$dataValues["maxWords"] = $action->maxwords;
		$dataValues["description"] = $action->description;
		$dataValues['uploadoriginalmessageasreferencefile'] = $action->uploadoriginalmessageasreferencefile?'1':'0';
		$dataValues['dueDate'] = $action->duedate.'';
		$dataValues['appendWCProofForecast'] = $action->appendwcproofforecast?'1':'0';
		$dataValues['appendMachineTransTo'] = isset($action->appendmachinetransto)?$action->appendmachinetransto:null;
		$dataValues['addIceWC'] = $action->addicewc?'1':'0';
		
		$table = ConnectionDB::ACTIONCREATE;
		$idCreateAction = $this->connection->setValues(DBCONN_INSERT, $table, $dataValues);
// 		$idCreateAction = $this->getLastInsert();
		if ($action->enableoverwrite) {
			$overwrite = $action->overwrite;
			$dataValues = array();
			$dataValues["idCreateAction"] = $idCreateAction;
			$dataValues["percentage_101"] = $overwrite->percentage_101.'';
			$dataValues["repetitions"] = $overwrite->repetition.'';
			$dataValues["percentage_100"] = $overwrite->percentage_100.'';
			$dataValues["percentage_95"] = $overwrite->percentage_95.'';
			$dataValues["percentage_85"] = $overwrite->percentage_85.'';
			$dataValues["percentage_75"] = $overwrite->percentage_75.'';
			$dataValues["percentage_50"] = $overwrite->percentage_50.'';
			$dataValues["machineTranslation"] = $overwrite->machinetranslation.'';
			$dataValues["minute"] = $overwrite->minute.'';
			$dataValues["weightedWords"] = $overwrite->weightedwords.'';
			$dataValues["noMatch"] = $overwrite->notmatch.'';
			$dataValues["totalWords"] = ($overwrite->percentage_101 + $overwrite->repetition + $overwrite->percentage_100 + $overwrite->percentage_95 + $overwrite->percentage_85 + $overwrite->percentage_75 + $overwrite->percentage_50 + $overwrite->machinetranslation + $overwrite->notmatch).'';
			$dataValues["weightedWordsTresshold"] = $action->maxwords.'';
			
		}else{
			$dataValues = array();
			$dataValues["idCreateAction"] = $idCreateAction;
			$dataValues["percentage_101"] = '0';
			$dataValues["repetitions"] = '0';
			$dataValues["percentage_100"] = '0';
			$dataValues["percentage_95"] = '0';
			$dataValues["percentage_85"] = '0';
			$dataValues["percentage_75"] = '0';
			$dataValues["percentage_50"] = '0';
			$dataValues["machineTranslation"] = '0';
			$dataValues["minute"] = '0';
			$dataValues["weightedWords"] = '0';
			$dataValues["noMatch"] = '0';
			$dataValues["totalWords"] = '0';
			$dataValues["weightedWordsTresshold"] = '0';
			// $table = ConnectionDB::ACTIONCREATEWCOVERWRITE;
			// $idWordAnalysis = $this->connection->setValues(DBCONN_INSERT, $table, $dataValues);
		}
		$table = ConnectionDB::ACTIONCREATEWCOVERWRITE;
		$this->connection->setValues(DBCONN_INSERT, $table, $dataValues);
		
		// $properties = get_object_vars($action);
		// $poperties = $properties['propertyactions'];
		foreach ($action->propertyactions as $property) {
			$dataValues = array();
			$dataValues["propertyName"] = $property->propertyname;
			$dataValues["propertyValue"] = $property->propertyvalue;
			$dataValues["idCreateAction"] = $idCreateAction;
			$dataValues["propertyType"] = $property->propertytype;
			
			$table = ConnectionDB::ACTIONCREATEADDITIONALPROPERTIES;
			$this->connection->setValues(DBCONN_INSERT, $table, $dataValues);
		}

		
		if(isset($action->enablefuzzyrelation)  && $action->enablefuzzyrelation == true){
			$table = ConnectionDB::ACTIONCREATECUSTOMFUZZYRELATION;
			$dataValues = array();
			$overwriteFuzzy = $action->overwriteFuzzy;
			$dataValues["idCreateAction"] = $idCreateAction;
			$dataValues["idProcess"] = $action->idprocess;
			$dataValues["percentage_101"] = $overwriteFuzzy->percentage_101.'';
			$dataValues["repetitions"] = $overwriteFuzzy->repetition.'';
			$dataValues["percentage_100"] = $overwriteFuzzy->percentage_100.'';
			$dataValues["percentage_95"] = $overwriteFuzzy->percentage_95.'';
			$dataValues["percentage_85"] = $overwriteFuzzy->percentage_85.'';
			$dataValues["percentage_75"] = $overwriteFuzzy->percentage_75.'';
			$dataValues["percentage_50"] = $overwriteFuzzy->percentage_50.'';
			$dataValues["machineTranslation"] = $overwriteFuzzy->mt.'';
			$dataValues["minute"] = $overwriteFuzzy->min.'';
			$dataValues["noMatch"] = $overwriteFuzzy->nm.''; 
			$this->connection->setValues(DBCONN_INSERT, $table, $dataValues);
		}
		
		if(isset($action->enablefilerelation)  && $action->enablefilerelation == true){
			$table = ConnectionDB::ACTIONCREATECUSTOMFILERELATION;
			$dataValues = array();
			$overwriteFile = $action->overwriteFile;
			$dataValues["idCreateAction"] = $idCreateAction;
			$dataValues["idProcess"] = $action->idprocess;
			$dataValues["source_files"] = $overwriteFile->source_files.'';
			$dataValues["reference_files"] = $overwriteFile->reference_files.''; 
			$dataValues["tm_files"] = $overwriteFile->tm_files.''; 
				$compressFile = $action->compressFiles;
				$dataValues["compress_source_files"] = $compressFile->compress_source_files;
				$dataValues["compress_reference_files"] = $compressFile->compress_reference_files;
			$this->connection->setValues(DBCONN_INSERT, $table, $dataValues);
		}



		
		return array("result" => $idCreateAction, "description" => $action->actionname);
	}

	private function getActionCreate($idProcess,$idAction){
        $sql = "SELECT * FROM ProcessActions WHERE idProcess=:IDP AND idActions=:IDA";
        $q = $this->connection->execute($sql, array("IDP" => $idProcess,"IDA"=>$idAction));
        $q->fetch();
        $sql2 = "SELECT * FROM ".ConnectionDB::ACTIONCREATE." WHERE idProcess=:IDP";
        $q2 = $this->connection->execute($sql2, array("IDP" => $idProcess));
        if($q2->fetch()){
            $action = array();

            $action['aditionalProperties'] = $this->getAditionalPropertiesForActionCreate();
            $action['idaction']=$q->idActions;
            $action['actiontype'] = "create";
            $action['idprocess'] = $idProcess;
            $action['sequence'] = $q->sequence;
            $action['enabled'] = ($q->enabled==1);
            $action['onfallback'] = ($q->onFallBack == 1);
            $action['actionname'] = $q->actionName;
                        
                        //Action "Create-TMS" Specific Properties.
            $action['idcreateaction'] = $q2->idCreateAction;
            $action['nametask'] = $q2->nameTask;
            $action['description'] = $q2->description;
            $action['duedate'] = $q2->dueDate;
            $action['uploadoriginalmessageasreferencefile'] = ($q2->uploadOriginalMessageAsReferenceFile==1);
            $action['enableoverwrite'] = ($q2->enableOverWrite==1);
                if ($action['enableoverwrite']) { //if WC Overwrite is enabled, get the specific WC analysis
                    $action['overwrite'] = $this->getOverwriteByCreateAction($action['idcreateaction'])->toArray();
                    $action['maxwords'] = isset($action['overwrite']['maxwords'])?$action['overwrite']['maxwords']:null; //Get the maxWords threeshold where the Overwrite WC will be set to the TMS task.          
				}
			$fuzzyrelation = $this->getFuzzyRelationByCreateAction($action['idcreateaction'],$idProcess);
			if($fuzzyrelation != null){
				$action["overwriteFuzzy"] = $fuzzyrelation->toArray();
			}
			$filerelation = $this->getFileRelation($action['idcreateaction'],$idProcess);
			if($filerelation != null){
				$action["fileoverwrite"] = $filerelation;
			}

                        //Get the property to add time of proof to the task[][]
            $action['appendwcproofforecast'] = $q2->appendWCProofForecast;
            $action['appendmachinetransto'] =$q2->appendMachineTransTo;
                        //Get the TMS Specific Properties set values.
            $data = $this->getPropertiesByCreateAction($q2->idCreateAction);
            $action['propertyactions'] = array();
            foreach ($data as $value) {
            	$action['propertyactions'][] = $value->toArray();
            }
            // $action['propertyactions'] = $this->getPropertiesByCreateAction($q2->idCreateAction);
        }
        $q->close();
        $q2->close();

        return $action;
    }

	/**
	 * Updates the data of a create action, the object properties maxWords and description are optional and will not
	 * throw an error or exception.
	 * This method is used inside a transaction, if the action object has additional properties,
	 * said properties will be checked to make sure all the properties are set, but if not are set, the property will be ignored
	 * and will not generate an error, so the trasaction continue.
	 *
	 * @param ActionCreate $action
	 * @return
	 */
	private function updateCreateAction($action){

		$dataValues = array();
		$dataValues["nameTask"] = $action->nametask;
		$dataValues["dueDate"] = $action->duedate.'';
		$dataValues["enableOverwrite"] = ($action->enableoverwrite? '1':'0');
		$dataValues["maxWords"] = $action->maxwords;
		$dataValues["description"] = $action->description;
		$dataValues['dueDate'] = $action->duedate.'';
		$dataValues['uploadoriginalmessageasreferencefile'] = $action->uploadoriginalmessageasreferencefile?'1':'0';
		$dataValues['appendWCProofForecast'] = $action->appendwcproofforecast?'1':'0';
		$dataValues['appendMachineTransTo'] = isset($action->appendmachinetransto)?$action->appendmachinetransto:null;
		$dataValues['addIceWC'] = $action->addicewc?'1':'0';
		
		$whereValues = array();
		$whereValues["idProcess"] = $action->idprocess;
		$whereValues["idActions"] = $action->idaction;
		
		$table = ConnectionDB::ACTIONCREATE;
		$whereCondition = "idProcess = :IDPROCESS AND idActions = :IDACTIONS";
		$this->connection->setValues(DBCONN_UPDATE, $table, $dataValues, $whereCondition, $whereValues);
		
		if ($action->enableoverwrite) {
			$overwrite = $action->overwrite;
			$dataValues = array();
			$dataValues["percentage_101"] = $overwrite->percentage_101.'';
			$dataValues["repetitions"] = $overwrite->repetition.'';
			$dataValues["percentage_100"] = $overwrite->percentage_100.'';
			$dataValues["percentage_95"] = $overwrite->percentage_95.'';
			$dataValues["percentage_85"] = $overwrite->percentage_85.'';
			$dataValues["percentage_75"] = $overwrite->percentage_75.'';
			$dataValues["percentage_50"] = $overwrite->percentage_50.'';
			$dataValues["machineTranslation"] = $overwrite->machinetranslation.'';
			$dataValues["minute"] = $overwrite->minute.'';
			$dataValues["weightedWords"] = $overwrite->weightedwords.'';
			$dataValues["noMatch"] = $overwrite->notmatch.'';
			$dataValues["totalWords"] = ($overwrite->percentage_101 + $overwrite->repetition + $overwrite->percentage_100 + $overwrite->percentage_95 + $overwrite->percentage_85 + $overwrite->percentage_75 + $overwrite->percentage_50 + $overwrite->machinetranslation + $overwrite->notmatch).'';
			$dataValues["weightedWordsTresshold"] = $action->maxwords.'';
			$whereValues = array();
			$whereValues["idCreateAction"] = $action->idcreateaction;
			$table = ConnectionDB::ACTIONCREATEWCOVERWRITE;
			$whereCondition = "idCreateAction = :IDCREATEACTION";
			$this->connection->setValues(DBCONN_UPDATE, $table, $dataValues, $whereCondition, $whereValues);
		}
		if($action->enablefuzzyrelation){
			$table = ConnectionDB::ACTIONCREATECUSTOMFUZZYRELATION;
			$overwriteFuzzy = $action->overwriteFuzzy;
			$dataValues_insert = array(); 
			$dataValues = array(); 
			$dataValues_insert["idCreateAction"] = $action->idcreateaction;;
			$dataValues_insert["idProcess"] = $action->idprocess;
			$dataValues["percentage_101"] = $overwriteFuzzy->percentage_101.'';
			$dataValues["repetitions"] = $overwriteFuzzy->repetition.'';
			$dataValues["percentage_100"] = $overwriteFuzzy->percentage_100.'';
			$dataValues["percentage_95"] = $overwriteFuzzy->percentage_95.'';
			$dataValues["percentage_85"] = $overwriteFuzzy->percentage_85.'';
			$dataValues["percentage_75"] = $overwriteFuzzy->percentage_75.'';
			$dataValues["percentage_50"] = $overwriteFuzzy->percentage_50.'';
			$dataValues["machineTranslation"] = $overwriteFuzzy->mt.'';
			$dataValues["minute"] = $overwriteFuzzy->min.'';
			$dataValues["noMatch"] = $overwriteFuzzy->nm.''; 
			$whereCondition = "idCreateAction =:IDCREATEACTION and idProcess =:IDPROCESS";
			$whereValues = array();
			$whereValues["idCreateAction"] = $action->idcreateaction;
			$whereValues["idProcess"] = $action->idprocess;
			try{
				$data_insert = array_merge($dataValues,$dataValues_insert);
				$this->connection->setValues(DBCONN_INSERT, $table, $data_insert);
			}catch(SQLException $e){
                try {
                    $this->connection->setValues(DBCONN_UPDATE, $table, $dataValues, $whereCondition, $whereValues);
                } catch (SQLException $sqle) {
                    $ex = AIException::createInstanceFromSQLException($sqle)->construct(__METHOD__, __NAMESPACE__, $f = func_get_args());
                    Functions::logException($ex->getMessage(), Functions::WARNING, __CLASS__, $ex);
                }
			}
		}else{
			try {
				$whereValues["idCreateAction"] = $action->idcreateaction;
				$whereValues["idProcess"] = $action->idprocess;
				$query = "DELETE FROM ".ConnectionDB::ACTIONCREATECUSTOMFUZZYRELATION." WHERE idCreateAction = :idCreateAction and idProcess=:idProcess";
				$result = $this->connection->execute($query,$whereValues);
			} catch (SQLException $sqle) {
				
			}
		}
		if($action->enablefilerelation){
			unset($dataValues);
			unset($dataValues_insert);
			unset($whereValues);
			unset($whereCondition);
			$table = ConnectionDB::ACTIONCREATECUSTOMFILERELATION;
			$overwriteFile = $action->overwriteFile;
			$dataValues_insert["idCreateAction"] = $action->idcreateaction;;
			$dataValues_insert["idProcess"] = $action->idprocess;
			$dataValues["source_files"] = $overwriteFile->source_files.'';
			$dataValues["reference_files"] = $overwriteFile->reference_files.'';
			$dataValues["tm_files"] = $overwriteFile->tm_files.'';
			$whereValues = array();
			$whereValues["idCreateAction"] = $action->idcreateaction;
			$whereValues["idProcess"] = $action->idprocess;
			$whereCondition = "idCreateAction =:IDCREATEACTION and idProcess =:IDPROCESS";
			//if($action->enablefilecompress){
			$compressFile = $action->compressFiles;
			$dataValues["compress_source_files"] = $compressFile->compress_source_files;
			$dataValues["compress_reference_files"] = $compressFile->compress_reference_files;
			try{
				$data_insert = array_merge($dataValues,$dataValues_insert);
				$this->connection->setValues(DBCONN_INSERT, $table, $data_insert);
			}catch(SQLException $e){
                try {
                    $this->connection->setValues(DBCONN_UPDATE, $table, $dataValues, $whereCondition, $whereValues);
                } catch (SQLException $sqle) {
                    $ex = AIException::createInstanceFromSQLException($sqle)->construct(__METHOD__, __NAMESPACE__, $f = func_get_args());
                    Functions::logException($ex->getMessage(), Functions::WARNING, __CLASS__, $ex);
                }
            }
		}else{
			try {
				$whereValues["idCreateAction"] = $action->idcreateaction;
				$whereValues["idProcess"] = $action->idprocess;
				$query = "DELETE FROM ".ConnectionDB::ACTIONCREATECUSTOMFILERELATION." WHERE idCreateAction = :idCreateAction and idProcess=:idProcess";
				$result = $this->connection->execute($query,$whereValues);
			} catch (SQLException $sqle) {
				
			}
		}

		foreach ($action->propertiesDelete as $delete) {
			$this->deleteProperties($delete->id);
		}
		foreach ($action->propertyactions as $add) {
			$dataValues = array();
			$dataValues["propertyName"] = $add->propertyname;
			$dataValues["propertyValue"] = $add->propertyvalue;
			$dataValues["idCreateAction"] = $action->idcreateaction;
			$dataValues["propertyType"] = $add->propertytype;
			
			$table = ConnectionDB::ACTIONCREATEADDITIONALPROPERTIES;
			$result = $this->connection->setValues(DBCONN_INSERT, $table, $dataValues);
		}
		
		return array('result' => true);
	}

	private function deleteProperties($id){
        $query = "DELETE FROM ".ConnectionDB::ACTIONCREATEADDITIONALPROPERTIES." WHERE idPropertyAction = :ID";
        $whereValues = array("ID" => $id);

        $result = $this->connection->execute($query,$whereValues);

        return $result;
    }

	/**
	 * Saves a ConfirmEmail action on the storage media, it also will save the emails to wich a confirmation must be send, this will only happen
	 * if, and only if, the array containing the emails is not empty and the Email object has all its properties initialized, but any trouble with said
	 * object will not stop the process transaction and only will be ignored
	 *
	 * @param ConfirmEmailAction $action
	 * @return
	 */
	private function saveConfirmEmailAction($action){

		$dataValues = array();
		$dataValues["addBasicTaskinformation"] = $action->addbasictaskinfo?'1':'0';
		$dataValues["addSourceFile"] = $action->addsourcefile?'1':'0';
		$dataValues["idProcess"] = $action->idprocess;
		$dataValues["idActions"] = $action->idaction;
		$dataValues["sendTo"] = $action->sendto;
		$dataValues["subject"] = $action->subject;
		
		$tableAction = "C";
		$table = ConnectionDB::ACTIONCONFIRMEMAIL;
		$idConfirmEmail = $this->connection->setValues($tableAction, $table, $dataValues);
		
		return array('result' => $idConfirmEmail);
	}

	/**
	 * Updates a confirm email action, all the properties of the object must have been initialized, any error will stop
	 * the trasaction.
	 *
	 * @param ConfirmEmailAction $action
	 * @return
	 */
	private function updateConfirmationEmail($action){

		$dataValues = array();
		$dataValues["addBasicTaskinformation"] = $action->addbasictaskinfo?'1':'0';
		$dataValues["addSourceFile"] = $action->addsourcefile?'1':'0';
		$dataValues["sendTo"] = $action->sendto;
		$dataValues["subject"] = $action->subject;

		$whereValues = array();
		$whereValues["idProcess"] = $action->idprocess;
		$whereValues["idActions"] = $action->idaction;
		
		$table = ConnectionDB::ACTIONCONFIRMEMAIL;
		$whereCondition = "idProcess = :IDPROCESS AND idActions = :IDACTIONS";
		$result = $this->connection->setValues(DBCONN_UPDATE, $table, $dataValues, $whereCondition, $whereValues);
		
		return array('result' => $result);
	}

	/**
	 * Saves the data of a send mail action, all the properties of the object must have been initilized
	 *
	 * @param EmailAction $action
	 * @return void This method is used inside a transaction, so any error will trigger the rollback
	 */
	private function saveSendMailAction($action){

		$dataValues = array();
		$dataValues["emailContent"] = $action->emailcontent;
		$dataValues["subject"] = $action->subject;
		$dataValues["sendTo"] = $action->sendto;
		$dataValues["idProcess"] = $action->idprocess;
		$dataValues["idActions"] = $action->idaction;
		// $dataValues["addBasicTaskInfo"] = $action->addbasictaskinfo;
		// $dataValues["addSourceFile"] = $action->addsourcefile;
		$dataValues["addBasicTaskInfo"] = $action->addbasictaskinfo?'1':'0';
		$dataValues["addSourceFile"] = $action->addsourcefile?'1':'0';
		
		$table = ConnectionDB::ACTIONEMAIL;
		$result = $this->connection->setValues(DBCONN_INSERT, $table, $dataValues);
		
		return array('result' => $result);
	}

	/**
	 * Updates a send mail action, all the properties must have been intialized or an error will be thrown and the
	 * transaction will stop.
	 *
	 * @param EmailAction $action
	 * @return
	 */
	private function updateSendMailAction($action){

		$dataValues = array();
		$dataValues["emailContent"] = isset($action->emailcontent)?$action->emailcontent:'empty';
		$dataValues["subject"] = $action->subject;
		$dataValues["sendTo"] = $action->sendto;
		// $dataValues["addBasicTaskInfo"] = $action->addbasictaskinfo;
		// $dataValues["addSourceFile"] = $action->addsourcefile;
		$dataValues["addBasicTaskInfo"] = $action->addbasictaskinfo?'1':'0';
		$dataValues["addSourceFile"] = $action->addsourcefile?'1':'0';
		$whereValues = array();
		$whereValues["idProcess"] = $action->idprocess;
		$whereValues["idActions"] = $action->idaction;
		
		$table = ConnectionDB::ACTIONEMAIL;
		$whereCondition = "idProcess = :IDPROCESS AND idActions = :IDACTIONS";
		$result = $this->connection->setValues(DBCONN_UPDATE, $table, $dataValues, $whereCondition, $whereValues);
		
		return array('result' => $result);
	}

	/**
	 * Gets the additional properties of the action, the returned array might be empty if no properties
	 * are found
	 *
	 * @param int $idCreateAction
	 * @return \model\PropertyAction[]
	 */
	private function getPropertiesByCreateAction(int $idCreateAction){

		$dataValues = array();
		$dataValues["IDCREATEACTION"] = $idCreateAction;
		$query = "SELECT * FROM " .ConnectionDB::ACTIONCREATEADDITIONALPROPERTIES." WHERE idCreateAction = :IDCREATEACTION";
		$propertiesFound = array();
		
		$result = $this->connection->execute($query, $dataValues);
		
		while($result->fetch()) {
			$property = new PropertyAction();
			$property->setIdProperty($result->idPropertyAction);
			$property->setPropertyName($result->propertyName);
            if(isset(AI::getInstance()->getBehavior()->getOndemandConfiguration()[$property->getPropertyName()])){
                $property->setPropetyValue(AI::getInstance()->getBehavior()->getOndemandConfiguration()[$property->getPropertyName()]);
            }else{
                $property->setPropetyValue($result->propertyValue);
            }
			$property->setIdCreate($result->idCreateAction);
			$property->setPropertyType($result->propertyType);
			$propertiesFound[] = $property;
		}
		$result->close();

		return $propertiesFound;
	}
	
	public function getValueProperty($property,$propertyValue){
        $values = '';
        switch ($property) {
            case 'folderType':
                $values = $this->getValueFolderType($propertyValue);
                break;
            case 'estat':
                $values = $this->getValueEstat($propertyValue);
                break;
            case 'folderCreationNames':
            	$values = $this->getValueFolder($propertyValue);
            	break;
        }

        return $values;
    }
    
    private function getValueFolderType($value){
        switch ($value) {
            case 0:
                return 'NORMAL';
                break;
            
            case 1:
                return 'ESPECIAL';
                break;
        }
    }

    private function getValueEstat($value){
        switch ($value){
            case 1:
                return "ASSIGNED";
            case 2:
                return "READY";
            case 3:
                return "WORKING";
            case 4:
                return "DELIVERED";
            case 5:
                return "RECEIVED";
            case 6:
                return "CLOSED";
        }
    }

    private function getValueFolder($value){
    	switch ($value) {
    		case 1:
                return "NO FOLDER";
            case 2:
                return "FOLDER TASK NAME";
            case 3:
                return "FOLDER BATCH COUNTER";
            case 4:
                return "FOLDER DATE";
            case 5:
                return "FOLDER DATA TIME";
    	}
    }

	/**
	 * Gets the overwrite related to a create action
	 *
	 * @param int $idWordAnalysis
	 * @return \model\Analysis
	 */
	private function getOverwriteByCreateAction(int $idCreateAction){
		//TODO::las líneas comentadas es código anterior que borraré una vez que todo funcione bien, just in case
		$dataValues = array();
		//$dataValues["IDWORDANALYSIS"] = $idWordAnalysis;
		$dataValues["IDCREATEACTION"] = $idCreateAction;
		//$query = "SELECT * FROM WordAnalysis WHERE idWordAnalysis = :IDWORDANALYSIS";
		$query = "SELECT * FROM ActionCreateWCOverwrite WHERE idCreateAction = :IDCREATEACTION";
		$result = $this->connection->execute($query, $dataValues);
		$analysis = new Analysis();
		if ($result->fetch()) {
			
			//$analysis->setIdAnalis($result->IDWORDANALYSIS);
			$analysis->setIdCreateAction($result->IDCREATEACTION);
			$analysis->setRepetition($result->REPETITIONS);
			$analysis->setTotalWords($result->TOTALWORDS);
			$analysis->setMinute($result->MINUTE);
			$analysis->setWeightedWord($result->WEIGHTEDWORDS);
			$analysis->setNotMatch($result->NOMATCH);
			$analysis->setMachineTanslation($result->MACHINETRANSLATION);
			$analysis->setPercentage_101($result->PERCENTAGE_101);
			$analysis->setPercentage_100($result->PERCENTAGE_100);
			$analysis->setPercentage_95($result->PERCENTAGE_95);
			$analysis->setPercentage_85($result->PERCENTAGE_85);
			$analysis->setPercentage_75($result->PERCENTAGE_75);
			$analysis->setPercentage_50($result->PERCENTAGE_50);
			$analysis->setWeightedWordsTresshold($result->WEIGHTEDWORDSTRESSHOLD);
		}
		$result->close();
		return $analysis;
	}


	/**
	 * Gets the fuzzy relation related to a create action
	 *
	 * @param int $idWordAnalysis
	 * @return \model\Analysis
	 */
	private function getFuzzyRelationByCreateAction(int $idCreateAction, int $idProcess){
		$dataValues = array();
		//$dataValues["IDWORDANALYSIS"] = $idWordAnalysis;
		$dataValues["IDCREATEACTION"] = $idCreateAction;
		$dataValues["IDPROCESS"] = $idProcess;
		//$query = "SELECT * FROM WordAnalysis WHERE idWordAnalysis = :IDWORDANALYSIS";
		$query = "SELECT * FROM ActionCreateCustomFuzzyRelation WHERE idCreateAction=:IDCREATEACTION and idProcess=:IDPROCESS";
		$result = $this->connection->execute($query, $dataValues);
		
		$analysis = new Analysis();
		if ($result->fetch()) {
			
			//$analysis->setIdAnalis($result->IDWORDANALYSIS);
			$analysis->setIdCreateAction($result->IDCREATEACTION);
			$analysis->setRepetition($result->REPETITIONS);
			
			$analysis->setMinute($result->MINUTE);
			
			$analysis->setNotMatch($result->NOMATCH);
			$analysis->setMachineTanslation($result->MACHINETRANSLATION);
			$analysis->setPercentage_101($result->PERCENTAGE_101);
			$analysis->setPercentage_100($result->PERCENTAGE_100);
			$analysis->setPercentage_95($result->PERCENTAGE_95);
			$analysis->setPercentage_85($result->PERCENTAGE_85);
			$analysis->setPercentage_75($result->PERCENTAGE_75);
			$analysis->setPercentage_50($result->PERCENTAGE_50);
			
			$result->close();
		}else{
			return null;
		}
		return $analysis;
	}

	/**
	 * Saves data related to task confirmations, will generate a token as a md5 for each configured email and store
	 * the Date when the confirmation is stored.
	 *
	 * @param string $uniqueSourceId
	 * @param int $idConfirmEmail
	 * @param string $sendTo
	 * @return boolean
	 */
	public function saveTaskConfirmation(string $uniqueSourceId, int $idConfirmEmail, string $sendTo){
        $query = "SELECT * FROM ".ConnectionDB::ACTIONCONFIRMEMAILFEEDBACK. " WHERE uniqueSourceId = :USID";
        $dataValues = array();
        $dataValues["USID"] = $uniqueSourceId;
        $rs = $this->connection->execute($query, $dataValues);
        if(!$rs->fetch()){
            $result = false;
            $dataValues = array();
            $dataValues["uniqueSourceId"] = $uniqueSourceId;
            $dataValues["idConfirmEmail"] = $idConfirmEmail;
            $dataValues["createdOn"] = \Functions::currentDate();

            $table = ConnectionDB::ACTIONCONFIRMEMAILFEEDBACK;
            try {
                $this->connection->setDefaultExecMode(DBCONN_TRANSACTION);
                $result = $this->connection->setValues(DBCONN_INSERT, $table, $dataValues);

                $mails = explode(",", $sendTo);
                $table = "ConfirmationToken";

                foreach($mails as $email) {
                    $dataValues = array();
                    $dataValues["token"] = md5($uniqueSourceId . $email);
                    $dataValues["uniqueSourceId"] = $uniqueSourceId;
                    $dataValues["idConfirmEmail"] = $idConfirmEmail;
                    $dataValues["tokenEmail"] = $email;
                    $result = $this->connection->setValues(DBCONN_INSERT, $table, $dataValues);
                }
                $this->connection->commit();
            } catch(SQLException $ex) {
                $this->connection->rollback();
                \Functions::addLog($ex->getMessage(), \Functions::SEVERE, json_encode($ex->getTrace()));
                $result = false;
            } finally {
                $this->connection->setDefaultExecMode(DBCONN_COMMIT_ON_SUCCESS);
            }
        }else{
            $result = true;
        }

		return $result;
	}

	/**
	 * Gets the encrypted info of the token, this will return the email, the uniqueSourceId and the email
	 * useful for confirm the task
	 *
	 * @param string $token
	 * @return boolean|NULL[]
	 * @return
	 */
	public function getTokenEmail(string $token){

		$dataValues = array();
		$dataValues["token"] = $token;
		
		$query = "SELECT * FROM ConfirmationToken WHERE token = :TOKEN";
		
		$dataFound = array();
		try {
			$result = $this->connection->execute($query, $dataValues);
			if ($result->fetch()) {
				$dataFound["token"] = $result->token;
				$dataFound["uniqueSourceId"] = $result->uniqueSourceId;
				$dataFound["idConfirmEmail"] = $result->idConfirmEmail;
				$dataFound["tokenEmail"] = $result->tokenEmail;
			}
		} catch(SQLException $ex) {
			\Functions::addLog($ex->getMessage(), \Functions::SEVERE, json_encode($ex->getTrace()));
		} finally{
			$result->close();
		}
		
		return (count($dataFound) > 0 ? $dataFound : false);
	}

	/**
	 * Saves who confirmed the task and adds a time stamp
	 *
	 * @param string $uniqueSourceId
	 * @param int $idConfirmEmail
	 * @param string $email
	 * @return boolean
	 * @return
	 */
	public function confirmTask(string $uniqueSourceId, int $idConfirmEmail, string $email){

		try {
			
			$query = "SELECT confirmedBy FROM ".ConnectionDB::ACTIONCONFIRMEMAILFEEDBACK." WHERE uniqueSourceId = :uniqueSourceId AND idConfirmEmail = :idConfirmEmail";
			$whereValues = array();
			$whereValues["uniqueSourceId"] = $uniqueSourceId;
			$whereValues["idConfirmEmail"] = $idConfirmEmail;
			
			$result = $this->connection->execute($query, $whereValues);
			$answer = false;
			$description = "";
			if ($result->fetch() && $result->confirmedBy !== null) {
				$description = "Task already confirmed";
				$result->close();
			} else {
				// $result->close();
				$dataValues = array();
				$dataValues["confirmedBy"] = $email;
				$dataValues["confirmedOn"] = \Functions::currentDate();
				
				$table = ConnectionDB::ACTIONCONFIRMEMAILFEEDBACK;
				$whereCondition = "uniqueSourceId = :UNIQUESOURCEID AND idConfirmEmail = :IDCONFIRMEMAIL";
				
				$this->connection->setDefaultExecMode(DBCONN_TRANSACTION);
				$result = false;
				$resultState = false;
				
				$taskDAO = new TaskDAO();
				$task = $taskDAO->getTask($uniqueSourceId);
				if ($task->getState()->getIdState() == State::PENDINGCONFIRMATION) {
					$result = $this->connection->setValues(DBCONN_UPDATE, $table, $dataValues, $whereCondition, $whereValues);
					
					$resultState = $taskDAO->updateTaskState($task, State::CONFIRMED, Behavior::TASKCONFIRMED.$email, Action::_CONFIRMEMAIL, 0, false);
					$this->connection->commit();
					
					if ($result && $resultState) {
						$answer = true;
						$description = "Task confirmed successfully";
					}
				}else{
					throw new SQLException("Task rejected by confirm timeout", 500);
					$description = "Task rejected by confirm timeout";
				}
			}
		} catch(SQLException $ex) {
			$this->connection->rollback();
			\Functions::addLog($ex->getMessage(), \Functions::SEVERE, json_encode($ex->getTrace()));
		} finally{
			$this->connection->setDefaultExecMode(DBCONN_COMMIT_ON_SUCCESS);
		}
		$status = array();
		$status["answer"] = $answer;
		$status["description"] = $description;
		return $status;
	}

	/**
	 * Gets the process in wich a task was associated and confirmed, this method should be called when a task is confirmed
	 * to resume the actions that have not been executed.
	 * This method will return an array containing two variables, wich
	 * can be accessed by the keys ["sequence"] and ["process"]. The array might be empty if no data are found.
	 *
	 * @param string $uniqueSourceId
	 * @return \model\Process[]|int[]
	 * @return
	 */
	public function getProcessOfConfirmedTask(string $uniqueSourceId){

		try {
			$query = "SELECT idConfirmEmail FROM ".ConnectionDB::ACTIONCONFIRMEMAILFEEDBACK." WHERE uniqueSourceId = :UNIQUESOURCEID";
			$dataValues = array();
			$dataValues["UNIQUESOURCEID"] = $uniqueSourceId;
			$result = $this->connection->execute($query, $dataValues);
			$idConfirmEmail = 0;
			$dataFound = array();
			
			if ($result->fetch()) {
				if ($result->idConfirmEmail !== null) {
					$idConfirmEmail = $result->idConfirmEmail;
					$query = "SELECT idProcess, idActions FROM ".ConnectionDB::ACTIONCONFIRMEMAIL." WHERE idConfirmEmail = :IDCONFIRMEMAIL";
					$dataValues = array();
					$dataValues["IDCONFIRMEMAIL"] = $idConfirmEmail;
					$result = $this->connection->execute($query, $dataValues);
					$idProcess = $result->idProcess;
					$idActions = $result->idActions;
					$result->close();
					
					$query = "SELECT sequence FROM ProcessActions WHERE idProcess = :IDPROCESS AND idActions = :IDACTIONS AND enabled = 1";
					$dataValues = array();
					$dataValues["IDPROCESS"] = $idProcess;
					$dataValues["IDACTIONS"] = $idActions;
					$result = $this->connection->execute($query, $dataValues);
					if ($result->fetch()) {
						$sequence = $result->sequence;
						$dataFound["sequence"] = $sequence;
						$process = $this->getProcess($idProcess);
						$dataFound["process"] = $process;
					}
					$result->close();
				}
			}
		} catch(SQLException $ex) {
			\Functions::addLog($ex->getMessage(), \Functions::SEVERE, json_encode($ex->getTrace()));
		} finally{
			// $result->close();
		}
		return $dataFound;
	}

	/**
	 * Gets the sequence of an action in a certain process, useful to resume an interrupted process.
	 *
	 * @param int $idProcess
	 * @param int $idActions
	 * @return number
	 */
	public function getSequence(int $idProcess, int $idActions){

		$query = "SELECT sequence FROM ProcessActions WHERE idProcess = :IDPROCESS AND idActions = :IDACTIONS AND enabled = 1";
		$dataValues = array();
		$dataValues["IDPROCESS"] = $idProcess;
		$dataValues["IDACTIONS"] = $idActions;
		$result = $this->connection->execute($query, $dataValues);
		$sequence = 0;
		if ($result->fetch()) {
			$sequence = $result->sequence;
		}
		$result->close();
		return (int)$sequence;
	}
	
	public function getMemoqSetupByProcess($idProcess){
		$toolSetup = null;
		$query = "SELECT * FROM ".ConnectionDB::MEMOQTOOLSETUP." WHERE idProcess = :IDPROCESS";
		$dataValues = array();
		$dataValues["IDPROCESS"] = $idProcess;
		$result = $this->connection->execute($query, $dataValues);
		if($result->fetch()){
			$toolSetup = new MemoQToolSetup();
			$toolSetup->setIdProcess($idProcess);
			$toolSetup->setFeedTM($result->feedTM==1);
			$toolSetup->setTmName($result->tmName);
			$toolSetup->setTmGuid($result->tmGuid);
			$toolSetup->setUploadSourceFileToProject($result->uploadSourceFileToProject==1);
			$toolSetup->setProjectName($result->projectName);
			$toolSetup->setProjectGuid($result->projectGuid);
			$toolSetup->setAnalyzeSourceFile($result->analyzeSourceFile==1);
			$toolSetup->setUpdateTaskWordCount($result->updateTaskWordCount==1);
		}
		$result->close();
		return $toolSetup;
	}

	public function getGlobalWorkLoad(){
//        $query = "SELECT current_load FROM GlobalWorkLoad WHERE id = 1";
        $query = "SELECT * FROM GlobalWorkLoad WHERE id = :id";
        $v = array("id" => 1);
        $result = $this->connection->execute($query, $v);
        $r = false;
        if($result->fetch()){
            $r = $result->currentLoad;
        }
        $result->close();
        return $r;
    }

    /**
     * @param $idProcess
     * @return PropertyAction[]
     */
    public function getActionCreatePropertiesByProcess($idProcess){
        $propertiesFound = array();
        $query = "select * from AI.ActionCreateAdditionalProperties where idCreateAction in (select idCreateAction from AI.ActionCreate where idProcess = :idProcess)";
        $vars = array("idProcess" => $idProcess);
        try{
            $result = $this->connection->execute($query, $vars);
            while($result->fetch()) {
                $property = new PropertyAction();
                $property->setIdProperty($result->idPropertyAction);
                $property->setPropertyName($result->propertyName);
                $property->setPropetyValue($result->propertyValue);
                $property->setIdCreate($result->idCreateAction);
                $property->setPropertyType($result->propertyType);
                $propertiesFound[] = $property;
            }
            $result->close();
        } catch(Throwable $t){
            $ex = AIException::createInstanceFromThrowable($t)->construct(__METHOD__,__NAMESPACE__, $f = func_get_args());
            Functions::logException($t->getMessage(), Functions::WARNING, __CLASS__, $ex);
        }

        return $propertiesFound;
    }

    /**
     * @param int $newValue
     * @throws AIException
     */
    public function setGlobalWorkLoadCount($newValue){
        $vars = array("currentLoad" => $newValue);
        $whereValues = array("id" => 1);
        $whereCondition = "id = :id";
        $result = false;
        try {

            $result = $this->connection->setValues(DBCONN_UPDATE, ConnectionDB::GLOBALWORKLOAD, $vars, $whereCondition, $whereValues);
        } catch (SQLException $ex){
            $e = AIException::createInstanceFromSQLException($ex)->construct(__METHOD__, __NAMESPACE__, $f = func_get_args());
            throw $e;
        }
        return $result;
    }

    public function processHasWorkgroupMaxLoadRule($idProcess){
        $sql = "select * from ProcessRules where idProcess = :idProcess && description = 'group_max_load'";
        $vars = array("idProcess" => $idProcess);
        try{
            $result = $this->connection->execute($sql, $vars);
            while($result->fetch()) {
                $result->close();
                return true;
            }
            $result->close();
        } catch(Throwable $t){
            $ex = AIException::createInstanceFromThrowable($t)->construct(__METHOD__,__NAMESPACE__, $f = func_get_args());
            Functions::logException($t->getMessage(), Functions::WARNING, __CLASS__, $ex);
        }
        return false;
    }
}
?>