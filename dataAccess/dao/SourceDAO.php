<?php

/**
* @author Ljimenez
* @version 1.0
* created on 22 jun. 2018
*/
namespace dataAccess\dao;

use dataAccess\interfaces\ISource;
use dataAccess\SQLException;
use model\SourceAditonalProperty;
use dataAccess\ConnectionDB;
use dataAccess\dbConn;
use model\Source;
use model\APIUser;

include_once BaseDir. '/dataAccess/interfaces/ISource.php';
include_once BaseDir. '/model/Source.php';
include_once BaseDir. '/model/APIUser.php';
include_once BaseDir.'/model/SourceAditonalProperty.php';

class SourceDAO implements ISource{
	
	/**
	 * 
	 * @var dbConn
	 */
	private $connection;
	
	public function __construct(){
	    $dataBase = new ConnectionDB();
		$this->connection = $dataBase->getConnection();
	}
	
	public function getAditionalProperties(int $idSource) {
		$query = "SELECT * FROM SourceAditionalProperties WHERE idSource = :IDSOURCE";
		$dataValues = array();
		$dataValues["IDSOURCE"]= $idSource;
		$result = $this->connection->execute($query, $dataValues);
		
		$properties = array();
		while($result->fetch()){
			$prop = new SourceAditonalProperty();
			$prop->setIdSourceAditionalProperty($result->idSourceAditionalProperties);
			$prop->setPropertyName($result->propertyName);
			$prop->setPropertyValue($result->propertyValue);
			$prop->setIdSource($result->idSource);
			$properties[] = $prop;
		}
		
		$result->close();
		return $properties;
	}

	public function editPropertyAction(SourceAditonalProperty $aditionalProperty) {
		
	}

	public function getUsersSources(int $idSource) {
		$users = array();
		$query = "SELECT * FROM APIUser WHERE idSource = :IDSOURCE ORDER BY priority ASC";
		$parameters = array();
		$parameters["IDSOURCE"] = $idSource;
		
		$result = $this->connection->execute($query,$parameters);
		
		while($result->fetch()){
			$user = new APIUser();
			$user->setIdApiUser($result->IDAPIUSER);
			$user->setPassWord($result->PASSWORD);
			$user->setUserName($result->USERNAME);
			$user->setToken($result->TOKEN);
			$user->setPriority($result->PRIORITY);
			$user->setClientSecret($result->CLIENTSECRET);
			$user->setTimeLife($result->EXPIRESIN);
			$users[] = $user;
		}
		
		$result->close();
		return $users;
	}

    /**
     * @return array Source list
     * @throws SQLException
     */
    public function getSources() {
		$sources = array();
		$query = "SELECT * FROM Source ORDER BY priority ASC";
		$result = $this->connection->execute($query);

		while($result->fetch()){
			$source = new Source();
			$source->setIdSource($result->getVal("idSource"));
			$source->setSourceName($result->getVal("sourceName"));
			$source->setDownload($result->getVal("download"));
			$source->setPriority($result->getVal("priority"));
			$source->setClassName($result->getVal("className"));
			$source->setAdditionalProperties($this->getAditionalProperties($source->getIdSource()));
			$sources[] = $source;
		}
		
		$result->close();
		return $sources;
	}

	public function saveSourceAditionalProperty(SourceAditonalProperty $aditionalProperty) {
		
	}

	public function deleteAditionalPropertyAction(int $idAditonalProperty) {
		
	}
	
	/**
	 * Get source by ID
	 * @param int $sourceId
	 * @return NULL|\model\Source
	 * @return
	 */
	public function getSourceById(int $sourceId){
		$values = array();
		$values["ID"] = $sourceId;
		$query = 'SELECT * FROM Source WHERE idSource = :ID';
		$result = $this->connection->execute($query, $values);
		$source = null;
		if($result->fetch()){
			$source = new Source();
			$source->setIdSource($result->IDSOURCE);
			$source->setSourceName($result->SOURCENAME);
			$source->setDownload($result->DOWNLOAD);
			$source->setPriority($result->PRIORITY);
			$source->setDescription($result->DESCRIPTION);
            $source->setClassName($result->CLASSNAME);
			$source->setAdditionalProperties($this->getAditionalProperties($result->idSource));
		}
		
		$result->close();
		return $source;
	}
	
	
	public function getSourceByName(string $sourceName){
		$values = array();
		$values["NAME"] = $sourceName;
		$query = 'SELECT * FROM Source WHERE sourceName = :NAME';
		
		$result = $this->connection->execute($query,$values);
		$source = null;
		if($result->fetch()){
			$source = new Source();
			$source->setIdSource($result->IDSOURCE);
			$source->setSourceName($result->SOURCENAME);
			$source->setDownload($result->DOWNLOAD);
			$source->setPriority($result->PRIORITY);
			$source->setDescription($result->DESCRIPTION);
            $source->setClassName($result->CLASSNAME);
			$source->setAdditionalProperties($this->getAditionalProperties($result->idSource));
		}
		
		$result->close();
		return $source;
	}
	
	public function getApiUser($idApiUser) {
		$values = array();
		$values["idAPIUser"] = $idApiUser;
		
		$query = 'SELECT * FROM APIUser WHERE idAPIUser = :IDAPIUSER';
		
		$result = $this->connection->execute($query,$values);
		$user = null;
		if($result->fetch()){
			$user = new APIUser();
			$user->setIdApiUser($result->IDAPIUSER);
			$user->setPassWord($result->PASSWORD);
			$user->setUserName($result->USERNAME);
			$user->setToken($result->TOKEN);
			$user->setPriority($result->PRIORITY);
			$user->setClientSecret($result->CLIENTSECRET);
			$user->setTimeLife($result->EXPIRESIN);
		}
		
		$result->close();
		
		return $user;
	}
	
	public function saveLastExecution(int $idSource, string $executeDate) {
		$values = array();
		$values["lastExecution"] = $executeDate;
		
		$whereValues = array();
		$whereValues["IDSOURCE"] = $idSource;
		
		$whereCondition = 'idSource = :IDSOURCE';
		
		$result = $this->connection->setValues(DBCONN_UPDATE, ConnectionDB::SOURCE, $values,$whereCondition,$whereValues);
		
		return $result;
	}

	public function saveEndExecution(int $idSource, string $executeDate = null) {
		$values = array();
		$values["endExecution"] = $executeDate;

		$whereValues = array();
		$whereValues["IDSOURCE"] = $idSource;

		$whereCondition = 'idSource = :IDSOURCE';

		$result = $this->connection->setValues(DBCONN_UPDATE, ConnectionDB::SOURCE, $values,$whereCondition,$whereValues);

		return $result;
	}

	public function saveStartExecution(int $idSource, string $executeDate) {
		$values = array();
		$values["startExecution"] = $executeDate;
		$values["endExecution"] = null;
		$whereValues = array();
		$whereValues["IDSOURCE"] = $idSource;

		$whereCondition = 'idSource = :IDSOURCE';

		$result = $this->connection->setValues(DBCONN_UPDATE, ConnectionDB::SOURCE, $values,$whereCondition,$whereValues);

		return $result;
	}

    /**
     * @return array|Source[]
     * @throws SQLException
     */
    public function getSourceToExecute() {
		$sources = array();
		//$query = "SELECT * FROM Source WHERE timeInterval >= 0 and DATE_ADD(lastExecution,INTERVAL timeInterval MINUTE) <= NOW() ORDER BY PRIORITY ASC";
		//$query = "SELECT * FROM Source WHERE timeInterval >= 0 and DATE_ADD(lastExecution,INTERVAL timeInterval MINUTE) <= UTC_TIMESTAMP() ORDER BY PRIORITY ASC";
		$query = "SELECT * FROM Source WHERE timeInterval >= 0 and (lastExecution is null or endExecution is null or DATE_ADD(lastExecution,INTERVAL timeInterval MINUTE) <= UTC_TIMESTAMP()) ORDER BY PRIORITY ASC";
		$result = $this->connection->execute($query);
		
		while($result->fetch()){
			$source = new Source();
			$source->setIdSource($result -> getVal("idSource"));
			$source->setSourceName($result->getVal("sourceName"));
			$source->setDownload($result->getVal("download"));
			$source->setPriority($result->getVal("priority"));
            $source->setClassName($result->getVal("className"));
			$source->setAdditionalProperties($this->getAditionalProperties($result->getVal("idSource")));
			$sources[] = $source;
		}
		
		$result->close();
		return $sources;
	}
	
	public function updateToken($idUser, $token, $time){
		$values = array();
		$date = new \DateTime();
		$date->modify($time);
		$values["EXPIRESIN"] = array($date->format("Y-m-d H:i:s"),DBCONN_DATATYPE_DATE);
		$values["TOKEN"] = $token;
		
		$whereValues = array();
		$whereValues["IDAPIUSER"] = $idUser;
		
		$whereCondition = 'idAPIUser = :IDAPIUSER';
		
		$result = $this->connection->setValues(DBCONN_UPDATE, 'APIUser', $values,$whereCondition,$whereValues);
		
		return $result;
	}


	
}
?>