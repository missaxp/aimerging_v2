<?php

/**
* @author zroque
* @version 1.0
* created on 26 jun. 2018
*/
namespace dataAccess\dao;

use dataAccess\interfaces\IRule;
use model\Rule;
use dataAccess\dbConn;
use dataAccess\ConnectionDB;

include_once BaseDir. '/dataAccess/interfaces/IRule.php';
include_once BaseDir. '/model/Rule.php';
//include 'dataAccess/dbConn.php';
//include 'dataAccess/ConnectionDB.php';

class RuleDAO implements IRule {
	
	/**
	 *
	 * @var dbConn
	 */
	private $connection;

	private $rest;
	
	public function __construct($rest = false) {
		$this->rest = $rest;
		$dataBase = new ConnectionDB();
		$this->connection = $dataBase->getConnection();
	}
	
	public function generateRule($rule){
		$newRule = new Rule();
		
		$newRule->setDescription($rule->description);
		$newRule->setWeight($rule->weight.'');
		$newRule->setIdProcess($rule->idProcess);
		$newRule->setEnabled($rule->enabled);
		$newRule->setValue($rule->value);
		$newRule->setProperty($rule->property);
		$newRule->setOperator($rule->operator);
		$newRule->setPriority(isset($rule->priority)?$rule->priority:100);
		
		return $newRule;
	}
	
	public function getAll() {
		$rulesFound = array();
		$query = "SELECT * FROM ".ConnectionDB::PROCESSRULES;
		$result = $this->connection->execute($query);
		while($result->fetch()){
			$rule = new Rule();
			$rule->setIdRule($result->getVal("idRule"));
			$rule->setWeight($result->getVal("weight"));
			$rule->setRuleTimeStamp($result->getVal("ruleTimeStamp"));
			$rule->setIdProcess($result->getVal("idProcess"));
			$rule->setDescription($result->getVal("description"));
			$rule->setEnabled($result->getVal("enabled"));
			$rule->setValue($result->getVal("value"));
			$rule->setProperty($result->getVal("property"));
			$rule->setOperator($result->getVal("operator"));
			$rule->setPriority($result->getVal("priority"));
			$rulesFound[] = $rule;
		}
		$result->close();
		return $rulesFound;
	}
	
	public function getRule($idRule) {
		$dataValues = array();
		$dataValues["IDRULE"] = $idRule;
		$query = "SELECT * FROM ".ConnectionDB::PROCESSRULES." WHERE idRule = :IDRULE";
		$result = $this->connection->execute($query, $dataValues);
		$rule = new Rule();
		
		if($result->fetch()){
			$rule->setIdRule($result->IDRULE);
			$rule->setWeight($result->weight);
			$rule->setRuleTimeStamp($result->ruleTimeStamp);
			$rule->setIdProcess($result->idProcess);
			$rule->setDescription($result->description);
			$rule->setEnabled($result->enabled);
			$rule->setValue($result->value);
			$rule->setProperty($result->property);
			$rule->setOperator($result->operator);
			$rule->setPriority($result->priority);
		}
		$result->close();
		return $rule;
	}
	
	public function getRulesByProcess($idProcess) {
		$dataValues = array();
		$dataValues["IDPROCESS"] = $idProcess;
		if ($this->rest) {
			$query = "SELECT * FROM ".ConnectionDB::PROCESSRULES." WHERE idProcess = :IDPROCESS ORDER BY priority ASC";
		}else{
			$query = "SELECT * FROM ".ConnectionDB::PROCESSRULES." WHERE idProcess = :IDPROCESS AND enabled = 1 ORDER BY priority ASC";
		}
		
		$rulesFound = array();
		$result = $this->connection->execute($query, $dataValues);
		
		while($result->fetch()){
			$rule = new Rule();
			$rule->setIdRule($result->idRule);
			$rule->setWeight($result->weight);
			$rule->setRuleTimeStamp($result->ruleTimeStamp);
			$rule->setIdProcess($result->idProcess);
			$rule->setDescription($result->description);
			$rule->setEnabled($result->enabled == 1);
			$rule->setValue($result->value);
			$rule->setProperty($result->property);
			$rule->setOperator($result->operator);
			$rule->setPriority($result->priority);
			$rulesFound[] = $rule;
		}
		$result->close();
		return $rulesFound;
	}
	
	public function deleteRule($idRule) {
		$dataValues = array();
		$dataValues["IDRULE"] = $idRule;
		$query = "DELETE FROM ".ConnectionDB::PROCESSRULES." WHERE idRule = :IDRULE";
		$result = $this->connection->execute($query, $dataValues);
		
		return array("result" => $result);
	}
	
	public function saveRule($rule) {
		$dataValues = array();
		$dataValues["weight"] = $rule->weight.'';
		$dataValues["ruleTimeStamp"] = array(
				date('Y/m/d H:i:s'),
				DBCONN_DATATYPE_DATE
		);
		$dataValues["idProcess"] = $rule->idprocess;
		$dataValues["description"] = $rule->description;
		$dataValues["enabled"] = $rule->enabled?'1':'0';
		$dataValues["value"] = $rule->value;
		$dataValues["property"] = $rule->property;
		$dataValues["operator"] = $rule->operator;
		$dataValues["priority"] = isset($rule->priority)?$rule->priority:100;
		
		$table = ConnectionDB::PROCESSRULES;
		try {
			$result = $this->connection->setValues("C", $table, $dataValues);
		} catch (\Exception $e) {
			$ex = $e->getMessage();
			$result = false;
		}
		
		
		return array("result" => $result, "description" => $rule->description, "sequence" => 1);
	}
	
	public function updateRule($rule) {
		$dataValues = array();
		$dataValues["weight"] = $rule->weight;
		$dataValues["description"] = $rule->description;
		$dataValues["enabled"] = $rule->enabled?'1':'0';
		$dataValues["value"] = $rule->value;
		$dataValues["property"] = $rule->property;
		$dataValues["operator"] = $rule->operator;
		$dataValues["priority"] = $rule->priority;
		$whereValues = array();
		$whereValues["idRule"] = $rule->idrule;
		$result = false;
		$ex = false;
		try {
			$whereCondition = "idRule = :IDRULE";
			$result = $this->connection->setValues(DBCONN_UPDATE, ConnectionDB::PROCESSRULES, $dataValues, $whereCondition, $whereValues);
		} catch (Exception $e) {
			$ex = $e->getMessage();
			$result = false;
		}
		return array("result" => $result, "description" => $rule->description, "sequence" => $ex);
	}
	
	public function getRulesByPreprocess(int $idPreprocess){
		$query = "SELECT * FROM ".ConnectionDB::PREPROCESSRULES ." where idPreprocess = :IDPREPROCESS";
		$values = array();
		$values["IDPREPROCESS"] = $idPreprocess;
		$result = $this->connection->execute($query, $values);
		$rules = array();
		while($result->fetch()){
			$rule = new Rule();
			$rule->setIdRule($result->idPreprocessRules);
			$rule->setProperty($result->propertyName);
			$rule->setOperator($result->operator);
			$rule->setValue($result->propertyValue);
			$rule->setIdProcess($result->idPreprocess);
			$rule->setWeight($result->weight);
			$rules[] = $rule;
		}
		$result->close();
		return $rules;
	}
}
?>