<?php

/**
* @author zroque
* @version 1.0
* created on 28 jun. 2018
*/
namespace dataAccess\dao;

use dataAccess\interfaces\INotifications;
use model\Notification;
use dataAccess\dbConn;
use dataAccess\ConnectionDB;

include_once BaseDir. '/dataAccess/interfaces/INotifications.php';
include_once BaseDir. '/model/Notification.php';
class NotificationDAO implements INotifications {
	
	/**
	 *
	 * @var dbConn
	 */
	private $connection;

	public function __construct(){

		$dataBase = new ConnectionDB();
		$this->connection = $dataBase->getConnection();
	}

	public function getNotificationByProcess(int $idProcess){

		
		$dataValues = array();
		$dataValues["IDPROCESS"] = $idProcess;
		$query = "SELECT * FROM ".ConnectionDB::PROCESSNOTIFICATIONS. " where idProcess = :IDPROCESS";
		$notificationsFound = array();
		$result = $this->connection->execute($query, $dataValues);
		$notification = new Notification();
		
		if($result->fetch()) {
			$notification = new Notification();
			$notification->setEmail($result->email);
			$notification->setWhenProcess($result->whenProcessed);
			$notification->setWhenWarnings($result->whenWarnings);
			$notification->setWhenErrors($result->whenErrors);
			$notification->setWhenRejected($result->whenRejected);
			$notification->setAddBasicTaskInfo($result->addBasicTaskInfo);
			$notification->setAddSourceFile($result->addSourceFile);
			$notification->setIdProcess($result->idProcess);
			$notification->setWhenChanged($result->whenChanged);
		}
		
		$result->close();
		return $notification;
	}

	public function getAll(){

		$notificationsFound = array();
		$query = "SELECT * FROM ".ConnectionDB::PROCESSNOTIFICATIONS;
		$result = $this->connection->execute($query);
		while($result->fetch()) {
			$notification = new Notification();
			$notification->setEmail($result->getVal("email"));
			$notification->setWhenProcess($result->getVal("whenProcessed"));
			$notification->setWhenWarnings($result->getVal("whenWarnings"));
			$notification->setWhenErrors($result->getVal("whenErrors"));
			$notification->setWhenRejected($result->getVal("whenRejected"));
			$notification->setIdProcess($result->getVal("idProcess"));
			$notificationsFound[] = $notification;
		}
		
		$result->close();
		return $notificationsFound;
	}

	public function saveNotification($notification){

		$result = $this->connection->setValues(DBCONN_INSERT, ConnectionDB::PROCESSNOTIFICATIONS, $notification);
        
        return $result;
	}

	public function updateNotification($notification,$idProcess){

		// $dataValues = array();
		// $dataValues["email"] = "acdc@gmail.com";
		// $dataValues["whenProcessed"] = true;
		// $dataValues["whenWarnings"] = true;
		// $dataValues["whenErrors"] = true;
		// $dataValues["whenRejected"] = true;
		// $dataValues["addBasicTaskInfo"] = true;
		// $dataValues["addSourceFile"] = true;
		// $dataValues["whenChanged"] = true;
		
		$whereValues = array();
        $whereValues["idProcess"] = $idProcess;
        // $notification = $dataValues;
		//$table = "Notification";
		$whereCondition = "idProcess = :IDPROCESS";
        $result = $this->connection->setValues(DBCONN_UPDATE, ConnectionDB::PROCESSNOTIFICATIONS, $notification, $whereCondition, $whereValues);
        
        return array("result" => $result);
	}


	public function deleteNotificacion(int $idProcess){
		
		$dataValues = array();
		$dataValues["IDPROCESS"] = $idProcess;
		$query = "DELETE FROM ".ConnectionDB::PROCESSNOTIFICATIONS. " WHERE idProcess = :IDPROCESS";
		
		$result = $this->connection->execute($query, $dataValues);
		
		return $result;
	}
}
?>