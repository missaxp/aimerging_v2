<?php

/**
 *
 * @author Ljimenez
 * @version 1.0
 *          created on 28 jun. 2018
 */
namespace dataAccess\dao;

use common\exceptions\AIException;
use dataAccess\interfaces\ITask;
use Functions;
use model\Analysis;
use model\DataSource;
use model\File;
use model\Category;
use model\TM;
use model\Task;
use model\TaskAditionalProperties;
use dataAccess\dbConn;
use dataAccess\ConnectionDB;
use model\Language;
use dataAccess\SQLException;
use model\State;
use core\AI;
use PHPMailer\PHPMailer\Exception;
use TaskErrorDAO;
use Throwable;

include_once BaseDir . '/model/Task.php';
include_once BaseDir . '/model/File.php';
include_once BaseDir . '/model/Category.php';
include_once BaseDir . '/model/Language.php';
include_once BaseDir . '/model/DataSource.php';
include_once BaseDir . '/dataAccess/interfaces/ITask.php';
include_once BaseDir . '/model/State.php';
include_once BaseDir . '/model/Analysis.php';
include_once BaseDir . '/dataAccess/ConnectionDB.php';
include_once BaseDir . '/model/TM.php';
include_once BaseDir . '/dataAccess/dao/TaskErrorDAO.php';
class TaskDAO implements ITask
{
	
	/**
	 *
	 * @var dbConn
	 */
	private $connection;
	
	public function __construct()
	{
		
		$dataBase = new ConnectionDB();
		$this->connection = $dataBase->getConnection();
	}
	
	public function updateTaskAditionalProperties(TaskAditionalProperties $taskAdProp)
	{ }
	
	public function deleteFile(int $idFile)
	{ 
		$queryTranslate = "DELETE FROM ".ConnectionDB::TASKFILESTOTRANSLATE." WHERE idFile = :IDFILE";
			
		$queryReference = "DELETE FROM ".ConnectionDB::TASKFILESFORREFERENCE." WHERE idFile = :IDFILE";
			
		$values = array("IDFILE"=>$idFile);
		$result = false;
		try{
			$this->connection->setDefaultExecMode(DBCONN_TRANSACTION);
			$this->connection->execute($queryTranslate,$values);
			$values = array("IDFILE"=>$idFile);
			$this->connection->execute($queryReference,$values);
			$values = array("IDFILE"=>$idFile);
			$query = "DELETE FROM ". ConnectionDB::FILE." WHERE idFile = :IDFILE";
			$this->connection->execute($query,$values);
			$result = $this->connection->commit();
		}catch(SQLException $e){
		    $ai = AIException::createInstanceFromSQLException($e)->construct(__METHOD__, __NAMESPACE__, $func = func_get_args());
		    Functions::logException($ai->getMessage(), Functions::ERROR, __CLASS__, $ai);
			$this->connection->rollback();
		}catch(Throwable $th){
            $ai = AIException::createInstanceFromThrowable($th)->construct(__METHOD__, __NAMESPACE__, $func = func_get_args());
            Functions::logException($ai->getMessage(), Functions::ERROR, __CLASS__, $ai);
            $this->connection->rollback();
        }
		finally{
			$this->connection->setDefaultExecMode(DBCONN_COMMIT_ON_SUCCESS);
		}
		return $result;
	}
	
	public function saveAnalysis(array $analysises, string $idTask, array $targetLanguage)
	{
		
		foreach ($analysises as $analysis) {
			$values = array();
			$values["percentage_101"] = $analysis->getPercentage_101() . "";
			$values["repetitions"] = $analysis->getRepetition() . "";
			$values["percentage_100"] = $analysis->getPercentage_100() . "";
			$values["percentage_95"] = $analysis->getPercentage_95() . "";
			$values["percentage_85"] = $analysis->getPercentage_85() . "";
			$values["percentage_75"] = $analysis->getPercentage_75() . "";
			$values["percentage_50"] = $analysis->getPercentage_50() . "";
			$values["machineTranslation"] = $analysis->getMachineTanslation() . "";
			$values["minute"] = $analysis->getMinute() . "";
			$values["weightedWords"] = $analysis->getWeightedWord() . "";
			$values["notMatch"] = $analysis->getNotMatch() . "";
			$values["totalWords"] = $analysis->getTotalWords() . "";
			$result = $this->connection->setValues(DBCONN_INSERT, ConnectionDB::ANALYSIS, $values);
			
			foreach ($targetLanguage as $target) {
				unset($values);
				$values = array();
				$values["uniqueSourceId"] = $idTask;
				
				$values["idLanguage"] = $target->getIdLanguage();
				$values["idWordAnalysis"] = $result;
				$this->connection->setValues(DBCONN_INSERT, ConnectionDB::TARGETLANGUAGE, $values);
			}
		}
		return ($result > 0);
	}
	
	public function deleteDataSource(int $idDataSource)
	{ }

	public function getTasksWithOnlyFiles($interval, $lastExecuting)
    {
        $tasks = array();
        $values = array();
        if($lastExecuting != null){
            $values["LASTEXECUTION"] = array($lastExecuting->format("Y-m-d H:i:s"),DBCONN_DATATYPE_DATE);
        }else{
            $values["LASTEXECUTION"] = " ";
        }

        $values["INTERVAL"] = array($interval->format("Y-m-d H:i:s"),DBCONN_DATATYPE_DATE);

	    $query = "SELECT * FROM ".ConnectionDB::TASK. " WHERE timeStamp between :LASTEXECUTION and :INTERVAL";
	    try{
	        $result = $this->connection->execute($query,$values);

	        while($result->fetch()){
                $task = new Task();
                $task->setUniqueSourceId($result->uniqueSourceId);
                $task->setUniqueTaskId($result->uniqueTaskId);
                $task->setTimeStamp($result->timeStamp);
                $task->setIdClientetask($result->idClientTask);
                $task->setTitle($result->title);
                $task->setAssignedUser($result->assignedUser);
                $task->setMenssage($result->message);
                $task->setStartDate($result->startDate);
                $task->setDueDate($result->dueDate);
                $task->setTries($result->tries);
                $state = new State($result->idState);
                $task->setState($state);
                $task->setRelatedOriginalData($result->relatedOriginalData);

                $task->setFilesToTranslate($this->getFilesByTask("TRANSLATE", $task->getUniqueSourceId()));
                $task->setFilesForReference($this->getFilesByTask("REFERENCE", $task->getUniqueSourceId()));
                $task->setDataSource($this->findDataSource($task->getUniqueSourceId()));

                $tasks[] = $task;
            }
        }catch (SQLException $ex){

        }

	    return $tasks;
    }
	
	public function saveTaskAditionalProperties(TaskAditionalProperties $taskAdProp, string $uniqueSourceId)
	{
		
		$values = array();
		$values["propertyName"] = $taskAdProp->getPropertyName();
		$values["propertyValue"] = $taskAdProp->getPropertyValue() . '';
		$values["uniqueSourceId"] = $uniqueSourceId;
		
		$result = $this->connection->setValues(DBCONN_INSERT, "TaskAditionalProperties", $values);
		
		return ($result > 0);
	}
	
	public function getAll()
	{ }
	
	public function deleteTaskAditionalProperties(int $idTaskAditionalProperties)
	{ }
	
	public function updateAnalysis(Analysis $analysis)
	{ 
		$values= array();
		$values["percentage_101"] = $analysis->getPercentage_101()."";
		$values["repetitions"] = $analysis->getRepetition()."";
		$values["percentage_100"] = $analysis->getPercentage_100()."";
		$values["percentage_95"] = $analysis->getPercentage_95()."";
		$values["percentage_85"] = $analysis->getPercentage_85()."";
		$values["percentage_75"] = $analysis->getPercentage_75()."";
		$values["percentage_50"] = $analysis->getPercentage_50()."";
		$values["machineTranslation"] = $analysis->getMachineTanslation()."";
		$values["weightedWords"] = $analysis->getWeightedWord()."";
		$values["notMatch"] = $analysis->getNotMatch()."";
		$values["totalWords"] = $analysis->getTotalWords()."";
		$whereCondition ="idWordAnalysis = :IDWORDANALYSIS";
		$whereValues["IDWORDANALYSIS"] = $analysis->getIdAnalis();
		try{
			$result = $this->connection->setValues(DBCONN_UPDATE, ConnectionDB::ANALYSIS, $values, $whereCondition, $whereValues);
		}catch(\Exception $e){
			Functions::addLog($e->getMessage(), Functions::ERROR, json_encode($e->getTrace()));
		}
		return $result;
	}
	
	/**
	 *
	 * @param array $file
	 * @return boolean
	 */
	public function saveTaskFileAsyncDownload(array $file)
	{
		$result = false;
		if ($this->validateTaskFileAsyncDownload($file['asyncHash'])) {
			$file['isDownloading'] = ($file['isDownloading'] ? '1' : '0');
			$file['isDownloaded'] = ($file['isDownloaded'] ? '1' : '0');
			$file['startDownloadDate'] = array(
					date('Y/m/d H:i:s'),
					DBCONN_DATATYPE_DATE
			);
			$result = $this->connection->setValues(DBCONN_INSERT, ConnectionDB::TASKFILEASYNCDOWNLOAD, $file);
		}
		
		return $result;
	}
	
	/**
	 *
	 *
	 * @param array $file
	 * @return boolean
	 */
	public function updateTaskFileAsyncDownload($file)
	{
		
		if (!$this->validateTaskFileAsyncDownload($file['asyncHash'])) {
			$values = array(
					"downloadPath" => $file['downloadPath'],
					"isDownloading" => ($file['isDownloading'] ? '1' : '0'),
					"isDownloaded" => ($file['isDownloaded'] ? '1' : '0'),
					"filePath" => $file['filePath']
			);
			$whereValues = array(
					"asyncHash" => $file['asyncHash']
			);
			
			if($file['isDownloaded'] || ($file['isDownloaded'] == false && $file['isDownloading'] == false)){
				$values['endDownloadDate'] = array(
						date('Y/m/d H:i:s'),
						DBCONN_DATATYPE_DATE
				);
			}
			
			$whereCondition = "asyncHash = :ASYNCHASH";
			try {
				$result = $this->connection->setValues(DBCONN_UPDATE, ConnectionDB::TASKFILEASYNCDOWNLOAD, $values, $whereCondition, $whereValues);
			} catch (\Exception $e) {
				Functions::addLog($e->getMessage(), Functions::ERROR, json_encode($e->getTrace()));
			}
		} else {
			$result = $this->saveTaskFileAsyncDownload($file);
		}
		return $result;
	}
	
	/**
	 * Validate if TaskFile was save
	 *
	 * @param string $asyncHash download token
	 * @return boolean true if it was save or false else
	 */
	 public function validateTaskFileAsyncDownload($asyncHash)
	 {
	 	$validation = true;
	 	$values = array("asyncHash" => $asyncHash);
	 	$query = "SELECT * from " . ConnectionDB::TASKFILEASYNCDOWNLOAD . " where asyncHash = :asyncHash";
	 	
	 	$result = $this->connection->execute($query, $values);
	 	
	 	if ($result->fetch()) {
	 		$validation = false;
	 	}
	 	
	 	$result->close();
	 	
	 	return $validation;
	 }
	 
	 public function updateFile(File $file)
	 {
	 	$values = array();
	 	$result = false;
	 	$values["fileName"] = $file->getFileName();
	 	$values["fileNameExtension"] = $file->getFileNameExtension();
	 	$values["mimeType"] = $file->getMimeType();
	 	$values["description"] = $file->getDescription();
	 	$values["timeStamp"] = array(
	 			$file->getTimeStamp(),
	 			DBCONN_DATATYPE_DATE
	 	);
	 	$values["sourcePath"] = $file->getSourcePath();
	 	$values["size"] = $file->getSize() . "";
	 	$values["path"] = $file->getPath();
	 	$values["checksum"] = $file->getChecksum();
	 	$values["successfullDownload"] = $file->getSuccessfulDownload() ? '1' : '0';
	 	$values["downloadPath"] = $file->getPathForDownload();
	 	$values["asyncHash"] = $file->getAsyncHash();
	 	$values["isDelete"] = $file->isDelete()?'1':'0';
	 	
	 	$whereCondition = "idFile = :IDFILE";
	 	$whereValues = array(
	 			"IDFILE" => $file->getIdFile()
	 	);
	 	try {
	 		$result = $this->connection->setValues(DBCONN_UPDATE, ConnectionDB::FILE, $values, $whereCondition, $whereValues);
	 	} catch (\Exception $e) {
	 		Functions::addLog($e->getMessage(), Functions::ERROR, json_encode($e->getTrace()));
	 	}
	 	
	 	return $result;
	 }
	 
	 public function saveFile(File $file, string $uniqueSourceId, bool $isTM = false)
	 {
	 	
	 	$values = array();
	 	$values["fileName"] = $file->getFileName();
	 	$values["fileNameExtension"] = $file->getFileNameExtension();
	 	$values["mimeType"] = $file->getMimeType();
	 	$values["description"] = $file->getDescription();
	 	$values["timeStamp"] = array(
	 			$file->getTimeStamp(),
	 			DBCONN_DATATYPE_DATE
	 	);
	 	$values["sourcePath"] = $file->getSourcePath();
	 	$values["size"] = $file->getSize() . "";
	 	$values["path"] = $file->getPath();
	 	$values["checksum"] = $file->getChecksum();
	 	$values["successfullDownload"] = $file->getSuccessfulDownload() ? '1' : '0';
	 	$values["downloadPath"] = $file->getPathForDownload();
	 	if ($file->getAsyncHash() != "Empty" || $file->getAsyncHash() != "") {
	 		$values["asyncHash"] = $file->getAsyncHash();
	 	}
	 	
	 	$idFile = $this->connection->setValues("C", ConnectionDB::FILE, $values);
	 	if (!$isTM) {
	 		unset($values);
	 		
	 		$values = array();
	 		$values["uniqueSourceId"] = $uniqueSourceId;
	 		$values["idFile"] = $idFile;
	 		$table = ConnectionDB::TASKFILESFORREFERENCE;
	 		if ($file->getFileType() == File::SOURCE) {
	 			$table = ConnectionDB::TASKFILESTOTRANSLATE;
	 		}
	 		
	 		$this->connection->setValues(DBCONN_INSERT, $table, $values);
	 	}
	 	return $idFile;
	 }
	 
	 public function saveDataSource(DataSource $dataSource, string $uniqueSourceId)
	 {
	 	
	 	$values = array();
	 	$values["sourceName"] = $dataSource->getSourceName();
	 	$values["type"] = $dataSource->getType();
	 	$values["timeStamp"] = array(
	 			$dataSource->getTimeStamp(),
	 			DBCONN_DATATYPE_DATE
	 	);
	 	$values["harvestMethod"] = $dataSource->getHarvestMethod();
	 	$values["tmsURL"] = $dataSource->getTmsURL();
	 	$values["tmsTaskUrl"] = $dataSource->getTmsTaskUrl();
	 	$values["originalData"] = $dataSource->getOriginalData();
	 	$values["uniqueSourceId"] = $uniqueSourceId;
	 	$values["idSource"] = $dataSource->getIdSource();
	 	$values["tmsUserCollector"] = $dataSource->getTmsUserCollector();
	 	$this->connection->setValues(DBCONN_INSERT, ConnectionDB::TASKSOURCE, $values);
	 }
	 
	 public function getFile(int $idFile)
	 {
	 	
	 	$values = array();
	 	$values["IDFILE"] = $idFile;
	 	$query = "SELECT * FROM " . ConnectionDB::FILE . " WHERE idFile = :IDFILE";
	 	$result = $this->connection->execute($query, $values);
	 	$file = null;
	 	if ($result->fetch()) {
	 		$file = new File();
	 		$file->setFileName($result->fileName);
	 		$file->setFileNameExtension($result->fileNameExtension);
	 		$file->setMimeType($result->mimeType);
	 		$file->setDescription($result->description);
	 		$file->setTimeStamp($result->timeStamp);
	 		$file->setSourcePath($result->sourcePath);
	 		$file->setSize($result->size);
	 		$file->setPath($result->path);
	 		$file->setChecksum($result->checksum);
	 		$file->setIdFile($result->idFile);
	 		$file->setPathForDownload($result->downloadPath);
	 		$file->setSuccessfulDownload($result->successfullDownload);
	 		$file->setAsyncHash($result->asyncHash);
	 		$file->setIsDelete($result->isDelete == 1);
	 		$result->close();
	 	}
	 	return $file;
	 }
	 
	 public function getAditionalPropertiesByTask(string $uniqueSourceId)
	 {
	 	
	 	$values = array();
	 	$values["UNIQUESOURCEID"] = $uniqueSourceId;
	 	$query = 'SELECT * FROM TaskAditionalProperties WHERE uniqueSourceID = :UNIQUESOURCEID';
	 	$properties = array();
	 	
	 	$result = $this->connection->execute($query, $values);
	 	
	 	while ($result->fetch()) {
	 		$property = new TaskAditionalProperties();
	 		$property->setIdTaskAditionalProperties($result->IDTASKADITIONALPROPERTIES);
	 		$property->setPropertyName($result->PROPERTYNAME);
	 		$property->setPropertyValue($result->PROPERTYVALUE);
	 		$property->setUniqueSourceId($result->UNIQUESOURCEID);
	 		$properties[] = $property;
	 	}
	 	$result->close();
	 	return $properties;
	 }
	 
	 public function changeStateTask(string $idTask, int $state, string $description, string $timeStamp = null)
	 {
	 	
	 	$values = array();
	 	$values["uniqueSourceId"] = $idTask;
	 	$values["idState"] = $state;
	 	if ($timeStamp === null) {
	 		$values["timeStamp"] = array(
	 				date('Y/m/d H:i:s'),
	 				DBCONN_DATATYPE_DATE
	 		);
	 	} else {
	 		$values["timeStamp"] = $timeStamp;
	 	}
	 	$values["description"] = $description;
	 	
	 	$result = $this->connection->setValues(DBCONN_INSERT, ConnectionDB::TASKCHANGEHISTORY, $values);
	 	return $result;
	 }
	 
	 public function findDataSource(string $uniqueSourceId)
	 {
	 	
	 	$query = "SELECT * FROM " . ConnectionDB::TASKSOURCE . " WHERE uniqueSourceId = :UNIQUESOURCEID";
	 	$dataValues = array();
	 	$dataValues["UNIQUESOURCEID"] = $uniqueSourceId;
	 	$result = $this->connection->execute($query, $dataValues);
	 	$dataSource = null;
	 	if ($result->fetch()) {
	 		$dataSource = new DataSource();
	 		$dataSource->setIdDataSource($result->idDataSource);
	 		$dataSource->setSourceName($result->sourceName);
	 		$dataSource->setType($result->type);
	 		$dataSource->setTimeStamp($result->timeStamp);
	 		$dataSource->setHarvestMethod($result->harvestMethod);
	 		$dataSource->setTmsURL($result->tmsURL);
	 		$dataSource->setTmsTaskUrl($result->tmsTaskUrl);
	 		$dataSource->setTmsUserCollector($result->tmsUserCollector);
	 		$dataSource->setOriginalData($result->originalData);
	 		$dataSource->setUniqueSourceId($result->uniqueSourceId);
	 		$dataSource->setIdSource($result->idSource);
	 	}
	 	$result->close();
	 	return $dataSource;
	 }
	 
	 public function updateTask(Task $task)
	 { }

    /**
     * Return true if id is not in DB
     * @param $client_task_id
     * @return bool
     * @throws AIException
     */
	 public function validateTask($client_task_id){
		 $query = "SELECT uniqueSourceID from Task where idClientTask=:idClientTask";
		 $dataValues = array();
		 $dataValues["idClientTask"] = $client_task_id;
		 try {
			 $result = $this->connection->execute($query, $dataValues);
			 if($result->count($this->connection) > 0){
			 	return false;
			 }else{
			 	return true;
			 }

		 } catch (SQLException $e) {
		     $ex = AIException::createInstanceFromSQLException($e);
		     $ex->construct(__METHOD__, __NAMESPACE__, $args = func_get_args());
		     throw $ex;
         } catch (\Exception $e) {
             $ex = AIException::createInstanceFromThrowable($e);
             $ex->construct(__METHOD__, __NAMESPACE__, $args = func_get_args());
             throw $ex;
         }
	 }
	 
	 public function saveTask(Task $task)
	 {
	 	
	 	$validation = true;
	 	$this->connection->setDefaultExecMode(DBCONN_TRANSACTION);
	 	try {
	 		
	 		$values = array();
	 		$values["uniqueSourceId"] = $task->getUniqueSourceId();
	 		$values["uniqueTaskId"] = $task->getUniqueTaskId();
	 		$values["timeStamp"] = array(
	 				$task->getTimeStamp(),
	 				DBCONN_DATATYPE_DATE
			 );
	 		$values["idClientTask"] = $task->getIdClientetask();
	 		$values["title"] = $task->getTitle();
	 		$values["message"] = $task->getMenssage();
	 		$values["startDate"] = $task->getStartDate()->format("Y-m-d H:i:s");
	 		$values["dueDate"] = $task->getDueDate()->format("Y-m-d H:i:s");
	 		$values["assignedUser"] = $task->getAssignedUser();
	 		$values["idCategory"] = 1;
	 		$values["sourceLanguage"] = $task->getSourceLanguage()->getIdLanguage();
	 		$values["idState"] = $task->getState()->getIdState();
	 		$values["tries"] = $task->getTries() . '';
	 		$values["relatedOriginalData"] = $task->getRelatedOriginalData();
	 		$this->connection->setValues(DBCONN_INSERT, ConnectionDB::TASK, $values);
	 		
	 		foreach ($task->getStatusHistory() as $state) {
	 			$this->changeStateTask($task->getUniqueSourceId(), $state->getIdState(), $state->getDescription(), $state->getTimeStamp()->format("Y-m-d H:i:s"));
	 		}
	 		
	 		foreach ($task->getFilesForReference() as $file) {
	 			$this->saveFile($file, $task->getUniqueSourceId());
	 		}
	 		
	 		foreach ($task->getFilesToTranslate() as $file) {
	 			$this->saveFile($file, $task->getUniqueSourceId());
	 		}

	 		$taskErrors =$task->getTaskErrors();
	 		if(isset($taskErrors)){
                for ($i = 0; $i < count($taskErrors); $i++) {
                    $taskErrors[$i]->setUniqueSourceId($task->getUniqueSourceId());
	 		    }
	 		    $daoErrores = new TaskErrorDAO();
	 		    $daoErrores->saveErrors($taskErrors);
            }
	 		/*if (!empty($task->getAnalysis())) {
	 			$this->saveAnalysis($task->getAnalysis(), $task->getUniqueSourceId(), $task->getTargetsLanguage());
	 		} elseif (!empty($task->getTargetsLanguage())) {
	 			$this->saveTargetLanguage($task->getTargetsLanguage(), $task->getUniqueSourceId());
	 		}*/
	 		
	 		if (!empty($task->getTargetsLanguage())) {
	 			foreach ($task->getTargetsLanguage() as $targetLanguage) {
	 				$this->saveTargetLanguage($targetLanguage, $task->getUniqueSourceId());
	 			}
	 		}
	 		
	 		foreach ($task->getTranslationMemories() as $tm) {
	 			$this->saveTM($tm, $task->getUniqueSourceId());
	 		}
	 		
	 		foreach ($task->getAditionalProperties() as $property) {
	 			$this->saveTaskAditionalProperties($property, $task->getUniqueSourceId());
	 		}
	 		
	 		$this->saveDataSource($task->getDataSource(), $task->getUniqueSourceId());
	 		
	 		$this->connection->commit();
	 	} catch (\Exception $e) {
			 $errMsg = $e->getMessage().", file: ".$e->getFile().":".$e->getLine()."<br />";
	 		$this->connection->rollback();
	 		Functions::addLog($errMsg, Functions::ERROR, json_encode($e->getTrace()));
	 		$validation = false;
	 	} finally {
	 		$this->connection->setDefaultExecMode(DBCONN_COMMIT_ON_SUCCESS);
	 	}
	 	return $validation;
	 }
	 
	 public function deleteAnalysis(int $idAnalysis)
	 { }
	 
	 public function getAnalysis(int $idAnalysis)
	 {
	 	
	 	$values = array();
	 	$values["IDWORDANALYSIS"] = $idAnalysis;
// 	 	$query = 'SELECT a.* FROM ' . ConnectionDB::ANALYSIS . ' a, ' . ConnectionDB::TARGETLANGUAGE . ' t WHERE t.uniqueSourceId = :IDTASK and a.idWordAnalysis = t.idWordAnalysis';
	 	$query = 'SELECT * FROM ' . ConnectionDB::ANALYSIS .' WHERE idWordAnalysis = :IDWORDANALYSIS';
	 	$analysisFound = null;
	 	try {
	 		$result = $this->connection->execute($query, $values);
	 		
	 		while ($result->fetch()) {
	 			$analysis = new Analysis();
	 			$analysis->setIdAnalis($result->IDWORDANALYSIS);
	 			$analysis->setRepetition($result->REPETITIONS);
	 			$analysis->setTotalWords($result->TOTALWORDS);
	 			$analysis->setMinute($result->MINUTE);
	 			$analysis->setWeightedWord($result->WEIGHTEDWORDS);
	 			$analysis->setNotMatch($result->NOTMATCH);
	 			$analysis->setMachineTanslation($result->MACHINETRANSLATION);
	 			$analysis->setPercentage_101($result->PERCENTAGE_101);
	 			$analysis->setPercentage_100($result->PERCENTAGE_100);
	 			$analysis->setPercentage_95($result->PERCENTAGE_95);
	 			$analysis->setPercentage_85($result->PERCENTAGE_85);
	 			$analysis->setPercentage_75($result->PERCENTAGE_75);
	 			$analysis->setPercentage_50($result->PERCENTAGE_50);
	 			$analysisFound = $analysis;
	 		}
	 	} catch (SQLException $e) {
	 		Functions::addLog($e->getMessage(), Functions::ERROR, json_encode($e->getTrace()));
	 	}
	 	$result->close();
	 	
	 	return $analysisFound;
	 }
	 
	 public function getTaskAditionalProperties(int $idTaskAditionalProperties)
	 { }
	 
	 public function updateDataSource(DataSource $dataSource)
	 { }
	 
	 public function deleteTask(string $idTask)
	 { }
	 
	 /**
	  * Given a UTID, returns a task or false.
	  *
	  * @param string $utid
	  * @return \model\Task
	  */
	 public function getTaskByUTID(string $utid)
	 {
	 	
	 	$query = "SELECT * FROM Task WHERE uniqueTaskId = :UTID";
	 	$dataValues = array();
	 	$dataValues["UTID"] = $utid;
	 	$task = new Task();
	 	try {
	 		$result = $this->connection->execute($query, $dataValues);
	 		if ($result->fetch()) {
	 			$task = $this->getTaskObject($result);
	 		}
	 	} catch (\dataAccess\SQLException $ex) {
	 		Functions::addLog($ex->getMessage(), Functions::ERROR, json_encode($ex->getTrace()));
	 	} finally {
	 		$result->close();
	 	}
	 	return $task;
	 }
	 
	 public function getTask(string $idTask)
	 {
	 	
	 	$query = "SELECT * FROM Task WHERE uniqueSourceId = :UNIQUESOURCEID";
	 	$dataValues = array();
	 	$dataValues["UNIQUESOURCEID"] = $idTask;
	 	$task = null;
	 	$result = null;
	 	try {
	 		$result = $this->connection->execute($query, $dataValues);
	 		if ($result->fetch()) {
	 			$task = $this->getTaskObject($result);
	 		}
	 	} catch (\dataAccess\SQLException $ex) {
	 		$e = AIException::createInstanceFromSQLException($ex)->construct(__METHOD__, __NAMESPACE__, $args= func_get_args());
	 		Functions::logException($ex->getMessage(), Functions::ERROR, __CLASS__, $e);
	 	} finally {
	 		if ($result != null) {
	 			$result->close();
	 		}
	 	}
	 	return $task;
	 }
	 
	 public function updateTaskState(Task $task, int $idState, string $description, int $idAction = 0, int $idProcess = 0, bool $alter = true, bool $alterAction = true)
	 {
	 	
	 	$result = false;
	 	$this->connection->setDefaultExecMode(DBCONN_TRANSACTION);
	 	try {
	 		$values = array();
	 		if ($alterAction) {
	 			$values["idAction"] = $idAction;
	 		}
	 		if ($alter) {
	 			$values["idProcess"] = $idProcess;
	 		}
	 		$whereValues = array();
	 		$whereValues["uniqueSourceId"] = $task->getUniqueSourceId();
	 		
	 		if ($idState == State::RETRY) {
	 			if ((int)$task->getTries() + 1 >= AI::getInstance()->getSetup()->numberOfTries) {
	 				$idState = State::ERROR;
	 			} else {
	 				$values["tries"] = (int)$task->getTries() + 1;
	 			}
	 		}
	 		$values["idState"] = $idState;
	 		
	 		$whereCondition = "uniqueSourceId = :UNIQUESOURCEID";
	 		
	 		$result = $this->connection->setValues(DBCONN_UPDATE, ConnectionDB::TASK, $values, $whereCondition, $whereValues);
	 		
	 		$result = $this->changeStateTask($task->getUniqueSourceId(), $idState, $description);
	 		
	 		$this->connection->commit();
	 	} catch (\dataAccess\SQLException $ex) {
	 		$this->connection->rollback();
	 		$result = -1;
	 		Functions::addLog($ex->getMessage(), Functions::ERROR, json_encode($ex->getTrace()));
	 	} finally {
	 		$this->connection->setDefaultExecMode(DBCONN_COMMIT_ON_SUCCESS);
	 	}
	 	return ($result > 0);
	 }
	 
	 public function saveDownloadFileFail(Task &$task){
	 	$whereCondition = "uniqueSourceId = :UNIQUESOURCEID";
	 	$whereValues = array(
	 			"uniqueSourceId" => $task->getUniqueSourceId()
	 	);
	 	$values = array();
	 	if ((int)$task->getTries() + 1 >= AI::getInstance()->getSetup()->numberOfTries) {
	 		$values["idState"] = State::ERROR;
	 		$values["tries"] = (int)$task->getTries() + 1;
	 	} else {
	 		$values["tries"] = (int)$task->getTries() + 1;
	 	}
	 	
	 	$result = $this->connection->setValues(DBCONN_UPDATE, ConnectionDB::TASK, $values, $whereCondition, $whereValues);
	 	
	 	return $result;
	 }
	 
	 public function getTasksByState(int $idState)
	 {
	 	
	 	$query = "SELECT * FROM Task WHERE idState = :IDSTATE";
	 	$dataValues = array();
	 	$dataValues["IDSTATE"] = $idState;
	 	$tasksFound = array();
	 	try {
	 		$result = $this->connection->execute($query, $dataValues);
	 		$tasksFound = $this->generateTasks($result);
	 	} catch (SQLException $ex) {
	 		Functions::addLog($ex->getMessage(), Functions::ERROR, $ex->getTrace());
	 	}
	 	
	 	return $tasksFound;
	 }
	 
	 public function getTargetLanguagesByTask(string $uniqueSourceId)
	 {
	 	
	 	$values = array();
	 	$values["IDTASK"] = $uniqueSourceId;
	 	$query = 'SELECT t.idWordAnalysis, l.* FROM ' . ConnectionDB::TARGETLANGUAGE . ' t, Language l WHERE t.idLanguage = l.idLanguage AND t.uniqueSourceId = :IDTASK';
	 	$targetLanguage = array(); 
	 	$result = null;
	 	try {
	 		$result = $this->connection->execute($query, $values);
	 		
	 		while ($result->fetch()) {
	 			$language = new Language();
	 			$language->setIdLanguage($result->IDLANGUAGE);
	 			$language->setLanguageName($result->LANGUAGENAME);
	 			$language->setAnalysis($this->getAnalysis($result->IDWORDANALYSIS));
	 			$targetLanguage[] = $language;
	 		}
	 	} catch (SQLException $e) {
	 		if ($result != null) {
	 			$result->close();
	 		}
	 		
	 		Functions::addLog($e->getMessage(), Functions::ERROR, json_encode($e->getTrace()));
	 	}finally {
	 		if($result != null){
	 			$result->close();
	 		}
	 	}
	 	return $targetLanguage;
	 }
	 
	 public function getFilesByTask(string $fileType, string $uniqueSourceId)
	 {
	 	
	 	$values = array();
	 	
	 	$values["IDTASK"] = $uniqueSourceId;
	 	
	 	if ($fileType == 'TRANSLATE')
	 		$query = 'SELECT f.* FROM ' . ConnectionDB::FILE . ' f, ' . ConnectionDB::TASKFILESTOTRANSLATE . ' t WHERE t.uniqueSourceId = :IDTASK AND t.idFile = f.idFile';
	 		else
	 			$query = 'SELECT f.* FROM ' . ConnectionDB::FILE . ' f, ' . ConnectionDB::TASKFILESFORREFERENCE . ' t WHERE t.uniqueSourceId = :IDTASK AND t.idFile = f.idFile';
	 			
	 			$files = array();
	 			$result = null;
	 			try {
	 				$result = $this->connection->execute($query, $values);
	 				while ($result->fetch()) {
	 					$file = new File();
	 					$file->setFileName($result->fileName);
	 					$file->setFileNameExtension($result->fileNameExtension);
	 					$file->setMimeType($result->mimeType);
	 					$file->setDescription($result->description);
	 					$file->setTimeStamp($result->timeStamp);
	 					$file->setSourcePath($result->sourcePath);
	 					$file->setSize($result->size);
	 					$file->setPath($result->path);
	 					$file->setChecksum($result->checksum);
	 					$file->setIdFile($result->idFile);
	 					$file->setPathForDownload($result->downloadPath);
	 					$file->setAsyncHash($result->asyncHash);
	 					$file->setSuccessfulDownload($result->successfullDownload);
	 					$file->setIsDelete($result->isDelete == 1);
	 					$files[] = $file;
	 				}
	 				
	 				$result->close();
	 			} catch (SQLException $e) {
	 				if ($result != null)
	 					Functions::addLog($e->getMessage(), Functions::ERROR, json_encode($e->getTrace()));
	 			}
	 			
	 			return $files;
	 }
	 
	 private function getCategory(int $idCategory)
	 {
	 	
	 	$query = "SELECT * FROM Category WHERE idCategory = :IDCATEGORY";
	 	$dataValues = array();
	 	$dataValues["IDCATEGORY"] = $idCategory;
	 	$result = $this->connection->execute($query, $dataValues);
	 	if ($result->fetch()) {
	 		$category = new Category($result->idCategory, $result->categoryName, $result->categoryDescription);
	 	}
	 	return $category;
	 }
	 
	 private function getSourceLanguage(string $idLanguage)
	 {
	 	
	 	$query = "SELECT * FROM Language WHERE idLanguage = :IDLANGUAGE";
	 	$dataValues = array();
	 	$dataValues["IDLANGUAGE"] = $idLanguage;
	 	$result = $this->connection->execute($query, $dataValues);
	 	$language = new Language();
	 	if ($result->fetch()) {
	 		$language->setIdLanguage($result->idLanguage);
	 		$language->setLanguageName($result->languageName);
	 	}
	 	$result->close();
	 	return $language;
	 }
	 
	 /**
	  *
	  * @author phidalgo
	  *         Given a task id returns their change history
	  * @param string $usid
	  * @return State[]
	  */
	 public function getTaskStatusHistory($usid)
	 {
	 	
	 	$stateHistory = array();
	 	$sql = "SELECT * from " . ConnectionDB::TASKCHANGEHISTORY . " where uniqueSourceId=:USID order by timeStamp asc";
	 	$q = $this->connection->execute($sql, array(
	 			"USID" => $usid
	 	));
	 	while ($q->fetch()) {
	 		$state = new State($q->idState);
	 		$state->setTimeStamp(\DateTime::createFromFormat("Y-m-d H:i:s", $q->timeStamp));
	 		$state->setDescription($q->description);
	 		$stateHistory[] = $state;
	 	}
	 	$q->close();
	 	
	 	return $stateHistory;
	 }
	 
	 public function cancelExpiredTasks(int $interval, int $idSource = null)
	 {
	 	
	 	$tasks = array();
	 	$values = array();
	 	$values["INTERVAL"] = $interval;
	 	$query = 'SELECT DISTINCT t.uniqueSourceId FROM Task t, ' . ConnectionDB::TASKCHANGEHISTORY . ' c WHERE t.uniqueSourceId = c.uniqueSourceId AND t.idState = ' . State::PENDINGCONFIRMATION . ' AND DATE_ADD(c.timeStamp, INTERVAL :INTERVAL HOUR) <= NOW()';
	 	
	 	$result = $this->connection->execute($query, $values);
	 	
	 	while ($result->fetch()) {
	 		$task = new Task();
	 		$task->setUniqueSourceId($result->UNIQUESOURCEID);
	 		$tasks[] = $task;
	 	}
	 	
	 	$result->close();
	 	
	 	foreach ($tasks as $task) {
	 		$this->updateTaskState($task, State::REJECTEDBYCONFIRMTIMEOUT, 'The task has exceeded the waiting time');
	 	}
	 }
	 
	 public function getTaskToReprocess()
	 {
	 	
	 	$values = array();
	 	$values["IDSTATE"] = State::RETRY;
	 	$query = 'SELECT * FROM Task WHERE idState = ' . State::CONFIRMED . ' OR idState = ' . State::FILTERED . ' OR idState = ' . State::EXECUTINGACTIONS . ' OR idState = :IDSTATE';
	 	
	 	$result = $this->connection->execute($query, $values);
	 	
	 	$tasks = $this->generateTasks($result);
	 	return $tasks;
	 }

    private function getTaskObject($result)
    {
        $task = new Task();
        $task->setUniqueSourceId($result->uniqueSourceId);
        $task->setUniqueTaskId($result->uniqueTaskId);
        $task->setTimeStamp($result->timeStamp);
        $task->setIdClientetask($result->idClientTask);
        $task->setTitle($result->title);
        $task->setAssignedUser($result->assignedUser);
        $task->setMenssage($result->message);
        $task->setStartDate($result->startDate);
        $task->setDueDate($result->dueDate);
        $task->setTries($result->tries);
        $idCategory = $result->idCategory;
        $sourceLanguage = $result->sourceLanguage;
        $state = new State($result->idState);
        $task->setState($state);
        $task->setRelatedOriginalData($result->relatedOriginalData);

        $task->setStatusHistory($this->getTaskStatusHistory($task->getUniqueSourceId()));
        $task->setCategory($this->getCategory($idCategory));
        $task->setTargetsLanguage($this->getTargetLanguagesByTask($task->getUniqueSourceId()));
        $task->setFilesToTranslate($this->getFilesByTask("TRANSLATE", $task->getUniqueSourceId()));
        $task->setFilesForReference($this->getFilesByTask("REFERENCE", $task->getUniqueSourceId()));
        $task->setSourceLanguage($this->getSourceLanguage($sourceLanguage));
        $task->setDataSource($this->findDataSource($task->getUniqueSourceId()));
// 	 		$task->setAnalysis($this->getAnalysis($task->getUniqueSourceId()));
        $task->setAditionalProperties($this->getAditionalPropertiesByTask($task->getUniqueSourceId()));
        $task->setTranslationMemories($this->getTMSByTask($task->getUniqueSourceId()));
        $task->setIdProcess($result->idProcess);
        $task->setIdActions($result->idAction);

        $daoErrors = new TaskErrorDAO();
        try {
            $task->setTaskErrors($daoErrors->getErrorsByUniqueSourceId($result->uniqueSourceId));
        } catch (AIException $e) {
            Functions::logException($e->getMessage(), Functions::ERROR, __CLASS__, $e);
        }

        return $task;
    }
	 
	 /**
	  *
	  *
	  * @param mixed $result
	  * @return \model\Task[]
	  * @return
	  */
	 private function generateTasks(&$result)
	 {
	 	$tasks = array();
	 	$daoErrors = new TaskErrorDAO();
	 	while ($result->fetch()) {
	 		$task = new Task();
	 		$task->setUniqueSourceId($result->uniqueSourceId);
	 		$task->setUniqueTaskId($result->uniqueTaskId);
	 		$task->setTimeStamp($result->timeStamp);
	 		$task->setIdClientetask($result->idClientTask);
	 		$task->setTitle($result->title);
	 		$task->setAssignedUser($result->assignedUser);
	 		$task->setMenssage($result->message);
	 		$task->setStartDate($result->startDate);
	 		$task->setDueDate($result->dueDate);
	 		$task->setTries($result->tries);
	 		$idCategory = $result->idCategory;
	 		$sourceLanguage = $result->sourceLanguage;
	 		$state = new State($result->idState);
	 		$task->setState($state);
	 		$task->setRelatedOriginalData($result->relatedOriginalData);

	 		$task->setStatusHistory($this->getTaskStatusHistory($task->getUniqueSourceId()));
	 		$task->setCategory($this->getCategory($idCategory));
	 		$task->setTargetsLanguage($this->getTargetLanguagesByTask($task->getUniqueSourceId()));
	 		$task->setFilesToTranslate($this->getFilesByTask("TRANSLATE", $task->getUniqueSourceId()));
	 		$task->setFilesForReference($this->getFilesByTask("REFERENCE", $task->getUniqueSourceId()));
	 		$task->setSourceLanguage($this->getSourceLanguage($sourceLanguage));
	 		$task->setDataSource($this->findDataSource($task->getUniqueSourceId()));
// 	 		$task->setAnalysis($this->getAnalysis($task->getUniqueSourceId()));
	 		$task->setAditionalProperties($this->getAditionalPropertiesByTask($task->getUniqueSourceId()));
	 		$task->setTranslationMemories($this->getTMSByTask($task->getUniqueSourceId()));
	 		$task->setIdProcess($result->idProcess);
	 		$task->setIdActions($result->idAction);


            try {
                $task->setTaskErrors($daoErrors->getErrorsByUniqueSourceId($result->uniqueSourceId));
            } catch (AIException $e) {
                Functions::logException($e->getMessage(), Functions::ERROR, __CLASS__, $e);
            }
	 		
	 		$tasks[] = $task;
	 	}
	 	
	 	$result->close();
	 	
	 	return $tasks;
	 }
	 
	 public function saveTargetLanguage(Language $targetLanguage, string $uniqueSourceId)
	 {
	 	$analysis = $targetLanguage->getAnalysis();
	 	$values = array();
	 	$values["percentage_101"] = $analysis->getPercentage_101() . "";
	 	$values["repetitions"] = $analysis->getRepetition() . "";
	 	$values["percentage_100"] = $analysis->getPercentage_100() . "";
	 	$values["percentage_95"] = $analysis->getPercentage_95() . "";
	 	$values["percentage_85"] = $analysis->getPercentage_85() . "";
	 	$values["percentage_75"] = $analysis->getPercentage_75() . "";
	 	$values["percentage_50"] = $analysis->getPercentage_50() . "";
	 	$values["machineTranslation"] = $analysis->getMachineTanslation() . "";
	 	$values["minute"] = $analysis->getMinute() . "";
	 	$values["weightedWords"] = $analysis->getWeightedWord() . "";
	 	$values["notMatch"] = $analysis->getNotMatch() . "";
	 	$values["totalWords"] = $analysis->getTotalWords() . "";
	 	$result = $this->connection->setValues(DBCONN_INSERT, ConnectionDB::ANALYSIS, $values);
	 	
	 	$values = array();
	 	$values["uniqueSourceId"] = $uniqueSourceId;
	 	$values["idLanguage"] = $targetLanguage->getIdLanguage();
	 	$values["idWordAnalysis"] = $result;
	 	$resulTargetLanguage = $this->connection->setValues(DBCONN_INSERT, ConnectionDB::TARGETLANGUAGE, $values);
	 	
	 	
	 	return $resulTargetLanguage;
	 	
	 }
	 
	 public function getTaskCollected()
	 {
	 	
	 	$values = array();
	 	$values["IDSTATE"] = State::COLLECTED;
	 	$values["IDSTATEPRE"] = State::PREPROCESSED;
	 	$query = 'SELECT * FROM Task WHERE idState = :IDSTATE or idState = :IDSTATEPRE';
	 	
	 	$result = $this->connection->execute($query, $values);
	 	
	 	$tasks = $this->generateTasks($result);
	 	
	 	return $tasks;
	 }
	 
	 /***
	  * Validate if async dowload has finished
	  *
	  * @param string $asyncHash dowload token
	  * @return array data file
	  */
	 public function getTaskFileAsyncDownload(string $asyncHash = null)
	 {
	 	$file = array();
	 	$values = array(
	 			'ASYNCHASH' => $asyncHash
	 	);
	 	$query = 'SELECT * FROM ' . ConnectionDB::TASKFILEASYNCDOWNLOAD . ' WHERE asyncHash = :ASYNCHASH';
	 	
	 	$result = $this->connection->execute($query, $values);
	 	
	 	if ($result->fetch()) {
	 		$file['isDownloading'] = $result->isDownloading == 1;
	 		$file['isDownloaded'] = $result->isDownloaded == 1;
	 		$file['downloadPath'] = $result->downloadPath;
	 		$file['filePath'] = $result->filePath;
	 		$file['endDownloadDate'] = $result->endDownloadDate;
	 		$file['startDownloadDate'] = $result->startDownloadDate;
	 	}
	 	$result->close();
	 	
	 	return $file;
	 }
	 
	 public function saveTM(TM $tm, string $uniqueSourceId)
	 {
	 	
	 	$idFile = $this->saveFile($tm->getFile(), $uniqueSourceId, true);
	 	$values = array();
	 	$values["idFile"] = $idFile;
	 	$values["sourceLanguage"] = $tm->getSourceLanguage()->getIdLanguage();
	 	$values["isMachineTrans"] = $tm->isIsMachineTranslation() . '';
	 	$values["version"] = $tm->getVersion();
	 	$values["uniqueSourceId"] = $uniqueSourceId;
	 	$values["targetLanguage"] = $tm->getTargetLanguage()->getIdLanguage();
	 	$idTM = $this->connection->setValues(DBCONN_INSERT, ConnectionDB::TASKFILESTM, $values);
	 	
	 	
	 }
	 
	 public function getTMSByTask(string $uniqueSourceId)
	 {
	 	
	 	$values = array();
	 	$values["UNIQUESOURCEID"] = $uniqueSourceId;
	 	$query = "SELECT * FROM " . ConnectionDB::TASKFILESTM . " WHERE uniqueSourceId = :UNIQUESOURCEID";
	 	$result = $this->connection->execute($query, $values);
	 	$tms = array();
	 	while ($result->fetch()) {
	 		$tm = new TM();
	 		$tm->setIdTaskFilesTM($result->idTaskFilesTM);
	 		$tm->setIsMachineTranslation($result->isMachineTrans);
	 		$sourceLanguage = new Language();
	 		$sourceLanguage->setIdLanguage($result->sourceLanguage);
	 		$targetLanguage = new Language();
	 		$targetLanguage->setIdLanguage($result->targetLanguage);
	 		$tm->setSourceLanguage($sourceLanguage);
	 		$tm->setTargetLanguage($targetLanguage);
	 		$tm->setVersion($result->version);
	 		$tm->setFile($this->getFile($result->idFile));
// 	 		$tm->setTargetLanguages($this->getTargetLanguagesByTM($result->idTaskFilesTM));
	 		$tms[] = $tm;
	 	}
	 	return $tms;
	 }
	 
	 
	 
	 public function getUniqueSourceIdByFile(int $idFile, string $typeFile){
	 	$values = array("IDFILE" => $idFile);
	 	$uniqueSourceId = null;
	 	if($typeFile == File::SOURCE){
	 		$table = ConnectionDB::TASKFILESTOTRANSLATE;
	 	}else{
	 		$table = ConnectionDB::TASKFILESFORREFERENCE;
	 	}
	 	
	 	$query = "Select * from ".$table." where idFile = :IDFILE";
	 	
	 	$result = $this->connection->execute($query,$values);
	 	
	 	if($result->fetch()){
	 		$uniqueSourceId = $result->uniqueSourceId;
	 	}
	 	
	 	$result->close();
	 	return $uniqueSourceId;
	 }

    public function getTaskByClientId($sourceName, $idClientTask){
        $query = "select t.uniqueTaskId as id from Task t, TaskSource d where t.idClientTask = :IDCLIENTTASK and t.uniqueSourceId = d.uniqueSourceId and d.sourceName = :SOURCENAME";
        $values = array(
            "IDCLIENTTASK" => $idClientTask,
            "SOURCENAME" => $sourceName
        );
        $task = null;
        $id = null;
        try{
            $result = $this->connection->execute($query,$values);

            if($result->fetch()){
                $id = $result->id;
            }

            if($id !== null){
                $task = $this->getTaskByUTID($id);
            }
        }catch (\dbconn\SQLException $ex){
            throw $ex;
        }

        return $task;
    }
	 
}
