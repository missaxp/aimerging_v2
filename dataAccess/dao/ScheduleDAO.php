<?php

/**
* @author zroque
* @version 1.0
* created on 26 jun. 2018
*/
namespace dataAccess\dao;

use dataAccess\interfaces\ISchedule;
use dataAccess\SQLException;
use dataAccess\dbConn;
use dataAccess\ConnectionDB;
use model\Schedule;

include_once BaseDir. '/dataAccess/interfaces/ISchedule.php';
include_once BaseDir. '/model/Schedule.php';
class ScheduleDAO implements ISchedule {
	
	/**
	 *
	 * @var dbConn
	 */
	private $connection;

	public function __construct(){

		$dataBase = new ConnectionDB();
		$this->connection = $dataBase->getConnection();
	}

	public function getAll(){

		$schedulesFound = array();
		$query = "SELECT * FROM ".ConnectionDB::PROCESSSCHEDULES;
		try {
			$result = $this->connection->execute($query);
			while($result->fetch()) {
				$schedule = new Schedule();
				$schedule->setIdSchedule($result->getVal("idSchedule"));
				$schedule->setProcessType($result->getVal("processType"));
				$schedule->setStart($result->getVal("start"));
				$schedule->setEnd($result->getVal("end"));
				$schedule->setInterval($result->getVal("processInterval"));
				$schedule->setIdProcess($result->getVal("idProcess"));
				$schedulesFound[] = $schedule;
			}
		} catch(SQLException $ex) {
			\Functions::addLog($ex->getMessage(), \Functions::ERROR, json_encode($ex->getTrace()));
		} finally{
			$result->close();
		}
		
		return $schedulesFound;
	}

	public function updateSchedule($schedule){

		$dataValues = array();
		$dataValues["processType"] = $schedule->getProcessType();
		$dataValues["start"] = $schedule->getStart();
		$dataValues["end"] = $schedule->getEnd();
		$dataValues["processInterval"] = $schedule->getInterval();
		
		$whereValues = array();
		$whereValues["idSchedule"] = $schedule->getIdSchedule();
		
		$action = "M";
		$whereCondition = "idSchedule = :IDSCHEDULE";
		$result = $this->connection->setValues($action, ConnectionDB::PROCESSSCHEDULES, $dataValues, $whereCondition, $whereValues);
		
		return $result;
	}

	public function getScheduleByProcess(int $idProcess){

		$dataValues = array();
		$dataValues["IDPROCESS"] = $idProcess;
		$dataValues["IDPROCESS2"] = $idProcess;
		$query = "SELECT * FROM ".ConnectionDB::PROCESSSCHEDULES." WHERE  CURRENT_TIME() BETWEEN start AND end AND idProcess = :IDPROCESS 
					OR (NOT CAST(CURRENT_TIME() AS time) BETWEEN end AND start AND start > end) AND 
					idProcess = :IDPROCESS2";
		
		$result = $this->connection->execute($query, $dataValues);
		$schedule = null;
		if($result->fetch()) {
			$schedule = new Schedule();
			$schedule->setIdSchedule($result->idSchedule);
			$schedule->setProcessType($result->processType);
			$schedule->setStart($result->start);
			$schedule->setEnd($result->end);
			$schedule->setInterval($result->processInterval);
			$schedule->setIdProcess($result->idProcess);
			
		}
		$result->close();
		return $schedule;
	}

	public function deleteSchedule($idSchedule){

		$dataValues = array();
		$dataValues["IDSCHEDULE"] = $idSchedule;
		$query = "DELETE FROM ".ConnectionDB::PROCESSSCHEDULES." WHERE idSchedule = :IDSCHEDULE";
		try {
			$result = $this->connection->execute($query, $dataValues);
		} catch(SQLException $ex) {
			\Functions::addLog($ex->getMessage(), \Functions::ERROR, json_encode($ex->getTrace()));
		}
		
		return ($result > 0);
	}

	public function saveSchedule($schedule){

		$dataValues = array();
		$dataValues["processType"] = $schedule->getProcessType();
		$dataValues["start"] = $schedule->getStart();
		$dataValues["end"] = $schedule->getEnd();
		$dataValues["processInterval"] = $schedule->getInterval();
		$dataValues["idProcess"] = $schedule->getIdProcess();
		
		$action = "C";
		$table = ConnectionDB::PROCESSSCHEDULES;
		$result = $this->connection->setValues($action, $table, $dataValues);
		
		return $result;
	}

	public function getSchedule($idSchedule){

		$dataValues = array();
		$dataValues["IDSCHEDULE"] = $idSchedule;
		$query = "SELECT * FROM ".ConnectionDB::PROCESSSCHEDULES." WHERE idSchedule = :IDSCHEDULE";
		try {
			$result = $this->connection->execute($query, $dataValues);
			$schedule = new Schedule();
			while($result->fetch()) {
				$schedule->setIdSchedule($result->idSchedule);
				$schedule->setProcessType($result->processType);
				$schedule->setStart($result->start);
				$schedule->setEnd($result->end);
				$schedule->setInterval($result->processInterval);
				$schedule->setIdProcess($result->idProcess);
			}
		} catch(SQLException $ex) {
			\Functions::addLog($ex->getMessage(), \Functions::ERROR, json_encode($ex->getTrace()));
		} finally{
			$result->close();
		}
		
		return $schedule;
	}
}
?>