<?php

use common\exceptions\AIException;
use dataAccess\ConnectionDB;
use dataAccess\SQLException;

include_once BaseDir . '/dataAccess/ConnectionDB.php';
include_once BaseDir . '/dataAccess/interfaces/ITaskError.php';

class TaskErrorDAO implements ITaskError{
    private $connection;

    public function __construct() {
        $dataBase = new ConnectionDB();
        $this->connection = $dataBase->getConnection();
    }

    /**
     * TaskError[]
     * @param string $uniqueSourceId
     * @return array
     * @throws AIException
     */
    public function getErrorsByUniqueSourceId(string $uniqueSourceId){
        $response = [];
        $query = 'SELECT uniqueSourceId, location, additionalData, timeStamp,idPredefinedError, name, description FROM  TaskErrors as t, TaskPredefinedError as t2 where t.uniqueSourceId = :usi and t.idPredefinedError = t2.idTaskPredefinedError;';
        try{
            $result = $this->connection->execute($query, array(
                "usi" => $uniqueSourceId
            ));
            while($result->fetch()){
//                var_export($result);
                $error = new TaskError();
                $predefinedError = new TaskPredefinedError();
                $predefinedError->setDescription($result->getVal("DESCRIPTION"));
                $predefinedError->setIdPredefinedError($result->getVal("IDPREDEFINEDERROR"));
                $predefinedError->setName($result->getVal("NAME"));

                $error->setUniqueSourceId($result->getVal("UNIQUESOURCEID"));
                $error->setAdditionalData($result->getVal("ADDITIONALDATA"));
                $error->setLocation($result->getVal("LOCATION"));
                $error->setTimeStamp(new DateTime($result->getVal("TIMESTAMP")));

                $error->setPredefinedError($predefinedError);
                $response[] = $error;
                continue;
            }

            $result->close();
        }catch (SQLException $e){
            throw AIException::createInstanceFromSQLException($e)->construct(__METHOD__, __NAMESPACE__, $args = func_get_args());
        }
        catch (Throwable $t){
            throw AIException::createInstanceFromThrowable($t)->construct(__METHOD__, __NAMESPACE__, $args = func_get_args());
        }

        return $response;
    }

    /**
     * @param TaskError[] $errors
     * @throws AIException
     */
    public function saveErrors(array $errors){
        foreach ($errors as $error){
            $predefinedError = $error->getPredefinedError();
            if(!isset($predefinedError) || $predefinedError == null){
                $predefinedError = new TaskPredefinedError();
                $predefinedError->setIdPredefinedError(1);
            }
            if(!$this->validatePredefinedError($predefinedError)){
                $ex = (new AIException("Predefined error not found in database"))->construct(__METHOD__, __NAMESPACE__, $f = func_get_args());
                throw $ex;
            }

            $vars["uniqueSourceId"] = $error->getUniqueSourceId();
            $vars["idPredefinedError"] = $predefinedError->getIdPredefinedError();
            $vars["location"] = $error->getLocation();
            $additionalData = $error->getAdditionalData();
            if(isset($additionalData)){
                $vars["additionalData"] = $error->getAdditionalData();
            }

            try{
                $this->connection->setValues(DBCONN_INSERT, "TaskErrors", $vars);
            } catch (SQLException $e) {
                throw AIException::createInstanceFromSQLException($e)->construct(__METHOD__,__NAMESPACE__, $f = func_get_args(), ["sql_vars" => $vars]);
            }
        }


    }

    /**
     * @param TaskPredefinedError $predefinedError
     * @return bool
     * @throws AIException
     */
    public function validatePredefinedError(TaskPredefinedError $predefinedError){
        $query = 'SELECT * FROM  TaskPredefinedError where idTaskPredefinedError = :id;';
        try{
            $result = $this->connection->execute($query, array(
                "id" => $predefinedError->getIdPredefinedError()
            ));
            if($result->fetch()){
                return true;
            }
        }catch (SQLException $e){
            throw AIException::createInstanceFromSQLException($e)->construct(__METHOD__, __NAMESPACE__, $args = func_get_args());
        }
        catch (Throwable $t){
            throw AIException::createInstanceFromThrowable($t)->construct(__METHOD__, __NAMESPACE__, $args = func_get_args());
        }
        return false;
    }
}
