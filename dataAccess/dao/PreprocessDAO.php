<?php

namespace dataAccess\dao;

use dataAccess\dbConn;
use dataAccess\ConnectionDB;
use dataAccess\interfaces\IPreprocess;
use model\Preprocess;
use model\Rule;

include_once BaseDir . '/dataAccess/interfaces/IPreprocess.php';
include_once BaseDir . '/model/Preprocess.php';
include_once BaseDir . '/dataAccess/ConnectionDB.php';
include_once BaseDir . '/dataAccess/dao/RuleDAO.php';

class PreprocessDAO implements IPreprocess {

	/**
	 *
	 * @var dbConn
	 */
	private $connection;

	public function __construct(){

		$dataBase = new ConnectionDB();
		$this->connection = $dataBase->getConnection();
	}

	public function getPreprocessBySource(int $idSource){
		$query = "SELECT * FROM ".ConnectionDB::PREPROCESS." where idSource = :IDSOURCE AND enabled = 1";
		$values = array();
		$values["IDSOURCE"] = $idSource;
		$result = $this->connection->execute($query, $values);
		$preprocess = array();
		while($result->fetch()){
			$process = new Preprocess();
			$process->setIdSource($result->idPreprocess);
			$process->setIdPreprocess($result->idPreprocess);
			$process->setPreprocessName($result->preprocessName);
			$process->setEnabled($result->enabled);
			$process->setCodeToExecute($result->codeToExecute);
			$process->setIdSource($result->idSource);
			$ruleDAO = new RuleDAO();
			$process->setRules($ruleDAO->getRulesByPreprocess($process->getIdPreprocess()));
			$preprocess[] = $process;
		}
		$result->close();
		return $preprocess;
	}

	public function savePreprocess(){

	}
}

