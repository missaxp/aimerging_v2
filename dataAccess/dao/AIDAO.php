<?php

namespace dataAccess\dao;

use common\exceptions\AIException;
use dataAccess\interfaces\IAI;
use dataAccess\dbConn;
use dataAccess\ConnectionDB;
use dataAccess\SQLException;
use core\AI;
use Exception;
use service\EmailManager;
use Throwable;

include_once BaseDir. '/dataAccess/ConnectionDB.php';
include_once BaseDir. '/dataAccess/interfaces/IAI.php';
include_once BaseDir. '/common/exceptions/AIException.php';
include_once BaseDir. '/Functions.php';

class AIDAO implements IAI{
	
	/**
	 * 
	 * @var dbConn
	 */
	private $connection;
	
	public function __construct(){
		$dataBase = new ConnectionDB();
		$this->connection = $dataBase->getConnection();
	}
	
	public function saveExecution(string $startDate, string $type) {
		$values = array();
		$id = null;
		$values["idLastExecution"] = md5($startDate.$type);
		if($this->validateExecutionId($values["idLastExecution"])){
			$id = $values["idLastExecution"];
			$values["startExecution"] = array(
					$startDate,
					DBCONN_DATATYPE_DATE
			);
			$values["isExecuting"] = true;
			$values["executionType"] = $type;
			
			try{
				$this->connection->setValues(DBCONN_INSERT, ConnectionDB::LASTEXECUTION, $values);
			}catch (throwable $e){
				return null;
			}
		}
		return $id;
	}

	public function getLastExecuting(string $typeExecuting){
	    $values = array("TYPE" => $typeExecuting);
        $query = "Select endExecution from ".ConnectionDB::LASTEXECUTION." where executionType = :TYPE order by endExecution desc limit 1";
        $date = null;
        $result = null;
        try{
            $result = $this->connection->execute($query,$values);
            if($result->fetch()){
                $date = new \DateTime($result->endExecution);
            }
        }catch (SQLException $ex){

        }finally{
            if($result != null){
                $result->close();
            }
        }

        return $date;
    }
	
	public function validateExecutionId($id){
		$values = array("ID"=>$id);
		$validation = true;
		$query = "Select idLastExecution from ".ConnectionDB::LASTEXECUTION." where idLastExecution = :ID";
		
		try{
			$result = $this->connection->execute($query,$values);
			if($result->fetch()){
				$validation = false;
			}
			
			$result->close();
		}catch(\Exception $e){
			
		}
		
		return $validation;
	}

    /**
     * @param string $level Error levele; WARNING, INFO etc
     * @param string $error "ERROR description"
     * @param string $class
     * @return bool|\dataAccess\dbStm|null
     * @throws AIException
     */
    public function saveError(string $level, string $error, string $class) {
		$values = array();
		
		$values["errorType"] = $level;
		$values["errorDate"] = date('Y-m-d');
		$values["class"] = $class;
		
		try{
			//check if the error has been registered in the same date 
			$query = "select errorType, errorDate, class from ErrorLog where errorType = :errorType and 
						DATE(errorDate) = :errorDate and class = :class";
			$result = $this->connection->execute($query, $values);
// 			$result->close();
			
			if($result->fetch()){
				$result->close();
				//If exists just update the number of incidences
				$query = "update ErrorLog set numberOfIncidences = numberOfIncidences + 1 where errorType = :errorType
							and DATE(errorDate) = :errorDate and class = :class";
				$result = $this->connection->execute($query, $values);
			}else{
				$result->close();
				//if not exists register the error
				$values = array();
				
				$values["errorType"] = $level;
				$values["description"] = $error;
				$values["errorDate"] = array(
						\Functions::currentDate(),
						DBCONN_DATATYPE_DATE
				);
				$values["class"] = $class;
				
				$result = $this->connection->setValues(DBCONN_INSERT, ConnectionDB::ERRORLOG, $values);
			}
		}catch (SQLException $ex){
			throw AIException::createInstanceFromSQLException($ex);
		} catch (Throwable $e) {
            throw AIException::createInstanceFromThrowable($e);
        }

        return $result;
	}

	public function saveEndExecution(string $idExecution, string $endDate, string $status) {
		$values = array();
		$values["endExecution"] = array(
				$endDate,
				DBCONN_DATATYPE_DATE
		);
		$values["isExecuting"] = "0";
		$values["executionStatus"] = $status;
		
		$whereValues = array();
		$whereValues["idLastExecution"] = $idExecution;
		
		$whereConditions = 'idLastExecution = :IDLASTEXECUTION';
		
		$result = $this->connection->setValues(DBCONN_UPDATE, ConnectionDB::LASTEXECUTION, $values,$whereConditions, $whereValues);
		
		return $result;
	}

	public function isExecuting(string $executionType) {
		$values = array();
		$values["EXECUTIONTYPE"] = $executionType;
		$query = 'SELECT * FROM '.ConnectionDB::LASTEXECUTION.' WHERE isExecuting = 1 and executionType = :EXECUTIONTYPE';
		$result = $this->connection->execute($query,$values);
		if($result->fetch()){
			$value = new \DateTime($result->startExecution);
			$value->add(new \DateInterval('P0Y0M0DT0H30M0S'));
			$currentValue = new \DateTime();
			if($currentValue > $value){
				$this->saveEndExecution($result->idLastExecution, \Functions::currentDate(), "FAIL");
				return false;
			}
			$result->close();
			return true;
		}
		
		$result->close();
		return false;
	}
	
	public function closeConnection(){
		$dataBase = new ConnectionDB();
		$dataBase->closeConnection();
	}
	
	public function getConfigurationLog() {
		$configuration = array();
		$query = 'SELECT * FROM LogConfiguration';
		
		$result = $this->connection->execute($query);
		
		if($result->fetch()){
			$configuration["sendTo"] = $result->getVal("sendTo");
			$configuration["lastSend"] = $result->getVal("lastSend");
			$configuration["sendInterval"] = $result->getVal("sendInterval");
		}
		
		$result->close();
		
		return $configuration;
	}
	
	public function saveLastShipment(){
		$values = array();
		$values["lastSend"] = \Functions::currentDate();
		
		$result = $this->connection->setValues(DBCONN_UPDATE, "LogConfiguration", $values);
		
		return ($result > 0);
	}
	
	public function getLog($date){
		$query = 'SELECT * FROM LogConfiguration WHERE errorDate >= :LASTDATE';
		$values = array();
		$values["LASTDATE"] = array($date,DBCONN_DATATYPE_DATE);
		$logs = array();
		$result = $this->connection->execute($query,$values);
		
		while ($result->fetch()){
			$log = array();
			
			$log["errorType"] = $result->ERRORTYPE;
			$log["description"] = $result->DESCRIPTION;
			$log["errorDate"] = $result->ERRORDATE;
			$log["class"] = $result->CLASS;
			
			$logs[] = $log;
		}
		
		$result->close();
		return $logs;
	}
}