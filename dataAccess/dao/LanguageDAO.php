<?php

/**
* @author Ljimenez
* @version 1.0
* created on 26 jun. 2018
*/
namespace dataAccess\dao;

use dataAccess\dbConn;
use dataAccess\interfaces\ILanguaje;
use model\Language;
use dataAccess\ConnectionDB;

include_once BaseDir. '/dataAccess/ConnectionDB.php';
include_once BaseDir. '/dataAccess/interfaces/ILanguage.php';
include_once BaseDir. '/model/Language.php';

class LanguageDAO implements ILanguaje{
	
	/**
	 *
	 * @var dbConn
	 */
	private $connection;
	
	public function __construct(){
		$dataBase = new ConnectionDB();
		$this->connection = $dataBase->getConnection();
	}

	public function updateLanguage(Language $language) {
		
	}

	public function getLanguage(string $idLanguage) {
		$query = "SELECT * FROM Language WHERE idLanguage LIKE :IDLANGUAGE";
		$parameters = array();
		$parameters["IDLANGUAGE"] = $idLanguage;
		$result = $this->connection->execute($query,$parameters);
		$language = null;
		if($result->fetch()){
			$language = new Language();
			$language->setIdLanguage($result->IDLANGUAGE);
			$language->setLanguageName($result->LANGUAGENAME);
		}
		
		$result->close();
		
		return $language;
	}

	public function getAll() {
		$languages = array();
		$query = "SELECT * FROM Language order by languageName";
		$result = $this->connection->execute($query);
		
		while($result->fetch()){
			$language = new Language();
			$language->setIdLanguage($result->getVal("idLanguage"));
			$language->setLanguageName($result->getVal("languageName"));
			$languages[] = $language;
		}
		$result->close();
		
		return $languages;
	}

	public function getAllLanguage() {
		$languages = array();
		$query = "SELECT * FROM Language order by languageName";
		$result = $this->connection->execute($query);
		
		while($result->fetch()){
			$language = array();
			$language['idlanguage'] = $result->getVal("idLanguage");
			$language['languagename'] = $result->getVal("languageName");
			$languages[] = $language;
		}
		$result->close();
		
		return $languages;
	}

	public function deleteLanguage(int $idLanguage) {
		
	}

	public function saveLanguage(Language $language) {
		$values = array();
		$values["IDLANGUAGE"] = $language->getIdLanguage();
		$values["LANGUAGENAME"] = $language->getLanguageName();
		
		$result = $this->connection->setValues(DBCONN_INSERT, ConnectionDB::LANGUAGE, $values);
		
		return ($result>0);
	}
	
	public function guardarIdiomasMoravia($nombreLanguage, $numberLanguage){
		$values = array();
		$values["NAME"] = $nombreLanguage;
		$query = 'SELECT l.idLanguage from Language l WHERE languageName = :NAME';
		
		$result = $this->connection->execute($query,$values);
		
		if($result->fetch()){
			$values = array();
			$idLanguage = $result->IDLANGUAGE;
			$values["idMoraviaLanguages"] = $numberLanguage;
			$values["idLanguage"] = $idLanguage;
			
			$this->connection->setValues(DBCONN_INSERT, "MoraviaLanguages", $values);
		}
		$result->close();
	}
	
	public function getMoraviaV1Language(int $idLanguage) {
		$values = array();
		$values["idLanguages"] = $idLanguage;
		$query = "SELECT l.idLanguage,l.languageName from Language l, MoraviaLanguages m WHERE m.idMoraviaLanguages = :IDLANGUAGE and l.idLanguage = m.idLanguage";
		
		$result = $this->connection->execute($query,$values);
		$language = null;
		if($result->fetch()){
			$language = new Language();
			$language->setIdLanguage($result->IDLANGUAGE);
			$language->setLanguageName($result->LANGUAGENAME);
		}
		
		$result->close();
		
		return $language;
	}
	
	public function searchLanguageForName(string $languageName) {
		$values = array();
		$values["NAME"] = $languageName;
		$query = 'SELECT * FROM Language WHERE languageName = :NAME';
		
		$result = $this->connection->execute($query,$values);
		$language = null;
		if ($result->fetch()){
			$language = new Language();
			
			$language->setIdLanguage($result->IDLANGUAGE);
			$language->setLanguageName($result->LANGUAGENAME);
		}
		
		$result->close();
		
		return $language;
	}


	
}
?>