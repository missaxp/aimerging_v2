<?php

use common\exceptions\AIException;
use dataAccess\dao\AIDAO;
use core\AI;
use service\EmailManager;
use PHPMailer\PHPMailer\PHPMailer;

require_once BaseDir . "/common/exceptions/AIException.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/PHPMailer/src/PHPMailer.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/PHPMailer/src/SMTP.php";
require_once BaseDir . '/common/EmailManager.php';

class Functions {
    public const WARNING = 'Warning';
    public const INFO = 'Info';
    public const SEVERE = 'Severe';
    public const ERROR = 'Error';

    /**
     * Generate standard format date of system
     *
     * @param string $date date with any format
     * @return string return date in format settled
     * @throws Exception
     */
    public static function createFormatDate(string $date,$zone = "UTC"){
        try{
            if($zone == 'UTC'){
                $date = new DateTime($date);
                return $date;
            }else{
                $format = new \DateTime($date." ".$zone);
                $format->setTimezone(new \DateTimeZone("UTC"));
                return $format;//->format("Y-m-d H:i:s");
            }

        }catch (Exception $e){
            Functions::addLog('DateError ['.$date.']: '.$e->getMessage(), Functions::WARNING,self::class);
            return new DateTime();
        }

    }

    public static function startsWith($haystack, $needle){
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    public static function endsWith($haystack, $needle){
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    /**
     * Generate currrent date in standard format of system
     *
     * @param string $zone
     * @param null $datetime
     * @return string current date
     */
    public static function currentDate($zone = "UTC",$datetime = null){
        $date = null;
        if ($zone != "UTC"){
            try{
                $format = new DateTime($datetime." ".$zone);
            } catch(Throwable $th){
                return date('Y/m/d H:i:s');
            }
            $format->setTimezone(new \DateTimeZone("UTC"));
            $date = $format->format("Y-m-d H:i:s");
        }else{
            $date = date('Y/m/d H:i:s');
        }

        return $date;
    }

    /**
     * Generate currrent date in standard format of system
     *
     * @param string $zone
     * @param null $datetime
     * @return string current date
     */
    public static function currentDateAsObject($zone = "UTC",$datetime = null){
        $date = null;
        if ($zone != "UTC"){
            try{
                $date = new DateTime($datetime." ".$zone);
            } catch(Throwable $th){
                $date = new DateTime($datetime);
            }
        }else{
            $date = new DateTime($datetime);
        }
        return $date;
    }




    /**
     * Save errors or information in log. It can save on database or file on disk
     *
     * @param string $information message to save on log
     * @param string $level level of error or information.
     * @param string $originClass class name where it arose.
     */
    public static function addLog(string $information, string $level, string $originClass){
        try {
            $devMode = AI::getInstance()->getSetup()->development;
            if($devMode){
                Functions::console("[". $level . "]" . $information);
            }
            self::logErrorToDB($information, $level, $originClass);
            if(AI::getInstance()->getSetup()->send_email_when_error){
                $em = new EmailManager();
                $level = "[" . strtoupper($level) . "]";
                $subject = (AI::getInstance()->isDev())? "[Development] - " : "";
                $subject .= "AI System $level :: on $originClass";
                $HTMLmail = "<html>\r\n";
                $HTMLmail .= "<head>\r\n";
                $HTMLmail .= "	<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\" />\r\n";
                $HTMLmail .= "	<title>AI System Error</title>\r\n";
                $HTMLmail .= "</head>\r\n";
                $HTMLmail .= "<body><br /><br />";
                $HTMLmail .= "<strong>Hora error:</strong> ".\Functions::currentDate()."<br /><br />";
                $HTMLmail .= $information."<br />";
                $HTMLmail .= "<b>Back Trace</b><br /><br />";
                $HTMLmail .= print_r(debug_backtrace(), true);


                $HTMLmail .= "</body>\r\n";

                $success = $em->sendMail(AI::getInstance()->getSetup()->mail_error_to, $subject, $HTMLmail);
            }
        }catch (Exception $e){
            self::writeLogToFS($level, $information, $originClass);
        }
    }

    public static function writeLogToFS(string $level, string $information, string $originClass){
        $path = FILEPATH.'log-'.date("Y-m-d").'.log';

        if(!file_exists($path)){
            $file = fopen($path,"w+");
            fwrite($file,'['.$level.']: ['.$originClass.'] ['.Functions::currentDate().'] '.$information);
            fclose($file);
        }else{
            $file = fopen($path, "a");
            if($file !== false){
                fwrite($file, "\r\n[".$level.']:  ['.$originClass.'] ['.Functions::currentDate().'] '.$information);
                fclose($file);
            }

        }
    }

    private static function sendEmail(string $information, string $level, string $originClass, AIException $exception){
        if(AI::getInstance()->getSetup()->send_email_when_error){
            $level = "[" . strtoupper($level) . "]";
            $em = new EmailManager();
            $subject = (AI::getInstance()->isDev())? "[Development] - " : "";
            $subject .= "AI System $level :: on $originClass";
            $HTMLmail = "<html>\r\n";
            $HTMLmail .= "<head>\r\n";
            $HTMLmail .= "	<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\" />\r\n";
            $HTMLmail .= "	<title>AI System $level</title>\r\n";
            $HTMLmail .= "</head>\r\n";
            $HTMLmail .= "<body>";
            $HTMLmail .= "<br><strong>Time the error ocurred:</strong> ". Functions::currentDate(AI::getInstance()->getSetup()->timeZone_TMS) ."<br />";
            $HTMLmail .= "<strong>Error message:</strong>" . $information."<br />";
            $HTMLmail .= "<strong>On namespace:</strong>" . $exception->getNamespace()."<br />";
            $HTMLmail .= "<strong>File:</strong>" . $exception->getFile()."<br />";
            $HTMLmail .= "<strong>Line:</strong>" . $exception->getLine()."<br />";
            $HTMLmail .= "<strong>Function:</strong>" . $exception->getFunctionName()."<br />";
            $HTMLmail .= "<b>Method params:</b><br />";
            $HTMLmail .= "<pre>";
            $HTMLmail .= print_r($exception->getFuntionParam(), true);
            $HTMLmail .= "</pre>";
            if($exception->getContextData() !== null){
                $HTMLmail .= "<b>Context data:</b><br />";
                $HTMLmail .= "<pre>";
                $HTMLmail .= print_r($exception->getContextData(), true);
                $HTMLmail .= "</pre>";
            }
            $HTMLmail .= "<b>Stack Trace</b><br />";
            $HTMLmail .= "<pre>";
            $HTMLmail .= print_r($exception->getTraceAsString(), true);
            $HTMLmail .= "</pre>";


            $HTMLmail .= "</body>\r\n";


            $success = $em->sendMail(AI::getInstance()->getSetup()->mail_error_to, $subject, $HTMLmail);
        }
    }

    /**
     * @param string $information
     * @param string $level
     * @param string $originClass
     * @param AIException $exception
     */
    public static function logException(string $information, string $level, string $originClass, AIException $exception){
        $devMode = false;
        try{
            $devMode = AI::getInstance()->getSetup()->development;
        } catch(Exception $e){
            Functions::print($e);
        }
        if($devMode){
            Functions::console("[". $level . "]" . $information);
            Functions::print(json_encode($exception));
        }
        self::logErrorToDB($information, $level, $originClass);
        self::sendEmail($information, $level, $originClass, $exception);
    }

    /**
     * Log the error directly to the database without send mail
     * @param String $level Use Functions::SEVERE etc
     * @param string $information Message to be saved
     * @param string $originClass Origin class
     */
    public static function logErrorToDB(string $information, string $level, string $originClass){
        if(AI::getInstance()->isDev()){
            Functions::console("Logging error... [$level] on [$originClass] " . $information );
        }
        $dao = new AIDAO();
        try {
            $dao->saveError($level, $information, $originClass);
        } catch(Throwable $e){
            self::writeLogToFS($level, $information, $originClass);
            self::sendEmail($e->getMessage(), Functions::ERROR, __CLASS__, AIException::createInstanceFromThrowable($e));
        }
    }

    /**
     * Output data.
     * @param string $msg
     * @param boolean $withTime (default true)
     * @return boolean
     * @return
     */
    public static function console($msg, $withTime = true){
        try{
            if(AI::getInstance()->getSetup()->fromHTTP){
                echo date("Y-m-d H:i:s")." - ".$msg."<br />";
            }
            else{
                print date("Y-m-d H:i:s")." - ".$msg."\r\n";
            }
        } catch(Exception $e){
            echo $e->getMessage() . "\n";
        }
        return true;
    }

    /**
     * Validate if a string is date.
     *
     * @param string $date date
     * @param array $formatAllowed array with accepted formats
     * @return boolean
     */
    public static function is_date($date, $formatAllowed=array()){
        $formats = (is_array($formatAllowed) AND !empty($formatAllowed))? $formatAllowed : array('Y-m-d', 'd-m-Y', 'Y/m/d', 'd/m/Y', 'Y-m-d H:i:s', 'd-m-Y H:i:s', 'Y/m/d H:i:s', 'd/m/Y H:i:s', 'd/m/Y H:i');
        foreach($formats as $format){
            $d = DateTime::createFromFormat($format, $date);
            if($d && $d->format($format) == $date){
                return true;
            }
        }
        return false;
    }

    /**
     * Print on screen with a readable format
     *
     * @param mixed variable to print
     */
    public static function print($data){
        print("<pre>".print_r($data, true)."</pre>");
    }

    public static function parseToBytes($string) {
        $size = 0;

        $array = explode("&nbsp;", $string);
        switch ($array[1]){
            case "MB":
                $size = 1048576 * ((float)$array[0]);
                break;
            case "kB":
                $size = 1024 * ((float)$array[0]);
                break;
        }
        return (int)$size;
    }

    public static function arrayToObject($array) {
        function array_to_obj($array, &$obj){
            foreach ($array as $key => $value){
                if (is_array($value)){
                    $obj->$key = new \stdClass();
                    array_to_obj($value,$obj->$key);
                }
                else{
                    if((strlen($value)==1 || (strlen($value)>1 && !startsWith($value, "0"))) && is_numeric($value)){ //If starts with 0 is NOT a number.
                        $value =  (int) $value;
                    }
                    if(is_string($value) && (strToUpper($value)=="TRUE" || strToUpper($value)=="FALSE")){
                        $value = (strToUpper($value)=="TRUE"? true:false);
                    }
                    $obj->$key = $value;
                }
            }
            return $obj;
        }
        $object = new \stdClass();
        return array_to_obj($array,$object);
    }

    public static function echo($msg){
        if(AI::getInstance()->getSetup()->fromHTTP){
            echo date("Y-m-d H:i:s")." - ".$msg."<br />";
        }
        else{
            print date("Y-m-d H:i:s")." - ".$msg."\r\n";
        }
    }
}
?>