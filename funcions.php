<?php

use model\Task;
use sources\webServices\Email;

/**
 * @uses Web_Desenvolupament Si és false no mostra el text de l'error i envia un email amb la descripció de l'error
 * @uses mail_notifica_error
 * @param Exception $exception 
 * @param boolean $sendEmail (optional) Indica si s'ha d'enviar l'email obligatoriament.
 * @param string $extramsg (optional) Indica un missatge addicional.
 * @param Task $task (optional) objecte tasca
 * @param boolean $stop (optional) Indica si finalitzarà l'execució
 */
function controlError($exception,$sendEmail=false,$extramsg = "",$task = null,$stop = false){
	
	if(!is_object($exception)){
		try{
			throw new Exception("Excepció REAL a la línia anterior: ".$exception);	
		}
		catch(Exception $e){
			echo "Exception no és objecte.";
			$exception = $e;
		}
	}
	
	showmsg($exception->getMessage().", extramsg: ".$extramsg); //Ho guardem a disc també
	
	// 	Neteja el buffer de resposta. 
	//	Si volem que es mostri el codi de la pàgina fins al moment s'hauria de deshabilitar
	/*if (ob_get_length()>0 && $stop) {
		ob_end_clean();
	}*/
	
	if (!defined("Web_Desenvolupament")) {
		define("Web_Desenvolupament", true);
	}
	
	$HTMLmail = "<html>";
	$HTMLmail .= "<head>";
	$HTMLmail .= "	<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\" />";
	$HTMLmail .= "	<title>Error processant Tasca</title>";
	$HTMLmail .= "</head>";
	$HTMLmail .= "<body><h1>Error processant Tasca</h1>";
	$HTMLmail .= "<br />";
	
	// Si està definida la variable Tasca la mostra
	if (isset($task) && is_object($task)) {
	}
	
	if (isset($extramsg) && $extramsg<>"") {
		$HTMLmail .= "<h2>Descripció de l'error</h2>";
		$HTMLmail .= "<p><strong>".$exception->getMessage().", <br />extramsg: ".$extramsg."</strong></p>";
		$HTMLmail .= "<br />";
		$HTMLmail .= "<hr />";
		$HTMLmail .= "<br />";
	}
	
	// Genera el missatge de resposta i canvia els \n per \n<BR \> de la traça
	$HTMLmail .= "<h2 style=\"color: #ff0000\">Informació de l'error</h2>";
	$HTMLmail .= "<p style=\"font-size: 0.9em; color: #ff0000\">".$exception->getMessage()." (".$exception->getFile();
	$HTMLmail .=" ->linia ".$exception->getLine().")<br/><br/>";
	$HTMLmail .=str_replace("\n","<br />\n",str_replace('/path/to/code/', '', $exception->getTraceAsString()))."</p>";
	$HTMLmail .= "<hr />";
	$HTMLmail .= "<br />";
	
	
	$HTMLmail .= "</body>";
	$HTMLmail .= "</html>";

	
	/*if (Web_Desenvolupament) {
		echo $HTMLmail;
	} */
	if(!Web_Desenvolupament || $sendEmail){
		$errmsg_envia = "";
		//Componer
		// Envia un email amb la descripció de l'error
		$mail = new Email();
		$mail->send("dTasks Error",$HTMLmail,array("correu" =>mail_notifica_error, "nom" => "INFORMATICA"));
	}
	if ($stop){
		echo "Stop true";
		die();
	}
}





function xmlToJSON($xmlstring) {
	$xml = simplexml_load_string($xmlstring);
	$json = json_encode($xml);
	return $json;
}

function strip_html_tags( $text )
{
    $text = preg_replace(
        array(
          // Remove invisible content
            '@<head[^>]*?>.*?</head>@siu',
            '@<style[^>]*?>.*?</style>@siu',
            '@<script[^>]*?.*?</script>@siu',
            '@<object[^>]*?.*?</object>@siu',
            '@<embed[^>]*?.*?</embed>@siu',
            '@<applet[^>]*?.*?</applet>@siu',
            '@<noframes[^>]*?.*?</noframes>@siu',
            '@<noscript[^>]*?.*?</noscript>@siu',
            '@<noembed[^>]*?.*?</noembed>@siu',
          // Add line breaks before and after blocks
            '@</?((address)|(blockquote)|(center)|(del))@iu',
            '@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
            '@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
            '@</?((table)|(th)|(td)|(caption))@iu',
            '@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
            '@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
            '@</?((frameset)|(frame)|(iframe))@iu',
        ),
        array(
            ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
            "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0",
            "\n\$0", "\n\$0",
        ),
        $text );
    return strip_tags( $text );
}

function getTS($oToken,$taskID){
	//Primer agafem el Task TimeStamp del WS
	$url = STR_SERVER."/Vendor/VendorTaskStatusTracking/TaskStatusTimestamp.".FORMAT_RESPOSTA."?taskId=".$taskID;
	
	$s = curl_init();
	
	curl_setopt($s,CURLOPT_URL,$url);
	//curl_setopt($s,CURLOPT_HEADER,true);
	curl_setopt($s,CURLOPT_USERAGENT,"iDISC Connector");
	curl_setopt($s,CURLOPT_HTTPHEADER,array('xciAuthenticationToken: '.$oToken));
	curl_setopt($s,CURLOPT_TIMEOUT,TIMEOUT);
	curl_setopt($s,CURLOPT_RETURNTRANSFER,true);
		
	try{
		//Enviem la consulta.
		//$httpRequest->send();
		$output = curl_exec($s);
	}
	catch(Exception $e){
		return array("estat" => "ERROR","missatge" => $e);
	}

	$ts = str_replace("\"","",$output);
	return $ts;
}

/**
 * Mostra un missatge
 * 
 * @param string $msg Missatge a mostrar
 */
function showmsg($msg){
	// Comprova si s'està executant des del navegador
	if (isset($_SERVER["HTTP_HOST"])) {
		echo "<xmp>".$msg."</xmp>\r\n";
		ob_flush();
		flush();
	} else {
		echo $msg.RETCHAR;
		try{
			$ruta = RUTA_LOG.SYSTEM_LOG;
			$fp = fopen($ruta,"a");
			fwrite($fp,$msg.RETCHAR);
			fclose($fp);
		}
		catch(Exception $e){
			try{
				$fp = fopen("C:\\dTasks\\error.log","a");
				fwrite($fp,$e->getMessage().RETCHAR);
				fclose($fp);	
			}
			catch(Exception $e2){
			}
		}
	}	
}

function checkJSON($json_decode,$original){
	//Comprovem que la resposta sigui en format JSON.
	$resposta = array();
	if(empty($json_decode)){
		//Primer intentem passar-ho a JSON perque he detetcat que encara que ho demanis en JSON, algunes coses les retorna en XML.
		$obj = @simplexml_load_string($original); // Parse XML
		$json_decode = json_decode(json_encode($obj), true); // Convert to array
		if(empty($json_decode)) //Si no es JSON ni XML llancem excepció
			$resposta["estat"] = "ERROR";
			$resposta["missatge"] = "El contingut no és JSON ni XML. : ".$json_decode;
			//throw new Exception("El contingut no és JSON ni XML");
			return $resposta;
	}
	//Comprovem que no s'hagi generat un error per part de Welocalize.
	foreach ($json_decode as $key => $value) {
		if($key == "exception"){ //Exception Handling for REST web services
			$resposta["estat"] = "ERROR";
			$resposta["missatge"] = "Exception: ".$json_decode["exception"]["exceptionMessage"];
			//throw new exception("Exception: ".$json_decode["exception"]["exceptionMessage"]);
			return $resposta;
			
		}
		if($key == "errors"){ //Validation Handling for REST web services
			//11/10/2013 -> Weloc no segueix la nomenclatura estandar que ells han normalitzat pel control d'errors. [0]. Pudé en un futur ho normalitzen.
			$resposta["estat"] = "ERROR";
			$resposta["missatge"] = "Validation: ".$json_decode["errors"][0]["errorCode"]." : ".$json_decode["errors"][0]["errorField"];
			//throw new Exception("Validation: ".$json_decode["errors"][0]["errorCode"]." : ".$json_decode["errors"][0]["errorField"]);
			return $resposta;
		}
	}
	$resposta["estat"] = "OK";
	$resposta["missatge"] = "";
	$resposta["json"] = $json_decode;
	return $resposta;
}


/**
 * Obté un string amb la data actual en el format d-m-Y
 * @return string
 */
function now() {
	return date("d-m-Y", time());
}


/**
 * Obté un string amb la data i hora actual en el format d-m-Y H:i:s
 * @return string
 */
function nowtime() {
	return date("d-m-Y H:i:s", time());
}

/*
 * Inserta un sysdate en una taula de la base de dades indicant la hora de la última execució correcte.
 */
function controlExecucio($dbcon){
	$sql = "SELECT ULTIMA_EXECUCIO FROM CONTROL_PRTG";
	$rs = $dbcon->Execute($sql);
	if($rs->fetch()){
		$sql = "UPDATE CONTROL_PRTG SET ULTIMA_EXECUCIO=sysdate";
	}
	else{
		$sql = "INSERT INTO CONTROL_PRTG VALUES(sysdate)";
	}
	$dbcon->Execute($sql);
}

/**
 * Elimina els tags HTML d'una cadena de text.
 * Modificació de la funció de PHP strip_tags per tal que no dongui problemes amb el símbol <
 * @param string $str Cadena d'entrada
 * @return string La cadena de text sense els tags HTML
 */
function eliminaTags($str) {
	// Elimina els scripts
	$str = strip_script($str);

	// Substitueix els < sense tancament per $lt;
	do {
		$count = 0;
		$str = preg_replace('/(<)([^>]*?<)/' , '&lt;$2' , $str , -1 , $count);
	} while ($count > 0);

	// Posa un espai rera els <br> i els </p> per tal que no ajunti paraules
	$str = preg_replace('(<br[^>]*>)', '$1'.chr(13).chr(10), $str);
	$str = preg_replace('(</p>)', '$1'.chr(13).chr(10), $str);

	// Elimina els tags html
	$str = strip_tags($str);
	// Substitueix els > que hagin quedat per $lt;
	$str = str_replace('>' , '&gt;' , $str);

	return $str;
}

/**
 * Elimina el codi entre dos tags script
 *
 * @param $string
 * @return string
 */
function strip_script($string) {
	// Prevent inline scripting and style
	$string = preg_replace('/<(style|script|noscript).*?<\/\1>/xmsi', '', $string);

	// Prevent linking to source files
	$string = preg_replace("/<script[^>]*>/i", "", $string);
	$string = preg_replace("/<style[^>]*>/i", "", $string);

	return $string;
}

function endsWith($haystack, $needle) {
	// search forward starting from end minus needle length characters
	return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
}


?>

