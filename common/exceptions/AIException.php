<?php


namespace common\exceptions;



use dataAccess\SQLException;
use Exception;
use JsonSerializable;
use Throwable;

class AIException extends Exception implements JsonSerializable{
    private $functionParams = "Not defined";
    public function getFuntionParam(){return $this->functionParams;}
    public function setFunctionParams(array $functionParams){$this->functionParams = $functionParams;}
    private $functionName = "Not defined";
    public function getFunctionName(){return $this->functionName;}
    public function setFunctionName(string $functionName){$this->functionName = $functionName;}
    private $namespace = "Not defined";
    public function getNamespace (){return $this->namespace;}
    public function setNamespace (string $namespace){$this->namespace = $namespace;}
    private $contextData = null;
    public function getContextData(){return $this->contextData;}
    public function setContextData(array $contextData){$this->contextData = $contextData;}

    public function __construct($message = "", $code = 0, Throwable $previous = null){
        parent::__construct($message, $code, $previous);
    }

    /**
     * @param string $functionName
     * @param string $namespace
     * @param array $functionParams
     * @param array|null $contextData
     * @return AIException This instance
     */
    public function construct(string $functionName, string $namespace, array $functionParams = array(), array $contextData = null){
        $this->functionName = $functionName;
        $this->namespace = $namespace;
        $this->functionParams = $functionParams;
        if(is_array($this->contextData) && $this->contextData !== null){
            if($contextData != null && is_array($contextData)){
                $this->contextData = array_merge($this->contextData, $contextData);
            } else{
                $this->contextData = ($contextData === null) ? $this->contextData :  $contextData ;
            }
        } else{
            $this->contextData =  $contextData ;
        }
        return $this;
    }

    public static function createInstanceFromThrowable(Throwable $t){
        $e = new self($t->getMessage(), $t->getCode(), $t);
        $e->line = $t->getLine();
        $e->file = $t->getFile();
        return $e;
    }

    /**
     * @param SQLException $exception
     * @return AIException
     */
    public static function createInstanceFromSQLException(SQLException $exception){
        $e = new self($exception->getMessage(), $exception->getCode(), $exception);
        $e->line = $exception->getLine();
        $e->file = $exception->getFile();
        $e->setContextData(array("connection_error" => $exception->getConnectionError()));
        return $e;
    }

    public static function createInstanceFromErrorHandler($errno, $errstr, $errfile, $errline){
        $e = new self($errstr, $errno);
        $e->line = $errline;
        $e->file = $errfile;
        $e->setContextData(array("error_type" => "Error catched on error handler"));
        return $e;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(){
        return get_object_vars($this);
    }
}