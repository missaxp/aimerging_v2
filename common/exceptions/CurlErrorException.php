<?php
namespace common\exceptions;

require_once BaseDir . "/common/exceptions/AIException.php";

class CurlErrorException extends AIException {
    /**
     * @param string $functionName
     * @param string $namespace
     * @param array $functionParams
     * @param array|null $contextData
     * @return CurlErrorException
     */
    public function construct(string $functionName, string $namespace, array $functionParams = array(), array $contextData = null)
    {
        parent::construct($functionName, $namespace, $functionParams, $contextData);
        return $this;
    }
}