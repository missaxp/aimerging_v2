<?php


namespace common\exceptions\sources\apple_liox;

use common\exceptions\AIException;
require_once BaseDir . "/common/exceptions/AIException.php";
/**
 * @author mhernandez
 * @version 1.0
 * created on 29 mar 2019
 */
class MailException extends AIException {
	const UNSUPPORTED_MAIL_TYPE_RE = "RE: Unsuported mail type";
	const UNSUPPORTED_MAIL_TYPE_FW = "FW: Unsuported mail type";
	const UNSUPPORTED_MAIL_TYPE_QA = "QA: Unsuported mail type";
    const UNSUPPORTED_MAIL_FORMAT = "QA: Unsuported mail type";
	const CANNOT_FIND_PROJECT_ID   = "Cannot find project ID";

}
?>