<?php
namespace common\exceptions\sources\apple_liox;
use common\exceptions\AIException;
require_once BaseDir . "/common/exceptions/AIException.php";
/**
* @author mhernandez
* @version 1.0
* created on 29 mar 2019
*/

class WorldServerException extends AIException {
	const CANNOT_DOWNLOAD_CSV              = "Error al descargar CSV";
	const CANNOT_DOWNLOAD_REFERENCE_FILES  = "Error al descargar archivos de referencia";
	const CANNOT_DOWNLOAD_SOURCE_FILES     = "Error al descargar archivos fuente";
	const CANNOT_ANALYZE_XLZ_JSON          = "Error al hacer petición por token para xlz";
	const CANNOT_ANALYZE_XLZ_IFRAME        = "Error al analizar respuesta iframe xlz";
	const CANNOT_ANALYZE_XLZ_POST          = "Error al realizar petición POST al iFrame";
	const CANNOT_ANALYZE_XLZ_DOWNLOAD_POST = "Imposible analizar respuesta post xlz";
	const CANNOT_DOWNLOAD_XLZ              = "Imposible descargar xlz";
	CONST CANNOT_OPEN_FILE_HANDLER         = "Imposible abrir el archivo";

	CONST PROJECT_ID_NOT_AVAILABLE         = "Proyecto no encontrado para la descarga";

    /**
     * @param string $functionName
     * @param string $namespace
     * @param array $functionParams
     * @param array|null $contextData
     * @return WorldServerException
     */
	public function construct(string $functionName, string $namespace, array $functionParams = array(), array $contextData = null)
    {
        parent::construct($functionName, $namespace, $functionParams, $contextData);
        return $this;
    }

}