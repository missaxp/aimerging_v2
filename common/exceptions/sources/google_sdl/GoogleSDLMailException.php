<?php


use common\exceptions\AIException;

class GoogleSDLMailException extends AIException {
    public function __construct($message = "", $code = 0, Throwable $previous = null){
        parent::__construct($message, $code, $previous);
    }
}