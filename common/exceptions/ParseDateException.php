<?php


namespace common\exceptions;

include_once BaseDir . "/common/exceptions/AIException.php";

class ParseDateException extends AIException{
    /**
     * @param string $functionName
     * @param string $namespace
     * @param array $functionParams
     * @param array|null $contextData
     * @return ParseDateException
     */
    public function construct(string $functionName, string $namespace, array $functionParams = array(), array $contextData = null)
    {
        parent::construct($functionName, $namespace, $functionParams, $contextData);
        return  $this;
    }
}