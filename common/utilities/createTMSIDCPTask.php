<?php
/**
*
* UTILITY: Create a Task into TMS IDCP.
* 
* You are able to set some vars to the specific value to create a task into the TMS.
* 
* param: USID. UniqueSource ID of a task.
* 
* @author phidalgo
* @version 1.0
* created on 7 jun. 2019
* 
* 
*/
use core\AI;
use dataAccess\dao\TaskDAO;
use behaviorModule\tms\idcp\IDCPtaskType as taskType;
use behaviorModule\tms\idcp\idcp;
use api\crud\Project;

if (isset($_SERVER["OS"])) {
	if (strpos(strtolower($_SERVER["OS"]), "windows")!==false) {
		$_SERVER["DOCUMENT_ROOT"] = dirname(__FILE__);
	}
}

if(!isset($_SERVER["DOCUMENT_ROOT"]) || $_SERVER["DOCUMENT_ROOT"]==""){
	$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__);
}


define ( "BaseDir", $_SERVER ["DOCUMENT_ROOT"] );


if(!isset($_GET["usid"]) || (isset($_GET["usid"]) && $_GET["usid"]=="")){
	die("Missing parameter USID");
}

$taskUSID = $_GET["usid"];

include_once BaseDir.'/declare.php';

$ai = AI::getInstance($setup);

include_once BaseDir.'/behaviorModule/tms/idcp.php';

$TMSDevelopment = false;
$IDCPProject = "217252";
$IDCPTaskType = taskType::_MIS;
$IDCPResource = "PHIDALGO";


//Execute function.
createTaskIntoTMSIDCP($taskUSID, $TMSDevelopment, $IDCPProject, $IDCPTaskType, $IDCPResource);

/**
 * Function to create a task by given some properties.
 * Prints some HTML data.
 * @param string $taskUSID
 * @param boolean $TMSDevelopment if is TMS dev or prod
 * @param string $IDCPProject Project
 * @param string $IDCPTaskType taskType
 * @param string $IDCPResource Resource idcp
 * @return boolean
 */
function createTaskIntoTMSIDCP($taskUSID, $TMSDevelopment, $IDCPProject, $IDCPTaskType, $IDCPResource){
	$taskDao = new TaskDAO();
	$foundTask = $taskDao->getTask($taskUSID);
	if($foundTask === null){
		echo "Task not found: $taskUSID<br />";
		return false;
	}
	
	$tmsInstance =  new idcp($foundTask, $TMSDevelopment);
	
	//Force 50 minutes on wc. Exampel to force some specific worcount.
	/*$wc = $foundTask->getTargetsLanguage()[0]->getAnalysis();
	$wc->setMinute(50);
	$wc->setPercentage_75(500);
	
	$foundTask->getTargetsLanguage()[0]->setAnalysis($wc);
	*/
	
	/**
	 * Set utility values.
	 * This is an example ones.
	 */
	foreach($tmsInstance->getTmsProperties() as &$tmsProperty){
		if($tmsProperty->name==="tipus_tasca"){
			$tmsProperty->setValue($IDCPTaskType);
		}
		
		if($tmsProperty->name==="projecte"){
			$tmsProperty->setValue($IDCPProject);
		}
		
		if($tmsProperty->name==="translator"){
			$tmsProperty->setValue($IDCPResource);
		}
	}
	
	//get due date returns $foundTask->getDueDate(): 2019-06-12 12:59:00
	
	$tmsInstance->createTaskIntoFS(false);
	
	$tmsInstance->setTitle($foundTask->getTitle()."_utility");
	$tmsInstance->setSourceLanguage($foundTask->getSourceLanguage());
	$tmsInstance->setTargetLanguages($foundTask->getTargetsLanguage());
	$tmsInstance->setDueDate($foundTask->getDueDate());
	
	$tmsInstanceCreateResponse = $tmsInstance->create();
	
	echo ($tmsInstanceCreateResponse? "SUCCESS: ".$tmsInstance->getTaskID():"FAIL");
	
	if(count($tmsInstance->getWarning())>0){
		echo "<br />Warnings:<br />";
		foreach ($tmsInstance->getWarning() as $msg){
			echo $msg."<br />";
		}
	}
	
	if(count($tmsInstance->getError())>0){
		echo "Warnings:<br />";
		foreach ($tmsInstance->getError() as $msg){
			echo $msg."<br />";
		}
	}
	
	return $tmsInstanceCreateResponse;
}






