<?php
use core\AI;
use dataAccess\ConnectionDB;

ini_set('display_errors', 'On');
error_reporting(E_ALL);

$_SERVER['DOCUMENT_ROOT'] = "/webs/ai";

define ( "BaseDir", $_SERVER ["DOCUMENT_ROOT"] );



include_once BaseDir . '/declare.php';
include_once BaseDir . '/model/Rule.php';

global $setup;
$ai = AI::getInstance($setup);

$dataBase = new ConnectionDB();
$dbconn = $dataBase->getConnection();

$sql = "select * from ExecutionLog where isExecuting=1 order by endExecution desc";
$q = $dbconn->execute($sql);?>
<strong>AI Executing? <?php echo ($q->fetch()? "YES":"NO");?></strong><br />

<?php
$q->close();

$sql = "select * from ExecutionLog where isExecuting=0 order by endExecution desc LIMIT 1";
?>
<br />
<br />
<?php 
$q = $dbconn->execute($sql);
if($q->fetch()){
	$dt = DateTime::createFromFormat("Y-m-d H:i:s", $q->getVal("startExecution"));
	$dt->setTimezone(new DateTimeZone("Europe/Madrid"));
	
	$ne = DateTime::createFromFormat("Y-m-d H:i:s", $q->getVal("endExecution"));
	$ne->setTimezone(new DateTimeZone("Europe/Madrid"));?>
	<strong>AI Last Execution Status:</strong><br />
	<table>
		<thead>
			<th>Start</th>
			<th>End</th>
			<th>Type</th>
			<th>Status</th>
		</thead>
		<tbody>
			<tr>
				<td><?php echo $dt->format("d/m/Y H:i:s");?></td>
				<td><?php echo $ne->format("d/m/Y H:i:s");?></td>
				<td><?php echo $q->getVal("executionType");?></td>
				<td><?php echo $q->getVal("executionStatus");?></td>
			</tr>
		</tbody>
	</table>
	<br />
	<br />
<?php }
$q->close();

$sql = "SELECT DATE_ADD(lastExecution,INTERVAL timeInterval MINUTE) as next_execute, idSource, sourceName, lastExecution, timeInterval FROM Source ORDER BY PRIORITY ASC";
$q = $dbconn->execute($sql);

?>



<table border="1">
	<thead>
		<th>Id Source</th>
		<th>Source Name</th>
		<th>Ultima Execució</th>
		<th>Propera Execució</th>
		<th>Interval</th>
		<th>Activat</th>
	</thead>
<?php 
while($q->fetch()){
	$lastExecutionTime = "-";
	$dt = DateTime::createFromFormat("Y-m-d H:i:s", $q->getVal("lastExecution"));
	if($dt!==false){
		$dt->setTimezone(new DateTimeZone("Europe/Madrid"));
		$lastExecutionTime = $dt->format("d/m/Y H:i:s");
	}
	
	
	$nextExecution = "-";
	$ne = DateTime::createFromFormat("Y-m-d H:i:s", $q->getVal("next_execute"));
	if($ne!==false){
		$ne->setTimezone(new DateTimeZone("Europe/Madrid"));
		$nextExecution = $ne->format("d/m/Y H:i:s");
	}
	
	
	$cd = new DateTime();
	
	
	
	
	if($ne<$cd){
		$nextExecution = "<span style=\"color:green;\"><strong>On Next AI Execution</strong></span>";
	}
	
?>
	<tr>
		<td><?php echo $q->getVal("idSource");?></td>
		<td><?php echo $q->getVal("sourceName");?></td>
		<td><?php echo $lastExecutionTime;?></td>
		<td><?php echo $nextExecution;?></td>
		<td><?php echo $q->getVal("timeInterval");?></td>
		<td><?php echo ((int) $q->getVal("timeInterval")>=0? "<span style=\"color:green;\">ON</span>":"<span style=\"color:red;\">OFF</span>");?>
	</tr>
<?php }
$q->close();
?>
</table>