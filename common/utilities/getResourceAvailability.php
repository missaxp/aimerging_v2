<?php

/**
* @author phidalgo
* @version 1.0
* created on 18 jul. 2019
*/
use core\AI;
use dataAccess\dbConn;
use behaviorModule\tms\idcp\resources\resourceAvailability;



require $_SERVER["DOCUMENT_ROOT"].'/dataAccess/dbConn.php';

if (isset($_SERVER["OS"])) {
	if (strpos(strtolower($_SERVER["OS"]), "windows")!==false) {
		$_SERVER["DOCUMENT_ROOT"] = dirname(__FILE__);
	}
}

if(!isset($_SERVER["DOCUMENT_ROOT"]) || $_SERVER["DOCUMENT_ROOT"]==""){
	$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__);
}


define ( "BaseDir", $_SERVER ["DOCUMENT_ROOT"] );


include_once BaseDir.'/declare.php';

$ai = AI::getInstance($setup);


$dbcon = new dbConn (DBCONN_ORACLE,
		"WEBTRADUC", "WEBTRADUC",
		"(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = oxigen.iit.idisc.es)(PORT = 1521)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = GESTIO.IDISC.ES)))");


require BaseDir."/behaviorModule/tms/idcp/resourceAvailability.php";


if(!isset($_GET["rid"]) || (isset($_GET["rid"]) && $_GET["rid"]=="")){
	die("Missing Parameter rid (Translator IDCP ID");
}

$TMSResourceId = $_GET["rid"];


$timeFrame = "P7D";
if(isset($_GET["timeFrame"]) && is_numeric($_GET["timeFrame"])){
	$timeFrame = "P".$_GET["timeFrame"]."D";
}

$resourceAvailability = new resourceAvailability($dbcon, $TMSResourceId, "");


$rsp = $resourceAvailability->getTimeFrame(new \DateTime('today midnight'), new \DateInterval($timeFrame));
?>
<table border="1">
	<thead>
		<tr>
			<th colspan="3"><?php echo $TMSResourceId?> Availability</th>
		</tr>
		<tr>
			<th>Day</th>
			<th>Assigned Hours</th>
			<th>Tasks</th>
		</tr>
<?php foreach($rsp as $r){?>
	<tr <?php echo ($r->isTodayWeekend()? " style=\"color:orange;\"":"");?>>
		<td><?php echo $r->day->format("Y-m-d");?></td>
		<td><?php echo $r->assignedWorkHours?> hours </td>
		<td><?php echo $r->taskCounter;?> tasks</td>
	</tr>
<?php }?>
</table>