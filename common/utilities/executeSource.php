<?php 
/**
*
* UTILITY: Executes a source.
*
* source name (sname) GET param is mandatory.
* 
* @author phidalgo
* @version 1.0
* created on 7 jun. 2019
*
*
*/
use core\AI;
use dataAccess\dao\TaskDAO;
use behaviorModule\tms\idcp\IDCPtaskType as taskType;
use behaviorModule\tms\idcp\idcp;
use api\crud\Project;
use dataAccess\dao\SourceDAO;
use harvestModule\Harvest;
use preprocessorModule\Preprocessor;
use behaviorModule\Behavior;



if (isset($_SERVER["OS"])) {
	if (strpos(strtolower($_SERVER["OS"]), "windows")!==false) {
		$_SERVER["DOCUMENT_ROOT"] = dirname(__FILE__);
	}
}

if(!isset($_SERVER["DOCUMENT_ROOT"]) || $_SERVER["DOCUMENT_ROOT"]==""){
	$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__);
}


define ( "BaseDir", $_SERVER ["DOCUMENT_ROOT"] );


if(!isset($_GET["sname"]) || (isset($_GET["sname"]) && $_GET["sname"]=="")){
	die("Missing parameter Source Name");
}

$sourceName = $_GET["sname"];

include_once BaseDir.'/declare.php';

define("FILEPATH", $setup->repository);


$ai = AI::getInstance($setup);
$ai->init();

$sourceDAO = new SourceDAO();

$source = $sourceDAO->getSourceByName($sourceName);

$instanceOfSource = Harvest::createObjectSource($source, $sourceDAO->getUsersSources($source->getIdSource()));

$instanceOfSource->collectTask();

$sourceTasks = $instanceOfSource->getTasks();

Functions::console("UTILITY: New Collected tasks: ".count($sourceTasks));

$preprocessorModule = new Preprocessor();
$preprocessedTasks = $preprocessorModule->preprocessTasks($sourceTasks);

//$behaviorModule  = new Behavior();

Functions::console("UTILITY: New Preprocessed tasks: ".count($preprocessedTasks));

Functions::console("END EXECUTING SOURCE: ".$sourceName);
?>