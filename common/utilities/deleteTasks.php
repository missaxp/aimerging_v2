<?php 
/**
*
* UTILITY: Delete task
*
*
* @author phidalgo
* @version 1.0
* created on 7 jun. 2019
*
*
*/
use core\AI;
use dataAccess\dao\TaskDAO;
use behaviorModule\tms\idcp\IDCPtaskType as taskType;
use behaviorModule\tms\idcp\idcp;
use api\crud\Project;
use dataAccess\dao\SourceDAO;
use harvestModule\Harvest;
use preprocessorModule\Preprocessor;
use behaviorModule\Behavior;
use dataAccess\ConnectionDB;


if(!isset($_GET["usids"]) || (isset($_GET["usids"]) && $_GET["usids"]=="")){
	die("Missing parameter USIDS");
	
}

$usids = explode(",",$_GET["usids"]);
if(count($usids)===0){
	die("Usids can not be null or empty");
}

//Basic filter.
$filteredUsids = array();

foreach($usids as $usid){
	if(strlen($usid)==32){
		$filteredUsids[] = $usid;
	}
	else{
		echo $usid." rejected<br />";
	}
}


if (isset($_SERVER["OS"])) {
	if (strpos(strtolower($_SERVER["OS"]), "windows")!==false) {
		$_SERVER["DOCUMENT_ROOT"] = dirname(__FILE__);
	}
}

if(!isset($_SERVER["DOCUMENT_ROOT"]) || $_SERVER["DOCUMENT_ROOT"]==""){
	$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__);
}


define ( "BaseDir", $_SERVER ["DOCUMENT_ROOT"] );


include_once BaseDir.'/declare.php';

$ai = AI::getInstance($setup);

define("FILEPATH", $setup->repository);



foreach($filteredUsids as $usid){
	deleteTask($usid);
}


function deleteTask($usid){
	$dataBase = new ConnectionDB();
	$dbconn = $dataBase->getConnection();
	
	$dbconn->setDefaultExecMode(DBCONN_TRANSACTION);
	
	$found = false;
	
	$sql = "SELECT uniqueSourceId From Task where uniqueSourceId=:USID";
	$q = $dbconn->execute($sql, array("USID" => $usid));
	$found =($q->fetch());
	$q->close();
	
	if(!$found){
		echo "Task not found: $usid<br />";
		return false;
	}
	
	//Eliminem Target Langs
	$sql = "DELETE FROM TaskTargetLanguages where uniqueSourceId=:USID";
	$dbconn->execute($sql, array("USID" => $usid));
	
	
	
	//Eliminem WordCounts dels target languages.
	$sqlWC = "(SELECT idWordAnalysis FROM TaskTargetLanguages where uniqueSourceId=:USID)";
	$sql = "DELETE FROM TaskWordCounts where idWordAnalysis in $sqlWC";
	$dbconn->execute($sql, array("USID" => $usid));
	
	
	//TODO:: faltaria buscar asynchash delsTask files i eliminarlos tb.
	$sql = "DELETE FROM TaskFilesPO where uniqueSourceId=:USID";
	$dbconn->execute($sql, array("USID" => $usid));
	
	//Elimimem TaskFilesToTranslate
	$sql = "DELETE FROM TaskFilesToTranslate where uniqueSourceId=:USID";
	$dbconn->execute($sql, array("USID" => $usid));
	
	//Elimimem TaskFilesForReference
	$sql = "DELETE FROM TaskFilesForReference where uniqueSourceId=:USID";
	$dbconn->execute($sql, array("USID" => $usid));
	
	//Elimimem TaskFilesTM
	$sql = "DELETE FROM TaskFilesTM where uniqueSourceId=:USID";
	$dbconn->execute($sql, array("USID" => $usid));
	
	//Elimimem Task Files dels Source
	$sqlSF = "(select idFile from TaskFilesToTranslate where uniqueSourceId=:USID)";
	$sql = "DELETE FROM TaskFiles WHERE idFile in $sqlSF";
	$dbconn->execute($sql, array("USID" => $usid));
	
	//Elimimem Task Files dels Ref
	$sqlRF = "(select idFile from TaskFilesForReference where uniqueSourceId=:USID)";
	$sql = "DELETE FROM TaskFiles WHERE idFile in $sqlRF";
	$dbconn->execute($sql, array("USID" => $usid));
	
	//Elimimem Task Files dels TM
	$sqlTMF = "(select idFile from TaskFilesToTranslate where uniqueSourceId=:USID)";
	$sql = "DELETE FROM TaskFiles WHERE idFile in $sqlTMF";
	$dbconn->execute($sql, array("USID" => $usid));
	
	
	//Delete task change history
	$sql = "DELETE FROM TaskChangeHistory where uniqueSourceId=:USID";
	$dbconn->execute($sql, array("USID" => $usid));
	
	//Delete TaskAditionalProperties
	$sql = "DELETE FROM TaskAditionalProperties where uniqueSourceId=:USID";
	$dbconn->execute($sql, array("USID" => $usid));
	
	//Delete TaskAditionalProperties
	$sql = "DELETE FROM TaskSource where uniqueSourceId=:USID";
	$dbconn->execute($sql, array("USID" => $usid));
	
	$sql = "DELETE FROM ActionCreateTMSResponse where uniqueSourceId=:USID";
	$dbconn->execute($sql, array("USID" => $usid));

    $sql = "DELETE FROM ConfirmationToken where uniqueSourceId=:USID";
    $dbconn->execute($sql, array("USID" => $usid));

    $sql = "DELETE FROM ActionConfirmEmailFeedback where uniqueSourceId=:USID";
    $dbconn->execute($sql, array("USID" => $usid));

    $sql = "DELETE FROM TaskErrors where uniqueSourceId=:USID";
    $dbconn->execute($sql, array("USID" => $usid));


    //Eliminem de Task
	$sql = "DELETE FROM Task where uniqueSourceId=:USID";
	$dbconn->execute($sql, array("USID" => $usid));
	
	$dbconn->commit();
	
	echo "DELETED: $usid<br />";
}



?>