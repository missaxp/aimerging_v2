<?php

/**
* @author phidalgo
* @version 1.0
* created on 18 jul. 2019
*/
use core\AI;
use dataAccess\dbConn;
use behaviorModule\tms\idcp\resources\resourceAvailability;



require $_SERVER["DOCUMENT_ROOT"].'/dataAccess/dbConn.php';

if (isset($_SERVER["OS"])) {
	if (strpos(strtolower($_SERVER["OS"]), "windows")!==false) {
		$_SERVER["DOCUMENT_ROOT"] = dirname(__FILE__);
	}
}

if(!isset($_SERVER["DOCUMENT_ROOT"]) || $_SERVER["DOCUMENT_ROOT"]==""){
	$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__);
}


define ( "BaseDir", $_SERVER ["DOCUMENT_ROOT"] );


include_once BaseDir.'/declare.php';

$ai = AI::getInstance($setup);


$dbcon = new dbConn (DBCONN_ORACLE,
		"WEBTRADUC", "WEBTRADUC",
		"(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = oxigen.iit.idisc.es)(PORT = 1521)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = GESTIO.IDISC.ES)))");


require BaseDir."/behaviorModule/tms/idcp/resourceAvailability.php";


if(!isset($_GET["rid"]) || (isset($_GET["rid"]) && $_GET["rid"]=="")){
	die("Missing Parameter rid (Translator IDCP ID");
}

$TMSResourceId = $_GET["rid"];



$taskForecast = 0;
if(isset($_GET["forecast"])){
	$taskForecast = floatval(str_replace(',', '.', str_replace('.', '', $_GET["forecast"])));
}

$resourceAvailability = new resourceAvailability($dbcon, $TMSResourceId, "");


$bestDeadline = $resourceAvailability->getBestDeadline($taskForecast, new \DateTime('today midnight'));

echo "Best deadline: ".$bestDeadline->format("Ymd H:i:s")."<br />";
?>
