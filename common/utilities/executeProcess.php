<?php
/**
 *
 * UTILITY: Pasa una tarea por un proceso en concreto.
 *
 * Parametros
 * process id (pid)
 * task id (usid)
 *
 * @author phidalgo
 * @version 1.0
 * created on 17 jun. 2019
 *
 *
 */
use core\AI;
use dataAccess\dao\TaskDAO;
use behaviorModule\Behavior;
use dataAccess\dao\ProcessDAO;
use model\State;
use model\Schedule;
use model\Action;
use dataAccess\ConnectionDB;
use common\exceptions\AIException;



if (isset($_SERVER["OS"])) {
	if (strpos(strtolower($_SERVER["OS"]), "windows")!==false) {
		$_SERVER["DOCUMENT_ROOT"] = dirname(__FILE__);
	}
}

if(!isset($_SERVER["DOCUMENT_ROOT"]) || $_SERVER["DOCUMENT_ROOT"]==""){
	$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__);
}


define ( "BaseDir", $_SERVER ["DOCUMENT_ROOT"] );


if(!isset($_GET["pid"]) || (isset($_GET["pid"]) && $_GET["pid"]=="")){
	die("Missing parameter Process id");
}

if(!isset($_GET["usid"]) || (isset($_GET["usid"]) && $_GET["usid"]=="")){
	die("Missing parameter usid");
}

$processId = $_GET["pid"];

$taskId = $_GET["usid"];



include_once BaseDir.'/declare.php';

define("FILEPATH", $setup->repository);


$ai = AI::getInstance($setup);
$ai->init();

$taskDao = new TaskDAO();
$task = $taskDao->getTask($taskId);
if($task === null){
	Functions::console("task not found");
	return null;
}


$sql = "delete from ActionCreateTMSResponse where uniqueSourceId=:USID";
$dataBase = new ConnectionDB();
$dbconn = $dataBase->getConnection();

$dbconn->setDefaultExecMode(DBCONN_TRANSACTION);

$dbconn->execute($sql, array("USID" => $taskId));
$dbconn->commit();


$pDao = new ProcessDAO();

$process = $pDao->getProcess($processId);
		\Functions::console("UTILITY:: Pasar tarea ".$task->getUniqueSourceId()." Por proceso: ".$process->getProcessName()." - ".$process->getIdProcess());
		$scheduleSetup = ($process->getSchedules() != null)?$process->getSchedules()->getProcessType():Schedule::_ONDEMAND;
		
		//TODO:: Siempre devuelve true.
		$scheduleProcessTask = ($scheduleSetup != null && ($scheduleSetup==Schedule::_ONDEMAND || ($scheduleSetup==Schedule::_INQUEUE) && $process->getSchedules()->checkIfHasProcessQueue()));
		
		$memoqErrors = null;
		
		if($scheduleProcessTask){
			if (! empty($process->getActions(true))){
				\Functions::console("UTILITY: Call to TaskDao::updateTaskState=".State::EXECUTINGACTIONS." to perform change into DB -".Behavior::EXECUTINGACTIONS. ' by process '. $process->getIdProcess());
				$state = new State(State::EXECUTINGACTIONS);
				$task->setState($state);
			}else{
				\Functions::console("UTILITY: Call to TaskDao::updateTaskState=".State::RETRY." to perform change into DB - Process " . $process->getProcessName() . " have not any Action");
				$state = new State(State::RETRY);
				$task->setState($state);
			}
			/**@var \model\Process $result*/
			if($process->getIdCATTool() !== null){
				\Functions::console("UTILITY::Processing task=> ".$task->getUniqueSourceId()." with memoq");
				$memoq = $process->getCATToolSetup();
				$memoq->executeMemoqActions($task);
				$memoqErrors = $memoq->getErrors();
			}
			if (! empty($process->getActions(true))) {
				
				foreach($process->getActions(true) as &$action) {
					
					if ($action->executeAction($task)) {
						$actionStatus = Behavior::processMatchedTask($action, $task);
						\Functions::console("UTILITY: Call to TaskDao::updateTaskState=".State::EXECUTINGACTIONS." to perform change into DB - ".$action->getActionName());
						if ($actionStatus == Action::_ACTION_PENDING_CONFIRM || $actionStatus == Action::_ACTION_WITH_ERRORS) {
							break;
						}
					} else {
						/* The action failed and onfallBack is false, so the process has to stop and the action must
						 * be executed again in the next iteration*/
						$actionStatus = Action::_ACTION_FAIL;
						if (!$action->getOnFallBack()) {
							\Functions::console("UTILITY: Call to TaskDao::updateTaskState=".State::RETRY." to perform change into DB - Fail action => ".$action->getActionName());
							\Functions::console("UTILITY Errors: ".implode("<br />",$action->getError()));
							\Functions::console("UTILITY Warnings: ".implode("<br />",$action->getWarning()));
							$state = new State(State::RETRY);
							$task->setState($state);
							$actionStatus = null;
							break;
						}else{
							/* The action failed but onFallBack is true, so the process must continue
							 * and the action should not be executed again so we update the task state*/
							$actionStatus = Behavior::processMatchedTask($action, $task);
							\Functions::console("UTILITY: Call to TaskDao::updateTaskState=".State::EXECUTINGACTIONS." to perform change into DB - ActionName: ".$action->getActionName().", action id: ".$action->getIdAction());
						}
					}
				}
				
				
			}else{
				\Functions::console("UTILITY: Call to TaskDao::updateTaskState=".State::PROCESSED." to perform change into DB -  but it did not take any action");
				$state = new State(State::PROCESSED);
				$task->setState($state);
				$actionStatus = Action::_ACTION_SUCCEDS;
			}
		}
		
		
		
		
		if(count($process->getActionsErrors()) == 0 && count($process->getActionsWarnings()) == 0 && $actionStatus != Action::_ACTION_FAIL){
			\Functions::console("UTILITY: Call to TaskDao::updateTaskState=".State::PROCESSED." to perform change into DB");
			$state = new State(State::PROCESSED);
			$task->setState($state);
		}
		
		if($actionStatus != null && $scheduleProcessTask){
			$process->processNotification($task, $memoqErrors);
		}