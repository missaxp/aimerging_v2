<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL);

$_SERVER['DOCUMENT_ROOT'] = "/webs/ai";

define ( "BaseDir", $_SERVER ["DOCUMENT_ROOT"] );


use core\AI;
use dataAccess\dao\ProcessDAO;
use dataAccess\dao\TaskDAO;
use model\Language;
use model\Process;
use model\Rule;
use model\Task;
use model\TaskAditionalProperties;


include_once BaseDir . '/declare.php';
include_once BaseDir . '/model/Rule.php';

global $setup;
$ai = AI::getInstance($setup);
$ai->init();

function printRule(Rule $rule){
    return ("<b>" . $rule->getDescription() . ":</b> " . $rule->getProperty() . " - ". $rule->getOperator() . " - " . $rule->getValue());
}
function printProcess(Process $process){
    $string = "";
    $string .= "<b>Checking " . $process->getProcessName() . "...</b>";
    return ($string);
}
function printAdditionalProperty(TaskAditionalProperties $aditionalProperties){
    return $aditionalProperties->getPropertyName() . " - " . $aditionalProperties->getPropertyValue();
}
function printLanguage(Language $lang){
    return $lang->getLanguageName();
}
function printTask(Task $task){
    $string = "";
    $string .= "<b>Title:</b> " . $task->getTitle() . "<br>";
    $string .= "<b>idClientTask:</b> " . $task->getIdClientetask() . "<br>";
    $string .= "<b>Due date:</b> " . $task->getDueDate()->format("Ymd H:i:s") . "<br>";
    $string .= "<b>Source language:</b> " . printLanguage($task->getSourceLanguage()) . "<br>";
    $string .= "<b>Target languages: </b>". "<br>";
    foreach ($task->getTargetsLanguage() as $prop){
        $string .= " - " . printLanguage($prop) . "<br>";
        $string .= " - - Total words:" . $prop->getAnalysis()->getTotalWords() . "<br>";
    }
    $string .= "<b>Additional Properties: </b>". "<br>";
    foreach ($task->getAditionalProperties() as $prop){
        $string .= " - " . printAdditionalProperty($prop) . "<br>";
    }

    echo $string;
}

function getMatchedProcess ($taskUSI){
    $taskDao = new TaskDAO();
    $foundTask = $taskDao->getTask($taskUSI);
    if($foundTask === null){
        Functions::console("task not found");
        return null;
    }
    printTask($foundTask);
    $processDao = new ProcessDAO();
    $processes = array();
    if($foundTask->getDataSource() !== null){
        $processes = $processDao->getProcessesBySource($foundTask->getDataSource()->getIdSource());
    }else{
        Functions::console("Task doesn't contains datasource");
        return null;
    }


    foreach($processes as $process) {
        echo "<br>";
        Functions::console(printProcess($process));
        $noMatchedRules = array();
        $rules = $process->getRules();
        $totalWeight = 0;
        foreach($rules as $rule) {
            Functions::console("Verifing " . printRule($rule));
            $check = $rule->check($foundTask);
            if (! $check) {
                Functions::console("<b>X</b> - Not passed");
                $totalWeight += $rule->getWeight();
            } else {
                Functions::console("<b>S</b> - Passed");
            }
        }
        if ($totalWeight < 1) {
            return $process;
        }
    }
    return null;
}
if (isset($_GET['usid']) || (isset($argv) && isset($argv[1]))) {
    $usid = (isset($_GET['usid'])) ? $_GET['usid']: $argv[1];
    Functions::console("Searching:" . $usid);
    $processToSearch = getMatchedProcess($usid);
    if($processToSearch === null){
        echo "<br>";
        Functions::console("<b>Process not found</b>");
    } else {
        echo "<br>";
        Functions::console("Process found: " . $processToSearch->getProcessName());
        Functions::console("Rules:");
        foreach ($processToSearch->getRules() as $rule){
            Functions::console(printRule($rule));
        }
    }
} else {
    Functions::console("USID parameter not found. Specify uniqueSourceId in URL or by console to continue");
}

