<?php

namespace common;

use core\AI;


/*
define ( "BaseDir", "/webs/ai" );
require BaseDir.'/declare.php';


global $setup;

AI::getInstance($setup);

define("FILEPATH", $setup->repository);


$dm = new downloadManager();
$rsp = $dm->setFileUrl("http://idcp.idisc.es/files/prova.zip")
->setAsync(true)
->setFileName("prova.iso")
->setDownloadPath("/testAsync/")
->addHeader(array(
		'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0',
		'Accept-Encoding: gzip, deflate, br',
		'Cookie: autoLogin=al; JSESSIONID=JSI;'
))
->addBody(array(
		'jobIdFilter' => '',
		'jobNameFilter' => '',
		'activityNameFilter' => '',
		'languageSet' => "TL",
		'assigneesNameFilter' => ''
))
->run();


die(var_dump($rsp));
*/

/**
* Download Manager class.
* Can call an internal webservice to make a async download or just use the curl to download the file.
* @author phidalgo
* @version 1.0
* created on 30 ene. 2019
 */
class downloadManager {
// 	private const _ASYNC_DOWNLOAD_API_URL_DEVELOMENT = "http://ai.linux/api/v1/core/helper/downloadFile";
	private const _ASYNC_DOWNLOAD_API_URL = "http://ai.linux/api/v1/core/helper/downloadFile";
	
	private const _ASYNC_DOWNLOAD_API_TOKEN = "AAAAAABBB";

	private $headers = array();
	
	private $body = array();
	
	private $async = null;

	private $fileUrl = null;
	
	private $downloadPath = null;
	
	private $fileMethod = null;
	
	private $fileName = null;

	private $syncCurlOptions = array();

	public function __construct($fileUrl = "", $fileName = "", $fileMethod = "GET", $async = false){
		$this->async = $async;
		$this->fileMethod = "GET";
		$this->setMethod($fileMethod);
		$this->fileUrl = $fileUrl;
		
	}

	public function setSyncCurlOptions($curlOptions){
	    $this->syncCurlOptions = $curlOptions;
	    return $this;
    }

	/**
	 * Determines if async or sync download
	 * @param bool $async
	 * @return downloadManager
	 */
	public function setAsync($async){
		$this->async = $async;
		return $this;
	}

	/**
	 * File download URL.
	 * @param string $fileUrl
	 * @return downloadManager
	 * 
	 */
	public function setFileUrl($fileUrl){
		$this->fileUrl = $fileUrl;
		return $this;
	}
	
	/**
	 * Set download path
	 * @param string $downloadPath
	 * @return downloadManager
	 */
	public function setDownloadPath($downloadPath){
		$this->downloadPath = $downloadPath;
		return $this;
	}
	
	/**
	 * Download file Name
	 * @param string $fileName
	 * @return downloadManager
	 */
	public function setFileName($fileName){
		$this->fileName = $fileName;
		return $this;
	}
	
	/**
	 * 
	 * @param array||string $header
	 * @return downloadManager
	 */
	public function addHeader($header){
		if(is_array($header)){
			$this->headers = $header;
		}
		else{
			$this->headers[] = $header;
		}
		return $this;
	}
	
	/**
	 * Adds body params
	 * @param array||string $body
	 * @return downloadManager
	 * @return
	 */
	public function addBody($body){
		if(is_array($body)){
			$this->body = $body;
		}
		else{
			$this->body[] = $body;
		}
		return $this;
		
	}
	
	/**
	 * Sets request method (GET, POST)
	 * @param string $method
	 * @return downloadManager
	 * @return
	 */
	public function setMethod($method){
		$method = strtoupper($method);
		$allowedMethods = array("GET", "POST");
		if(in_array($method, $allowedMethods)){
			$this->fileMethod = $method;
		}
		return $this;
	}
	
	/**
	 * Run download.
	 * @return boolean
	 * @return
	 */
	public function run(){
		return ($this->async? $this->asyncDownload():$this->syncDownload());
	}

	public function addCurlCustom($curlOpt, $curlVal){

    }

	/**
	 * Requests to internal HTTP REST API to download a file.
	 * If request data are ok, the response is 202 accepted and a asyncHash to track the file. 
	 * @return array
	 */
	private function asyncDownload() {
		$handler = curl_init();
		
		curl_setopt($handler, CURLOPT_URL, AI::getInstance()->getSetup()->urlDownloadManager);
		curl_setopt($handler, CURLOPT_POST, true);
		
		//We generate a sha1 hash with a private token inside the hashed array.
		//On the backend, we construct again the received array, adding the privat key and the generate the sha1 again.
		//if two hashes are the same, we trust the data.
		$body = array(
				"params" 		=> $this->body,
				"url" 			=> $this->fileUrl,
				"method" 		=> $this->fileMethod,
				"headers" 		=> $this->headers,
				"fileName" 		=> $this->fileName,
				"downloadPath" 	=> $this->downloadPath,
				"cryptToken"	=> AI::getInstance()->getSetup()->apiPrivateToken,
		);
		
		$authHash = sha1(http_build_query($body));
		
		unset($body["cryptToken"]);
		
		curl_setopt($handler, CURLOPT_HTTPHEADER, array("Authorization: ".self::_ASYNC_DOWNLOAD_API_TOKEN));
		curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($handler, CURLOPT_FORBID_REUSE, true);
		curl_setopt($handler, CURLOPT_CONNECTTIMEOUT, 1);
		curl_setopt($handler, CURLOPT_DNS_CACHE_TIMEOUT, 10);
		curl_setopt($handler, CURLOPT_USERAGENT, 'AISYSTEM');
		
		curl_setopt($handler, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($handler, CURLOPT_POSTFIELDS, http_build_query($body)."&authHash=$authHash");
		
		
		
		$response = curl_exec($handler);
		$httpCode = curl_getinfo($handler, CURLINFO_HTTP_CODE);

		
		
		return json_decode($response, true);
	}

	/**
	 * Sync download the file.
	 * @return boolean
	 */
	private function syncDownload(){
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->fileMethod);
		curl_setopt($ch, CURLOPT_URL, $this->fileUrl);

		foreach($this->syncCurlOptions as $option){
		    if(isset($option[0]) && isset($option[1]))
                curl_setopt($ch, $option[0], $option[1]);
        }
		
		
		if(count($this->headers)>0){
			curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
		}
		
		if($this->fileMethod!="GET" && count($this->body)>0){
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->body));
		}
		
		
		$filePath = AI::getInstance()->getSetup()->repository.$this->downloadPath;
		if (! file_exists($filePath)) {
			@mkdir($filePath, 0760, true);
		}
		
		$newFile = fopen($filePath.$this->fileName, "w+");
		curl_setopt($ch, CURLOPT_FILE, $newFile);
		curl_exec($ch);
		fclose($newFile);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);

		return ($httpCode==200);
	}
	
}
  
  ?>