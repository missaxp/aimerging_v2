<?php

namespace service;

use Exception;
use core\AI;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception as mail__error;

require_once BaseDir . "/PHPMailer/src/PHPMailer.php";
require_once BaseDir . "/PHPMailer/src/SMTP.php";
require_once BaseDir . "/PHPMailer/src/Exception.php";

class EmailManager {
	private $emailManager;
	const CHARSET = "UTF-8";

	public function __construct(){
		$this->emailManager = new PHPMailer(true);
        $this->emailManager->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true,
                'peer_name' => AI::getInstance()->getSetup()->mail_from, AI::getInstance()->getSetup()->mail_from_name)
        );  
		$this->emailManager->IsSMTP();
        $this->emailManager->Host = AI::getInstance()->getSetup()->mail_server;
		$this->emailManager->CharSet = self::CHARSET;
		$this->emailManager->SMTPAuth = false;
        $this->emailManager->SMTPSecure = 'tls';
        $this->emailManager->IsHTML(true);
		$this->emailManager->SetFrom(AI::getInstance()->getSetup()->mail_from, AI::getInstance()->getSetup()->mail_from_name);

	}
	/**
	 * Sends the mail to the specified recipients
	 * @param string $sendTo
	 * @param string $subject
	 * @param string $content
	 * @return boolean True if success, false otherwise
	 */
	public function sendMail($sendTo = "", $subject, $content){

		$result = false;
		try {
			if ($sendTo != "") {
				$recipients = explode(",", $sendTo);
				if (is_array($recipients)) {
					foreach($recipients as $recipient) {
						$this->emailManager->AddAddress($recipient);
					}
				}
			}
			
			$this->emailManager->Subject = $subject;
			$this->emailManager->Body = $content;
			$result = $this->emailManager->Send();
			$this->emailManager->ClearAllRecipients();
			//$this->emailManager->ClearAttachments();
		} catch(\phpmailerException $ex) {
			\Functions::addLog($ex->getMessage(), "Fatal",json_encode($ex->getTrace()));
		} catch(Exception $ex) {
			\Functions::addLog($ex->getMessage(), "Fatal",json_encode($ex->getTrace()));
		}
		
		return $result;
	}

	public function addAttachment(string $filePath, string $name){

		try {
			if (file_exists($filePath)) {
				$this->emailManager->AddAttachment($filePath, $name);
			}
		} catch(\phpmailerException $ex) {
			\Functions::addLog($ex->getMessage(), "Fatal",json_encode($ex->getTrace()));
		}
	}

	public function addAddress(string $address, string $addressName = ""){

		$this->emailManager->AddAddress($address, ($addressName == "" ? $address : $addressName));
	}
}
?>