<?php


namespace common;

use common\exceptions\AIException;
use Functions;
use PHPMailer\PHPMailer\Exception;
use Throwable;
use ZipArchive;


class ZipFile{
    /* creates a compressed zip file */
    /**
     * @param array $files
     * @param string $destination
     * @throws AIException on failure
     */
    public static function create_zip($files = array(), $destination = '', $overwrite = false) {
        //if the zip file already exists and overwrite is false, return false
        if(file_exists($destination) && !$overwrite) {
            throw (new AIException("The override parameter must be set TRUE because file <$destination> already exists"))
                ->construct(__FUNCTION__, __NAMESPACE__, $args = func_get_args());
        }
        //vars
        $valid_files = array();
        //if files were passed in...
        if(is_array($files)) {
            //cycle through each file
            foreach($files as $file) {
                //make sure the file exists
                if(file_exists($file)) {
                    $valid_files[] = $file;
                }
            }
        }
        //if we have good files...
        $zip = new ZipArchive();
        if(count($valid_files)) {
            //create the archive
            $openResult = $zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE);
            if( $openResult !== true) {
                throw (new AIException("Cannot open zip file for creation"))
                ->construct(__FUNCTION__, __NAMESPACE__, $args = func_get_args(), array(
                    "file_name_to_be_open" => $destination,
                    "override_mode" => $overwrite ? 'ZIPARCHIVE::OVERWRITE': 'ZIPARCHIVE::CREATE'
                ));
            }
            //add the files
            foreach($valid_files as $file) {
                $filename = explode("/", "$file");
                $filename = end($filename);
                if(stripos("$file", "xlz") != false){
                    $zip->addFile($file,"xliff_projects/" . $filename);
                } else if(stripos("$file", "source") != false){
                    $zip->addFile($file,"multipleProjects/" . $filename);
                }else if(stripos("$file", "attachments") != false){
                    $zip->addFile($file,"attachments/" . $filename);
                } else if(stripos("$file", "ltb") != false){
                    $zip->addFile($file,"ltb/" . $filename);
                } else if(stripos("$file", "instructions") != false) {
                    $zip->addFile($file,"" . $filename);
                } else {
                    $zip->addFile($file,"multipleProjects/" . $filename);
                }
            }
            //debug
            //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

            //close the zip -- done!
			$zip->close();

            //check to make sure the file exists
            $fileHasBeenWritten = file_exists($destination);
            if(!$fileHasBeenWritten){
                throw (new AIException("File has not been written on FS"))
                    ->construct(__FUNCTION__, __NAMESPACE__, $ARGS = func_get_args());
            }
        } else {
            throw (new AIException("Cannot create ZipFile, zip status: $zip->status"))
                ->construct(__FUNCTION__, __NAMESPACE__, $ARGS = func_get_args());
        }
    }

    /* creates a compressed zip file */
    /**
     * @param array $files
     * @param string $destination
     * @throws AIException on failure
     */
    public static function create_zip_welocalize($files = array(), $destination = '', $overwrite = false) {
        //if the zip file already exists and overwrite is false, return false
        if(file_exists($destination) && !$overwrite) {
            throw (new AIException("The override parameter must be set TRUE because file <$destination> already exists"))
                ->construct(__FUNCTION__, __NAMESPACE__, $args = func_get_args());
        }
        //vars
        $valid_files = array();
        //if files were passed in...
        if(is_array($files)) {
            //cycle through each file
            foreach($files as $file) {
                //make sure the file exists
                if(file_exists($file)) {
                    $valid_files[] = $file;
                }
            }
        }
        //if we have good files...
        $zip = new ZipArchive();
        if(count($valid_files)) {
            //create the archive
            $openResult = $zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE);
            if( $openResult !== true) {
                throw (new AIException("Cannot open zip file for creation"))
                    ->construct(__FUNCTION__, __NAMESPACE__, $args = func_get_args(), array(
                        "file_name_to_be_open" => $destination,
                        "override_mode" => $overwrite ? 'ZIPARCHIVE::OVERWRITE': 'ZIPARCHIVE::CREATE'
                    ));
            }
            //add the files
            foreach($valid_files as $file) {
                $filename = explode("/", "$file");
                $filename = end($filename);
                if(stripos("$file", "xlz") != false){
                    $zip->addFile($file,"xlz/" . $filename);
                } else if(stripos("$file", "source") != false){
                    $zip->addFile($file,"SourceFilesReference/" . $filename);
                }else if(stripos("$file", "attachments") != false){
                    $zip->addFile($file,"ReferenceFilesMail/" . $filename);
                } else if(stripos("$file", "ltb") != false){
                    $zip->addFile($file,"ltb/" . $filename);
                } else if(stripos("$file", "instructions") != false) {
                    $zip->addFile($file,"" . $filename);
                } else {
                    $zip->addFile($file,"ReferenceFilesWS/" . $filename);
                }
            }
            //debug
            //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

            //close the zip -- done!
            $zip->close();

            //check to make sure the file exists
            $fileHasBeenWritten = file_exists($destination);
            if(!$fileHasBeenWritten){
                throw (new AIException("File has not been written on FS"))
                    ->construct(__FUNCTION__, __NAMESPACE__, $ARGS = func_get_args());
            }
        } else {
            throw (new AIException("Cannot create ZipFile, zip status: $zip->status"))
                ->construct(__FUNCTION__, __NAMESPACE__, $ARGS = func_get_args());
        }
    }

    public static function unZipFile($zipFilePath, $destinationPath){
        $zip = new ZipArchive;
        $zip->open($zipFilePath);
        $res = $zip->extractTo($destinationPath);
        $zip->close();
        return $res;
    }

    /**
     * @param $zipFilePath string
     * @param $filesToUnZup string[]
     * @param $destinationPath string
     * @return boolean
     */
    public static function unZipAnyFiles($zipFilePath, $filesToUnZup ,$destinationPath){
        $res = false;
        $zip = new ZipArchive;
        Functions::print($filesToUnZup);
        if($zip->open($zipFilePath) === true){
            $res = $zip->extractTo($destinationPath,$filesToUnZup);
            $zip->close();
        }
        return $res;
    }

    public static function listFiles($zipFilePath){
        $files = array();
        if(file_exists($zipFilePath)){
            $zip = new ZipArchive();
            $openResult = $zip->open($zipFilePath);
            if($openResult === true){
                $numFiles = $zip->numFiles;
                for($i = 0; $i < $numFiles; $i++){
                    $fileName = $zip->getNameIndex($i);
                    $files[] = $fileName;
                }
                $zip->close();
            }else{
                $files = false;
            }

        }
        return $files;
    }

    public static function deleteFilesOnZip($path,$filesToDelete){
        $validation = false;
        if(file_exists($path)){
            $zip = new ZipArchive();
            if($zip->open($path) === TRUE){
                foreach ($filesToDelete as $file){
                    $zip->deleteName($file);
                }
                $validation = $zip->close();
            }
        }

        return $validation;
    }

	public static function get_language_xlz($file_xlz){
		$zip = new ZipArchive;
		try{
			$zip->open($file_xlz);
			$file_name = $zip->getNameIndex(0);
			$zip->close();
			if(stripos($file_name,"es_ES") !== false) {
				return("Spanish (Spain)");
			}elseif (stripos($file_name,"ca_ES") !== false){
				return("Catalan");
			}

		}catch (Throwable $exception){
			echo($exception);
		}
	}


}