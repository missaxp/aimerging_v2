<?php


namespace common;

use core\AI;
use DateTime;
use Functions;

class Repository
{
    private $taskDAO;
    private $aiDAO;

    public function __construct()
    {
        $this->aiDAO = new \dataAccess\dao\AIDAO();
        $this->taskDAO = new \dataAccess\dao\TaskDAO();
    }

    public function deleteFiles($lastExecuting)
    {
        if($lastExecuting !== null){
            $lastExecuting->sub(new \DateInterval(AI::getInstance()->getSetup()->intervalForFiles));
        }
        $date = new \DateTime();
        $id = $this->aiDAO->saveExecution($date->format("Y-m-d H:i:s"),AI::REPOSITORY);
        $date->sub(new \DateInterval(AI::getInstance()->getSetup()->intervalForFiles));
        $tasks = $this->taskDAO->getTasksWithOnlyFiles($date,$lastExecuting);

        foreach ($tasks as $task) {
            foreach ($task->getAttachments() as $file) {
                if (file_exists($file->getPath()) && @unlink($file->getPath())) {
                    $file->setIsDelete(1);
                    $this->taskDAO->updateFile($file);
                    if($task->getDataSource() !== null){
                        Functions::console("FR::".$task->getDataSource()->getSourceName()." - ".$task->getUniqueSourceId()." - ".$file->getPath());
                    }
                }else{
                    $file->setIsDelete(1);
                    $this->taskDAO->updateFile($file);
                    if($task->getDataSource() !== null){
                        Functions::console("FR::File not found - ".$task->getDataSource()->getSourceName()." - ".$task->getUniqueSourceId()." - ".$file->getPath());
                    }
                }
            }
        }
        Functions::console("FR::Tasks: ".count($tasks));
        $date->add(new \DateInterval(AI::getInstance()->getSetup()->intervalForFiles));
        $this->aiDAO->saveEndExecution($id,$date->format("Y-m-d H:i:s"),AI::SUCCESSFULL);
    }

    public function execute($withConfiguration = true, $configuration = null){
        $lastExecuting = $this->aiDAO->getLastExecuting(AI::REPOSITORY);
        if($configuration === null){
            $interval = explode("T", AI::getInstance()->getSetup()->invervalRepositoryClean);
            $time = explode(":", $interval[1]);
        }else{
            $interval = $configuration["Interval"];
            $time = $configuration["Time"];
        }

        if($withConfiguration){
            $currentDate = new DateTime();
            $repositoryCleanDate = $this->calculateInterval($lastExecuting, $time, $interval);
            /**
             * Checks if it has to be executed
             */
            if($lastExecuting === null || ($currentDate >= $repositoryCleanDate && $currentDate->format("H") === $repositoryCleanDate->format("H"))){
                Functions::console("Delete files");
                $this->deleteFiles($lastExecuting);
                Functions::console("Process finished");
            }else{
                Functions::console("Waiting for scheduled execution");
            }
        }else{
            Functions::console("Delete files without configuration");
            $this->deleteFiles($lastExecuting);
        }
    }

    /**
     * @param DateTime $lastExecuting
     * @param array $time
     * @param array $interval
     * @return null | DateTime
     * @throws \Exception
     *
     */
    public function calculateInterval($lastExecuting,$time,$interval)
    {
        /**
         * @var \DateTime
         */
        $repositoryCleanDate = null;

        if (count($interval) === 2 && count($time) === 3) {
            if ($lastExecuting !== null) {
                $repositoryCleanDate = clone($lastExecuting);
                $repositoryCleanDate->add(new \DateInterval($interval[0]));
                $repositoryCleanDate->setTime($time[0], $time[1], $time[2], 0);
            }else{

            }
        } else {
            Functions::console("Misconfiguration");
        }
        return $repositoryCleanDate;
    }
}