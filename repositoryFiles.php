<?php

use core\AI;

if(0 > version_compare(PHP_VERSION, '7.2')){
    die("AI Requires PHP 7.2 or higher");
}

if (isset($_SERVER["OS"])) {
    if (strpos(strtolower($_SERVER["OS"]), "windows")!==false) {
        $_SERVER["DOCUMENT_ROOT"] = dirname(__FILE__);
    }
}

if(!isset($_SERVER["DOCUMENT_ROOT"]) || $_SERVER["DOCUMENT_ROOT"]==""){
    $_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__);
}

define ( "BaseDir", $_SERVER ["DOCUMENT_ROOT"] );
require_once BaseDir.'/declare.php';

global $setup;

$ai = AI::getInstance($setup);
require_once BaseDir.'/common/Repository.php';
$configuration = true;
if(isset($_GET['configuration'])) {
    $configuration = $_GET['configuration'] === "true";
}
$repository = new \common\Repository();
Functions::console("Start process");
$repository->execute($configuration);
Functions::console("Finished process");
?>