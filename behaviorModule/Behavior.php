<?php

/**
 *
 * @author zroque
 * @version 1.0
 *          created on 29 jun. 2018
 */
namespace behaviorModule;

use common\exceptions\AIException;
use Functions;
use model\Task;
use model\State;
use dataAccess\dao\ProcessDAO;
use dataAccess\dao\TaskDAO;
use model\Process;
use core\AI;
use model\File;
use behaviorModule\tms\tms;
use model\Action;
use model\Schedule;
use dataAccess\dao\NotificationDAO;
use dataAccess\dao\SourceDAO;
use model\Source;
use Throwable;

include_once BaseDir . '/dataAccess/dao/ProcessDAO.php';
include_once BaseDir .'/dataAccess/dao/NotificationDAO.php';
class Behavior {
    /**
     * @var TaskDAO
     */
    private $taskDao;
    private $noMatchedRules;
    public const NORULES = "No rules where found for this task";
    public const NOMATCH = "No match for the following rules: ";
    public const MATCH = "The task matched the criteria for process: ";
    public const PENDING = "Waiting for confirmation to continue with actions";
    public const PROCESSED = "The task was processed";
    public const EXECUTINGACTIONS = "The actions are being executed";
    public const TASKCONFIRMED = "The task was confirmed by: ";
    private $tms;
    private $ondemandConfiguration;
    private $ondemandLog;

    public function getOndemandConfiguration(){
        return $this->ondemandConfiguration;
    }


    public function addOndemandConfiguration($key,$value){
        $this->ondemandConfiguration[$key] = $value;
    }
    public function addOndemandLog($key,$value){
        $this->ondemandLog[$key] = $value;
    }

    public function getOndemandLog(){
        return $this->ondemandLog;
    }

    public function setOndemandLog($ondemandLog){
        $this->ondemandLog = array_merge($ondemandLog,$this->ondemandLog);
    }

    public function setOndemandConfiguration($ondemandConfiguration){
        $this->ondemandConfiguration = $ondemandConfiguration;
    }

    public function __construct(){
        $this->ondemandLog= array();
        $this->ondemandConfiguration = array();
        $this->taskDao = new TaskDAO();
        $this->noMatchedRules = array();
    }

    /**
     * Returns default TMS
     *
     * @param Task $task
     * @param boolean $isDevelopment
     * @return tms
     * @throws
     */
    public static function tms(Task $task, $isDevelopment = null){

        include_once BaseDir . '/behaviorModule/tms/' . AI::getInstance()->getSetup()->default_TMS . '.php';
        $tmsS = AI::getInstance()->getSetup()->default_TMS;
        $className = "behaviorModule\\tms\\$tmsS\\$tmsS";

        return new $className($task, $isDevelopment);
    }

    /**
     * Receives the tasks from the harvest module, also will reprocess the tasks with retry, executingActions
     * or filtered state.
     * If a process is matched the actions related wil be executed in the configured order.
     *
     * @param Task[] $tasks
     * @return boolean
     * @throws \Exception
     */
    public function processTasks($tasks,$inqueue = true){
        Functions::console("BM::Start Behaviour");
        Functions::console("BM::Start Processing Pending Tasks");
        $memoqErrors = null;
        /**
         * Process pending tasks
         */
        if($inqueue){
            $taskDao = new TaskDAO();
            $pendingTasks = $taskDao->getTaskToReprocess();
            foreach($pendingTasks as $task) {
                Functions::console("BM::".$task->getUniqueSourceId()." - Processing Pending task");
                try {
                    $this->resumeProcessActions($task);
                } catch (Throwable $e) {
                    $ex = AIException::createInstanceFromThrowable($e) ->construct(__METHOD__, __NAMESPACE__, $args = func_get_args());
                    Functions::logException($e->getMessage(), Functions::ERROR, __CLASS__, $ex);
                }
            }
            unset($tasks);
            Functions::console("BM::Start processing COLLECTED tasks");
            $tasks = $taskDao->getTaskCollected();
        }

        //Gettasks COLLECTING.
        /**
         * SELECT * From Task where idState == COLLECTING
         * FOREACH row->getRow){
         * Para cada fichero con successDownload = 0 mirar si el fichero en la ruta Path EXISTE y SI YA ESTA DESCARGADO.
         * Si lo está miramos que se haya descargado bien y si todos los ficheros de la tarea en COLLECTING se han descargado bien -> nuevo estado -> COLLECTED.
         * }
         *
         *
         */

        // For each new task try to get the matched process.
        foreach($tasks as $task) {
            $scheduleProcessTask = false;
            $actionStatus = null;
            $process = $this->getMatchedProcess($task);
// 			$process =
            // If a process is matched, execute their action secuentially.
            if ($process !== false) {
                Functions::console("BM::".$task->getUniqueSourceId()." - Task COLLECTED. Process found: ".$process->getProcessName());
                $scheduleSetup = ($process->getSchedules() != null)?$process->getSchedules()->getProcessType():Schedule::_ONDEMAND;

                //TODO:: Siempre devuelve true.
                $scheduleProcessTask = ($scheduleSetup != null && ($scheduleSetup==Schedule::_ONDEMAND || ($scheduleSetup==Schedule::_INQUEUE) && $process->getSchedules()->checkIfHasProcessQueue()));

                if($scheduleProcessTask){
                    if (! empty($process->getActions(true))){
                        $this->taskDao->updateTaskState($task, State::EXECUTINGACTIONS, self::EXECUTINGACTIONS. ' by process '. $process->getIdProcess(), 0, $process->getIdProcess());
                        $state = new State(State::EXECUTINGACTIONS);
                        $task->setState($state);
                    }else{
                        $this->taskDao->updateTaskState($task, State::RETRY, "Process " . $process->getProcessName() . " have not any Action", 0, $process->getIdProcess(), true, false);
                        $state = new State(State::RETRY);
                        $task->setState($state);
                    }
                    /**@var \model\Process $result*/
                    if($process->getIdCATTool() !== null){
                        Functions::console("BM::Processing task=> ".$task->getUniqueSourceId()." with memoq");
                        $memoq = $process->getCATToolSetup();
                        $memoq->executeMemoqActions($task);
                        $memoqErrors = $memoq->getErrors();
                    }
                    if (! empty($process->getActions(true))) {

                        foreach($process->getActions(true) as &$action) {

                            if ($action->executeAction($task)) {

                                $actionStatus = $this->processMatchedTask($action, $task);

                                $this->taskDao->updateTaskState($task, State::EXECUTINGACTIONS, $action->getActionName(), $action->getIdAction(), $action->getIdProcess());

                                if ($actionStatus == Action::_ACTION_PENDING_CONFIRM || $actionStatus == Action::_ACTION_WITH_ERRORS) {
                                    break;
                                }
                            } else {
                                /* The action failed and onfallBack is false, so the process has to stop and the action must
                                 * be executed again in the next iteration*/
                                $actionStatus = Action::_ACTION_FAIL;
                                if (!$action->getOnFallBack()) {
                                    $this->taskDao->updateTaskState($task, State::RETRY, "Fail action => ".$action->getActionName(), 0, $action->getIdProcess(),true,false);
                                    $state = new State(State::RETRY);
                                    $task->setState($state);
                                    $actionStatus = null;
                                    break;
                                }else{
                                    /* The action failed but onFallBack is true, so the process must continue
                                     * and the action should not be executed again so we update the task state*/
                                    $actionStatus = $this->processMatchedTask($action, $task);
                                    $this->taskDao->updateTaskState($task, State::EXECUTINGACTIONS, $action->getActionName(), $action->getIdAction(), $action->getIdProcess());
                                }
                            }
                        }
                    }else{
                        if(count($task->getTaskErrors()) > 0){
                            Functions::console("BM::".$task->getUniqueSourceId()." - Task PROCESSEDWITHHARVESTWARNINGS.");
                            $this->taskDao->updateTaskState($task, State::PROCESSEDWITHWARNING, 'Processed with warnings and it did not take any action');
                            $state = new State(State::PROCESSEDWITHWARNING);
                            $task->setState($state);
                            $actionStatus = Action::_ACTION_WITH_WARNINGS;
                        } else {
                            $this->taskDao->updateTaskState($task, State::PROCESSED, self::PROCESSED.' but it did not take any action');
                            $state = new State(State::PROCESSED);
                            $task->setState($state);
                            $actionStatus = Action::_ACTION_SUCCEDS;
                        }
                    }
                }
                if(count($process->getActionsErrors()) == 0 && count($process->getActionsWarnings()) == 0 && $actionStatus != Action::_ACTION_FAIL && $actionStatus != Action::_ACTION_PENDING_CONFIRM){
                    if(count($task->getTaskErrors()) > 0){
                        if($task->getState()->getIdState() != State::PROCESSEDWITHWARNING) {
                            Functions::console("BM::".$task->getUniqueSourceId()." - Task PROCESSEDWITHHARVESTWARNINGS.");
                            $this->taskDao->updateTaskState($task, State::PROCESSEDWITHWARNING, "Harvesting errors");
                            $state = new State(State::PROCESSEDWITHWARNING);
                            $task->setState($state);
                        }
                    } else {
                        $this->taskDao->updateTaskState($task, State::PROCESSED, self::PROCESSED,0,0,false,false);
                        $state = new State(State::PROCESSED);
                        $task->setState($state);
                    }
                }

                if($actionStatus != null && $scheduleProcessTask){
                    if($memoqErrors === null){
                        $this->updateTaskStates($process, $actionStatus, $task);
                    }else{
                        $this->updateTaskStates($process, $actionStatus, $task, $memoqErrors);
                    }
                }
                $this->ondemandLog[$task->getIdClientetask()]["state"] = $task->getState()->getStateName();
            } else {
                Functions::console("BM::".$task->getUniqueSourceId()." - Task rejected. No process where found for this task");
                $this->taskDao->updateTaskState($task, State::REJECTED, 'Task rejected');
                $task->setState(new State(State::REJECTED));
                $this->ondemandLog[$task->getIdClientetask()]["state"] = $task->getState()->getStateName();
                $this->ondemandLog[$task->getIdClientetask()]["info"] = $task->getUniqueSourceId()." - Task rejected. No process where found for this task";
            }

        }

        Functions::console("BM::End processing tasks");

        return true;
    }

    /**
     * Returns true if action succeds or succeds with warnings, false when action fails or null if action is confirmEmail.
     *
     * @author phidalgo
     * @param Action $action
     * @param Task $task
     * @return boolean|null
     */
    private function processMatchedTask(&$action, $task){

        // Si la acción tiene algun error y no tenemos OnFallbackContinue seteado a TRUE, break de actions
        if ($action->hasErrors() && ! $action->getOnFallBack()) {
            return Action::_ACTION_WITH_ERRORS;
        }
        if ($action->hasErrors() && $action->getOnFallBack()) {
            return Action::_ACTION_WITH_ERRORS_ONFALLBACK_CONTINUE;
        }

        if ($action->hasWarnings()) {
            return Action::_ACTION_WITH_WARNINGS;
        }

        if ($action->getActionType() == "confirmEmail") {
            return Action::_ACTION_PENDING_CONFIRM;
        }

        return Action::_ACTION_SUCCEDS;
    }

    /**
     *
     * @param Process $process
     * @param string $actionStatus
     * @return boolean
     */
    private function updateTaskStates(&$process, $actionStatus, Task $task, $memoqErrors = array()){
        if ($actionStatus !== State::REJECTED) {
            if($actionStatus == Action::_ACTION_FAIL){
                Functions::console("BM::".$task->getUniqueSourceId()." - Task RETRY. An action fails");
                $this->taskDao->updateTaskState($task, State::RETRY, "Action fail",0,0,false,false);
                $state = new State(State::RETRY);
                $task->setState($state);
            }else{
                if ($actionStatus == Action::_ACTION_PENDING_CONFIRM) {
                    Functions::console("BM::".$task->getUniqueSourceId()." - Task PENDINGCONFIRMATION. Task pending to be confirmed.");
                    $this->taskDao->updateTaskState($task, State::PENDINGCONFIRMATION, self::PENDING,0,0,false,false);
                    $state = new State(State::PENDINGCONFIRMATION);
                    $task->setState($state);
                } else {
                    switch ($process->checkActionStatus()) {
                        case Process::_ACTIONS_STATUS_SUCCESS :
                            if(count($task->getTaskErrors()) > 0){
                                if($task->getState()->getIdState() != State::PROCESSEDWITHWARNING) {
                                    Functions::console("BM::" . $task->getUniqueSourceId() . " - Task PROCESSEDWITHHARVESTWARNINGS.");
                                    $this->taskDao->updateTaskState($task, State::PROCESSEDWITHWARNING, "Harvesting errors");
                                    $state = new State(State::PROCESSEDWITHWARNING);
                                    $task->setState($state);
                                }
                            } else {
                                Functions::console("BM::".$task->getUniqueSourceId()." - Task PROCESSED.");
                                if($task->getState()->getIdState() != State::PROCESSED){
                                    $this->taskDao->updateTaskState($task, State::PROCESSED, self::PROCESSED,0,0,false,false);
                                    $state = new State(State::PROCESSED);
                                    $task->setState($state);
                                }
                            }
                            break;
                        case Process::_ACTIONS_STATUS_WARNING :
                            Functions::console("BM::".$task->getUniqueSourceId()." - Task PROCESSEDWITHWARNINGS.");
                            $this->taskDao->updateTaskState($task, State::PROCESSEDWITHWARNING, $process->getActionsWarningsAsString(false));
                            $state = new State(State::PROCESSEDWITHWARNING);
                            $task->setState($state);
                            break;
                        case Process::_ACTIONS_STATUS_ERROR :
                            Functions::console("BM::".$task->getUniqueSourceId()." - Task RETRY. An action fails");
                            $this->taskDao->updateTaskState($task, State::RETRY, $process->getActionsErrorsAsString(false),0,0,false,false);
                            $state = new State(State::RETRY);
                            $task->setState($state);
                            break;
                        case Process::_ACTIONS_STATUS_ERROR_ONFALLBACK_CONTINUE:
                            Functions::console("BM::".$task->getUniqueSourceId()." - Task PROCESSED WITH ERRORS");
                            $this->taskDao->updateTaskState($task, State::PROCESSED, $process->getActionsErrorsAsString(false),0,0,false,false);
                            $state = new State(State::PROCESSED);
                            $task->setState($state);
                            break;
                    }

                    $process->processNotification($task, $memoqErrors);
                }
            }

        } else {
            $this->taskDao->updateTaskState($task, State::REJECTED, self::NORULES);
        }
        return true;
    }

    /**
     * Gets the process related with the source of the task, checks each rule and return the process if matched, false otherwise
     *
     * @param Task $task
     * @return Process || boolean
     */
    private function getMatchedProcess(Task $task){

        $processDao = new ProcessDAO();
        $processes = array();
        if($task->getDataSource() !== null){
            $processes = $processDao->getProcessesBySource($task->getDataSource()->getIdSource());
        }else{
            Functions::console($task->getUniqueSourceId());
        }


        foreach($processes as $process) {
            $this->noMatchedRules = array();
            $rules = $process->getRules();
            $totalWeight = 0;
            foreach($rules as $rule) {
                $check = $rule->check($task);
                if (! $check) {
                    $totalWeight += $rule->getWeight();
                }
            }
            if ($totalWeight < 1) {
                $this->taskDao->updateTaskState($task, State::FILTERED, Behavior::MATCH.$process->getIdProcess()." ".$process->getProcessName(), 0, $process->getIdProcess(), true, false);
                $state = new State(State::FILTERED);
                $task->setState($state);
                return $process;
            }
        }

        return false;
    }

    /**
     * If the processActions where stoped for confirmation or errors, this method will resume the process and
     * execute the remaining actions
     *
     * @param Task $task
     * @throws \Exception
     */
    public function resumeProcessActions(Task $task){

        $dao = new ProcessDao();
        $sequence = 0;
        if ($task->getIdActions() !== null) {
            $sequence = $dao->getSequence($task->getIdProcess(), $task->getIdActions());
        }
        $process = $dao->getProcess($task->getIdProcess());

        if ($process !== null) {
            $actionStatus = null;
            $this->taskDao->updateTaskState($task, State::EXECUTINGACTIONS, self::EXECUTINGACTIONS, 0, $process->getIdProcess(), true,false);
            $totalActions = count($process->getActions(true));
            $executedActions = 0;
            foreach($process->getActions(true) as $action) {
                if ($action->getSequence() > $sequence) {

                    if ($action->executeAction($task)) {

                        $actionStatus = $this->processMatchedTask($action, $task);
                        if(Action::_ACTION_SUCCEDS == $actionStatus || Action::_ACTION_WITH_WARNINGS == $actionStatus){
                            $this->taskDao->updateTaskState($task, State::EXECUTINGACTIONS, $action->getActionName(), $action->getIdAction(), $action->getIdProcess());
                        }
                        if ($actionStatus == Action::_ACTION_PENDING_CONFIRM || $actionStatus == Action::_ACTION_WITH_ERRORS) {
                            break;
                        }
                    } else {
                        /* The action failed and onfallBack is false, so the process has to stop and the action must
                         * be executed again in the next iteration*/
                        $actionStatus = Action::_ACTION_FAIL;
                        if (!$action->getOnFallBack()) {
                            $this->taskDao->updateTaskState($task, State::RETRY, 'Fail action => ' . $action->getActionName(), 0, $action->getIdProcess(),true,false);
                            $actionStatus = null;
                            break;
                        }else{
                            /* The action failed but onFallBack is true, so the process must continue
                             * and the action should not be executed again so we update the task state*/
                            $actionStatus = $this->processMatchedTask($action, $task);
                            $this->taskDao->updateTaskState($task, State::EXECUTINGACTIONS, $action->getActionName(), $action->getIdAction(), $action->getIdProcess());
                        }
                    }
                }
                $executedActions++;
            }

            if($executedActions == $totalActions && count($process->getActionsErrors()) == 0 && count($process->getActionsWarnings()) == 0 && $actionStatus != Action::_ACTION_FAIL){
                $actionStatus = Action::_ACTION_SUCCEDS;
            }

            if($actionStatus != null){
                $this->updateTaskStates($process, $actionStatus, $task);
            }
        } else {
            $this->taskDao->updateTaskState($task, State::REJECTED, "Process not found when resumed task");
            throw new \Exception("Confirm task USID Not FOUND", 500);
        }
    }

    /**
     * Check if a task given a Unique Task ID already exists
     * Return true if exists otherwise returns false.
     *
     * @param Task $taskToCheck
     * @return boolean
     */
    public function checkIfTaskExist($taskToCheck){

        $dao = new TaskDAO();

        $task = $dao->getTaskByUTID($taskToCheck->getUniqueTaskId());
        //return ($task->getUniqueSourceId() != null);
        $changeDetails = "";
        if($task->getUniqueTaskId() != null){
            $isChanged = false;
            if($task->getMenssage() != $taskToCheck->getMenssage()){
                $isChanged = true;
                $changeDetails .="MESSAGE => ".$task->getMenssage()."TO => ".$taskToCheck->getMenssage()."<br>";
            }
            if($task->getDueDate() != $taskToCheck->getDueDate()){
                $isChanged = true;
                $changeDetails .= "DUE DATE => ".$task->getDueDate()->format("Y-m-d H:i:s")." TO => ".$taskToCheck->getDueDate()->format("Y-m-d H:i:s")."<br>";
            }

            $taskFiles = $task->getFilesToTranslate();
            $taskToCheckFiles = $taskToCheck->getFilesToTranslate();
            if(count($taskFiles) != count($taskToCheckFiles)){
                $isChanged = true;
                $changeDetails .="ADDED FILE"."<br>";
            }else{
                $changedFiles = 0;
                foreach($taskFiles as $file){
                    foreach($taskToCheckFiles as $fileToCheck){
                        if(strcmp($file->getFileName(), $fileToCheck->getFileName()) === 0){
                            if($file->getSize() != $fileToCheck->getSize()){
                                $changedFiles++;
                                $changeDetails .= "CHANGED FILE =>".$file->getFileName()."<br>";
                            }
                        }
                    }
                    if($changedFiles > 0){
                        $isChanged = true;
                    }
                }
            }
            if($isChanged){
                $notificationDao = new NotificationDAO();
                $idProcess = 0;
                if($task->getIdProcess() != null){
                    $idProcess = $task->getIdProcess();
                }
                $notif = $notificationDao->getNotificationByProcess($idProcess);
                if($notif->getIdProcess() != null){
                    if($notif->getWhenChanged()){
                        $messages = array();
                        $messages["changes"] = $changeDetails;
                        $notif->send($task, $messages, $task->getIdProcess());
                    }
                }
            }
        }
        return ($task->getUniqueSourceId() != null);
    }
}
?>