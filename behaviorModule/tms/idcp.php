<?php
namespace behaviorModule\tms\idcp;


use behaviorModule\tms\tms;
use common\exceptions\AIException;
use dataAccess\dbConn;
use behaviorModule\tms\tmsProperty;
use behaviorModule\tms\tmsException;
use behaviorModule\tms\tmsPropertyValues;
use Functions;
use model\Task;
use behaviorModule\tms\tmsErrors;
use service\EmailManager;
use model\Language;
use core\AI;
use Throwable;
use api\crud\TaskType;

require_once BaseDir .'/behaviorModule/tms/tms.php';
/**
*
* Specific class for TMS :: IDCP - iDISC Collaboration Portal
* 
* @author phidalgo
* @version 1.0
* created on 12 jul. 2018
* 
* 
*/
class idcp extends tms{
	private static $TMS_CREATOR = "AISYSTEM";
	//Specific static variables for this TMS.
	private static $FILESYSTEMFOLDER = null;
	private static $FTPFOLDER = null;
	private static $CON_STRING = null;
	private static $CON_USER = null;
	private static $CON_PWD = null;
	private static $fuzzies = array('XTranslated','Repetition','100%','95%','85%','75%','50%','New Words','Hours');
	private static $fileCategories = array('Source files','Reference files');
	
	
	/**
	 * Only set when it is created.
	 * If task type is TEP, returns TRANSLATION TASK ID.
	 * @var integer
	 */
	private $IDCPTaskID = null;
	
	/**
	 * Determines when the task is created, if we create the specified folder structure into FileSystem by calling createTaskIntoFS().
	 * @var boolean
	 */
	private $createTaskIntoFS = true;
	/**
	 * DataBase Connection object
	 * @var dbConn
	 */
	private static $dbconn = null;
	
	/**
	 * IDCP TMS has ORACLE DATABASE SYSTEM.
	 * 
	 * @return dbConn
	 */
	private static function getDbconn(){
		if(self::$dbconn==null){
			self::$dbconn = new dbConn (DBCONN_ORACLE, self::$CON_USER, self::$CON_PWD, self::$CON_STRING);
		}
		return self::$dbconn;
	}
	
	public function __construct(Task $task, $development = null){
		parent::__construct($task, $development);

		//Depending on the environment, we set static vars. 
		if($this->isDevelopment){
			self::$FILESYSTEMFOLDER = $_SERVER["DOCUMENT_ROOT"]."/testTMSData/";
			self::$FTPFOLDER = $_SERVER["DOCUMENT_ROOT"]."/testTMSData/testTMSData/";
			self::$CON_STRING = "(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = cesi.iit.idisc.es)(PORT = 1521)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = idisc)))";
			self::$CON_USER = "webtraduc";
			self::$CON_PWD = "webtraduc";
		}
		else{
			self::$FILESYSTEMFOLDER = "/media/L/";
			self::$FTPFOLDER = "/media/traductemp/";
			self::$CON_STRING = "(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = oxigen.iit.idisc.es)(PORT = 1521)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = GESTIO.IDISC.ES)))";
			self::$CON_USER = "webtraduc";
			self::$CON_PWD = "webtraduc";
		}
		self::getDbconn();
		
		//Generates the $tmsProperties array with the allowed values if needed.
		if(parent::$tmsProperties==null){
			parent::$tmsProperties = array();
			$areas = array();
			$sql = "SELECT * FROM AREAS ORDER BY CODI ASC";
			$q = self::$dbconn->execute($sql);
			while($q->fetch()){
				$areas[] = new tmsPropertyValues($q->CODI, $q->CODI);
			}
			$q->close();
			
			$grups = array();
			$sql = "SELECT * FROM GRUPS ORDER BY DESCRIPCIO ASC";
			$q = self::$dbconn->execute($sql);
			while($q->fetch()){
				$grups[] = new tmsPropertyValues($q->ID, $q->DESCRIPCIO);
			}
			$q->close();
			
			$resources = array();
			$sql = "select codi_recurs,nom from dades_recurs where codi_recurs in (select codi from sygesrecursos where codi=codi_recurs) AND PRIORITAT<=6 order by codi_recurs asc";
			$q = self::$dbconn->execute($sql);
			while($q->fetch()){
				$resources[] = new tmsPropertyValues($q->codi_recurs, $q->codi_recurs);
			}
			$q->close();
			

			$sql ="select codi,descripcio,area,a_prop_area from sygesprojectes where nivell='1' and obert='A' and departament like '%T%' order by codi asc";
			$projects = array();
			$q = self::$dbconn->execute($sql);
			while($q->fetch()){
				$projects[] = new tmsPropertyValues($q->codi, $q->descripcio);
			}
			$q->close();
			
			$sql ="select codi_recurs,nom from dades_recurs WHERE tipus_recurs in ('P','A','SA') and prioritat<9 order by codi_recurs asc";
			$pms = array();
			$q = self::$dbconn->execute($sql);
			while($q->fetch()){
				$pms[] = new tmsPropertyValues($q->codi_recurs, $q->codi_recurs);
			}
			$q->close();
			
			
			$taskTypes = array();
			$IDCPtaskTypes[] = new tmsPropertyValues(IDCPtaskType::_TR, IDCPtaskType::_TR);
			$IDCPtaskTypes[] = new tmsPropertyValues(IDCPtaskType::_PR, IDCPtaskType::_PR);
			$IDCPtaskTypes[] = new tmsPropertyValues(IDCPtaskType::_TP, IDCPtaskType::_TP);
			$IDCPtaskTypes[] = new tmsPropertyValues(IDCPtaskType::_PE, IDCPtaskType::_PE);
			$IDCPtaskTypes[] = new tmsPropertyValues(IDCPtaskType::_MIS, IDCPtaskType::_MIS);
			$IDCPtaskTypes[] = new tmsPropertyValues(IDCPtaskType::_ENG, IDCPtaskType::_ENG);
			$IDCPtaskTypes[] = new tmsPropertyValues(IDCPtaskType::_DTP, IDCPtaskType::_DTP);
			$IDCPtaskTypes[] = new tmsPropertyValues(IDCPtaskType::_MNG, IDCPtaskType::_MNG);
			
			$rates = array();
			$rates[] = new tmsPropertyValues("N", "N");
			$rates[] = new tmsPropertyValues("X", "X");
			
			$taskStatus = array();
			$taskStatus[] = new tmsPropertyValues(taskStatus::_ASSIGNED, taskStatus::_ASSIGNED);
			$taskStatus[] = new tmsPropertyValues(taskStatus::_READY, taskStatus::_READY);
			$taskStatus[] = new tmsPropertyValues(taskStatus::_WORKING, taskStatus::_WORKING);
			$taskStatus[] = new tmsPropertyValues(taskStatus::_DELIVERED, taskStatus::_DELIVERED);
			$taskStatus[] = new tmsPropertyValues(taskStatus::_RECEIVED, taskStatus::_RECEIVED);
			$taskStatus[] = new tmsPropertyValues(taskStatus::_CLOSED, taskStatus::_CLOSED);
			
			$folderTypes = array();
			$folderTypes[] = new tmsPropertyValues(folderType::NORMAL, folderType::NORMAL);
			$folderTypes[] = new tmsPropertyValues(folderType::ESPECIAL, folderType::ESPECIAL);
			
			$folderCreationNames = array();
			$folderCreationNames[] = new tmsPropertyValues(folderCreationName::_NO_FOLDER, folderCreationName::_NO_FOLDER);
			$folderCreationNames[] = new tmsPropertyValues(folderCreationName::_FOLDER_TASK_NAME, folderCreationName::_FOLDER_TASK_NAME);
			$folderCreationNames[] = new tmsPropertyValues(folderCreationName::_FOLDER_BATCH_COUNTER, folderCreationName::_FOLDER_BATCH_COUNTER);
			$folderCreationNames[] = new tmsPropertyValues(folderCreationName::_FOLDER_DATE, folderCreationName::_FOLDER_DATE);
			$folderCreationNames[] = new tmsPropertyValues(folderCreationName::_FOLDER_DATETIME, folderCreationName::_FOLDER_DATETIME);
			
			
			// parent::$tmsProperties[] = new tmsProperty("area", tmsProperty::_STRING, $areas);
			// parent::$tmsProperties[] = new tmsProperty("translator", tmsProperty::_STRING, $resources);
			// parent::$tmsProperties[] = new tmsProperty("proofer", tmsProperty::_STRING, $resources);
			// parent::$tmsProperties[] = new tmsProperty("area2", tmsProperty::_STRING, $areas);
			// parent::$tmsProperties[] = new tmsProperty("origen", tmsProperty::_STRING, $pms);
			// parent::$tmsProperties[] = new tmsProperty("projecte", tmsProperty::_STRING, $projects, true);
			// parent::$tmsProperties[] = new tmsProperty("origen", tmsProperty::_STRING, $pms);
			// parent::$tmsProperties[] = new tmsProperty("inici_previst", tmsProperty::_DATE);
			// parent::$tmsProperties[] = new tmsProperty("tipus_tasca", tmsProperty::_STRING, $taskTypes);
			// parent::$tmsProperties[] = new tmsProperty("tarifa", tmsProperty::_STRING, $rates);
			// parent::$tmsProperties[] = new tmsProperty("estat", tmsProperty::_INT, $taskStatus);
			// parent::$tmsProperties[] = new tmsProperty("id_grup", tmsProperty::_INT, $grups);
			// parent::$tmsProperties[] = new tmsProperty("folderType", tmsProperty::_STRING, $folderTypes);
			// parent::$tmsProperties[] = new tmsProperty("folderCreationNames", tmsProperty::_INT, $folderCreationNames);
			parent::$tmsProperties['area'] = new tmsProperty("area", tmsProperty::_STRING, $areas);
			parent::$tmsProperties['translator'] = new tmsProperty("translator", tmsProperty::_STRING, $resources);
			parent::$tmsProperties['proofer'] = new tmsProperty("proofer", tmsProperty::_STRING, $resources);
			parent::$tmsProperties['area2'] = new tmsProperty("area2", tmsProperty::_STRING, $areas);
			parent::$tmsProperties['origen'] = new tmsProperty("origen", tmsProperty::_STRING, $pms);
			parent::$tmsProperties['projecte'] = new tmsProperty("projecte", tmsProperty::_STRING, $projects, true);
			parent::$tmsProperties['origen'] = new tmsProperty("origen", tmsProperty::_STRING, $pms);
			parent::$tmsProperties['inici_previst'] = new tmsProperty("inici_previst", tmsProperty::_DATE);
			parent::$tmsProperties['tipus_tasca'] = new tmsProperty("tipus_tasca", tmsProperty::_STRING, $IDCPtaskTypes);
			parent::$tmsProperties['tarifa'] = new tmsProperty("tarifa", tmsProperty::_STRING, $rates);
			parent::$tmsProperties['estat'] = new tmsProperty("estat", tmsProperty::_INT, $taskStatus);
			parent::$tmsProperties['id_grup'] = new tmsProperty("id_grup", tmsProperty::_INT, $grups);
			parent::$tmsProperties['folderType'] = new tmsProperty("folderType", tmsProperty::_STRING, $folderTypes);
			parent::$tmsProperties['folderCreationNames'] = new tmsProperty("folderCreationNames", tmsProperty::_INT, $folderCreationNames);
			unset($areas);
			unset($grups);
			unset($taskTypes);
			unset($taskStatus);
			unset($resources);
			unset($pms);
		}
	}
	
	
	/**
	 * Use this method if you want to test to create a task and set if you want to create the task filesystem folder (true) or not (false).
	 * By default, the task folder into FS is created 
	 * If this value is set to false, a warning will be generated.
	 * @param boolean $value
	 * @return boolean
	 */
	public function createTaskIntoFS($value){
		$this->createTaskIntoFS = (is_bool($value)? $value:true);
		return true;
	}
	
	private function createFolder($ftpPath,$type){
		if(!file_exists($ftpPath)){
			if(!@mkdir($ftpPath,0770,true)){
				$this->setWarning("Can't create folder into FTP: The folder already exist: ".$ftpPath);
			}
		}
		if($type == "S"){
			$path = $ftpPath."/Source/";
		}else if($type == "D"){
			$path = $ftpPath."/Discarded/";
		}
		else{
			$path = $ftpPath."/Reference/";
		}
		if(!file_exists($path)){
			if(!@mkdir($path,0770,true)){
				$this->setWarning("IDCP Can not create subfolders on $path");
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Returns task ID only after created is called.
	 * IF TEP, returns TRANS id.  
	 * @return integer
	 */
	public function getTaskID(){
		return $this->IDCPTaskID;
	}
	
	/**
	 * Creates task into the IDCP TMS
	 * {@inheritDoc}
	 * @see \behaviorModule\tms\tms::create()
	 */
	public function create(){
		/**@var Language $tlang*/
		foreach($this->targetLanguages as $tlang){
			if($this->wordCount == null){
				$this->setWordCount($tlang->getAnalysis());
			}
			
			$isTep = false;
			$isGroup = false;
			$ftp = false;
			$vars["TIPUS_TASCA"] = IDCPtaskType::_TR;
			$vars["DESCRIPCIO"] = substr($this->getTitle(), 0, 90);
			$vars["INSTRUCCIONS"] = substr($this->getDescription(), 0 , 1999);
			$vars["ORIGEN_LANG"] = self::getTmsLanguage($this->getSourceLanguage());
			if($vars["ORIGEN_LANG"] == null){
                $this->setWarning("IDCP Language not found. Set to default \"Spanish (Iberian)\". Original language : ".$this->getSourceLanguage()->getLanguageName()." (".$this->getSourceLanguage()->getIdLanguage().")");
                $vars["ORIGEN_LANG"] = "Spanish (Iberian)";
            }
			$vars["DESTI_LANG"] = self::getTmsLanguage($tlang);
            if($vars["DESTI_LANG"] == null){
                $this->setWarning("IDCP Language not found. Set to default \"Spanish (Iberian)\". Original language : ".$tlang->getLanguageName()." (".$tlang->getIdLanguage().")");
                $vars["DESTI_LANG"] = "Spanish (Iberian)";
            }
			$inici = new \DateTime();
			$vars["INICI_PREVIST"] = array($inici->setTimezone(new \DateTimeZone(AI::getInstance()->getSetup()->timeZone_TMS))->format("d/m/Y H:i:s"), DBCONN_DATATYPE_DATE);
			$vars["FINAL_PREVIST"] = array($this->getDueDate()->setTimezone(new \DateTimeZone(AI::getInstance()->getSetup()->timeZone_TMS))->format("d/m/Y H:i:s"), DBCONN_DATATYPE_DATE);
			$vars["CREADOR"] = self::$TMS_CREATOR;
			$vars["ESTAT"] = taskStatus::_ASSIGNED;
			$vars["TARIFA"] = "N";
			$vars["AREA"] = "AAA_UNDEFINED";
			$vars["TASCA_IMPUTADA"] = "N";
			
			$translator = null;
			$proofer = null;
			$projectID = null;
			$fileSystemFolderType = folderType::NORMAL;
			//Task name will be used as default folder name if is not set in DB
			$fileSystemFolderName = folderCreationName::_FOLDER_TASK_NAME;
			foreach(parent::$tmsProperties as $tmsProperty){
				if(!$tmsProperty->hasValue()){ continue;}
				
				if($tmsProperty->getName()=="tipus_tasca" && $tmsProperty->getValue()==IDCPtaskType::_TP){
					$isTep = true;
				}
				
				if($tmsProperty->getName()=="id_grup"){
					$isGroup = true;
				}
				
				if($tmsProperty->getName() == "translator"){
					$translator = $tmsProperty->getValue();
					continue;
				}
				
				if($tmsProperty->getName() == "proofer"){
					$proofer = $tmsProperty->getValue();
					continue;
				}
				
				if($tmsProperty->getName() == "projecte"){
					$projectID = $tmsProperty->getValue();
					continue;
				}
				
				if($tmsProperty->getName() == "folderType"){
					$fileSystemFolderType = $tmsProperty->getValue();
					continue;
				}
				
				if($tmsProperty->getName() == "folderCreationNames"){
					$fileSystemFolderName = $tmsProperty->getValue();
					continue;
				}
				
				
				switch($tmsProperty->getType()){
					case tmsProperty::_CHAR:
					case tmsProperty::_INT:
					case tmsProperty::_STRING:
						$vars[strtoupper($tmsProperty->getName())] = $tmsProperty->getValue();
						break;
					case tmsProperty::_DATE:
						$vars[strtoupper($tmsProperty->getName())] = array($tmsProperty->getValue()->setTimezone(new \DateTimeZone(AI::getInstance()->getSetup()->timeZone_TMS))->format("d/m/Y H:i:s"), DBCONN_DATATYPE_DATE);
						break;
					case tmsProperty::_FLOAT:
						$vars[strtoupper($tmsProperty->getName())] = array($tmsProperty->getValue(), DBCONN_DATATYPE_FLOAT);
						break;
				}
			}
			
			if($projectID==null){
                $e = (new AIException("IDCP error: Project ID = null"))->construct(__METHOD__, __NAMESPACE__, $args = func_get_args());
                Functions::logException($e->getMessage(), Functions::SEVERE, __CLASS__, $e);
                $this->setError(tmsErrors::getErrorMessage(tmsErrors::_PROJECT_ID_CAN_NOT_BE_NULL));
				$this->setWordCount();
				continue;
				//throw new tmsException("IDCP Required property \"projecte\" is missing", 404);
			}
			
			//Comprovamos si el proyecto existe y está abierto, además, si no está definido el origen de la tarea, asignamos el PM como origen de la tarea
			$foundProject = false;
			$sql = "SELECT RESPONSABLE FROM SYGESPROJECTES WHERE CODI=:CODI AND DEPARTAMENT='T' AND OBERT='A'";
			$q = self::$dbconn->execute($sql, array("CODI" => $projectID));
			if($q->fetch()){
				$foundProject = true;
				if(!isset($vars["ORIGEN"])){
					$vars["ORIGEN"] = $q->RESPONSABLE;
				}
				
			}
			$q->close();
			
			if(!$foundProject){
				$this->setError(str_replace("%PROJECT_ID%", $projectID, tmsErrors::getErrorMessage(tmsErrors::_PROJECT_ID_NOT_FOUND)));
				$this->setWordCount();
				continue;
				//throw new tmsException("Project not found on Database. Perhaps is closed", 404);
			}
			
			
			/* Check if we should add the proof forecast to the WordCount minutes of the task*/
			if($this->appendWCProofForecast){
				$proofForecast = $this->forecast(IDCPtaskType::_PR, $vars["TARIFA"]);
				$minutes= $this->wordCount->getMinute();
				//The forecast method returns hours so we should multiply to get it in minutes
				$minutes = $minutes + ($proofForecast * 60);
				$this->wordCount->setMinute($minutes);
			}
			
			
			$tasksToAddWordcount = array(IDCPtaskType::_TR, IDCPtaskType::_PR, IDCPtaskType::_PE, IDCPtaskType::_TP);
			
			$vars["PROJECTE"] =  $projectID;
			$vars["COMP_100"] = 0;
			$vars["COMP_REP"] = 0;
			$vars["COMP_FUZZY"] = 0;
			$vars["COMP_50"] = 0;
			$vars["COMP_0"] = 0;
			$vars["COMP_75"] = 0;
			$vars["COMP_85"] = 0;
			$vars["COMP_95"] = 0;
			$vars["COMP_X"] = 0;
			$transForeCast = $this->wordCount->getMinute()/60;
			if(	in_array($vars["TIPUS_TASCA"], $tasksToAddWordcount)){
				$transForeCast = $this->forecast($vars["TIPUS_TASCA"], $vars["TARIFA"], ($vars["TIPUS_TASCA"]!==IDCPtaskType::_PR)?true:false);
				$vars["COMP_100"] = ($isTep?0:$this->wordCount->getPercentage_100());
				$vars["COMP_REP"] = $this->wordCount->getRepetition();
				$vars["COMP_FUZZY"] = 0;
				$vars["COMP_50"] = $this->wordCount->getPercentage_50();
				$vars["COMP_0"] = $this->wordCount->getNotMatch();
				$vars["COMP_75"] = $this->wordCount->getPercentage_75();
				$vars["COMP_85"] = $this->wordCount->getPercentage_85();
				$vars["COMP_95"] = $this->wordCount->getPercentage_95();
				if($this->addIceWC){
					$vars["COMP_X"] = $this->wordCount->getPercentage_101();
				}
			}
			
			$vars["COMP_HORES"] = array(($this->wordCount->getMinute()/60), DBCONN_DATATYPE_FLOAT);
			$vars["HORES_PREVIST"] = array($transForeCast, DBCONN_DATATYPE_FLOAT);
			
			
			
			
			$grup = null;
			if($isGroup){
				$vars["DESTI"] = "ZZZZZZ";
				$grup = $vars["ID_GRUP"];
			}
			else{
				$vars["DESTI"] = $translator;
			}
			
			if($isTep){
				$vars["TIPUS_TASCA"] = IDCPtaskType::_TR;
			}
			
			$vars["ESTAT1"] = array(date("d/m/Y H:i:s"), DBCONN_DATATYPE_DATE);
			$i = 1;
			while($i<= (int) $vars["ESTAT"]){
				$vars["ESTAT".$i] = array(date("d/m/Y H:i:s"), DBCONN_DATATYPE_DATE);
				$i++;
			}
			$vars["INSTRUCONFIRMATION"] = "N"; //Això s'ha de revisar
			//$vars["TASCA_ORIGEN"] = ""; //Per si és TR
			$vars["FACTURAT"] = "N";
			$vars["ENTREGAT"] = "N";
			
			$fileSystemPath = self::$FILESYSTEMFOLDER.$projectID."/".date("m")."_".$this->getMonthName(date("m"));
			
			$folderName = $this->getFolderCreationName($fileSystemPath, $fileSystemFolderName, $vars["DESCRIPCIO"], $tlang->getIdLanguage());
			$folderNameWithOutPath = explode("/", $folderName);
            $folderNameWithOutPath = end($folderNameWithOutPath );
            $vars["INSTRUCCIONS"] = str_replace("%TASK_DESCRIPTION_IDCP%",$folderNameWithOutPath ,$vars["INSTRUCCIONS"]);
            $vars["INSTRUCCIONS"] = substr($vars["INSTRUCCIONS"], 0 , 1999);

			$descripcio = $this->getTaskName($fileSystemPath, $fileSystemFolderName, $vars["DESCRIPCIO"]);
			$descripcioTEP = ($isTep?"TRAD- ".$descripcio:$descripcio);
			$vars["DESCRIPCIO"] = substr($descripcioTEP, 0, 90);
			
			if($this->createTaskIntoFS && $folderName!==null){
				$taskPath = $fileSystemPath."/".$folderName;
				$taskPath = str_replace("/media/L/", "L:\\", $taskPath);
				$taskPath = str_replace("/", "\\", $taskPath);
				$vars["TASK_PATH"] = $taskPath;
				$this->createTaskIntoFS = true;
			}
			$taskID = $this->getNewTaskID();
			//Vars copy to use them after setValues.
			$varsProof = $vars;
			
			if($taskID===false){
				$this->setError("IDCP New Task ID can not be generated");
				$this->setWordCount();
				//throw new tmsException("IDCP New Task ID can not be generated" , 500);
				continue;
			}
			$vars["ID_TASCA"] = $taskID;
			
			$FTPPath = self::$FTPFOLDER.$projectID."/".date("m")."_".$this->getMonthName(date("m"));
			
			foreach ($this->filesToTranslate as $file){
			    if(is_file($file->getPath())){
                    if(filesize($file->getPath() ) >= AI::getInstance()->getSetup()->maxSizeFile) {
                        $ftp = true;
                        break;
                    }
                }
			}
			
			if(!$ftp){
				foreach ($this->filesForReference as $file){
                    if(is_file($file->getPath())) {
                        if (filesize($file->getPath()) >= AI::getInstance()->getSetup()->maxSizeFile) {
                            $ftp = true;
                            break;
                        }
                    }
				}
			}
			
			
			if($ftp){
				if(strlen($vars["INSTRUCCIONS"]) < 2000){
					$ftpPath = $FTPPath."/".$folderName;
					$ftpPath = str_replace("/media/traductemp/", "", $ftpPath);
					$ftpPath = str_replace("/", "\\", $ftpPath);
					$vars["INSTRUCCIONS"] .= "<p>FTP ROUTE:<br><b>Files for translation/refenrece can be found in our FTP (iDISC) under below path:</b><br><b>".$ftpPath."</b></p>";
					$vars["INSTRUCCIONS"] = substr($vars["INSTRUCCIONS"], 0 , 1999);
				}
			}
			
			try{
				
				//Check mandatory fields.
				//Añadir tantos campos obligatorios como sea necesario.
				$mandatoryFields = array(
				    "DESTI", // Este campo es obligatorio, aunque solo sea tarea de revisión
                );
				foreach($mandatoryFields as $mandatoryField){
					if(!isset($vars[$mandatoryField])){
						$this->setError(str_replace("%FIELD%", $mandatoryField, tmsErrors::getErrorMessage(tmsErrors::_MANDATORY_FIELD_MISSING)));
						$this->setWordCount();
						continue;
					}
				}

				self::$dbconn->setDefaultExecMode(DBCONN_TRANSACTION);
				self::$dbconn->setValues("C", "TASQUES", $vars);
				
				$this->IDCPTaskID = $taskID;
				
				$vars["HORES_PREVIST"] = 0;
				$vars["COMP_HORES"] = 0;
				
				$this->setTMSResponse($taskID);
				
				if($this->createTaskIntoFS){
					if(file_exists(self::$FILESYSTEMFOLDER.$projectID)){
						if(!file_exists($fileSystemPath)){
							if(!@mkdir($fileSystemPath,0770)){
								$this->setWarning("Can't create folder into FileSystem: The folder already exist: ".$fileSystemPath);
							}
						}
					}
					else{
						//throw new \Exception("Error creant la carpeta: El projecte ".$projectID." te la carpeta creada correctament a \"".self::$FILESYSTEMFOLDER."\"?");
						$this->setError("Can not create task. The project path :".self::$FILESYSTEMFOLDER.$projectID." does not exist");
						continue;
					}
					
					if (!file_exists($fileSystemPath."/".$folderName)){
						if(@mkdir($fileSystemPath."/".$folderName,0770,true)){
							$this->createFileSystemFolder($fileSystemPath, $folderName, $fileSystemFolderType);
							$this->writeLogFileToFS($taskID, $varsProof["DESCRIPCIO"], $varsProof["TARIFA"], $varsProof["AREA"], $varsProof["ORIGEN"], $varsProof["ORIGEN_LANG"], $varsProof["DESTI_LANG"], $grup,
							$varsProof["TIPUS_TASCA"], $translator, $proofer, $this->getDueDate(), $fileSystemPath, $folderName, $fileSystemFolderType);
						}else{
							$this->setWarning("Can't create folder into FileSystem: ".$fileSystemPath."/".$folderName);
						}
					}
					else {
						//throw new \Exception("Error creant la carpeta de la tasca: La carpeta ja existeix: ".$fileSystemPath."/".$folderName);
						$this->setWarning("Can't create folder into FileSystem: The folder already exist: ".$fileSystemPath."/".$folderName);
					}
				}
				else{
					$this->setWarning("ICP class property createTaskIntoFS is set to false. No folder into FS has been created");
				}
				
				$FTPPath = self::$FTPFOLDER.$projectID."/".date("m")."_".$this->getMonthName(date("m"));
				
					
				
				
				
				//Upload files to translate
				foreach($this->filesToTranslate as $file){
				    if(!file_exists($file->getPath())){
				        $ex1 = (new AIException("File not found"))->construct(__METHOD__,__NAMESPACE__, $func = func_get_args(), array("file" => $file));
				        Functions::logException($ex1->getMessage(), Functions::SEVERE, __CLASS__, $ex1);
				        $this->setWarning("File not found:" . $file->getPath());
				        continue;
                    }
					if(filesize($file->getPath()) >= AI::getInstance()->getSetup()->maxSizeFile){
						$this->createFolder($FTPPath."/".$folderName,"S");
						$this->moveFile($file->getPath(), $FTPPath."/".$folderName, "S",true);

					}else{
						$this->uploadFile($fileSystemPath."/".$folderName, $taskID, self::$TMS_CREATOR, $file->getPath(), "S");
						//$this->moveFile($file->getPath(), $fileSystemPath."/".$folderName, "S");
					}
                    $this->moveFile($file->getPath(), $fileSystemPath."/".$folderName, "S");
				}
				
				//Upload files for reference
				foreach($this->filesForReference as $file){
                    if(!file_exists($file->getPath())){
                        $ex1 = (new AIException("File not found"))->construct(__METHOD__,__NAMESPACE__, $func = func_get_args(), array("file" => $file));
                        Functions::logException($ex1->getMessage(), Functions::SEVERE, __CLASS__, $ex1);
                        $this->setWarning("File not found:" . $file->getPath());
                        continue;
                    }
					if(filesize($file->getPath()) >= AI::getInstance()->getSetup()->maxSizeFile){
						$this->createFolder($FTPPath."/".$folderName,"I");
						$this->moveFile($file->getPath(), $FTPPath."/".$folderName, "I",true);
                    }else{
                        $this->uploadFile($fileSystemPath."/".$folderName, $taskID, self::$TMS_CREATOR, $file->getPath(), "I");
                        //$this->moveFile($file->getPath(), $fileSystemPath."/".$folderName, "I");
                    }
                    $this->moveFile($file->getPath(), $fileSystemPath."/".$folderName, "I");
				}
				
				//Upload discarded files
				foreach($this->discardedFiles as $file){
                    if(!file_exists($file->getPath())){
                        $ex1 = (new AIException("File not found"))->construct(__METHOD__,__NAMESPACE__, $func = func_get_args(), array("file" => $file));
                        Functions::logException($ex1->getMessage(), Functions::SEVERE, __CLASS__, $ex1);
                        $this->setWarning("File not found:" . $file->getPath());
                        continue;
                    }
                    $this->moveFile($file->getPath(), $fileSystemPath."/".$folderName, "D");
                }
				
				$this->sendIDCPEmail($isGroup, ($isGroup? $varsProof["ID_GRUP"]:null), $translator, $varsProof["DESCRIPCIO"], $this->getDueDate(), $transForeCast, $taskID, $varsProof["ESTAT"]);
				
				if($isTep){
					if($isGroup){
						$varsProof["DESTI"] = "ZZZZZZ";
						//$varsProof["ID_GRUP"] = $this->idgrup;
					}
					else{
						if($proofer==null){
							$proofer = $translator;
						}
						$varsProof["DESTI"] = $proofer;
						$varsProof["ID_GRUP"] = "";
					}
					$varsProof["DESCRIPCIO"] = "PROOF- ".$descripcio;
					$varsProof["ID_TASCA"] = $this->getNewTaskID();
					$varsProof["TIPUS_TASCA"] = IDCPtaskType::_PR;
					$varsProof["INSTRUCCIONS"] = "";
					$varsProof["COMP_100"] = $this->wordCount->getPercentage_100();
					//$varsProof["INSTRUCCIONS"] = $this->instruccions;
					//$varsProof["COMP_100"] = $this->comptatge->f100;
					$varsProof["ESTAT"] = taskStatus::_ASSIGNED;
					$varsProof["HORES_PREVIST"] = array($this->forecast(IDCPtaskType::_PR, $varsProof["TARIFA"]), DBCONN_DATATYPE_FLOAT);
					$varsProof["ESTAT1"] = array(date("d/m/Y H:i:s"), DBCONN_DATATYPE_DATE);
					$varsProof["TASCA_ORIGEN"] = $taskID;
					if(isset($varsProof ["ESTAT2"])){
					    unset($varsProof ["ESTAT2"]);
                    }
					$v3 = $varsProof;
					self::$dbconn->setValues("C", "TASQUES", $varsProof);
					
					$this->sendIDCPEmail($isGroup, ($isGroup? $v3["ID_GRUP"]:null), $proofer, $v3["DESCRIPCIO"], $this->getDueDate(), $v3["HORES_PREVIST"][0], $v3["ID_TASCA"], $v3["ESTAT"]);
				}

				self::$dbconn->commit();
			}
			catch(Throwable $th){
			    $e = AIException::createInstanceFromThrowable($th)->construct(__METHOD__, __NAMESPACE__, $args = func_get_args());
			    Functions::logException("IDCP error: " . $th->getMessage(), Functions::SEVERE, __CLASS__, $e);
				$this->setError(tmsErrors::_CREATE_TMS);
				self::$dbconn->rollback();
				continue;
			}
			$this->setWordCount();
		}

		return true;
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \behaviorModule\tms\tms::getFuzziesTMS()
	 */
	public function getFuzziesTMS(){
		return self::$fuzzies;	
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see \behaviorModule\tms\tms::getFileCategoriesTMS()
	 */
	public function getFileCategoriesTMS(){
		return self::$fileCategories;
	}
	
	/**
	 *
	 * {@inheritDoc}
	 * @see \behaviorModule\tms\tms::checkifIsValidTMSUser()
	 */
	public function checkifIsValidTMSUser(String $userId){
		$userFound = false;
		$sql = "SELECT codi_recurs FROM DADES_RECURS WHERE CODI_RECURS=:USERID AND PRIORITAT<9 AND TIPUS_RECURS!='Z'";
		$q = self::$dbconn->execute($sql, array("USERID" => $userId));
		if($q->fetch()){
			$userFound = true;
		}
		$q->close();
		
		return $userFound;
	}
	
	/***
	 * Gets the name for the task folder, there are 4 posible format options:
	 * 1 No folder
	 * 2 Task name as folder name
	 * 3 Batch counter format
	 * 4 Date format YYYYMMDD
	 * 5 Datetime format YYYYMMDDHHMM
	 * @param  $fileSystemPath 
	 * @param  $fileSystemFolderName
	 * @param  $descripcio
	 * @param  $languageId
	 * @return NULL|string
	 */
	private function getFolderCreationName($fileSystemPath, $fileSystemFolderName, $descripcio, $languageId){
		$descripcio = strip_tags($descripcio);
		$descripcio = str_replace("&nbsp;", "", $descripcio);
		switch($fileSystemFolderName){
			case folderCreationName::_NO_FOLDER:
				return null;
				break;
			case folderCreationName::_FOLDER_TASK_NAME:
				return $this->normalizedFolderName($descripcio)."_".$languageId;
				break;
			case folderCreationName::_FOLDER_BATCH_COUNTER:
				$batchNFolders = glob($fileSystemPath."/Batch*", GLOB_ONLYDIR );
				$nFolderNumber = (count($batchNFolders)+1);
				//Add a leading zero if the number of folders is less than 10
				$counter = sprintf("%03d", $nFolderNumber);
				return "Batch".$counter."_".$this->normalizedFolderName($descripcio)."_".$languageId;
				break;
			case folderCreationName::_FOLDER_DATE: //YYYYMMDD
				$date = new \DateTime();
				return $date->format("Ymd")."_".$this->normalizedFolderName($descripcio)."_".$languageId;
				break;
			case folderCreationName::_FOLDER_DATETIME: //YYYYMMDDHHMM
				$date = new \DateTime();
				$date->setTimezone(new \DateTimeZone(AI::getInstance()->getSetup()->timeZone_TMS));
				return $date->format("YmdHi")."_".$this->normalizedFolderName($descripcio)."_".$languageId;
				break;
				
		}
	}
	
	public function moveFile($path,$fileSystemPath,$type,$ftp = false){
		$nom_doc = basename($path);
		if($this->createTaskIntoFS){
			if($ftp){
				if($type=="S"){
					copy($path,$fileSystemPath."/Source/".$nom_doc);
				}else if($type=="D"){
					copy($path,$fileSystemPath."/Discarded/".$nom_doc);
				}else{
					copy($path,$fileSystemPath."/Reference/".$nom_doc);
				}
			}else{
				copy($path,$fileSystemPath."/".folderName::C0."/".$nom_doc);
				if($type=="S"){
					copy($fileSystemPath."/".folderName::C0."/".$nom_doc,$fileSystemPath."/".folderName::C1."/".$nom_doc);
				}
				else if($type=="I"){
					copy($fileSystemPath."/".folderName::C0."/".$nom_doc,$fileSystemPath."/".folderName::CRF."/".$nom_doc);
				}
				else{
					copy($fileSystemPath."/".folderName::C0."/".$nom_doc,$fileSystemPath."/".folderName::CG."/".$nom_doc);
				}
			}
			
		}
	}
	
	/***
	 *  Gets the name for the task, there are 4 posible format options:
	 * 1 No folder
	 * 2 Task name as folder name
	 * 3 Batch counter format
	 * 4 Date format YYYYMMDD
	 * 5 Datetime format YYYYMMDDHHMM
	 * @param  $fileSystemPath
	 * @param  $fileSystemFolderName
	 * @param  $descripcio
	 * @return |string
	 * @return
	 */
	private function getTaskName($fileSystemPath, $fileSystemFolderName, $descripcio){
		
		$descripcio = str_replace("&nbsp;", "", $descripcio);
		switch($fileSystemFolderName){
			case folderCreationName::_NO_FOLDER:
			case folderCreationName::_FOLDER_TASK_NAME:
				return $descripcio;
				break;
			case folderCreationName::_FOLDER_BATCH_COUNTER:
				$batchNFolders = glob($fileSystemPath."/Batch*", GLOB_ONLYDIR );
				$nFolderNumber = (count($batchNFolders)+1);
				//Add two leading zeros if the number of folders is less than 100
				$counter = sprintf("%03d", $nFolderNumber);
				return "Batch".$counter."_".$descripcio;
				break;
			case folderCreationName::_FOLDER_DATE: //YYYYMMDD
				$date = new \DateTime();
				return $date->format("Ymd")."_".$descripcio;
				break;
			case folderCreationName::_FOLDER_DATETIME: //YYYYMMDDHHMM
				$date = new \DateTime();
				$date->setTimezone(new \DateTimeZone(AI::getInstance()->getSetup()->timeZone_TMS));
				return $date->format("YmdHi")."_".$descripcio;
				break;
				
		}
	}
	
	/**
	 * Gets new task ID for the TMS IDCP.
	 * @return string|boolean
	 */
	private function getNewTaskID(){
		$taskID = null;
		$sql = "select IFNULL[max(ID_TASCA)+1,1] NEWID from TASQUES";
		$rs = self::$dbconn->execute($sql);
		if($rs->fetch()){
			$taskID = $rs->getVal('NEWID');
		}
		$rs->close();
		
		return ($taskID!==null? $taskID:false);
	}
	
	/**
	 * Write a file into the FileSystem with task creation information
	 * @param integer $taskID
	 * @param string $description
	 * @param string $rate
	 * @param string $area
	 * @param string $origen
	 * @param string $slang
	 * @param string $tlang
	 * @param string $group
	 * @param string $taskType
	 * @param string $translator
	 * @param string $proofer
	 * @param \DateTime $dueDate
	 * @param string $fileSystemPath
	 * @throws \Exception
	 * @return boolean
	 */
	private function writeLogFileToFS($taskID, $description, $rate, $area, $origen, $slang, $tlang, $group, $taskType, $translator, $proofer, \DateTime $dueDate, $fileSystemPath, $folderName, $folderType){
		$output = "This is an automated generated file\r\n";
		$output .= "Use only for your information\r\n";
		$output .= "******************\r\n";
		$output .= "iDISC Information Technologies ".date("Y")."\r\n";
		$output .= "AI Project  v ".AI::getInstance()->getSetup()->version."\r\n";
		$output .= "******************\r\n\r\n";
		$output .= $description."\r\n";
		$output .= "Task Creation Date: ".date("d/m/Y G:i:s")."\r\n";
		$output .= "Task Creator: ".$origen."\r\n";
		$output .= "Task Type: ".(($taskType==IDCPtaskType::_TP)? "Translation && ProofReading": $taskType)."\r\n";
		$output .= "Source Lang: ".$slang."\r\n";
		$output .= "Target Lang: ".$tlang."\r\n";
		if($translator == "ZZZZZZ"){
			$output .= "Translator: Group Task (group Id): ".$group."\r\n";
		}
		else{
			$output .= "Translator: ".$translator."\r\n";
		}
		
		if ($taskType == IDCPtaskType::_TP){
			$output .="Reviewer: ".$proofer."\r\n";
		}
		$output .= "Deadline: ".$dueDate->setTimezone(new \DateTimeZone(AI::getInstance()->getSetup()->timeZone_TMS))->format('d/m/Y h:i:s')."\r\n";
		$output .= "Rate: ".$rate."\r\n";
		$output .= "Area: ".$area."\r\n";
		$output .= "IDCP: http://idcp.idisc.es/tasca.asp?idtasca=".$taskID."\r\n";
		if ($taskType == IDCPtaskType::_TP){
			$idtascaproof = $taskID+1;
			$output .= "IDCP Proof: http://idcp.idisc.es/tasca.asp?idtasca=".$idtascaproof."\r\n";
		}
		$output .= "Unique Source ID: ".$this->task->getUniqueSourceId()."\r\n";
		
		switch($folderType){
			case folderType::NORMAL:
				$ruta = $fileSystemPath."/".$folderName."/GESTIO/task_info.txt";
				break;
			case folderType::ESPECIAL:
				$ruta = $fileSystemPath."/".$folderName."/task_info.txt";
				break;
		}
		try{
			$fp = fopen($ruta,"w");
			if (!$fp) {
				$this->setWarning("IDCP Can not create FileSystem Log");
			}
			fwrite($fp,$output);
			fclose($fp);
		}catch (\Exception $e){
			\Functions::addLog($e->getMessage(), \Functions::ERROR, $e->getTrace());
		}
		return true;
	}
	
	/**
	 * Creates the file system task structure.
	 * @param string $fileSystemPath
	 * @param string $folderName
	 * @param string $folderType
	 * @throws tmsException
	 * @return boolean|string
	 */
	private function createFileSystemFolder($fileSystemPath, $folderName, $folderType){
		
		$taskSystemPath = $fileSystemPath."/".$folderName;
		$carpetes = array();
		switch($folderType){
			case folderType::NORMAL:
				$carpetes[0] = folderName::C0;
				$carpetes[1] = folderName::C1;
				$carpetes[2] = folderName::C2;
				$carpetes[3] = folderName::C3;
				$carpetes[4] = folderName::C4;
				$carpetes[5] = folderName::C5;
				$carpetes[6] = folderName::C6;
				$carpetes[7] = folderName::C7;
				$carpetes[8] = folderName::CG;
				$carpetes[9] = folderName::CPI;
				$carpetes[10] = folderName::CRF;
				$carpetes[11] = folderName::CTM;
				$carpetes[12] = folderName::CQ;
				break;
			case folderType::ESPECIAL:
				$carpetes[0] = "1FILES_TRANSLATED";
				$carpetes[1] = "2FILES_PROOFED";
				$carpetes[2] = "3TO_CLIENT";
				$carpetes[3] = folderName::C0;
				$carpetes[4] = folderName::CRF;
				break;
			default:
				throw new tmsException("Error trying to get IDCP folder type", 500);
		}
		
		if(!file_exists($taskSystemPath)){
			$this->setWarning("IDCP the task folder does not exist. $taskSystemPath");
			return false;
		}
		else{
			for ($i=0;$i<count($carpetes);$i++){
				if(!@mkdir($taskSystemPath."/".$carpetes[$i],0770)){
					$this->setWarning("IDCP Can not create subfolders on $taskSystemPath");
					return false;
				}
			}
		}
		return $taskSystemPath;
	}
	
	/**
	 * Upload file into the specified TMS IDCP.
	 * Returns true on success otherwise returns false. And adds a warning.
	 * @param string $fileSystemPath 
	 * @param integer $taskID
	 * @param string $origen
	 * @param string $path
	 * @param string $type
	 * @param boolean $toUpload
	 * @return boolean
	 */
	private function uploadFile($fileSystemPath, $taskID, $origen, $path,$type,$toUpload = true){
		$nom_doc = basename($path);
		
		if($type!="A"){ //Si és l'analyze no el pugem.
			if($toUpload){
				try{
					$sql = "select IFNULL[max(ID_FITXER)+1,1] NEWID from FITXERS";
					$rs = self::$dbconn->execute($sql);
					$rs->fetch();
					$idf = $rs->getVal('NEWID');
					$rs->close();
					
					$vars["ID_FITXER"] = $idf;
					$vars["TIPUS"] = $type;
					$vars["id_tasca"] = $taskID;
					$vars["CODI_RECURS"] = $origen;
					$vars["NOM"] = $nom_doc;
					$vars["DATA"] = array(date("d/m/Y H:i:s"), DBCONN_DATATYPE_DATE);
					
					self::$dbconn->setValues("C","FITXERS",$vars);
					
					//Llegir el fitxer
					$contingut = "";
					if (file_exists($path) && $path<>null) {
						$nom_arxiu_tmp = $path;
						//Llegeix el fitxer
						$fh = fopen($path, 'r');
						if (!$fh) {
							$this->setWarning("IDCP Can not read the file $path");
							return false;
						}
						$fileSize = filesize($nom_arxiu_tmp);
						if($fileSize !== 0){
                            $contingut = fread($fh, $fileSize);
                        } else {
                            $this->setWarning("IDCP Can not read the file $path");
                            return false;
                        }
						fclose($fh);
					}
					
					$vars["ID_FITXER"] = $idf;
					self::$dbconn->writeLob($contingut,"ARXIU","FITXERS","ID_FITXER=:ID_FITXER",$vars);
				}
				catch(Throwable $e){
				    $aiex = AIException::createInstanceFromThrowable($e)->construct(__METHOD__, __NAMESPACE__, $f = func_get_args());
				    Functions::logException($e->getMessage(), Functions::WARNING, __CLASS__, $aiex);
					$this->setWarning("IDCP The file $path can not be uploaded into the TMS");
					return false;
				}
				
			}
		}
		
		
		
		
		return true;
		
		//Eliminem el fitxer.
		/*try{
			unlink($path);
		}
		catch(\Exception $e) {
			
		}*/
	}
	
	/**
	 * Returns the Month name given a month number (01, ..., 12)
	 * @param string $month_number
	 * @return string
	 * @return
	 */
	private function getMonthName($month_number){
		switch($month_number){
			case "01":
				return "JANUARY";
			case "02":
				return "FEBRUARY";
			case "03":
				return "MARCH";
			case "04":
				return "APRIL";
			case "05":
				return "MAY";
			case "06":
				return "JUNE";
			case "07":
				return "JULY";
			case "08":
				return "AUGUST";
			case "09":
				return "SEPTEMBER";
			case "10":
				return "OCTOBER";
			case "11":
				return "NOVEMBER";
			case "12":
				return "DECEMBER";
		}
	}
	
	/**
	 * Normalize the folder name to be able to create it into a NTFS FileSystem.
	 * @param string $nom
	 * @return string
	 */
	private function normalizedFolderName($nom){
		$bad = array_merge(
				array_map('chr', range(0,31)),
				array("<", ">", ":", '"', "/", "\\", "|", "?", "*"));
		return str_replace($bad, "_", substr($nom,0,100));
	}

	public function calculateForecast($taskType, $rate, $isTrans = false,$excludeF100 = false){
	    return $this->forecast($taskType, $rate, $isTrans,$excludeF100);
    }
	/**
	 * Given a TMS Analyze class, returns the specific TMS calculated forecast.
	 * @param taskType $taskType
	 * @param string $rate
	 * @param boolean $isTrans
	 * @param boolean $excludeF100
	 * @return string
	 */
	private function forecast($taskType, $rate, $isTrans = false,$excludeF100 = false){
		$temps = 0;
		$punter = 0;
		$rates = array();
		if ($taskType==IDCPtaskType::_TR || $isTrans){
			if ($rate == "N") {
				$rates = array(
						"ice" =>0,
						"rep" =>1/6250,
						"f100" =>1/6250,
						"f95" => 1/1250,
						"f85" => 1/625,
						"f75" => 1/625,
						"f50" => 1/375,
						"nm" => 1/375,
				);
			}
			else{
				$rates = array(
						"ice" => 0,
						"rep" => 1/5000,
						"f100" =>  1/5000,
						"f95" => 1/1000,
						"f85" => 1/500,
						"f75" => 1/500,
						"f50" => 1/300,
						"nm" => 1/300,
				);
			}
		}
		else{
			if($rate == "N"){
				$rates = array(
						"ice" => 0,
						"rep" => 1/1250,
						"f100" =>  1/1250,
						"f95" => 1/1250,
						"f85" => 1/1250,
						"f75" => 1/1250,
						"f50" => 1/1250,
						"nm" => 1/1250,
				);
			}
			else{
				$rates = array(
						"ice" => 0,
						"rep" => 1/1000,
						"f100" =>  1/1000,
						"f95" => 1/1000,
						"f85" => 1/1000,
						"f75" => 1/1000,
						"f50" => 1/1000,
						"nm" => 1/1000,
				);
			}
		}
		$temps = $temps + $this->wordCount->getPercentage_101()*$rates["ice"];
		$temps = $temps + $this->wordCount->getRepetition()*$rates["rep"];
		if (!$excludeF100){
			$temps = $temps + $this->wordCount->getPercentage_100()*$rates["f100"];
		}
		$temps = $temps + $this->wordCount->getPercentage_95()*$rates["f95"];
		$temps = $temps + $this->wordCount->getPercentage_85()*$rates["f85"];
		$temps = $temps + $this->wordCount->getPercentage_75()*$rates["f75"];
		$temps = $temps + $this->wordCount->getPercentage_50()*$rates["f50"];
		$temps = $temps + $this->wordCount->getNotMatch()*$rates["nm"];
		$temps = $temps + ($this->wordCount->getMinute()/60);
		return number_format($temps, 2);
	}
	
	/**
	 * Sends an specific TMS IDCP Email to the resource.
	 * 
	 * @param boolean $isGroup
	 * @param int $groupID
	 * @param string $resource
	 * @param string $description
	 * @param \DateTime $dueDate
	 * @param float $forecast
	 * @param int $taskID
	 * @param int $taskStatusID
	 * @return boolean
	 * @return
	 */
	private function sendIDCPEmail($isGroup, $groupID, $resource, $description, \DateTime $dueDate, $forecast, $taskID, $taskStatusID){
		
		$statusName = taskStatus::getStatusName($taskStatusID);
		/**
		 *
		 * En aquest cas, les tasques van al grup de Welocalize, en funció del grup, mirarem quins usuaris són membres i cercarem
		 * els emails.
		 */
		$title = "IDCP Task [Status $statusName] ".substr($description,0,60)." - Deliver: ".$dueDate->setTimezone(new \DateTimeZone(AI::getInstance()->getSetup()->timeZone_TMS))->format('d/m/Y H:i:s')." - Forecast: ".$forecast."h.";
		$body = "There is a task (http://idcp.idisc.es/tasca.asp?idtasca=".$taskID.") ";
		$body.="assigned to you that is $statusName for translation.<br />";
		$body.="Task description: ".$description.".<br />";
		$body.="Please go to http://idcp.idisc.es and login using your ID and password.<br />";
		$body.="Your ID and password will have been provided to you by the project PM.<br />";
		$body.="The password can be changed at any time.<br />";
		$body.="Once you have downloaded the file to the system, select the Down Arrow to put the task into the next WORKING status.<br />";
		$body.="By doing this, the PM will know that you are working on the file(s).<br />";
		$body.="You can address the project PM normally by E-mail.<br />";
		$body.="Best regards\r\n\r\n\r\nIDCP Automatic Manager Response<br />";
		$body.="iDisc Information Technologies, S.L.<br />";
		$body.="Telf (+34) 93 778 73 00\r\nFax (+34) 93 778 35 80<br />Web: http://www.idisc.es<br />";
		$body.='"The Worldwide Spanish Localization Company"';
		
		$mail = new EmailManager();
		
		if($isGroup){
			$sql = "select email,nom from dades_recurs where codi_recurs in (select recurs from grups_usuaris  where id_grup=:GROUPID)";
			$rs = self::$dbconn->execute($sql, array("GROUPID" => $groupID));
			$emails_grup = array();
			while($rs->fetch()){
				$mail->addAddress($rs->getVal("email"), $rs->getVal("nom"));
				$mail->sendMail("", $title, $body);
			}
			$rs->close();
		}
		else{
			$sql = "SELECT EMAIL,NOM from dades_recurs where codi_recurs=:USERID";
			$rs = self::$dbconn->execute($sql, array("USERID" => $resource));
			if($rs->fetch()){
				$mail->addAddress($rs->getVal("EMAIL"));
				$mail->sendMail("", $title, $body);
			}
			$rs->close();
		}
		return true;
	}
	
	/**
	 * Given a BM Language class, returns the specific TMS IDCP Language value.
	 * If the language is not found on the IDCP TMS, a warning will be generated and a default TMS language (Spanish (Iberian)) will be set.
	 * @param Language $language
	 * @return string
	 */
	private static function getTmsLanguage($language){
		switch($language->getIdLanguage()){
			case "af":
			case "af-ZA":
				return "Afrikaans";
			case "ar":
			case "ar-AE":
			case "ar-BH":
			case "ar-DZ":
			case "ar-EG":
			case "ar-IQ":
			case "ar-JO":
			case "ar-KW":
			case "ar-LB":
			case "ar-LY":
			case "ar-MA":
			case "ar-OM":
			case "ar-QA":
			case "ar-SA":
			case "ar-SY":
			case "ar-TN":
			case "ar-YE":
				return "Arabic";
			case "ay":
				return "Aymara";
			case "az":
			case "az-AZ":
			case "az-Cyrl-AZ":
				return "Azery";
			case "eu":
			case "eu-ES":
				return "Basque";
			case "be":
			case "be-BY":
				return "Belorussian";
			case "bg":
			case "bg-BG":
				return "Bulgarian";
			case "ca":
			case "ca-ES":
				return "Catalan";
			case "zh":
			case "zh-HK":
			case "zh-MO":
			case "zh-CN":
				return "Chinese (Simplified)";
			case "zh-SG":
			case "zh-TW":
				return "Chinese(Traditional)";
			case "hr":
			case "hr-BA":
				return "Croat";
			case "hr-HR":
			case "cs":
			case "cs-CZ":
				return "Czech";
			case "da":
			case "da-DK":
				return "Danish";
			case "nl":
			case "nl-BE":
			case "nl-NL":
				return "Dutch";
			case "en":
			case "en-AU":
			case "en-BZ":
			case "en-CA":
			case "en-CB":
			case "en-IE":
			case "en-JM":
			case "en-NZ":
			case "en-PH":
			case "en-ZA":
			case "en-TT":
			case "en-ZW":
			case "en-GB":
			case "en-US":
				return "English";
				//return "English (UK)";
				//return "English (US)";
			case "et":
			case "et-EE":
				return "Estonian";
			case "fi":
			case "fi-FI":
				return "Finnish";
			case "fr":
			case "fr-FR":
			case "fr-LU":
			case "fr-CH":
				return "French";
			case "fr-BE":
				return "Flemish";
			case "fr-CA":
				return "French (Canadian)";
			case "fr-MC":
				return "French (Morocco)";
			case "gl":
			case "gl-ES":
				return "Galician";
			case "de":
			case "de-AT":
			case "de-DE":
			case "de-LI":
			case "de-LU":
			case "de-CH":
				return "German";
			case "el":
			case "el-GR":
				return "Greek";
			case "he":
			case "he-IL":
				return "Hebrew";
			case "hu":
			case "hu-HU":
				return "Hungarian";
			case "is-IS":
				return "Icelandic";
			case "id":
			case "id-ID":
				return "Indonesian";
			case "it":
			case "it-IT":
			case "it-CH":
				return "Italian";
			case "ja":
			case "ja-JP":
				return "Japanese";
			case "ko":
			case "ko-KR":
				return "Korean";
			case "lv":
			case "lv-LV":
				return "Latvian";
			case "lt":
			case "lt-LT":
				return "Lithuanian";
			case "ms":
			case "ms-BN":
			case "ms-MY":
				return "Malay";
			case "mt":
			case "mt-MT":
				return "Maltese";
			case "nb":
			case "nb-NO":
			case "nn-NO":
				return "Norwegian";
			case "pl":
			case "pl-PL":
				return "Polish";
			case "pt":
				return "Portuguese (SOURCE)";
			case "pt-BR":
				return "Portuguese (Brazil)";
			case "pt-PT":
				return "Portuguese (Iberian)";
            case "qu":
			case "qu-BO":
			case "qu-EC":
			case "qu-PE":
				return "Quechua";
            case "quc":
                return "Quiche";
			case "ro":
			case "ro-RO":
				return "Romanian";
			case "ru":
			case "ru-RU":
				return "Rusian";
			case "sr-Cyrl-BA":
			case "sr-Cyrl-SP":
			case "sr-BA":
			case "sr-SP":
				return "Serbian";
			case "sk":
			case "sk-SK":
				return "Slovak";
			case "sl":
			case "sl-SI":
				return "Slovenian";
				
				//Spanish (US)
			case "es-BO":
			case "es-CL":
			case "es-CO":
			case "es-CR":
			case "es-DO":
			case "es-EC":
			case "es-SV":
			case "es-GT":
			case "es-HN":
			case "es-NI":
			case "es-PA":
			case "es-PY":
			case "es-PE":
			case "es-PR":
			case "es-UY":
			case "es-VE":
			case "es-AR":
			case "es-LatAm":
            case "es-MX":
            case "es-LA":
				return "Spanish (LA)";
			case "es-Intl":
            case "es-UN":
				return "Spanish (Global)";
			case "es":
			case "es-ES":
				return "Spanish (Iberian)";
			case "vl-ES":
			case "va":
				return "Valencian";
			case "mj-ES":
				return "Majorcan";
			case "sv":
			case "sv-FI":
			case "sv-SE":
				return "Swedish";
			case "tl":
			case "tl-PH":
				return "Tagalog";
			case "ta":
			case "ta-IN":
				return "Tamil";
			case "th":
			case "th-TH":
				return "Thai";
			case "tr":
			case "tr-TR":
				return "Turkish";
			case "uk":
			case "uk-UA":
				return "Ukranian";
			case "ur":
			case "ur-PK":
				return "Urdu";
			case "vi":
			case "vi-VN":
				return "Vietnamese";
			case "cac":
				return "Chuj";
			case "nhd":
			case "gn":
			case "gnw":
			case "gug":
			case "gui":
			case "gun":
				return "Guaraní";
			case "my":
			case "Mymr":
			case "obr":
				return "Burmese";
			case "hi":
				return "Hindi";
			case "cak":
				return "Kaqchikel";
			case "Khmr":
				return "Khmer";
			case "lo":
				return "Lao";
			case "la":
				return "Latin";
			case "mam":
				return "Mam";
			case "myn":
				return "Maya";
			case "nah":
				return "Nahuatl";
			case "nsp":
				return "Nepalese";
			case "pes":
				return "Persian";
			case "gd":
				return "Scottish";
			case "wo":
				return "Wolof";
			default :
                $aiex = (new AIException("Language not found. Using default Spanish (Iberian)"))->construct(__METHOD__, __NAMESPACE__, $f = func_get_args(), array(
                    "language" => $language
                ));
                Functions::logException($aiex->getMessage(), Functions::INFO, __CLASS__, $aiex);
                return null;
				//return "Spanish (Iberian)";
		}
	}
	
}


/**
 * Specific TMS IDCP Task Status types.
* @author phidalgo
* @version 1.0
* created on 14 jul. 2018
 */
abstract class taskStatus{
	const _ASSIGNED = 1;
	const _READY = 2;
	const _WORKING = 3;
	const _DELIVERED = 4;
	const _RECEIVED = 5;
	const _CLOSED = 6;
	
	public static function getStatusName($statusID){
		switch ($statusID){
			case self::_ASSIGNED:
				return "ASSIGNED";
			case self::_READY:
				return "READY";
			case self::_WORKING:
				return "WORKING";
			case self::_DELIVERED:
				return "DELIVERED";
			case self::_RECEIVED:
				return "RECEIVED";
			case self::_CLOSED:
				return "CLOSED";
		}
	}
}

/**
 * Specific TMS IDCP Task Types.
* @author phidalgo
* @version 1.0
* created on 14 jul. 2018
 */
abstract class IDCPtaskType {
	
	/**
	 * Tasca tipus TP (TRAD & REV)
	 */
	Const _TP = "TP";
	/**
	 * Tasca tipus Translation
	 */
	Const _TR = "Translation";
	
	/**
	 * Tasca tipus ProofReading
	 */
	Const _PR = "ProofReading";
	
	/**
	 * Tasca Tipus PostEditing
	 */
	Const _PE = "PostEditing";
	
	/**
	 * Tasca Tipus Miscellaneous
	 */
	Const _MIS = "Miscellaneous";
	
	/**
	 * Tasca Tipus Management
	 */
	Const _MNG = "Management";
	
	/**
	 * Tasca Tipus Engineering
	 */
	Const _ENG = "Engineering";
	
	/**
	 * Tasca Tipus Engineering
	 */
	Const _DTP = "DTP";
}


/**
 * Specific TMS IDCP FileSystem folder types.
* @author phidalgo
* @version 1.0
* created on 14 jul. 2018
 */
abstract class folderType{
	const NORMAL = 0;
	const ESPECIAL = 1;
}


/**
 * Specific TMS IDCP FileSystem folder setup for each task.
* @author phidalgo
* @version 1.0
* created on 14 jul. 2018
 */
abstract class folderName{
	const C0 = "0FILES_FROM_CLIENT";
	const C1 = "1FILES_TO_TRANS";
	const C2 = "2FILES_PRETRANS";
	const C3 = "3FILES_TO_IDCP";
	const C4 = "4FILES_TRANSLATED";
	const C5 = "5FILES_PROOFED";
	const C6 = "6FINAL_FILES";
	const C7 = "7FILES_TO_CLIENT";
	const CG = "GESTIO";
	const CPI = "PROJECT_INSTRUCTIONS";
	const CRF = "REFERENCE_FILES";
	const CTM = "TM";
	const CQ = "QUERIES";
}

abstract class folderCreationName{
	const _NO_FOLDER = 1;
	const _FOLDER_TASK_NAME = 2;
	const _FOLDER_BATCH_COUNTER = 3;
	const _FOLDER_DATE = 4;
	const _FOLDER_DATETIME = 5;
}
?>