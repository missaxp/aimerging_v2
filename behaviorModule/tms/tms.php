<?php
namespace behaviorModule\tms;

use model\Analysis;
use dataAccess\dao\LanguageDAO;
use model\File;
use model\Task;
use core\AI;
use model\Language;

/**
* General TMS class.
* Used as a connector between BM and TMS.
* This class has a general methods and is extended by a specific TMS class.
* 
* 
* @author phidalgo
* @version 1.0
* created on 12 jul. 2018
*
*
* @method boolean setTitle() setTitle($value) set titles value
* @method string||bolean getTitle() getTitle() get titles value
* @method boolean setDescription() setDescription($value) set Description value
* @method string||bolean getDescription() getDescription() get Description value
* @method boolean setSourceLanguage() setSourceLanguage(Language $value) set SourceLanguage
* @method Language||boolean getSourceLanguage() getSourceLanguage() get SourceLanguage
* @method boolean setTargetLanguages() setTargetLanguages(Language[] $value) set TargetLanguages
* @method Language[] getTargetLanguages() getTargetLanguages() get TargetLanguages
* @method boolean setDueDate() setDueDate(\DateTime $value) set DueDate
* @method \DateTime||boolean getDueDate() getDueDate() get DueDate
 */
class tms {
	/**
	 * Array of TMS errors
	 * @var array
	 */
	private $errors = array();
	
	/**
	 * Array of TMS warnings
	 * @var array
	 */
	private $warnings = array();
	
	
	/**
	 * This array contains TMS specific properties and also the tms property value if set.
	 * @var tmsProperty[]
	 */
	protected static $tmsProperties = null;
	
	/**
	 * List of Languages
	 * @var Language[]
	 */
	protected static $languages = null;
	
	/**
	 * Task title
	 * @var string
	 */
	protected $title;
	
	/**
	 * Task description (instructions)
	 * @var string
	 */
	protected $description;
	
	/**
	 * Task source language
	 * @var Language
	 */
	protected $sourceLanguage;
	
	/**
	 * Array of task target languages.
	 * @var Language[]
	 */
	protected $targetLanguages;
	
	/**
	 * Task due date.
	 * @var \DateTime
	 */
	protected $dueDate;
	
	/**
	 * Task word count.
	 * @var Analysis
	 */
	protected $wordCount = null;
	
	/**
	 * Array of Files to translate 
	 * @var File[]
	 */
	protected $filesToTranslate = array();
	
	/**
	 * Array of files for reference.
	 * @var File[]
	 */
	protected $filesForReference = array();
	
	/**
	 * Array of discarded files
	 * @var File[]
	 */
	protected $discardedFiles = array();
	
	/**
	 * The harvest task object.
	 * @var Task
	 */
	protected $task;
	
	/**
	 * Environment.
	 * @var boolean
	 */
	protected $isDevelopment;
	
	/**
	 * Enables append time of Proof to task
	 * @var boolean
	 */
	protected $appendWCProofForecast = false;
	
	/**
	 * Append ICE words to Wordcount
	 * @var boolean
	 */
	protected $addIceWC = false;
	/**
	 * TMS Response messages.
	 * @var array
	 */
	private $TMSResponse = array();
	
	public function __construct(Task $task, $development){
		$this->isDevelopment = $development;
		if($this->isDevelopment==null){
			$this->isDevelopment == AI::getInstance()->isDev();
		}
		if(self::$languages==null){
			$ldao = new LanguageDAO();
			self::$languages = $ldao->getAll();
		}
		
		$this->task = $task;
	}
	
	public function __call($methodName, $args){
		if(substr($methodName, 0, 3) =="get" && count($args)==0){
			$property = lcfirst (preg_replace("/get/","" , $methodName, 1));
			if(property_exists($this, $property)){
				return $this->$property;
			}
			throw new tmsException("TMS basic Property $property not exist", 500);
		}
		else if(substr($methodName, 0, 3)=="set" && count($args)==1){
			$property = lcfirst (preg_replace("/set/","" , $methodName, 1));
			if(property_exists($this, $property)){
				$this->$property = $args[0];
				return true;
			}
			throw new tmsException("TMS basic Property $property not exist", 500);
		}
	}
	
	/**
     * @return boolean
     */
    public function isAppendWCProofForecast()
    {
        return $this->appendWCProofForecast;
    }

	/**
     * @param boolean $appendWCProofForecast
     */
    public function setAppendWCProofForecast($appendWCProofForecast)
    {
        $this->appendWCProofForecast = $appendWCProofForecast;
    }

	/**
     * @return boolean
     */
    public function getAddIceWC()
    {
        return $this->addIceWC;
    }

	/**
     * @param boolean $addIceWC
     */
    public function setAddIceWC($addIceWC)
    {
        $this->addIceWC = $addIceWC;
    }

	protected function setTMSResponse($string){
		$this->TMSResponse[] = $string;
		
	}
	
	public function getTMSResponse(){
		return $this->TMSResponse;
	}
	
	
	/**
	 * Set the task wordcount.
	 * @param Analysis $analyze
	 * @return boolean
	 * @return
	 */
	public function setWordCount(Analysis $analyze = null){
		$this->wordCount = $analyze;
		return true;
		
	}
	
	/**
	 * Creates a task into the specified TMS. 
	 * @return boolean
	 */
	public function create(){
		return true;
	}
	
	
	/**
	 * Sets files for translate 
	 * @param File[] $files
	 * @return boolean
	 */
	public function setFilesToTranslate($files){
		foreach($files as $file){
			if($file->getSuccessfulDownload()){
				$this->filesToTranslate[] = $file;
			}
		}
		return true;	
	}

	
	/**
	 * Sets files for translate 
	 * @param File[] $files
	 * @return boolean
	 */
	public function setDiscardedFiles($files){
		foreach($files as $file){
				$this->discardedFiles[] = $file;
		}
		return true;	
	}
	
	/**
	 * Sets files for reference
	 * @param File[] $files
	 * @return boolean
	 */
	public function setFilesForReference($files){
		foreach($files as $file){
			if($file->getSuccessfulDownload()){
				$this->filesForReference[] = $file;
			}
		}
		return true;
	}
	
	/**
	 * Returns all the TMS Specific properties.  
	 * @return \behaviorModule\tms\tmsProperty[]
	 */
	public function getTmsProperties(){
		return self::$tmsProperties;
	}
	
	/**
	 * Set tms specific property 
	 * @param string $propName
	 * @param string $propValue
	 * @throws tmsException
	 * @return boolean
	 */
	public function setProperty($propName, $propValue) {
		$allowedPropertyValue = false;
		foreach(self::$tmsProperties as &$tmsProperty) {
			if($tmsProperty->getName()==$propName){
				if($tmsProperty->getAllowedValues()!=null) {
					foreach($tmsProperty->getAllowedValues()  as $pValue){
						if($pValue->getId()==$propValue){
							$allowedPropertyValue = true;
							break;
						}
					}
				}
				else{
					$allowedPropertyValue = true;
				}
				
				if(!$allowedPropertyValue){
					throw new tmsException("Set ".AI::getInstance()->getSetup()->default_TMS." Property $propName , value : $propValue is not allowed, allowed values in list", 403);
				}
				
				$tmsProperty->setValue($propValue);
				return true;
			}
		}
		
		
		throw new tmsException("Set ".AI::getInstance()->getSetup()->default_TMS." Property $propName is not allowed", 403);
		return false;
	}
	
	/**
	 * Returns if warnings are generated
	 * @return array
	 */
	public function getWarning(){
		return $this->warnings;
	}
	
	/**
	 * Adds a TMS warning 
	 * @param string $warning
	 * @return
	 */
	protected function setWarning($warning){
		$this->warnings[] = $warning;
	}
	
	/**
	 * Get array of errors.
	 * @return array
	 * @return
	 */
	public function getError(){
		return $this->errors;
	}
	
	/**
	 * Adds a TMS error
	 * @param string $error
	 * @return
	 */
	protected function setError($error){
		$this->errors[] = $error;
	}
	
	/**
	 * Check if has warnings
	 * @return boolean
	 */
	public function hasWarnings(){
		return (count($this->warnings)>0);
	}
	
	/**
	 * Check if has errors
	 * @return boolean
	 */
	public function hasErrors(){
		return (count($this->errors)>0);
	}


	/**
	 * Return de fuzzie's name for the selected TMS
	 */
	public function getFuzziesTMS(){
		return array();
	}

	/**
	 *Returns the name of the category of the files
	 */

	public function getFileCategoriesTMS(){
		return array();
	}

	/**
	 * Check if given User is as valid User ID from TMS.
	 * @param String $userId
	 * @return boolean
	 */
	public function checkifIsValidTMSUser(String $userId){
		return true;
	}
	
}

/**
 * 
* @author phidalgo
* @version 1.0
* created on 12 jul. 2018
 */
class tmsProperty {
	//Var type Constants
	const _INT = "int";
	const _FLOAT = "float";
	const _CHAR = "char";
	const _STRING = "string";
	const _DATE = "date";
	
	/**
	 * Property name
	 * @var string
	 */
	public $name = "";
	
	/**
	 * Property type (see available type constants).
	 * @var string
	 */
	private $type = null;
	
	/**
	 * The set value 
	 * @var string
	 */
	private $value = null;
	
	/**
	 * 
	 * @var [tmsPropertyValues]|mixed
	 */
	public $allowedValues = null;
	
	/**
	 * Determines if this property is mandatory (default false)
	 * @var boolean
	 */
	private $required;
	
	public function __construct($name, $type, $allowedValues = null, $required = false){
		if($type != self::_INT && $type != self::_FLOAT && $type != self::_CHAR && $type != self::_STRING && $type != self::_DATE){
			throw new tmsException("TMS property type $type not allowed", 500);
		}
		
		$this->name = $name;
		$this->type = $type;
		$this->allowedValues = $allowedValues;
		$this->required = $required;	
	}

	/**
	 * Gets property name
	 * @return string
	 * @return
	 */
	public function getName(){
		return $this->name;
	}
	
	/**
	 * Gets property variable type
	 * @return string
	 */
	public function getType(){
		return $this->type;
	}
	
	/**
	 * Return tms Allowed Properties.
	 * @return tmsPropertyValues[]
	 */
	public function getAllowedValues(){
		return $this->allowedValues;
	}
	
	/**
	 * Returns if this property has value or not. 
	 * @return boolean
	 */
	public function hasValue(){
		return ($this->value!==null);
	}
	
	/**
	 * Returns if this property is mandatory or not.
	 * @return boolean
	 */
	public function getRequired(){
		return $this->required;
	}
	
	/**
	 * Returns the set value or false if no value is set.
	 * @return boolean
	 * @return string
	 */
	public function getValue(){
		return ($this->value!==null? $this->value:false);
	}
	
	/**
	 * Sets the property value
	 * @param string $value
	 * @return boolean
	 */
	public function setValue($value){
		$this->value = $value;
		return true;
	}
}

/**
 * 
 * This class is used to generate the allowed Values for the tmsProperty class, if an allowed list of values is needed.
* @author phidalgo
* @version 1.0
* created on 12 jul. 2018
 */
class tmsPropertyValues{
	/**
	 * Property value ID.
	 * @var string
	 */
	public $id;
	
	/**
	 * Property value.
	 * @var string
	 */
	public $name;
	
	public function __construct($id, $value){
		$this->id = $id;
		$this->name = $value;
	}
	
	/**
	 * Ger property value ID
	 * @return string
	 * @return
	 */
	public function getId(){
		return $this->id;
	}
	
	/**
	 * Get property value value
	 * @return string
	 * @return
	 */
	public function getValue(){
		return $this->name;
	}
}

/**
 * An abstract class used to generate a list of general TMS error codes
* @author phidalgo
* @version 1.0
* created on 16 jul. 2018
 */
abstract class tmsErrors {
	const _CREATE_TMS = 1;
	
	const _PROJECT_ID_CAN_NOT_BE_NULL = 2;
	
	const _PROJECT_ID_NOT_FOUND = 3;
	
	const _MANDATORY_FIELD_MISSING = 4;
	
	public static function getErrorMessage($errorCode){
		switch($errorCode){

            case self::_PROJECT_ID_CAN_NOT_BE_NULL:
				return "2- Project id can not be null";
            case self::_PROJECT_ID_NOT_FOUND:
				return "3- Project %PROJECT_ID% not found on Database. Perhaps is closed";
            case self::_MANDATORY_FIELD_MISSING:
				return "3- Mandatory field %FIELD% not found.";
            case self::_CREATE_TMS:
            default:
                return "1- Unknown error";
        }
	}
}

/**
 * TMS Specific Exception class.
* @author phidalgo
* @version 1.0
* created on 16 jul. 2018
 */
class tmsException extends \Exception{
	public function __construct($message, $code){
		parent::__construct($message, $code);
	}
}
?>