<?php
namespace behaviorModule\tms\idcp\resources;

use dataAccess\dbConn;

class resourceAvailability {
	const _PHP_DATE_MONDAY = 1;
	const _PHP_DATE_TUESDAY = 2;
	const _PHP_DATE_WEDNESDAY = 3;
	const _PHP_DATE_THURSDAY = 4;
	const _PHP_DATE_FRIDAY = 5;
	const _PHP_DATE_SATURDAY = 6;
	const _PHP_DATE_SUNDAY = 7;
	
	/**
	 * Database Connection
	 * @var dbConn
	 */
	private $dbcon;
	
	/**
	 * TMS Resource ID
	 * @var string
	 */
	private $TMSResourceId;
	
	/**
	 * TMS Resource Role ID
	 * @var string
	 */
	private $TMSResourceRoleId;
	
	/**
	 * In case of we have to offer our best deadline, if we can offer dates where company office is close 
	 * (i.e. weekends). 
	 * @var boolean
	 */
	private $deliverWhenOfficeIsClosed = false;
	
	private $resourceWorkingHoursPerDay = null;
	
	
	/**
	 * Set if we can assign deliver dates on tasks on days that the company office is closed
	 * @param Bool $set
	 * @return boolean
	 */
	public function setDeliverWhenOfficeIsClosed(Bool $set){
		$this->deliverWhenOfficeIsClosed = $set;
		return true;
	}
	
	/**
	 * Get if we can assign deliver dates on tasks on days that the company office is closed
	 * @return boolean
	 */
	public function getDeliverWhenOfficeIsClosed(){
		return $this->deliverWhenOfficeIsClosed;
	}
	
	/**
	 * Determines if the Company office is open on the argument day. 
	 * @param \DateTime $date
	 * @return boolean
	 */
	public function companyOfficeIsOpenThisDay(\DateTime $date){
		if(!$this->deliverWhenOfficeIsClosed){
			if(in_array($date->format("N"), array(self::_PHP_DATE_SATURDAY, self::_PHP_DATE_SUNDAY))){
				return false;
			}
		}
		//TODO:: -	Configuració de compañía per a establir paràmetres de diez d’obertura i tancament per oferir i proposar deadlines.
		return true;
	}
	
	
	/**
	 * 
	 * @param dbConn $dbcon
	 * @param String $TMSResourceId
	 * @param String $TMSResourceRoleId
	 * @return
	 */
	public function __construct(dbConn $dbcon, String $TMSResourceId, String $TMSResourceRoleId){
		$this->dbcon = $dbcon;
		$this->TMSResourceId = $TMSResourceId;
		$this->TMSResourceRoleId = $TMSResourceRoleId;
	}
	
	/**
	 * Resource available days of week to work.
	 * @return number[]
	 * @return
	 */
	public function workingDays(){
		//TODO:: Mirar en configuración AI los dias disponibles que este recurso trabaja.
		//7 checkbox uno por dia de la semana
		//[x]	Monday
		//[x]	Tuesday
		//[x] 	...
		//[ ]	Saturday
		//[ ]	Sunday
		
		///$sql  ="SELECT ... FROM AI.... WHERE TMSRESOURCE=:TMSRESOURCEID";
		
		//DateTime->format("N") devuelve [1, ..., 7] 1=monday, 7=sunday
		return array(1,2,3,4,5);
	}
	
	/**
	 * Check if work on the argument day 
	 * @param \DateTime $date
	 * @return boolean
	 */
	public function workThisDay(\DateTime $date){
		return (in_array($date->format("N"), $this->workingDays()));
	}
	
	/**
	 * Return if this resource work on weekends. 
	 * @return boolean
	 */
	public function workOnWeekend(){
		$wd = $this->workingDays();
		return (count(array_intersect($wd, array(self::_PHP_DATE_SATURDAY, self::_PHP_DATE_SUNDAY))) > 0);
	}
	
	/**
	 * Return if resource is in holydays
	 * @return boolean
	 */
	public function isInHolyDays(){
		//TODO:: Mirar en configuración AI los dias disponibles que este recurso trabaja.
		//TODO Un selector entre fechas para determinar periodos vacacionales.
		//Between 20190801 and 20190831 HOLYDAYS.
		return false;
	}
	
	/**
	 * If this resource is available to do the job on time. 
	 * @param Float $taskForecast
	 * @param \DateTime $startDate
	 * @param \DateTime $deadline
	 * @throws ResourceAvailabilityException
	 * @return boolean
	 */
	public function isAvailableToWorkOn(Float $taskForecast, \DateTime $startDate, \DateTime $deadline){
		if($deadline<$startDate){
			throw new ResourceAvailabilityException("Deadline can not be lower than start date", 415);
		}
		
		$dDiff = $startDate->diff($deadline);
		
		$dateDiffDays = $dDiff->format("%r%a");
		
		if(!is_numeric($dateDiffDays)){
			throw new ResourceAvailabilityException("DateDiff does not return a valid day difference", 409);
		}
		
		$availableHours = 0;
		
		$dayAvailability = $this->getTimeFrame($startDate, new \DateInterval("P".$dateDiffDays."D"));
		foreach($dayAvailability as $resourceDay){
			if($this->workThisDay($resourceDay->day) && !$this->isInHolyDays()){
				$dayAvailableTime = $this->workingHoursPerDay() - $resourceDay->assignedWorkHours;
				if($dayAvailableTime>0){
					$availableHours += $dayAvailableTime;
				}
			}
		}
		
		return ($availableHours>=$taskForecast);
	}
	
	/**
	 * The best deadline that we can offer on a job done by this resource.
	 * @param Float $taskForecast
	 * @param \DateTime $startDate
	 */
	public function getBestDeadline(Float $taskForecast, \DateTime $startDate){
		$availableHours = 0;
		$dayAvailability = null;
		$timeBuffer = clone $startDate;
		$lastDayNeedWorkHours = $taskForecast;
		$doingJob = true;
		do {
			$dayAvailability = $this->getDay($timeBuffer);
			$dayFreeTime = ($this->workingHoursPerDay() - $dayAvailability->assignedWorkHours);
			if($this->workThisDay($dayAvailability->day) && !$this->isInHolyDays() && $dayFreeTime>0){
				$availableHours += $dayFreeTime;
				$doingJob = ($availableHours<=$taskForecast || !$this->companyOfficeIsOpenThisDay($dayAvailability->day));
				if($doingJob){
					$lastDayNeedWorkHours -= $dayFreeTime;
				}
			}
			$timeBuffer->add(new \DateInterval("P1D"));
		}
		while($doingJob); //Mientras no haya horas suficientes para realizar la tarea o la oficina esté cerrada...
		
		//Añadimos a las horas que le quedan el último dia, las horas que ya tiene asignadas
		$lastDayJobs = $this->getDay($dayAvailability->day);
		$lastDayNeedWorkHours+= $lastDayJobs->assignedWorkHours;
		
		
		if((9+$lastDayNeedWorkHours)==9){
			$dayAvailability->day->setTime(9, 00, 00);
		}
		else if((9+$lastDayNeedWorkHours)<12){
			$dayAvailability->day->setTime(12, 00, 00);
		}
		else if((9+$lastDayNeedWorkHours)<14){
			$dayAvailability->day->setTime(14, 00, 00);
		}
		else if((9+$lastDayNeedWorkHours)<16){
			$dayAvailability->day->setTime(16, 00, 00);
		}
		else{
			$dayAvailability->day->setTime(17, 00, 00);
		}
		
		return $dayAvailability->day;
	}
	

	
	/**
	 * Resource Working hours per day 
	 * @return number
	 */
	public function workingHoursPerDay(){
		if($this->resourceWorkingHoursPerDay==null){
			//TODO:: Mirar de AI.
			$this->resourceWorkingHoursPerDay =  (float) 8.0;
		}
		return $this->resourceWorkingHoursPerDay;
	}
	
	/**
	 * Returns a resource workload on a specific day.
	 * @param \DateTime $day
	 * @return ResourceDay
	 */
	public function getDay(\DateTime $day){
		return $this->getTimeFrame($day, new \DateInterval("P0D"))[$day->format("Ymd")];
	}
	
	/**
	 * Returns resource workload on a specific time frame 
	 * @param \DateTime $fromTime
	 * @param \DateInterval $timeFrame
	 * @throws ResourceAvailabilityException
	 * @return ResourceDay[]
	 */
	public function getTimeFrame(\DateTime $fromTime, \DateInterval $timeFrame = null){
		if($timeFrame==null){
			$timeFrame = new \DateInterval("P7D");
		}
		
		$resourceDay = array();
		
		$toTime = clone $fromTime;
		$toTime->add($timeFrame);
		
		$vars["FROMTIME"] = $fromTime->format("Ymd H:i:s");
		$vars["TMSRESOURCEID"] = $this->TMSResourceId;
		$vars["TOTIME"] = $toTime->format("Ymd 23:59:59");
		
		
		//Obtiene todas las tareas del recurso entre las fechas seleccionadas y devuelve el numero de tareas por dia
		//y la suma de horas previstas (forecast)
		
		$sql = "SELECT to_char(final_previst,'YYYY-MM-DD') as DAY, Count(1) as TASK_COUNTER, sum(hores_previst) as ASSIGNEDWORKHOURS
		FROM Tasques_all
		WHERE final_previst BETWEEN to_date(:FROMTIME, 'YYYY-MM-DD HH24:MI:SS') AND to_date(:TOTIME, 'YYYY-MM-DD HH24:MI:SS')
		AND desti=:TMSRESOURCEID and tipus_tasca in ('Translation', 'PostEditing', 'ProofReading')
		GROUP by to_char(final_previst,'YYYY-MM-DD')
		HAVING Count(1) > 1
		order by to_char(final_previst,'YYYY-MM-DD') desc";
		
		$timeBuffer = clone $fromTime;
		do {
			$tmpBuff = clone $timeBuffer;
			$resourceDay[$tmpBuff->format("Ymd")] = new ResourceDay($this->dbcon, $tmpBuff, 0, 0);
			if(!$this->workOnWeekend() && $resourceDay[$tmpBuff->format("Ymd")]->isTodayWeekend()){
				$resourceDay[$tmpBuff->format("Ymd")] = new ResourceDay($this->dbcon, $tmpBuff, 0, $this->workingHoursPerDay());
			}
			$timeBuffer = $timeBuffer->add(new \DateInterval("P1D"));
			$tmpBuff = null;
		}
		while($timeBuffer<$toTime);
		
		$timeBuffer = null;
		
		try {
			$q = $this->dbcon->execute($sql, $vars);
			
			while($q->fetch()){
				$sql = "SELECT id_tasca  FROM Tasques_all
		  		WHERE trunc(final_previst)=to_date(:THEDAY, 'YYYY-MM-DD')
		  		AND desti=:TMSRESOURCEID and tipus_tasca in ('Translation', 'PostEditing', 'ProofReading')";
				$q2 = $this->dbcon->execute($sql, array("TMSRESOURCEID" => $this->TMSResourceId, "THEDAY" => $q->DAY));
				$taskids = array();
				while($q2->fetch()){
					$taskids[] = $q2->id_tasca;
				}
				$q2->close();
				
				$resourceDayDate = \DateTime::createFromFormat("Y-m-d h:i:s", $q->DAY." 00:00:00");
				
				$resourceDay[$resourceDayDate->format("Ymd")] = new ResourceDay($this->dbcon, clone $resourceDayDate, 
						(int) $q->TASK_COUNTER, floatval(str_replace(',', '.', str_replace('.', '', $q->ASSIGNEDWORKHOURS))), $taskids);
				
				if(!$this->workOnWeekend() && $resourceDay[$resourceDayDate->format("Ymd")]->isTodayWeekend()){
					$resourceDay[$resourceDayDate->format("Ymd")] = new ResourceDay($this->dbcon, clone $resourceDayDate, 0, $this->workingHoursPerDay());
				}
			}
			$q->close();
		}
		catch(\Exception $e){
			throw new ResourceAvailabilityException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
		catch(\dbconn\SQLException $e){
			throw new ResourceAvailabilityException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
		
		return $resourceDay;
	}
}

class ResourceDay {
	/**
	 * Day
	 * @var \DateTime
	 */
	public $day;
	/**
	 * Number of tasks on this day.
	 * @var integer
	 */
	public $taskCounter;
	/**
	 * Assigned work hours on this day
	 * @var float
	 */
	public $assignedWorkHours;
	
	/**
	 * Determine if day is weekend. 
	 * @var boolean
	 */
	public $isWeekend;
	
	/**
	 * Array of tasks.
	 * @var Task[]
	 */
	private $tasks;
	
	public function __construct(Dbconn &$dbcon, \DateTime $day, Int $taskCounter, Float $forecast, $taskids = null){
		$this->day = $day;
		$this->taskCounter = $taskCounter;
		$this->assignedWorkHours = $forecast;
		$this->isWeekend = $this->isTodayWeekend($day);
		
		if($taskids!=null){
			foreach($taskids as $tid){
				$this->tasks[] = new Task($dbcon, $tid);
			}
		}
	}
	
	/**
	 * Check if current date is weekend.
	 * @param \DateTime $date
	 * @return boolean
	 * @return
	 */
	public function isTodayWeekend() {
		return ($this->day->format("N")>= 6);
	}
}

/**
 * TMS IDCP Task 
 * @author phidalgo
 *
 */
class Task {
	/**
	 * Task ID - Primary Key, uniqueIdentifier
	 * @var integer
	 */
	private $id_tasca;
	/**
	 * TMSResourceID user where the task comes from
	 * @var string
	 */
	private $origen;
	/**
	 * TMSResourceId - Task Destination - the user who will do the task
	 * @var string
	 */
	private $desti;
	
	/**
	 * TMS Specific category system. Project to group similar tasks (p.ex.: Same customer, same final client, same language combination, etc).
	 * @var string
	 */
	private $projecte;
	
	/**
	 * Date time when it is supposed to start the task
	 * @var \DateTime
	 */
	private $inici_previst;
	/**
	 * DEADLINE - Date time when it is supposed to end the task
	 * @var \DateTime
	 */
	private $final_previst;
	/**
	 * Task Type - Could be one of the following ones: 
	 * PostEditing, DTP, Miscelaneous, LanguageLead, Management, Miscellaneous, Translation, Quality, TEST, ProofReading, Spotcheck, Engineering
	 * 
	 * Most used: Translation, ProofReading, PostEditing
	 * @var string
	 */
	private $tipus_tasca;
	/**
	 * Word Purchase Price type. Can be N or X
	 * TMSResourceId has two different prices. One for N translation and one for X translation. 
	 * @var string
	 */
	private $tarifa;
	
	/**
	 * Task Source Language. Can be one of SELECT CODI FROM IDIOMES;
	 * @var string
	 */
	private $origen_lang;
	/**
	 * Task Target Language. Can be one of SELECT CODI FROM IDIOMES;
	 * Only ONE task Target Language is allowed on this TMS.
	 * @var string
	 */
	private $desti_lang;
	
	/**
	 * Task Forecast. Number of hours needed to finish the task.
	 * @var float
	 */
	private $hores_previst;
	
	/**
	 * Task Status. Indicates the current status of the task.
	 * The task change status history date is found on estat1, estat2, estat3, estat4,estat5,estat6 db fields. 
	 * 
	 * Could be one of the following:
	 * 1 - Assigned
	 * 2 - Ready
	 * 3 - Working
	 * 4 - Delivered
	 * 5 - Received
	 * 6 - Closed
	 * @var integer
	 */
	private $estat;
	/**
	 * WC - 100% Matches
	 * @var integer
	 */
	private $comp_100;
	/**
	 * WC - REPETITION Matches
	 * @var integer
	 */
	private $comp_rep;
	/**
	 * WC - FUZZY Matches
	 * @var integer
	 */
	private $comp_fuzzy;
	/**
	 * WC - NEW WORDS Matches
	 * @var integer
	 */
	private $comp_0;
	/**
	 * WC - Additional HOURS (2 decimals)
	 * @var float
	 */
	private $comp_hores;
	/**
	 * Task Instructions
	 * @var string
	 */
	private $instruccions;
	/**
	 * Task Notes
	 * @var string
	 */
	private $notes;
	/**
	 * Task Date state 1 (ASSIGNED) change
	 * @var \DateTime
	 */
	private $estat1;
	/**
	 * Task Date state 2 (READY) change
	 * @var \DateTime
	 */
	private $estat2;
	/**
	 * Task Date state 3 (WORKING) change
	 * @var \DateTime
	 */
	private $estat3;
	/**
	 * Task Date state 4 (DELIVERED) change
	 * @var \DateTime
	 */
	private $estat4;
	/**
	 * Task Date state 5 (RECEIVED) change
	 * @var \DateTime
	 */
	private $estat5;
	/**
	 * Task Date state 6 (CLOSED) change
	 * @var \DateTime
	 */
	private $estat6;
	/**
	 * TITLE - Task Title
	 * @var string
	 */
	private $descripcio;
	/**
	 * DGEST Subproject CODE when task is STATUS=6 (CLOSED) and is IMPUTED 
	 * @var string
	 */
	private $codi_subprojecte;
	/**
	 * DGEST task CODE.
	 * @var string
	 */
	private $codi_tasca;
	/**
	 * DGEST article CODE.
	 * @var string
	 */
	private $codi_article;
	/**
	 * N/D
	 * @var string
	 */
	private $qau;
	/**
	 * N/D
	 * @var string
	 */
	private $qah;
	/**
	 * N/D
	 * @var string
	 */
	private $qal;
	/**
	 * N/D
	 * @var string
	 */
	private $qaf;
	/**
	 * WC - 50% Matches
	 * @var integer
	 */
	private $comp_50;
	/**
	 * WC - 75% Matches
	 * @var integer
	 */
	private $comp_75;
	/**
	 * WC - 85% Matches
	 * @var integer
	 */
	private $comp_85;
	/**
	 * WC - 95% Matches
	 * @var integer
	 */
	private $comp_95;
	/**
	 * WC - 101%/ICE Matches
	 * @var integer
	 */
	private $comp_x;
	/**
	 * TMSResourceId - The user who has CREATED the task.
	 * @var string
	 */
	private $creador;
	/**
	 * First area of the task (select CODI from AREAS)
	 * @var string
	 */
	private $area;
	/**
	 * Second area of the task (select CODI from AREAS)
	 * @var string
	 */
	private $area2;
	
	/**
	 * If the project instructions has been accepted (checkbox on the UI). On DB Can be N/S 
	 * @var boolean
	 */
	private $instruconfirmation;
	
	/**
	 * QA - PASS/FAIL
	 * TODO:: Hay que revisar exactamente qué implica.
	 * @var string
	 */
	private $qa_comments;
	/**
	 * QA - ND
	 * @var string
	 */
	private $qa_excel1;
	/**
	 * QA - ND
	 * @var string
	 */
	private $qa_excel2;
	/**
	 * QA - ND
	 * @var string
	 */
	private $qa_observa;
	/**
	 * QA - ND
	 * @var string
	 */
	private $nom_excel1;
	/**
	 * QA - ND
	 * @var string
	 */
	private $nom_excel2;
	/**
	 * TASK_ID - Relation to another Task ID.
	 * Only filled if the parent 
	 * @var string
	 */
	private $tasca_origen;
	/**
	 * If task is billed. On DB values N/S
	 * @var boolean
	 */
	private $facturat;
	/**
	 * If task is delivered to client. On DB values N/S
	 * @var boolean
	 */
	private $entregat;
	/**
	 * ND
	 * @var integer
	 */
	private $valoracio;
	/**
	 * ND - Not used i think.
	 * This field is filled on project_task.asp when a user saves the form... but i am not sure.
	 * @var \DateTime
	 */
	private $data_entrega;
	/**
	 * Just for filter purposes.
	 * @var boolean
	 */
	private $marcar;
	/**
	 * ND
	 * @var string
	 */
	private $preu_paraula;
	/**
	 * ND
	 * @var string
	 */
	private $preu_hora;
	/**
	 * If the task is part of a group. A group is a pull of tasks where some resources can get and assign itself.
	 * If this value is null, no grup.
	 * I think is not too much used now.
	 * @var integer
	 */
	private $id_grup;
	/**
	 * Where the task is assigned to the grup (i am not sure, on DB appears a date and the field is called asigna_grup so i think is the date where 
	 * the task is assigned to the group but 
	 * @var \DateTime
	 */
	private $asigna_grup;
	/**
	 * If task has automatic route or not. (on DB is N/S)
	 * If is a translation task and this value has true and has a proofreading task relationated with tasca_origen, when
	 * the translation task status equals 4 (DELIVERED), automatically the proofreading task will be set to status 2 (READY)
	 * @var boolean 
	 */
	private $route_automatic;
	/**
	 * If task has to be done using CATTool Logoport (i think now this field is not used). on DB N/S
	 * @var boolean
	 */
	private $logoport;
	/**
	 * TWS User in case of this task uses CATTool Translation Workspace.
	 * @var string
	 */
	private $tws_user;
	/**
	 * If this task is a Translation and has a relation with a PROOF TASK, the proofer has to punctuate the overall quality
	 * of the translation by giving, from 1 to 10 the right value.
	 * @var integer
	 */
	private $proof_valoracio;
	/**
	 * If the user has passed the spellcheck on this task.
	 * This is filled when the user change the state from 3 (WORKING to 4 (DELIVERED) and asks if has passed the spellcheck.
	 * On DB N/S
	 * @var boolean 
	 */
	private $spellcheck;
	
	/**
	 * In case of the user does not pass the spellcheck, the reason why will be here.
	 * @var string
	 */
	private $spellcomments;
	/**
	 * ND 
	 * @var string
	 */
	private $qanonaproved;
	/**
	 * ND
	 * @var string
	 */
	private $memoq_lic;
	/**
	 * FileSystem path where the task and its files are stored.
	 * @var string
	 */
	private $task_path;
	/**
	 * Task Creation date
	 * @var \DateTime
	 */
	private $creation_date;
	/**
	 * If the task is imputed or not. On DB N/S
	 * @var boolean
	 */
	private $tasca_imputada;
	
	public function __construct(dbConn &$dbcon, Int $task_id){
		$sql = "SELECT * FROM TASQUES_ALL WHERE ID_TASCA=:TASK_ID";
		$q = $dbcon->execute($sql, array("TASK_ID" => $task_id));
		$row = $q->getRow();
		foreach($row as $property => $value){
			if(isset($this->$property)){
				$this->$property = $value;
			}
		}
		$q->close();
		
	}
	
}


class ResourceAvailabilityException extends \Exception{
	public function __construct($message=null, $code=null, $previous=null){
		parent::__construct($message, $code, $previous);
	}
}