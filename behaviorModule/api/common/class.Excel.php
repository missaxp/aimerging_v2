<?php

namespace common;

use core\Environment;
use core\security\SecurityFeatures;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;

abstract class Excel {
	protected static $environment;
	
	
	public static function createXLSfromData($data,$fname = "export"){
		self::$environment = Environment::getInstance ();
		// Removes the R__ column added on the recordset
		foreach ( $data as &$row ) {
			unset ($row ["r__"]);
		}
		
		$filename = "";
		if(self::$environment->authentication_required){
			$filename = self::$environment->user->username;
		}
		else{
			$filename = SecurityFeatures::SU_UNAME;
		}
		$filename .= "_".$fname."_".time().".xlsx";
		
		require_once BaseDirAPI .'/libs/Spout/Autoloader/autoload.php';
		
		$writer = WriterFactory::create(Type::XLSX); // for XLSX files
		$writer->openToFile(self::$environment->tmpDir.$filename); // write data to a file or to a PHP stream
		
		$style = new StyleBuilder();
		$style = $style->setFontBold()->setFontSize(15)->setFontColor(Color::BLUE)->setShouldWrapText()->build();		

		$titles = array();
		foreach ( $data [0] as $key => $value ) {
			$titles[] = $key;
		}
		$writer->addRowWithStyle($titles,$style);
		unset($titles);
		unset($style);
		
		foreach ( $data as $values ) {
			$rows = array();
			foreach ($values as $value) {
				$rows[] = $value;
			}
			$writer->addRow($rows);
		}
		$writer->close();
		return $filename;
	}
	
	public static function createMaecPO() {
		self::$environment = Environment::getInstance ();
		
		$filename = self::$environment->user->username . "_po.xls";
		
		require_once BaseDirAPI . "/libs/excel/PHPExcel.php";
		require_once BaseDirAPI . "/libs/excel/PHPExcel/IOFactory.php";
		$objPHPExcel = new \PHPExcel ();
		$objReader = \PHPExcel_IOFactory::createReader ( "Excel5" );
		$objPHPExcel->setActiveSheetIndex ( 0 );
		
		// Borde blanc desde A1 fins a S45
		$objPHPExcel->getActiveSheet ()->getStyle ( 'A1:S45' )->applyFromArray ( array (
				'borders' => array (
						'allborders' => array (
								'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
								'color' => array (
										'rgb' => 'FFFFFF' 
								) 
						) 
				) 
		) );
		
		// Fulla activa background color FF003300 (ARGB)
		$objPHPExcel->getDefaultStyle ()->applyFromArray ( array (
				'fill' => array (
						'type' => \PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array (
								'argb' => 'FF003300' 
						) 
				) 
		) );
		
		$boldCentrat = array (
				'font' => array (
						'bold' => true,
						'size' => 8,
						'name' => 'Arial' 
				),
				'alignment' => array (
						'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER 
				) 
		);
		
		// Amaguem columnes.
		$objPHPExcel->getActiveSheet ()->getColumnDimension ( "D" )->setVisible ( false );
		$objPHPExcel->getActiveSheet ()->getColumnDimension ( "E" )->setVisible ( false );
		$objPHPExcel->getActiveSheet ()->getColumnDimension ( "J" )->setVisible ( false );
		$objPHPExcel->getActiveSheet ()->getColumnDimension ( "K" )->setVisible ( false );
		$objPHPExcel->getActiveSheet ()->getColumnDimension ( "L" )->setVisible ( false );
		$objPHPExcel->getActiveSheet ()->getColumnDimension ( "M" )->setVisible ( false );
		$objPHPExcel->getActiveSheet ()->getColumnDimension ( "N" )->setVisible ( false );
		$objPHPExcel->getActiveSheet ()->getColumnDimension ( "O" )->setVisible ( false );
		$objPHPExcel->getActiveSheet ()->getColumnDimension ( "I" )->setVisible ( false );
		
		// Juntem files.
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'B10:B11' );
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'C10:C11' );
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'F10:F11' );
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'G10:G11' );
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'H10:H11' );
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'I10:I11' );
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'P10:P11' );
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'Q10:Q11' );
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'R10:R11' );
		
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'Q4:R5' ); // Casella fecha
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "Q4", now () );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'Q4:R5' )->applyFromArray ( $boldCentrat );
		
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'C2:I5' ); // Casella Información Organización
		
		$objPHPExcel->getActiveSheet ()->getColumnDimension ( 'C' )->setWidth ( 50 ); // Posem ample a columna C
		$objPHPExcel->getActiveSheet ()->getColumnDimension ( 'H' )->setWidth ( 15 ); // Posem ample a columna H
		$objPHPExcel->getActiveSheet ()->getColumnDimension ( 'F' )->setWidth ( 20 ); // Posem ample a columna F
		$objPHPExcel->getActiveSheet ()->getColumnDimension ( 'G' )->setWidth ( 20 ); // Posem ample a columna G
		                                                                              // Borde negre desde Q4 fins a R5 i color FFFFCC a background.
		$objPHPExcel->getActiveSheet ()->getStyle ( 'Q4:R5' )->applyFromArray ( array (
				'borders' => array (
						'allborders' => array (
								'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
								'color' => array (
										'rgb' => '000000' 
								) 
						) 
				),
				'fill' => array (
						'type' => \PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array (
								'rgb' => 'FFFFCC' 
						) 
				) 
		) );
		
		// Escribim titols.
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "B10", "Tipo (1)" );
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "C10", "Titulo documento" );
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "F10", "Fecha recepción" );
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "G10", "Fecha entrega" );
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "H10", "Total palabras traducidas" );
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "P10", "Idiomas (2)" );
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "Q10", "Tarifa" );
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "R10", "Total €" );
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "Q3", "Fecha" );
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "C2", "iDISC Information Technologies S.L." );
		
		// Border "dashed" de B a R TODO: NO VA
		$objPHPExcel->getActiveSheet ()->getStyle ( 'B10:R11' )->applyFromArray ( array (
				'borders' => array (
						'allborders' => array (
								'style' => \PHPExcel_Style_Border::BORDER_DASHED,
								'color' => array (
										'rgb' => '000000' 
								) 
						) 
				) 
		) );
		
		$objPHPExcel->getActiveSheet ()->getStyle ( 'B10:C11' )->applyFromArray ( $boldCentrat );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'Q10:R11' )->applyFromArray ( $boldCentrat );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'C2:I5' )->applyFromArray ( $boldCentrat ); // Titulo Información Organización
		
		$boldCentrat ["font"] ["bold"] = false;
		$objPHPExcel->getActiveSheet ()->getStyle ( 'F10:P11' )->applyFromArray ( $boldCentrat );
		$boldCentrat ["font"] ["bold"] = true;
		
		$objPHPExcel->getActiveSheet ()->getStyle ( 'F10:F11' )->getAlignment ()->setWrapText ( true );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'G10:G11' )->getAlignment ()->setWrapText ( true );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'H10:H11' )->getAlignment ()->setWrapText ( true );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'P10:P11' )->getAlignment ()->setWrapText ( true );
		
		$sql = "SELECT (t.comp_0+t.comp_50+t.comp_75+t.comp_85+t.comp_95+t.comp_100+t.comp_rep) as wc, t.descripcio as descripcio, dt.data as data_recepcio, t.estat4 as data_entrega,t.desti_lang,'N' as tarifa, '1.00' as preu,dt.fitxer_email as data FROM tasques t, tasques_entrades dt WHERE dt.id_tasca is not null and dt.id_tasca=t.id_tasca and dt.estat='PROCESSADA' and dt.client='MAEC'";
		
		$rs = self::$environment->dbcon_webtraduc->execute ( $sql );
		$fetch = $rs->getAll ();
		$rs->close ();
		$fila = 12;
		$total_task_price = 0;
		$total_words = 0;
		foreach ( $fetch as $task ) {
			
			if ($task ["DESTI_LANG"] == "French") {
				$lang = "FR";
			} elseif ($task ["DESTI_LANG"] == "English") {
				$lang = "EN";
			} else {
				$lang = "UN";
			}
			
			$data = json_decode ( $task ["DATA"], true );
			$task_price = 1;
			$task_words = 159;
			$objPHPExcel->getActiveSheet ()->SetCellValue ( "B" . $fila, $data ["type"] );
			$objPHPExcel->getActiveSheet ()->SetCellValue ( "C" . $fila, $task ["DESCRIPCIO"] );
			$objPHPExcel->getActiveSheet ()->SetCellValue ( "F" . $fila, $task ["DATA_RECEPCIO"] );
			$objPHPExcel->getActiveSheet ()->SetCellValue ( "G" . $fila, $task ["DATA_ENTREGA"] );
			$objPHPExcel->getActiveSheet ()->SetCellValue ( "H" . $fila, $task ["WC"] );
			$objPHPExcel->getActiveSheet ()->SetCellValue ( "P" . $fila, $lang );
			$objPHPExcel->getActiveSheet ()->SetCellValue ( "Q" . $fila, $task ["TARIFA"] );
			$objPHPExcel->getActiveSheet ()->SetCellValue ( "R" . $fila, round ( $task ["PREU"], 2 ) );
			$total_task_price += $task ["PREU"];
			$total_words += $task ["WC"];
			$fila ++;
		}
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "R" . $fila, round ( $total_task_price, 2 ) );
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "R" . ($fila + 1), round ( ($total_task_price * 0.21), 2 ) );
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "R" . ($fila + 2), round ( ($total_task_price * 1.21), 2 ) );
		
		$objPHPExcel->getActiveSheet ()->getStyle ( 'B' . $fila . ':R' . $fila )->applyFromArray ( array (
				'borders' => array (
						'top' => array (
								'style' => \PHPExcel_Style_Border::BORDER_DOTTED,
								'color' => array (
										'rgb' => '000000' 
								) 
						) 
				) 
		) );
		
		$objPHPExcel->getActiveSheet ()->getStyle ( 'B9:R9' )->applyFromArray ( array (
				'borders' => array (
						'bottom' => array (
								'style' => \PHPExcel_Style_Border::BORDER_DOTTED,
								'color' => array (
										'rgb' => '000000' 
								) 
						) 
				) 
		) );
		
		$objPHPExcel->getActiveSheet ()->getStyle ( 'B9:B' . $fila )->applyFromArray ( array (
				'borders' => array (
						'left' => array (
								'style' => \PHPExcel_Style_Border::BORDER_DOTTED,
								'color' => array (
										'rgb' => '000000' 
								) 
						) 
				) 
		) );
		
		$fila ++;
		
		$filaInicial = $fila;
		
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'B' . $fila . ':C' . $fila );
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "B" . $fila, "(1)     CM- Comunicado        (2)  EN- inglés" );
		$fila ++;
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'B' . $fila . ':C' . $fila );
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "B" . $fila, " NP- Nota de prensa           FR- francés" );
		$fila ++;
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'B' . $fila . ':C' . $fila );
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "B" . $fila, "  OT- Otro                             CA- catalán" );
		$fila ++;
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "C" . $fila, "                                  GA- gallego" );
		$fila ++;
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "C" . $fila, "                                   EU- euskera" );
		$fila ++;
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "C" . $fila, "                                   DE- alemán" );
		
		$borderDashed = array (
				'borders' => array (
						'allborders' => array (
								'style' => \PHPExcel_Style_Border::BORDER_DOTTED,
								'color' => array (
										'rgb' => '000000' 
								) 
						) 
				) 
		);
		
		$objPHPExcel->getActiveSheet ()->getStyle ( 'B12:R' . ($filaInicial - 2) )->applyFromArray ( $borderDashed );
		
		$arial10bold = array (
				'font' => array (
						'bold' => true,
						'size' => 10,
						'name' => 'Arial' 
				),
				'alignment' => array (
						'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER 
				) 
		);
		
		$objPHPExcel->getActiveSheet ()->getStyle ( 'B' . $filaInicial . ':C' . $fila )->applyFromArray ( $arial10bold );
		
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "P" . $filaInicial, "IVA" );
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "Q" . $filaInicial, "21%" );
		
		$boldCentrat ["font"] ["bold"] = false;
		$objPHPExcel->getActiveSheet ()->getStyle ( 'P' . $filaInicial . ':Q' . $filaInicial )->applyFromArray ( $boldCentrat );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'B12:R' . ($filaInicial - 2) )->applyFromArray ( $boldCentrat );
		$boldCentrat ["font"] ["bold"] = true;
		
		// Borde negre desde Q4 fins a R5 i color FFFFCC a background.
		$objPHPExcel->getActiveSheet ()->getStyle ( 'R12:R' . ($filaInicial + 1) )->applyFromArray ( array (
				'fill' => array (
						'type' => \PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array (
								'rgb' => 'FFFFCC' 
						) 
				) 
		) );
		
		$objPHPExcel->getActiveSheet ()->SetCellValue ( "Q" . ($filaInicial + 1), "TOTAL" );
		
		$objPHPExcel->getActiveSheet ()->getStyle ( 'Q' . ($filaInicial + 1) )->applyFromArray ( array (
				'font' => array (
						'bold' => true,
						'size' => 10,
						'name' => 'Arial' 
				) 
		) ); // Total amb bold
		     
		// Borde negre desde Q4 fins a R5 i color FFFFCC a background.
		$objPHPExcel->getActiveSheet ()->mergeCells ( 'P' . ($filaInicial + 2) . ':R' . ($filaInicial + 5) );
		$objPHPExcel->getActiveSheet ()->getStyle ( 'P' . ($filaInicial + 2) . ':R' . ($filaInicial + 5) )->applyFromArray ( array (
				'fill' => array (
						'type' => \PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array (
								'rgb' => 'FFFFCC' 
						) 
				),
				'borders' => array (
						'outline' => array (
								'style' => \PHPExcel_Style_Border::BORDER_DOTTED,
								'color' => array (
										'rgb' => '000000' 
								) 
						) 
				) 
		) );
		
		$objWriter = \PHPExcel_IOFactory::createWriter ( $objPHPExcel, "Excel5" );
		$objWriter->save ( $_SERVER ["DOCUMENT_ROOT"] . "/tmp/" . $filename );
		return $filename;
	}
}