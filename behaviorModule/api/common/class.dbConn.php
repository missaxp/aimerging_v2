<?php

namespace dbconn;

use core\Exception\OracleException;
use core\Exception\AppException;
use core\Exception\MyException;
/*
 * File: dbConn.php
 * @author Carles Temporal
 */

/* Constants */
// define ( "DBCONN_ORACLE", "ORACLE" );
// define ( "DBCONN_MYSQL", "MYSQL" );

// define ( "DBCONN_PERSISTENT", 1 );
// define ( "DBCONN_STANDARD", 2 );

// define ( "DBCONN_COMMIT_ON_SUCCESS", "C" );
// define ( "DBCONN_TRANSACTION", "T" );

// define ( "DBCONN_DADES", "DADES" );
// define ( "DBCONN_FITXER", "FITXER" );

// define ( "DBCONN_INSERT", "C" );
// define ( "DBCONN_UPDATE", "M" );

// define ( "DBCONN_DATATYPE_DATE", "D" );
// define ( "DBCONN_DATATYPE_FLOAT", "F" );


/**
 * Excepció SQL
 */
class SQLException extends \Exception {
	public function __construct($msg){
		parent::__construct($msg);
	}
	
}



/**
 * ****** Extén la classe mysqli per tal de poguer fer un fetch_assoc ********
 */
/**
 * ****** en les consules amb paràmetres.
 * ********
 */
class mysqli_Extended extends \mysqli {
	protected $selfReference;
	public function __construct($dbHost, $dbUsername, $dbPassword, $dbDatabase) {
		parent::__construct ( $dbHost, $dbUsername, $dbPassword, $dbDatabase );
	}
	/*public function prepare($query) {
		$stmt = new stmt_Extended ( $this, $query );
		return $stmt;
	}*/
}
class stmt_Extended extends \mysqli_stmt {
	protected $varsBound = false;
	protected $results;
	public function __construct($link, $query) {
		parent::__construct ( $link, $query );
	}
	
	/**
	 * 
	 * @return unknown|NULL
	 */
	public function fetch_assoc(){
		$results = array();
		// checks to see if the variables have been bound, this is so that when
		// using a while ($row = $this->stmt->fetch_assoc()) loop the following
		// code is only executed the first time
		if (! $this->varsBound) {
			$meta = $this->result_metadata ();
			while ( $column = $meta->fetch_field () ) {
				// this is to stop a syntax error if a column name has a space in
				// e.g. "This Column". 'Typer85 at gmail dot com' pointed this out
				$columnName = mb_strtoupper ( str_replace ( ' ', '_', $column->name ), "UTF-8" );
				$bindVarArray [] = &$this->results [$columnName];
			}
			call_user_func_array ( array (
					$this,
					'bind_result'
			), $bindVarArray );
			$this->varsBound = true;
		}
		
		if ($this->fetch () != null) {
			// this is a hack. The problem is that the array $this->results is full
			// of references not actual data, therefore when doing the following:
			// while ($row = $this->stmt->fetch_assoc()) {
			// $results[] = $row;
			// }
			// $results[0], $results[1], etc, were all references and pointed to
			// the last dataset
			foreach ( $this->results as $k => $v ) {
				$results[$k] = $v;
			}
			return $results;
		} else {
			return null;
		}
	}
	
	
}
/**
 * ****** Fi de la classe extesa ********
 */

/**
 * ***************************************
 * Classe per la conexió
 * ***************************************
 */
class dbConn {
	private static $conexions;
	public static $tipus = DBCONN_PERSISTENT; // DBCONN_STANDARD o DBCONN_PERSISTENT
	public $default_execmode = DBCONN_COMMIT_ON_SUCCESS;
	public $provider;
	public $user;
	private $pswd;
	public $db;
	public $host;
	public $conn;
	public $row;
	public $charset;
	const CONNECTION_ERROR = 1;
	const SQLEXECUTION_ERROR = 2;
	
	/**
	 * Set how to handle exceptions. If as errors or not.
	 * @var boolean
	 */
	private $exceptionasError = true;
	
	public function ExceptionAsError($bool = true){
		$this->exceptionasError = $bool;
	}
	
	// translation table
	private static $dbTransTable = array (
			"\[BEGINCALL\]((.)*)\[ENDCALL\]" => array (
					DBCONN_ORACLE => "begin \\1 end;",
					DBCONN_MYSQL => "call \\1" 
			),
			"DATE\[C\]" => array (
					DBCONN_ORACLE => "SYSDATE",
					DBCONN_MYSQL => "NOW()" 
			),
			"OUTDATE\[([A-Za-z0-9_().]+)\]" => array (
					DBCONN_ORACLE => "TO_CHAR(\\1,yyyy-mm-dd')",
					DBCONN_MYSQL => "DATE_FORMAT(\\1,'%Y-%m-%d')" 
			),
			"OUTDATETIME\[([A-Za-z0-9_().]+)\]" => array (
					DBCONN_ORACLE => "TO_CHAR(\\1,'YYYYMMDD\"T\"hh24miss\"Z\"')",
					DBCONN_MYSQL => "DATE_FORMAT(\\1,'%Y/%m/%d %T')" 
			),
			"OUTFLOAT\[([A-Za-z0-9_().]+)\]" => array (
					DBCONN_ORACLE => "TO_CHAR(\\1,'999G999G990D00')",
					DBCONN_MYSQL => "\\1" 
			),
			"INDATE\['([0-9\/]+)\']" => array (
					DBCONN_ORACLE => "TO_DATE('\\1','yyyy-mm-dd')",
					DBCONN_MYSQL => "str_to_date('\\1', '%Y-%m-%d')" 
			),
			"INDATETIME\['([0-9\/ :]+)\']" => array (
					DBCONN_ORACLE => "TO_DATE('\\1','yyyy-mm-dd hh24:mi:ss')",
					DBCONN_MYSQL => "str_to_date('\\1', '%Y-%m-%d %T')" 
			),
			"IFNULL\[([A-Za-z0-9_() \+\-:,.*']+)\]" => array (
					DBCONN_ORACLE => "nvl(\\1)",
					DBCONN_MYSQL => "IFNULL(\\1)" 
			),
					/*"CONTAINS\[((.)+)[ ]*,[ ]*(([^\]])*)\]"=>
									  	array(DBCONN_ORACLE=>"CONTAINS(\\1,\\3,1)>0",
									  		  DBCONN_MYSQL=>"MATCH(\\1) AGAINST (\\3 IN NATURAL LANGUAGE MODE)"),
					*/
					"CONTAINS\[([A-Za-z0-9_.]+)[ ]*,[ ]*(([A-Za-z0-9_.:])*)\]" => array (
					DBCONN_ORACLE => "CONTAINS(\\1,\\2,1)>0",
					DBCONN_MYSQL => "MATCH(\\1) AGAINST (\\2 IN NATURAL LANGUAGE MODE)" 
			),
			"LOBLENGTH\[([A-Za-z0-9_.]+)\]" => array (
					DBCONN_ORACLE => "dbms_lob.getlength(\\1)",
					DBCONN_MYSQL => "OCTET_LENGTH(\\1)" 
			),
			"\[RANDOM\]" => array (
					DBCONN_ORACLE => "dbms_random.value",
					DBCONN_MYSQL => "RAND()" 
			)
									  		  
					/*
					"ID[%s]"=>			array(DBCONN_ORACLE=>"SELECT %s.CURRVAL FROM DUAL",
									  	DBCONN_MYSQL=>"SELECT LAST_INSERT_ID()"),
					"NEWID[%s],"=>		array(DBCONN_ORACLE=>"%s.NEXTVAL,",
									  	DBCONN_MYSQL=>"null,")
					*/
			);
	
	/**
	 * Constructor de la classe
	 */
	function __construct($provider = "", $user = "", $pswd = "", $host = "", $db = "", $charset = "UTF8") {
		if ($provider == "") {
			if (isset ( self::$conexions )) {
				$this->provider = self::$conexions ["PROVIDER"];
				$this->user = self::$conexions ["USER"];
				$this->pswd = self::$conexions ["PASSW"];
				$this->host = self::$conexions ["HOST"];
				$this->db = self::$conexions ["DB"];
				$this->charset = self::$conexions ["CHARSET"];
			} else
				throw new SQLException ( 'No s\'han definit els paràmetres de conexió.', self::CONNECTION_ERROR );
		} else {
			$this->provider = $provider;
			$this->user = $user;
			$this->pswd = $pswd;
			$this->db = $db;
			$this->host = $host;
			$this->charset = $charset;
		}
		
		// Dos intents per problemes amb la primera consulta del POOL
		try {
			$this->getConn ();
		} catch ( \Exception $e ) {
			$this->getConn ();
		}
	}
	
	/**
	 * Destructor de la classe
	 */
	function __destruct() {
		$this->close ();
	}
	
	/*
	 * Funció estàtica per establir el paràmetres de conexió i que quedin guardats en la classe.
	 */
	static function setConnex($provider, $user, $pswd, $host, $db = "", $charset = "UTF8") {
		self::$conexions = array (
				"PROVIDER" => $provider,
				"USER" => $user,
				"PASSW" => $pswd,
				"HOST" => $host,
				"DB" => $db,
				"CHARSET" => $charset 
		);
	}
	
	/**
	 * Obté la cadena tractada per realizar la consulta
	 *
	 * @param string $sql        	
	 * @param int $provider
	 *        	DBCONN_ORACLE,DBCONN_MYSQL
	 */
	static function tractaSQL($sql, $provider = null) {
		if (is_null ( $provider ) && isset ( $this ))
			$provider = $this->provider;
		else if (is_null ( $provider ))
			$provider = DBCONN_ORACLE;
		
		$sql = trim ( $sql );
		
		foreach ( self::$dbTransTable as $key => $value ) {
			$sql = preg_replace ( '/' . $key . '/i', $value [$provider], $sql );
		}
		
		switch ($provider) {
			// Oracle
			case DBCONN_ORACLE :
				
				// Reemplaça els LEFT JOIN pel format d'Oracle:
				// Utilitzar taula1 alias1 LEFT JOIN taula2 alias2 ON camptaula1=camptaula2
				// $sql = preg_replace('/([ ,])([a-zA-Z0-9_ ]+)[ ]+([a-zA-Z0-9_]+)[ ]+LEFT JOIN[ ]+([a-zA-Z0-9_ ]+)[ ]+ON[ ]+([a-zA-Z0-9._]+)=([a-zA-Z0-9._]+)[ ]+WHERE/i', '$1$2 $3,$4 WHERE $5=$6(+) AND', $sql);
				// $sql = preg_replace('/([ ,])([a-zA-Z0-9_ ]+)[ ]+([a-zA-Z0-9_]+)[ ]+LEFT JOIN[ ]+([a-zA-Z0-9_ ]+)[ ]+ON[ ]+([a-zA-Z0-9._]+)=([a-zA-Z0-9._]+)/i', '$1$2 $3,$4 WHERE $5=$6(+)', $sql);
				break;
			// MySQL
			case DBCONN_MYSQL :
				
				// Canvia els :nom_var a ? per les consultes en MySQL
				$sql = preg_replace ( '/:[A-Za-z]+[A-Za-z0-9_]*/i', '?', $sql );
				break;
		}
		
		return $sql;
	}
	
	/*
	 * Obté una conexió a la base de dades
	 *
	 * @param int $tipus DBCONN_PERSISTENT o DBCONN_STANDARD
	 */
	function getConn($tipus = null) {
		if (! isset ( $tipus )) {
			$tipus = self::$tipus;
		}
		switch ($this->provider) {
			// Oracle
			case DBCONN_ORACLE :
				if ($tipus == DBCONN_PERSISTENT) {
					if (! function_exists ( 'oci_pconnect' )) {
						throw new SQLException ( 'La llibreria OCI8 no està activa.', self::CONNECTION_ERROR );
					}
					
					try{
						$this->conn = oci_pconnect ( $this->user, $this->pswd, $this->host, $this->charset );
						if (!$this->conn) {
							throw new OracleException(oci_error ($stm), $e,$this->exceptionasError);
						}
					}
					catch(\Exception $e){
						throw new OracleException(oci_error ($stm), $e,$this->exceptionasError);
					}
					
					if (! $this->conn = oci_pconnect ( $this->user, $this->pswd, $this->host, $this->charset )) {
						$err = oci_error ();
						throw new SQLException ( 'No es pot establir la conexió amb la BD: ' . $err ['message'], self::CONNECTION_ERROR );
					}
				} else {
					if (! function_exists ( 'oci_connect' )) {
						throw new SQLException ( 'La llibreria OCI8 no està activa.', self::CONNECTION_ERROR );
					}
					if (! $this->conn = oci_connect ( $this->user, $this->pswd, $this->host, $this->charset )) {
						$err = oci_error ();
						throw new SQLException ( 'No es pot establir la conexió amb la BD: ' . $err ['message'], self::CONNECTION_ERROR );
					}
				}
				
				// Evitar bug DCRP amb el canvi de format de les dates
				$sql = "ALTER SESSION SET NLS_LANGUAGE='ENGLISH' NLS_NUMERIC_CHARACTERS='.,' NLS_DATE_FORMAT='YYYYMMDD\"T\"HH24MISS\"Z\"' TIME_ZONE='+00:00'";
				$this->execute ($sql);
				
				break;
			// MySQL
			case DBCONN_MYSQL :
				$conn = new mysqli_Extended ( $this->host, $this->user, $this->pswd, $this->db );
				/* change character set to utf8 */
				if (! $conn->set_charset ( $this->charset )) {
					throw new SQLException ( 'Error establint el charset:' . $conn->error, self::CONNECTION_ERROR );
				}
				/* check connection */
				if ($conn->connect_error) {
					throw new SQLException ( "No es pot establir la conexió amb la BD: (" . $conn->connect_errno . ") " . $conn->connect_error, self::CONNECTION_ERROR );
				}
				$this->conn = $conn;
				break;
		}
	}
	
	/**
	 * Estableix el mètode d'execució per defecte.
	 *
	 * @param $execmode DBCONN_COMMIT_ON_SUCCESS
	 *        	o DBCONN_TRANSACTION.
	 * @return bool tue si es correcte, false en cas d'error
	 */
	function setDefaultExecMode($execmode) {
		if ($execmode == DBCONN_COMMIT_ON_SUCCESS || $execmode == DBCONN_TRANSACTION) {
			$this->default_execmode = $execmode;
			return true;
		} else
			return false;
	}
	
	function getDefaultExecMode(){
		return $this->default_execmode==DBCONN_TRANSACTION? 'TRANSACTION':'NO_TRANSACTION';
	}
	
	
	/**
	 * Genera la instrucció sql segons el tipus, la taula i les variables.
	 *
	 * @param string $tipus
	 *        	"INSERT" o "UPDATE"
	 * @param string $taula
	 *        	Taula de la instrucció
	 * @param array $vars
	 *        	Array de variables
	 * @param string $where
	 *        	(optional) Condició del where pels updates
	 * @return string La senténcia sql generada
	 */
	static function generaSQL($accio, $taula, $vars, $where = "") {
		$sql = "";
		
		if ($accio == DBCONN_INSERT) {
			$sql = "insert into " . $taula . " (";
			$sqlvars = "";
			foreach ( $vars as $key => $value ) {
				if ($sqlvars != "")
					$sqlvars .= ",";
				$sqlvars .= $key;
			}
			$sql .= $sqlvars . ") values (";
			$sqlvars = "";
			foreach ( $vars as $key => $value ) {
				if ($sqlvars != "")
					$sqlvars .= ",";
				$sqlvars .= ":" . $key;
			}
			$sql .= $sqlvars . ")";
		} elseif ($accio == DBCONN_UPDATE) {
			if ($where != "") {
				$sql = "update " . $taula . " set ";
				$sqlvars = "";
				foreach ( $vars as $key => $value ) {
					if ($sqlvars != "")
						$sqlvars .= ",";
					$sqlvars .= $key . "=:" . $key;
				}
				$sql .= $sqlvars . " where " . $where;
			} else {
				throw new SQLException ( "generaSQL: Falta la condició del WHERE." );
				return false;
			}
		}
		return $sql;
	}
	
	/*
	 * Obté el mètode d'execució segons el tipus de base de dades.
	 * @param string $exec_mode DBCONN_COMMIT_ON_SUCCESS, DBCONN_TRANSACTION
	 */
	function getExecMode($exec_mode) {
		switch ($this->provider) {
			// Oracle
			case DBCONN_ORACLE :
				if ($exec_mode == DBCONN_COMMIT_ON_SUCCESS) {
					return OCI_COMMIT_ON_SUCCESS;
				} elseif ($exec_mode == DBCONN_TRANSACTION) {
					return OCI_NO_AUTO_COMMIT; // PHP<5.3.2 -> OCI_DEFAULT
				} else
					return false;
				break;
			// MySQL
			case DBCONN_MYSQL :
				if ($exec_mode == DBCONN_COMMIT_ON_SUCCESS) {
					return TRUE;
				} elseif ($exec_mode == DBCONN_TRANSACTION) {
					return FALSE;
				} else
					break;
		}
	}
	
	/**
	 * Executa la instrucció SQL.
	 *
	 * @param string $sql Instrucció SQL que s'executara.
	 * @param array $vars [opt=null] Variables utilitzades del la instrucció SQL
	 * @param string $commit_mode [opt=null] Tipus d'execució DBCONN_COMMIT_ON_SUCCESS o DBCONN_TRANSACTION
	 * @param bool $mostra_sql [opt=false] Mostra la consulta sql sense executar-la
	 *
	 * @return dbStm Retorna un objecte dbStm pels SELECT
	 * o el id de l'últim autonumèric insertat pels inserts de MySQL
	 * o true si s'ha executat correctament
	 */
	function execute($sql, $vars = null, $commit_mode = null, $mostra_sql = false,$called_by = null) {
		$stm = null;
		$id = 0;
		
		// En cas de que no s'indiqui explicitament el mètode d'execució agafa l'establert per defecte
		if ($commit_mode == null)
			$commit_mode = $this->default_execmode;
			
			// Obté el tipus d'execució en funció la base de dades
		$exec_mode = $this->getExecMode ( $commit_mode );
		
		// Tracta la consulta pels diferents tipus de BD
		$sql = self::tractaSQL ( $sql, $this->provider );
		
		if ($mostra_sql) {
			echo "<xmp>$sql</xmp>";
			die ();
		}
		try {
			switch ($this->provider) {
				// Oracle
				case DBCONN_ORACLE:
					/*
					// Estableix un identificador per la conexió (a partir de OCI8 1.4 i Oracle 11G)
					// es podria habilitar per auditar conexions
					try {
						oci_set_client_identifier($this->conn, $this->user);
					} catch (Exapetion $e) {
					}
					*/
					
					if (! $stm = oci_parse ( $this->conn, $sql )) {
						$err = oci_error ( $this->conn );
						throw new SQLException ( 'Error executant SQL: ' . $err ['message'], self::SQLEXECUTION_ERROR );
					} else {
						if (! is_null ( $vars )) {
							foreach ( $vars as $key => $value ) {
								// Comprova si s'ha pasat un array per indicar el tipus
								if (is_array ( $value )) {
									
									// Obté el tipus i tracte les dades
									if ($value [1] == DBCONN_DATATYPE_DATE) {
										try {
											$dateTime = new \DateTime ( str_replace ( "/", "-", $value [0] ) );
											$value [0] = $dateTime->format ( 'Y-m-d H:i:s' );
										} catch ( \Exception $e ) {
											$value [0] = null;
										}
									} else if ($value [1] == DBCONN_DATATYPE_FLOAT) {
										$aux = str_replace ( ",", ".", $value [0] );
										$value [0] = $aux;
									}
									$vars [$key] = $value [0];
								}
								try{
									oci_bind_by_name ( $stm, ':' . $key, $vars [$key] );
								}
								catch(\Exception $e){
									$oer = oci_error($stm);
									if($oer===false){
										$oer["sqltext"] = $sql;
										$oer["message"] = "oci_bind_by_name (\$stm, ':' . $key, \$vars[\$key])";
										$oer["code"] = "ND";
									}
									throw new OracleException($oer, $e,$vars);
								}
							}
						}
						
						try {
							if (!oci_execute($stm,$exec_mode)) {
								throw new OracleException(oci_error ($stm), $e,$vars);
							}
						}
						catch (\Exception $e ) {
							//TODO: -> Quan peta aquesta LINEA, el sistema de control d'errors funciona bé (la linea que diu que falla és la linea on s'executa $dbcon->execute)
							throw new OracleException(oci_error	($stm), $e,$vars);
						}
					}
					
					break;
				// MySQL
				case DBCONN_MYSQL :
					
					// Mira si la connexió està oberta
					if ($this->conn) {
						// set autocommit TRUE o FALSE per habilitar les transaccions
						$this->conn->autocommit ( $exec_mode );
						
						if(is_array($vars) && count($vars)==0){
							$vars = null;
						}
						
						// Comprova si és una consulta amb paràmetres
						if (is_null ( $vars )) {
							if (! $stm = $this->conn->query ( $sql )) {
								throw new SQLException ( 'Error executant SQL: ' . $this->conn->error, self::SQLEXECUTION_ERROR );
							}
						} else {
							if (! $stm = $this->conn->prepare ( $sql )) {
								throw new SQLException ( 'Error preparant SQL: ' . $this->conn->error, self::SQLEXECUTION_ERROR );
							}
							try {
								// Estableix el tipus S (STRING) pels paràmetres
								// En principi funciona per a tots els tipus de dades
								$types = str_repeat ( 's', count ( $vars ) );
								$bind_names [] = $types;
								foreach ( $vars as $key => $value ) {
									$bind_name = 'bind' . $key;
									// Comprova si s'ha pasat un array per indicar el tipus
									if (is_array ( $value )) {
										// Obté el tipus i tracte les dades
										if ($value [1] == DBCONN_DATATYPE_DATE) {
											try {
												$dateTime = new \DateTime ( str_replace ( "/", "-", $value [0] ) );
												$value [0] = $dateTime->format ( 'Y-m-d H:i:s' );
											} catch ( \Exception $e ) {
												$value [0] = null;
											}
										} else if ($value [1] == DBCONN_DATATYPE_FLOAT) {
											$aux = str_replace ( ",", ".", $value [0] );
											$value [0] = $aux;
										}
										$$bind_name = $value [0];
									} else {
										if ($value == '')
											$value = null;
										$$bind_name = $value;
									}
									
									$bind_names [] = &$$bind_name;
								}
								
								// Crida el mètode bind_param de la classe $stm amb el paràmetres
								call_user_func_array ( array (
										&$stm,
										'bind_param' 
								), $this->refValues($bind_names));
								// $stmt->bind_param('sssd', $code, $language, $official, $percent);
								
								if (! $stm->execute ()) {
									// mysqli->info;
									throw new SQLException ( '(' . $stm->errno . ') ' . $stm->error . '<br/>' . $sql, self::SQLEXECUTION_ERROR );
								}
								
								// Obté l'últim id insertat en un autonumèric
								$id = $stm->insert_id;
							} catch ( \Exception $e ) {
								throw new SQLException ( 'Error execució SQL: ' . $e->getMessage (), self::SQLEXECUTION_ERROR );
							}
						}
					} else {
						// Connexió erronia o inexistent
						throw new SQLException ( 'Error executant SQL: Conexió errònia o inexistent', self::SQLEXECUTION_ERROR );
					}
					break;
			}
		} catch ( SQLException $e ) {
			$msg = $sql . "\r\n<br/>" . $e->getMessage();
			$msg .= "\r\n<br/>user:" . $this->user;
			throw new MyException($msg, 500, $e->getFile(), $e->getLine(), null);
		}
		try {
			// Obté el primer caràcter de la consulta per determinar el tipus
			switch (substr ( strtolower ( $sql ), 0, 1 )) {
				case 's': /* Select */
					$mystm = new dbStm ( $this->provider, $stm, $sql, $vars );
					return $mystm;
					break;
				case 'u' : /* Update */
				case 'i' : /* Insert */
				case 'd': /* Delete */
					$id = null;
					if($this->provider == DBCONN_ORACLE){
						$id = oci_num_rows ( $stm );
					}
				case 'a': /* Alter */
					if ($this->provider == DBCONN_ORACLE)
						oci_free_statement ( $stm );
					elseif ($this->provider == DBCONN_MYSQL) {
						if (is_object ( $stm ))
							$stm->close ();
					}
					break;
			}
		} catch ( \Exception $e ) {
			throw new SQLException ( 'Error execució SQL: ' . $e->getMessage (), self::SQLEXECUTION_ERROR );
		}
		//echo $sql."\r\n";
		$this->exceptionasError = true;
		if ($id > 0)
			return $id;
		else
			return true;
	}
	
	
	private function refValues($arr){
		if (strnatcmp(phpversion(),'5.3') >= 0) //Reference is required for PHP 5.3+
		{
			$refs = array();
			foreach($arr as $key => $value)
				$refs[$key] = &$arr[$key];
				return $refs;
		}
		return $arr;
	}
	
	/*
	 * Executa la consulta SQL i retorna els resultats corresponents a la pàgina segons
	 * el nº màxim de registres per pàgina
	 *
	 * @param string $sql Consulta que es vol paginar
	 * @param int $npage Nº de pàgina
	 * @param int $maxrowspage Nº màxim de registres per pàgina
	 * @param int $nregistres
	 * @param int $npagines
	 * @param $vars
	 * @param bool $mostra_sql
	 *
	 * @return dbStm
	 */
	function paging($sql, &$npage, $maxrowspage, &$nregistres = 0, &$npagines = 0, $vars = null, $mostra_sql = false) {
		
		// Comprovacions inicials
		if (substr ( strtolower ( $sql ), 0, 1 ) != "s") {
			throw new SQLException ( 'Error paginant: La consulta ha se der un SELECT.', self::SQLEXECUTION_ERROR );
		}
		$maxrowspage = ( int ) $maxrowspage;
		if (! is_int ( $maxrowspage ) || $maxrowspage <= 0 || is_null ( $maxrowspage ))
			$maxrowspage = 0;
			// throw new Exception('Error paginant: maxrowspage ha de ser un número enter.', self::SQLEXECUTION_ERROR);
		
		$npage = ( int ) $npage;
		if (! is_int ( $npage ) || $npage <= 0 || is_null ( $npage ))
			$npage = 1;
			
			// Tracta la consulta pels diferents tipus de BD
		$sql = self::tractaSQL ( $sql, $this->provider );
		
		if ($mostra_sql) {
			echo "<xmp>$sql</xmp>";
			die ();
		}
		// Obté el total de registres que compleixen la Query
		$sql_count = "select count(*) as NUM_FILES from (" . $sql . ")";
		if ($this->provider == DBCONN_MYSQL)
			$sql_count .= " as c";
		try {
			$stm = $this->execute ( $sql_count, $vars, null, false, false );
		} catch ( \Exception $e ) {
			/*
			 * @author: Pau Hidalgo
			 * Degut a un bug amb Oracle, els counts de selects donen un error genèric d'oracle (ORA-00600)
			 * Pel que he comprovat, es dona quan s'intenta fer un sum dels unions amb resultats de 0 del count.
			 * Aquesta part del catch és per quan passa això, separo els selects i creo una consulta SQL
			 * sumant manualment cada select.
			 * SELECT (SELECT COUNT(1) FROM $CONSULTA1)+(...) as NUM_FILES FROM DUAL;"
			 */
			$selects = explode ( "UNION ALL", $sql );
			$sql_c = "SELECT (";
			foreach ( $selects as $select ) {
				$sql_c .= "(SELECT COUNT(1) FROM (" . $select . "))+";
			}
			$sql_c = substr ( $sql_c, 0, - 1 );
			$sql_c .= ") as NUM_FILES from dual";
			$stm = $this->execute ( $sql_c, $vars, null, false, false );
		}
		
		// $dbcon = new dbConn();
		if ($stm->fetch ()){
			$nregistres = $stm->getVal ( "NUM_FILES" );
		}
		else{
			$nregistres = 0;
		}
		$stm->close ();
		
		// Calculem el total de pàgines
		if ($maxrowspage != 0) {
			//die("npagines: $npagines, nregistres: $nregistres, maxrowspage: $maxrowspage");
			$npagines = ceil ( $nregistres / $maxrowspage );
			if ($npagines <= 0)
				$npagines = 1;
		} else
			$npagines = 1;
			
			// Evita que la pàgina actual sigui més gran que el total
		if ($npage > $npagines) {
			$npage = $npagines;
		}
		
		// Prepara la consulta de la pàginació
		if ($maxrowspage > 0) {
			switch ($this->provider) {
				// Oracle
				case DBCONN_ORACLE :
					$sql = "SELECT * FROM (SELECT a.*, rownum r__ FROM (" . $sql . ") a WHERE rownum < ((" . $npage . " * " . $maxrowspage . ") + 1 )) WHERE r__ >= (((" . $npage . "-1) * " . $maxrowspage . ") + 1)";
					break;
				// MySQL
				case DBCONN_MYSQL :
					$sql = $sql . " LIMIT " . (($npage * ($maxrowspage)) - $maxrowspage) . "," . $maxrowspage;
					;
					break;
			}
		}
		
		// $rs = $this->execute($sql,$vars,null,false,true);
		return ($this->execute ( $sql, $vars, null, false, false ));
	}
	
	/**
	 * Guarda un LOB en la base de dades
	 *
	 * @param string $data
	 *        	El contingut del LOB o el nom del fitxer
	 * @param string $camp
	 *        	Nom del camp
	 * @param string $taula
	 *        	Nom de la taula
	 * @param string $where
	 *        	Condició del where
	 * @param string $tipus
	 *        	"DADES" -> $data conté les dades a guardar.
	 *        	"FITXER"-> $data conté el nom del fitxer.
	 *        	
	 * @return bool FALSE-> Si s'ha produït un error
	 */
	function writeLob($data, $camp, $taula, $where, $vars = null, $tipus = DBCONN_DADES) {
		if ($where == "")
			throw new SQLException ( 'Falta WHERE en writeLob.', self::SQLEXECUTION_ERROR );
		if ($camp == "")
			throw new SQLException ( 'Falta el camp en writeLob.', self::SQLEXECUTION_ERROR );
		if ($taula == "")
			throw new SQLException ( 'Falta la taula en writeLob.', self::SQLEXECUTION_ERROR );
		
		$camp = mb_strtoupper ( $camp, "UTF-8" );
		
		switch ($this->provider) {
			// Oracle
			case DBCONN_ORACLE :
				$sql = "SELECT " . $camp . " FROM " . $taula . " WHERE " . $where . " FOR UPDATE";
				// DBCONN_TRANSACTION -> Inicia una transacció
				$stmt = $this->execute ( $sql, $vars, DBCONN_TRANSACTION, false );
				
				// Obté la fila actual
				$row = oci_fetch_assoc ( $stmt->stm );
				if (! $row) {
					$stmt->close ();
					
					// No s'han trobat files
					return false;
				}
				
				
				// Elimina el contingut actual del LOB
				if (! $row [$camp]->truncate ()) {
					// Error
					$row [$camp]->free ();
					$stmt->close ();
					return false;
				}
				
				if ($data != "") {
					if ($tipus == DBCONN_FITXER) {
						// Now save a value to the LOB
						if (! $row [$camp]->savefile ( $data )) {
							// Error
							$row [$camp]->free ();
							$stmt->close ();
							return false;
						}
					} else {
						// Now save a value to the LOB
						if (! $row [$camp]->save ( $data )) {
							// Error
							$row [$camp]->free ();
							$stmt->close ();
							return false;
						}
					}
				}
				
				// Si la conexió no es de tipus transacció realitza el commit
				if ($this->default_execmode != DBCONN_TRANSACTION)
					$this->commit ();
					
					// Allibera els recursos
				$stmt->close ();
				$row [$camp]->free ();
				break;
			
			// MySQL
			case DBCONN_MYSQL :
				
				// TODO: http://www.java-samples.com/showtutorial.php?tutorialid=930
				// No utilitzar file_get_contents per tal de guardar el BLOB per paquets
				if ($tipus == DBCONN_FITXER) {
					$data = file_get_contents ( $data );
				}
				// Posa el paràmetre $camp al principi de l'array $var
				// per poguer realitzar inserts passant els valors del where per paràmetres
				$vars [$camp] = $data;
				if (! is_null ( $vars )) {
					$aux [$camp] = $data;
					foreach ( $vars as $key => $value ) {
						$aux [$key] = $value;
					}
					$vars = $aux;
					unset ( $aux );
				}
				
				$sql = "UPDATE " . $taula . " SET " . $camp . "=? WHERE " . $where;
				$this->execute ( $sql, $vars );
				break;
		}
		
		return true;
	}
	
	/*
	 * Indexa el contingut d'una taula d'index intermedia.
	 * @param string $index Nom de l'index
	 */
	function syncIndex($index) {
		switch ($this->provider) {
			// Oracle
			case DBCONN_ORACLE :
				
				// Atenció: Aquesta execució provoca un commit en la conexió
				// encara que s'indiqui el mode d'execució de transacció.
				$sql = "[BEGINCALL]ctx_ddl.sync_index('" . $index . "');[ENDCALL]";
				$this->execute ( $sql, null, null, false );
				break;
			// MySQL
			case DBCONN_MYSQL :
				
				break;
		}
	}
	
	/**
	 * Crea una nova variable de LOB
	 *
	 * @return Un LOB buit.
	 */
	public function newLob() {
		$lob = null;
		switch ($this->provider) {
			// Oracle
			case DBCONN_ORACLE :
				$lob = oci_new_descriptor ( $this->conn, OCI_DTYPE_LOB );
				break;
			// MySQL
			case DBCONN_MYSQL :
				$lob = null;
				break;
		}
		return $lob;
	}
	
	/**
	 * Deixa a empty un lob
	 *
	 * @param $blob Un
	 *        	Objecte de tipus LOB.
	 * @author Anselm Garcia
	 */
	function eraseLob($lob) {
		switch ($this->provider) {
			// Oracle
			case DBCONN_ORACLE :
				if ($lob != null) {
					if (($lob->size ()) > 0)
						return $lob->truncate ();
				}
				break;
			// MySQL
			case DBCONN_MYSQL :
				$lob = null;
				break;
				break;
		}
	}
	
	/*
	 * Realitza un rollback d'una transacció.
	 */
	public function rollback() {
		switch ($this->provider) {
			// Oracle
			case DBCONN_ORACLE :
				oci_rollback ( $this->conn );
				break;
			// MySQL
			case DBCONN_MYSQL :
				$this->conn->rollback ();
				break;
		}
	}
	
	/*
	 * Realitza un commit d'una transacció.
	 */
	function commit() {
		$result = false;
		switch ($this->provider) {
			// Oracle
			case DBCONN_ORACLE :
				$result = oci_commit ( $this->conn );
				break;
			// MySQL
			case DBCONN_MYSQL :
				$result = $this->conn->commit ();
				break;
		}
		return $result;
	}
	
	
	
	/**
	 * Alias for setValues("C")
	 * 
	 * @param string $table
	 * @param array $vars
	 * @param string $where
	 * @param array $varswhere
	 * @param bool $show_sql
	 */
	public function add($table, &$vars, $show_sql = false){
		$ww = array();
		try{
			$r = $this->setValues("C", $table, $vars,"",$ww,$show_sql);
		}
		catch(\Exception $e){
			$oraError["code"] = $e->oracode;
			$oraError["message"] = $e->oramessage;
			$oraError["sqltext"] = $e->orasql;
			throw new OracleException($oraError,$e,$e->vars);
		}
		return $r;
	}

	/**
	 * Alias for setValues("M")
	 *
	 * @param string $table
	 * @param array $vars
	 * @param string $where
	 * @param array $varswhere
	 * @param bool $show_sql
	 */
	public function update($table, &$vars, $where = "", &$varswhere = null, $show_sql = false){
		try{
			$r = $this->setValues("M", $table, $vars,$where,$varswhere,$show_sql);
		}
		catch(\Exception $e){
			$oraError["code"] = $e->oracode;
			$oraError["message"] = $e->oramessage;
			$oraError["sqltext"] = $e->orasql;
			throw new OracleException($oraError,$e,$e->vars);
		}
		return $r;
	}
	
	/*
	 * Realitza l'accio amb les variables en la taula indicada.
	 * Atenció: Si s'utilitzen paràmetres en el WHERE de l'Update. Els paràmetres
	 * que s'utilitzen s'han de passar en l'array $varswhere i han d'haber estat
	 * declarats en l'ordre que apareixen en el WHERE. A més no poden tenir el
	 * mateix nom que els paràmetres de $vars.
	 *
	 * @param string $accio "C"->Insert, "M"->Update
	 * @param string $taula Taula sobre la que es realitzarà l'acció.
	 * @param array $vars Array amb els camps i els seus valors.
	 * @param string $where Condició del WHERE els Update.
	 * @param array $varswhere Array amb els camps i els seus valor de la condició del WHERE.
	 *
	 * @return int Id de l'autonumèric creat en mySQL
	 */
	function setValues($accio, $taula, &$vars, $where = "", &$varswhere = null, $mostra_sql = false,$called_by = null) {
		$id = null;
		$accio = mb_strtoupper ( $accio, "UTF-8" );
		if ($sql = self::generaSQL ( $accio, $taula, $vars, $where )) {
			// Afegeix al final de vars les variables de $varswhere
			if (! is_null ( $varswhere ) && $accio == "M") {
				foreach ( $varswhere as $key => $value ) {
					$vars [$key] = $value;
				}
			}
			try{
				$id = $this->execute ( $sql, $vars, null, $mostra_sql,$called_by);
			}
			catch(OracleException $e){
				$oraError["code"] = $e->oracode;
				$oraError["message"] = $e->oramessage;
				$oraError["sqltext"] = $e->orasql;
				// $oraError = get_object_vars($oraE);
				throw new OracleException($oraError,$e,$e->vars);
			}
		}
		
		$vars = null;
		unset ( $vars );
		$varswhere = null;
		unset ( $varswhere );
		return $id;
	}
	
	/*
	 * Tanca la conexió amb la base de dades
	 *
	 */
	function close() {
		switch ($this->provider) {
			// Oracle
			case DBCONN_ORACLE :
				if (is_object ( $this->conn ))
					oci_close ( $this->conn );
				break;
			// MySQL
			case DBCONN_MYSQL :
				if($this->conn!=null){
					$this->conn->close();
				}
				
				break;
		}
	}
}

/**
 * ***************************************
 * Classe pels statements
 * ***************************************
 */
class dbStm {
	private $provider;
	public $stm;
	private $mysql_get_result;
	private $row;
	private $sql;
	private $use_vars;
	private $vars;
	const CONNECTION_ERROR = 1;
	const SQLEXECUTION_ERROR = 2;
	
	/**
	 * Constructor de la classe
	 *
	 * @param string $provider
	 *        	DBCONN_ORACLE, DBCONN_MYSQL
	 * @param
	 *        	$stm
	 * @param string $sql        	
	 * @param $vars (optional)        	
	 */
	function __construct($provider, $stm, $sql, $vars = null) {
		$this->provider = $provider;
		$this->stm = $stm;
		$this->sql = $sql;
		$this->use_vars = ! is_null ( $vars );
		$this->vars = $vars;
		
		if($this->provider == DBCONN_MYSQL){
			if($this->use_vars){
				$this->mysql_get_result = $stm->get_result();
			}
			else{
				$this->mysql_get_result = &$stm; 
			}
			
		}
		
	}
	
	public function getProvider(){
		return $this->provider;
	}
	
	/**
	 * Donada una propietat inexistent, fem el getVal.
	 * 
	 * És a dir, dona igual fer $rs->getVal("ID") que $rs->id
	 * 
	 * @param unknown $property
	 * @return Ambigous <string, unknown>
	 */
	public function __get($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}
		else{
			return $this->getVal(strtoupper($property));			
		}
	}
	
	/*
	 * Destructor de la classe
	 */
	function __destruct() {
		if (! is_null ( $this->stm )) {
			$this->close ();
		}
	}
	
	/**
	 * Retorna el número de files d'una consulta
	 *
	 * @param
	 *        	dbConn
	 * @return int
	 */
	public function count(dbConn $dbcon) {
		$numfiles = 0;
		
		switch ($this->provider) {
			// Oracle
			case DBCONN_ORACLE :
				$sql = "select count(*) as NUM_FILES from (" . $this->sql . ")";
				// Executa la consulta com a transaction, ja que si ho fa en mode normal
				// efectua un commit d'altres execucions anteriors marcades com a transaction
				$aux = $dbcon->execute ( $sql, $this->vars, DBCONN_TRANSACTION, false );
				if ($aux->fetch ()) {
					$numfiles = $aux->getVal ( 'NUM_FILES' );
				}
				$aux->close ();
				break;
			
			// MySQL
			case DBCONN_MYSQL :
				$numfiles = $this->stm->num_rows;
				break;
		}
		
		return $numfiles;
	}
	
	/**
	 *
	 * @param object $blob
	 *        	Un Objecte de tipus LOB.
	 * @return string El buffer resultant de la lectura de $blob.
	 */
	public function readLob($lob) {
		switch ($this->provider) {
			// Oracle
			case DBCONN_ORACLE :
				if ($lob != null && is_object ( $lob )) {
					if (($lob->size ()) > 0)
						return $lob->read ( $lob->size () );
					else
						return "";
				} else
					return "";
				break;
			// MySQL
			case DBCONN_MYSQL :
				return $lob;
				break;
				break;
		}
	}
	
	/*
	 * Obté la següent fila i l'estableix en la classe
	 *
	 * @return bool true-> si ha recuperat la fila, false-> si no hi han més files
	 */
	public function fetch() {
		switch ($this->provider) {
			// Oracle
			case DBCONN_ORACLE :
				
				// TODO: Si canviem oci_fetch_assoc per oci_fetch_array($this->stm, OCI_ASSOC+OCI_RETURN_NULLS+OCI_RETURN_LOBS))
				// podriem recuperar els valors dels LOBS directament
				if ($this->row = oci_fetch_assoc ( $this->stm ))
					return true;
				else
					return false;
				break;
			// MySQL
			case DBCONN_MYSQL :
				//$this->row = $this->stm->fetch_assoc();
				if ($this->row = $this->mysql_get_result->fetch_assoc()){
					$this->row = array_change_key_case_recursive($this->row, CASE_UPPER);
					return true;
				}
				else{
					return false;
				}
				break;
		}
	}
	
	/*
	 * Retorna un array amb la següent fila o false si no hi han més files
	 *
	 * @return mixed[] Un array amb els camps de la següent fila o FALSE si no hi han més files.
	 */
	public function getRow() {
		switch ($this->provider) {
			// Oracle
			case DBCONN_ORACLE :
				if ($this->row = oci_fetch_array ( $this->stm, OCI_ASSOC + OCI_RETURN_NULLS + OCI_RETURN_LOBS ))
					return ($this->row);
				else
					return false;
				break;
			// MySQL
			case DBCONN_MYSQL :
				if ($this->row = $this->mysql_get_result->fetch_assoc ()){
					return (array_change_key_case_recursive($this->row,CASE_UPPER));
				}
				else{
					return false;
				}
					
				break;
		}
	}
	
	/**
	 * Fetches the recordset to an ARRAY (for SELECT statements) into the internal result-buffer
	 *
	 * @return mixed[] The array where the function will fetch the results
	 */
	public function getAll() {
		$resultArray = null;
		switch ($this->provider) {
			// Oracle
			case DBCONN_ORACLE :
				oci_fetch_all ( $this->stm, $resultArray, 0, - 1, OCI_ASSOC + OCI_FETCHSTATEMENT_BY_ROW );
				break;
			// MySQL
			case DBCONN_MYSQL :
				$resultArray = $this->mysql_get_result->fetch_all ( MYSQLI_ASSOC );
				break;
		}
		if ($resultArray == null)
			$resultArray = array();
		
		return array_change_key_case_recursive($resultArray);
	}
	
	/*
	 * Obté el valor d'un camp
	 *
	 * @return mixed El valor del camp
	 */
	public function getVal($camp) {
		$camp = mb_strtoupper ( $camp, "UTF-8" );
		switch ($this->provider) {
			// Oracle
			case DBCONN_ORACLE :
				$tipus = oci_field_type ( $this->stm, $camp );
				// echo "<xmp>camp=$camp, tipus=$tipus</xmp>";
				if ($tipus == "CLOB" || $tipus == "BLOB") {
					return $this->readLob ( $this->row [$camp] );
				} elseif ($tipus == "ROWID") {
					return $this->row [$camp];
				} else
					return $this->row [$camp];
				break;
			// MySQL
			case DBCONN_MYSQL :
				return $this->row [$camp];
				break;
		}
	}
	
	/*
	 * Obté el número camps de la fila
	 *
	 * @return int El número camps de la fila
	 */
	public function getNumFields() {
		switch ($this->provider) {
			// Oracle
			case DBCONN_ORACLE :
				$ncols = oci_num_fields ( $this->stm );
				break;
			// MySQL
			case DBCONN_MYSQL :
				return $this->stm->field_count ();
				break;
		}
	}
	
	/*
	 * Tanca el statement (recorset)
	 */
	public function close() {
		switch ($this->provider) {
			// Oracle
			case DBCONN_ORACLE :
				if (! oci_free_statement ( $this->stm )) {
					$err = oci_error ();
					throw new \Exception ( 'Error alliberant statement: ' . $err ['message'] );
				}
				break;
			// MySQL
			case DBCONN_MYSQL :
				
				// Allibera el buffer (en el cas de les crides amb paràmetres store_result)
				if ($this->use_vars){
					$this->stm->free_result ();
				}
				$this->stm->close ();
				break;
		}
		$this->stm = null;
	}
}
?>