<?php

namespace api\crud;

use memoq\project;

require_once BaseDirAPI.'/libs/memoq/class/class.project.php';
abstract class memoqManagement {
	
	//
	public static function projectExist() {
		$args = func_get_args ();
		$args = $args [0];
		$project = new project ();
		return $project->projectExist ( $args );
	}
	public static function addProject() {
		$args = func_get_args ();
		$args = $args [0];
		$task = new project ();
		return $task->addProject ( $args );
	}
	public static function uploadFileToProject() {
		$args = func_get_args ();
		$args = $args [0];
		$task = new project ();
		return $task->uploadFileToProject ( $args );
	}
	public static function stadisticDocValue() {
		$args = func_get_args ();
		$args = $args [0];
		$task = new project ();
		return $task->stadisticDocValue ( $args );
	}
	public static function assignedUsertoProject() {
		$args = func_get_args ();
		$args = $args [0];
		$task = new project ();
		return $task->assignedUsertoProject ( $args );
	}
	public static function assigedUsertoDoc() {
		$args = func_get_args ();
		$args = $args [0];
		$task = new project ();
		return $task->assigedUsertoDoc ( $args );
	}
	public static function documentInfo() {
		$args = func_get_args ();
		$args = $args [0];
		$task = new project ();
		return $task->documentInfo ( $args );
	}
	public static function downloadFileFromProject() {
		$args = func_get_args ();
		$args = $args [0];
		$task = new project ();
		return $task->downloadFileFromProject ( $args );
	}
}