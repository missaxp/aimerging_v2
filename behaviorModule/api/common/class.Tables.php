<?php 

namespace common;
/**
 * Basic class to get table data from DB.
 * 
 * 
 * @author phidalgo
 * @created 20150909
 * @modified 20160330
 */
 
use core\Environment;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;

class Table{
	const THIRD_PARTY = 1;
	const CONTACTS = 2;
	const USERS = 3;
	const CHAPTERS = 4;
	const QUALIFICATIONS = 5;
	const PROJECTS = 6;
	const TEAMS = 7;
	const PROVIDERS = 8;
	const CUSTOMERS = 9;
	const FIELDS = 10;
	const TASKTYPE = 12;
	const TASKCONCEPTS = 13;
	const TASK_EVENTS = 14;
	const MBRAND = 15;
	
	const DEPARTMENT_COLUMN_NAME = "DEPARTMENT_ID";
	
	protected static $environment;
	
	/**
	 * Table ID
	 * @var integer
	 */
	public $id;
	/**
	 * Table name (alias)
	 * @var string
	 */
	public $name;
	/**
	 * DB Table name. Same name as table in the DB.
	 * @var string
	 */
	public $db_name;
	/**
	 * if this table has tags.
	 * @var boolean
	 */
	public $hasTags;
	/**
	 * If this table has dynamic attributes
	 * @var boolean
	 */
	public $hasDynAttr;
	/**
	 * If this table has Granular Permission System
	 * @var boolean
	 */
	public $hasGranularP;
	
	/**
	 * If this table has Department to filtering.
	 * 
	 * @var boolean $hasDepartment
	 */	
	public $hasDepartment;
	
	/**
	 * If this table has series for create new records.
	 * @var boolean
	 */
	public $hasSeries;
	
	/**
	 * Table series. Only not null if hasSeries==true.
	 * @var array[\common\series]
	 */
	public $series;
	
	/**
	 * Parent table if has parent table.
	 * @var Table
	 */
	public $parent = null;
	
	/**
	 * If has parent table.
	 */
	public $hasParent;
	/**
	 * Si té series, el camp de la DB on anirà.
	 * @var string
	 */
	private $serieField;
	/**
	 * Array of primary Keys
	 * @var array[]
	 */
	public $primary_key;
	
	/**
	 * Array of fields
	 * @var array[\common\columns]
	 */
	private $columns;
	
	public function __construct($id,$saved_table = true){
		self::$environment = Environment::getInstance();
		
		if($saved_table){ //Per defecte carreguem la informació de les taules desades en la taula "TABLES" de la DB. Serveix per les taules IMPORTANTS (thirdparty,users...etc)
			$rs = self::$environment->dbcon->execute("SELECT * FROM TABLES WHERE ID=:ID",array("ID" => $id));
			if($rs->fetch()){
				$this->id = $rs->id;
				$this->name = $rs->name;
				$this->db_name = $rs->table_name;
				$this->hasDynAttr = (($rs->attributes=="Y")? true:false);
				$this->hasGranularP = (($rs->granular_p=="Y")? true:false);
				$this->hasTags = (($rs->tags=="Y")? true:false);
				$this->hasDepartment = false;
				$this->hasSeries = (($rs->series=="Y")? true:false);
				$this->serieField = $rs->serie_field;
				$this->hasParent = false;
				$parent_table = $rs->parent_table;
				if($parent_table!=null){
					$this->parent = new self($parent_table);
					$this->hasParent = true;
				}
			}
			$rs->close();
			if($this->id==null){
				throw new AppException(Status::S5_InternalServerError,ErrorCode::T_NOT_FOUND,"Table $id not found");
			}	
		}
		else{ //Però si volem aquesta informació d'alguna taula secundaria, ...
			$this->id = null;
			$this->name = strtoupper($id);
			$this->db_name = strtoupper($id);
			$this->hasDynAttr = false;
			$this->hasGranularP = false;
			$this->hasTags = false;
			$this->hasDepartment = false;
			$this->hasSeries = false;
			$this->serieField = null;
			$this->hasParent = false;
			$this->parent = null;
			$this->primary_key = array();
			$this->columns = array();
		}
		
		
		//Get table Primary Keys
		$sql = "select COLUMN_NAME from USER_CONS_COLUMNS UCC,USER_CONSTRAINTS UC WHERE
			UC.TABLE_NAME=UCC.TABLE_NAME AND UC.TABLE_NAME=:TABLE_NAME AND UC.CONSTRAINT_NAME=UCC.CONSTRAINT_NAME AND UC.CONSTRAINT_TYPE='P'";
		
		$rs = self::$environment->dbcon->execute($sql, array("TABLE_NAME" => $this->db_name));
		while($rs->fetch()){
			$this->primary_key[] = $rs->column_name;
		}
		$rs->close();

		if($this->primary_key==null){
			throw new AppException(Status::S5_InternalServerError,ErrorCode::TABLE_PK_NOT_FOUND);
		}
		
		//Get columns for this table (name, type, length).
		$sql = "select COLUMN_NAME,DATA_TYPE,DATA_LENGTH FROM USER_TAB_COLS WHERE TABLE_NAME=:TABLE_NAME";
		$rs = self::$environment->dbcon->execute($sql, array("TABLE_NAME" => $this->db_name));
		while($rs->fetch()){
			$this->columns[] = new columns($rs->column_name, $rs->data_type, $rs->data_length);
			if($rs->column_name == self::DEPARTMENT_COLUMN_NAME){
				$this->hasDepartment = true;
			}
		}
		$rs->close();
		
		if($this->hasSeries){
			$var["TID"] = $this->id;
			$sql = "SELECT * FROM TABLES_SERIES WHERE TABLE_ID=:TID";
			$rs = self::$environment->dbcon->execute($sql,$var);
			$series = $rs->getAll();
			$rs->close();
			foreach($series as $serie){
				$this->series[] = new series($serie,$this->serieField,$this->db_name);
			}
		}
	}
	
	
	public function getSerieField(){
		return ($this->hasSeries? strtolower($this->serieField):null);
	}
	
	/**
	 * Get database column
	 * @param string $name
	 */
	public function getColumn($name = null){
		if($name == null){
			return $this->columns;
		}
		else{
			foreach($this->columns as $column){
				if(strtoupper($column->name) == strtoupper($name)){
					return $column;
				}
			}
			return false;
		}
	}
}

class series{
	protected static $environment;
	private $table_id;
	public $name;
	public $serie;
	public $counter;
	public $department_id;
	private $serieLength = 8;
	private $db_field;
	private $db_table;
	
	public function __construct($serie,$db_field,$db_table){
		$serie = array_change_key_case_recursive($serie);
		self::$environment = Environment::getInstance();
		$this->table_id = $serie["table_id"];
		$this->name = $serie["name"];
		$this->counter = (int)$serie["counter"];
		$this->serie = $serie["serie"];
		$this->department_id = $serie["department_id"];
		$this->db_field = $db_field;
		$this->db_table = $db_table;
	}
	
	public function newValue(){
		$sl = strlen($this->serie);
		$available_counter_length = ($this->serieLength - $sl);
		$newSerie = false;
		do{
			$this->counter++;
			$string = $this->getCountString($available_counter_length, $this->counter);
			$newSerie = $this->checkSerie($string);
		}
		while($newSerie===false);
		
		
		return $this->serie.$string;
	}
	
	public function setValue(){
		$var["COUNTER"] = $this->counter;
		$vw["TID"] = $this->table_id;
		$vw["SERIE"] = $this->serie;
		self::$environment->dbcon->update("TABLES_SERIES",$var,"TABLE_ID=:TID AND SERIE=:SERIE",$vw);
	}
	
	public function checkSerie($serie){
		$validSerie = false;
		$sql = "SELECT ".$this->db_field." FROM ".$this->db_table." WHERE ".$this->db_field."=:SERIE";
		$rs = self::$environment->dbcon->execute($sql,array("SERIE" => $this->serie.$serie));
		if(!$rs->fetch()){
			$validSerie = true;
		}
		$rs->close();
		return $validSerie;
	}
	
	private function getCountString($length,$counter){
		$string = "";
		$cl = strlen((string) $counter);
		for($i = 0;$i<$length;$i++){
			if($i<($length-$cl)){
				$string.= "0";
			}
			else{
				$cs = (string)$counter;
				$string.= $cs{($length-($i+1))};
			}
		}
		return $string;
	}
}

class columns{
	/**
	 * Column Name
	 * @var string
	 */
	public $name;
	/**
	 * Column Type
	 * @var string
	 */
	public $type;
	/**
	 * Column length
	 * @var string
	 */
	public $length;
	
	public function __construct($name,$type,$length){
		$this->name = $name;
		$this->type = $type;
		$this->length = $length;
		
	}
}
 
class Tables{
	protected static $environment;
	/**
	 * Array of tables.
	 * @var array[Table]
	 */
	private $tables = null;
	
	public function __construct(){
		self::$environment = Environment::getInstance();
		
		$sql = "SELECT ID FROM tables";
		$rs = self::$environment->dbcon->execute($sql);
		while($rs->fetch()){
			$this->tables[] = new Table($rs->id);
		}
		$rs->close();
	}
	
	
	/**
	 * Given an integer (id) or a string (db_name) return $table.
	 * By default throws an exception if no table is found. Set second argument to false to avoid this behaviour.
	 * 
	 * @param mixed $id
	 * @param boolean throw
	 * @throws AppException
	 */
	public function _get($id = null,$throw = true){
		if($id == null){
			return $this->tables;
		}
		else{
			foreach($this->tables as $table){
				if($table->id==$id){
					return $table;
				}
				else if($table->db_name==$id){
					return $table;
				}
			}
		}
		if($throw){
			throw new AppException(Status::S5_InternalServerError,ErrorCode::T_NOT_FOUND,"Table $id not found on Tables array");
		}
		else{
			return false;
		}
	}
	
	/**
	 * Return array of tables with:
	 * id,name,hasDynAttr,hasDepartments,hasGranular_p,hasTags
	 */
	public function _getBasic(){
		$rsp = array();
		foreach($this->tables as $table){
			$obj = new \stdClass();
			$obj->id = $table->id;
			$obj->name = $table->name;
			$obj->hasDynAttr = $table->hasDynAttr;
			$obj->hasDepartment = $table->hasDepartment;
			$obj->hasGranularP = $table->hasGranularP;
			$obj->hasTags = $table->hasTags;
			$obj->hasSeries = $table->hasSeries;
			$obj->series = array();
			if($table->hasSeries){
				if(self::$environment->user->user_department==null){
					$obj->series = $table->series;
				}
				else{
					foreach($table->series as $serie){
						if($serie->department_id==self::$environment->user->user_department || $serie->department_id==null){
							$obj->series[] = $serie;
						}
					}	
				}
			}
			
			$rsp[] = $obj;
		}
		return $rsp;
	}
}
?>