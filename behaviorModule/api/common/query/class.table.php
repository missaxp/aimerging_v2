<?php
namespace query;

use core\Environment;

class table extends database_element{
	protected static $environment;
	private $alias;
	private $name;
	/**@var Table*/
	private $table;
	
	public function __construct(){
		self::$environment = Environment::getInstance();
		$arg = func_get_args();
		if(count($arg)>1){
			$this->alias = $arg[0];
			$this->name = $arg[1];
		}
		else{
			$a = explode($arg[0]," ");
			if(count($a)>1){
				$this->alias = $a[0];
				$this->name = $a[1];
			}
			else{
				$this->name = $arg[0];
			}
		}
		
		$table = self::$environment->tables->_get($this->name,false);
		
		if($table!==false){
			$this->table = $table;
		}
	}
	
	public function hasAlias(){
		return ($this->alias!=null? true:false);
	}
	
	public function getAlias(){
		return ($this->alias!=null? $this->alias:false);
	}
	
	public function getName(){
		return $this->name;
	}
	
	public function mount($addSeparator = true){
		$this->mount = $this->name.($this->alias!=null? " ".$this->alias:" ").($addSeparator? ", ":"");
		return $this->mount;
	}
	
	/**
	 * Check if this table has enabled Granular Permission System.
	 * @return \common\Table
	 */
	public function hasGPS(){
		if($this->table==null) return false;
		return (($this->table->hasGranularP)? $this->table:false);
	}
}