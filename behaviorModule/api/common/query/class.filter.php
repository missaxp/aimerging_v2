<?php
namespace query;

use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;
class filter extends database_element{

	private $alias;
	private $first;
	private $operator;
	private $union;
	private $second;
	private $var = array();
	private $multiple_var;
	private $publish;
	private $manual;
	private $manual_callback;
	private $manual_response;
	
	private $fromSubQuery;
	private $query = null;
	private $defaultValues = null;
	
	private $dbProvider = null;
	
	public function __construct($alias,$first = null,$fromSubQuery = false,$values = null, $dbProvider){
		$this->dbProvider = $dbProvider;
		$this->alias = ($dbProvider==DBCONN_ORACLE? strtoupper($alias):$alias);
		$this->first = $first;
		$this->type = _OT::TEXT;
		$this->operator = _OO::EQUAL;
		$this->union = _OU::OR_;
		$this->defaultValues = $values;
		
		$a = explode(".",$this->first);
		if(count($a)>1){
			$this->table_alias = $a[0];
		}
		$this->manual = false;
		$this->fromSubQuery = $fromSubQuery;
	}
	
	public function getFirst(){
		return $this->first;
	}
	
	public function getAlias(){
		return $this->alias;
	}
	
	public function publish($publish = true){
		$this->publish = $publish;
	}
	
	public function published(){
		return $this->publish;
	}
	
	public function hasTableAlias(){
		return ($this->table_alias!=null)? true:false;
	}
	
	public function getTable(){
		return $this->table_alias;
	}
	
	public function getVar(){
		if($this->query!=null){
			foreach($this->query->getFilters() as $filter){
				foreach($filter->getVar() as $key => $val){
					$this->var[$key] = $val;
				}
			}
		}
		return $this->var;
	}
	
	public function operator($operator){
		$this->operator = $operator;
		return $this;
	}
	
	//Alias for $this->operator(_OO::IN);
	public function in(){
		$this->operator = _OO::IN;
		return $this;
	}
	
	//Alias for $this->operator(_OO::LIKE);
	public function like(){
		$this->operator = _OO::LIKE;
	}
	
	public function greaterEQthan(){
		$this->operator = _OO::GREATER_EQ;
	}
	
	public function lessThan(){
		$this->operator = _OO::LESS;
	}
	
	public function lessEQthan(){
		$this->operator = _OO::LESS_EQ;
	}
	
	public function type($type){
		$this->type = $type;
		return $this;
	}
	
	public function union($union){
		$this->union = $union;
	}
	
	public function compareTo($compare){
		$this->second = $compare;
		if($compare==null || strtoupper($compare)=="NULL"){
			if($this->query!=null && ($this->operator==_OO::IN || $this->operator==_OO::NOT_IN)){
				$this->operator = _OO::NOT_IN;
			}
			else{
				if($this->fromSubQuery){
					$this->publish(false);
				}
				else{
					$this->operator = _OO::IS_NULL;
					$this->second = null;
				}
			}
		}
		$a = explode(",",$compare);
		if(count($a)>1){
			$this->multiple_var = $a;
		}
		
		
		if(startsWith($this->second,"NOT_")){
			$this->second = str_ireplace("NOT_","",$this->second);
			$this->negateOperator();
		}
		$this->type = $this->getType();
	}
	
	public function query($sql){
		$this->query =  new Query($sql,null,true);
		return $this->query;
	}
	
	public function mount($addUnion = true){
		if(!$this->publish) return "";
		
		$this->var = array(); //Empty vars array.
		if($this->query!=null){
			$this->mount = $this->subQuery();
		}
		else{
			$this->mount = $this->render();
		}
		return $this->mount.($addUnion? " "._OU::AND_." ":"");
	}
	
	public function manual($callback){
		if(is_callable($callback)){
			$this->manual_callback = $callback;
			$this->manual = true;
		}
		else{
			throw new AppException(Status::S5_InternalServerError,ErrorCode::Q_FILTER_MANUAL);
		}
	}
	
	public function isManual(){
		return $this->manual;
	}
	
	public function getManual($vars){
		if(is_callable($this->manual_callback)){
			$rsp = $this->manual_callback;
			$a = explode(",",$vars);
			if(count($a)>1){
				$vars = $a;
			}
			else{
				$vars = array($vars);
			}
			$this->manual_response =  $rsp($vars);
			return true;
		}
		else{
			return false;
		}
		
	}
	
	/**
	 * This filter may have the available values from it. In that case,
	 * returns it.
	 */
	public function getAvailableValues(){
		return $this->defaultValues;
	}
	
	/**
	 * If called, assign the oposite operator.
	 * NULL -> NOT NULL | IN -> NOT IN | ...
	 */
	private function negateOperator(){
		switch($this->operator){
			case _OO::CONTAINS:
				$this->operator = _OO::NOT_CONTAINS;
				break;
			case _OO::EQUAL:
				$this->operator = _OO::NOT_EQUAL;
				break;
			case _OO::GREATER:
				break;
			case _OO::GREATER_EQ:
				break;
			case _OO::IN:
				$this->operator = _OO::NOT_IN;
				break;
			case _OO::IS_NULL:
				$this->operator = _OO::NOT_NULL;
				break;
			case _OO::LESS:
				break;
			case _OO::LESS_EQ:
				break;
			case _OO::LIKE:
				$this->operator = _OO::NOT_LIKE;
				break;
			case _OO::NOT_EQUAL:
				$this->operator = _OO::EQUAL;
				break;
			case _OO::NOT_IN:
				$this->operator = _OO::IN;
				break;
			case _OO::NOT_NULL:
				$this->operator = _OO::IS_NULL;
				break;
		}
		
		
	}
	
	private function subQuery(){
		//if(!$this->query->isSubQuery) return false;
		$subQuery = "";
		if($this->union==_OU::AND_ && ($this->operator==_OO::IN || $this->operator==_OO::NOT_IN) && $this->multiple_var!=null){
			$tot_vars = count($this->multiple_var);
			for($i=0;$i<$tot_vars;$i++){
				$subQuery .= $this->makeQuery($this->multiple_var[$i]).(($i+1<$tot_vars)? (" "._OU::AND_." "):"");
			}
		}
		else{
			$subQuery = $this->makeQuery($this->second);
		}
		return $subQuery;
		
	}
	
	private function makeQuery($vars){
		$addAND = false;
		if(strpos($this->query->userSQL,"WHERE")!==false){
			$a = explode("WHERE",$this->query->userSQL);
			if(count($a)==2 && strlen($a[1])>5){
				$addAND = true;
			}
		}
		$filters = $addAND? " AND ":"";
		/** @var $filter filter*/
		foreach($this->query->getFilters() as $filter){
			$filter->publish();
			$filter->compareTo($vars);
			$filters .= $filter->mount();
		}
		$filters = rtrim($filters);
		if(endsWith($filters,_OU::AND_)){
			$filters = str_lreplace(_OU::AND_, "", $filters);
		}
		if(endsWith($filters,_OU::OR_)){
			$filters = str_lreplace(_OU::OR_, "", $filters);
		}
		
		return $this->first." ".$this->operator." (".$this->query->userSQL.$filters.") ";
	}
	
	/**
	 * Check if for each filter and for  the specific compared value, if it needs to create a multiple filters (department=5 AND department=6 AND ...) or if it only 
	 * needs one filter (department IN ('5','7'), depending the type of required operator, the type of the vars and the type of union.
	 */
	private function needMultipleFields(){
		return ($this->multiple_var!=null && $this->operator!=_OO::IN && $this->operator!=_OO::NOT_IN) || 
		($this->multiple_var!=null && $this->union==_OU::AND_ && ($this->operator==_OO::IN || $this->operator==_OO::NOT_IN));
	}
	
	private function render(){
		$render = "";
		if($this->isManual()){
			$tot_var  = count($this->manual_response);
			$render = "(";
			for($i=0;$i<$tot_var;$i++){
				$filter = $this->manual_response[$i];
				$render .= $this->renderFilter($this->alias."_MANUAL_VAR_".$i,$filter["value"],$filter["field"]).(($i+1<$tot_var)? (" ".$this->union." "):"");
			}
			$render .=")";
		}
		else{
			if($this->needMultipleFields()){
				$tot_var  = count($this->multiple_var);
				$render = "(";
				for($i=0;$i<$tot_var;$i++){
					$render .=  $this->renderFilter($this->alias."_".$i,$this->multiple_var[$i]).(($i+1<$tot_var)? (" ".$this->union." "):"");
				}
				$render .= ")";
			}
			else{
				$render =  $this->renderFilter($this->alias,$this->second);
			}	
		}
		
		
		return $render;
	}
	
	private function renderFilter($varName,$var,$first = null){
		if($first == null){
			$first = $this->first;
		}
		$varName = \randomString::getRandom();
		
		$render = "";
		
		/**
		 * If type is date range, we have to add to filters.
		 * FROM>= DATE and TO<DATE
		 */
		if($this->type==_OT::DATERANGE){
			$ranges = explode("-",$this->second);
			$from = $ranges[0];
			$to = $ranges[1];
			$fromVN = \randomString::getRandom();
			$toVN = \randomString::getRandom();
			$render = "(".$first._OO::GREATER_EQ.':'.$fromVN." "._OU::AND_." ".$first._OO::LESS.':'.$toVN.")";
			$this->var[$fromVN] = $from;
			$this->var[$toVN] = $to;
		}
		else{
			switch ($this->operator) {
				case _OO::LIKE:
					$render = "upper(".$first.") LIKE upper(:$varName)";
					$this->var[$varName] = "%".$var."%";
					break;
				case _OO::IN:
				case _OO::NOT_IN:
					$render = $first." ".$this->operator." (".$this->standarizedVar($var).")";
					break;
				case _OO::EQUAL:
				case _OO::NOT_EQUAL:
				case _OO::GREATER:
				case _OO::LESS:
				case _OO::GREATER_EQ:
				case _OO::LESS_EQ:
					switch($this->type){
						case _OT::DATE:
							$render = $first.$this->operator.":$varName";
							$this->var[$varName] = $var;
							break;
						case _OT::FLOAT:
							$render = $first.$this->operator.":".$varName;
							$this->var[$varName] = array($var,DBCONN_DATATYPE_FLOAT);
							break;
						case _OT::INTEGER:
						case _OT::BOOL:
							$render = $first.$this->operator.":".$varName;
							$this->var[$varName] = $var;
							break;
						case _OT::TEXT:
							$render = "upper(".$first.")".$this->operator."upper(:$varName)";
							$this->var[$varName] = $var;
							break;
					}
					break;
				case _OO::IS_NULL:
				case _OO::NOT_NULL:
					$render = $first." ".$this->operator;
					$this->var[$varName] = $var;
					break;
				case _OO::CONTAINS:
					break;
			}
		}
		return $render;
	}
	
	private function standarizedVar($var){
		$a = explode(",",$var);
		$ret = "";
		if(count($a)>1){
			foreach($a as $b){
				$ret .= "'".$b."',";
			}
			$ret = rtrim($ret, ",");
		}
		else{
			$ret = "'".$var."'";
		}
		return $ret;
	}
	
	private static function is_Date($str){
		$regexp = "/^(19|20)[0-9][0-9][0-1][0-9][0-3][0-9]T[0-5][0-9][0-5][0-9][0-5][0-9]Z$/"; //Pattern to search a datetime in format: YYYYMMDDTHHMISS±TZ
		return preg_match($regexp,$str);
	}
	
	private static function is_DateRange($str){
		$regexp = "/^(19|20)[0-9][0-9][0-1][0-9][0-3][0-9]T[0-5][0-9][0-5][0-9][0-5][0-9]Z-(19|20)[0-9][0-9][0-1][0-9][0-3][0-9]T[0-5][0-9][0-5][0-9][0-5][0-9]Z$/";
		return preg_match($regexp,$str);
	}
	
	private function getType(){
		if(is_float($this->second)){
			return _OT::FLOAT;
		}
		else if(self::is_Date($this->second)){
			return _OT::DATE;
		}
		else if(self::is_DateRange($this->second)){
			return _OT::DATERANGE;
		}
		else if(is_integer($this->second)){
			return _OT::INTEGER;
		}
		else if(is_bool($this->second)){
			return _OT::BOOL;
		}
		else{
			return _OT::TEXT;
		}
	}
}