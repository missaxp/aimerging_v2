<?php
namespace query;

class database_element{
	protected $table_alias;
	protected $mount;
	protected $type;
	
	
	
	/**
	 * Alias for type(_OT::INTEGER)
	 * @return \query\filter
	 */
	public function integer(){
		$this->type = _OT::INTEGER;
		return $this;
	}
	
	public function date(){
		$this->type = _OT::DATE;
		return $this;
	}
	
	public function datetime(){
		$this->type = _OT::DATETIME;
	}
	
	public function text(){
		$this->type = _OT::TEXT;
	}
	
	public function boolean(){
		$this->type = _OT::BOOL;
	}
	
	public function char(){
		$this->type = _OT::CHAR;
	}
}