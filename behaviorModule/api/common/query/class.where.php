<?php
namespace query;

class where extends database_element{
	private $alias_table_first;
	private $alias_table_second;
	private $first;
	private $second;
	private $operator;
	private $union;
	private $publish = false;
	private $where;
	
	private $dbProvider; //Si MYSQL o ORACLE
	
	
	public function __construct($arg, $dbProvider){
		$this->operator = _OO::EQUAL;
		$this->union = _OU::AND_;
		if(count($arg)>1){
			$this->first = $arg[0];
			$this->compareTo($arg[1]);			
		}
		else{
			$a = explode("=",$arg[0]);
			if(count($a)>1){
				$this->first = $a[0];
				$this->compareTo($a[1]);
			}
			else{
				$this->first = $arg[0];
			}
		}
		$this->first = ($dbProvider==DBCONN_ORACLE? strtoupper($this->first):$this->first);
		
		$a = explode(".",$this->first);
		if(count($a)==2){
			$this->alias_table_first = trim($a[0]);
		}
	}
	
	public function relation($alias){
		return ($this->alias_table_second==null || (in_array($this->alias_table_first,$alias) && in_array($this->alias_table_second,$alias)));
	}

	public function operator($operator){
		$this->operator = $operator;
		return $this;
	}

	public function union($union){
		$this->union = $union;
		return $this;
	}
	
	public function compareTo($compare){
		//$this->second = strtoupper($compare);
		$this->second = $compare;
		$a = explode(".",$this->second);
		if(count($a)==2){
			$this->alias_table_second = trim($a[0]);
		}
		return $this;
	}
	
	public function mount($addUnion = true){
		//if($this->publish) return "";
		if($this->where!=null){
			$this->mount = "(".$this->first." ".(($this->second!=null)? $this->operator." ".$this->second:'')." ".$this->union." ".$this->where->mount(false).")".($addUnion? " "._OU::AND_." ":"");
		}
		else{
			$this->mount = $this->first." ".(($this->second!=null)? $this->operator." ".$this->second:'').($addUnion? " "._OU::AND_." ":"");
		}
		$this->publish = true;
		return $this->mount;
	}
	
	/**
	 * In case of we have a where clause that is a MUST in all Querys, we will add it.
	 */
	public function mandatoryCompare(){
		return ($this->alias_table_second==null? $this->alias_table_first:false);
	}
	
	public function where(){
		$this->where = new self(func_get_args());
		return $this->where;
	}
}