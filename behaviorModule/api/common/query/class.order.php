<?php
namespace query;

class order extends database_element{
	const _DIR_ASC = "ASC";
	const _DIR_DESC = "DESC";
	
	private $alias;
	private $direction;
	private $publish = false;
	
	public function __construct($oname,$order){
		$this->type = _OT::TEXT;
		$this->alias = strtoupper($oname);
		$this->mount = strtoupper($order);
		$this->direction = self::_DIR_ASC;
		$a = explode(".",$order);
		if(count($a)>1){
			$this->table_alias = strtoupper($a[0]);
		}
	}
	
	public function direction($dir){
		if($dir=="+"){
			$this->direction = self::_DIR_ASC;
		}
		else if($dir=="-"){
			$this->direction = self::_DIR_DESC;
		}
	}
	
	public function publish(){
		$this->publish = true;
	}
	
	public function published(){
		return $this->publish;
	}
	
	public function getTable(){
		return $this->table_alias;
	}
	
	public function getAlias(){
		return $this->alias;
	}
	
	public function mount($addSeparator = true){
		if(!$this->publish) return "";
		return $this->alias." ".$this->direction.($addSeparator? ", ":"");
	}

}