<?php

namespace query;

use core\Environment;
use dbconn\dbConn;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;
use core\http\HTTP_METHOD;
use api\crud\User;
use common\Excel;
use api\crud\DepartmentManagement;
use api\crud\AttributesManagement;

class Query {
	protected static $environment;
	/**
	 * Dbcon reference link
	 *
	 * @var dbConn
	 */
	private $dbcon;
	private $options;
	private $maxRows;
	private $currentPage;
	private $sql;
	private $vars;
	private $totalRows;
	private $searchString;
	private $sortfields;
	private $sort;
	private $returnfields;
	private $filtervalues;
	private $searchfields;
	private $filterfields;
	private $filterchoice = null; //Used only with Advanced Search Features. Allows to determine the condition operator between to fields. 
	
	private $from;
	private $i_where;
	private $groupBy = null;
	private $createXLS;
	private $specialVars = array();
	private $debug = false;
	
	public $userSQL = null;
	
	private $interactive = true; 
	
	private $isAdvanced = false;
	
	//NEW PROPRS.
	/**@var array[select]*/
	private $selects = array();
	
	/**@var array[filter]*/
	private $filters = array();
	
	/**@var array[where]*/
	private $where = array();
	
	/**@var array[table]*/
	private $froms = array();
	
	/**@var array[order]*/
	private $orders = array();
	
	private $isSubQuery = false;
	
	private $errors  = array();
	
	private $http_method;
	
	private $called_internally; //Check if this method is called internally or is called by a API request.
	
	/**
	 * Aquesta variable determina que, donat el cas que la taula en qüestio tingui el sitema de permissos granulars,
	 * si habilitem la funcionalitat (el filtratge per departament i/o per usuari/grup) o no.
	 * 
	 * @var boolean
	 */
	private $gps; 
	
	public function __construct($mixed_value = null,dbConn $dbcon = null,$isSubQuery = false) {
		self::$environment = Environment::getInstance();
		$this->debug = self::$environment->development;
		$this->called_internally = false;
		$this->gps = true;
		
		if(is_null($mixed_value) || is_bool($mixed_value)){
			$this->debug = ($mixed_value===null? self::$environment->development:$mixed_value);
		}
		else if(is_string($mixed_value) && startsWith($mixed_value,"SELECT")){
			$this->userSQL = $mixed_value;
		}
		else if(is_array($mixed_value) && $mixed_value!=null){ //Default returnfield.
			$this->called_internally = true;
			$this->defaultReturn($mixed_value);
		}
		else{
			$this->debug = false;
		}
		
		// If we assign a diferent instance of dbcon, we priorize them
		if ($dbcon != null) {
			$this->dbcon = $dbcon;
		} else { // If not, we assign the default environment connection.
			$this->dbcon = self::$environment->dbcon;
		}
		
		$this->maxRows = (int) self::$environment->maxRows; //This value may be lower due request needs, but never higher.
		
		$this->isSubQuery = $isSubQuery;
		$this->vars = array ();
	}
	
	public function called_internally(){
		$this->called_internally = true;
	}
	
	/**
	 * Set default returnFields
	 * When this method is called, clear all previous (or via $_GET) set returnfields.
	 * @param array $return;
	 */
	public function defaultReturn($return = array()){
		if(!is_array($return) || $return==null) return;
		
		$this->called_internally = true;
		$this->returnfields = array();
		foreach($return as $field){
			$f = strtoupper($field);
			if(in_array($f,$this->returnfields)){
				throw new AppException (Status::S4_PreconditionFailed, ErrorCode::Forbidden, "Field $field ambiguously defined." );
			}
			else{
				$this->returnfields[] = $f;
			}
		}
	}
	
	/**
	 * Set default order
	 * When this method is called, clear all previous (or via $_GET) set orderfields.
	 * @param array $return;
	 */
	public function defaultOrder($orders){
		if(!is_array($orders) && !is_string($orders) || $orders==null || $orders=="") return;
		$this->called_internally = true;
		$this->sortfields = array();
		if(is_array($orders)){
			$this->sortfields = $orders;
		}
		else if(is_string(($orders))){
			$this->sortfields = array($orders);
		}
	}
	
	public function gps($gps){
		$this->gps = $gps;
		return $this->gps;
	}
	
	
	
	public function getSQL(){
		return $this->userSQL;
	}
	
	public function setInteractive($interactive = true){
		$this->interactive = $interactive;
	}
	
	public function execute($distinct = false,$showSQL = false){
		return $this->invokeDB($distinct,$showSQL,false);
	}
	
	public function paging($distinct = false, $showSQL = false) {
		return $this->invokeDB($distinct,$showSQL,true);
	}
	
	
	/**
	 * You can call any times you want to add a new return value on the response.
	 * You can add a value using different ways:
	 * 
	 * Way 1: select("DB_FIELD AS FIELD_NAME, DB_FIELD2 AS FIELD_NAME2,..:");
	 * 
	 * Way 2: select("DB_FIELD,DB_FIELD2, DB_FIELD3,...");
	 * 
	 * Way 3: select("FIELD_NAME","DB_FIELD");
	 * 
	 * The SQL will be:
	 * 
	 * SELECT DB_FIELD AS FIELD_NAME,... in any of the 3 cases.
	 * @return Query 
	 */
	public function select(){
		$arg = func_get_args();
		$rowName = $arg[0];
		$recordRow = isset($arg[1])? $arg[1]:null;
		if($recordRow!=null){ //If select is called with two params:
			$this->selects[] = new select(strtoupper($rowName), ($this->dbcon->provider==DBCONN_ORACLE? strtoupper($recordRow):$recordRow));
		}
		else{ //if it is called with one param
			$s = explode(",",$rowName);
			foreach($s as &$sel){
				$sel = str_replace(" as "," AS ", $sel);
				$sel = ($this->dbcon->provider==DBCONN_ORACLE? strtoupper($sel):$sel);
				$sle = explode("AS",$sel);
				$csle = count($sle);
				if($csle==2){ //If the contains "AS" 
					$this->selects[] = new select(trim($sle[1]), trim($sle[0]));
				}
				else{ //If not contains "AS"
					$sledot = explode(".",$sle[0]); //If has table identifier [TABLE].[FIELD]
					if(count($sledot)==2){
						$this->selects[] = new select(trim($sledot[1]), trim($sle[0]));
					}
					else{
						$this->selects[] = new select(trim($sle[0]), trim($sle[0]));
					}
					
				}
			}	
		}
		return $this;
	}

	public function from($t){
		$this->from = $t;
		$t = ($this->dbcon->provider==DBCONN_ORACLE? strtoupper($t):$t);
		$tables = explode(",",$t);
		if(count($tables)>1){
			foreach($tables as $table){
				$ta = preg_replace('!\s+!', ' ', trim($table));
				$ta = explode(" ",$ta);
				$this->froms[] = new table($ta[1],$ta[0]);
			}	
		}
		else{
			$ta = preg_replace('!\s+!', ' ', trim($this->from));
			$tab = explode(" ",$ta);
			if(count($tab)>1){
				$this->froms[] = new table($tab[1],$tab[0]);
			}
			else{
				$this->froms[] = new table($ta);
			}
		}
		return $this;
	}
	
	public function where(){
		$where = new where(func_get_args(), $this->dbcon->provider);
		$this->where[] = $where;
		return $where;
	}

	public function getFilters(){
		return $this->filters;
	}
	
	/**
	 * 
	 * @param string $alias the visible alias to make a request (like ... NOMUSUARI)
	 * @param string $condition the database field to get the data (like USER.USERNAME)
	 * @param unknown $values the default values for this filter (or null).
	 */
	public function filter($alias,$condition = null,$values = null){
		$condition = $condition==null? $alias:$condition;
		$filter = new filter($alias,$condition,$this->isSubQuery,$values, $this->dbcon->provider);
		$this->filters[] = $filter;
		return $filter;
	}
	
	public function order($oname,$order,$otype = _OT::TEXT){
		$order = new order($oname,$order);
		$this->orders[] = $order;
		return $order;
	}
	
	public function groupBy($groupBy){
		$a = explode(",",$groupBy);
		foreach($a as $group){
			$this->groupBy[] = new group($group);
		}
		return $this;
	}
	
	private function getInnerJoin($op){
		return ($op==_OU::OR_)? _OU::OR_:_OU::AND_;
	}
	
	private function addVars($var){
		if($var!=null){
			$tot_var = count($var);
			foreach($var as $varName => $value){
				if($value!=null){
					if(isset($this->vars[$varName])){
						throw new AppException ( Status::S5_InternalServerError, ErrorCode::InternalServerError, "Error this var is already set. " . $varName . " => " . $value );
					}
					$this->vars[$varName] = $value;
				}
			}
		}
	}
	
	/**
	 * If we get a search get info, we generate the right sql.
	 * @return string
	 */
	private function getSearchSQL(){
		$search_s = "";
		if ($this->searchfields != null && $this->searchString != null) {
			$count_fields = count ( $this->searchfields );
			$i = 0;
			$search_s .= "(";
			WHILE ( $i < $count_fields ) {
				$db_column = $this->getDBColumn ( $this->searchfields [$i] );
				$search_s .= "upper(".$db_column->getFirst().") LIKE  '%'||:SEARCHSTRING||'%' " . (($i + 1 != $count_fields) ? "OR " : " ");
				$i ++;
			}
			$search_s .= ") ";
			$this->specialVars["SEARCHSTRING"] = strtoupper ( $this->searchString );
		}
		return $search_s;
	}

	private function invokeDB($distinct,$showSQL,$paging){
		if(!$this->isSubQuery && !$this->called_internally){
			$this->getOptionalInfo(); //Get queryString data.
		}
		
		//Fist I check if returnfields array is null. If is null, i fill it with all the select data.
		if($this->returnfields==null){
			foreach($this->selects as $select){
				$this->returnfields[] = $select->getAlias();
			}
		}
		
		foreach($this->froms as $table){
			$tableObj = self::$environment->tables->_get($table->getName(),false);
			if($tableObj!==false){
				//Per a tota consulta a la db que la taula tingui departament, afegim el departament com a retorn i com a filtre..
				if($tableObj->hasDepartment){
					$this->select("OWNER_DEPARTMENT","(SELECT NAME FROM DEPARTMENTS WHERE ID=".$table->getAlias().".DEPARTMENT_ID)");
					$this->filter("OWNER_DEPARTMENT",$table->getAlias().".DEPARTMENT_ID",DepartmentManagement::getDepartments(false,array("ID","NAME")));
				}
				//Si la taula té atributs dinàmics, posem els filtres per atributs dinàmics.
				if($tableObj->hasTags){
					$this->filter("TAGS","C.ID")->in()->query("SELECT ELEMENT_ID FROM TAGS_RELATION WHERE TABLE_ID=".$tableObj->id)->filter("TAG_ID")->in();
					$dynAttr = AttributesManagement::getTableAttributes($tableObj->id);
					foreach($dynAttr as $attr){
						$this->filter("DYNATTR_".$attr["attr_id"],"TP.ID")->
						in()->
						query("SELECT ELEMENT_ID FROM ATTRIBUTES_VALUES AV WHERE AV.ELEMENT_ID=TP.ID AND AV.TABLE_ID=".$tableObj->id." AND AV.ATTR_ID=".$attr["attr_id"])->
						filter("AV.VALUE")->like();
					}
				}
			}
		}
		
		if($this->http_method==HTTP_METHOD::OPTIONS){
			return $this->returnOptions();
		}
		
		if($this->userSQL!=null){
			$this->sql = $this->userSQL;
		}
		else{
			if($this->interactive){
				$this->sql= $this->generateiSQL($distinct);
			}
			else{
				$this->sql = $this->generateSQL($distinct);
			}
		}
		
		if ($this->specialVars != null) {
			$this->vars = array_merge ( $this->vars, $this->specialVars);
		}
		
		if($showSQL){
			die($this->sql);
		}
		
		$vars = $this->vars; //Això ho fem per si execute fa unset $vars (pq ho pilla per ref).
		
		// Obtain the total ROWS on the SQL request
		if($paging && !$this->createXLS){
			try{
				$sql_count = "select count(1) as NUM_FILES from (" . $this->sql . ")";
				if($this->dbcon->provider==DBCONN_MYSQL){
					$sql_count .= " as c";
				}
				$rs = $this->dbcon->execute ($sql_count, $this->vars);
				$rs->fetch();
				$this->totalRows = $rs->num_files;
				$rs->close();
			}
			catch(\Exception $e){
				throw new AppException ( Status::S5_InternalServerError, ErrorCode::DBQueryError, "Query execute error: ".$e->getMessage () );
			}
		}
		// Get the SQL request paginated
		$nregistres = 0;
		$npagines = 0;
		unset ($fetch);
		$this->vars = $vars;
		
		if($paging  && !$this->createXLS){
			$rs = $this->dbcon->paging($this->sql,$this->currentPage,$this->maxRows,$nregistres,$npagines,$this->vars);
		}
		else{
			$rs = $this->dbcon->execute($this->sql,$this->vars);
		}
		
		$fetch = $rs->getAll();
		$rs->close();
		
	
		if ($this->createXLS) {
			return Excel::createXLSfromData($fetch);
		}
		return $this->cleanReturn($fetch,$paging);
	}
	
	/**
	 * El 20160905 he canviat el $talias. per ($talias!==false? $talias.".":"")
	 * ja que $talias pot ser false i llavors el string quedava .FIELD_NAME en comptes de FIELD_NAME.
	 * @param unknown $talias
	 * @param unknown $tid
	 * @return string
	 */
	private function filterByPermission($talias,$tid){
		$chmod = self::$environment->security->table($tid)->visibility(); //Get Table permission by user group.
		//Per defecte, consulta més restrictiva.
		$permissionSQL = "(".($talias!==false? $talias.".":"")."USER_ID=-45"; //TODO : Per testejar. Donat aquest cas no s'ha d'executar cap consulta a DB.
		switch($chmod){
			case "000": //No veig res.
				break;
			case "001": //Que siguin d'altres grups, però no del meu, independentment de l'usuari propietari.
				$permissionSQL = "(".($talias!==false? $talias.".":"")."GROUP_ID!=:SPV_PGID";
				$this->specialVars['SPV_PGID'] = self::$environment->user->user_group;
				break;
			case "010": //Que el grup sigui el de l'usuari, però l'usuari propietari no sigui l'usuari.
				$permissionSQL = "(".($talias!==false? $talias.".":"")."GROUP_ID=:SPV_PGID";
				$this->specialVars['SPV_PGID'] = self::$environment->user->user_group;
				break;
			case "100": //Que l'usuari pugui llegir-les, independentment del grup que tingui. OK (PHIDALGO I JBALLUS)
				$permissionSQL = "(".($talias!==false? $talias.".":"")."USER_ID=:SPV_PUID";
				$this->specialVars['SPV_PUID'] = self::$environment->user->id;
				break;
			case "101": //Que siguin del meu usuari o que siguin d'altres grups que no sigui el meu.
				$permissionSQL = "(".($talias!==false? $talias.".":"")."USER_ID=:SPV_PUID OR $talias.GROUP_ID!=:SPV_PGID";
				$this->specialVars['SPV_PUID'] = self::$environment->user->id;
				$this->specialVars['SPV_PGID'] = self::$environment->user->user_group;
				break;
			case "110": //Que siguin del meu grup o que siguin del meu usuari.
				$permissionSQL = "(".($talias!==false? $talias.".":"")."USER_ID=:SPV_PUID OR $talias.GROUP_ID=:SPV_PGID";
				$this->specialVars['SPV_PUID'] = self::$environment->user->id;
				$this->specialVars['SPV_PGID'] = self::$environment->user->user_group;
				break;
			case "011": //Independentment del grup que tingui
			case "111": //Ho veig tot ja sigui del meu usuari, o del meu grup o d'altres grups (no cal aplicar filtre de permisos).
				$permissionSQL = "";
				break;
		}
		
		//Cas especial taula usuaris. Independentment dels permisos... hem de retornar el nostre usuari.
		if($tid==\common\Table::USERS){
			$permissionSQL .= ($permissionSQL==""? "":" OR ".($talias!==false? $talias.".":"")."ID=".self::$environment->user->id.($permissionSQL==""? "":")"));
		}
		else{
			$permissionSQL .=$permissionSQL==""? "":")";
				
		}
		return $permissionSQL;
	}
	
	private function cleanReturn($fetch,$paging){
		// Removes the R__ column added on the recordset
		foreach ( $fetch as &$row ) {
			if(isset($row["r__"])){
				unset ($row ["r__"]);
			}
		}
		$count = count ($fetch);
		if(!$this->debug){
			$rsp["debug"]["paging"] = $paging;
			$rsp["debug"]["interactive"]["enabled"] = $this->interactive;
			$rsp["debug"]["interactive"]["error"] = $this->getLastError();
			$rsp["debug"]["sql"] = dbConn::tractaSQL($this->sql);
			$rsp["debug"]["vars"] = $this->vars;
			$rsp["debug"]["isAdvanced"] = $this->isAdvanced;
			$rsp["debug"]["provider"] = $this->dbcon->provider;
			//$rsp["debug"]["visibility"] = self::$environment->security->table($tid)->visibility(); //Get Table permission by user group.
		}
		if($paging){
			$rsp["query"]["page"] = $this->currentPage;
			$rsp["query"]["limit"] = $this->maxRows;
			$rsp["query"]["total"] = (int) ($this->totalRows);
			$rsp["query"]["pages"] = ceil($this->totalRows/$this->maxRows);
			$rsp["query"]["count"] = $count;
		}
		
		
		if(!$this->called_internally){
			//Si cridem a query amb una consulta SQL directament, $this->returnfields val null. No podem omplir això.
			if($this->userSQL==""){
				$f = array();
				foreach($this->returnfields as $rf){
					$s = array();
					$sel = $this->getDBField($rf);
					$s["field"] = strtolower($rf);
					$s["type"] = $sel->getType();
					$f[] = $s;
				}
				$rsp["query"]["fields"]["returned"] = $f;
			}
			
			
			$f = array();
			foreach($this->selects as $select){
				$f[] = strtolower($select->getAlias());
			}

			$rsp["query"]["fields"]["available"] = $f;
			
			$fall = array();
			foreach($this->filters as $filter){
				$f = array();
				$f["name"] = strtolower($filter->getAlias());
				$f["values"] = $filter->getAvailableValues();
				$fall[] = $f;
					
			}
			$rsp["query"]["filters"]["available"] = $fall;
			
			$f = array();
			foreach($this->orders as $order){
				$f[] = strtolower($order->getAlias());
			}
			$rsp["query"]["sortables"]["available"] = $f;
		}

		$rsp["results"] = $fetch;
		unset($fetch);
		return $rsp;
	}
	
	private function generateiSQL($distinct){
		$must_tables = array();
		$sql = "SELECT ".($distinct? "distinct ":"");
		$count_select = count($this->returnfields);
		$fields = "";
		for($i=0;$i<$count_select;$i++){
			$select = $this->getDBField($this->returnfields[$i]);
			$talias = $select->getTable();
			if($talias == null) $this->addError($select);
			if(!in_array($talias,$must_tables)){
				array_push($must_tables,$talias);
			}
			if(in_array($select->getAlias(),$this->returnfields)){
				$fields .= $select->mount();
			}
		}
		$fields = rtrim($fields,",");
		
		/**
		 * If we get a filters get info, we generate the right sql.
		 */
		$filters = "";
		if ($this->filterfields != null && $this->filtervalues != null){
			$count_fields = count($this->filterfields);
			for($i=0;$i<$count_fields;$i++){
				$filter = $this->getDBColumn($this->filterfields[$i]);
				if($filter->isManual()){
					if($this->filtervalues[$i]!=null && $this->filtervalues[$i]!="null"){
						$rsp = $filter->getManual($this->filtervalues[$i]);
						if(!$rsp) $this->addError($filter);
						if($this->isAdvanced){
							$filter->union($this->getInnerJoin($this->filterchoice[$i]));
						}
						$filter->publish();
						$filters .= $filter->mount(($i+1<$count_fields));
						$this->addVars($filter->getVar());
					}
					else{
						$this->addError($filter->getAlias()." can not be null");
					}
				}
				else{
					$talias = $filter->getTable();
					if($talias == null) $this->addError($filter);
					if(!in_array($talias,$must_tables)){
						array_push($must_tables,$talias);
					}
					$filter->compareTo($this->filtervalues[$i]);
					if($this->isAdvanced){
						$filter->union($this->getInnerJoin($this->filterchoice[$i]));
					}
					$filter->publish();
					$filters .= $filter->mount(($i+1<$count_fields));
					$this->addVars($filter->getVar());
				}
			}
		}
		
		/**
		 * If we get a order get info, we apply them into the SQL.
		 * No ho passo com a parametre, però com que ho tinc filtrat per un array, no em poden injectar CODI.
		 */
		$order_st = "";
		if ($this->sortfields != null) {
			$count_fields = count($this->sortfields);
			$order_st = " ORDER BY ";
			for($i=0;$i<$count_fields;$i++){
				$direction = substr($this->sortfields[$i], 0, 1);
				$alias = ltrim($this->sortfields[$i],$direction);
				$order = $this->getDBOrder($alias);
				$order->direction($direction);
				$order->publish();
				$talias = $order->getTable();
				//if($talias == null) $this->addError($order); //ORDER MAY BE USE ALIAS INSTEAD OF ROW FIELD.
				if($talias!=null && !in_array($talias,$must_tables)){
					array_push($must_tables,$talias);
				}
				$order_st .= $order->mount(($i+1<$count_fields));
				if(!in_array($order->getAlias(),$this->returnfields)){
					$select = $this->getDBField($order->getAlias());
					$fields .= ",".$select->mount(false);
				}
			}
		}
		
		$group_by_s = "";
		if($this->groupBy!=null){
			$group_by_s = " GROUP BY ";
			$count_group = count($this->groupBy);
			for($i=0;$i<$count_group;$i++){
				$talias = $this->groupBy[$i]->getTable();
				if($talias == null) $this->addError($this->groupBy[$i]);
				else if(!in_array($talias,$must_tables)){
					array_push($must_tables,$talias);
				}
				$group_by_s .= $this->groupBy[$i]->mount(($i+1<$count_group));
			}
		}
		
		$count_where = count($this->where);
		
		$where_m = "";
		for($i=0;$i<$count_where;$i++){
			$mcompare = $this->where[$i]->mandatoryCompare();
			if($mcompare!==false && !in_array($mcompare,$must_tables)){
				array_push($must_tables,$mcompare);
			}
			
			
			if($this->where[$i]->relation($must_tables)){
				$where_m .= $this->where[$i]->mount();
			}
		}
		$where_m = str_lreplace(" "._OU::AND_." ", " ", $where_m); //Borrem l'últim AND.
		
		if($this->getLastError()!==false){
			$this->vars = array(); //Reinitialize vars array.
			$sql = $this->generateSQL($distinct);
		}
		else{
			
			$count_tables = count($must_tables);
			$tables_s = "";
			/**
			 * HERE add tables and also filters for Granular Permission System and visibility given by department.
			 */
			$permissionSQL = "";
			$visibilitySQL = "";
			for($i=0;$i<$count_tables;$i++){
				foreach($this->froms as $table){
					if($table->getAlias()==$must_tables[$i]){
						if($this->gps){ //Si el gps esta on (per defecte ho esta) comprovem permisos (GPS) i visibilitat departament
							$tableObj = self::$environment->tables->_get($table->getName(),false);
							if(self::$environment->authentication_required && $tableObj!==false && $tableObj->hasGranularP){
								$permissionSQL .= ($permissionSQL!=""? " AND ":"").$this->filterByPermission($table->getAlias(),$tableObj->id);
								$fields.=", ".$table->getAlias().".USER_ID AS OWNER_UID, ".$table->getAlias().".GROUP_ID AS OWNER_GID";
							}
							
							/**
							 * FILTER BY DEPARTMENT.
							 * USERS CAN HAVE 1 DEPARTMENT OR NONE.
							 * If none (null), no filtering by department.
							 * if he has one, filtering by that department.
							 *
							 * Unless.. auth required is false, so in this case no filtering by department.
							 *
							 */
							if(self::$environment->authentication_required && $tableObj!==false && $tableObj->hasDepartment && self::$environment->user->user_department!=null){
								$visibilitySQL = "(".$table->getAlias().".".\common\Table::DEPARTMENT_COLUMN_NAME."=:SPV_DPTID OR ".$table->getAlias().".".\common\Table::DEPARTMENT_COLUMN_NAME." is null)";
								$this->specialVars["SPV_DPTID"] = self::$environment->user->user_department;
							}	
						}
						$tables_s .= $table->mount();
					}
				}
			}
			$tables_s = trim($tables_s);
			$tables_s = rtrim($tables_s,",");
			$sql .= $fields." FROM ";
			$sql .= $tables_s;
			
			$search_s = $this->getSearchSQL();
			
			if($where_m=="" && $filters=="" && $search_s=="" && $permissionSQL=="" && $visibilitySQL==""){
				$sql.= $group_by_s.$order_st;
			}
			else{
				$sql.= " WHERE ";
				if($where_m!=""){
					$sql.= $where_m;
				}
				if($filters!=""){
					$sql.= (($where_m!="")? " AND ":"").$filters;
				}
				if($search_s!=""){
					$sql.= (($where_m!="" || $filters!="")? " AND ":"").$search_s;
				}
				if($permissionSQL!=""){
					$sql.= (($where_m!="" || $filters!="" || $search_s!="")? " AND ":"").$permissionSQL;
				}
				if($visibilitySQL!=""){
					$sql.= (($where_m!="" || $filters!="" || $search_s!="" || $permissionSQL!="")? " AND ":"").$visibilitySQL;
				}
				$sql.= $group_by_s.$order_st;
			}
		}
		return $sql;
	}
	
	
	
	private function generateSQL($distinct) {
		$sql = "SELECT ".($distinct? "distinct ":"");
		$count_select = count($this->returnfields);
		
		$fields = "";
		for($i=0;$i<$count_select;$i++){
			$select = $this->getDBField($this->returnfields[$i]);
			if(in_array($select->getAlias(),$this->returnfields)){
				$fields .= $select->mount(($i+1<$count_select));
			}
		}
		

		$count_where = count($this->where);
		$where_m = "";
		for($i=0;$i<$count_where;$i++){
			$where_m .= $this->where[$i]->mount(($i+1<$count_where));
		}
		
		$filters = "";
		
		/**
		 * If we get a filters get info, we generate the right sql.
		 */
		if ($this->filterfields != null && $this->filtervalues != null) {
			$count_fields = count($this->filterfields);
			for($i=0;$i<$count_fields;$i++){
				$filter = $this->getDBColumn($this->filterfields[$i]);
				if($filter->isManual()){
					if($this->filtervalues[$i]!=null && $this->filtervalues[$i]!="null"){
						$rsp = $filter->getManual($this->filtervalues[$i]);
						if(!$rsp) $this->addError($filter);
						if($this->isAdvanced){
							$filter->union($this->getInnerJoin($this->filterchoice[$i]));
						}
						$filter->publish();
						$filters .= $filter->mount(($i+1<$count_fields));
						$this->addVars($filter->getVar());
					}
					else{
						$this->addError($filter->getAlias()." can not be null");
					}
				}
				else{
					$filter->compareTo($this->filtervalues[$i]);
					if($this->isAdvanced){
						$filter->union($this->getInnerJoin($this->filterchoice[$i]));
					}
					$filter->publish();
					$filters .= $filter->mount(($i+1<$count_fields));
					$this->addVars($filter->getVar());
				}
				
			}
		}
		
		/**
		 * If we get a order get info, we apply them into the SQL.
		 * No ho passo com a parametre, però com que ho tinc filtrat per un array, no em poden injectar CODI.
		 */
		$order_st = "";
		if ($this->sortfields != null) {
			$count_fields = count($this->sortfields);
			$order_st = " ORDER BY ";
			for($i=0;$i<$count_fields;$i++){
				$direction = substr($this->sortfields[$i], 0, 1);
				$alias = ltrim($this->sortfields[$i],$direction);
				$order = $this->getDBOrder($alias);
				$order->direction($direction);
				$order->publish();
				$order_st .= $order->mount(($i+1<$count_fields));
				if(!in_array($order->getAlias(),$this->returnfields)){
					$select = $this->getDBField($order->getAlias());
					$fields .= ",".$select->mount(false);
				}
			}
		}
		
		$permissionSQL = "";
		$visibilitySQL = "";
		$count_tables = count($this->froms);
		$tables_sql = "";
		for($i=0;$i<$count_tables;$i++){
			$tables_sql .= $this->froms[$i]->mount(($i+1<$count_tables));
			if($this->gps){
				$tableObj = self::$environment->tables->_get($this->froms[$i]->getName(),false);
				$table_alias = $this->froms[$i]->getAlias();
				if(self::$environment->authentication_required && $tableObj!==false && $tableObj->hasGranularP){
					$permissionSQL .= ($permissionSQL!=""? " AND ":"").$this->filterByPermission($table_alias,$tableObj->id);
					$fields.=", ".($table_alias!==false? ".":"")."USER_ID AS OWNER_UID, ".($table_alias!==false? ".":"")."GROUP_ID AS OWNER_GID";
				}
					
				/**
				 * FILTER BY DEPARTMENT.
				 * USERS CAN HAVE 1 DEPARTMENT OR NONE.
				 * If none (null), no filtering by department.
				 * if he has one, filtering by that department.
				 *
				 * Unless.. auth required is false, so in this case no filtering by department.
				 *
				 */
				if(self::$environment->authentication_required && $tableObj!==false && $tableObj->hasDepartment && self::$environment->user->user_department!=null){
					$visibilitySQL = "(".$this->froms[$i]->getAlias().".".\common\Table::DEPARTMENT_COLUMN_NAME."=:SPV_DPTID OR ".($table_alias!==false? ".":"").\common\Table::DEPARTMENT_COLUMN_NAME." is null)";
					$this->specialVars["SPV_DPTID"] = self::$environment->user->user_department;
				}	
			}
		}
		$sql .= $fields." FROM ".$tables_sql;
		$search_s = $this->getSearchSQL();
		if($where_m!="" || $filters!="" || $search_s!="" || $permissionSQL!="" || $visibilitySQL!=""){
			if($where_m!="" && $filters!=""){
				$where_m .= " AND ";
			}
			if($filters!="" && $search_s!=""){
				$filters .= " AND ";
			}
			if($where_m!="" && $filters!="" && $search_s!=""){
				$permissionSQL = " AND ".$permissionSQL;
			}
			if($where_m!="" && $filters!="" && $search_s!="" && $permissionSQL!=""){
				$visibilitySQL = " AND ".$visibilitySQL;
			}
			$sql .=" WHERE ".$where_m.$filters.$search_s.$permissionSQL.$visibilitySQL;
		}

		$group_by_s = "";
		if($this->groupBy!=null){
			$group_by_s = " GROUP BY ";
			$count_group = count($this->groupBy);
			for($i=0;$i<$count_group;$i++){
				$group_by_s .= $this->groupBy[$i]->mount(($i+1<$count_group));
			}
		}
		$sql .= $group_by_s;
		$sql .= $order_st;
		return $sql;
	}
	
	
	/**
	 * Check if the the requested db alias is allowed to be a return value on the SQL sentence.
	 *
	 * @param String $alias        	
	 * @throws AppException
	 * @return select
	 */
	private function getDBField($alias) {
		$alias = strtoupper($alias);
		/**@var $select select*/
		foreach ($this->selects as $select){
			if($select->getAlias()==$alias){
				return $select;
			}
		}
		throw new AppException(Status::S4_PreconditionFailed, ErrorCode::Forbidden, "Error RETURN FIELD by $alias not allowed" );
	}
	
	/**
	 * Check if the the requested db alias is allowed to be a filter field on SQL sentence (WHERE .
	 *
	 * ..).
	 *
	 * @param String $alias        	
	 * @throws AppException
	 * @return filter
	 */
	private function getDBColumn($alias) {
		$alias = strtoupper(($alias));
		/**@var $filter filter*/
		foreach ( $this->filters as $filter) {
			if ($filter->getAlias()==$alias) {
				return $filter;
			}
		}
		throw new AppException ( Status::S4_PreconditionFailed, ErrorCode::Forbidden, "Error SEARCH or FILTER by $alias not allowed." );
	}
	
	/**
	 * Check if the requested db alias is allowed to be a sortable field on SQL sentence (ORDER BY...)
	 *
	 * @param string $alias        	
	 * @throws AppException
	 * @return order
	 */
	private function getDBOrder($alias) {
		$alias = strtoupper(($alias));
		/**@var $order order*/
		foreach ( $this->orders as $order ) {
			if ($order->getAlias()==$alias) {
				return $order;
			}
		}
		throw new AppException ( Status::S4_PreconditionFailed, ErrorCode::Forbidden, "Error ORDER_BY $alias not allowed." );
	}
	
	/**
	 * Set Optional request values to the sentence, such as paging, search, sort and filter fields given by the Front End.
	 */
	private function getOptionalInfo() {
		$get = self::$environment->request->params ();
		
		$this->http_method = $_SERVER['REQUEST_METHOD'];
		
		$this->currentPage = (isset ( $get ["page"] ) ? ( int ) $get ["page"] : 1);
		$this->maxRows = (isset($get["rowspage"])? ( int ) $get ["rowspage"]:(int) ($this->maxRows));
		
		// The most important is environment->maxRows
		if ($this->maxRows > self::$environment->maxRows) {
			$this->maxRows = self::$environment->maxRows;
		}
		
		if (isset ($get["returnFields"])){
			$this->returnfields = array();
			foreach($get["returnFields"] as $field){
				$f = strtoupper($field);
				if(in_array($f,$this->returnfields)){
					throw new AppException (Status::S4_PreconditionFailed, ErrorCode::Forbidden, "Field $field ambiguously defined." );
				}
				else{
					$this->returnfields[] = $f;
				}
			}
		} 
		
		$this->searchString = (isset ( $get ["searchString"] ) ? strtoupper ( $get ["searchString"] ) : null);
		$this->sortfields =  (isset ( $get ["sortfield"] ) ? explode ( ",", $get ["sortfield"] ) : null);
		$this->sort = (isset ( $get ["sort"] ) ? $get ["sort"] : null);
		$this->filtervalues = (isset ( $get ["filtervalue"] ) ? explode ( "|", $get ["filtervalue"] ) : null);
		$this->searchfields = (isset ( $get ["fields"] ) ? explode ( ",", $get ["fields"] ) : null);
		$this->filterfields = (isset ( $get ["filterfield"] ) ? explode ( ",", $get ["filterfield"] ) : null);
		$this->filterchoice = (isset ( $get ["filterchoice"] ) ? explode ( ",", $get["filterchoice"] ) : null);
		$this->createXLS = (isset ( $get ["toxls"] ) ? ($get ["toxls"] == "yes" ? true : false) : false);
		
		if($this->filterchoice!=null){
			$this->isAdvanced = true;
		}
	}
	
	/**
	 * Can accept both class object and string.
	 * @param unknown $element
	 */
	private function addError($element){
		if(is_object($element)){
			$oname = get_class($element);
			$this->errors[] = "Can not find table alias on ".$element->getAlias()." for element $oname";
		}
		if(is_string($element)){
			$this->errors[] = $element;
		}
	}
	
	
	private function getLastError(){
		if(isset($this->errors[count($this->errors)-1])){
			return $this->errors[count($this->errors)-1];
		}
		return false;
	}
	
	private function returnOptions(){
		self::$environment->response->addData("Services for : ".$_SERVER["REQUEST_URI"]);
		$rsp = array();
		$rsp["ROWS"] = array();
		$rsp["FILTERS"] = array();
		$rsp["ORDER"] = array();
		foreach($this->selects as $select){
			$rsp["ROWS"][] = $select->getAlias();
		}
		
		foreach($this->filters as $filter){
			$rsp["FILTERS"][] = $filter->getAlias();
		}
		
		foreach($this->orders as $order){
			$rsp["ORDER"][] = $order->getAlias();
		}
		return $rsp;
	}
}


abstract class _OT{
	const TEXT = "text";
	const INTEGER = "integer";
	const FUNCT = "function";
	const BOOL = "boolean";
	const DATE = "date";
	const DATETIME = "datetime";
	const DATERANGE = "daterange";
	const FLOAT = "float";
	const CHAR = "char";
}

abstract class _OO{
	const LIKE = "LIKE";
	const NOT_LIKE = "NOT LIKE";
	const IN = "IN";
	const EQUAL = "=";
	const NOT_EQUAL = "!=";
	const GREATER = ">";
	const LESS = "<";
	const GREATER_EQ = ">=";
	const LESS_EQ = "<=";
	const NOT_IN = "NOT IN";
	const IS_NULL = "IS NULL";
	const NOT_NULL = "IS NOT NULL";
	const CONTAINS = "CONTAINS";
	const NOT_CONTAINS = "NOT CONTAINS";
	
	
}

abstract class _OU{
	const AND_ = "AND";
	const OR_ = "OR";
}

?>