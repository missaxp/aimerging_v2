<?php
namespace query;

class group{
	private $field;
	
	public function __construct($group){
		$group = strtoupper($group);
		$a = explode(".",$group);
		if(count($a)==2){
			$this->field = $group;
			$this->table_alias = $a[0];
		}
		else{
			$this->field = $group;
		}
	}
	
	public function mount($addSeparator = true){
		$this->mount = $this->field.($addSeparator? ",":"");
		return $this->mount;
	}
	public function getTable(){
		return $this->table_alias;
	}
}