<?php
namespace query;

class select extends database_element{
	private $alias;
	private $field;
	
	// translation table
	private static $dbTransTable = array (
			"\[BEGINCALL\]((.)*)\[ENDCALL\]",
			"DATE\[C\]",
			"OUTDATE\[([A-Za-z0-9_().]+)\]",
			"OUTDATETIME\[([A-Za-z0-9_().]+)\]",
			"OUTFLOAT\[([A-Za-z0-9_().]+)\]",
			"INDATE\['([0-9\/]+)\']",
			"INDATETIME\['([0-9\/ :]+)\']",
			"IFNULL\[([A-Za-z0-9_() \+\-:,.*']+)\]",
			"CONTAINS\[([A-Za-z0-9_.]+)[ ]*,[ ]*(([A-Za-z0-9_.:])*)\]",
			"LOBLENGTH\[([A-Za-z0-9_.]+)\]",
			"\[RANDOM\]",
			"COUNT\(([A-Za-z0-9_.]+)\)"
	);
	
	public function __construct($alias,$field){
		$this->alias = strtoupper($alias);
		$this->field = $field;
		$this->type = _OT::TEXT;
		
		$a = explode(".",$this->removeORASpecialWords($this->field));
		if(count($a)>1){
			$this->table_alias = $a[0];
		}
		unset($a);
		
		if(startsWith($this->field, "OUTDATETIME[")){
			$this->datetime();
		}
		
		$this->mount();
	}
	
	public function getType(){
		return $this->type;
	}
	
	public function hasTableAlias(){
		return ($this->table_alias!=null)? true:false;
	}
	
	public function getTable(){
		return $this->table_alias;
	}
	
	public function getAlias(){
		return $this->alias;
	}
	
	public function mount($addSeparator = true){
		$this->mount = $this->field." AS ".$this->alias.($addSeparator? ",":"");
		return $this->mount;
	}
	
	public function removeORASpecialWords($obj){
		foreach ( self::$dbTransTable as $trans) {
	//		echo $trans."\r\n";
			$obj = preg_replace ( '/' . $trans . '/i', "\\1", $obj );
		}
//		die();
		return $obj;
	}
}