<?php
class TranslationDocumentDetailedAssignments {
	
	/**
	 *
	 * @var TranslationDocumentDetailedAssignmentInfo[] $Assignments
	 * @access public
	 */
	public $Assignments = null;
	
	/**
	 *
	 * @var guid $DocumentGuid
	 * @access public
	 */
	public $DocumentGuid = null;
	
	/**
	 *
	 * @param guid $DocumentGuid        	
	 * @access public
	 */
	public function __construct($DocumentGuid) {
		$this->DocumentGuid = $DocumentGuid;
	}
}
