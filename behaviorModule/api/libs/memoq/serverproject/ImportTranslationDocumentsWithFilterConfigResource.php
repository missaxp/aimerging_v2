<?php
class ImportTranslationDocumentsWithFilterConfigResource {
	
	/**
	 *
	 * @var guid $serverProjectGuid
	 * @access public
	 */
	public $serverProjectGuid = null;
	
	/**
	 *
	 * @var guid[] $fileGuids
	 * @access public
	 */
	public $fileGuids = null;
	
	/**
	 *
	 * @var string[] $targetLangCodes
	 * @access public
	 */
	public $targetLangCodes = null;
	
	/**
	 *
	 * @var guid $filterConfigResGuid
	 * @access public
	 */
	public $filterConfigResGuid = null;
	
	/**
	 *
	 * @param guid $serverProjectGuid        	
	 * @param guid[] $fileGuids        	
	 * @param string[] $targetLangCodes        	
	 * @param guid $filterConfigResGuid        	
	 * @access public
	 */
	public function __construct($serverProjectGuid, $fileGuids, $targetLangCodes, $filterConfigResGuid) {
		$this->serverProjectGuid = $serverProjectGuid;
		$this->fileGuids = $fileGuids;
		$this->targetLangCodes = $targetLangCodes;
		$this->filterConfigResGuid = $filterConfigResGuid;
	}
}
