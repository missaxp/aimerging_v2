<?php
class ListProjectResourceAssignments {
	
	/**
	 *
	 * @var guid $serverProjectGuid
	 * @access public
	 */
	public $serverProjectGuid = null;
	
	/**
	 *
	 * @var ResourceType $resourceType
	 * @access public
	 */
	public $resourceType = null;
	
	/**
	 *
	 * @param guid $serverProjectGuid        	
	 * @param ResourceType $resourceType        	
	 * @access public
	 */
	public function __construct($serverProjectGuid, $resourceType) {
		$this->serverProjectGuid = $serverProjectGuid;
		$this->resourceType = $resourceType;
	}
}
