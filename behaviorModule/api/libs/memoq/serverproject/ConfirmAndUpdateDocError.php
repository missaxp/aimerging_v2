<?php
class ConfirmAndUpdateDocError {
	
	/**
	 *
	 * @var guid $DocumentGuid
	 * @access public
	 */
	public $DocumentGuid = null;
	
	/**
	 *
	 * @var string $ErrorCode
	 * @access public
	 */
	public $ErrorCode = null;
	
	/**
	 *
	 * @param guid $DocumentGuid        	
	 * @access public
	 */
	public function __construct($DocumentGuid) {
		$this->DocumentGuid = $DocumentGuid;
	}
}
