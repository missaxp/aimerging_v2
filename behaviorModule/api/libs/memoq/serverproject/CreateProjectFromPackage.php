<?php
class CreateProjectFromPackage {
	
	/**
	 *
	 * @var ServerProjectCreateInfo $createInfo
	 * @access public
	 */
	public $createInfo = null;
	
	/**
	 *
	 * @var guid $fileId
	 * @access public
	 */
	public $fileId = null;
	
	/**
	 *
	 * @param ServerProjectCreateInfo $createInfo        	
	 * @param guid $fileId        	
	 * @access public
	 */
	public function __construct($createInfo, $fileId) {
		$this->createInfo = $createInfo;
		$this->fileId = $fileId;
	}
}
