<?php
class ListContentOfPackages {
	
	/**
	 *
	 * @var guid[] $packageIds
	 * @access public
	 */
	public $packageIds = null;
	
	/**
	 *
	 * @param guid[] $packageIds        	
	 * @access public
	 */
	public function __construct($packageIds) {
		$this->packageIds = $packageIds;
	}
}
