<?php
class ReimportDocumentOptions {
	
	/**
	 *
	 * @var guid[] $DocumentsToReplace
	 * @access public
	 */
	public $DocumentsToReplace = null;
	
	/**
	 *
	 * @var guid $FileGuid
	 * @access public
	 */
	public $FileGuid = null;
	
	/**
	 *
	 * @param guid $FileGuid        	
	 * @access public
	 */
	public function __construct($FileGuid) {
		$this->FileGuid = $FileGuid;
	}
}
