<?php
class ServerProjectResourceAssignmentDetails {
	
	/**
	 *
	 * @var string $ObjectId
	 * @access public
	 */
	public $ObjectId = null;
	
	/**
	 *
	 * @var boolean $Primary
	 * @access public
	 */
	public $Primary = null;
	
	/**
	 *
	 * @var LightResourceInfo $ResourceInfo
	 * @access public
	 */
	public $ResourceInfo = null;
	
	/**
	 *
	 * @param boolean $Primary        	
	 * @access public
	 */
	public function __construct($Primary) {
		$this->Primary = $Primary;
	}
}
