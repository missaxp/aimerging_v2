<?php
class TranslationDocumentUserRoleAssignment {
	
	/**
	 *
	 * @var dateTime $DeadLine
	 * @access public
	 */
	public $DeadLine = null;
	
	/**
	 *
	 * @var int $DocumentAssignmentRole
	 * @access public
	 */
	public $DocumentAssignmentRole = null;
	
	/**
	 *
	 * @var guid $UserGuid
	 * @access public
	 */
	public $UserGuid = null;
	
	/**
	 *
	 * @param dateTime $DeadLine        	
	 * @param int $DocumentAssignmentRole        	
	 * @param guid $UserGuid        	
	 * @access public
	 */
	public function __construct($DeadLine, $DocumentAssignmentRole, $UserGuid) {
		$this->DeadLine = $DeadLine;
		$this->DocumentAssignmentRole = $DocumentAssignmentRole;
		$this->UserGuid = $UserGuid;
	}
}
