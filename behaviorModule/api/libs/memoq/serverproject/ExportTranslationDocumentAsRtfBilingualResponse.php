<?php
class ExportTranslationDocumentAsRtfBilingualResponse {
	
	/**
	 *
	 * @var FileResultInfo $ExportTranslationDocumentAsRtfBilingualResult
	 * @access public
	 */
	public $ExportTranslationDocumentAsRtfBilingualResult = null;
	
	/**
	 *
	 * @param FileResultInfo $ExportTranslationDocumentAsRtfBilingualResult        	
	 * @access public
	 */
	public function __construct($ExportTranslationDocumentAsRtfBilingualResult) {
		$this->ExportTranslationDocumentAsRtfBilingualResult = $ExportTranslationDocumentAsRtfBilingualResult;
	}
}
