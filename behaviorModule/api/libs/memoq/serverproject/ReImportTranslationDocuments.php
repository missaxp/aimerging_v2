<?php
class ReImportTranslationDocuments {
	
	/**
	 *
	 * @var guid $serverProjectGuid
	 * @access public
	 */
	public $serverProjectGuid = null;
	
	/**
	 *
	 * @var ReimportDocumentOptions[] $reimportActions
	 * @access public
	 */
	public $reimportActions = null;
	
	/**
	 *
	 * @var string $importSettingsXML
	 * @access public
	 */
	public $importSettingsXML = null;
	
	/**
	 *
	 * @param guid $serverProjectGuid        	
	 * @param ReimportDocumentOptions[] $reimportActions        	
	 * @param string $importSettingsXML        	
	 * @access public
	 */
	public function __construct($serverProjectGuid, $reimportActions, $importSettingsXML) {
		$this->serverProjectGuid = $serverProjectGuid;
		$this->reimportActions = $reimportActions;
		$this->importSettingsXML = $importSettingsXML;
	}
}
