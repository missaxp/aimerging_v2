<?php
class XTranslateDocInfo {
	
	/**
	 *
	 * @var guid $DocumentGuid
	 * @access public
	 */
	public $DocumentGuid = null;
	
	/**
	 *
	 * @var int $SourceMajorVersion
	 * @access public
	 */
	public $SourceMajorVersion = null;
	
	/**
	 *
	 * @param guid $DocumentGuid        	
	 * @param int $SourceMajorVersion        	
	 * @access public
	 */
	public function __construct($DocumentGuid, $SourceMajorVersion) {
		$this->DocumentGuid = $DocumentGuid;
		$this->SourceMajorVersion = $SourceMajorVersion;
	}
}
