<?php
include_once ('ResultInfo.php');
class AsiaOnlineGetProjectIdsResultInfo extends ResultInfo {
	
	/**
	 *
	 * @var int[] $AOProjectIds
	 * @access public
	 */
	public $AOProjectIds = null;
	
	/**
	 *
	 * @param ResultStatus $ResultStatus        	
	 * @access public
	 */
	public function __construct($ResultStatus) {
		parent::__construct ( $ResultStatus );
	}
}
