<?php
class ServerProjectResourceAssignment {
	
	/**
	 *
	 * @var string $ObjectId
	 * @access public
	 */
	public $ObjectId = null;
	
	/**
	 *
	 * @var boolean $Primary
	 * @access public
	 */
	public $Primary = null;
	
	/**
	 *
	 * @var guid $ResourceGuid
	 * @access public
	 */
	public $ResourceGuid = null;
	
	/**
	 *
	 * @param boolean $Primary        	
	 * @param guid $ResourceGuid        	
	 * @access public
	 */
	public function __construct($Primary, $ResourceGuid) {
		$this->Primary = $Primary;
		$this->ResourceGuid = $ResourceGuid;
	}
}
