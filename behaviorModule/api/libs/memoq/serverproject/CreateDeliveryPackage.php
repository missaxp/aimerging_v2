<?php
class CreateDeliveryPackage {
	
	/**
	 *
	 * @var guid $projectGuid
	 * @access public
	 */
	public $projectGuid = null;
	
	/**
	 *
	 * @var guid[] $documentGuids
	 * @access public
	 */
	public $documentGuids = null;
	
	/**
	 *
	 * @var boolean $returnToPreviousActor
	 * @access public
	 */
	public $returnToPreviousActor = null;
	
	/**
	 *
	 * @param guid $projectGuid        	
	 * @param guid[] $documentGuids        	
	 * @param boolean $returnToPreviousActor        	
	 * @access public
	 */
	public function __construct($projectGuid, $documentGuids, $returnToPreviousActor) {
		$this->projectGuid = $projectGuid;
		$this->documentGuids = $documentGuids;
		$this->returnToPreviousActor = $returnToPreviousActor;
	}
}
