<?php
class UpdateProject {
	
	/**
	 *
	 * @var ServerProjectUpdateInfo $spInfo
	 * @access public
	 */
	public $spInfo = null;
	
	/**
	 *
	 * @param ServerProjectUpdateInfo $spInfo        	
	 * @access public
	 */
	public function __construct($spInfo) {
		$this->spInfo = $spInfo;
	}
}
