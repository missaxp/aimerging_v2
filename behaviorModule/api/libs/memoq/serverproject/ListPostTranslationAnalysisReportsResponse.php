<?php
class ListPostTranslationAnalysisReportsResponse {
	
	/**
	 *
	 * @var PostTranslationAnalysisReportInfo[] $ListPostTranslationAnalysisReportsResult
	 * @access public
	 */
	public $ListPostTranslationAnalysisReportsResult = null;
	
	/**
	 *
	 * @param PostTranslationAnalysisReportInfo[] $ListPostTranslationAnalysisReportsResult        	
	 * @access public
	 */
	public function __construct($ListPostTranslationAnalysisReportsResult) {
		$this->ListPostTranslationAnalysisReportsResult = $ListPostTranslationAnalysisReportsResult;
	}
}
