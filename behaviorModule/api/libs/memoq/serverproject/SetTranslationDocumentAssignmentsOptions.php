<?php
class SetTranslationDocumentAssignmentsOptions {
	
	/**
	 *
	 * @var TranslationDocumentAssignments[] $DocumentAssignments
	 * @access public
	 */
	public $DocumentAssignments = null;
	
	/**
	 *
	 * @var boolean $ThrowFault
	 * @access public
	 */
	public $ThrowFault = null;
	
	/**
	 *
	 * @param boolean $ThrowFault        	
	 * @access public
	 */
	public function __construct($ThrowFault) {
		$this->ThrowFault = $ThrowFault;
	}
}
