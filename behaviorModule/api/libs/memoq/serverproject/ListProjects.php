<?php
class ListProjects {
	
	/**
	 *
	 * @var ServerProjectListFilter $filter
	 * @access public
	 */
	public $filter = null;
	
	/**
	 *
	 * @param ServerProjectListFilter $filter        	
	 * @access public
	 */
	public function __construct($filter) {
		$this->filter = $filter;
	}
}
