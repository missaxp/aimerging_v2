<?php
class CreateProject2 {
	
	/**
	 *
	 * @var ServerProjectDesktopDocsCreateInfo $spInfo
	 * @access public
	 */
	public $spInfo = null;
	
	/**
	 *
	 * @param ServerProjectDesktopDocsCreateInfo $spInfo        	
	 * @access public
	 */
	public function __construct($spInfo) {
		$this->spInfo = $spInfo;
	}
}
