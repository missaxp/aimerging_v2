<?php
class SetTranslationDocumentAssignments {
	
	/**
	 *
	 * @var guid $serverProjectGuid
	 * @access public
	 */
	public $serverProjectGuid = null;
	
	/**
	 *
	 * @var SetTranslationDocumentAssignmentsOptions $options
	 * @access public
	 */
	public $options = null;
	
	/**
	 *
	 * @param guid $serverProjectGuid        	
	 * @param SetTranslationDocumentAssignmentsOptions $options        	
	 * @access public
	 */
	public function __construct($serverProjectGuid, $options) {
		$this->serverProjectGuid = $serverProjectGuid;
		$this->options = $options;
	}
}
