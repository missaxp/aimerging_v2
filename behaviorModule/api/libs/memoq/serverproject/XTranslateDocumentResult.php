<?php
class XTranslateDocumentResult {
	
	/**
	 *
	 * @var guid $DocumentGuid
	 * @access public
	 */
	public $DocumentGuid = null;
	
	/**
	 *
	 * @var int $NumberOfCandidates
	 * @access public
	 */
	public $NumberOfCandidates = null;
	
	/**
	 *
	 * @var int $NumberOfRows
	 * @access public
	 */
	public $NumberOfRows = null;
	
	/**
	 *
	 * @var int $NumberOfXTranslatedRows
	 * @access public
	 */
	public $NumberOfXTranslatedRows = null;
	
	/**
	 *
	 * @param guid $DocumentGuid        	
	 * @param int $NumberOfCandidates        	
	 * @param int $NumberOfRows        	
	 * @param int $NumberOfXTranslatedRows        	
	 * @access public
	 */
	public function __construct($DocumentGuid, $NumberOfCandidates, $NumberOfRows, $NumberOfXTranslatedRows) {
		$this->DocumentGuid = $DocumentGuid;
		$this->NumberOfCandidates = $NumberOfCandidates;
		$this->NumberOfRows = $NumberOfRows;
		$this->NumberOfXTranslatedRows = $NumberOfXTranslatedRows;
	}
}
