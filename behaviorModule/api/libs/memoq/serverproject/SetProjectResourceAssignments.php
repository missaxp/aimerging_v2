<?php
class SetProjectResourceAssignments {
	
	/**
	 *
	 * @var guid $serverProjectGuid
	 * @access public
	 */
	public $serverProjectGuid = null;
	
	/**
	 *
	 * @var ServerProjectResourceAssignmentForResourceType[] $assignments
	 * @access public
	 */
	public $assignments = null;
	
	/**
	 *
	 * @param guid $serverProjectGuid        	
	 * @param ServerProjectResourceAssignmentForResourceType[] $assignments        	
	 * @access public
	 */
	public function __construct($serverProjectGuid, $assignments) {
		$this->serverProjectGuid = $serverProjectGuid;
		$this->assignments = $assignments;
	}
}
