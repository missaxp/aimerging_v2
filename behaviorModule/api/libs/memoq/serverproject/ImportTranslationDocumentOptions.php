<?php
class ImportTranslationDocumentOptions {
	
	/**
	 *
	 * @var string $ExternalDocumentId
	 * @access public
	 */
	public $ExternalDocumentId = null;
	
	/**
	 *
	 * @var guid $FileGuid
	 * @access public
	 */
	public $FileGuid = null;
	
	/**
	 *
	 * @var guid $FilterConfigResGuid
	 * @access public
	 */
	public $FilterConfigResGuid = null;
	
	/**
	 *
	 * @var boolean $ImportEmbeddedImages
	 * @access public
	 */
	public $ImportEmbeddedImages = null;
	
	/**
	 *
	 * @var boolean $ImportEmbeddedObjects
	 * @access public
	 */
	public $ImportEmbeddedObjects = null;
	
	/**
	 *
	 * @var string $ImportSettingsXML
	 * @access public
	 */
	public $ImportSettingsXML = null;
	
	/**
	 *
	 * @var string $PathToSetAsImportPath
	 * @access public
	 */
	public $PathToSetAsImportPath = null;
	
	/**
	 *
	 * @var string[] $TargetLangCodes
	 * @access public
	 */
	public $TargetLangCodes = null;
	
	/**
	 *
	 * @param guid $FileGuid        	
	 * @param guid $FilterConfigResGuid        	
	 * @param boolean $ImportEmbeddedImages        	
	 * @param boolean $ImportEmbeddedObjects        	
	 * @access public
	 */
	public function __construct($FileGuid, $FilterConfigResGuid, $ImportEmbeddedImages, $ImportEmbeddedObjects) {
		$this->FileGuid = $FileGuid;
		$this->FilterConfigResGuid = $FilterConfigResGuid;
		$this->ImportEmbeddedImages = $ImportEmbeddedImages;
		$this->ImportEmbeddedObjects = $ImportEmbeddedObjects;
	}
}
