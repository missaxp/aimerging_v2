<?php
include_once ('DocumentHistoryItemInfo.php');
class DeadlineChangeHistoryItemInfo extends DocumentHistoryItemInfo {
	
	/**
	 *
	 * @var dateTime $NewDeadline
	 * @access public
	 */
	public $NewDeadline = null;
	
	/**
	 *
	 * @var int $Role
	 * @access public
	 */
	public $Role = null;
	
	/**
	 *
	 * @param guid $DocumentOrDivisionGuid        	
	 * @param DocumentHistoryItemType $HistoryItemType        	
	 * @param guid $ModifierSVGroupId        	
	 * @param guid $ModifierUserGuid        	
	 * @param dateTime $TimeStamp        	
	 * @param dateTime $NewDeadline        	
	 * @param int $Role        	
	 * @access public
	 */
	public function __construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp, $NewDeadline, $Role) {
		parent::__construct ( $DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp );
		$this->NewDeadline = $NewDeadline;
		$this->Role = $Role;
	}
}
