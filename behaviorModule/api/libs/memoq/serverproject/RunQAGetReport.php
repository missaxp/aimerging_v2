<?php
class RunQAGetReport {
	
	/**
	 *
	 * @var guid $serverProjectGuid
	 * @access public
	 */
	public $serverProjectGuid = null;
	
	/**
	 *
	 * @var RunQAGetReportOptions $options
	 * @access public
	 */
	public $options = null;
	
	/**
	 *
	 * @param guid $serverProjectGuid        	
	 * @param RunQAGetReportOptions $options        	
	 * @access public
	 */
	public function __construct($serverProjectGuid, $options) {
		$this->serverProjectGuid = $serverProjectGuid;
		$this->options = $options;
	}
}
