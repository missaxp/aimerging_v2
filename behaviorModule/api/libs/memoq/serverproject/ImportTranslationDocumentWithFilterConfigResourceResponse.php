<?php
class ImportTranslationDocumentWithFilterConfigResourceResponse {
	
	/**
	 *
	 * @var TranslationDocImportResultInfo $ImportTranslationDocumentWithFilterConfigResourceResult
	 * @access public
	 */
	public $ImportTranslationDocumentWithFilterConfigResourceResult = null;
	
	/**
	 *
	 * @param TranslationDocImportResultInfo $ImportTranslationDocumentWithFilterConfigResourceResult        	
	 * @access public
	 */
	public function __construct($ImportTranslationDocumentWithFilterConfigResourceResult) {
		$this->ImportTranslationDocumentWithFilterConfigResourceResult = $ImportTranslationDocumentWithFilterConfigResourceResult;
	}
}
