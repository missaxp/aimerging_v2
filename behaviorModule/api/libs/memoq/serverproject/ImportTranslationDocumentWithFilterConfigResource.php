<?php
class ImportTranslationDocumentWithFilterConfigResource {
	
	/**
	 *
	 * @var guid $serverProjectGuid
	 * @access public
	 */
	public $serverProjectGuid = null;
	
	/**
	 *
	 * @var guid $fileGuid
	 * @access public
	 */
	public $fileGuid = null;
	
	/**
	 *
	 * @var string[] $targetLangCodes
	 * @access public
	 */
	public $targetLangCodes = null;
	
	/**
	 *
	 * @var guid $filterConfigResGuid
	 * @access public
	 */
	public $filterConfigResGuid = null;
	
	/**
	 *
	 * @param guid $serverProjectGuid        	
	 * @param guid $fileGuid        	
	 * @param string[] $targetLangCodes        	
	 * @param guid $filterConfigResGuid        	
	 * @access public
	 */
	public function __construct($serverProjectGuid, $fileGuid, $targetLangCodes, $filterConfigResGuid) {
		$this->serverProjectGuid = $serverProjectGuid;
		$this->fileGuid = $fileGuid;
		$this->targetLangCodes = $targetLangCodes;
		$this->filterConfigResGuid = $filterConfigResGuid;
	}
}
