<?php
include_once ('LightResourceInfo.php');
class FilterConfigResourceInfo extends LightResourceInfo {
	
	/**
	 *
	 * @var string $FilterName
	 * @access public
	 */
	public $FilterName = null;
	
	/**
	 *
	 * @param boolean $IsDefault        	
	 * @access public
	 */
	public function __construct($IsDefault) {
		parent::__construct ( $IsDefault );
	}
}
