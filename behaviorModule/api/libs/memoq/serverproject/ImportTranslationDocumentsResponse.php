<?php
class ImportTranslationDocumentsResponse {
	
	/**
	 *
	 * @var TranslationDocImportResultInfo[] $ImportTranslationDocumentsResult
	 * @access public
	 */
	public $ImportTranslationDocumentsResult = null;
	
	/**
	 *
	 * @param TranslationDocImportResultInfo[] $ImportTranslationDocumentsResult        	
	 * @access public
	 */
	public function __construct($ImportTranslationDocumentsResult) {
		$this->ImportTranslationDocumentsResult = $ImportTranslationDocumentsResult;
	}
}
