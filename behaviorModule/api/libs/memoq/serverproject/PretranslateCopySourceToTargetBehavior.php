<?php
class PretranslateCopySourceToTargetBehavior {
	
	/**
	 *
	 * @var PretranslateCopySourceToTargetConditions $Condition
	 * @access public
	 */
	public $Condition = null;
	
	/**
	 *
	 * @var boolean $Lock
	 * @access public
	 */
	public $Lock = null;
	
	/**
	 *
	 * @param PretranslateCopySourceToTargetConditions $Condition        	
	 * @param boolean $Lock        	
	 * @access public
	 */
	public function __construct($Condition, $Lock) {
		$this->Condition = $Condition;
		$this->Lock = $Lock;
	}
}
