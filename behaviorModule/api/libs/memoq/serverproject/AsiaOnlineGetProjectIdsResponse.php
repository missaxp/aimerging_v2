<?php
class AsiaOnlineGetProjectIdsResponse {
	
	/**
	 *
	 * @var AsiaOnlineGetProjectIdsResultInfo $AsiaOnlineGetProjectIdsResult
	 * @access public
	 */
	public $AsiaOnlineGetProjectIdsResult = null;
	
	/**
	 *
	 * @param AsiaOnlineGetProjectIdsResultInfo $AsiaOnlineGetProjectIdsResult        	
	 * @access public
	 */
	public function __construct($AsiaOnlineGetProjectIdsResult) {
		$this->AsiaOnlineGetProjectIdsResult = $AsiaOnlineGetProjectIdsResult;
	}
}
