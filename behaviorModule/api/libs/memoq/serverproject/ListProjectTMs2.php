<?php
class ListProjectTMs2 {
	
	/**
	 *
	 * @var guid $serverProjectGuid
	 * @access public
	 */
	public $serverProjectGuid = null;
	
	/**
	 *
	 * @var string[] $targetLangCodes
	 * @access public
	 */
	public $targetLangCodes = null;
	
	/**
	 *
	 * @param guid $serverProjectGuid        	
	 * @param string[] $targetLangCodes        	
	 * @access public
	 */
	public function __construct($serverProjectGuid, $targetLangCodes) {
		$this->serverProjectGuid = $serverProjectGuid;
		$this->targetLangCodes = $targetLangCodes;
	}
}
