<?php
include_once (ROOT_PATH . '/common/ResourceInfo.php');
class LightResourceInfo extends ResourceInfo {
	
	/**
	 *
	 * @var boolean $IsDefault
	 * @access public
	 */
	public $IsDefault = null;
	
	/**
	 *
	 * @param guid $Guid        	
	 * @param boolean $Readonly        	
	 * @param boolean $IsDefault        	
	 * @access public
	 */
	public function __construct($Guid, $Readonly, $IsDefault) {
		parent::__construct ( $Guid, $Readonly );
		$this->IsDefault = $IsDefault;
	}
}
