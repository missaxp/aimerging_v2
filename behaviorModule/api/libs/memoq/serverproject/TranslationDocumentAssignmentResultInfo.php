<?php
include_once ('ResultInfo.php');
class TranslationDocumentAssignmentResultInfo extends ResultInfo {
	
	/**
	 *
	 * @var string $ErrorCode
	 * @access public
	 */
	public $ErrorCode = null;
	
	/**
	 *
	 * @var TranslationDocumentRoleAssignmentResultInfo[] $RoleAssignmentResults
	 * @access public
	 */
	public $RoleAssignmentResults = null;
	
	/**
	 *
	 * @param ResultStatus $ResultStatus        	
	 * @access public
	 */
	public function __construct($ResultStatus) {
		parent::__construct ( $ResultStatus );
	}
}
