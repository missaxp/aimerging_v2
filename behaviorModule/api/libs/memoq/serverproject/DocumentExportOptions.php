<?php
class DocumentExportOptions {
	
	/**
	 *
	 * @var boolean $CopySourceToEmptyTarget
	 * @access public
	 */
	public $CopySourceToEmptyTarget = null;
	
	/**
	 *
	 * @var boolean $CopySourceToUnconfirmedRows
	 * @access public
	 */
	public $CopySourceToUnconfirmedRows = null;
	
	/**
	 *
	 * @var boolean $RevertFaultyTargetsToSource
	 * @access public
	 */
	public $RevertFaultyTargetsToSource = null;
	
	/**
	 *
	 * @param boolean $CopySourceToEmptyTarget        	
	 * @param boolean $CopySourceToUnconfirmedRows        	
	 * @param boolean $RevertFaultyTargetsToSource        	
	 * @access public
	 */
	public function __construct($CopySourceToEmptyTarget, $CopySourceToUnconfirmedRows, $RevertFaultyTargetsToSource) {
		$this->CopySourceToEmptyTarget = $CopySourceToEmptyTarget;
		$this->CopySourceToUnconfirmedRows = $CopySourceToUnconfirmedRows;
		$this->RevertFaultyTargetsToSource = $RevertFaultyTargetsToSource;
	}
}
