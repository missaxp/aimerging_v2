<?php
class ServerProjectAddLanguageInfo {
	
	/**
	 *
	 * @var boolean $CopyTargetSideFromSourceDocument
	 * @access public
	 */
	public $CopyTargetSideFromSourceDocument = null;
	
	/**
	 *
	 * @var AddProjectLanguageTBHandlingBehavior $TBLanguageHandlingBehavior
	 * @access public
	 */
	public $TBLanguageHandlingBehavior = null;
	
	/**
	 *
	 * @var string $TargetLangCode
	 * @access public
	 */
	public $TargetLangCode = null;
	
	/**
	 *
	 * @var string $TargetLangOfSourceDoc
	 * @access public
	 */
	public $TargetLangOfSourceDoc = null;
	
	/**
	 *
	 * @param boolean $CopyTargetSideFromSourceDocument        	
	 * @param AddProjectLanguageTBHandlingBehavior $TBLanguageHandlingBehavior        	
	 * @access public
	 */
	public function __construct($CopyTargetSideFromSourceDocument, $TBLanguageHandlingBehavior) {
		$this->CopyTargetSideFromSourceDocument = $CopyTargetSideFromSourceDocument;
		$this->TBLanguageHandlingBehavior = $TBLanguageHandlingBehavior;
	}
}
