<?php
class ListProjectResourceAssignmentsForMultipleTypes {
	
	/**
	 *
	 * @var guid $serverProjectGuid
	 * @access public
	 */
	public $serverProjectGuid = null;
	
	/**
	 *
	 * @var ResourceType[] $resourceTypes
	 * @access public
	 */
	public $resourceTypes = null;
	
	/**
	 *
	 * @param guid $serverProjectGuid        	
	 * @param ResourceType[] $resourceTypes        	
	 * @access public
	 */
	public function __construct($serverProjectGuid, $resourceTypes) {
		$this->serverProjectGuid = $serverProjectGuid;
		$this->resourceTypes = $resourceTypes;
	}
}
