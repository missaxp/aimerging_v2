<?php
class ExpectedFinalStateAfterXTranslate {
	const __default = 'SameAsPrevious';
	const SameAsPrevious = 'SameAsPrevious';
	const Pretranslated = 'Pretranslated';
	const Confirmed = 'Confirmed';
	const Proofread = 'Proofread';
	const Reviewer1Confirmed = 'Reviewer1Confirmed';
}
