<?php
include_once ('LightResourceInfo.php');
class PathRuleResourceInfo extends LightResourceInfo {
	
	/**
	 *
	 * @var PathRuleType $RuleType
	 * @access public
	 */
	public $RuleType = null;
	
	/**
	 *
	 * @param boolean $IsDefault        	
	 * @param PathRuleType $RuleType        	
	 * @access public
	 */
	public function __construct($IsDefault, $RuleType) {
		parent::__construct ( $IsDefault );
		$this->RuleType = $RuleType;
	}
}
