<?php
include_once ('DocumentHistoryItemInfo.php');
class DocumentSlicingHistoryItemInfo extends DocumentHistoryItemInfo {
	
	/**
	 *
	 * @var int $NumberOfParts
	 * @access public
	 */
	public $NumberOfParts = null;
	
	/**
	 *
	 * @param guid $DocumentOrDivisionGuid        	
	 * @param DocumentHistoryItemType $HistoryItemType        	
	 * @param guid $ModifierSVGroupId        	
	 * @param guid $ModifierUserGuid        	
	 * @param dateTime $TimeStamp        	
	 * @param int $NumberOfParts        	
	 * @access public
	 */
	public function __construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp, $NumberOfParts) {
		parent::__construct ( $DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp );
		$this->NumberOfParts = $NumberOfParts;
	}
}
