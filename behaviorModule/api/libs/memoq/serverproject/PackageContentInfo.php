<?php
include_once ('PackageInfo.php');
class PackageContentInfo extends PackageInfo {
	
	/**
	 *
	 * @var PackageContentDocument[] $Documents
	 * @access public
	 */
	public $Documents = null;
	
	/**
	 *
	 * @param guid $PackageID        	
	 * @param guid $ProjectGuid        	
	 * @param int $SequenceNumber        	
	 * @param guid $UserGuid        	
	 * @access public
	 */
	public function __construct($PackageID, $ProjectGuid, $SequenceNumber, $UserGuid) {
		parent::__construct ( $PackageID, $ProjectGuid, $SequenceNumber, $UserGuid );
	}
}
