<?php
class PackageContentDocument {
	
	/**
	 *
	 * @var dateTime $Deadline
	 * @access public
	 */
	public $Deadline = null;
	
	/**
	 *
	 * @var guid $DocumentGuid
	 * @access public
	 */
	public $DocumentGuid = null;
	
	/**
	 *
	 * @var string $DocumentName
	 * @access public
	 */
	public $DocumentName = null;
	
	/**
	 *
	 * @var int $Role
	 * @access public
	 */
	public $Role = null;
	
	/**
	 *
	 * @var string $TargetLang
	 * @access public
	 */
	public $TargetLang = null;
	
	/**
	 *
	 * @param dateTime $Deadline        	
	 * @param guid $DocumentGuid        	
	 * @param int $Role        	
	 * @access public
	 */
	public function __construct($Deadline, $DocumentGuid, $Role) {
		$this->Deadline = $Deadline;
		$this->DocumentGuid = $DocumentGuid;
		$this->Role = $Role;
	}
}
