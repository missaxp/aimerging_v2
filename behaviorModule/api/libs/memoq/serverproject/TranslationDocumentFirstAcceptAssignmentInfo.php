<?php
include_once ('TranslationDocumentRoleAssignmentInfo.php');
class TranslationDocumentFirstAcceptAssignmentInfo extends TranslationDocumentRoleAssignmentInfo {
	
	/**
	 *
	 * @var dateTime $FirstAcceptDeadline
	 * @access public
	 */
	public $FirstAcceptDeadline = null;
	
	/**
	 *
	 * @var guid[] $UserGuids
	 * @access public
	 */
	public $UserGuids = null;
	
	/**
	 *
	 * @param int $RoleId        	
	 * @param dateTime $FirstAcceptDeadline        	
	 * @access public
	 */
	public function __construct($RoleId, $FirstAcceptDeadline) {
		parent::__construct ( $RoleId );
		$this->FirstAcceptDeadline = $FirstAcceptDeadline;
	}
}
