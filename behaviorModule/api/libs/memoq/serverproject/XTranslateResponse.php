<?php
class XTranslateResponse {
	
	/**
	 *
	 * @var XTranslateResultInfo $XTranslateResult
	 * @access public
	 */
	public $XTranslateResult = null;
	
	/**
	 *
	 * @param XTranslateResultInfo $XTranslateResult        	
	 * @access public
	 */
	public function __construct($XTranslateResult) {
		$this->XTranslateResult = $XTranslateResult;
	}
}
