<?php
class WorkflowStatus {
	const __default = 'Unknown';
	const Unknown = 'Unknown';
	const TranslationNotStarted = 'TranslationNotStarted';
	const TranslationInProgress = 'TranslationInProgress';
	const Review1NotStarted = 'Review1NotStarted';
	const Review1InProgress = 'Review1InProgress';
	const Review2NotStarted = 'Review2NotStarted';
	const Review2InProgress = 'Review2InProgress';
	const Completed = 'Completed';
}
