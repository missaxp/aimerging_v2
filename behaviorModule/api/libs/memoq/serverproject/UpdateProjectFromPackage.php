<?php
class UpdateProjectFromPackage {
	
	/**
	 *
	 * @var guid $projectGuid
	 * @access public
	 */
	public $projectGuid = null;
	
	/**
	 *
	 * @var guid $fileId
	 * @access public
	 */
	public $fileId = null;
	
	/**
	 *
	 * @param guid $projectGuid        	
	 * @param guid $fileId        	
	 * @access public
	 */
	public function __construct($projectGuid, $fileId) {
		$this->projectGuid = $projectGuid;
		$this->fileId = $fileId;
	}
}
