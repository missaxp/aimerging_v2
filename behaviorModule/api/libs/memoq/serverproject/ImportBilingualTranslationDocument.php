<?php
class ImportBilingualTranslationDocument {
	
	/**
	 *
	 * @var guid $serverProjectGuid
	 * @access public
	 */
	public $serverProjectGuid = null;
	
	/**
	 *
	 * @var guid $fileGuid
	 * @access public
	 */
	public $fileGuid = null;
	
	/**
	 *
	 * @var BilingualDocFormat $docFormat
	 * @access public
	 */
	public $docFormat = null;
	
	/**
	 *
	 * @var string[] $targetLangCodes
	 * @access public
	 */
	public $targetLangCodes = null;
	
	/**
	 *
	 * @param guid $serverProjectGuid        	
	 * @param guid $fileGuid        	
	 * @param BilingualDocFormat $docFormat        	
	 * @param string[] $targetLangCodes        	
	 * @access public
	 */
	public function __construct($serverProjectGuid, $fileGuid, $docFormat, $targetLangCodes) {
		$this->serverProjectGuid = $serverProjectGuid;
		$this->fileGuid = $fileGuid;
		$this->docFormat = $docFormat;
		$this->targetLangCodes = $targetLangCodes;
	}
}
