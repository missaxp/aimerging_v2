<?php
class ListServerProjectTranslationDocument2Options {
	
	/**
	 *
	 * @var boolean $FillInAssignmentInformation
	 * @access public
	 */
	public $FillInAssignmentInformation = null;
	
	/**
	 *
	 * @param boolean $FillInAssignmentInformation        	
	 * @access public
	 */
	public function __construct($FillInAssignmentInformation) {
		$this->FillInAssignmentInformation = $FillInAssignmentInformation;
	}
}
