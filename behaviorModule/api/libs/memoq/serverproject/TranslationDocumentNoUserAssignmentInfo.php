<?php
include_once ('TranslationDocumentRoleAssignmentInfo.php');
class TranslationDocumentNoUserAssignmentInfo extends TranslationDocumentRoleAssignmentInfo {
	
	/**
	 *
	 * @param int $RoleId        	
	 * @access public
	 */
	public function __construct($RoleId) {
		parent::__construct ( $RoleId );
	}
}
