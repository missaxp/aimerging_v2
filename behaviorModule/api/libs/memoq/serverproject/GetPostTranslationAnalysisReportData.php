<?php
class GetPostTranslationAnalysisReportData {
	
	/**
	 *
	 * @var guid $serverProjectGuid
	 * @access public
	 */
	public $serverProjectGuid = null;
	
	/**
	 *
	 * @var guid $reportId
	 * @access public
	 */
	public $reportId = null;
	
	/**
	 *
	 * @param guid $serverProjectGuid        	
	 * @param guid $reportId        	
	 * @access public
	 */
	public function __construct($serverProjectGuid, $reportId) {
		$this->serverProjectGuid = $serverProjectGuid;
		$this->reportId = $reportId;
	}
}
