<?php
class ServerProjectUpdateInfo {
	
	/**
	 *
	 * @var string $CallbackWebServiceUrl
	 * @access public
	 */
	public $CallbackWebServiceUrl = null;
	
	/**
	 *
	 * @var string $Client
	 * @access public
	 */
	public $Client = null;
	
	/**
	 *
	 * @var string $CustomMetas
	 * @access public
	 */
	public $CustomMetas = null;
	
	/**
	 *
	 * @var dateTime $Deadline
	 * @access public
	 */
	public $Deadline = null;
	
	/**
	 *
	 * @var string $Description
	 * @access public
	 */
	public $Description = null;
	
	/**
	 *
	 * @var string $Domain
	 * @access public
	 */
	public $Domain = null;
	
	/**
	 *
	 * @var string $Project
	 * @access public
	 */
	public $Project = null;
	
	/**
	 *
	 * @var guid $ServerProjectGuid
	 * @access public
	 */
	public $ServerProjectGuid = null;
	
	/**
	 *
	 * @var string $Subject
	 * @access public
	 */
	public $Subject = null;
	
	/**
	 *
	 * @param dateTime $Deadline        	
	 * @param guid $ServerProjectGuid        	
	 * @access public
	 */
	public function __construct($Deadline, $ServerProjectGuid) {
		$this->Deadline = $Deadline;
		$this->ServerProjectGuid = $ServerProjectGuid;
	}
}
