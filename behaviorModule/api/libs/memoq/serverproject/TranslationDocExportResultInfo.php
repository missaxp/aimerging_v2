<?php
include_once ('FileResultInfo.php');
class TranslationDocExportResultInfo extends FileResultInfo {
	
	/**
	 *
	 * @var int[] $ErrorSegmentIndices
	 * @access public
	 */
	public $ErrorSegmentIndices = null;
	
	/**
	 *
	 * @param guid $FileGuid        	
	 * @access public
	 */
	public function __construct($FileGuid) {
		parent::__construct ( $FileGuid );
	}
}
