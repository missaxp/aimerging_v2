<?php
class AsiaOnlineGetTranslationStatusResponse {
	
	/**
	 *
	 * @var AsiaOnlineTranslationResultInfo[] $AsiaOnlineGetTranslationStatusResult
	 * @access public
	 */
	public $AsiaOnlineGetTranslationStatusResult = null;
	
	/**
	 *
	 * @param AsiaOnlineTranslationResultInfo[] $AsiaOnlineGetTranslationStatusResult        	
	 * @access public
	 */
	public function __construct($AsiaOnlineGetTranslationStatusResult) {
		$this->AsiaOnlineGetTranslationStatusResult = $AsiaOnlineGetTranslationStatusResult;
	}
}
