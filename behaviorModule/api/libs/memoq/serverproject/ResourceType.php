<?php
class ResourceType {
	const __default = 'AutoTrans';
	const AutoTrans = 'AutoTrans';
	const NonTrans = 'NonTrans';
	const AutoCorrect = 'AutoCorrect';
	const IgnoreLists = 'IgnoreLists';
	const SegRules = 'SegRules';
	const TMSettings = 'TMSettings';
	const FilterConfigs = 'FilterConfigs';
	const KeyboardShortcuts = 'KeyboardShortcuts';
	const PathRules = 'PathRules';
	const QASettings = 'QASettings';
	const Stopwords = 'Stopwords';
	const LQA = 'LQA';
	const LiveDocsSettings = 'LiveDocsSettings';
	const WebSearchSettings = 'WebSearchSettings';
	const FontSubstitution = 'FontSubstitution';
}
