<?php
class ImportImageLocalizationPackResponse {
	
	/**
	 *
	 * @var ImportImageLocalizationPackResultInfo $ImportImageLocalizationPackResult
	 * @access public
	 */
	public $ImportImageLocalizationPackResult = null;
	
	/**
	 *
	 * @param ImportImageLocalizationPackResultInfo $ImportImageLocalizationPackResult        	
	 * @access public
	 */
	public function __construct($ImportImageLocalizationPackResult) {
		$this->ImportImageLocalizationPackResult = $ImportImageLocalizationPackResult;
	}
}
