<?php
class QAReportForDocument {
	
	/**
	 *
	 * @var guid $DocumentGuid
	 * @access public
	 */
	public $DocumentGuid = null;
	
	/**
	 *
	 * @var string $DocumentName
	 * @access public
	 */
	public $DocumentName = null;
	
	/**
	 *
	 * @var int $NumberOfErrors
	 * @access public
	 */
	public $NumberOfErrors = null;
	
	/**
	 *
	 * @var int $NumberOfUnignoredWarnings
	 * @access public
	 */
	public $NumberOfUnignoredWarnings = null;
	
	/**
	 *
	 * @var int $NumberOfWarnings
	 * @access public
	 */
	public $NumberOfWarnings = null;
	
	/**
	 *
	 * @var string $TargetLanguage
	 * @access public
	 */
	public $TargetLanguage = null;
	
	/**
	 *
	 * @param guid $DocumentGuid        	
	 * @param int $NumberOfErrors        	
	 * @param int $NumberOfUnignoredWarnings        	
	 * @param int $NumberOfWarnings        	
	 * @access public
	 */
	public function __construct($DocumentGuid, $NumberOfErrors, $NumberOfUnignoredWarnings, $NumberOfWarnings) {
		$this->DocumentGuid = $DocumentGuid;
		$this->NumberOfErrors = $NumberOfErrors;
		$this->NumberOfUnignoredWarnings = $NumberOfUnignoredWarnings;
		$this->NumberOfWarnings = $NumberOfWarnings;
	}
}
