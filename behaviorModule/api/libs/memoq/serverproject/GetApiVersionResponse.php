<?php
class GetApiVersionResponse {
	
	/**
	 *
	 * @var string $GetApiVersionResult
	 * @access public
	 */
	public $GetApiVersionResult = null;
	
	/**
	 *
	 * @param string $GetApiVersionResult        	
	 * @access public
	 */
	public function __construct($GetApiVersionResult) {
		$this->GetApiVersionResult = $GetApiVersionResult;
	}
}
