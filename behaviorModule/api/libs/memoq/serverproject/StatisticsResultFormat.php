<?php
class StatisticsResultFormat {
	const __default = 'Html';
	const Html = 'Html';
	const CSV_WithTable = 'CSV_WithTable';
	const CSV_Trados = 'CSV_Trados';
	const CSV_MemoQ = 'CSV_MemoQ';
}
