<?php
class ListProjectTranslationDocuments2Response {
	
	/**
	 *
	 * @var ServerProjectTranslationDocInfo2[] $ListProjectTranslationDocuments2Result
	 * @access public
	 */
	public $ListProjectTranslationDocuments2Result = null;
	
	/**
	 *
	 * @param ServerProjectTranslationDocInfo2[] $ListProjectTranslationDocuments2Result        	
	 * @access public
	 */
	public function __construct($ListProjectTranslationDocuments2Result) {
		$this->ListProjectTranslationDocuments2Result = $ListProjectTranslationDocuments2Result;
	}
}
