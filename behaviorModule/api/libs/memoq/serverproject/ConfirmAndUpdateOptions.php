<?php
class ConfirmAndUpdateOptions {
	
	/**
	 *
	 * @var string $CustomMetas
	 * @access public
	 */
	public $CustomMetas = null;
	
	/**
	 *
	 * @var string $DefaultUserName
	 * @access public
	 */
	public $DefaultUserName = null;
	
	/**
	 *
	 * @var int $DocumentRoleToUse
	 * @access public
	 */
	public $DocumentRoleToUse = null;
	
	/**
	 *
	 * @var boolean $MakeRowsProofread
	 * @access public
	 */
	public $MakeRowsProofread = null;
	
	/**
	 *
	 * @var int $RoleForConfirmation
	 * @access public
	 */
	public $RoleForConfirmation = null;
	
	/**
	 *
	 * @var ConfirmAndUpdateSegmentStatuses $Status
	 * @access public
	 */
	public $Status = null;
	
	/**
	 *
	 * @var boolean $UpdateTMRepository
	 * @access public
	 */
	public $UpdateTMRepository = null;
	
	/**
	 *
	 * @var boolean $UseRoleForConfirmation
	 * @access public
	 */
	public $UseRoleForConfirmation = null;
	
	/**
	 *
	 * @var ConfirmAndUpdateUserNameBehaviors $UserNameBehavior
	 * @access public
	 */
	public $UserNameBehavior = null;
	
	/**
	 *
	 * @param int $DocumentRoleToUse        	
	 * @param boolean $MakeRowsProofread        	
	 * @param int $RoleForConfirmation        	
	 * @param ConfirmAndUpdateSegmentStatuses $Status        	
	 * @param boolean $UpdateTMRepository        	
	 * @param boolean $UseRoleForConfirmation        	
	 * @param ConfirmAndUpdateUserNameBehaviors $UserNameBehavior        	
	 * @access public
	 */
	public function __construct($DocumentRoleToUse, $MakeRowsProofread, $RoleForConfirmation, $Status, $UpdateTMRepository, $UseRoleForConfirmation, $UserNameBehavior) {
		$this->DocumentRoleToUse = $DocumentRoleToUse;
		$this->MakeRowsProofread = $MakeRowsProofread;
		$this->RoleForConfirmation = $RoleForConfirmation;
		$this->Status = $Status;
		$this->UpdateTMRepository = $UpdateTMRepository;
		$this->UseRoleForConfirmation = $UseRoleForConfirmation;
		$this->UserNameBehavior = $UserNameBehavior;
	}
}
