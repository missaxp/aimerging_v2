<?php
class ListProjectResourceAssignmentsForMultipleTypesResponse {
	
	/**
	 *
	 * @var ArrayOfServerProjectResourceAssignmentDetails[] $ListProjectResourceAssignmentsForMultipleTypesResult
	 * @access public
	 */
	public $ListProjectResourceAssignmentsForMultipleTypesResult = null;
	
	/**
	 *
	 * @param ArrayOfServerProjectResourceAssignmentDetails[] $ListProjectResourceAssignmentsForMultipleTypesResult        	
	 * @access public
	 */
	public function __construct($ListProjectResourceAssignmentsForMultipleTypesResult) {
		$this->ListProjectResourceAssignmentsForMultipleTypesResult = $ListProjectResourceAssignmentsForMultipleTypesResult;
	}
}
