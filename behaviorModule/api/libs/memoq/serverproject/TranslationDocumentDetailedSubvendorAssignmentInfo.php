<?php
include_once ('TranslationDocumentDetailedRoleAssignmentInfo.php');
class TranslationDocumentDetailedSubvendorAssignmentInfo extends TranslationDocumentDetailedRoleAssignmentInfo {
	
	/**
	 *
	 * @var TranslationDocumentAssigneeInfo $SubvendorGroup
	 * @access public
	 */
	public $SubvendorGroup = null;
	
	/**
	 *
	 * @param int $RoleId        	
	 * @access public
	 */
	public function __construct($RoleId) {
		parent::__construct ( $RoleId );
	}
}
