<?php
class SetProjectUsers {
	
	/**
	 *
	 * @var guid $serverProjectGuid
	 * @access public
	 */
	public $serverProjectGuid = null;
	
	/**
	 *
	 * @var ServerProjectUserInfo[] $userInfos
	 * @access public
	 */
	public $userInfos = null;
	
	/**
	 *
	 * @param guid $serverProjectGuid        	
	 * @param ServerProjectUserInfo[] $userInfos        	
	 * @access public
	 */
	public function __construct($serverProjectGuid, $userInfos) {
		$this->serverProjectGuid = $serverProjectGuid;
		$this->userInfos = $userInfos;
	}
}
