<?php
include_once ('DocumentHistoryItemInfo.php');
class AssignmentChangeHistoryItemInfo extends DocumentHistoryItemInfo {
	
	/**
	 *
	 * @var dateTime $Deadline
	 * @access public
	 */
	public $Deadline = null;
	
	/**
	 *
	 * @var guid $NewUserGuid
	 * @access public
	 */
	public $NewUserGuid = null;
	
	/**
	 *
	 * @var int $Role
	 * @access public
	 */
	public $Role = null;
	
	/**
	 *
	 * @param guid $DocumentOrDivisionGuid        	
	 * @param DocumentHistoryItemType $HistoryItemType        	
	 * @param guid $ModifierSVGroupId        	
	 * @param guid $ModifierUserGuid        	
	 * @param dateTime $TimeStamp        	
	 * @param dateTime $Deadline        	
	 * @param guid $NewUserGuid        	
	 * @param int $Role        	
	 * @access public
	 */
	public function __construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp, $Deadline, $NewUserGuid, $Role) {
		parent::__construct ( $DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp );
		$this->Deadline = $Deadline;
		$this->NewUserGuid = $NewUserGuid;
		$this->Role = $Role;
	}
}
