<?php
include_once ('FileResultInfo.php');
class ImportImageLocalizationPackResultInfo extends FileResultInfo {
	
	/**
	 *
	 * @var string[] $SkippedImages
	 * @access public
	 */
	public $SkippedImages = null;
	
	/**
	 *
	 * @param guid $FileGuid        	
	 * @access public
	 */
	public function __construct($FileGuid) {
		parent::__construct ( $FileGuid );
	}
}
