<?php
include_once ('ResultInfo.php');
class PostTranslationAnalysisResultInfo extends ResultInfo {
	
	/**
	 *
	 * @var PostTranslationResultForLang[] $ResultsForTargetLangs
	 * @access public
	 */
	public $ResultsForTargetLangs = null;
	
	/**
	 *
	 * @param ResultStatus $ResultStatus        	
	 * @access public
	 */
	public function __construct($ResultStatus) {
		parent::__construct ( $ResultStatus );
	}
}
