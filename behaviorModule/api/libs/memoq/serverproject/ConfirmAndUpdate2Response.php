<?php
class ConfirmAndUpdate2Response {
	
	/**
	 *
	 * @var ConfirmAndUpdateResultInfo $ConfirmAndUpdate2Result
	 * @access public
	 */
	public $ConfirmAndUpdate2Result = null;
	
	/**
	 *
	 * @param ConfirmAndUpdateResultInfo $ConfirmAndUpdate2Result        	
	 * @access public
	 */
	public function __construct($ConfirmAndUpdate2Result) {
		$this->ConfirmAndUpdate2Result = $ConfirmAndUpdate2Result;
	}
}
