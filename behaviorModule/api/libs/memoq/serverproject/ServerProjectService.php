<?php
if (! defined ( 'ROOT_PATH' )) {
	define ( 'ROOT_PATH', realpath ( __DIR__ . '/../' ) );
}

include_once ('ServerProjectResourcesInPackages.php');
include_once ('DocumentStatus.php');
include_once ('ServerProjectStatus.php');
include_once ('AddProjectLanguageTBHandlingBehavior.php');
include_once ('ServerProjectRoles.php');
include_once (ROOT_PATH . '/common/UserPackageWorkflowType.php');
include_once ('WorkflowStatus.php');
include_once ('TranslationDocumentAssignmentType.php');
include_once ('FirstAcceptUserDecision.php');
include_once ('FirstAcceptStatus.php');
include_once ('ResultStatus.php');
include_once ('BilingualDocFormat.php');
include_once ('StatisticsAlgorithm.php');
include_once ('StatisticsResultFormat.php');
include_once ('PretranslateStateToConfirmAndLock.php');
include_once ('PretranslateCopySourceToTargetConditions.php');
include_once ('PretranslateExpectedFinalTranslationState.php');
include_once ('PretranslateLookupBehavior.php');
include_once ('AsiaOnlineTranslationStatus.php');
include_once ('ConfirmAndUpdateSegmentStatuses.php');
include_once ('ConfirmAndUpdateUserNameBehaviors.php');
include_once ('NewRevisionScenarioOptions.php');
include_once ('ExpectedFinalStateAfterXTranslate.php');
include_once ('ExpectedSourceStateBeforeXTranslate.php');
include_once ('XTranslateScenario.php');
include_once ('XTranslateResultInfo.php');
include_once ('XTranslateDocumentResult.php');
include_once ('DocumentHistoryItemType.php');
include_once ('DocumentRowsLockedHistoryItemInfoLockModes.php');
include_once ('PackageDeliveryResult.php');
include_once ('DocDeliveryResult.php');
include_once ('CreateDeliveryResult.php');
include_once ('QAReportTypes.php');
include_once ('GetApiVersion.php');
include_once ('GetApiVersionResponse.php');
include_once (ROOT_PATH . '/common/UnexpectedFault.php');
include_once (ROOT_PATH . '/common/GenericFault.php');
include_once ('ServerProjectDesktopDocsCreateInfo.php');
include_once ('ServerProjectCreateInfo.php');
include_once ('CreateProject.php');
include_once ('CreateProjectResponse.php');
include_once ('CreateProject2.php');
include_once ('CreateProject2Response.php');
include_once ('UpdateProject.php');
include_once ('ServerProjectUpdateInfo.php');
include_once ('UpdateProjectResponse.php');
include_once ('GetProject.php');
include_once ('GetProjectResponse.php');
include_once ('ServerProjectInfo.php');
include_once ('ListProjects.php');
include_once ('ServerProjectListFilter.php');
include_once ('ListProjectsResponse.php');
include_once ('SetClosedStatusOfProject.php');
include_once ('SetClosedStatusOfProjectResponse.php');
include_once ('DeleteProject.php');
include_once ('DeleteProjectResponse.php');
include_once ('AddLanguageToProject.php');
include_once ('ServerProjectAddLanguageInfo.php');
include_once ('AddLanguageToProjectResponse.php');
include_once ('AddProjectLanguageFault.php');
include_once ('ListProjectTMs2.php');
include_once ('ListProjectTMs2Response.php');
include_once ('ServerProjectTMAssignmentDetails.php');
include_once (ROOT_PATH . '/common/TMInfo.php');
include_once (ROOT_PATH . '/common/HeavyResourceInfo.php');
include_once (ROOT_PATH . '/common/ResourceInfo.php');
include_once ('SetProjectTMs2.php');
include_once ('ServerProjectTMAssignmentsForTargetLang.php');
include_once ('SetProjectTMs2Response.php');
include_once ('SetProjectTBs.php');
include_once ('SetProjectTBsResponse.php');
include_once ('SetProjectTBs2.php');
include_once ('SetProjectTBs2Response.php');
include_once ('ListProjectTBs.php');
include_once ('ListProjectTBsResponse.php');
include_once ('ServerProjectTBAssignments.php');
include_once (ROOT_PATH . '/common/TBInfo.php');
include_once ('SetProjectCorpora.php');
include_once ('SetProjectCorporaResponse.php');
include_once ('ListProjectCorpora.php');
include_once ('ListProjectCorporaResponse.php');
include_once ('ServerProjectCorporaAssignments.php');
include_once (ROOT_PATH . '/common/CorpusInfo.php');
include_once ('SetProjectResourceAssignments.php');
include_once ('ServerProjectResourceAssignmentForResourceType.php');
include_once ('ResourceType.php');
include_once ('ServerProjectResourceAssignment.php');
include_once ('SetProjectResourceAssignmentsResponse.php');
include_once ('ListProjectResourceAssignments.php');
include_once ('ListProjectResourceAssignmentsResponse.php');
include_once ('ServerProjectResourceAssignmentDetails.php');
include_once ('LightResourceInfo.php');
include_once ('FilterConfigResourceInfo.php');
include_once ('PathRuleResourceInfo.php');
include_once ('PathRuleType.php');
include_once ('LightResourceInfoWithLang.php');
include_once ('ListProjectResourceAssignmentsForMultipleTypes.php');
include_once ('ListProjectResourceAssignmentsForMultipleTypesResponse.php');
include_once ('SetProjectUsers.php');
include_once ('ServerProjectUserInfo.php');
include_once ('SetProjectUsersResponse.php');
include_once ('ListProjectUsers.php');
include_once ('ListProjectUsersResponse.php');
include_once ('ServerProjectUserInfoHeader.php');
include_once (ROOT_PATH . '/common/UserInfo.php');
include_once ('ListProjectTranslationDocuments.php');
include_once ('ListProjectTranslationDocumentsResponse.php');
include_once ('ServerProjectTranslationDocInfo.php');
include_once ('TranslationDocumentUserRoleAssignmentDetails.php');
include_once ('UserInfoHeader.php');
include_once ('ListProjectTranslationDocuments2.php');
include_once ('ListServerProjectTranslationDocument2Options.php');
include_once ('ListProjectTranslationDocuments2Response.php');
include_once ('ServerProjectTranslationDocInfo2.php');
include_once ('TranslationDocumentDetailedAssignmentInfo.php');
include_once ('TranslationDocumentDetailedRoleAssignmentInfo.php');
include_once ('TranslationDocumentDetailedSingleUserAssignmentInfo.php');
include_once ('TranslationDocumentAssigneeInfo.php');
include_once ('TranslationDocumentGroupSourcingUserInfo.php');
include_once ('TranslationDocumentFirstAcceptUserInfo.php');
include_once ('TranslationDocumentDetailedFirstAcceptAssignmentInfo.php');
include_once ('TranslationDocumentDetailedSubvendorAssignmentInfo.php');
include_once ('TranslationDocumentDetailedGroupSourcingAssignmentInfo.php');
include_once ('DeleteTranslationDocument.php');
include_once ('DeleteTranslationDocumentResponse.php');
include_once ('SetProjectTranslationDocumentUserAssignments.php');
include_once ('ServerProjectTranslationDocumentUserAssignments.php');
include_once ('TranslationDocumentUserRoleAssignment.php');
include_once ('SetProjectTranslationDocumentUserAssignmentsResponse.php');
include_once ('SetTranslationDocumentAssignments.php');
include_once ('SetTranslationDocumentAssignmentsOptions.php');
include_once ('TranslationDocumentAssignments.php');
include_once ('TranslationDocumentAssignmentInfo.php');
include_once ('TranslationDocumentRoleAssignmentInfo.php');
include_once ('TranslationDocumentSingleUserAssignmentInfo.php');
include_once ('TranslationDocumentFirstAcceptAssignmentInfo.php');
include_once ('TranslationDocumentGroupSourcingAssignmentInfo.php');
include_once ('TranslationDocumentNoUserAssignmentInfo.php');
include_once ('TranslationDocumentSubvendorAssignmentInfo.php');
include_once ('SetTranslationDocumentAssignmentsResponse.php');
include_once ('TranslationDocumentAssignmentResultInfo.php');
include_once ('ResultInfo.php');
include_once ('TranslationDocumentRoleAssignmentResultInfo.php');
include_once ('TranslationDocumentAssignmentFault.php');
include_once ('ListTranslationDocumentAssignments.php');
include_once ('ListTranslationDocumentAssignmentsOptions.php');
include_once ('ListTranslationDocumentAssignmentsResponse.php');
include_once ('TranslationDocumentDetailedAssignments.php');
include_once ('SetDocumentWorkflowStatus.php');
include_once ('ServerProjectTranslationDocumentWorkflowStatusChange.php');
include_once ('SetDocumentWorkflowStatusResponse.php');
include_once ('DistributeProject.php');
include_once ('DistributeProjectResponse.php');
include_once ('DeliverDocument.php');
include_once ('DeliverDocumentRequest.php');
include_once ('DeliverDocumentResponse.php');
include_once ('ImportTranslationDocument.php');
include_once ('ImportTranslationDocumentResponse.php');
include_once ('TranslationDocImportResultInfo.php');
include_once ('ImportTranslationDocuments.php');
include_once ('ImportTranslationDocumentsResponse.php');
include_once ('ImportTranslationDocumentWithFilterConfigResource.php');
include_once ('ImportTranslationDocumentWithFilterConfigResourceResponse.php');
include_once ('ImportTranslationDocumentsWithFilterConfigResource.php');
include_once ('ImportTranslationDocumentsWithFilterConfigResourceResponse.php');
include_once ('ImportTranslationDocumentsWithOptions.php');
include_once ('ImportTranslationDocumentOptions.php');
include_once ('ImportTranslationDocumentsWithOptionsResponse.php');
include_once ('ImportBilingualTranslationDocument.php');
include_once ('ImportBilingualTranslationDocumentResponse.php');
include_once ('UpdateTranslationDocumentFromBilingual.php');
include_once ('UpdateTranslationDocumentFromBilingualResponse.php');
include_once ('ExportTranslationDocument.php');
include_once ('ExportTranslationDocumentResponse.php');
include_once ('TranslationDocExportResultInfo.php');
include_once ('FileResultInfo.php');
include_once ('ExportTranslationDocument2.php');
include_once ('DocumentExportOptions.php');
include_once ('ExportTranslationDocument2Response.php');
include_once ('ExportTranslationDocumentAsXliffBilingual.php');
include_once ('XliffBilingualExportOptions.php');
include_once ('ExportTranslationDocumentAsXliffBilingualResponse.php');
include_once ('ExportTranslationDocumentAsRtfBilingual.php');
include_once ('RtfBilingualExportOptions.php');
include_once ('ExportTranslationDocumentAsRtfBilingualResponse.php');
include_once ('ExportTranslationDocumentAsTwoColumnRtf.php');
include_once ('TwoColumnRtfBilingualExportOptions.php');
include_once ('ExportTranslationDocumentAsTwoColumnRtfResponse.php');
include_once ('ReImportTranslationDocuments.php');
include_once ('ReimportDocumentOptions.php');
include_once ('ReImportTranslationDocumentsResponse.php');
include_once ('ReImportTranslationDocumentsWithFilterConfigResource.php');
include_once ('ReImportTranslationDocumentsWithFilterConfigResourceResponse.php');
include_once ('GetStatisticsOnProject.php');
include_once ('StatisticsOptions.php');
include_once ('GetStatisticsOnProjectResponse.php');
include_once ('StatisticsResultInfo.php');
include_once ('StatisticsResultForLang.php');
include_once ('GetStatisticsOnTranslationDocuments.php');
include_once ('GetStatisticsOnTranslationDocumentsResponse.php');
include_once ('ListPostTranslationAnalysisReports.php');
include_once ('ListPostTranslationAnalysisReportsResponse.php');
include_once ('PostTranslationAnalysisReportInfo.php');
include_once ('GetPostTranslationAnalysisReportData.php');
include_once ('GetPostTranslationAnalysisReportDataResponse.php');
include_once ('PostTranslationAnalysisResultInfo.php');
include_once ('PostTranslationResultForLang.php');
include_once ('PostTransAnalysisReportForDocument.php');
include_once ('PostTransAnalysisReportForUser.php');
include_once ('PostTransAnalysisReportItem.php');
include_once ('PostTranslationReportCounts.php');
include_once ('GetPostTranslationAnalysisReportAsCSV.php');
include_once ('GetPostTranslationAnalysisReportAsCSVResponse.php');
include_once ('PostTranslationAnalysisAsCSVResult.php');
include_once ('PostTranslationAnalysisAsCSVResultForLang.php');
include_once ('RunPostTranslationAnalysis.php');
include_once ('PostTranslationAnalysisOptions.php');
include_once ('RunPostTranslationAnalysisResponse.php');
include_once ('PretranslateProject.php');
include_once ('PretranslateOptions.php');
include_once ('PretranslateCopySourceToTargetBehavior.php');
include_once ('PretranslateProjectResponse.php');
include_once ('PretranslateDocuments.php');
include_once ('PretranslateDocumentsResponse.php');
include_once ('AsiaOnlineGetLanguagePairCode.php');
include_once ('AsiaOnlineGetLanguagePairCodeResponse.php');
include_once ('AsiaOnlineGetLanguagePairCodeResultInfo.php');
include_once ('AsiaOnlineGetDomainCombinations.php');
include_once ('AsiaOnlineGetDomainCombinationsResponse.php');
include_once ('AsiaOnlineGetDomainCombinationsResultInfo.php');
include_once ('AsiaOnlineDomainCombination.php');
include_once ('AsiaOnlineBeginTranslation.php');
include_once ('AsiaOnlineTranslateOptions.php');
include_once ('AsiaOnlineBeginTranslationResponse.php');
include_once ('AsiaOnlineBeginTranslationResultInfo.php');
include_once ('AsiaOnlineGetTranslationStatus.php');
include_once ('AsiaOnlineGetTranslationStatusResponse.php');
include_once ('AsiaOnlineTranslationResultInfo.php');
include_once ('AsiaOnlineGetProjectIds.php');
include_once ('AsiaOnlineGetProjectIdsResponse.php');
include_once ('AsiaOnlineGetProjectIdsResultInfo.php');
include_once ('ConfirmAndUpdate.php');
include_once ('ConfirmAndUpdateOptions.php');
include_once ('ConfirmAndUpdateResponse.php');
include_once ('ConfirmAndUpdateResultInfo.php');
include_once ('ConfirmAndUpdateDocError.php');
include_once ('ConfirmAndUpdate2.php');
include_once ('ConfirmAndUpdate2Response.php');
include_once ('XTranslate.php');
include_once ('XTranslateOptions.php');
include_once ('XTranslateDocInfo.php');
include_once ('XTranslateResponse.php');
include_once ('DocumentImportHistoryItemInfo.php');
include_once ('DocumentHistoryItemInfo.php');
include_once ('FirstAcceptAssignHistoryItemInfo.php');
include_once ('DocumentRowsLockedHistoryItemInfo.php');
include_once ('FirstAcceptDeclineHistoryItemInfo.php');
include_once ('FirstAcceptAcceptHistoryItemInfo.php');
include_once ('FirstAcceptFailedHistoryItemInfo.php');
include_once ('DocumentSlicingHistoryItemInfo.php');
include_once ('GroupSourcingAssignHistoryItemInfo.php');
include_once ('AutomatedActionStartedHistoryItemInfo.php');
include_once ('AssignmentChangeHistoryItemInfo.php');
include_once ('DeadlineChangeHistoryItemInfo.php');
include_once ('DocumentBilingualImportHistoryItemInfo.php');
include_once ('DocumentDeliverHistoryItemInfo.php');
include_once ('DocumentReturnHistoryItemInfo.php');
include_once ('GroupSourcingDocumentDeliverHistoryItemInfo.php');
include_once ('SubvendorAssignDeadlineChangeHistoryItemInfo.php');
include_once ('SubvendorAssignHistoryItemInfo.php');
include_once ('WorkflowStatusChangeHistoryItemInfo.php');
include_once ('DocumentXTranslationHistoryItemInfo.php');
include_once ('DocumentSnapshotCreatedHistoryItemInfo.php');
include_once ('WorkingTMsDeletedHistoryItemInfo.php');
include_once ('ProjectLaunchedHistoryItemInfo.php');
include_once ('GetDocumentHistory.php');
include_once ('DocumentHistoryRequest.php');
include_once ('GetDocumentHistoryResponse.php');
include_once ('ListPackagesForProject.php');
include_once ('ListPackagesForProjectResponse.php');
include_once ('PackageInfo.php');
include_once ('ListPackagesForProjectAndUser.php');
include_once ('ListPackagesForProjectAndUserResponse.php');
include_once ('PreparePackageForDownload.php');
include_once ('PreparePackageForDownloadResponse.php');
include_once ('PreparePackageResultInfo.php');
include_once ('ListContentOfPackages.php');
include_once ('ListContentOfPackagesResponse.php');
include_once ('PackageContentInfo.php');
include_once ('PackageContentDocument.php');
include_once ('DeliverPackage.php');
include_once ('DeliverPackageResponse.php');
include_once ('PackageDeliveryResultInfo.php');
include_once ('DocDeliveryResultInfo.php');
include_once ('CreateProjectFromPackage.php');
include_once ('CreateProjectFromPackageResponse.php');
include_once ('CreateProjectFromPackage2.php');
include_once ('CreateProjectFromPackage2Response.php');
include_once ('UpdateProjectFromPackage.php');
include_once ('UpdateProjectFromPackageResponse.php');
include_once ('CreateDeliveryPackage.php');
include_once ('CreateDeliveryPackageResponse.php');
include_once ('RunQAGetReport.php');
include_once ('RunQAGetReportOptions.php');
include_once ('RunQAGetReportResponse.php');
include_once ('QAReport.php');
include_once ('QAReportForDocument.php');
include_once ('CreateImageLocalizationPack.php');
include_once ('CreateImageLocalizationPackResponse.php');
include_once ('ImportImageLocalizationPack.php');
include_once ('ImportImageLocalizationPackResponse.php');
include_once ('ImportImageLocalizationPackResultInfo.php');

/**
 */
class ServerProjectService extends \SoapClient {
	
	/**
	 *
	 * @var array $classmap The defined classes
	 * @access private
	 */
	private static $classmap = array (
			'ServerProjectRoles' => '\ServerProjectRoles',
			'NewRevisionScenarioOptions' => '\NewRevisionScenarioOptions',
			'XTranslateResultInfo' => '\XTranslateResultInfo',
			'XTranslateDocumentResult' => '\XTranslateDocumentResult',
			'CreateDeliveryResult' => '\CreateDeliveryResult',
			'GetApiVersion' => '\GetApiVersion',
			'GetApiVersionResponse' => '\GetApiVersionResponse',
			'UnexpectedFault' => '\UnexpectedFault',
			'GenericFault' => '\GenericFault',
			'ServerProjectDesktopDocsCreateInfo' => '\ServerProjectDesktopDocsCreateInfo',
			'ServerProjectCreateInfo' => '\ServerProjectCreateInfo',
			'CreateProject' => '\CreateProject',
			'CreateProjectResponse' => '\CreateProjectResponse',
			'CreateProject2' => '\CreateProject2',
			'CreateProject2Response' => '\CreateProject2Response',
			'UpdateProject' => '\UpdateProject',
			'ServerProjectUpdateInfo' => '\ServerProjectUpdateInfo',
			'UpdateProjectResponse' => '\UpdateProjectResponse',
			'GetProject' => '\GetProject',
			'GetProjectResponse' => '\GetProjectResponse',
			'ServerProjectInfo' => '\ServerProjectInfo',
			'ListProjects' => '\ListProjects',
			'ServerProjectListFilter' => '\ServerProjectListFilter',
			'ListProjectsResponse' => '\ListProjectsResponse',
			'SetClosedStatusOfProject' => '\SetClosedStatusOfProject',
			'SetClosedStatusOfProjectResponse' => '\SetClosedStatusOfProjectResponse',
			'DeleteProject' => '\DeleteProject',
			'DeleteProjectResponse' => '\DeleteProjectResponse',
			'AddLanguageToProject' => '\AddLanguageToProject',
			'ServerProjectAddLanguageInfo' => '\ServerProjectAddLanguageInfo',
			'AddLanguageToProjectResponse' => '\AddLanguageToProjectResponse',
			'AddProjectLanguageFault' => '\AddProjectLanguageFault',
			'ListProjectTMs2' => '\ListProjectTMs2',
			'ListProjectTMs2Response' => '\ListProjectTMs2Response',
			'ServerProjectTMAssignmentDetails' => '\ServerProjectTMAssignmentDetails',
			'TMInfo' => '\TMInfo',
			'HeavyResourceInfo' => '\HeavyResourceInfo',
			'ResourceInfo' => '\ResourceInfo',
			'SetProjectTMs2' => '\SetProjectTMs2',
			'ServerProjectTMAssignmentsForTargetLang' => '\ServerProjectTMAssignmentsForTargetLang',
			'SetProjectTMs2Response' => '\SetProjectTMs2Response',
			'SetProjectTBs' => '\SetProjectTBs',
			'SetProjectTBsResponse' => '\SetProjectTBsResponse',
			'SetProjectTBs2' => '\SetProjectTBs2',
			'SetProjectTBs2Response' => '\SetProjectTBs2Response',
			'ListProjectTBs' => '\ListProjectTBs',
			'ListProjectTBsResponse' => '\ListProjectTBsResponse',
			'ServerProjectTBAssignments' => '\ServerProjectTBAssignments',
			'TBInfo' => '\TBInfo',
			'SetProjectCorpora' => '\SetProjectCorpora',
			'SetProjectCorporaResponse' => '\SetProjectCorporaResponse',
			'ListProjectCorpora' => '\ListProjectCorpora',
			'ListProjectCorporaResponse' => '\ListProjectCorporaResponse',
			'ServerProjectCorporaAssignments' => '\ServerProjectCorporaAssignments',
			'CorpusInfo' => '\CorpusInfo',
			'SetProjectResourceAssignments' => '\SetProjectResourceAssignments',
			'ServerProjectResourceAssignmentForResourceType' => '\ServerProjectResourceAssignmentForResourceType',
			'ServerProjectResourceAssignment' => '\ServerProjectResourceAssignment',
			'SetProjectResourceAssignmentsResponse' => '\SetProjectResourceAssignmentsResponse',
			'ListProjectResourceAssignments' => '\ListProjectResourceAssignments',
			'ListProjectResourceAssignmentsResponse' => '\ListProjectResourceAssignmentsResponse',
			'ServerProjectResourceAssignmentDetails' => '\ServerProjectResourceAssignmentDetails',
			'LightResourceInfo' => '\LightResourceInfo',
			'FilterConfigResourceInfo' => '\FilterConfigResourceInfo',
			'PathRuleResourceInfo' => '\PathRuleResourceInfo',
			'LightResourceInfoWithLang' => '\LightResourceInfoWithLang',
			'ListProjectResourceAssignmentsForMultipleTypes' => '\ListProjectResourceAssignmentsForMultipleTypes',
			'ListProjectResourceAssignmentsForMultipleTypesResponse' => '\ListProjectResourceAssignmentsForMultipleTypesResponse',
			'SetProjectUsers' => '\SetProjectUsers',
			'ServerProjectUserInfo' => '\ServerProjectUserInfo',
			'SetProjectUsersResponse' => '\SetProjectUsersResponse',
			'ListProjectUsers' => '\ListProjectUsers',
			'ListProjectUsersResponse' => '\ListProjectUsersResponse',
			'ServerProjectUserInfoHeader' => '\ServerProjectUserInfoHeader',
			'UserInfo' => '\UserInfo',
			'ListProjectTranslationDocuments' => '\ListProjectTranslationDocuments',
			'ListProjectTranslationDocumentsResponse' => '\ListProjectTranslationDocumentsResponse',
			'ServerProjectTranslationDocInfo' => '\ServerProjectTranslationDocInfo',
			'TranslationDocumentUserRoleAssignmentDetails' => '\TranslationDocumentUserRoleAssignmentDetails',
			'UserInfoHeader' => '\UserInfoHeader',
			'ListProjectTranslationDocuments2' => '\ListProjectTranslationDocuments2',
			'ListServerProjectTranslationDocument2Options' => '\ListServerProjectTranslationDocument2Options',
			'ListProjectTranslationDocuments2Response' => '\ListProjectTranslationDocuments2Response',
			'ServerProjectTranslationDocInfo2' => '\ServerProjectTranslationDocInfo2',
			'TranslationDocumentDetailedAssignmentInfo' => '\TranslationDocumentDetailedAssignmentInfo',
			'TranslationDocumentDetailedRoleAssignmentInfo' => '\TranslationDocumentDetailedRoleAssignmentInfo',
			'TranslationDocumentDetailedSingleUserAssignmentInfo' => '\TranslationDocumentDetailedSingleUserAssignmentInfo',
			'TranslationDocumentAssigneeInfo' => '\TranslationDocumentAssigneeInfo',
			'TranslationDocumentGroupSourcingUserInfo' => '\TranslationDocumentGroupSourcingUserInfo',
			'TranslationDocumentFirstAcceptUserInfo' => '\TranslationDocumentFirstAcceptUserInfo',
			'TranslationDocumentDetailedFirstAcceptAssignmentInfo' => '\TranslationDocumentDetailedFirstAcceptAssignmentInfo',
			'TranslationDocumentDetailedSubvendorAssignmentInfo' => '\TranslationDocumentDetailedSubvendorAssignmentInfo',
			'TranslationDocumentDetailedGroupSourcingAssignmentInfo' => '\TranslationDocumentDetailedGroupSourcingAssignmentInfo',
			'DeleteTranslationDocument' => '\DeleteTranslationDocument',
			'DeleteTranslationDocumentResponse' => '\DeleteTranslationDocumentResponse',
			'SetProjectTranslationDocumentUserAssignments' => '\SetProjectTranslationDocumentUserAssignments',
			'ServerProjectTranslationDocumentUserAssignments' => '\ServerProjectTranslationDocumentUserAssignments',
			'TranslationDocumentUserRoleAssignment' => '\TranslationDocumentUserRoleAssignment',
			'SetProjectTranslationDocumentUserAssignmentsResponse' => '\SetProjectTranslationDocumentUserAssignmentsResponse',
			'SetTranslationDocumentAssignments' => '\SetTranslationDocumentAssignments',
			'SetTranslationDocumentAssignmentsOptions' => '\SetTranslationDocumentAssignmentsOptions',
			'TranslationDocumentAssignments' => '\TranslationDocumentAssignments',
			'TranslationDocumentAssignmentInfo' => '\TranslationDocumentAssignmentInfo',
			'TranslationDocumentRoleAssignmentInfo' => '\TranslationDocumentRoleAssignmentInfo',
			'TranslationDocumentSingleUserAssignmentInfo' => '\TranslationDocumentSingleUserAssignmentInfo',
			'TranslationDocumentFirstAcceptAssignmentInfo' => '\TranslationDocumentFirstAcceptAssignmentInfo',
			'TranslationDocumentGroupSourcingAssignmentInfo' => '\TranslationDocumentGroupSourcingAssignmentInfo',
			'TranslationDocumentNoUserAssignmentInfo' => '\TranslationDocumentNoUserAssignmentInfo',
			'TranslationDocumentSubvendorAssignmentInfo' => '\TranslationDocumentSubvendorAssignmentInfo',
			'SetTranslationDocumentAssignmentsResponse' => '\SetTranslationDocumentAssignmentsResponse',
			'TranslationDocumentAssignmentResultInfo' => '\TranslationDocumentAssignmentResultInfo',
			'ResultInfo' => '\ResultInfo',
			'TranslationDocumentRoleAssignmentResultInfo' => '\TranslationDocumentRoleAssignmentResultInfo',
			'TranslationDocumentAssignmentFault' => '\TranslationDocumentAssignmentFault',
			'ListTranslationDocumentAssignments' => '\ListTranslationDocumentAssignments',
			'ListTranslationDocumentAssignmentsOptions' => '\ListTranslationDocumentAssignmentsOptions',
			'ListTranslationDocumentAssignmentsResponse' => '\ListTranslationDocumentAssignmentsResponse',
			'TranslationDocumentDetailedAssignments' => '\TranslationDocumentDetailedAssignments',
			'SetDocumentWorkflowStatus' => '\SetDocumentWorkflowStatus',
			'ServerProjectTranslationDocumentWorkflowStatusChange' => '\ServerProjectTranslationDocumentWorkflowStatusChange',
			'SetDocumentWorkflowStatusResponse' => '\SetDocumentWorkflowStatusResponse',
			'DistributeProject' => '\DistributeProject',
			'DistributeProjectResponse' => '\DistributeProjectResponse',
			'DeliverDocument' => '\DeliverDocument',
			'DeliverDocumentRequest' => '\DeliverDocumentRequest',
			'DeliverDocumentResponse' => '\DeliverDocumentResponse',
			'ImportTranslationDocument' => '\ImportTranslationDocument',
			'ImportTranslationDocumentResponse' => '\ImportTranslationDocumentResponse',
			'TranslationDocImportResultInfo' => '\TranslationDocImportResultInfo',
			'ImportTranslationDocuments' => '\ImportTranslationDocuments',
			'ImportTranslationDocumentsResponse' => '\ImportTranslationDocumentsResponse',
			'ImportTranslationDocumentWithFilterConfigResource' => '\ImportTranslationDocumentWithFilterConfigResource',
			'ImportTranslationDocumentWithFilterConfigResourceResponse' => '\ImportTranslationDocumentWithFilterConfigResourceResponse',
			'ImportTranslationDocumentsWithFilterConfigResource' => '\ImportTranslationDocumentsWithFilterConfigResource',
			'ImportTranslationDocumentsWithFilterConfigResourceResponse' => '\ImportTranslationDocumentsWithFilterConfigResourceResponse',
			'ImportTranslationDocumentsWithOptions' => '\ImportTranslationDocumentsWithOptions',
			'ImportTranslationDocumentOptions' => '\ImportTranslationDocumentOptions',
			'ImportTranslationDocumentsWithOptionsResponse' => '\ImportTranslationDocumentsWithOptionsResponse',
			'ImportBilingualTranslationDocument' => '\ImportBilingualTranslationDocument',
			'ImportBilingualTranslationDocumentResponse' => '\ImportBilingualTranslationDocumentResponse',
			'UpdateTranslationDocumentFromBilingual' => '\UpdateTranslationDocumentFromBilingual',
			'UpdateTranslationDocumentFromBilingualResponse' => '\UpdateTranslationDocumentFromBilingualResponse',
			'ExportTranslationDocument' => '\ExportTranslationDocument',
			'ExportTranslationDocumentResponse' => '\ExportTranslationDocumentResponse',
			'TranslationDocExportResultInfo' => '\TranslationDocExportResultInfo',
			'FileResultInfo' => '\FileResultInfo',
			'ExportTranslationDocument2' => '\ExportTranslationDocument2',
			'DocumentExportOptions' => '\DocumentExportOptions',
			'ExportTranslationDocument2Response' => '\ExportTranslationDocument2Response',
			'ExportTranslationDocumentAsXliffBilingual' => '\ExportTranslationDocumentAsXliffBilingual',
			'XliffBilingualExportOptions' => '\XliffBilingualExportOptions',
			'ExportTranslationDocumentAsXliffBilingualResponse' => '\ExportTranslationDocumentAsXliffBilingualResponse',
			'ExportTranslationDocumentAsRtfBilingual' => '\ExportTranslationDocumentAsRtfBilingual',
			'RtfBilingualExportOptions' => '\RtfBilingualExportOptions',
			'ExportTranslationDocumentAsRtfBilingualResponse' => '\ExportTranslationDocumentAsRtfBilingualResponse',
			'ExportTranslationDocumentAsTwoColumnRtf' => '\ExportTranslationDocumentAsTwoColumnRtf',
			'TwoColumnRtfBilingualExportOptions' => '\TwoColumnRtfBilingualExportOptions',
			'ExportTranslationDocumentAsTwoColumnRtfResponse' => '\ExportTranslationDocumentAsTwoColumnRtfResponse',
			'ReImportTranslationDocuments' => '\ReImportTranslationDocuments',
			'ReimportDocumentOptions' => '\ReimportDocumentOptions',
			'ReImportTranslationDocumentsResponse' => '\ReImportTranslationDocumentsResponse',
			'ReImportTranslationDocumentsWithFilterConfigResource' => '\ReImportTranslationDocumentsWithFilterConfigResource',
			'ReImportTranslationDocumentsWithFilterConfigResourceResponse' => '\ReImportTranslationDocumentsWithFilterConfigResourceResponse',
			'GetStatisticsOnProject' => '\GetStatisticsOnProject',
			'StatisticsOptions' => '\StatisticsOptions',
			'GetStatisticsOnProjectResponse' => '\GetStatisticsOnProjectResponse',
			'StatisticsResultInfo' => '\StatisticsResultInfo',
			'StatisticsResultForLang' => '\StatisticsResultForLang',
			'GetStatisticsOnTranslationDocuments' => '\GetStatisticsOnTranslationDocuments',
			'GetStatisticsOnTranslationDocumentsResponse' => '\GetStatisticsOnTranslationDocumentsResponse',
			'ListPostTranslationAnalysisReports' => '\ListPostTranslationAnalysisReports',
			'ListPostTranslationAnalysisReportsResponse' => '\ListPostTranslationAnalysisReportsResponse',
			'PostTranslationAnalysisReportInfo' => '\PostTranslationAnalysisReportInfo',
			'GetPostTranslationAnalysisReportData' => '\GetPostTranslationAnalysisReportData',
			'GetPostTranslationAnalysisReportDataResponse' => '\GetPostTranslationAnalysisReportDataResponse',
			'PostTranslationAnalysisResultInfo' => '\PostTranslationAnalysisResultInfo',
			'PostTranslationResultForLang' => '\PostTranslationResultForLang',
			'PostTransAnalysisReportForDocument' => '\PostTransAnalysisReportForDocument',
			'PostTransAnalysisReportForUser' => '\PostTransAnalysisReportForUser',
			'PostTransAnalysisReportItem' => '\PostTransAnalysisReportItem',
			'PostTranslationReportCounts' => '\PostTranslationReportCounts',
			'GetPostTranslationAnalysisReportAsCSV' => '\GetPostTranslationAnalysisReportAsCSV',
			'GetPostTranslationAnalysisReportAsCSVResponse' => '\GetPostTranslationAnalysisReportAsCSVResponse',
			'PostTranslationAnalysisAsCSVResult' => '\PostTranslationAnalysisAsCSVResult',
			'PostTranslationAnalysisAsCSVResultForLang' => '\PostTranslationAnalysisAsCSVResultForLang',
			'RunPostTranslationAnalysis' => '\RunPostTranslationAnalysis',
			'PostTranslationAnalysisOptions' => '\PostTranslationAnalysisOptions',
			'RunPostTranslationAnalysisResponse' => '\RunPostTranslationAnalysisResponse',
			'PretranslateProject' => '\PretranslateProject',
			'PretranslateOptions' => '\PretranslateOptions',
			'PretranslateCopySourceToTargetBehavior' => '\PretranslateCopySourceToTargetBehavior',
			'PretranslateProjectResponse' => '\PretranslateProjectResponse',
			'PretranslateDocuments' => '\PretranslateDocuments',
			'PretranslateDocumentsResponse' => '\PretranslateDocumentsResponse',
			'AsiaOnlineGetLanguagePairCode' => '\AsiaOnlineGetLanguagePairCode',
			'AsiaOnlineGetLanguagePairCodeResponse' => '\AsiaOnlineGetLanguagePairCodeResponse',
			'AsiaOnlineGetLanguagePairCodeResultInfo' => '\AsiaOnlineGetLanguagePairCodeResultInfo',
			'AsiaOnlineGetDomainCombinations' => '\AsiaOnlineGetDomainCombinations',
			'AsiaOnlineGetDomainCombinationsResponse' => '\AsiaOnlineGetDomainCombinationsResponse',
			'AsiaOnlineGetDomainCombinationsResultInfo' => '\AsiaOnlineGetDomainCombinationsResultInfo',
			'AsiaOnlineDomainCombination' => '\AsiaOnlineDomainCombination',
			'AsiaOnlineBeginTranslation' => '\AsiaOnlineBeginTranslation',
			'AsiaOnlineTranslateOptions' => '\AsiaOnlineTranslateOptions',
			'AsiaOnlineBeginTranslationResponse' => '\AsiaOnlineBeginTranslationResponse',
			'AsiaOnlineBeginTranslationResultInfo' => '\AsiaOnlineBeginTranslationResultInfo',
			'AsiaOnlineGetTranslationStatus' => '\AsiaOnlineGetTranslationStatus',
			'AsiaOnlineGetTranslationStatusResponse' => '\AsiaOnlineGetTranslationStatusResponse',
			'AsiaOnlineTranslationResultInfo' => '\AsiaOnlineTranslationResultInfo',
			'AsiaOnlineGetProjectIds' => '\AsiaOnlineGetProjectIds',
			'AsiaOnlineGetProjectIdsResponse' => '\AsiaOnlineGetProjectIdsResponse',
			'AsiaOnlineGetProjectIdsResultInfo' => '\AsiaOnlineGetProjectIdsResultInfo',
			'ConfirmAndUpdate' => '\ConfirmAndUpdate',
			'ConfirmAndUpdateOptions' => '\ConfirmAndUpdateOptions',
			'ConfirmAndUpdateResponse' => '\ConfirmAndUpdateResponse',
			'ConfirmAndUpdateResultInfo' => '\ConfirmAndUpdateResultInfo',
			'ConfirmAndUpdateDocError' => '\ConfirmAndUpdateDocError',
			'ConfirmAndUpdate2' => '\ConfirmAndUpdate2',
			'ConfirmAndUpdate2Response' => '\ConfirmAndUpdate2Response',
			'XTranslate' => '\XTranslate',
			'XTranslateOptions' => '\XTranslateOptions',
			'XTranslateDocInfo' => '\XTranslateDocInfo',
			'XTranslateResponse' => '\XTranslateResponse',
			'DocumentImportHistoryItemInfo' => '\DocumentImportHistoryItemInfo',
			'DocumentHistoryItemInfo' => '\DocumentHistoryItemInfo',
			'FirstAcceptAssignHistoryItemInfo' => '\FirstAcceptAssignHistoryItemInfo',
			'DocumentRowsLockedHistoryItemInfo' => '\DocumentRowsLockedHistoryItemInfo',
			'FirstAcceptDeclineHistoryItemInfo' => '\FirstAcceptDeclineHistoryItemInfo',
			'FirstAcceptAcceptHistoryItemInfo' => '\FirstAcceptAcceptHistoryItemInfo',
			'FirstAcceptFailedHistoryItemInfo' => '\FirstAcceptFailedHistoryItemInfo',
			'DocumentSlicingHistoryItemInfo' => '\DocumentSlicingHistoryItemInfo',
			'GroupSourcingAssignHistoryItemInfo' => '\GroupSourcingAssignHistoryItemInfo',
			'AutomatedActionStartedHistoryItemInfo' => '\AutomatedActionStartedHistoryItemInfo',
			'AssignmentChangeHistoryItemInfo' => '\AssignmentChangeHistoryItemInfo',
			'DeadlineChangeHistoryItemInfo' => '\DeadlineChangeHistoryItemInfo',
			'DocumentBilingualImportHistoryItemInfo' => '\DocumentBilingualImportHistoryItemInfo',
			'DocumentDeliverHistoryItemInfo' => '\DocumentDeliverHistoryItemInfo',
			'DocumentReturnHistoryItemInfo' => '\DocumentReturnHistoryItemInfo',
			'GroupSourcingDocumentDeliverHistoryItemInfo' => '\GroupSourcingDocumentDeliverHistoryItemInfo',
			'SubvendorAssignDeadlineChangeHistoryItemInfo' => '\SubvendorAssignDeadlineChangeHistoryItemInfo',
			'SubvendorAssignHistoryItemInfo' => '\SubvendorAssignHistoryItemInfo',
			'WorkflowStatusChangeHistoryItemInfo' => '\WorkflowStatusChangeHistoryItemInfo',
			'DocumentXTranslationHistoryItemInfo' => '\DocumentXTranslationHistoryItemInfo',
			'DocumentSnapshotCreatedHistoryItemInfo' => '\DocumentSnapshotCreatedHistoryItemInfo',
			'WorkingTMsDeletedHistoryItemInfo' => '\WorkingTMsDeletedHistoryItemInfo',
			'ProjectLaunchedHistoryItemInfo' => '\ProjectLaunchedHistoryItemInfo',
			'GetDocumentHistory' => '\GetDocumentHistory',
			'DocumentHistoryRequest' => '\DocumentHistoryRequest',
			'GetDocumentHistoryResponse' => '\GetDocumentHistoryResponse',
			'ListPackagesForProject' => '\ListPackagesForProject',
			'ListPackagesForProjectResponse' => '\ListPackagesForProjectResponse',
			'PackageInfo' => '\PackageInfo',
			'ListPackagesForProjectAndUser' => '\ListPackagesForProjectAndUser',
			'ListPackagesForProjectAndUserResponse' => '\ListPackagesForProjectAndUserResponse',
			'PreparePackageForDownload' => '\PreparePackageForDownload',
			'PreparePackageForDownloadResponse' => '\PreparePackageForDownloadResponse',
			'PreparePackageResultInfo' => '\PreparePackageResultInfo',
			'ListContentOfPackages' => '\ListContentOfPackages',
			'ListContentOfPackagesResponse' => '\ListContentOfPackagesResponse',
			'PackageContentInfo' => '\PackageContentInfo',
			'PackageContentDocument' => '\PackageContentDocument',
			'DeliverPackage' => '\DeliverPackage',
			'DeliverPackageResponse' => '\DeliverPackageResponse',
			'PackageDeliveryResultInfo' => '\PackageDeliveryResultInfo',
			'DocDeliveryResultInfo' => '\DocDeliveryResultInfo',
			'CreateProjectFromPackage' => '\CreateProjectFromPackage',
			'CreateProjectFromPackageResponse' => '\CreateProjectFromPackageResponse',
			'CreateProjectFromPackage2' => '\CreateProjectFromPackage2',
			'CreateProjectFromPackage2Response' => '\CreateProjectFromPackage2Response',
			'UpdateProjectFromPackage' => '\UpdateProjectFromPackage',
			'UpdateProjectFromPackageResponse' => '\UpdateProjectFromPackageResponse',
			'CreateDeliveryPackage' => '\CreateDeliveryPackage',
			'CreateDeliveryPackageResponse' => '\CreateDeliveryPackageResponse',
			'RunQAGetReport' => '\RunQAGetReport',
			'RunQAGetReportOptions' => '\RunQAGetReportOptions',
			'RunQAGetReportResponse' => '\RunQAGetReportResponse',
			'QAReport' => '\QAReport',
			'QAReportForDocument' => '\QAReportForDocument',
			'CreateImageLocalizationPack' => '\CreateImageLocalizationPack',
			'CreateImageLocalizationPackResponse' => '\CreateImageLocalizationPackResponse',
			'ImportImageLocalizationPack' => '\ImportImageLocalizationPack',
			'ImportImageLocalizationPackResponse' => '\ImportImageLocalizationPackResponse',
			'ImportImageLocalizationPackResultInfo' => '\ImportImageLocalizationPackResultInfo' 
	);
	
	/**
	 *
	 * @param array $options
	 *        	A array of config values
	 * @param string $wsdl
	 *        	The wsdl file to use
	 * @access public
	 */
	public function __construct(array $options = array(), $wsdl = 'serverproject.wsdl') {
		foreach ( self::$classmap as $key => $value ) {
			if (! isset ( $options ['classmap'] [$key] )) {
				$options ['classmap'] [$key] = $value;
			}
		}
		
		parent::__construct ( $wsdl, $options );
	}
	
	/**
	 *
	 * @param GetApiVersion $parameters        	
	 * @access public
	 * @return GetApiVersionResponse
	 */
	public function GetApiVersion(GetApiVersion $parameters) {
		return $this->__soapCall ( 'GetApiVersion', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param CreateProject $parameters        	
	 * @access public
	 * @return CreateProjectResponse
	 */
	public function CreateProject(CreateProject $parameters) {
		return $this->__soapCall ( 'CreateProject', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param CreateProject2 $parameters        	
	 * @access public
	 * @return CreateProject2Response
	 */
	public function CreateProject2(CreateProject2 $parameters) {
		return $this->__soapCall ( 'CreateProject2', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param UpdateProject $parameters        	
	 * @access public
	 * @return UpdateProjectResponse
	 */
	public function UpdateProject(UpdateProject $parameters) {
		return $this->__soapCall ( 'UpdateProject', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param GetProject $parameters        	
	 * @access public
	 * @return GetProjectResponse
	 */
	public function GetProject(GetProject $parameters) {
		return $this->__soapCall ( 'GetProject', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ListProjects $parameters        	
	 * @access public
	 * @return ListProjectsResponse
	 */
	public function ListProjects(ListProjects $parameters) {
		return $this->__soapCall ( 'ListProjects', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param SetClosedStatusOfProject $parameters        	
	 * @access public
	 * @return SetClosedStatusOfProjectResponse
	 */
	public function SetClosedStatusOfProject(SetClosedStatusOfProject $parameters) {
		return $this->__soapCall ( 'SetClosedStatusOfProject', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param DeleteProject $parameters        	
	 * @access public
	 * @return DeleteProjectResponse
	 */
	public function DeleteProject(DeleteProject $parameters) {
		return $this->__soapCall ( 'DeleteProject', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param AddLanguageToProject $parameters        	
	 * @access public
	 * @return AddLanguageToProjectResponse
	 */
	public function AddLanguageToProject(AddLanguageToProject $parameters) {
		return $this->__soapCall ( 'AddLanguageToProject', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ListProjectTMs2 $parameters        	
	 * @access public
	 * @return ListProjectTMs2Response
	 */
	public function ListProjectTMs2(ListProjectTMs2 $parameters) {
		return $this->__soapCall ( 'ListProjectTMs2', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param SetProjectTMs2 $parameters        	
	 * @access public
	 * @return SetProjectTMs2Response
	 */
	public function SetProjectTMs2(SetProjectTMs2 $parameters) {
		return $this->__soapCall ( 'SetProjectTMs2', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param SetProjectTBs $parameters        	
	 * @access public
	 * @return SetProjectTBsResponse
	 */
	public function SetProjectTBs(SetProjectTBs $parameters) {
		return $this->__soapCall ( 'SetProjectTBs', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param SetProjectTBs2 $parameters        	
	 * @access public
	 * @return SetProjectTBs2Response
	 */
	public function SetProjectTBs2(SetProjectTBs2 $parameters) {
		return $this->__soapCall ( 'SetProjectTBs2', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ListProjectTBs $parameters        	
	 * @access public
	 * @return ListProjectTBsResponse
	 */
	public function ListProjectTBs(ListProjectTBs $parameters) {
		return $this->__soapCall ( 'ListProjectTBs', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param SetProjectCorpora $parameters        	
	 * @access public
	 * @return SetProjectCorporaResponse
	 */
	public function SetProjectCorpora(SetProjectCorpora $parameters) {
		return $this->__soapCall ( 'SetProjectCorpora', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ListProjectCorpora $parameters        	
	 * @access public
	 * @return ListProjectCorporaResponse
	 */
	public function ListProjectCorpora(ListProjectCorpora $parameters) {
		return $this->__soapCall ( 'ListProjectCorpora', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param SetProjectResourceAssignments $parameters        	
	 * @access public
	 * @return SetProjectResourceAssignmentsResponse
	 */
	public function SetProjectResourceAssignments(SetProjectResourceAssignments $parameters) {
		return $this->__soapCall ( 'SetProjectResourceAssignments', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ListProjectResourceAssignments $parameters        	
	 * @access public
	 * @return ListProjectResourceAssignmentsResponse
	 */
	public function ListProjectResourceAssignments(ListProjectResourceAssignments $parameters) {
		return $this->__soapCall ( 'ListProjectResourceAssignments', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ListProjectResourceAssignmentsForMultipleTypes $parameters        	
	 * @access public
	 * @return ListProjectResourceAssignmentsForMultipleTypesResponse
	 */
	public function ListProjectResourceAssignmentsForMultipleTypes(ListProjectResourceAssignmentsForMultipleTypes $parameters) {
		return $this->__soapCall ( 'ListProjectResourceAssignmentsForMultipleTypes', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param SetProjectUsers $parameters        	
	 * @access public
	 * @return SetProjectUsersResponse
	 */
	public function SetProjectUsers(SetProjectUsers $parameters) {
		return $this->__soapCall ( 'SetProjectUsers', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ListProjectUsers $parameters        	
	 * @access public
	 * @return ListProjectUsersResponse
	 */
	public function ListProjectUsers(ListProjectUsers $parameters) {
		return $this->__soapCall ( 'ListProjectUsers', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ListProjectTranslationDocuments $parameters        	
	 * @access public
	 * @return ListProjectTranslationDocumentsResponse
	 */
	public function ListProjectTranslationDocuments(ListProjectTranslationDocuments $parameters) {
		return $this->__soapCall ( 'ListProjectTranslationDocuments', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ListProjectTranslationDocuments2 $parameters        	
	 * @access public
	 * @return ListProjectTranslationDocuments2Response
	 */
	public function ListProjectTranslationDocuments2(ListProjectTranslationDocuments2 $parameters) {
		return $this->__soapCall ( 'ListProjectTranslationDocuments2', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param DeleteTranslationDocument $parameters        	
	 * @access public
	 * @return DeleteTranslationDocumentResponse
	 */
	public function DeleteTranslationDocument(DeleteTranslationDocument $parameters) {
		return $this->__soapCall ( 'DeleteTranslationDocument', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param SetProjectTranslationDocumentUserAssignments $parameters        	
	 * @access public
	 * @return SetProjectTranslationDocumentUserAssignmentsResponse
	 */
	public function SetProjectTranslationDocumentUserAssignments(SetProjectTranslationDocumentUserAssignments $parameters) {
		return $this->__soapCall ( 'SetProjectTranslationDocumentUserAssignments', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param SetTranslationDocumentAssignments $parameters        	
	 * @access public
	 * @return SetTranslationDocumentAssignmentsResponse
	 */
	public function SetTranslationDocumentAssignments(SetTranslationDocumentAssignments $parameters) {
		return $this->__soapCall ( 'SetTranslationDocumentAssignments', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ListTranslationDocumentAssignments $parameters        	
	 * @access public
	 * @return ListTranslationDocumentAssignmentsResponse
	 */
	public function ListTranslationDocumentAssignments(ListTranslationDocumentAssignments $parameters) {
		return $this->__soapCall ( 'ListTranslationDocumentAssignments', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param SetDocumentWorkflowStatus $parameters        	
	 * @access public
	 * @return SetDocumentWorkflowStatusResponse
	 */
	public function SetDocumentWorkflowStatus(SetDocumentWorkflowStatus $parameters) {
		return $this->__soapCall ( 'SetDocumentWorkflowStatus', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param DistributeProject $parameters        	
	 * @access public
	 * @return DistributeProjectResponse
	 */
	public function DistributeProject(DistributeProject $parameters) {
		return $this->__soapCall ( 'DistributeProject', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param DeliverDocument $parameters        	
	 * @access public
	 * @return DeliverDocumentResponse
	 */
	public function DeliverDocument(DeliverDocument $parameters) {
		return $this->__soapCall ( 'DeliverDocument', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ImportTranslationDocument $parameters        	
	 * @access public
	 * @return ImportTranslationDocumentResponse
	 */
	public function ImportTranslationDocument(ImportTranslationDocument $parameters) {
		return $this->__soapCall ( 'ImportTranslationDocument', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ImportTranslationDocuments $parameters        	
	 * @access public
	 * @return ImportTranslationDocumentsResponse
	 */
	public function ImportTranslationDocuments(ImportTranslationDocuments $parameters) {
		return $this->__soapCall ( 'ImportTranslationDocuments', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ImportTranslationDocumentWithFilterConfigResource $parameters        	
	 * @access public
	 * @return ImportTranslationDocumentWithFilterConfigResourceResponse
	 */
	public function ImportTranslationDocumentWithFilterConfigResource(ImportTranslationDocumentWithFilterConfigResource $parameters) {
		return $this->__soapCall ( 'ImportTranslationDocumentWithFilterConfigResource', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ImportTranslationDocumentsWithFilterConfigResource $parameters        	
	 * @access public
	 * @return ImportTranslationDocumentsWithFilterConfigResourceResponse
	 */
	public function ImportTranslationDocumentsWithFilterConfigResource(ImportTranslationDocumentsWithFilterConfigResource $parameters) {
		return $this->__soapCall ( 'ImportTranslationDocumentsWithFilterConfigResource', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ImportTranslationDocumentsWithOptions $parameters        	
	 * @access public
	 * @return ImportTranslationDocumentsWithOptionsResponse
	 */
	public function ImportTranslationDocumentsWithOptions(ImportTranslationDocumentsWithOptions $parameters) {
		return $this->__soapCall ( 'ImportTranslationDocumentsWithOptions', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ImportBilingualTranslationDocument $parameters        	
	 * @access public
	 * @return ImportBilingualTranslationDocumentResponse
	 */
	public function ImportBilingualTranslationDocument(ImportBilingualTranslationDocument $parameters) {
		return $this->__soapCall ( 'ImportBilingualTranslationDocument', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param UpdateTranslationDocumentFromBilingual $parameters        	
	 * @access public
	 * @return UpdateTranslationDocumentFromBilingualResponse
	 */
	public function UpdateTranslationDocumentFromBilingual(UpdateTranslationDocumentFromBilingual $parameters) {
		return $this->__soapCall ( 'UpdateTranslationDocumentFromBilingual', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ExportTranslationDocument $parameters        	
	 * @access public
	 * @return ExportTranslationDocumentResponse
	 */
	public function ExportTranslationDocument(ExportTranslationDocument $parameters) {
		return $this->__soapCall ( 'ExportTranslationDocument', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ExportTranslationDocument2 $parameters        	
	 * @access public
	 * @return ExportTranslationDocument2Response
	 */
	public function ExportTranslationDocument2(ExportTranslationDocument2 $parameters) {
		return $this->__soapCall ( 'ExportTranslationDocument2', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ExportTranslationDocumentAsXliffBilingual $parameters        	
	 * @access public
	 * @return ExportTranslationDocumentAsXliffBilingualResponse
	 */
	public function ExportTranslationDocumentAsXliffBilingual(ExportTranslationDocumentAsXliffBilingual $parameters) {
		return $this->__soapCall ( 'ExportTranslationDocumentAsXliffBilingual', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ExportTranslationDocumentAsRtfBilingual $parameters        	
	 * @access public
	 * @return ExportTranslationDocumentAsRtfBilingualResponse
	 */
	public function ExportTranslationDocumentAsRtfBilingual(ExportTranslationDocumentAsRtfBilingual $parameters) {
		return $this->__soapCall ( 'ExportTranslationDocumentAsRtfBilingual', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ExportTranslationDocumentAsTwoColumnRtf $parameters        	
	 * @access public
	 * @return ExportTranslationDocumentAsTwoColumnRtfResponse
	 */
	public function ExportTranslationDocumentAsTwoColumnRtf(ExportTranslationDocumentAsTwoColumnRtf $parameters) {
		return $this->__soapCall ( 'ExportTranslationDocumentAsTwoColumnRtf', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ReImportTranslationDocuments $parameters        	
	 * @access public
	 * @return ReImportTranslationDocumentsResponse
	 */
	public function ReImportTranslationDocuments(ReImportTranslationDocuments $parameters) {
		return $this->__soapCall ( 'ReImportTranslationDocuments', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ReImportTranslationDocumentsWithFilterConfigResource $parameters        	
	 * @access public
	 * @return ReImportTranslationDocumentsWithFilterConfigResourceResponse
	 */
	public function ReImportTranslationDocumentsWithFilterConfigResource(ReImportTranslationDocumentsWithFilterConfigResource $parameters) {
		return $this->__soapCall ( 'ReImportTranslationDocumentsWithFilterConfigResource', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param GetStatisticsOnProject $parameters        	
	 * @access public
	 * @return GetStatisticsOnProjectResponse
	 */
	public function GetStatisticsOnProject(GetStatisticsOnProject $parameters) {
		return $this->__soapCall ( 'GetStatisticsOnProject', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param GetStatisticsOnTranslationDocuments $parameters        	
	 * @access public
	 * @return GetStatisticsOnTranslationDocumentsResponse
	 */
	public function GetStatisticsOnTranslationDocuments(GetStatisticsOnTranslationDocuments $parameters) {
		return $this->__soapCall ( 'GetStatisticsOnTranslationDocuments', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ListPostTranslationAnalysisReports $parameters        	
	 * @access public
	 * @return ListPostTranslationAnalysisReportsResponse
	 */
	public function ListPostTranslationAnalysisReports(ListPostTranslationAnalysisReports $parameters) {
		return $this->__soapCall ( 'ListPostTranslationAnalysisReports', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param GetPostTranslationAnalysisReportData $parameters        	
	 * @access public
	 * @return GetPostTranslationAnalysisReportDataResponse
	 */
	public function GetPostTranslationAnalysisReportData(GetPostTranslationAnalysisReportData $parameters) {
		return $this->__soapCall ( 'GetPostTranslationAnalysisReportData', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param GetPostTranslationAnalysisReportAsCSV $parameters        	
	 * @access public
	 * @return GetPostTranslationAnalysisReportAsCSVResponse
	 */
	public function GetPostTranslationAnalysisReportAsCSV(GetPostTranslationAnalysisReportAsCSV $parameters) {
		return $this->__soapCall ( 'GetPostTranslationAnalysisReportAsCSV', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param RunPostTranslationAnalysis $parameters        	
	 * @access public
	 * @return RunPostTranslationAnalysisResponse
	 */
	public function RunPostTranslationAnalysis(RunPostTranslationAnalysis $parameters) {
		return $this->__soapCall ( 'RunPostTranslationAnalysis', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param PretranslateProject $parameters        	
	 * @access public
	 * @return PretranslateProjectResponse
	 */
	public function PretranslateProject(PretranslateProject $parameters) {
		return $this->__soapCall ( 'PretranslateProject', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param PretranslateDocuments $parameters        	
	 * @access public
	 * @return PretranslateDocumentsResponse
	 */
	public function PretranslateDocuments(PretranslateDocuments $parameters) {
		return $this->__soapCall ( 'PretranslateDocuments', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param AsiaOnlineGetLanguagePairCode $parameters        	
	 * @access public
	 * @return AsiaOnlineGetLanguagePairCodeResponse
	 */
	public function AsiaOnlineGetLanguagePairCode(AsiaOnlineGetLanguagePairCode $parameters) {
		return $this->__soapCall ( 'AsiaOnlineGetLanguagePairCode', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param AsiaOnlineGetDomainCombinations $parameters        	
	 * @access public
	 * @return AsiaOnlineGetDomainCombinationsResponse
	 */
	public function AsiaOnlineGetDomainCombinations(AsiaOnlineGetDomainCombinations $parameters) {
		return $this->__soapCall ( 'AsiaOnlineGetDomainCombinations', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param AsiaOnlineBeginTranslation $parameters        	
	 * @access public
	 * @return AsiaOnlineBeginTranslationResponse
	 */
	public function AsiaOnlineBeginTranslation(AsiaOnlineBeginTranslation $parameters) {
		return $this->__soapCall ( 'AsiaOnlineBeginTranslation', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param AsiaOnlineGetTranslationStatus $parameters        	
	 * @access public
	 * @return AsiaOnlineGetTranslationStatusResponse
	 */
	public function AsiaOnlineGetTranslationStatus(AsiaOnlineGetTranslationStatus $parameters) {
		return $this->__soapCall ( 'AsiaOnlineGetTranslationStatus', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param AsiaOnlineGetProjectIds $parameters        	
	 * @access public
	 * @return AsiaOnlineGetProjectIdsResponse
	 */
	public function AsiaOnlineGetProjectIds(AsiaOnlineGetProjectIds $parameters) {
		return $this->__soapCall ( 'AsiaOnlineGetProjectIds', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ConfirmAndUpdate $parameters        	
	 * @access public
	 * @return ConfirmAndUpdateResponse
	 */
	public function ConfirmAndUpdate(ConfirmAndUpdate $parameters) {
		return $this->__soapCall ( 'ConfirmAndUpdate', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ConfirmAndUpdate2 $parameters        	
	 * @access public
	 * @return ConfirmAndUpdate2Response
	 */
	public function ConfirmAndUpdate2(ConfirmAndUpdate2 $parameters) {
		return $this->__soapCall ( 'ConfirmAndUpdate2', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param XTranslate $parameters        	
	 * @access public
	 * @return XTranslateResponse
	 */
	public function XTranslate(XTranslate $parameters) {
		return $this->__soapCall ( 'XTranslate', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param GetDocumentHistory $parameters        	
	 * @access public
	 * @return GetDocumentHistoryResponse
	 */
	public function GetDocumentHistory(GetDocumentHistory $parameters) {
		return $this->__soapCall ( 'GetDocumentHistory', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ListPackagesForProject $parameters        	
	 * @access public
	 * @return ListPackagesForProjectResponse
	 */
	public function ListPackagesForProject(ListPackagesForProject $parameters) {
		return $this->__soapCall ( 'ListPackagesForProject', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ListPackagesForProjectAndUser $parameters        	
	 * @access public
	 * @return ListPackagesForProjectAndUserResponse
	 */
	public function ListPackagesForProjectAndUser(ListPackagesForProjectAndUser $parameters) {
		return $this->__soapCall ( 'ListPackagesForProjectAndUser', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param PreparePackageForDownload $parameters        	
	 * @access public
	 * @return PreparePackageForDownloadResponse
	 */
	public function PreparePackageForDownload(PreparePackageForDownload $parameters) {
		return $this->__soapCall ( 'PreparePackageForDownload', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ListContentOfPackages $parameters        	
	 * @access public
	 * @return ListContentOfPackagesResponse
	 */
	public function ListContentOfPackages(ListContentOfPackages $parameters) {
		return $this->__soapCall ( 'ListContentOfPackages', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param DeliverPackage $parameters        	
	 * @access public
	 * @return DeliverPackageResponse
	 */
	public function DeliverPackage(DeliverPackage $parameters) {
		return $this->__soapCall ( 'DeliverPackage', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param CreateProjectFromPackage $parameters        	
	 * @access public
	 * @return CreateProjectFromPackageResponse
	 */
	public function CreateProjectFromPackage(CreateProjectFromPackage $parameters) {
		return $this->__soapCall ( 'CreateProjectFromPackage', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param CreateProjectFromPackage2 $parameters        	
	 * @access public
	 * @return CreateProjectFromPackage2Response
	 */
	public function CreateProjectFromPackage2(CreateProjectFromPackage2 $parameters) {
		return $this->__soapCall ( 'CreateProjectFromPackage2', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param UpdateProjectFromPackage $parameters        	
	 * @access public
	 * @return UpdateProjectFromPackageResponse
	 */
	public function UpdateProjectFromPackage(UpdateProjectFromPackage $parameters) {
		return $this->__soapCall ( 'UpdateProjectFromPackage', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param CreateDeliveryPackage $parameters        	
	 * @access public
	 * @return CreateDeliveryPackageResponse
	 */
	public function CreateDeliveryPackage(CreateDeliveryPackage $parameters) {
		return $this->__soapCall ( 'CreateDeliveryPackage', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param RunQAGetReport $parameters        	
	 * @access public
	 * @return RunQAGetReportResponse
	 */
	public function RunQAGetReport(RunQAGetReport $parameters) {
		return $this->__soapCall ( 'RunQAGetReport', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param CreateImageLocalizationPack $parameters        	
	 * @access public
	 * @return CreateImageLocalizationPackResponse
	 */
	public function CreateImageLocalizationPack(CreateImageLocalizationPack $parameters) {
		return $this->__soapCall ( 'CreateImageLocalizationPack', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ImportImageLocalizationPack $parameters        	
	 * @access public
	 * @return ImportImageLocalizationPackResponse
	 */
	public function ImportImageLocalizationPack(ImportImageLocalizationPack $parameters) {
		return $this->__soapCall ( 'ImportImageLocalizationPack', array (
				$parameters 
		) );
	}
}
