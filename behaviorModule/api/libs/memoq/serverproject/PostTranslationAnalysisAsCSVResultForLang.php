<?php
class PostTranslationAnalysisAsCSVResultForLang {
	
	/**
	 *
	 * @var base64Binary $ExportedContent
	 * @access public
	 */
	public $ExportedContent = null;
	
	/**
	 *
	 * @var string $LanguageCode
	 * @access public
	 */
	public $LanguageCode = null;
	
	/**
	 *
	 * @access public
	 */
	public function __construct() {
	}
}
