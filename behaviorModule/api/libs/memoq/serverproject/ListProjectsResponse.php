<?php
class ListProjectsResponse {
	
	/**
	 *
	 * @var ServerProjectInfo[] $ListProjectsResult
	 * @access public
	 */
	public $ListProjectsResult = null;
	
	/**
	 *
	 * @param ServerProjectInfo[] $ListProjectsResult        	
	 * @access public
	 */
	public function __construct($ListProjectsResult) {
		$this->ListProjectsResult = $ListProjectsResult;
	}
}
