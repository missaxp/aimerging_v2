<?php
class PretranslateExpectedFinalTranslationState {
	const __default = 'NoChange';
	const NoChange = 'NoChange';
	const Confirmed = 'Confirmed';
	const Proofread = 'Proofread';
	const Pretranslated = 'Pretranslated';
	const Reviewer1Confirmed = 'Reviewer1Confirmed';
}
