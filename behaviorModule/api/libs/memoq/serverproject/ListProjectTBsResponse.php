<?php
class ListProjectTBsResponse {
	
	/**
	 *
	 * @var ServerProjectTBAssignments $ListProjectTBsResult
	 * @access public
	 */
	public $ListProjectTBsResult = null;
	
	/**
	 *
	 * @param ServerProjectTBAssignments $ListProjectTBsResult        	
	 * @access public
	 */
	public function __construct($ListProjectTBsResult) {
		$this->ListProjectTBsResult = $ListProjectTBsResult;
	}
}
