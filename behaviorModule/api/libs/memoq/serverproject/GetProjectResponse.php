<?php
class GetProjectResponse {
	
	/**
	 *
	 * @var ServerProjectInfo $GetProjectResult
	 * @access public
	 */
	public $GetProjectResult = null;
	
	/**
	 *
	 * @param ServerProjectInfo $GetProjectResult        	
	 * @access public
	 */
	public function __construct($GetProjectResult) {
		$this->GetProjectResult = $GetProjectResult;
	}
}
