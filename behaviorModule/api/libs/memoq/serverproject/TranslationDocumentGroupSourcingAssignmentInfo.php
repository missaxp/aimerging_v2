<?php
include_once ('TranslationDocumentRoleAssignmentInfo.php');
class TranslationDocumentGroupSourcingAssignmentInfo extends TranslationDocumentRoleAssignmentInfo {
	
	/**
	 *
	 * @var guid[] $UserGuids
	 * @access public
	 */
	public $UserGuids = null;
	
	/**
	 *
	 * @param int $RoleId        	
	 * @access public
	 */
	public function __construct($RoleId) {
		parent::__construct ( $RoleId );
	}
}
