<?php
class ExpectedSourceStateBeforeXTranslate {
	const __default = 'ProofreadOnly';
	const ProofreadOnly = 'ProofreadOnly';
	const ProofreadOrConfirmed = 'ProofreadOrConfirmed';
	const AllTarget = 'AllTarget';
}
