<?php
include_once ('DocumentHistoryItemInfo.php');
class DocumentBilingualImportHistoryItemInfo extends DocumentHistoryItemInfo {
	
	/**
	 *
	 * @var string $BilingualFormat
	 * @access public
	 */
	public $BilingualFormat = null;
	
	/**
	 *
	 * @param guid $DocumentOrDivisionGuid        	
	 * @param DocumentHistoryItemType $HistoryItemType        	
	 * @param guid $ModifierSVGroupId        	
	 * @param guid $ModifierUserGuid        	
	 * @param dateTime $TimeStamp        	
	 * @access public
	 */
	public function __construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp) {
		parent::__construct ( $DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp );
	}
}
