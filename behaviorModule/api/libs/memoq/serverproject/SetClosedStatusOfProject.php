<?php
class SetClosedStatusOfProject {
	
	/**
	 *
	 * @var guid $serverProjectGuid
	 * @access public
	 */
	public $serverProjectGuid = null;
	
	/**
	 *
	 * @var boolean $isClosed
	 * @access public
	 */
	public $isClosed = null;
	
	/**
	 *
	 * @param guid $serverProjectGuid        	
	 * @param boolean $isClosed        	
	 * @access public
	 */
	public function __construct($serverProjectGuid, $isClosed) {
		$this->serverProjectGuid = $serverProjectGuid;
		$this->isClosed = $isClosed;
	}
}
