<?php
class GetPostTranslationAnalysisReportDataResponse {
	
	/**
	 *
	 * @var PostTranslationAnalysisResultInfo $GetPostTranslationAnalysisReportDataResult
	 * @access public
	 */
	public $GetPostTranslationAnalysisReportDataResult = null;
	
	/**
	 *
	 * @param PostTranslationAnalysisResultInfo $GetPostTranslationAnalysisReportDataResult        	
	 * @access public
	 */
	public function __construct($GetPostTranslationAnalysisReportDataResult) {
		$this->GetPostTranslationAnalysisReportDataResult = $GetPostTranslationAnalysisReportDataResult;
	}
}
