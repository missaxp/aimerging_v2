<?php
class ExportTranslationDocument {
	
	/**
	 *
	 * @var guid $serverProjectGuid
	 * @access public
	 */
	public $serverProjectGuid = null;
	
	/**
	 *
	 * @var guid $docGuid
	 * @access public
	 */
	public $docGuid = null;
	
	/**
	 *
	 * @param guid $serverProjectGuid        	
	 * @param guid $docGuid        	
	 * @access public
	 */
	public function __construct($serverProjectGuid, $docGuid) {
		$this->serverProjectGuid = $serverProjectGuid;
		$this->docGuid = $docGuid;
	}
}
