<?php
include_once ('TranslationDocumentRoleAssignmentInfo.php');
class TranslationDocumentSubvendorAssignmentInfo extends TranslationDocumentRoleAssignmentInfo {
	
	/**
	 *
	 * @var guid $SubvendorGroupGuid
	 * @access public
	 */
	public $SubvendorGroupGuid = null;
	
	/**
	 *
	 * @param int $RoleId        	
	 * @param guid $SubvendorGroupGuid        	
	 * @access public
	 */
	public function __construct($RoleId, $SubvendorGroupGuid) {
		parent::__construct ( $RoleId );
		$this->SubvendorGroupGuid = $SubvendorGroupGuid;
	}
}
