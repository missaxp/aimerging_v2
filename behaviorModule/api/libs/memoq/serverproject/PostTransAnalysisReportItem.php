<?php
class PostTransAnalysisReportItem {
	
	/**
	 *
	 * @var PostTranslationReportCounts $All
	 * @access public
	 */
	public $All = null;
	
	/**
	 *
	 * @var PostTranslationReportCounts $AutoPropagated
	 * @access public
	 */
	public $AutoPropagated = null;
	
	/**
	 *
	 * @var PostTranslationReportCounts $Fragments
	 * @access public
	 */
	public $Fragments = null;
	
	/**
	 *
	 * @var PostTranslationReportCounts $Hit100
	 * @access public
	 */
	public $Hit100 = null;
	
	/**
	 *
	 * @var PostTranslationReportCounts $Hit101
	 * @access public
	 */
	public $Hit101 = null;
	
	/**
	 *
	 * @var PostTranslationReportCounts $Hit50_74
	 * @access public
	 */
	public $Hit50_74 = null;
	
	/**
	 *
	 * @var PostTranslationReportCounts $Hit75_84
	 * @access public
	 */
	public $Hit75_84 = null;
	
	/**
	 *
	 * @var PostTranslationReportCounts $Hit85_94
	 * @access public
	 */
	public $Hit85_94 = null;
	
	/**
	 *
	 * @var PostTranslationReportCounts $Hit95_99
	 * @access public
	 */
	public $Hit95_99 = null;
	
	/**
	 *
	 * @var PostTranslationReportCounts $NoMatch
	 * @access public
	 */
	public $NoMatch = null;
	
	/**
	 *
	 * @var PostTranslationReportCounts $XTranslated
	 * @access public
	 */
	public $XTranslated = null;
	
	/**
	 *
	 * @access public
	 */
	public function __construct() {
	}
}
