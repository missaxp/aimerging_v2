<?php
class ExportTranslationDocument2Response {
	
	/**
	 *
	 * @var TranslationDocExportResultInfo $ExportTranslationDocument2Result
	 * @access public
	 */
	public $ExportTranslationDocument2Result = null;
	
	/**
	 *
	 * @param TranslationDocExportResultInfo $ExportTranslationDocument2Result        	
	 * @access public
	 */
	public function __construct($ExportTranslationDocument2Result) {
		$this->ExportTranslationDocument2Result = $ExportTranslationDocument2Result;
	}
}
