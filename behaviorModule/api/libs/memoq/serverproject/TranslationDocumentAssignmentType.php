<?php
class TranslationDocumentAssignmentType {
	const __default = 'SingleUser';
	const SingleUser = 'SingleUser';
	const FirstAccept = 'FirstAccept';
	const GroupSourcing = 'GroupSourcing';
	const Subvendor = 'Subvendor';
	const NoUser = 'NoUser';
}
