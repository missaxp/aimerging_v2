<?php
class ListProjectUsersResponse {
	
	/**
	 *
	 * @var ServerProjectUserInfoHeader[] $ListProjectUsersResult
	 * @access public
	 */
	public $ListProjectUsersResult = null;
	
	/**
	 *
	 * @param ServerProjectUserInfoHeader[] $ListProjectUsersResult        	
	 * @access public
	 */
	public function __construct($ListProjectUsersResult) {
		$this->ListProjectUsersResult = $ListProjectUsersResult;
	}
}
