<?php
class UserInfoHeader {
	
	/**
	 *
	 * @var string $FullName
	 * @access public
	 */
	public $FullName = null;
	
	/**
	 *
	 * @var guid $UserGuid
	 * @access public
	 */
	public $UserGuid = null;
	
	/**
	 *
	 * @var string $UserName
	 * @access public
	 */
	public $UserName = null;
	
	/**
	 *
	 * @param guid $UserGuid        	
	 * @access public
	 */
	public function __construct($UserGuid) {
		$this->UserGuid = $UserGuid;
	}
}
