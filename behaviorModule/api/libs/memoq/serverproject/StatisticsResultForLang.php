<?php
class StatisticsResultForLang {
	
	/**
	 *
	 * @var base64Binary $ResultData
	 * @access public
	 */
	public $ResultData = null;
	
	/**
	 *
	 * @var string $TargetLangCode
	 * @access public
	 */
	public $TargetLangCode = null;
	
	/**
	 *
	 * @access public
	 */
	public function __construct() {
	}
}
