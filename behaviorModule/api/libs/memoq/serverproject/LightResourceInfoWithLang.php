<?php
include_once ('LightResourceInfo.php');
class LightResourceInfoWithLang extends LightResourceInfo {
	
	/**
	 *
	 * @var string $LanguageCode
	 * @access public
	 */
	public $LanguageCode = null;
	
	/**
	 *
	 * @param boolean $IsDefault        	
	 * @access public
	 */
	public function __construct($IsDefault) {
		parent::__construct ( $IsDefault );
	}
}
