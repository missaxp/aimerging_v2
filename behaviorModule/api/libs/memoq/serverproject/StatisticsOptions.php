<?php
class StatisticsOptions {
	
	/**
	 *
	 * @var StatisticsAlgorithm $Algorithm
	 * @access public
	 */
	public $Algorithm = null;
	
	/**
	 *
	 * @var boolean $Analysis_Homogenity
	 * @access public
	 */
	public $Analysis_Homogenity = null;
	
	/**
	 *
	 * @var boolean $Analysis_ProjectTMs
	 * @access public
	 */
	public $Analysis_ProjectTMs = null;
	
	/**
	 *
	 * @var boolean $Analyzis_DetailsByTM
	 * @access public
	 */
	public $Analyzis_DetailsByTM = null;
	
	/**
	 *
	 * @var boolean $DisableCrossFileRepetition
	 * @access public
	 */
	public $DisableCrossFileRepetition = null;
	
	/**
	 *
	 * @var boolean $IncludeLockedRows
	 * @access public
	 */
	public $IncludeLockedRows = null;
	
	/**
	 *
	 * @var boolean $RepetitionPreferenceOver100
	 * @access public
	 */
	public $RepetitionPreferenceOver100 = null;
	
	/**
	 *
	 * @var boolean $ShowCounts
	 * @access public
	 */
	public $ShowCounts = null;
	
	/**
	 *
	 * @var boolean $ShowCounts_IncludeTargetCount
	 * @access public
	 */
	public $ShowCounts_IncludeTargetCount = null;
	
	/**
	 *
	 * @var boolean $ShowCounts_IncludeWhitespacesInCharCount
	 * @access public
	 */
	public $ShowCounts_IncludeWhitespacesInCharCount = null;
	
	/**
	 *
	 * @var boolean $ShowCounts_StatusReport
	 * @access public
	 */
	public $ShowCounts_StatusReport = null;
	
	/**
	 *
	 * @var boolean $ShowResultsPerFile
	 * @access public
	 */
	public $ShowResultsPerFile = null;
	
	/**
	 *
	 * @var float $TagCharWeight
	 * @access public
	 */
	public $TagCharWeight = null;
	
	/**
	 *
	 * @var float $TagWordWeight
	 * @access public
	 */
	public $TagWordWeight = null;
	
	/**
	 *
	 * @param StatisticsAlgorithm $Algorithm        	
	 * @param boolean $Analysis_Homogenity        	
	 * @param boolean $Analysis_ProjectTMs        	
	 * @param boolean $Analyzis_DetailsByTM        	
	 * @param boolean $DisableCrossFileRepetition        	
	 * @param boolean $IncludeLockedRows        	
	 * @param boolean $RepetitionPreferenceOver100        	
	 * @param boolean $ShowCounts        	
	 * @param boolean $ShowCounts_IncludeTargetCount        	
	 * @param boolean $ShowCounts_IncludeWhitespacesInCharCount        	
	 * @param boolean $ShowCounts_StatusReport        	
	 * @param boolean $ShowResultsPerFile        	
	 * @param float $TagCharWeight        	
	 * @param float $TagWordWeight        	
	 * @access public
	 */
	public function __construct($Algorithm, $Analysis_Homogenity, $Analysis_ProjectTMs, $Analyzis_DetailsByTM, $DisableCrossFileRepetition, $IncludeLockedRows, $RepetitionPreferenceOver100, $ShowCounts, $ShowCounts_IncludeTargetCount, $ShowCounts_IncludeWhitespacesInCharCount, $ShowCounts_StatusReport, $ShowResultsPerFile, $TagCharWeight, $TagWordWeight) {
		$this->Algorithm = $Algorithm;
		$this->Analysis_Homogenity = $Analysis_Homogenity;
		$this->Analysis_ProjectTMs = $Analysis_ProjectTMs;
		$this->Analyzis_DetailsByTM = $Analyzis_DetailsByTM;
		$this->DisableCrossFileRepetition = $DisableCrossFileRepetition;
		$this->IncludeLockedRows = $IncludeLockedRows;
		$this->RepetitionPreferenceOver100 = $RepetitionPreferenceOver100;
		$this->ShowCounts = $ShowCounts;
		$this->ShowCounts_IncludeTargetCount = $ShowCounts_IncludeTargetCount;
		$this->ShowCounts_IncludeWhitespacesInCharCount = $ShowCounts_IncludeWhitespacesInCharCount;
		$this->ShowCounts_StatusReport = $ShowCounts_StatusReport;
		$this->ShowResultsPerFile = $ShowResultsPerFile;
		$this->TagCharWeight = $TagCharWeight;
		$this->TagWordWeight = $TagWordWeight;
	}
}
