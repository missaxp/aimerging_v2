<?php
class DocumentStatus {
	const __default = 'TranslationInProgress';
	const TranslationInProgress = 'TranslationInProgress';
	const TranslationFinished = 'TranslationFinished';
	const ProofreadingFinished = 'ProofreadingFinished';
}
