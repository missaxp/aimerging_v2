<?php
class ServerProjectTMAssignmentsForTargetLang {
	
	/**
	 *
	 * @var guid $MasterTMGuid
	 * @access public
	 */
	public $MasterTMGuid = null;
	
	/**
	 *
	 * @var guid $PrimaryTMGuid
	 * @access public
	 */
	public $PrimaryTMGuid = null;
	
	/**
	 *
	 * @var guid[] $TMGuids
	 * @access public
	 */
	public $TMGuids = null;
	
	/**
	 *
	 * @var string $TargetLangCode
	 * @access public
	 */
	public $TargetLangCode = null;
	
	/**
	 *
	 * @param guid $MasterTMGuid        	
	 * @param guid $PrimaryTMGuid        	
	 * @access public
	 */
	public function __construct($MasterTMGuid, $PrimaryTMGuid) {
		$this->MasterTMGuid = $MasterTMGuid;
		$this->PrimaryTMGuid = $PrimaryTMGuid;
	}
}
