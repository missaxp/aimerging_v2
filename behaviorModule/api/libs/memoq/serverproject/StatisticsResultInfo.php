<?php
include_once ('ResultInfo.php');
class StatisticsResultInfo extends ResultInfo {
	
	/**
	 *
	 * @var StatisticsResultForLang[] $ResultsForTargetLangs
	 * @access public
	 */
	public $ResultsForTargetLangs = null;
	
	/**
	 *
	 * @param ResultStatus $ResultStatus        	
	 * @access public
	 */
	public function __construct($ResultStatus) {
		parent::__construct ( $ResultStatus );
	}
}
