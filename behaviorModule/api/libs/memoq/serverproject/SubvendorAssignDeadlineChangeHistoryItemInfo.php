<?php
include_once ('DocumentHistoryItemInfo.php');
class SubvendorAssignDeadlineChangeHistoryItemInfo extends DocumentHistoryItemInfo {
	
	/**
	 *
	 * @var dateTime $Deadline
	 * @access public
	 */
	public $Deadline = null;
	
	/**
	 *
	 * @param guid $DocumentOrDivisionGuid        	
	 * @param DocumentHistoryItemType $HistoryItemType        	
	 * @param guid $ModifierSVGroupId        	
	 * @param guid $ModifierUserGuid        	
	 * @param dateTime $TimeStamp        	
	 * @param dateTime $Deadline        	
	 * @access public
	 */
	public function __construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp, $Deadline) {
		parent::__construct ( $DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp );
		$this->Deadline = $Deadline;
	}
}
