<?php
class ServerProjectInfo {
	
	/**
	 *
	 * @var boolean $AllowOverlappingWorkflow
	 * @access public
	 */
	public $AllowOverlappingWorkflow = null;
	
	/**
	 *
	 * @var boolean $AllowPackageCreation
	 * @access public
	 */
	public $AllowPackageCreation = null;
	
	/**
	 *
	 * @var string $Client
	 * @access public
	 */
	public $Client = null;
	
	/**
	 *
	 * @var int $ConfirmedCharacterCount
	 * @access public
	 */
	public $ConfirmedCharacterCount = null;
	
	/**
	 *
	 * @var int $ConfirmedSegmentCount
	 * @access public
	 */
	public $ConfirmedSegmentCount = null;
	
	/**
	 *
	 * @var int $ConfirmedWordCount
	 * @access public
	 */
	public $ConfirmedWordCount = null;
	
	/**
	 *
	 * @var boolean $CreateOfflineTMTBCopies
	 * @access public
	 */
	public $CreateOfflineTMTBCopies = null;
	
	/**
	 *
	 * @var dateTime $CreationTime
	 * @access public
	 */
	public $CreationTime = null;
	
	/**
	 *
	 * @var guid $CreatorUser
	 * @access public
	 */
	public $CreatorUser = null;
	
	/**
	 *
	 * @var string $CustomMetas
	 * @access public
	 */
	public $CustomMetas = null;
	
	/**
	 *
	 * @var dateTime $Deadline
	 * @access public
	 */
	public $Deadline = null;
	
	/**
	 *
	 * @var string $Description
	 * @access public
	 */
	public $Description = null;
	
	/**
	 *
	 * @var boolean $DesktopDocs
	 * @access public
	 */
	public $DesktopDocs = null;
	
	/**
	 *
	 * @var DocumentStatus $DocumentStatus
	 * @access public
	 */
	public $DocumentStatus = null;
	
	/**
	 *
	 * @var string $Domain
	 * @access public
	 */
	public $Domain = null;
	
	/**
	 *
	 * @var boolean $DownloadPreview
	 * @access public
	 */
	public $DownloadPreview = null;
	
	/**
	 *
	 * @var boolean $DownloadSkeleton
	 * @access public
	 */
	public $DownloadSkeleton = null;
	
	/**
	 *
	 * @var boolean $EnableCommunication
	 * @access public
	 */
	public $EnableCommunication = null;
	
	/**
	 *
	 * @var boolean $EnableSplitJoin
	 * @access public
	 */
	public $EnableSplitJoin = null;
	
	/**
	 *
	 * @var boolean $EnableWebTrans
	 * @access public
	 */
	public $EnableWebTrans = null;
	
	/**
	 *
	 * @var int $LockedCharacterCount
	 * @access public
	 */
	public $LockedCharacterCount = null;
	
	/**
	 *
	 * @var int $LockedSegmentCount
	 * @access public
	 */
	public $LockedSegmentCount = null;
	
	/**
	 *
	 * @var int $LockedWordCount
	 * @access public
	 */
	public $LockedWordCount = null;
	
	/**
	 *
	 * @var string $Name
	 * @access public
	 */
	public $Name = null;
	
	/**
	 *
	 * @var ServerProjectResourcesInPackages $PackageResourceHandling
	 * @access public
	 */
	public $PackageResourceHandling = null;
	
	/**
	 *
	 * @var boolean $PreventDeliveryOnQAError
	 * @access public
	 */
	public $PreventDeliveryOnQAError = null;
	
	/**
	 *
	 * @var string $Project
	 * @access public
	 */
	public $Project = null;
	
	/**
	 *
	 * @var ServerProjectStatus $ProjectStatus
	 * @access public
	 */
	public $ProjectStatus = null;
	
	/**
	 *
	 * @var int $ProofreadCharacterCount
	 * @access public
	 */
	public $ProofreadCharacterCount = null;
	
	/**
	 *
	 * @var int $ProofreadSegmentCount
	 * @access public
	 */
	public $ProofreadSegmentCount = null;
	
	/**
	 *
	 * @var int $ProofreadWordCount
	 * @access public
	 */
	public $ProofreadWordCount = null;
	
	/**
	 *
	 * @var boolean $RecordVersionHistory
	 * @access public
	 */
	public $RecordVersionHistory = null;
	
	/**
	 *
	 * @var int $Reviewer1ConfirmedCharacterCount
	 * @access public
	 */
	public $Reviewer1ConfirmedCharacterCount = null;
	
	/**
	 *
	 * @var int $Reviewer1ConfirmedSegmentCount
	 * @access public
	 */
	public $Reviewer1ConfirmedSegmentCount = null;
	
	/**
	 *
	 * @var int $Reviewer1ConfirmedWordCount
	 * @access public
	 */
	public $Reviewer1ConfirmedWordCount = null;
	
	/**
	 *
	 * @var guid $ServerProjectGuid
	 * @access public
	 */
	public $ServerProjectGuid = null;
	
	/**
	 *
	 * @var string $SourceLanguageCode
	 * @access public
	 */
	public $SourceLanguageCode = null;
	
	/**
	 *
	 * @var boolean $StrictSubLangMatching
	 * @access public
	 */
	public $StrictSubLangMatching = null;
	
	/**
	 *
	 * @var string $Subject
	 * @access public
	 */
	public $Subject = null;
	
	/**
	 *
	 * @var string[] $TargetLanguageCodes
	 * @access public
	 */
	public $TargetLanguageCodes = null;
	
	/**
	 *
	 * @var dateTime $TimeClosed
	 * @access public
	 */
	public $TimeClosed = null;
	
	/**
	 *
	 * @var int $TotalCharacterCount
	 * @access public
	 */
	public $TotalCharacterCount = null;
	
	/**
	 *
	 * @var int $TotalSegmentCount
	 * @access public
	 */
	public $TotalSegmentCount = null;
	
	/**
	 *
	 * @var int $TotalWordCount
	 * @access public
	 */
	public $TotalWordCount = null;
	
	/**
	 *
	 * @param boolean $AllowOverlappingWorkflow        	
	 * @param boolean $AllowPackageCreation        	
	 * @param int $ConfirmedCharacterCount        	
	 * @param int $ConfirmedSegmentCount        	
	 * @param int $ConfirmedWordCount        	
	 * @param boolean $CreateOfflineTMTBCopies        	
	 * @param dateTime $CreationTime        	
	 * @param guid $CreatorUser        	
	 * @param dateTime $Deadline        	
	 * @param boolean $DesktopDocs        	
	 * @param DocumentStatus $DocumentStatus        	
	 * @param boolean $DownloadPreview        	
	 * @param boolean $DownloadSkeleton        	
	 * @param boolean $EnableCommunication        	
	 * @param boolean $EnableSplitJoin        	
	 * @param boolean $EnableWebTrans        	
	 * @param int $LockedCharacterCount        	
	 * @param int $LockedSegmentCount        	
	 * @param int $LockedWordCount        	
	 * @param ServerProjectResourcesInPackages $PackageResourceHandling        	
	 * @param boolean $PreventDeliveryOnQAError        	
	 * @param ServerProjectStatus $ProjectStatus        	
	 * @param int $ProofreadCharacterCount        	
	 * @param int $ProofreadSegmentCount        	
	 * @param int $ProofreadWordCount        	
	 * @param boolean $RecordVersionHistory        	
	 * @param int $Reviewer1ConfirmedCharacterCount        	
	 * @param int $Reviewer1ConfirmedSegmentCount        	
	 * @param int $Reviewer1ConfirmedWordCount        	
	 * @param guid $ServerProjectGuid        	
	 * @param boolean $StrictSubLangMatching        	
	 * @param dateTime $TimeClosed        	
	 * @param int $TotalCharacterCount        	
	 * @param int $TotalSegmentCount        	
	 * @param int $TotalWordCount        	
	 * @access public
	 */
	public function __construct($AllowOverlappingWorkflow, $AllowPackageCreation, $ConfirmedCharacterCount, $ConfirmedSegmentCount, $ConfirmedWordCount, $CreateOfflineTMTBCopies, $CreationTime, $CreatorUser, $Deadline, $DesktopDocs, $DocumentStatus, $DownloadPreview, $DownloadSkeleton, $EnableCommunication, $EnableSplitJoin, $EnableWebTrans, $LockedCharacterCount, $LockedSegmentCount, $LockedWordCount, $PackageResourceHandling, $PreventDeliveryOnQAError, $ProjectStatus, $ProofreadCharacterCount, $ProofreadSegmentCount, $ProofreadWordCount, $RecordVersionHistory, $Reviewer1ConfirmedCharacterCount, $Reviewer1ConfirmedSegmentCount, $Reviewer1ConfirmedWordCount, $ServerProjectGuid, $StrictSubLangMatching, $TimeClosed, $TotalCharacterCount, $TotalSegmentCount, $TotalWordCount) {
		$this->AllowOverlappingWorkflow = $AllowOverlappingWorkflow;
		$this->AllowPackageCreation = $AllowPackageCreation;
		$this->ConfirmedCharacterCount = $ConfirmedCharacterCount;
		$this->ConfirmedSegmentCount = $ConfirmedSegmentCount;
		$this->ConfirmedWordCount = $ConfirmedWordCount;
		$this->CreateOfflineTMTBCopies = $CreateOfflineTMTBCopies;
		$this->CreationTime = $CreationTime;
		$this->CreatorUser = $CreatorUser;
		$this->Deadline = $Deadline;
		$this->DesktopDocs = $DesktopDocs;
		$this->DocumentStatus = $DocumentStatus;
		$this->DownloadPreview = $DownloadPreview;
		$this->DownloadSkeleton = $DownloadSkeleton;
		$this->EnableCommunication = $EnableCommunication;
		$this->EnableSplitJoin = $EnableSplitJoin;
		$this->EnableWebTrans = $EnableWebTrans;
		$this->LockedCharacterCount = $LockedCharacterCount;
		$this->LockedSegmentCount = $LockedSegmentCount;
		$this->LockedWordCount = $LockedWordCount;
		$this->PackageResourceHandling = $PackageResourceHandling;
		$this->PreventDeliveryOnQAError = $PreventDeliveryOnQAError;
		$this->ProjectStatus = $ProjectStatus;
		$this->ProofreadCharacterCount = $ProofreadCharacterCount;
		$this->ProofreadSegmentCount = $ProofreadSegmentCount;
		$this->ProofreadWordCount = $ProofreadWordCount;
		$this->RecordVersionHistory = $RecordVersionHistory;
		$this->Reviewer1ConfirmedCharacterCount = $Reviewer1ConfirmedCharacterCount;
		$this->Reviewer1ConfirmedSegmentCount = $Reviewer1ConfirmedSegmentCount;
		$this->Reviewer1ConfirmedWordCount = $Reviewer1ConfirmedWordCount;
		$this->ServerProjectGuid = $ServerProjectGuid;
		$this->StrictSubLangMatching = $StrictSubLangMatching;
		$this->TimeClosed = $TimeClosed;
		$this->TotalCharacterCount = $TotalCharacterCount;
		$this->TotalSegmentCount = $TotalSegmentCount;
		$this->TotalWordCount = $TotalWordCount;
	}
}
