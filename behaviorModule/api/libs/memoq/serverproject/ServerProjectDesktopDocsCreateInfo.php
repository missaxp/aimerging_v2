<?php
include_once ('ServerProjectCreateInfo.php');
class ServerProjectDesktopDocsCreateInfo extends ServerProjectCreateInfo {
	
	/**
	 *
	 * @var string $CallbackWebServiceUrl
	 * @access public
	 */
	public $CallbackWebServiceUrl = null;
	
	/**
	 *
	 * @var boolean $DownloadPreview
	 * @access public
	 */
	public $DownloadPreview = null;
	
	/**
	 *
	 * @var boolean $DownloadSkeleton
	 * @access public
	 */
	public $DownloadSkeleton = null;
	
	/**
	 *
	 * @var boolean $EnableSplitJoin
	 * @access public
	 */
	public $EnableSplitJoin = null;
	
	/**
	 *
	 * @var boolean $EnableWebTrans
	 * @access public
	 */
	public $EnableWebTrans = null;
	
	/**
	 *
	 * @param boolean $AllowOverlappingWorkflow        	
	 * @param boolean $AllowPackageCreation        	
	 * @param guid $CreatorUser        	
	 * @param dateTime $Deadline        	
	 * @param boolean $DownloadPreview2        	
	 * @param boolean $DownloadSkeleton2        	
	 * @param boolean $EnableCommunication        	
	 * @param ServerProjectResourcesInPackages $PackageResourceHandling        	
	 * @param boolean $PreventDeliveryOnQAError        	
	 * @param boolean $RecordVersionHistory        	
	 * @param boolean $StrictSubLangMatching        	
	 * @param boolean $DownloadPreview        	
	 * @param boolean $DownloadSkeleton        	
	 * @param boolean $EnableSplitJoin        	
	 * @param boolean $EnableWebTrans        	
	 * @access public
	 */
	public function __construct($AllowOverlappingWorkflow, $AllowPackageCreation, $CreatorUser, $Deadline, $DownloadPreview2, $DownloadSkeleton2, $EnableCommunication, $PackageResourceHandling, $PreventDeliveryOnQAError, $RecordVersionHistory, $StrictSubLangMatching, $DownloadPreview, $DownloadSkeleton, $EnableSplitJoin, $EnableWebTrans) {
		parent::__construct ( $AllowOverlappingWorkflow, $AllowPackageCreation, $CreatorUser, $Deadline, $DownloadPreview2, $DownloadSkeleton2, $EnableCommunication, $PackageResourceHandling, $PreventDeliveryOnQAError, $RecordVersionHistory, $StrictSubLangMatching );
		$this->DownloadPreview = $DownloadPreview;
		$this->DownloadSkeleton = $DownloadSkeleton;
		$this->EnableSplitJoin = $EnableSplitJoin;
		$this->EnableWebTrans = $EnableWebTrans;
	}
}
