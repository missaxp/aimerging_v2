<?php
class ConfirmAndUpdateSegmentStatuses {
	const __default = 'Edited';
	const Edited = 'Edited';
	const Confirmed = 'Confirmed';
	const Proofread = 'Proofread';
	const PreTranslated = 'PreTranslated';
	const Locked = 'Locked';
	const All = 'All';
	const Reviewer1Confirmed = 'Reviewer1Confirmed';
}
