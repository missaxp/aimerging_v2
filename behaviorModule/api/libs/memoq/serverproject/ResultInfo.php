<?php
class ResultInfo {
	
	/**
	 *
	 * @var string $DetailedMessage
	 * @access public
	 */
	public $DetailedMessage = null;
	
	/**
	 *
	 * @var string $MainMessage
	 * @access public
	 */
	public $MainMessage = null;
	
	/**
	 *
	 * @var ResultStatus $ResultStatus
	 * @access public
	 */
	public $ResultStatus = null;
	
	/**
	 *
	 * @param ResultStatus $ResultStatus        	
	 * @access public
	 */
	public function __construct($ResultStatus) {
		$this->ResultStatus = $ResultStatus;
	}
}
