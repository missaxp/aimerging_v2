<?php
class NewRevisionScenarioOptions {
	
	/**
	 *
	 * @var ExpectedFinalStateAfterXTranslate $ExpectedFinalState
	 * @access public
	 */
	public $ExpectedFinalState = null;
	
	/**
	 *
	 * @var boolean $InsertEmptyTranslations
	 * @access public
	 */
	public $InsertEmptyTranslations = null;
	
	/**
	 *
	 * @var boolean $LockXTranslatedRows
	 * @access public
	 */
	public $LockXTranslatedRows = null;
	
	/**
	 *
	 * @var ExpectedSourceStateBeforeXTranslate $SourceFilter
	 * @access public
	 */
	public $SourceFilter = null;
	
	/**
	 *
	 * @param ExpectedFinalStateAfterXTranslate $ExpectedFinalState        	
	 * @param boolean $InsertEmptyTranslations        	
	 * @param boolean $LockXTranslatedRows        	
	 * @param ExpectedSourceStateBeforeXTranslate $SourceFilter        	
	 * @access public
	 */
	public function __construct($ExpectedFinalState, $InsertEmptyTranslations, $LockXTranslatedRows, $SourceFilter) {
		$this->ExpectedFinalState = $ExpectedFinalState;
		$this->InsertEmptyTranslations = $InsertEmptyTranslations;
		$this->LockXTranslatedRows = $LockXTranslatedRows;
		$this->SourceFilter = $SourceFilter;
	}
}
