<?php
class ReImportTranslationDocumentsWithFilterConfigResource {
	
	/**
	 *
	 * @var guid $serverProjectGuid
	 * @access public
	 */
	public $serverProjectGuid = null;
	
	/**
	 *
	 * @var ReimportDocumentOptions[] $reimportActions
	 * @access public
	 */
	public $reimportActions = null;
	
	/**
	 *
	 * @var guid $filterConfigResGuid
	 * @access public
	 */
	public $filterConfigResGuid = null;
	
	/**
	 *
	 * @param guid $serverProjectGuid        	
	 * @param ReimportDocumentOptions[] $reimportActions        	
	 * @param guid $filterConfigResGuid        	
	 * @access public
	 */
	public function __construct($serverProjectGuid, $reimportActions, $filterConfigResGuid) {
		$this->serverProjectGuid = $serverProjectGuid;
		$this->reimportActions = $reimportActions;
		$this->filterConfigResGuid = $filterConfigResGuid;
	}
}
