<?php
class TwoColumnRtfBilingualExportOptions {
	
	/**
	 *
	 * @var boolean $ExportComment
	 * @access public
	 */
	public $ExportComment = null;
	
	/**
	 *
	 * @var boolean $ExportSegmentStatus
	 * @access public
	 */
	public $ExportSegmentStatus = null;
	
	/**
	 *
	 * @var boolean $ExportTwoTargetColumns
	 * @access public
	 */
	public $ExportTwoTargetColumns = null;
	
	/**
	 *
	 * @var boolean $SecondTargetColumnEmpty
	 * @access public
	 */
	public $SecondTargetColumnEmpty = null;
	
	/**
	 *
	 * @param boolean $ExportComment        	
	 * @param boolean $ExportSegmentStatus        	
	 * @param boolean $ExportTwoTargetColumns        	
	 * @param boolean $SecondTargetColumnEmpty        	
	 * @access public
	 */
	public function __construct($ExportComment, $ExportSegmentStatus, $ExportTwoTargetColumns, $SecondTargetColumnEmpty) {
		$this->ExportComment = $ExportComment;
		$this->ExportSegmentStatus = $ExportSegmentStatus;
		$this->ExportTwoTargetColumns = $ExportTwoTargetColumns;
		$this->SecondTargetColumnEmpty = $SecondTargetColumnEmpty;
	}
}
