<?php
include_once ('ResultInfo.php');
class FileResultInfo extends ResultInfo {
	
	/**
	 *
	 * @var guid $FileGuid
	 * @access public
	 */
	public $FileGuid = null;
	
	/**
	 *
	 * @param ResultStatus $ResultStatus        	
	 * @param guid $FileGuid        	
	 * @access public
	 */
	public function __construct($ResultStatus, $FileGuid) {
		parent::__construct ( $ResultStatus );
		$this->FileGuid = $FileGuid;
	}
}
