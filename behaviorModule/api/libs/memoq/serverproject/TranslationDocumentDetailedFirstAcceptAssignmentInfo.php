<?php
include_once ('TranslationDocumentDetailedRoleAssignmentInfo.php');
class TranslationDocumentDetailedFirstAcceptAssignmentInfo extends TranslationDocumentDetailedRoleAssignmentInfo {
	
	/**
	 *
	 * @var dateTime $FirstAcceptDeadline
	 * @access public
	 */
	public $FirstAcceptDeadline = null;
	
	/**
	 *
	 * @var FirstAcceptStatus $Status
	 * @access public
	 */
	public $Status = null;
	
	/**
	 *
	 * @var TranslationDocumentFirstAcceptUserInfo[] $Users
	 * @access public
	 */
	public $Users = null;
	
	/**
	 *
	 * @param int $RoleId        	
	 * @param dateTime $FirstAcceptDeadline        	
	 * @param FirstAcceptStatus $Status        	
	 * @access public
	 */
	public function __construct($RoleId, $FirstAcceptDeadline, $Status) {
		parent::__construct ( $RoleId );
		$this->FirstAcceptDeadline = $FirstAcceptDeadline;
		$this->Status = $Status;
	}
}
