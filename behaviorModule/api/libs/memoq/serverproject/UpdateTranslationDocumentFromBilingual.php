<?php
class UpdateTranslationDocumentFromBilingual {
	
	/**
	 *
	 * @var guid $serverProjectGuid
	 * @access public
	 */
	public $serverProjectGuid = null;
	
	/**
	 *
	 * @var guid $fileGuid
	 * @access public
	 */
	public $fileGuid = null;
	
	/**
	 *
	 * @var BilingualDocFormat $docFormat
	 * @access public
	 */
	public $docFormat = null;
	
	/**
	 *
	 * @param guid $serverProjectGuid        	
	 * @param guid $fileGuid        	
	 * @param BilingualDocFormat $docFormat        	
	 * @access public
	 */
	public function __construct($serverProjectGuid, $fileGuid, $docFormat) {
		$this->serverProjectGuid = $serverProjectGuid;
		$this->fileGuid = $fileGuid;
		$this->docFormat = $docFormat;
	}
}
