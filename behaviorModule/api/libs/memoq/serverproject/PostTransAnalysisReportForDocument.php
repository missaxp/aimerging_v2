<?php
class PostTransAnalysisReportForDocument {
	
	/**
	 *
	 * @var PostTransAnalysisReportForUser[] $ByUser
	 * @access public
	 */
	public $ByUser = null;
	
	/**
	 *
	 * @var guid $DocumentGuid
	 * @access public
	 */
	public $DocumentGuid = null;
	
	/**
	 *
	 * @var PostTransAnalysisReportItem $Summary
	 * @access public
	 */
	public $Summary = null;
	
	/**
	 *
	 * @param guid $DocumentGuid        	
	 * @access public
	 */
	public function __construct($DocumentGuid) {
		$this->DocumentGuid = $DocumentGuid;
	}
}
