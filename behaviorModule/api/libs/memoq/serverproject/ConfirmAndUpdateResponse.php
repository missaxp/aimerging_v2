<?php
class ConfirmAndUpdateResponse {
	
	/**
	 *
	 * @var ConfirmAndUpdateResultInfo $ConfirmAndUpdateResult
	 * @access public
	 */
	public $ConfirmAndUpdateResult = null;
	
	/**
	 *
	 * @param ConfirmAndUpdateResultInfo $ConfirmAndUpdateResult        	
	 * @access public
	 */
	public function __construct($ConfirmAndUpdateResult) {
		$this->ConfirmAndUpdateResult = $ConfirmAndUpdateResult;
	}
}
