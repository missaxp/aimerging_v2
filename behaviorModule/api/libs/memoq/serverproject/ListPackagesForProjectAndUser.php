<?php
class ListPackagesForProjectAndUser {
	
	/**
	 *
	 * @var guid $projectGuid
	 * @access public
	 */
	public $projectGuid = null;
	
	/**
	 *
	 * @var guid $userGuid
	 * @access public
	 */
	public $userGuid = null;
	
	/**
	 *
	 * @param guid $projectGuid        	
	 * @param guid $userGuid        	
	 * @access public
	 */
	public function __construct($projectGuid, $userGuid) {
		$this->projectGuid = $projectGuid;
		$this->userGuid = $userGuid;
	}
}
