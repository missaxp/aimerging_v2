<?php
class AsiaOnlineDomainCombination {
	
	/**
	 *
	 * @var int $DomainCombinationCode
	 * @access public
	 */
	public $DomainCombinationCode = null;
	
	/**
	 *
	 * @var string $DomainCombinationName
	 * @access public
	 */
	public $DomainCombinationName = null;
	
	/**
	 *
	 * @param int $DomainCombinationCode        	
	 * @access public
	 */
	public function __construct($DomainCombinationCode) {
		$this->DomainCombinationCode = $DomainCombinationCode;
	}
}
