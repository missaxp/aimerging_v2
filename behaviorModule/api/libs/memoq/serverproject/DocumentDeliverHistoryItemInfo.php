<?php
include_once ('DocumentHistoryItemInfo.php');
class DocumentDeliverHistoryItemInfo extends DocumentHistoryItemInfo {
	
	/**
	 *
	 * @var WorkflowStatus $NewWorkflowStatus
	 * @access public
	 */
	public $NewWorkflowStatus = null;
	
	/**
	 *
	 * @param guid $DocumentOrDivisionGuid        	
	 * @param DocumentHistoryItemType $HistoryItemType        	
	 * @param guid $ModifierSVGroupId        	
	 * @param guid $ModifierUserGuid        	
	 * @param dateTime $TimeStamp        	
	 * @param WorkflowStatus $NewWorkflowStatus        	
	 * @access public
	 */
	public function __construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp, $NewWorkflowStatus) {
		parent::__construct ( $DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp );
		$this->NewWorkflowStatus = $NewWorkflowStatus;
	}
}
