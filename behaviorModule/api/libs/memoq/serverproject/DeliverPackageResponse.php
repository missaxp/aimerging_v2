<?php
class DeliverPackageResponse {
	
	/**
	 *
	 * @var PackageDeliveryResultInfo $DeliverPackageResult
	 * @access public
	 */
	public $DeliverPackageResult = null;
	
	/**
	 *
	 * @param PackageDeliveryResultInfo $DeliverPackageResult        	
	 * @access public
	 */
	public function __construct($DeliverPackageResult) {
		$this->DeliverPackageResult = $DeliverPackageResult;
	}
}
