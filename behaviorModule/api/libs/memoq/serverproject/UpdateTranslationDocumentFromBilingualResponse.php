<?php
class UpdateTranslationDocumentFromBilingualResponse {
	
	/**
	 *
	 * @var TranslationDocImportResultInfo[] $UpdateTranslationDocumentFromBilingualResult
	 * @access public
	 */
	public $UpdateTranslationDocumentFromBilingualResult = null;
	
	/**
	 *
	 * @param TranslationDocImportResultInfo[] $UpdateTranslationDocumentFromBilingualResult        	
	 * @access public
	 */
	public function __construct($UpdateTranslationDocumentFromBilingualResult) {
		$this->UpdateTranslationDocumentFromBilingualResult = $UpdateTranslationDocumentFromBilingualResult;
	}
}
