<?php
include_once ('DocumentHistoryItemInfo.php');
class SubvendorAssignHistoryItemInfo extends DocumentHistoryItemInfo {
	
	/**
	 *
	 * @var dateTime $Deadline
	 * @access public
	 */
	public $Deadline = null;
	
	/**
	 *
	 * @var guid $SubvendorGroupId
	 * @access public
	 */
	public $SubvendorGroupId = null;
	
	/**
	 *
	 * @param guid $DocumentOrDivisionGuid        	
	 * @param DocumentHistoryItemType $HistoryItemType        	
	 * @param guid $ModifierSVGroupId        	
	 * @param guid $ModifierUserGuid        	
	 * @param dateTime $TimeStamp        	
	 * @param dateTime $Deadline        	
	 * @param guid $SubvendorGroupId        	
	 * @access public
	 */
	public function __construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp, $Deadline, $SubvendorGroupId) {
		parent::__construct ( $DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp );
		$this->Deadline = $Deadline;
		$this->SubvendorGroupId = $SubvendorGroupId;
	}
}
