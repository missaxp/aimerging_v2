<?php
class PretranslateCopySourceToTargetConditions {
	const __default = 'SegmentsWithOnlyTagsAndWhitespace';
	const SegmentsWithOnlyTagsAndWhitespace = 'SegmentsWithOnlyTagsAndWhitespace';
	const SegmentsWithOnlyTagsWhitespaceAndPunctuation = 'SegmentsWithOnlyTagsWhitespaceAndPunctuation';
}
