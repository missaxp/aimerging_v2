<?php
class SetProjectTMs2 {
	
	/**
	 *
	 * @var guid $serverProjectGuid
	 * @access public
	 */
	public $serverProjectGuid = null;
	
	/**
	 *
	 * @var ServerProjectTMAssignmentsForTargetLang[] $tmAssignments
	 * @access public
	 */
	public $tmAssignments = null;
	
	/**
	 *
	 * @param guid $serverProjectGuid        	
	 * @param ServerProjectTMAssignmentsForTargetLang[] $tmAssignments        	
	 * @access public
	 */
	public function __construct($serverProjectGuid, $tmAssignments) {
		$this->serverProjectGuid = $serverProjectGuid;
		$this->tmAssignments = $tmAssignments;
	}
}
