<?php
class AddProjectLanguageTBHandlingBehavior {
	const __default = 'AddMissingLanguages';
	const AddMissingLanguages = 'AddMissingLanguages';
	const RemoveTBs = 'RemoveTBs';
	const ThrowFault = 'ThrowFault';
}
