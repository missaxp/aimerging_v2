<?php
class ServerProjectListFilter {
	
	/**
	 *
	 * @var string $Client
	 * @access public
	 */
	public $Client = null;
	
	/**
	 *
	 * @var string $Domain
	 * @access public
	 */
	public $Domain = null;
	
	/**
	 *
	 * @var string $Project
	 * @access public
	 */
	public $Project = null;
	
	/**
	 *
	 * @var string $SourceLanguageCode
	 * @access public
	 */
	public $SourceLanguageCode = null;
	
	/**
	 *
	 * @var string $Subject
	 * @access public
	 */
	public $Subject = null;
	
	/**
	 *
	 * @var string $TargetLanguageCode
	 * @access public
	 */
	public $TargetLanguageCode = null;
	
	/**
	 *
	 * @var dateTime $TimeClosed
	 * @access public
	 */
	public $TimeClosed = null;
	
	/**
	 *
	 * @param dateTime $TimeClosed        	
	 * @access public
	 */
	public function __construct($TimeClosed) {
		$this->TimeClosed = $TimeClosed;
	}
}
