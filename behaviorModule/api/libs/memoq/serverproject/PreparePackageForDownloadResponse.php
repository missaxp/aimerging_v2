<?php
class PreparePackageForDownloadResponse {
	
	/**
	 *
	 * @var PreparePackageResultInfo $PreparePackageForDownloadResult
	 * @access public
	 */
	public $PreparePackageForDownloadResult = null;
	
	/**
	 *
	 * @param PreparePackageResultInfo $PreparePackageForDownloadResult        	
	 * @access public
	 */
	public function __construct($PreparePackageForDownloadResult) {
		$this->PreparePackageForDownloadResult = $PreparePackageForDownloadResult;
	}
}
