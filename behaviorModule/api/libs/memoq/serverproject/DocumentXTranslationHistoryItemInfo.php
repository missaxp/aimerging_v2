<?php
include_once ('DocumentHistoryItemInfo.php');
class DocumentXTranslationHistoryItemInfo extends DocumentHistoryItemInfo {
	
	/**
	 *
	 * @var boolean $MidProjectUpdate
	 * @access public
	 */
	public $MidProjectUpdate = null;
	
	/**
	 *
	 * @param guid $DocumentOrDivisionGuid        	
	 * @param DocumentHistoryItemType $HistoryItemType        	
	 * @param guid $ModifierSVGroupId        	
	 * @param guid $ModifierUserGuid        	
	 * @param dateTime $TimeStamp        	
	 * @param boolean $MidProjectUpdate        	
	 * @access public
	 */
	public function __construct($DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp, $MidProjectUpdate) {
		parent::__construct ( $DocumentOrDivisionGuid, $HistoryItemType, $ModifierSVGroupId, $ModifierUserGuid, $TimeStamp );
		$this->MidProjectUpdate = $MidProjectUpdate;
	}
}
