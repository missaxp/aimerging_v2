<?php
class DeliverDocumentResponse {
	
	/**
	 *
	 * @var WorkflowStatus $DeliverDocumentResult
	 * @access public
	 */
	public $DeliverDocumentResult = null;
	
	/**
	 *
	 * @param WorkflowStatus $DeliverDocumentResult        	
	 * @access public
	 */
	public function __construct($DeliverDocumentResult) {
		$this->DeliverDocumentResult = $DeliverDocumentResult;
	}
}
