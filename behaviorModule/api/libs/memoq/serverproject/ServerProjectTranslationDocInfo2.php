<?php
class ServerProjectTranslationDocInfo2 {
	
	/**
	 *
	 * @var int $ConfirmedCharacterCount
	 * @access public
	 */
	public $ConfirmedCharacterCount = null;
	
	/**
	 *
	 * @var int $ConfirmedSegmentCount
	 * @access public
	 */
	public $ConfirmedSegmentCount = null;
	
	/**
	 *
	 * @var int $ConfirmedWordCount
	 * @access public
	 */
	public $ConfirmedWordCount = null;
	
	/**
	 *
	 * @var guid $DocumentGuid
	 * @access public
	 */
	public $DocumentGuid = null;
	
	/**
	 *
	 * @var string $DocumentName
	 * @access public
	 */
	public $DocumentName = null;
	
	/**
	 *
	 * @var DocumentStatus $DocumentStatus
	 * @access public
	 */
	public $DocumentStatus = null;
	
	/**
	 *
	 * @var string $ExportPath
	 * @access public
	 */
	public $ExportPath = null;
	
	/**
	 *
	 * @var string $ExternalDocumentId
	 * @access public
	 */
	public $ExternalDocumentId = null;
	
	/**
	 *
	 * @var string $ImportPath
	 * @access public
	 */
	public $ImportPath = null;
	
	/**
	 *
	 * @var boolean $IsImage
	 * @access public
	 */
	public $IsImage = null;
	
	/**
	 *
	 * @var int $LockedCharacterCount
	 * @access public
	 */
	public $LockedCharacterCount = null;
	
	/**
	 *
	 * @var int $LockedSegmentCount
	 * @access public
	 */
	public $LockedSegmentCount = null;
	
	/**
	 *
	 * @var int $LockedWordCount
	 * @access public
	 */
	public $LockedWordCount = null;
	
	/**
	 *
	 * @var int $MajorVersion
	 * @access public
	 */
	public $MajorVersion = null;
	
	/**
	 *
	 * @var int $MinorVersion
	 * @access public
	 */
	public $MinorVersion = null;
	
	/**
	 *
	 * @var guid $ParentDocumentId
	 * @access public
	 */
	public $ParentDocumentId = null;
	
	/**
	 *
	 * @var int $ProofreadCharacterCount
	 * @access public
	 */
	public $ProofreadCharacterCount = null;
	
	/**
	 *
	 * @var int $ProofreadSegmentCount
	 * @access public
	 */
	public $ProofreadSegmentCount = null;
	
	/**
	 *
	 * @var int $ProofreadWordCount
	 * @access public
	 */
	public $ProofreadWordCount = null;
	
	/**
	 *
	 * @var int $Reviewer1ConfirmedCharacterCount
	 * @access public
	 */
	public $Reviewer1ConfirmedCharacterCount = null;
	
	/**
	 *
	 * @var int $Reviewer1ConfirmedSegmentCount
	 * @access public
	 */
	public $Reviewer1ConfirmedSegmentCount = null;
	
	/**
	 *
	 * @var int $Reviewer1ConfirmedWordCount
	 * @access public
	 */
	public $Reviewer1ConfirmedWordCount = null;
	
	/**
	 *
	 * @var string $TargetLangCode
	 * @access public
	 */
	public $TargetLangCode = null;
	
	/**
	 *
	 * @var int $TotalCharacterCount
	 * @access public
	 */
	public $TotalCharacterCount = null;
	
	/**
	 *
	 * @var int $TotalSegmentCount
	 * @access public
	 */
	public $TotalSegmentCount = null;
	
	/**
	 *
	 * @var int $TotalWordCount
	 * @access public
	 */
	public $TotalWordCount = null;
	
	/**
	 *
	 * @var TranslationDocumentDetailedAssignmentInfo[] $UserAssignments
	 * @access public
	 */
	public $UserAssignments = null;
	
	/**
	 *
	 * @var string $WebTransUrl
	 * @access public
	 */
	public $WebTransUrl = null;
	
	/**
	 *
	 * @var WorkflowStatus $WorkflowStatus
	 * @access public
	 */
	public $WorkflowStatus = null;
	
	/**
	 *
	 * @param int $ConfirmedCharacterCount        	
	 * @param int $ConfirmedSegmentCount        	
	 * @param int $ConfirmedWordCount        	
	 * @param guid $DocumentGuid        	
	 * @param DocumentStatus $DocumentStatus        	
	 * @param boolean $IsImage        	
	 * @param int $LockedCharacterCount        	
	 * @param int $LockedSegmentCount        	
	 * @param int $LockedWordCount        	
	 * @param int $MajorVersion        	
	 * @param int $MinorVersion        	
	 * @param guid $ParentDocumentId        	
	 * @param int $ProofreadCharacterCount        	
	 * @param int $ProofreadSegmentCount        	
	 * @param int $ProofreadWordCount        	
	 * @param int $Reviewer1ConfirmedCharacterCount        	
	 * @param int $Reviewer1ConfirmedSegmentCount        	
	 * @param int $Reviewer1ConfirmedWordCount        	
	 * @param int $TotalCharacterCount        	
	 * @param int $TotalSegmentCount        	
	 * @param int $TotalWordCount        	
	 * @param WorkflowStatus $WorkflowStatus        	
	 * @access public
	 */
	public function __construct($ConfirmedCharacterCount, $ConfirmedSegmentCount, $ConfirmedWordCount, $DocumentGuid, $DocumentStatus, $IsImage, $LockedCharacterCount, $LockedSegmentCount, $LockedWordCount, $MajorVersion, $MinorVersion, $ParentDocumentId, $ProofreadCharacterCount, $ProofreadSegmentCount, $ProofreadWordCount, $Reviewer1ConfirmedCharacterCount, $Reviewer1ConfirmedSegmentCount, $Reviewer1ConfirmedWordCount, $TotalCharacterCount, $TotalSegmentCount, $TotalWordCount, $WorkflowStatus) {
		$this->ConfirmedCharacterCount = $ConfirmedCharacterCount;
		$this->ConfirmedSegmentCount = $ConfirmedSegmentCount;
		$this->ConfirmedWordCount = $ConfirmedWordCount;
		$this->DocumentGuid = $DocumentGuid;
		$this->DocumentStatus = $DocumentStatus;
		$this->IsImage = $IsImage;
		$this->LockedCharacterCount = $LockedCharacterCount;
		$this->LockedSegmentCount = $LockedSegmentCount;
		$this->LockedWordCount = $LockedWordCount;
		$this->MajorVersion = $MajorVersion;
		$this->MinorVersion = $MinorVersion;
		$this->ParentDocumentId = $ParentDocumentId;
		$this->ProofreadCharacterCount = $ProofreadCharacterCount;
		$this->ProofreadSegmentCount = $ProofreadSegmentCount;
		$this->ProofreadWordCount = $ProofreadWordCount;
		$this->Reviewer1ConfirmedCharacterCount = $Reviewer1ConfirmedCharacterCount;
		$this->Reviewer1ConfirmedSegmentCount = $Reviewer1ConfirmedSegmentCount;
		$this->Reviewer1ConfirmedWordCount = $Reviewer1ConfirmedWordCount;
		$this->TotalCharacterCount = $TotalCharacterCount;
		$this->TotalSegmentCount = $TotalSegmentCount;
		$this->TotalWordCount = $TotalWordCount;
		$this->WorkflowStatus = $WorkflowStatus;
	}
}
