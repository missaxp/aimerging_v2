<?php
class FirstAcceptUserDecision {
	const __default = 'NotDecided';
	const NotDecided = 'NotDecided';
	const Rejected = 'Rejected';
	const Accepted = 'Accepted';
}
