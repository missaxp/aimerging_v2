<?php
class FirstAcceptStatus {
	const __default = 'NotStarted';
	const NotStarted = 'NotStarted';
	const Pending = 'Pending';
	const DocTaken = 'DocTaken';
	const Failed = 'Failed';
	const TimedOut = 'TimedOut';
}
