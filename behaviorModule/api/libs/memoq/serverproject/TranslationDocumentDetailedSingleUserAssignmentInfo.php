<?php
include_once ('TranslationDocumentDetailedRoleAssignmentInfo.php');
class TranslationDocumentDetailedSingleUserAssignmentInfo extends TranslationDocumentDetailedRoleAssignmentInfo {
	
	/**
	 *
	 * @var TranslationDocumentAssigneeInfo $User
	 * @access public
	 */
	public $User = null;
	
	/**
	 *
	 * @param int $RoleId        	
	 * @access public
	 */
	public function __construct($RoleId) {
		parent::__construct ( $RoleId );
	}
}
