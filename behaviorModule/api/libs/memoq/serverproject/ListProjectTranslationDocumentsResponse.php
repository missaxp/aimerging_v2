<?php
class ListProjectTranslationDocumentsResponse {
	
	/**
	 *
	 * @var ServerProjectTranslationDocInfo[] $ListProjectTranslationDocumentsResult
	 * @access public
	 */
	public $ListProjectTranslationDocumentsResult = null;
	
	/**
	 *
	 * @param ServerProjectTranslationDocInfo[] $ListProjectTranslationDocumentsResult        	
	 * @access public
	 */
	public function __construct($ListProjectTranslationDocumentsResult) {
		$this->ListProjectTranslationDocumentsResult = $ListProjectTranslationDocumentsResult;
	}
}
