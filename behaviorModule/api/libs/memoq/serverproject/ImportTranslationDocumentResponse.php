<?php
class ImportTranslationDocumentResponse {
	
	/**
	 *
	 * @var TranslationDocImportResultInfo $ImportTranslationDocumentResult
	 * @access public
	 */
	public $ImportTranslationDocumentResult = null;
	
	/**
	 *
	 * @param TranslationDocImportResultInfo $ImportTranslationDocumentResult        	
	 * @access public
	 */
	public function __construct($ImportTranslationDocumentResult) {
		$this->ImportTranslationDocumentResult = $ImportTranslationDocumentResult;
	}
}
