<?php
include_once ('TranslationDocumentRoleAssignmentInfo.php');
class TranslationDocumentSingleUserAssignmentInfo extends TranslationDocumentRoleAssignmentInfo {
	
	/**
	 *
	 * @var guid $UserGuid
	 * @access public
	 */
	public $UserGuid = null;
	
	/**
	 *
	 * @param int $RoleId        	
	 * @param guid $UserGuid        	
	 * @access public
	 */
	public function __construct($RoleId, $UserGuid) {
		parent::__construct ( $RoleId );
		$this->UserGuid = $UserGuid;
	}
}
