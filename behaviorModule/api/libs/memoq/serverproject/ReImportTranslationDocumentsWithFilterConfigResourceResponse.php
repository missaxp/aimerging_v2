<?php
class ReImportTranslationDocumentsWithFilterConfigResourceResponse {
	
	/**
	 *
	 * @var TranslationDocImportResultInfo[] $ReImportTranslationDocumentsWithFilterConfigResourceResult
	 * @access public
	 */
	public $ReImportTranslationDocumentsWithFilterConfigResourceResult = null;
	
	/**
	 *
	 * @param TranslationDocImportResultInfo[] $ReImportTranslationDocumentsWithFilterConfigResourceResult        	
	 * @access public
	 */
	public function __construct($ReImportTranslationDocumentsWithFilterConfigResourceResult) {
		$this->ReImportTranslationDocumentsWithFilterConfigResourceResult = $ReImportTranslationDocumentsWithFilterConfigResourceResult;
	}
}
