<?php
class DocDeliveryResultInfo {
	
	/**
	 *
	 * @var DocDeliveryResult $DeliveryResult
	 * @access public
	 */
	public $DeliveryResult = null;
	
	/**
	 *
	 * @var guid $DocumentGuid
	 * @access public
	 */
	public $DocumentGuid = null;
	
	/**
	 *
	 * @var string $DocumentName
	 * @access public
	 */
	public $DocumentName = null;
	
	/**
	 *
	 * @var string $TargetLang
	 * @access public
	 */
	public $TargetLang = null;
	
	/**
	 *
	 * @param DocDeliveryResult $DeliveryResult        	
	 * @param guid $DocumentGuid        	
	 * @access public
	 */
	public function __construct($DeliveryResult, $DocumentGuid) {
		$this->DeliveryResult = $DeliveryResult;
		$this->DocumentGuid = $DocumentGuid;
	}
}
