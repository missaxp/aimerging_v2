<?php
class RunQAGetReportResponse {
	
	/**
	 *
	 * @var QAReport $RunQAGetReportResult
	 * @access public
	 */
	public $RunQAGetReportResult = null;
	
	/**
	 *
	 * @param QAReport $RunQAGetReportResult        	
	 * @access public
	 */
	public function __construct($RunQAGetReportResult) {
		$this->RunQAGetReportResult = $RunQAGetReportResult;
	}
}
