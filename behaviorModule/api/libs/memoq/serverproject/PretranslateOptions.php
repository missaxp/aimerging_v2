<?php
class PretranslateOptions {
	
	/**
	 *
	 * @var PretranslateStateToConfirmAndLock $ConfirmLockPretranslated
	 * @access public
	 */
	public $ConfirmLockPretranslated = null;
	
	/**
	 *
	 * @var boolean $ConfirmLockUnambiguousMatchesOnly
	 * @access public
	 */
	public $ConfirmLockUnambiguousMatchesOnly = null;
	
	/**
	 *
	 * @var PretranslateCopySourceToTargetBehavior $CopySourceToTarget
	 * @access public
	 */
	public $CopySourceToTarget = null;
	
	/**
	 *
	 * @var PretranslateExpectedFinalTranslationState $FinalTranslationState
	 * @access public
	 */
	public $FinalTranslationState = null;
	
	/**
	 *
	 * @var int $GoodMatchRate
	 * @access public
	 */
	public $GoodMatchRate = null;
	
	/**
	 *
	 * @var boolean $LockPretranslated
	 * @access public
	 */
	public $LockPretranslated = null;
	
	/**
	 *
	 * @var boolean $OnlyUnambiguousMatches
	 * @access public
	 */
	public $OnlyUnambiguousMatches = null;
	
	/**
	 *
	 * @var PretranslateLookupBehavior $PretranslateLookupBehavior
	 * @access public
	 */
	public $PretranslateLookupBehavior = null;
	
	/**
	 *
	 * @var boolean $UseMT
	 * @access public
	 */
	public $UseMT = null;
	
	/**
	 *
	 * @param PretranslateStateToConfirmAndLock $ConfirmLockPretranslated        	
	 * @param boolean $ConfirmLockUnambiguousMatchesOnly        	
	 * @param PretranslateExpectedFinalTranslationState $FinalTranslationState        	
	 * @param int $GoodMatchRate        	
	 * @param boolean $LockPretranslated        	
	 * @param boolean $OnlyUnambiguousMatches        	
	 * @param PretranslateLookupBehavior $PretranslateLookupBehavior        	
	 * @param boolean $UseMT        	
	 * @access public
	 */
	public function __construct($ConfirmLockPretranslated, $ConfirmLockUnambiguousMatchesOnly, $FinalTranslationState, $GoodMatchRate, $LockPretranslated, $OnlyUnambiguousMatches, $PretranslateLookupBehavior, $UseMT) {
		$this->ConfirmLockPretranslated = $ConfirmLockPretranslated;
		$this->ConfirmLockUnambiguousMatchesOnly = $ConfirmLockUnambiguousMatchesOnly;
		$this->FinalTranslationState = $FinalTranslationState;
		$this->GoodMatchRate = $GoodMatchRate;
		$this->LockPretranslated = $LockPretranslated;
		$this->OnlyUnambiguousMatches = $OnlyUnambiguousMatches;
		$this->PretranslateLookupBehavior = $PretranslateLookupBehavior;
		$this->UseMT = $UseMT;
	}
}
