<?php
include_once ('TranslationDocumentDetailedRoleAssignmentInfo.php');
class TranslationDocumentDetailedGroupSourcingAssignmentInfo extends TranslationDocumentDetailedRoleAssignmentInfo {
	
	/**
	 *
	 * @var TranslationDocumentGroupSourcingUserInfo[] $Users
	 * @access public
	 */
	public $Users = null;
	
	/**
	 *
	 * @param int $RoleId        	
	 * @access public
	 */
	public function __construct($RoleId) {
		parent::__construct ( $RoleId );
	}
}
