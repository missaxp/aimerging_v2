<?php
class PackageDeliveryResultInfo {
	
	/**
	 *
	 * @var PackageDeliveryResult $DeliveryResult
	 * @access public
	 */
	public $DeliveryResult = null;
	
	/**
	 *
	 * @var DocDeliveryResultInfo[] $DocumentResults
	 * @access public
	 */
	public $DocumentResults = null;
	
	/**
	 *
	 * @param PackageDeliveryResult $DeliveryResult        	
	 * @access public
	 */
	public function __construct($DeliveryResult) {
		$this->DeliveryResult = $DeliveryResult;
	}
}
