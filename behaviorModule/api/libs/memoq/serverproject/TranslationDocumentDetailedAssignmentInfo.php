<?php
class TranslationDocumentDetailedAssignmentInfo {
	
	/**
	 *
	 * @var TranslationDocumentAssignmentType $AssignmentType
	 * @access public
	 */
	public $AssignmentType = null;
	
	/**
	 *
	 * @var dateTime $Deadline
	 * @access public
	 */
	public $Deadline = null;
	
	/**
	 *
	 * @param TranslationDocumentAssignmentType $AssignmentType        	
	 * @param dateTime $Deadline        	
	 * @access public
	 */
	public function __construct($AssignmentType, $Deadline) {
		$this->AssignmentType = $AssignmentType;
		$this->Deadline = $Deadline;
	}
}
