<?php
class GetProject {
	
	/**
	 *
	 * @var guid $spGuid
	 * @access public
	 */
	public $spGuid = null;
	
	/**
	 *
	 * @param guid $spGuid        	
	 * @access public
	 */
	public function __construct($spGuid) {
		$this->spGuid = $spGuid;
	}
}
