<?php
class AsiaOnlineBeginTranslation {
	
	/**
	 *
	 * @var guid $serverProjectGuid
	 * @access public
	 */
	public $serverProjectGuid = null;
	
	/**
	 *
	 * @var guid[] $documentGuids
	 * @access public
	 */
	public $documentGuids = null;
	
	/**
	 *
	 * @var AsiaOnlineTranslateOptions $options
	 * @access public
	 */
	public $options = null;
	
	/**
	 *
	 * @param guid $serverProjectGuid        	
	 * @param guid[] $documentGuids        	
	 * @param AsiaOnlineTranslateOptions $options        	
	 * @access public
	 */
	public function __construct($serverProjectGuid, $documentGuids, $options) {
		$this->serverProjectGuid = $serverProjectGuid;
		$this->documentGuids = $documentGuids;
		$this->options = $options;
	}
}
