<?php
class CreateAndPublish {
	
	/**
	 *
	 * @var TMInfo $info
	 * @access public
	 */
	public $info = null;
	
	/**
	 *
	 * @param TMInfo $info        	
	 * @access public
	 */
	public function __construct($info) {
		$this->info = $info;
	}
}
