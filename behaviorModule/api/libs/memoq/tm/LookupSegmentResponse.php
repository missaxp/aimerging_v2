<?php
class LookupSegmentResponse {
	
	/**
	 *
	 * @var string $LookupSegmentResult
	 * @access public
	 */
	public $LookupSegmentResult = null;
	
	/**
	 *
	 * @param string $LookupSegmentResult        	
	 * @access public
	 */
	public function __construct($LookupSegmentResult) {
		$this->LookupSegmentResult = $LookupSegmentResult;
	}
}
