<?php
class EndChunkedTMXImportResponse {
	
	/**
	 *
	 * @var TmxImportResult $EndChunkedTMXImportResult
	 * @access public
	 */
	public $EndChunkedTMXImportResult = null;
	
	/**
	 *
	 * @param TmxImportResult $EndChunkedTMXImportResult        	
	 * @access public
	 */
	public function __construct($EndChunkedTMXImportResult) {
		$this->EndChunkedTMXImportResult = $EndChunkedTMXImportResult;
	}
}
