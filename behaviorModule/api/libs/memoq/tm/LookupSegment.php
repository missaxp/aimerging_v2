<?php
class LookupSegment {
	
	/**
	 *
	 * @var string $sessionId
	 * @access public
	 */
	public $sessionId = null;
	
	/**
	 *
	 * @var string $lookupDataXml
	 * @access public
	 */
	public $lookupDataXml = null;
	
	/**
	 *
	 * @var guid $tmGuid
	 * @access public
	 */
	public $tmGuid = null;
	
	/**
	 *
	 * @var LookupSegmentRequest $options
	 * @access public
	 */
	public $options = null;
	
	/**
	 *
	 * @param string $sessionId        	
	 * @param string $lookupDataXml        	
	 * @param guid $tmGuid        	
	 * @param LookupSegmentRequest $options        	
	 * @access public
	 */
	public function __construct($sessionId, $lookupDataXml, $tmGuid, $options) {
		$this->sessionId = $sessionId;
		$this->lookupDataXml = $lookupDataXml;
		$this->tmGuid = $tmGuid;
		$this->options = $options;
	}
}
