<?php
class ListTMsResponse {
	
	/**
	 *
	 * @var TMInfo[] $ListTMsResult
	 * @access public
	 */
	public $ListTMsResult = null;
	
	/**
	 *
	 * @param TMInfo[] $ListTMsResult        	
	 * @access public
	 */
	public function __construct($ListTMsResult) {
		$this->ListTMsResult = $ListTMsResult;
	}
}
