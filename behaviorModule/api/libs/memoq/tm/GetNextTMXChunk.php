<?php
class GetNextTMXChunk {
	
	/**
	 *
	 * @var guid $sessionId
	 * @access public
	 */
	public $sessionId = null;
	
	/**
	 *
	 * @param guid $sessionId        	
	 * @access public
	 */
	public function __construct($sessionId) {
		$this->sessionId = $sessionId;
	}
}
