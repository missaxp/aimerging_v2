<?php
class LookupSegmentRequest {
	
	/**
	 *
	 * @var boolean $AdjustFuzzyMatches
	 * @access public
	 */
	public $AdjustFuzzyMatches = null;
	
	/**
	 *
	 * @var InlineTagStrictness $InlineTagStrictness
	 * @access public
	 */
	public $InlineTagStrictness = null;
	
	/**
	 *
	 * @var int $MatchThreshold
	 * @access public
	 */
	public $MatchThreshold = null;
	
	/**
	 *
	 * @var boolean $OnlyBest
	 * @access public
	 */
	public $OnlyBest = null;
	
	/**
	 *
	 * @var boolean $OnlyUnambiguous
	 * @access public
	 */
	public $OnlyUnambiguous = null;
	
	/**
	 *
	 * @var boolean $ReverseLookup
	 * @access public
	 */
	public $ReverseLookup = null;
	
	/**
	 *
	 * @param boolean $AdjustFuzzyMatches        	
	 * @param InlineTagStrictness $InlineTagStrictness        	
	 * @param int $MatchThreshold        	
	 * @param boolean $OnlyBest        	
	 * @param boolean $OnlyUnambiguous        	
	 * @param boolean $ReverseLookup        	
	 * @access public
	 */
	public function __construct($AdjustFuzzyMatches, $InlineTagStrictness, $MatchThreshold, $OnlyBest, $OnlyUnambiguous, $ReverseLookup) {
		$this->AdjustFuzzyMatches = $AdjustFuzzyMatches;
		$this->InlineTagStrictness = $InlineTagStrictness;
		$this->MatchThreshold = $MatchThreshold;
		$this->OnlyBest = $OnlyBest;
		$this->OnlyUnambiguous = $OnlyUnambiguous;
		$this->ReverseLookup = $ReverseLookup;
	}
}
