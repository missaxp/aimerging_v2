<?php
if (! defined ( 'ROOT_PATH' )) {
	define ( 'ROOT_PATH', realpath ( __DIR__ . '/../' ) );
}

include_once ('ListTMs.php');
include_once ('ListTMsResponse.php');
include_once (ROOT_PATH . '/common/TMInfo.php');
include_once (ROOT_PATH . '/common/HeavyResourceInfo.php');
include_once (ROOT_PATH . '/common/ResourceInfo.php');
include_once (ROOT_PATH . '/common/UnexpectedFault.php');
include_once (ROOT_PATH . '/common/GenericFault.php');
include_once ('BeginChunkedTMXImport.php');
include_once ('BeginChunkedTMXImportResponse.php');
include_once ('AddNextTMXChunk.php');
include_once ('AddNextTMXChunkResponse.php');
include_once ('EndChunkedTMXImport.php');
include_once ('EndChunkedTMXImportResponse.php');
include_once ('TmxImportResult.php');
include_once ('BeginChunkedTMXExport.php');
include_once ('BeginChunkedTMXExportResponse.php');
include_once ('GetNextTMXChunk.php');
include_once ('GetNextTMXChunkResponse.php');
include_once ('EndChunkedTMXExport.php');
include_once ('EndChunkedTMXExportResponse.php');
include_once ('CreateAndPublish.php');
include_once ('CreateAndPublishResponse.php');
include_once ('DeleteTM.php');
include_once ('DeleteTMResponse.php');
include_once ('LookupSegment.php');
include_once ('LookupSegmentRequest.php');
include_once ('InlineTagStrictness.php');
include_once ('LookupSegmentResponse.php');
include_once ('TMFault.php');
include_once ('RequestXmlFormatFault.php');
include_once ('InvalidSessionIdFault.php');
include_once ('UnauthorizedAccessFault.php');
include_once ('NoLicenseFault.php');
include_once ('Concordance.php');
include_once ('ConcordanceRequest.php');
include_once ('ConcordanceResponse.php');
include_once ('AddOrUpdateEntry.php');
include_once ('AddOrUpdateEntryResponse.php');

/**
 */
class TMService extends \SoapClient {
	
	/**
	 *
	 * @var array $classmap The defined classes
	 * @access private
	 */
	private static $classmap = array (
			'ListTMs' => '\ListTMs',
			'ListTMsResponse' => '\ListTMsResponse',
			'TMInfo' => '\TMInfo',
			'HeavyResourceInfo' => '\HeavyResourceInfo',
			'ResourceInfo' => '\ResourceInfo',
			'UnexpectedFault' => '\UnexpectedFault',
			'GenericFault' => '\GenericFault',
			'BeginChunkedTMXImport' => '\BeginChunkedTMXImport',
			'BeginChunkedTMXImportResponse' => '\BeginChunkedTMXImportResponse',
			'AddNextTMXChunk' => '\AddNextTMXChunk',
			'AddNextTMXChunkResponse' => '\AddNextTMXChunkResponse',
			'EndChunkedTMXImport' => '\EndChunkedTMXImport',
			'EndChunkedTMXImportResponse' => '\EndChunkedTMXImportResponse',
			'TmxImportResult' => '\TmxImportResult',
			'BeginChunkedTMXExport' => '\BeginChunkedTMXExport',
			'BeginChunkedTMXExportResponse' => '\BeginChunkedTMXExportResponse',
			'GetNextTMXChunk' => '\GetNextTMXChunk',
			'GetNextTMXChunkResponse' => '\GetNextTMXChunkResponse',
			'EndChunkedTMXExport' => '\EndChunkedTMXExport',
			'EndChunkedTMXExportResponse' => '\EndChunkedTMXExportResponse',
			'CreateAndPublish' => '\CreateAndPublish',
			'CreateAndPublishResponse' => '\CreateAndPublishResponse',
			'DeleteTM' => '\DeleteTM',
			'DeleteTMResponse' => '\DeleteTMResponse',
			'LookupSegment' => '\LookupSegment',
			'LookupSegmentRequest' => '\LookupSegmentRequest',
			'LookupSegmentResponse' => '\LookupSegmentResponse',
			'TMFault' => '\TMFault',
			'RequestXmlFormatFault' => '\RequestXmlFormatFault',
			'InvalidSessionIdFault' => '\InvalidSessionIdFault',
			'UnauthorizedAccessFault' => '\UnauthorizedAccessFault',
			'NoLicenseFault' => '\NoLicenseFault',
			'Concordance' => '\Concordance',
			'ConcordanceRequest' => '\ConcordanceRequest',
			'ConcordanceResponse' => '\ConcordanceResponse',
			'AddOrUpdateEntry' => '\AddOrUpdateEntry',
			'AddOrUpdateEntryResponse' => '\AddOrUpdateEntryResponse' 
	);
	
	/**
	 *
	 * @param array $options
	 *        	A array of config values
	 * @param string $wsdl
	 *        	The wsdl file to use
	 * @access public
	 */
	public function __construct(array $options = array(), $wsdl = 'memoq/tm.wsdl') {
		foreach ( self::$classmap as $key => $value ) {
			if (! isset ( $options ['classmap'] [$key] )) {
				$options ['classmap'] [$key] = $value;
			}
		}
		
		parent::__construct ( $wsdl, $options );
	}
	
	/**
	 *
	 * @param ListTMs $parameters        	
	 * @access public
	 * @return ListTMsResponse
	 */
	public function ListTMs(ListTMs $parameters) {
		return $this->__soapCall ( 'ListTMs', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param BeginChunkedTMXImport $parameters        	
	 * @access public
	 * @return BeginChunkedTMXImportResponse
	 */
	public function BeginChunkedTMXImport(BeginChunkedTMXImport $parameters) {
		return $this->__soapCall ( 'BeginChunkedTMXImport', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param AddNextTMXChunk $parameters        	
	 * @access public
	 * @return AddNextTMXChunkResponse
	 */
	public function AddNextTMXChunk(AddNextTMXChunk $parameters) {
		return $this->__soapCall ( 'AddNextTMXChunk', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param EndChunkedTMXImport $parameters        	
	 * @access public
	 * @return EndChunkedTMXImportResponse
	 */
	public function EndChunkedTMXImport(EndChunkedTMXImport $parameters) {
		return $this->__soapCall ( 'EndChunkedTMXImport', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param BeginChunkedTMXExport $parameters        	
	 * @access public
	 * @return BeginChunkedTMXExportResponse
	 */
	public function BeginChunkedTMXExport(BeginChunkedTMXExport $parameters) {
		return $this->__soapCall ( 'BeginChunkedTMXExport', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param GetNextTMXChunk $parameters        	
	 * @access public
	 * @return GetNextTMXChunkResponse
	 */
	public function GetNextTMXChunk(GetNextTMXChunk $parameters) {
		return $this->__soapCall ( 'GetNextTMXChunk', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param EndChunkedTMXExport $parameters        	
	 * @access public
	 * @return EndChunkedTMXExportResponse
	 */
	public function EndChunkedTMXExport(EndChunkedTMXExport $parameters) {
		return $this->__soapCall ( 'EndChunkedTMXExport', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param CreateAndPublish $parameters        	
	 * @access public
	 * @return CreateAndPublishResponse
	 */
	public function CreateAndPublish(CreateAndPublish $parameters) {
		return $this->__soapCall ( 'CreateAndPublish', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param DeleteTM $parameters        	
	 * @access public
	 * @return DeleteTMResponse
	 */
	public function DeleteTM(DeleteTM $parameters) {
		return $this->__soapCall ( 'DeleteTM', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param LookupSegment $parameters        	
	 * @access public
	 * @return LookupSegmentResponse
	 */
	public function LookupSegment(LookupSegment $parameters) {
		return $this->__soapCall ( 'LookupSegment', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param Concordance $parameters        	
	 * @access public
	 * @return ConcordanceResponse
	 */
	public function Concordance(Concordance $parameters) {
		return $this->__soapCall ( 'Concordance', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param AddOrUpdateEntry $parameters        	
	 * @access public
	 * @return AddOrUpdateEntryResponse
	 */
	public function AddOrUpdateEntry(AddOrUpdateEntry $parameters) {
		return $this->__soapCall ( 'AddOrUpdateEntry', array (
				$parameters 
		) );
	}
}
