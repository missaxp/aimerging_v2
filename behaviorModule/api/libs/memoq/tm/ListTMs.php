<?php
class ListTMs {
	
	/**
	 *
	 * @var string $srcLang
	 * @access public
	 */
	public $srcLang = null;
	
	/**
	 *
	 * @var string $targetLang
	 * @access public
	 */
	public $targetLang = null;
	
	/**
	 *
	 * @param string $srcLang        	
	 * @param string $targetLang        	
	 * @access public
	 */
	public function __construct($srcLang, $targetLang) {
		$this->srcLang = $srcLang;
		$this->targetLang = $targetLang;
	}
}
