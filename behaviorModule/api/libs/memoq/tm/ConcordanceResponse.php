<?php
class ConcordanceResponse {
	
	/**
	 *
	 * @var string $ConcordanceResult
	 * @access public
	 */
	public $ConcordanceResult = null;
	
	/**
	 *
	 * @param string $ConcordanceResult        	
	 * @access public
	 */
	public function __construct($ConcordanceResult) {
		$this->ConcordanceResult = $ConcordanceResult;
	}
}
