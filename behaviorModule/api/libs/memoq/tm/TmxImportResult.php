<?php
class TmxImportResult {
	
	/**
	 *
	 * @var int $AllSegmentCount
	 * @access public
	 */
	public $AllSegmentCount = null;
	
	/**
	 *
	 * @var int $ImportedSegmentCount
	 * @access public
	 */
	public $ImportedSegmentCount = null;
	
	/**
	 *
	 * @param int $AllSegmentCount        	
	 * @param int $ImportedSegmentCount        	
	 * @access public
	 */
	public function __construct($AllSegmentCount, $ImportedSegmentCount) {
		$this->AllSegmentCount = $AllSegmentCount;
		$this->ImportedSegmentCount = $ImportedSegmentCount;
	}
}
