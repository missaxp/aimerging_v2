<?php
class AddNextTMXChunk {
	
	/**
	 *
	 * @var guid $sessionId
	 * @access public
	 */
	public $sessionId = null;
	
	/**
	 *
	 * @var base64Binary $tmxData
	 * @access public
	 */
	public $tmxData = null;
	
	/**
	 *
	 * @param guid $sessionId        	
	 * @param base64Binary $tmxData        	
	 * @access public
	 */
	public function __construct($sessionId, $tmxData) {
		$this->sessionId = $sessionId;
		$this->tmxData = $tmxData;
	}
}
