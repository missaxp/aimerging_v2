<?php
class EndChunkedFileUpload {
	
	/**
	 *
	 * @var guid $fileIdAndSessionId
	 * @access public
	 */
	public $fileIdAndSessionId = null;
	
	/**
	 *
	 * @param guid $fileIdAndSessionId        	
	 * @access public
	 */
	public function __construct($fileIdAndSessionId) {
		$this->fileIdAndSessionId = $fileIdAndSessionId;
	}
}
