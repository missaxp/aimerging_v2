<?php
class AddNextFileChunk {
	
	/**
	 *
	 * @var guid $fileIdAndSessionId
	 * @access public
	 */
	public $fileIdAndSessionId = null;
	
	/**
	 *
	 * @var base64Binary $fileData
	 * @access public
	 */
	public $fileData = null;
	
	/**
	 *
	 * @param guid $fileIdAndSessionId        	
	 * @param base64Binary $fileData        	
	 * @access public
	 */
	public function __construct($fileIdAndSessionId, $fileData) {
		$this->fileIdAndSessionId = $fileIdAndSessionId;
		$this->fileData = $fileData;
	}
}
