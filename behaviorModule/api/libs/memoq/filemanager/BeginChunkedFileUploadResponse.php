<?php
class BeginChunkedFileUploadResponse {
	
	/**
	 *
	 * @var guid $BeginChunkedFileUploadResult
	 * @access public
	 */
	public $BeginChunkedFileUploadResult = null;
	
	/**
	 *
	 * @param guid $BeginChunkedFileUploadResult        	
	 * @access public
	 */
	public function __construct($BeginChunkedFileUploadResult) {
		$this->BeginChunkedFileUploadResult = $BeginChunkedFileUploadResult;
	}
}
