<?php
include_once ('BeginChunkedFileUpload.php');
include_once ('BeginChunkedFileUploadResponse.php');
include_once (ROOT_PATH . '/common/GenericFault.php');
include_once (ROOT_PATH . '/common/UnexpectedFault.php');
include_once ('AddNextFileChunk.php');
include_once ('AddNextFileChunkResponse.php');
include_once ('EndChunkedFileUpload.php');
include_once ('EndChunkedFileUploadResponse.php');
include_once ('BeginChunkedFileDownload.php');
include_once ('BeginChunkedFileDownloadResponse.php');
include_once ('GetNextFileChunk.php');
include_once ('GetNextFileChunkResponse.php');
include_once ('EndChunkedFileDownload.php');
include_once ('EndChunkedFileDownloadResponse.php');
include_once ('DeleteFile.php');
include_once ('DeleteFileResponse.php');

/**
 */
class FileManagerService extends \SoapClient {
	
	/**
	 *
	 * @var array $classmap The defined classes
	 * @access private
	 */
	private static $classmap = array (
			'BeginChunkedFileUpload' => '\BeginChunkedFileUpload',
			'BeginChunkedFileUploadResponse' => '\BeginChunkedFileUploadResponse',
			'GenericFault' => '\GenericFault',
			'UnexpectedFault' => '\UnexpectedFault',
			'AddNextFileChunk' => '\AddNextFileChunk',
			'AddNextFileChunkResponse' => '\AddNextFileChunkResponse',
			'EndChunkedFileUpload' => '\EndChunkedFileUpload',
			'EndChunkedFileUploadResponse' => '\EndChunkedFileUploadResponse',
			'BeginChunkedFileDownload' => '\BeginChunkedFileDownload',
			'BeginChunkedFileDownloadResponse' => '\BeginChunkedFileDownloadResponse',
			'GetNextFileChunk' => '\GetNextFileChunk',
			'GetNextFileChunkResponse' => '\GetNextFileChunkResponse',
			'EndChunkedFileDownload' => '\EndChunkedFileDownload',
			'EndChunkedFileDownloadResponse' => '\EndChunkedFileDownloadResponse',
			'DeleteFile' => '\DeleteFile',
			'DeleteFileResponse' => '\DeleteFileResponse' 
	);
	
	/**
	 *
	 * @param array $options
	 *        	A array of config values
	 * @param string $wsdl
	 *        	The wsdl file to use
	 * @access public
	 */
	public function __construct(array $options = array(), $wsdl = 'memoq/filemanager.wsdl') {
		foreach ( self::$classmap as $key => $value ) {
			if (! isset ( $options ['classmap'] [$key] )) {
				$options ['classmap'] [$key] = $value;
			}
		}
		
		parent::__construct ( $wsdl, $options );
	}
	
	/**
	 *
	 * @param BeginChunkedFileUpload $parameters        	
	 * @access public
	 * @return BeginChunkedFileUploadResponse
	 */
	public function BeginChunkedFileUpload(BeginChunkedFileUpload $parameters) {
		return $this->__soapCall ( 'BeginChunkedFileUpload', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param AddNextFileChunk $parameters        	
	 * @access public
	 * @return AddNextFileChunkResponse
	 */
	public function AddNextFileChunk(AddNextFileChunk $parameters) {
		return $this->__soapCall ( 'AddNextFileChunk', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param EndChunkedFileUpload $parameters        	
	 * @access public
	 * @return EndChunkedFileUploadResponse
	 */
	public function EndChunkedFileUpload(EndChunkedFileUpload $parameters) {
		return $this->__soapCall ( 'EndChunkedFileUpload', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param BeginChunkedFileDownload $parameters        	
	 * @access public
	 * @return BeginChunkedFileDownloadResponse
	 */
	public function BeginChunkedFileDownload(BeginChunkedFileDownload $parameters) {
		return $this->__soapCall ( 'BeginChunkedFileDownload', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param GetNextFileChunk $parameters        	
	 * @access public
	 * @return GetNextFileChunkResponse
	 */
	public function GetNextFileChunk(GetNextFileChunk $parameters) {
		return $this->__soapCall ( 'GetNextFileChunk', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param EndChunkedFileDownload $parameters        	
	 * @access public
	 * @return EndChunkedFileDownloadResponse
	 */
	public function EndChunkedFileDownload(EndChunkedFileDownload $parameters) {
		return $this->__soapCall ( 'EndChunkedFileDownload', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param DeleteFile $parameters        	
	 * @access public
	 * @return DeleteFileResponse
	 */
	public function DeleteFile(DeleteFile $parameters) {
		return $this->__soapCall ( 'DeleteFile', array (
				$parameters 
		) );
	}
}
