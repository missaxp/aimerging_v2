<?php
class BeginChunkedFileDownloadResponse {
	
	/**
	 *
	 * @var guid $BeginChunkedFileDownloadResult
	 * @access public
	 */
	public $BeginChunkedFileDownloadResult = null;
	
	/**
	 *
	 * @var string $fileName
	 * @access public
	 */
	public $fileName = null;
	
	/**
	 *
	 * @var int $fileSize
	 * @access public
	 */
	public $fileSize = null;
	
	/**
	 *
	 * @param guid $BeginChunkedFileDownloadResult        	
	 * @param string $fileName        	
	 * @param int $fileSize        	
	 * @access public
	 */
	public function __construct($BeginChunkedFileDownloadResult, $fileName, $fileSize) {
		$this->BeginChunkedFileDownloadResult = $BeginChunkedFileDownloadResult;
		$this->fileName = $fileName;
		$this->fileSize = $fileSize;
	}
}
