<?php
class GetNextFileChunk {
	
	/**
	 *
	 * @var guid $sessionId
	 * @access public
	 */
	public $sessionId = null;
	
	/**
	 *
	 * @var int $byteCount
	 * @access public
	 */
	public $byteCount = null;
	
	/**
	 *
	 * @param guid $sessionId        	
	 * @param int $byteCount        	
	 * @access public
	 */
	public function __construct($sessionId, $byteCount) {
		$this->sessionId = $sessionId;
		$this->byteCount = $byteCount;
	}
}
