<?php
class GetGroupResponse {
	
	/**
	 *
	 * @var GroupInfo $GetGroupResult
	 * @access public
	 */
	public $GetGroupResult = null;
	
	/**
	 *
	 * @param GroupInfo $GetGroupResult        	
	 * @access public
	 */
	public function __construct($GetGroupResult) {
		$this->GetGroupResult = $GetGroupResult;
	}
}
