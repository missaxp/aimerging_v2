<?php
class ListObjectPermissions {
	
	/**
	 *
	 * @var guid $objectGuid
	 * @access public
	 */
	public $objectGuid = null;
	
	/**
	 *
	 * @param guid $objectGuid        	
	 * @access public
	 */
	public function __construct($objectGuid) {
		$this->objectGuid = $objectGuid;
	}
}
