<?php
class UpdateGroup {
	
	/**
	 *
	 * @var GroupInfo $groupInfo
	 * @access public
	 */
	public $groupInfo = null;
	
	/**
	 *
	 * @param GroupInfo $groupInfo        	
	 * @access public
	 */
	public function __construct($groupInfo) {
		$this->groupInfo = $groupInfo;
	}
}
