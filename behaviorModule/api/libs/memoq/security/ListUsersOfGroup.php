<?php
class ListUsersOfGroup {
	
	/**
	 *
	 * @var guid $groupGuid
	 * @access public
	 */
	public $groupGuid = null;
	
	/**
	 *
	 * @param guid $groupGuid        	
	 * @access public
	 */
	public function __construct($groupGuid) {
		$this->groupGuid = $groupGuid;
	}
}
