<?php
if (! defined ( 'ROOT_PATH' )) {
	define ( 'ROOT_PATH', realpath ( __DIR__ . '/../' ) );
}

include_once (ROOT_PATH . '/common/UserPackageWorkflowType.php');
include_once ('ListUsers.php');
include_once ('ListUsersResponse.php');
include_once (ROOT_PATH . '/common/UserInfo.php');
include_once (ROOT_PATH . '/common/GenericFault.php');
include_once (ROOT_PATH . '/common/UnexpectedFault.php');
include_once ('GetUser.php');
include_once ('GetUserResponse.php');
include_once ('CreateUser.php');
include_once ('CreateUserResponse.php');
include_once ('UpdateUser.php');
include_once ('UpdateUserResponse.php');
include_once ('DeleteUser.php');
include_once ('DeleteUserResponse.php');
include_once ('ListGroupsOfUser.php');
include_once ('ListGroupsOfUserResponse.php');
include_once ('GroupInfo.php');
include_once ('SetGroupsOfUser.php');
include_once ('SetGroupsOfUserResponse.php');
include_once ('ListGroups.php');
include_once ('ListGroupsResponse.php');
include_once ('ListSubvendorGroups.php');
include_once ('ListSubvendorGroupsResponse.php');
include_once ('GetGroup.php');
include_once ('GetGroupResponse.php');
include_once ('CreateGroup.php');
include_once ('CreateGroupResponse.php');
include_once ('UpdateGroup.php');
include_once ('UpdateGroupResponse.php');
include_once ('DeleteGroup.php');
include_once ('DeleteGroupResponse.php');
include_once ('ListUsersOfGroup.php');
include_once ('ListUsersOfGroupResponse.php');
include_once ('SetUsersOfGroup.php');
include_once ('SetUsersOfGroupResponse.php');
include_once ('SetObjectPermissions.php');
include_once ('ObjectPermission.php');
include_once ('SetObjectPermissionsResponse.php');
include_once ('ListObjectPermissions.php');
include_once ('ListObjectPermissionsResponse.php');
include_once ('Login.php');
include_once ('LoginResponse.php');
include_once ('Logout.php');
include_once ('LogoutResponse.php');

/**
 */
class SecurityService extends \SoapClient {
	
	/**
	 *
	 * @var array $classmap The defined classes
	 * @access private
	 */
	private static $classmap = array (
			'ListUsers' => '\ListUsers',
			'ListUsersResponse' => '\ListUsersResponse',
			'UserInfo' => '\UserInfo',
			'GenericFault' => '\GenericFault',
			'UnexpectedFault' => '\UnexpectedFault',
			'GetUser' => '\GetUser',
			'GetUserResponse' => '\GetUserResponse',
			'CreateUser' => '\CreateUser',
			'CreateUserResponse' => '\CreateUserResponse',
			'UpdateUser' => '\UpdateUser',
			'UpdateUserResponse' => '\UpdateUserResponse',
			'DeleteUser' => '\DeleteUser',
			'DeleteUserResponse' => '\DeleteUserResponse',
			'ListGroupsOfUser' => '\ListGroupsOfUser',
			'ListGroupsOfUserResponse' => '\ListGroupsOfUserResponse',
			'GroupInfo' => '\GroupInfo',
			'SetGroupsOfUser' => '\SetGroupsOfUser',
			'SetGroupsOfUserResponse' => '\SetGroupsOfUserResponse',
			'ListGroups' => '\ListGroups',
			'ListGroupsResponse' => '\ListGroupsResponse',
			'ListSubvendorGroups' => '\ListSubvendorGroups',
			'ListSubvendorGroupsResponse' => '\ListSubvendorGroupsResponse',
			'GetGroup' => '\GetGroup',
			'GetGroupResponse' => '\GetGroupResponse',
			'CreateGroup' => '\CreateGroup',
			'CreateGroupResponse' => '\CreateGroupResponse',
			'UpdateGroup' => '\UpdateGroup',
			'UpdateGroupResponse' => '\UpdateGroupResponse',
			'DeleteGroup' => '\DeleteGroup',
			'DeleteGroupResponse' => '\DeleteGroupResponse',
			'ListUsersOfGroup' => '\ListUsersOfGroup',
			'ListUsersOfGroupResponse' => '\ListUsersOfGroupResponse',
			'SetUsersOfGroup' => '\SetUsersOfGroup',
			'SetUsersOfGroupResponse' => '\SetUsersOfGroupResponse',
			'SetObjectPermissions' => '\SetObjectPermissions',
			'ObjectPermission' => '\ObjectPermission',
			'SetObjectPermissionsResponse' => '\SetObjectPermissionsResponse',
			'ListObjectPermissions' => '\ListObjectPermissions',
			'ListObjectPermissionsResponse' => '\ListObjectPermissionsResponse',
			'Login' => '\Login',
			'LoginResponse' => '\LoginResponse',
			'Logout' => '\Logout',
			'LogoutResponse' => '\LogoutResponse' 
	);
	
	/**
	 *
	 * @param array $options
	 *        	A array of config values
	 * @param string $wsdl
	 *        	The wsdl file to use
	 * @access public
	 */
	public function __construct(array $options = array(), $wsdl = 'security.wsdl') {
		foreach ( self::$classmap as $key => $value ) {
			if (! isset ( $options ['classmap'] [$key] )) {
				$options ['classmap'] [$key] = $value;
			}
		}
		
		parent::__construct ( $wsdl, $options );
	}
	
	/**
	 *
	 * @param ListUsers $parameters        	
	 * @access public
	 * @return ListUsersResponse
	 */
	public function ListUsers(ListUsers $parameters) {
		return $this->__soapCall ( 'ListUsers', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param GetUser $parameters        	
	 * @access public
	 * @return GetUserResponse
	 */
	public function GetUser(GetUser $parameters) {
		return $this->__soapCall ( 'GetUser', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param CreateUser $parameters        	
	 * @access public
	 * @return CreateUserResponse
	 */
	public function CreateUser(CreateUser $parameters) {
		return $this->__soapCall ( 'CreateUser', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param UpdateUser $parameters        	
	 * @access public
	 * @return UpdateUserResponse
	 */
	public function UpdateUser(UpdateUser $parameters) {
		return $this->__soapCall ( 'UpdateUser', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param DeleteUser $parameters        	
	 * @access public
	 * @return DeleteUserResponse
	 */
	public function DeleteUser(DeleteUser $parameters) {
		return $this->__soapCall ( 'DeleteUser', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ListGroupsOfUser $parameters        	
	 * @access public
	 * @return ListGroupsOfUserResponse
	 */
	public function ListGroupsOfUser(ListGroupsOfUser $parameters) {
		return $this->__soapCall ( 'ListGroupsOfUser', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param SetGroupsOfUser $parameters        	
	 * @access public
	 * @return SetGroupsOfUserResponse
	 */
	public function SetGroupsOfUser(SetGroupsOfUser $parameters) {
		return $this->__soapCall ( 'SetGroupsOfUser', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ListGroups $parameters        	
	 * @access public
	 * @return ListGroupsResponse
	 */
	public function ListGroups(ListGroups $parameters) {
		return $this->__soapCall ( 'ListGroups', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ListSubvendorGroups $parameters        	
	 * @access public
	 * @return ListSubvendorGroupsResponse
	 */
	public function ListSubvendorGroups(ListSubvendorGroups $parameters) {
		return $this->__soapCall ( 'ListSubvendorGroups', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param GetGroup $parameters        	
	 * @access public
	 * @return GetGroupResponse
	 */
	public function GetGroup(GetGroup $parameters) {
		return $this->__soapCall ( 'GetGroup', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param CreateGroup $parameters        	
	 * @access public
	 * @return CreateGroupResponse
	 */
	public function CreateGroup(CreateGroup $parameters) {
		return $this->__soapCall ( 'CreateGroup', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param UpdateGroup $parameters        	
	 * @access public
	 * @return UpdateGroupResponse
	 */
	public function UpdateGroup(UpdateGroup $parameters) {
		return $this->__soapCall ( 'UpdateGroup', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param DeleteGroup $parameters        	
	 * @access public
	 * @return DeleteGroupResponse
	 */
	public function DeleteGroup(DeleteGroup $parameters) {
		return $this->__soapCall ( 'DeleteGroup', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ListUsersOfGroup $parameters        	
	 * @access public
	 * @return ListUsersOfGroupResponse
	 */
	public function ListUsersOfGroup(ListUsersOfGroup $parameters) {
		return $this->__soapCall ( 'ListUsersOfGroup', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param SetUsersOfGroup $parameters        	
	 * @access public
	 * @return SetUsersOfGroupResponse
	 */
	public function SetUsersOfGroup(SetUsersOfGroup $parameters) {
		return $this->__soapCall ( 'SetUsersOfGroup', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param SetObjectPermissions $parameters        	
	 * @access public
	 * @return SetObjectPermissionsResponse
	 */
	public function SetObjectPermissions(SetObjectPermissions $parameters) {
		return $this->__soapCall ( 'SetObjectPermissions', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ListObjectPermissions $parameters        	
	 * @access public
	 * @return ListObjectPermissionsResponse
	 */
	public function ListObjectPermissions(ListObjectPermissions $parameters) {
		return $this->__soapCall ( 'ListObjectPermissions', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param Login $parameters        	
	 * @access public
	 * @return LoginResponse
	 */
	public function Login(Login $parameters) {
		return $this->__soapCall ( 'Login', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param Logout $parameters        	
	 * @access public
	 * @return LogoutResponse
	 */
	public function Logout(Logout $parameters) {
		return $this->__soapCall ( 'Logout', array (
				$parameters 
		) );
	}
}
