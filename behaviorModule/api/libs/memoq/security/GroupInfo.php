<?php
class GroupInfo {
	
	/**
	 *
	 * @var string $Description
	 * @access public
	 */
	public $Description = null;
	
	/**
	 *
	 * @var guid $GroupGuid
	 * @access public
	 */
	public $GroupGuid = null;
	
	/**
	 *
	 * @var string $GroupName
	 * @access public
	 */
	public $GroupName = null;
	
	/**
	 *
	 * @var boolean $IsDisabled
	 * @access public
	 */
	public $IsDisabled = null;
	
	/**
	 *
	 * @param guid $GroupGuid        	
	 * @param boolean $IsDisabled        	
	 * @access public
	 */
	public function __construct($GroupGuid, $IsDisabled) {
		$this->GroupGuid = $GroupGuid;
		$this->IsDisabled = $IsDisabled;
	}
}
