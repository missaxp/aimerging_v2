<?php
class ObjectPermission {
	
	/**
	 *
	 * @var dateTime $ExpirationDate
	 * @access public
	 */
	public $ExpirationDate = null;
	
	/**
	 *
	 * @var guid $GuidOfUserOrGroup
	 * @access public
	 */
	public $GuidOfUserOrGroup = null;
	
	/**
	 *
	 * @var int $PermissionId
	 * @access public
	 */
	public $PermissionId = null;
	
	/**
	 *
	 * @param dateTime $ExpirationDate        	
	 * @param guid $GuidOfUserOrGroup        	
	 * @param int $PermissionId        	
	 * @access public
	 */
	public function __construct($ExpirationDate, $GuidOfUserOrGroup, $PermissionId) {
		$this->ExpirationDate = $ExpirationDate;
		$this->GuidOfUserOrGroup = $GuidOfUserOrGroup;
		$this->PermissionId = $PermissionId;
	}
}
