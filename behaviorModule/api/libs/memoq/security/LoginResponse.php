<?php
class LoginResponse {
	
	/**
	 *
	 * @var string $LoginResult
	 * @access public
	 */
	public $LoginResult = null;
	
	/**
	 *
	 * @param string $LoginResult        	
	 * @access public
	 */
	public function __construct($LoginResult) {
		$this->LoginResult = $LoginResult;
	}
}
