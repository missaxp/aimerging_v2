<?php
class ListGroupsResponse {
	
	/**
	 *
	 * @var GroupInfo[] $ListGroupsResult
	 * @access public
	 */
	public $ListGroupsResult = null;
	
	/**
	 *
	 * @param GroupInfo[] $ListGroupsResult        	
	 * @access public
	 */
	public function __construct($ListGroupsResult) {
		$this->ListGroupsResult = $ListGroupsResult;
	}
}
