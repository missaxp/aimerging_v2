<?php
class ListSubvendorGroupsResponse {
	
	/**
	 *
	 * @var GroupInfo[] $ListSubvendorGroupsResult
	 * @access public
	 */
	public $ListSubvendorGroupsResult = null;
	
	/**
	 *
	 * @param GroupInfo[] $ListSubvendorGroupsResult        	
	 * @access public
	 */
	public function __construct($ListSubvendorGroupsResult) {
		$this->ListSubvendorGroupsResult = $ListSubvendorGroupsResult;
	}
}
