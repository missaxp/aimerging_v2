<?php
class ListObjectPermissionsResponse {
	
	/**
	 *
	 * @var ObjectPermission[] $ListObjectPermissionsResult
	 * @access public
	 */
	public $ListObjectPermissionsResult = null;
	
	/**
	 *
	 * @param ObjectPermission[] $ListObjectPermissionsResult        	
	 * @access public
	 */
	public function __construct($ListObjectPermissionsResult) {
		$this->ListObjectPermissionsResult = $ListObjectPermissionsResult;
	}
}
