<?php
class SetGroupsOfUser {
	
	/**
	 *
	 * @var guid $userGuid
	 * @access public
	 */
	public $userGuid = null;
	
	/**
	 *
	 * @var guid[] $groupGuids
	 * @access public
	 */
	public $groupGuids = null;
	
	/**
	 *
	 * @param guid $userGuid        	
	 * @param guid[] $groupGuids        	
	 * @access public
	 */
	public function __construct($userGuid, $groupGuids) {
		$this->userGuid = $userGuid;
		$this->groupGuids = $groupGuids;
	}
}
