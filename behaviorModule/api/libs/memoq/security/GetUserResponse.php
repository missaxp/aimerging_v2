<?php
class GetUserResponse {
	
	/**
	 *
	 * @var UserInfo $GetUserResult
	 * @access public
	 */
	public $GetUserResult = null;
	
	/**
	 *
	 * @param UserInfo $GetUserResult        	
	 * @access public
	 */
	public function __construct($GetUserResult) {
		$this->GetUserResult = $GetUserResult;
	}
}
