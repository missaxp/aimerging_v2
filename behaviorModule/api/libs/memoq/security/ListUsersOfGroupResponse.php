<?php
class ListUsersOfGroupResponse {
	
	/**
	 *
	 * @var UserInfo[] $ListUsersOfGroupResult
	 * @access public
	 */
	public $ListUsersOfGroupResult = null;
	
	/**
	 *
	 * @param UserInfo[] $ListUsersOfGroupResult        	
	 * @access public
	 */
	public function __construct($ListUsersOfGroupResult) {
		$this->ListUsersOfGroupResult = $ListUsersOfGroupResult;
	}
}
