<?php
class CreateUser {
	
	/**
	 *
	 * @var UserInfo $userInfo
	 * @access public
	 */
	public $userInfo = null;
	
	/**
	 *
	 * @param UserInfo $userInfo        	
	 * @access public
	 */
	public function __construct($userInfo) {
		$this->userInfo = $userInfo;
	}
}
