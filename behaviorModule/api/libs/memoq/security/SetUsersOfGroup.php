<?php
class SetUsersOfGroup {
	
	/**
	 *
	 * @var guid $groupGuid
	 * @access public
	 */
	public $groupGuid = null;
	
	/**
	 *
	 * @var guid[] $userGuids
	 * @access public
	 */
	public $userGuids = null;
	
	/**
	 *
	 * @param guid $groupGuid        	
	 * @param guid[] $userGuids        	
	 * @access public
	 */
	public function __construct($groupGuid, $userGuids) {
		$this->groupGuid = $groupGuid;
		$this->userGuids = $userGuids;
	}
}
