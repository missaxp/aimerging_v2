<?php
class ListGroupsOfUser {
	
	/**
	 *
	 * @var guid $userGuid
	 * @access public
	 */
	public $userGuid = null;
	
	/**
	 *
	 * @param guid $userGuid        	
	 * @access public
	 */
	public function __construct($userGuid) {
		$this->userGuid = $userGuid;
	}
}
