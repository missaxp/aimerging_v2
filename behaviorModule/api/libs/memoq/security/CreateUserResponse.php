<?php
class CreateUserResponse {
	
	/**
	 *
	 * @var guid $CreateUserResult
	 * @access public
	 */
	public $CreateUserResult = null;
	
	/**
	 *
	 * @param guid $CreateUserResult        	
	 * @access public
	 */
	public function __construct($CreateUserResult) {
		$this->CreateUserResult = $CreateUserResult;
	}
}
