<?php
class UserPackageWorkflowType {
	const __default = 'Online';
	const Online = 'Online';
	const Both = 'Both';
	const PackagesOnly = 'PackagesOnly';
}
