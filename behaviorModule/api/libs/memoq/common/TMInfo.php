<?php
include_once ('HeavyResourceInfo.php');
class TMInfo extends HeavyResourceInfo {
	
	/**
	 *
	 * @var boolean $AllowMultiple
	 * @access public
	 */
	public $AllowMultiple = null;
	
	/**
	 *
	 * @var boolean $AllowReverseLookup
	 * @access public
	 */
	public $AllowReverseLookup = null;
	
	/**
	 *
	 * @var string $SourceLanguageCode
	 * @access public
	 */
	public $SourceLanguageCode = null;
	
	/**
	 *
	 * @var boolean $StoreFormatting
	 * @access public
	 */
	public $StoreFormatting = null;
	
	/**
	 *
	 * @var string $TargetLanguageCode
	 * @access public
	 */
	public $TargetLanguageCode = null;
	
	/**
	 *
	 * @var boolean $UseContext
	 * @access public
	 */
	public $UseContext = null;
	
	/**
	 *
	 * @var boolean $UseIceSpiceContext
	 * @access public
	 */
	public $UseIceSpiceContext = null;
	
	/**
	 *
	 * @param boolean $AllowMultiple        	
	 * @param boolean $AllowReverseLookup        	
	 * @param boolean $StoreFormatting        	
	 * @param boolean $UseContext        	
	 * @param boolean $UseIceSpiceContext        	
	 * @access public
	 */
	public function __construct($AllowMultiple, $AllowReverseLookup, $StoreFormatting, $UseContext, $UseIceSpiceContext) {
		parent::__construct ();
		$this->AllowMultiple = $AllowMultiple;
		$this->AllowReverseLookup = $AllowReverseLookup;
		$this->StoreFormatting = $StoreFormatting;
		$this->UseContext = $UseContext;
		$this->UseIceSpiceContext = $UseIceSpiceContext;
	}
}
