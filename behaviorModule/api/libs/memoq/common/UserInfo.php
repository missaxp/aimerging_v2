<?php
class UserInfo {
	
	/**
	 *
	 * @var string $Address
	 * @access public
	 */
	public $Address = null;
	
	/**
	 *
	 * @var string $EmailAddress
	 * @access public
	 */
	public $EmailAddress = null;
	
	/**
	 *
	 * @var string $FullName
	 * @access public
	 */
	public $FullName = null;
	
	/**
	 *
	 * @var boolean $IsDisabled
	 * @access public
	 */
	public $IsDisabled = null;
	
	/**
	 *
	 * @var string $LanguagePairs
	 * @access public
	 */
	public $LanguagePairs = null;
	
	/**
	 *
	 * @var string $MobilePhoneNumber
	 * @access public
	 */
	public $MobilePhoneNumber = null;
	
	/**
	 *
	 * @var UserPackageWorkflowType $PackageWorkflowType
	 * @access public
	 */
	public $PackageWorkflowType = null;
	
	/**
	 *
	 * @var string $Password
	 * @access public
	 */
	public $Password = null;
	
	/**
	 *
	 * @var string $PhoneNumber
	 * @access public
	 */
	public $PhoneNumber = null;
	
	/**
	 *
	 * @var guid $UserGuid
	 * @access public
	 */
	public $UserGuid = null;
	
	/**
	 *
	 * @var string $UserName
	 * @access public
	 */
	public $UserName = null;
	
	/**
	 *
	 * @param boolean $IsDisabled        	
	 * @param UserPackageWorkflowType $PackageWorkflowType        	
	 * @param guid $UserGuid        	
	 * @access public
	 */
	public function __construct($IsDisabled, $PackageWorkflowType, $UserGuid) {
		$this->IsDisabled = $IsDisabled;
		$this->PackageWorkflowType = $PackageWorkflowType;
		$this->UserGuid = $UserGuid;
	}
}
