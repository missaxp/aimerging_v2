<?php
include_once ('HeavyResourceInfo.php');
class CorpusInfo extends HeavyResourceInfo {
	
	/**
	 *
	 * @var string[] $LanguageCodes
	 * @access public
	 */
	public $LanguageCodes = null;
	
	/**
	 *
	 * @access public
	 */
	public function __construct() {
		parent::__construct ();
	}
}
