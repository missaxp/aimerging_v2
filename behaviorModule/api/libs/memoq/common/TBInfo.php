<?php
include_once ('HeavyResourceInfo.php');
class TBInfo extends HeavyResourceInfo {
	
	/**
	 *
	 * @var boolean $IsModerated
	 * @access public
	 */
	public $IsModerated = null;
	
	/**
	 *
	 * @var string[] $LanguageCodes
	 * @access public
	 */
	public $LanguageCodes = null;
	
	/**
	 *
	 * @param boolean $IsModerated        	
	 * @access public
	 */
	public function __construct($IsModerated) {
		parent::__construct ();
		$this->IsModerated = $IsModerated;
	}
}
