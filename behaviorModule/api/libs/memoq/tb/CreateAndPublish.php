<?php
class CreateAndPublish {
	
	/**
	 *
	 * @var TBInfo $info
	 * @access public
	 */
	public $info = null;
	
	/**
	 *
	 * @param TBInfo $info        	
	 * @access public
	 */
	public function __construct($info) {
		$this->info = $info;
	}
}
