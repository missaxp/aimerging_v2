<?php
class ConvertToQTerm {
	
	/**
	 *
	 * @var guid $tbGuid
	 * @access public
	 */
	public $tbGuid = null;
	
	/**
	 *
	 * @param guid $tbGuid        	
	 * @access public
	 */
	public function __construct($tbGuid) {
		$this->tbGuid = $tbGuid;
	}
}
