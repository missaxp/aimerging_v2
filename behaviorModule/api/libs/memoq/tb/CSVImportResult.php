<?php
class CSVImportResult {
	
	/**
	 *
	 * @var int $AllCount
	 * @access public
	 */
	public $AllCount = null;
	
	/**
	 *
	 * @var int $SuccessCount
	 * @access public
	 */
	public $SuccessCount = null;
	
	/**
	 *
	 * @param int $AllCount        	
	 * @param int $SuccessCount        	
	 * @access public
	 */
	public function __construct($AllCount, $SuccessCount) {
		$this->AllCount = $AllCount;
		$this->SuccessCount = $SuccessCount;
	}
}
