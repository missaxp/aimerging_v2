<?php
class TBFilter {
	
	/**
	 *
	 * @var TBFilterLangMode $LangMode
	 * @access public
	 */
	public $LangMode = null;
	
	/**
	 *
	 * @var string[] $LanguageCodes
	 * @access public
	 */
	public $LanguageCodes = null;
	
	/**
	 *
	 * @param TBFilterLangMode $LangMode        	
	 * @access public
	 */
	public function __construct($LangMode) {
		$this->LangMode = $LangMode;
	}
}
