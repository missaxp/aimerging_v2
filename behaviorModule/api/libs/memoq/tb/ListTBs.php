<?php
class ListTBs {
	
	/**
	 *
	 * @var string[] $languages
	 * @access public
	 */
	public $languages = null;
	
	/**
	 *
	 * @param string[] $languages        	
	 * @access public
	 */
	public function __construct($languages) {
		$this->languages = $languages;
	}
}
