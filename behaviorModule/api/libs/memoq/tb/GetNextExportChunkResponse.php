<?php
class GetNextExportChunkResponse {
	
	/**
	 *
	 * @var base64Binary $GetNextExportChunkResult
	 * @access public
	 */
	public $GetNextExportChunkResult = null;
	
	/**
	 *
	 * @param base64Binary $GetNextExportChunkResult        	
	 * @access public
	 */
	public function __construct($GetNextExportChunkResult) {
		$this->GetNextExportChunkResult = $GetNextExportChunkResult;
	}
}
