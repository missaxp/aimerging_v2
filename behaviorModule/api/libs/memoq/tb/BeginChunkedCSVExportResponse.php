<?php
class BeginChunkedCSVExportResponse {
	
	/**
	 *
	 * @var guid $BeginChunkedCSVExportResult
	 * @access public
	 */
	public $BeginChunkedCSVExportResult = null;
	
	/**
	 *
	 * @param guid $BeginChunkedCSVExportResult        	
	 * @access public
	 */
	public function __construct($BeginChunkedCSVExportResult) {
		$this->BeginChunkedCSVExportResult = $BeginChunkedCSVExportResult;
	}
}
