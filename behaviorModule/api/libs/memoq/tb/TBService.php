<?php
include_once ('ListTBs.php');
include_once ('ListTBsResponse.php');
include_once (ROOT_PATH . '/common/TBInfo.php');
include_once ('HeavyResourceInfo.php');
include_once ('ResourceInfo.php');
include_once ('UnexpectedFault.php');
include_once ('GenericFault.php');
include_once ('ListTBs2.php');
include_once ('TBFilter.php');
include_once ('TBFilterLangMode.php');
include_once ('ListTBs2Response.php');
include_once ('BeginChunkedCSVImport.php');
include_once ('BeginChunkedCSVImportResponse.php');
include_once ('AddNextCSVChunk.php');
include_once ('AddNextCSVChunkResponse.php');
include_once ('EndChunkedCSVImport.php');
include_once ('EndChunkedCSVImportResponse.php');
include_once ('CSVImportResult.php');
include_once ('BeginChunkedCSVExport.php');
include_once ('BeginChunkedCSVExportResponse.php');
include_once ('BeginChunkedMultiTermExport.php');
include_once ('BeginChunkedMultiTermExportResponse.php');
include_once ('GetNextExportChunk.php');
include_once ('GetNextExportChunkResponse.php');
include_once ('EndChunkedExport.php');
include_once ('EndChunkedExportResponse.php');
include_once ('GetNextCSVChunk.php');
include_once ('GetNextCSVChunkResponse.php');
include_once ('EndChunkedCSVExport.php');
include_once ('EndChunkedCSVExportResponse.php');
include_once ('CreateAndPublish.php');
include_once ('CreateAndPublishResponse.php');
include_once ('DeleteTB.php');
include_once ('DeleteTBResponse.php');
include_once ('ConvertToQTerm.php');
include_once ('ConvertToQTermResponse.php');

/**
 */
class TBService extends \SoapClient {
	
	/**
	 *
	 * @var array $classmap The defined classes
	 * @access private
	 */
	private static $classmap = array (
			'ListTBs' => '\ListTBs',
			'ListTBsResponse' => '\ListTBsResponse',
			'TBInfo' => '\TBInfo',
			'HeavyResourceInfo' => '\HeavyResourceInfo',
			'ResourceInfo' => '\ResourceInfo',
			'UnexpectedFault' => '\UnexpectedFault',
			'GenericFault' => '\GenericFault',
			'ListTBs2' => '\ListTBs2',
			'TBFilter' => '\TBFilter',
			'ListTBs2Response' => '\ListTBs2Response',
			'BeginChunkedCSVImport' => '\BeginChunkedCSVImport',
			'BeginChunkedCSVImportResponse' => '\BeginChunkedCSVImportResponse',
			'AddNextCSVChunk' => '\AddNextCSVChunk',
			'AddNextCSVChunkResponse' => '\AddNextCSVChunkResponse',
			'EndChunkedCSVImport' => '\EndChunkedCSVImport',
			'EndChunkedCSVImportResponse' => '\EndChunkedCSVImportResponse',
			'CSVImportResult' => '\CSVImportResult',
			'BeginChunkedCSVExport' => '\BeginChunkedCSVExport',
			'BeginChunkedCSVExportResponse' => '\BeginChunkedCSVExportResponse',
			'BeginChunkedMultiTermExport' => '\BeginChunkedMultiTermExport',
			'BeginChunkedMultiTermExportResponse' => '\BeginChunkedMultiTermExportResponse',
			'GetNextExportChunk' => '\GetNextExportChunk',
			'GetNextExportChunkResponse' => '\GetNextExportChunkResponse',
			'EndChunkedExport' => '\EndChunkedExport',
			'EndChunkedExportResponse' => '\EndChunkedExportResponse',
			'GetNextCSVChunk' => '\GetNextCSVChunk',
			'GetNextCSVChunkResponse' => '\GetNextCSVChunkResponse',
			'EndChunkedCSVExport' => '\EndChunkedCSVExport',
			'EndChunkedCSVExportResponse' => '\EndChunkedCSVExportResponse',
			'CreateAndPublish' => '\CreateAndPublish',
			'CreateAndPublishResponse' => '\CreateAndPublishResponse',
			'DeleteTB' => '\DeleteTB',
			'DeleteTBResponse' => '\DeleteTBResponse',
			'ConvertToQTerm' => '\ConvertToQTerm',
			'ConvertToQTermResponse' => '\ConvertToQTermResponse' 
	);
	
	/**
	 *
	 * @param array $options
	 *        	A array of config values
	 * @param string $wsdl
	 *        	The wsdl file to use
	 * @access public
	 */
	public function __construct(array $options = array(), $wsdl = 'memoq/tb.wsdl') {
		foreach ( self::$classmap as $key => $value ) {
			if (! isset ( $options ['classmap'] [$key] )) {
				$options ['classmap'] [$key] = $value;
			}
		}
		
		parent::__construct ( $wsdl, $options );
	}
	
	/**
	 *
	 * @param ListTBs $parameters        	
	 * @access public
	 * @return ListTBsResponse
	 */
	public function ListTBs(ListTBs $parameters) {
		return $this->__soapCall ( 'ListTBs', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ListTBs2 $parameters        	
	 * @access public
	 * @return ListTBs2Response
	 */
	public function ListTBs2(ListTBs2 $parameters) {
		return $this->__soapCall ( 'ListTBs2', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param BeginChunkedCSVImport $parameters        	
	 * @access public
	 * @return BeginChunkedCSVImportResponse
	 */
	public function BeginChunkedCSVImport(BeginChunkedCSVImport $parameters) {
		return $this->__soapCall ( 'BeginChunkedCSVImport', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param AddNextCSVChunk $parameters        	
	 * @access public
	 * @return AddNextCSVChunkResponse
	 */
	public function AddNextCSVChunk(AddNextCSVChunk $parameters) {
		return $this->__soapCall ( 'AddNextCSVChunk', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param EndChunkedCSVImport $parameters        	
	 * @access public
	 * @return EndChunkedCSVImportResponse
	 */
	public function EndChunkedCSVImport(EndChunkedCSVImport $parameters) {
		return $this->__soapCall ( 'EndChunkedCSVImport', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param BeginChunkedCSVExport $parameters        	
	 * @access public
	 * @return BeginChunkedCSVExportResponse
	 */
	public function BeginChunkedCSVExport(BeginChunkedCSVExport $parameters) {
		return $this->__soapCall ( 'BeginChunkedCSVExport', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param BeginChunkedMultiTermExport $parameters        	
	 * @access public
	 * @return BeginChunkedMultiTermExportResponse
	 */
	public function BeginChunkedMultiTermExport(BeginChunkedMultiTermExport $parameters) {
		return $this->__soapCall ( 'BeginChunkedMultiTermExport', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param GetNextExportChunk $parameters        	
	 * @access public
	 * @return GetNextExportChunkResponse
	 */
	public function GetNextExportChunk(GetNextExportChunk $parameters) {
		return $this->__soapCall ( 'GetNextExportChunk', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param EndChunkedExport $parameters        	
	 * @access public
	 * @return EndChunkedExportResponse
	 */
	public function EndChunkedExport(EndChunkedExport $parameters) {
		return $this->__soapCall ( 'EndChunkedExport', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param GetNextCSVChunk $parameters        	
	 * @access public
	 * @return GetNextCSVChunkResponse
	 */
	public function GetNextCSVChunk(GetNextCSVChunk $parameters) {
		return $this->__soapCall ( 'GetNextCSVChunk', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param EndChunkedCSVExport $parameters        	
	 * @access public
	 * @return EndChunkedCSVExportResponse
	 */
	public function EndChunkedCSVExport(EndChunkedCSVExport $parameters) {
		return $this->__soapCall ( 'EndChunkedCSVExport', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param CreateAndPublish $parameters        	
	 * @access public
	 * @return CreateAndPublishResponse
	 */
	public function CreateAndPublish(CreateAndPublish $parameters) {
		return $this->__soapCall ( 'CreateAndPublish', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param DeleteTB $parameters        	
	 * @access public
	 * @return DeleteTBResponse
	 */
	public function DeleteTB(DeleteTB $parameters) {
		return $this->__soapCall ( 'DeleteTB', array (
				$parameters 
		) );
	}
	
	/**
	 *
	 * @param ConvertToQTerm $parameters        	
	 * @access public
	 * @return ConvertToQTermResponse
	 */
	public function ConvertToQTerm(ConvertToQTerm $parameters) {
		return $this->__soapCall ( 'ConvertToQTerm', array (
				$parameters 
		) );
	}
}
