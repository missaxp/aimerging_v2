<?php

namespace memoq;

class project {
	public $client;
	public $project;
	public $name;
	public $sourceLanguageCode;
	
	/**
	 * Id del botó
	 *
	 * @var int
	 */
	public $id_boto;
	
	/**
	 * Títol visible
	 *
	 * @var string
	 */
	public $titol = null;
	
	/**
	 * Descripció
	 *
	 * @var string
	 */
	public $descripcio = null;
	
	/**
	 * Url on apunta el botó
	 *
	 * @var string
	 */
	public $link = null;
	
	/**
	 * Id de la imatge d'on associada al botó
	 *
	 * @var int
	 */
	public $id_img_on = null;
	
	/**
	 * Id de la imatge d'off associada al botó
	 *
	 * @var int
	 */
	public $id_img_off = null;
	
	// --- MÈTODES ---
	/**
	 * Constructor
	 *
	 * @param mixed[] $array_valors        	
	 * @param Pagina $pagina        	
	 * @param Idioma $idioma
	 *        	(optional)
	 */
	public function __construct($id = null) {
		if ($id != null) {
			// recupera la tasca;
		}
		// Recupera els valors de l'array
	}
	public function projectExist() {
		$args = func_get_args ();
		$args = $args [0];
		
		if (isset ( $args ["CLIENT"] )) {
			$client = $args ["CLIENT"];
		} else {
			$client = "";
		}
		
		if (isset ( $args ["PROJECT_NAME"] )) {
			$name_project = $args ["PROJECT_NAME"];
		} else {
			$name_project = "";
		}
		
		$projectGuid = "";
		
		/**
		 * LLibreria generada amb wsdl2phpgenerator:
		 * /webs/d2website/stuff/wsdl2phpgenerator/# php wsdl2phpgenerator-2.4.0.phar -i memoq/serverproject.wsdl -o memoq/serverproject
		 */
		include_once $_SERVER ["DOCUMENT_ROOT"] . '/libs/memoq/serverproject/ServerProjectService.php';
		$reqProject = new \ServerProjectService ( array (
				'trace' => 1,
				"exception" => 0 
		), "http://memoq2.idisc.es/memoqservices/serverproject" );
		
		$parameters = new \ServerProjectListFilter ( null );
		if ($client != "") {
			//$parameters->Client = $client;
		}
		$parameters->Project = $args["Project"];
		
		//die(var_dump($parameters));
		
		$listProject = new \ListProjects ( $parameters );
		
		//die(var_dump($reqProject->ListProjects($listProject)));
		
		$respProjects = new \ListProjectsResponse ( $reqProject->ListProjects ( $listProject ) );

		$projectGuidList = $respProjects->ListProjectsResult->ListProjectsResult->ServerProjectInfo;
		
		if(is_array($projectGuidList)){
			foreach ( $projectGuidList as $project_item ) {
				if ($project_item->Name == $name_project) {
					$projectGuid = $project_item->ServerProjectGuid;
					break;
				}
			}	
		}
		else{
			$projectGuid = $projectGuidList->ServerProjectGuid;
		}
		return $projectGuid;
	}
	public function addProject() {
		$args = func_get_args ();
		$args = $args [0];
		
		if (isset ( $args ["CLIENT"] )) {
			$client = $args ["CLIENT"];
		} else {
			$client = "";
		}
		
		if (isset ( $args ["PROJECT_ID"] )) {
			$project = $args ["PROJECT_ID"];
		} else {
			$project = "";
		}
		
		if (isset ( $args ["NAME_PROJECT"] )) {
			$name_project = $args ["NAME_PROJECT"];
		} else {
			$name_project = "";
		}
		
		if (isset ( $args ["SOURCE_LANG"] )) {
			$source_lang = $args ["SOURCE_LANG"];
		} else {
			$source_lang = "";
		}
		
		if (isset ( $args ["TARGET_LANG"] )) {
			$target_lang = $args ["TARGET_LANG"];
		} else {
			$target_lang = array ();
		}
		
		if (isset ( $args ["USERS"] )) {
			$user = $args ["USERS"];
		} else {
			$user = array ();
		}
		
		if (isset ( $args ["TM_NAME"] )) {
			$targetTM = $args ["TM_NAME"];
		} else {
			$targetTM = array ();
		}
		
		/**
		 * LLibreria generada amb wsdl2phpgenerator:
		 * /webs/d2website/stuff/wsdl2phpgenerator/# php wsdl2phpgenerator-2.4.0.phar -i memoq/serverproject.wsdl -o memoq/serverproject
		 */
		include_once $_SERVER ["DOCUMENT_ROOT"] . '/libs/memoq/serverproject/ServerProjectService.php';
		$reqProject = new \ServerProjectService ( array (
				'trace' => 1,
				"exception" => 0 
		), "http://memoq2.idisc.es/memoqservices/serverproject" );
		
		include_once $_SERVER ["DOCUMENT_ROOT"] . '/libs/memoq/security/SecurityService.php';
		$reqSecurity = new \SecurityService ( array (
				'trace' => 1,
				"exception" => 0 
		), "http://memoq2.idisc.es/memoqservices/security" );
		
		// Objecte ServerProjectCreateInfo amb la informació de la petició
		$SPCInfo = new \ServerProjectCreateInfo ();
		$SPCInfo->AllowOverlappingWorkflow = false;
		$SPCInfo->Client = $client;
		$SPCInfo->Project = $project;
		$SPCInfo->Name = $name_project;
		$SPCInfo->SourceLanguageCode = $source_lang;
		
		// En funció de com vulguin guardar la data la generem amb +0h o +1h
		// $dt = new DateTime(null, new DateTimeZone('Europe/Madrid')); // Zona +1
		$dt = new \DateTime ( null, new \DateTimeZone ( 'UTC' ) ); // Zona +0
		$dt->add ( new \DateInterval ( 'P03M' ) );
		
		// Altres formats:
		// echo $dt->format('c')."<br />"; // UTC
		// echo $dt->format(DateTime::ISO8601)."<br />"; //ISO8601
		$SPCInfo->Deadline = $dt->format ( 'Y-m-d\TH:i:s\Z' );
		
		$SPCInfo->CreatorUser = self::getUser ( $user [0] );
		
		$arrayTargetLang = Array ();
		
		foreach ( $target_lang as $key => $value ) {
			array_push ( $arrayTargetLang, $value );
		}
		
		$SPCInfo->TargetLanguageCodes = $arrayTargetLang;
		$SPCInfo->DesktopDocs = true;
		$SPCInfo->DocumentStatus = "TranslationInProgress";
		
		$createProject = new \CreateProject ( $SPCInfo );
		
		$respProject = new \CreateProjectResponse ( $reqProject->CreateProject ( $createProject ) );
		$guidProject = $respProject->CreateProjectResult->CreateProjectResult;
		
		// ASSIGNACIÓ TM
		$AllTm = array ();
		foreach ( $targetTM as $key => $value ) {
			$TMGuid = self::getTM ( $targetTM [$key], $source_lang, $target_lang [$key] );
			$TMforLang = new \ServerProjectTMAssignmentsForTargetLang ( $TMGuid, null );
			$TMforLang->TargetLangCode = $target_lang [$key];
			$TMforLang->PrimaryTMGuid = $TMGuid;
			$TMforLang->TMGuids = array (
					$TMGuid 
			);
			
			array_push ( $AllTm, $TMforLang );
		}
		
		$setPTm = new \SetProjectTMs2 ( $guidProject, $AllTm );
		$respSetProject = new \SetProjectTMs2Response ( $reqProject->SetProjectTMs2 ( $setPTm ) );
		
		// ASSIGNACIÓ USERs
		$listUser = array ();
		$usrInf = new \ServerProjectUserInfo ( true, self::getUser ( $user [0] ) );
		$usrInf->ProjectRoles = new \ServerProjectRoles ( true, false );
		array_push ( $listUser, $usrInf );
		
		for($i = 1; $i <= count ( $user ) - 1; $i ++) {
			$usrInf = new \ServerProjectUserInfo ( true, self::getUser ( $user [$i] ) );
			$usrInf->ProjectRoles = new \ServerProjectRoles ( false, false );
			array_push ( $listUser, $usrInf );
		}
		
		$SetProjectUsr = new \SetProjectUsers ( $guidProject, $listUser );
		$respProjectUsr = new \SetProjectUsersResponse ( $reqProject->SetProjectUsers ( $SetProjectUsr ) );
		
		return $guidProject;
	}
	public function assigedUsertoDoc() {
		$args = func_get_args ();
		$args = $args [0];
		
		if (isset ( $args ["GUID_PROJECT"] )) {
			$guidProject = $args ["GUID_PROJECT"];
		} else {
			$guidProject = "";
		}
		
		if (isset ( $args ["USER_NAME"] )) {
			$userName = $args ["USER_NAME"];
		} else {
			$userName = "";
		}
		
		if (isset ( $args ["DOCUMENT_GUID"] )) {
			$documentGuid = $args ["DOCUMENT_GUID"];
		} else {
			$documentGuid = "";
		}
		
		include_once $_SERVER ["DOCUMENT_ROOT"] . '/libs/memoq/serverproject/ServerProjectService.php';
		$reqProject = new \ServerProjectService ( array (
				'trace' => 1,
				"exception" => 0 
		), "http://memoq2.idisc.es/memoqservices/serverproject" );
		
		$setListPrjTrans = new \ListProjectTranslationDocuments ( $guidProject );
		$respsetListPrjTrans = new \ListProjectTranslationDocumentsResponse ( $reqProject->ListProjectTranslationDocuments ( $setListPrjTrans ) );
		
		$documentsList = $respsetListPrjTrans->ListProjectTranslationDocumentsResult->ListProjectTranslationDocumentsResult;
		foreach ( $documentsList->ServerProjectTranslationDocInfo as $doc ) {
			if ($documentGuid == ( string ) $doc->DocumentGuid) {
				$documentGuid = $doc->DocumentGuid;
				break;
			}
		}
		
		$dt = new \DateTime ( null, new \DateTimeZone ( 'UTC' ) ); // Zona +0
		$dt->add ( new \DateInterval ( 'PT24H' ) );
		$DeadLine = $dt->format ( 'Y-m-d\TH:i:s\Z' );
		$DocumentAssignmentRole = 0;
		
		$guidUser = self::getUser ( $userName );
		
		unset ( $params );
		$params = array (
				"GUID_PROJECT" => $guidProject,
				"USER_NAME" => $userName 
		);
		
		$listUserPrj = new \ListProjectUsers ( $guidProject );
		$resplistUserPrj = new \ListProjectUsersResponse ( $reqProject->ListProjectUsers ( $listUserPrj ) );
		
		$userList = $resplistUserPrj->ListProjectUsersResult->ListProjectUsersResult->ServerProjectUserInfoHeader;
		
		$existUser = false;
		$arrayUser = array ();
		
		foreach ( $userList as $auxUser ) {
			// echo print_r ($auxUser, true)."<br />";
			if ($auxUser->User->UserName == $userName)
				$existUser = true;
			
			$usrInf = new \ServerProjectUserInfo ( true, self::getUser ( $auxUser->User->UserName ) );
			$usrInf->ProjectRoles = new \ServerProjectRoles ( $auxUser->ProjectRoles->ProjectManager, $auxUser->ProjectRoles->Terminologist );
			array_push ( $arrayUser, $usrInf );
		}
		
		if ($existUser) {
			echo "Existe <br />";
		} else {
			echo "No existe <br />";
		}
		
		if (! $existUser) {
			// ASSIGNACIÓ USUARIS
			$usrInf = new \ServerProjectUserInfo ( true, self::getUser ( $userName ) );
			$usrInf->ProjectRoles = new \ServerProjectRoles ( false, false );
			array_push ( $arrayUser, $usrInf );
			
			$SetProjectUsr = new \SetProjectUsers ( $guidProject, $arrayUser );
			$respProjectUsr = new \SetProjectUsersResponse ( $reqProject->SetProjectUsers ( $SetProjectUsr ) );
		}
		
		unset ( $arrayUser );
		
		$arrayUser = array ();
		
		$usrAssignment = new \ServerProjectTranslationDocumentUserAssignments ( $documentGuid );
		$usrAssignment->UserRoleAssignments [] = new \TranslationDocumentUserRoleAssignment ( $DeadLine, $DocumentAssignmentRole, $guidUser );
		array_push ( $arrayUser, $usrAssignment );
		
		$setPrjTrans = new \SetProjectTranslationDocumentUserAssignments ( $guidProject, $arrayUser );
		$respsetPrjTrans = new \SetProjectTranslationDocumentUserAssignmentsResponse ( $reqProject->SetProjectTranslationDocumentUserAssignments ( $setPrjTrans ) );
	}
	public function documentInfo() {
		$args = func_get_args ();
		$args = $args [0];
		
		if (isset ( $args ["GUID_PROJECT"] )) {
			$guidProject = $args ["GUID_PROJECT"];
		} else {
			$guidProject = "";
		}
		
		if (isset ( $args ["DOCUMENT_GUID"] )) {
			$documentGuid = $args ["DOCUMENT_GUID"];
		} else {
			$documentGuid = "";
		}
		
		include_once $_SERVER ["DOCUMENT_ROOT"] . '/libs/memoq/serverproject/ServerProjectService.php';
		$reqProject = new \ServerProjectService ( array (
				'trace' => 1,
				"exception" => 0 
		), "http://memoq2.idisc.es/memoqservices/serverproject" );
		
		$setListPrjTrans = new \ListProjectTranslationDocuments ( $guidProject );
		$respsetListPrjTrans = new \ListProjectTranslationDocumentsResponse ( $reqProject->ListProjectTranslationDocuments ( $setListPrjTrans ) );
		
		
		$documentsList = $respsetListPrjTrans->ListProjectTranslationDocumentsResult->ListProjectTranslationDocumentsResult;
		foreach ( $documentsList->ServerProjectTranslationDocInfo as $doc ) {
			if ($documentGuid == $doc->DocumentGuid) {
				$infoDoc = $doc;
			}
		}
		
		return $infoDoc;
	}
	
	public function assignedUsertoProject() {
		$args = func_get_args ();
		$args = $args [0];
		
		if (isset ( $args ["GUID_PROJECT"] )) {
			$guidProject = $args ["GUID_PROJECT"];
		} else {
			$guidProject = "";
		}
		
		if (isset ( $args ["USER_NAME"] )) {
			$userName = $args ["USER_NAME"];
		} else {
			$userName = "";
		}
		
		include_once $_SERVER ["DOCUMENT_ROOT"] . '/libs/memoq/serverproject/ServerProjectService.php';
		$reqProject = new \ServerProjectService ( array (
				'trace' => 1,
				"exception" => 0 
		), "http://memoq2.idisc.es/memoqservices/serverproject" );
		
		// ASSIGNACIÓ USERs
		$listUser = array ();
		$usrInf = new \ServerProjectUserInfo ( true, self::getUser ( $userName ) );
		$usrInf->ProjectRoles = new \ServerProjectRoles ( false, false );
		$SetProjectUsr = new \SetProjectUsers ( $guidProject, $listUser );
		$respProjectUsr = new \SetProjectUsersResponse ( $reqProject->SetProjectUsers ( $SetProjectUsr ) );
		
		return $guidProject;
	}
	public function getUser($name) {
		$guidUserValue = '';
		/**
		 * LLibreria generada amb wsdl2phpgenerator:
		 * /webs/d2website/stuff/wsdl2phpgenerator/# php wsdl2phpgenerator-2.4.0.phar -i memoq/security.wsdl -o memoq/security
		 */
		include_once $_SERVER ["DOCUMENT_ROOT"] . '/libs/memoq/security/SecurityService.php';
		$reqSecurity = new \SecurityService ( array (
				'trace' => 1,
				"exception" => 0 
		), "http://memoq2.idisc.es/memoqservices/security" );
		
		$listUsers = new \ListUsers ();
		$respUsers = new \ListUsersResponse ( $reqSecurity->ListUsers ( $listUsers ) );
		
		$listUsr = $respUsers->ListUsersResult->ListUsersResult->UserInfo;
		
		foreach ( $listUsr as $usr_item ) {
			if ($usr_item->UserName == $name) {
				$guidUserValue = $usr_item->UserGuid;
			}
		}
		
		unset ( $listUsr );
		
		return $guidUserValue;
	}
	public function getTM($name, $srcLang, $targetLang) {
		$guidTMValue = '';
		
		include_once ($_SERVER ["DOCUMENT_ROOT"] . '/libs/memoq/tm/TMService.php');
		$reqTm = new \TMService ( array (
				'trace' => 1,
				"exception" => 0 
		), "http://memoq2.idisc.es/memoqservices/tm" );
		
		$listTM = new \ListTMs ( $srcLang, $targetLang );
		$respTm = new \ListTMsResponse ( $reqTm->ListTMs ( $listTM ) );
		
		$listTM = $respTm->ListTMsResult->ListTMsResult->TMInfo;
		
		foreach ( $listTM as $TM_item ) {
			if ($TM_item->Name == $name) {
				$guidTMValue = $TM_item->Guid;
			}
		}
		
		unset ( $listTM );
		
		return $guidTMValue;
	}
	public function uploadFileToProject($args) {
		include_once $_SERVER ["DOCUMENT_ROOT"] . '/libs/memoq/serverproject/ServerProjectService.php';
		$reqProject = new \ServerProjectService ( array (
				'trace' => 1,
				"exception" => 0 
		), "http://memoq2.idisc.es/memoqservices/serverproject" );
		
		$args = func_get_args ();
		$args = $args [0];
		
		if (isset ( $args ["GUID_PROJECT"] )) {
			$guidProject = $args ["GUID_PROJECT"];
		} else {
			$guidProject = "";
		}
		
		if (isset ( $args ["FILE_PATH"] )) {
			$filePath = $args ["FILE_PATH"];
		} else {
			$filePath = "";
		}
		
		if (isset ( $args ["TARGET_LANG"] )) {
			$target_lang = $args ["TARGET_LANG"];
		} else {
			$target_lang = array ();
		}
		
		$fileGuid = '';
		$fileGuid = self::uploadFile ( $filePath );
		
		$arrayTargetLang = array ();
		foreach ( $target_lang as $key => $value ) {
			array_push ( $arrayTargetLang, $value );
		}
		
		$fileGuidTrad = array ();
		
		$importSettings = "importSettingsdoc";
		$docSettings = new \ImportTranslationDocument ( $guidProject, $fileGuid, $arrayTargetLang, $importSettings );
		
		$respImportTrans = new \ImportTranslationDocumentResponse ( $reqProject->ImportTranslationDocument ( $docSettings ) );
		
		$listGuid = $respImportTrans->ImportTranslationDocumentResult->ImportTranslationDocumentResult->DocumentGuids;
		
		$guidsPretrans = array ();
		if (count ( $target_lang ) == 1) {
			try {
				$fileGuidTrad [key ( $target_lang )] = $listGuid->guid;
				array_push ( $guidsPretrans, $listGuid->guid );
			} catch ( Exception $e ) {
				echo print_r ( $e, true );
				die ();
			}
		} else {
			$i = 0;
			foreach ( $target_lang as $key => $value ) {
				$fileGuidTrad [$key] = $listGuid->guid [$i];
				array_push ( $guidsPretrans, $listGuid->guid [$i] );
				$i ++;
			}
		}
		
		$ConfirmLockPretranslated = "ExactMatch";
		$ConfirmLockUnambiguousMatchesOnly = false;
		$FinalTranslationState = "Proofread";
		$GoodMatchRate = 99;
		$LockPretranslated = true;
		$OnlyUnambiguousMatches = false;
		$PretranslateLookupBehavior = "ExactMatch";
		$UseMT = false;
		
		$preTransOptions = new \PretranslateOptions ( $ConfirmLockPretranslated, $ConfirmLockUnambiguousMatchesOnly, $FinalTranslationState, $GoodMatchRate, $LockPretranslated, $OnlyUnambiguousMatches, $PretranslateLookupBehavior, $UseMT );
		$preTransDoc = new \PretranslateDocuments ( $guidProject, $guidsPretrans, $preTransOptions );
		$respPreTranslateDocuments = new \PretranslateDocumentsResponse ( $reqProject->PretranslateDocuments ( $preTransDoc ) );
		
		return $fileGuidTrad;
	}
	public function stadisticDocValue($args) {
		include_once $_SERVER ["DOCUMENT_ROOT"] . '/libs/memoq/serverproject/ServerProjectService.php';
		$reqProject = new \ServerProjectService ( array (
				'trace' => 1,
				"exception" => 0 
		), "http://memoq2.idisc.es/memoqservices/serverproject" );
		
		$args = func_get_args ();
		$args = $args [0];
		
		if (isset ( $args ["GUID_PROJECT"] )) {
			$guidProject = $args ["GUID_PROJECT"];
		} else {
			$guidProject = "";
		}
		
		if (isset ( $args ["GUIDS_FILE"] )) {
			$filesGuid = $args ["GUIDS_FILE"];
		} else {
			$filesGuid = "";
		}
		
		if (isset ( $args ["TARGET_LANG"] )) {
			$target_langs = $args ["TARGET_LANG"];
		} else {
			$target_langs = array ();
		}
		
		// ResultsFormat : Html, CSV_WithTable, CSV_Trados, CSV_MemoQ
		// Algorithm : Trados, MemoQ
		
		$Algorithm = "Trados";
		$Analysis_Homogenity = True;
		$Analysis_ProjectTMs = True;
		$Analyzis_DetailsByTM = True;
		$DisableCrossFileRepetition = False;
		$IncludeLockedRows = False;
		$RepetitionPreferenceOver100 = True;
		$ShowCounts = True;
		$ShowCounts_IncludeTargetCount = True;
		$ShowCounts_IncludeWhitespacesInCharCount = False;
		$ShowCounts_StatusReport = True;
		$ShowResultsPerFile = True;
		$TagCharWeight = 0;
		$TagWordWeight = 0;
		
		$returnedCount = array ();
		
		foreach ( $target_langs as $key => $value ) {
			$options = new \StatisticsOptions ( $Algorithm, $Analysis_Homogenity, $Analysis_ProjectTMs, $Analyzis_DetailsByTM, $DisableCrossFileRepetition, $IncludeLockedRows, $RepetitionPreferenceOver100, $ShowCounts, $ShowCounts_IncludeTargetCount, $ShowCounts_IncludeWhitespacesInCharCount, $ShowCounts_StatusReport, $ShowResultsPerFile, $TagCharWeight, $TagWordWeight );
			$resultFormat = "CSV_Trados";
			$guidsFiles = array (
					$filesGuid [$key] 
			);
			$gStTransDoc = new \GetStatisticsOnTranslationDocuments ( $guidProject, $guidsFiles, $options, $resultFormat );
			$respgStTransDoc = new \GetStatisticsOnTranslationDocumentsResponse ( $reqProject->GetStatisticsOnTranslationDocuments ( $gStTransDoc ) );
			
			$contListFile = $respgStTransDoc->GetStatisticsOnTranslationDocumentsResult->GetStatisticsOnTranslationDocumentsResult;
			
			if ($contListFile->ResultStatus == "Success") {
				
				$fitxerStadistica = $contListFile->ResultsForTargetLangs->StatisticsResultForLang->ResultData;
				$listCharacter = explode ( ";", $fitxerStadistica );
				$initChar = 73;
				
				$returnedCount [$key] = array (
						"CM" => ( int ) preg_replace ( "~[^0-9]~", "", $listCharacter [$initChar + 4] ),
						"Repes" => ( int ) preg_replace ( "~[^0-9]~", "", $listCharacter [$initChar + 8] ),
						"100%" => ( int ) preg_replace ( "~[^0-9]~", "", $listCharacter [$initChar + 12] ),
						"95%" => ( int ) preg_replace ( "~[^0-9]~", "", $listCharacter [$initChar + 16] ),
						"85%" => ( int ) preg_replace ( "~[^0-9]~", "", $listCharacter [$initChar + 20] ),
						"75%" => ( int ) preg_replace ( "~[^0-9]~", "", $listCharacter [$initChar + 24] ),
						"50%" => ( int ) preg_replace ( "~[^0-9]~", "", $listCharacter [$initChar + 28] ),
						"News" => ( int ) preg_replace ( "~[^0-9]~", "", $listCharacter [$initChar + 32] ) 
				);
			}
		}
		
		return $returnedCount;
	}
	public function uploadFile($filePath) {
		$fileGuid = '';
		
		$file = file_get_contents ( $filePath );
		
		include_once ($_SERVER ["DOCUMENT_ROOT"] . '/libs/memoq/filemanager/FileManagerService.php');
		$reqFileManager = new \FileManagerService ( array (
				'trace' => 1,
				"exception" => 0 
		), "http://memoq2.idisc.es/memoqservices/filemanager" );
		
		$BCFileUp = new \BeginChunkedFileUpload ( $filePath, false );
		$respBCFileUp = new \BeginChunkedFileUploadResponse ( $reqFileManager->BeginChunkedFileUpload ( $BCFileUp ) );
		
		$fileGuid = $respBCFileUp->BeginChunkedFileUploadResult->BeginChunkedFileUploadResult;
		
		$AddNext = new \AddNextFileChunk ( $fileGuid, $file );
		$respBCFileUp = new \AddNextFileChunkResponse ( $reqFileManager->AddNextFileChunk ( $AddNext ) );
		
		$endFileUp = new \EndChunkedFileUpload ( $fileGuid );
		$respBCFileUp = new \EndChunkedFileUpload ( $reqFileManager->EndChunkedFileUpload ( $endFileUp ) );
		
		return $fileGuid;
	}
	public function downloadFileFromProject() {
		$args = func_get_args ();
		$args = $args [0];
		
		if (isset ( $args ["GUID_PROJECT"] )) {
			$guidProject = $args ["GUID_PROJECT"];
		} else {
			$guidProject = "";
		}
		
		if (isset ( $args ["DOCUMENT_GUID"] )) {
			$documentGuid = $args ["DOCUMENT_GUID"];
		} else {
			$documentGuid = "";
		}
		
		if (isset ( $args ["TARGETS_PATH"] )) {
			$path = $args ["TARGETS_PATH"];
		} else {
			$path = array ();
		}
		
		if (isset ( $args ["TARGET_LANG"] )) {
			$lang = $args ["TARGET_LANG"];
		} else {
			$lang = array ();
		}
		
		include_once ($_SERVER ["DOCUMENT_ROOT"] . '/libs/memoq/filemanager/FileManagerService.php');
		$reqFileManager = new \FileManagerService ( array (
				'trace' => 1,
				"exception" => 0 
		), "http://memoq2.idisc.es/memoqservices/filemanager" );
		
		include_once $_SERVER ["DOCUMENT_ROOT"] . '/libs/memoq/serverproject/ServerProjectService.php';
		$reqProject = new \ServerProjectService ( array (
				'trace' => 1,
				"exception" => 0 
		), "http://memoq2.idisc.es/memoqservices/serverproject" );
		
		$expTransDoc = new \ExportTranslationDocument ( $guidProject, $documentGuid );
		$respExpTransDoc = new \ExportTranslationDocumentResponse ( $reqProject->ExportTranslationDocument ( $expTransDoc ) );
		
		$infoDoc = $respExpTransDoc->ExportTranslationDocumentResult->ExportTranslationDocumentResult;
		
		$docFileGuid = $infoDoc->FileGuid;
		$docResultStatus = $infoDoc->ResultStatus;
		
		if ($docResultStatus == "Success") {
			
			$fileName = "";
			$fileSize = "";
			
			$BCFileDown = new \BeginChunkedFileDownload ( $docFileGuid, False );
			$respBCFileDown = new \BeginChunkedFileDownloadResponse ( $reqFileManager->BeginChunkedFileDownload ( $BCFileDown ), $fileName, $fileSize );
			
			$infoBCFileDown = $respBCFileDown->BeginChunkedFileDownloadResult->BeginChunkedFileDownloadResult;
			
			$intDown = $respBCFileDown->BeginChunkedFileDownloadResult->BeginChunkedFileDownloadResult;
			$intNameFile = $respBCFileDown->BeginChunkedFileDownloadResult->fileName;
			$intSize = $respBCFileDown->BeginChunkedFileDownloadResult->fileSize;
			
			$getNextF = new \GetNextFileChunk ( $infoBCFileDown, $intSize );
			$respGetNextF = new \GetNextFileChunkResponse ( $reqFileManager->GetNextFileChunk ( $getNextF ) );
			
			$fileInfo = $respGetNextF->GetNextFileChunkResult->GetNextFileChunkResult;
			
			$endFileDown = new \EndChunkedFileDownload ( $infoBCFileDown );
			$respEndFileDown = new \EndChunkedFileDownloadResponse ( $reqFileManager->EndChunkedFileDownload ( $endFileDown ) );
			
			$fileAux = explode ( ".", $intNameFile );
			$nameFile = "";
			for($i = 0; $i < count ( $fileAux ) - 1; $i ++) {
				$nameFile .= $fileAux [$i] . ".";
			}
			
			$nameFile = trim ( substr ( $nameFile, 0, - 1 ) );
			
			$temp2 = count ( $fileAux ) - 1;
			$ext = $fileAux [$temp2];
			
			foreach ( $path as $FilePath ) {
				$fp = fopen ( $FilePath . $nameFile . "_" . $lang . "." . $ext, "w" );
				fwrite ( $fp, $fileInfo, strlen ( $fileInfo ) );
				fclose ( $fp );
			}
		}
		
		return $intNameFile;
	}
}
?>