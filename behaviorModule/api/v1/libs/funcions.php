<?php
use core\Environment;
use core\http\Status;
use core\http\ErrorCode;

/**
 * Funcions d'ús general
 */

/**
 * Obté el buffer d'error i el retorna en un string.
 *
 * @return string
 */
function debug_string_backtrace() {
	ob_start();
	debug_print_backtrace();
	$trace = ob_get_contents();
	ob_end_clean();

	// Remove first item from backtrace as it's this function which
	// is redundant.
	$trace = preg_replace('/^#0\s+'.__FUNCTION__."[^\n]*\n/", '', $trace, 1);

	
	// Renumber backtrace items.
	// El paràmetre /me en PHP 5.4 està obsolet
	//$trace = preg_replace('/^#(\d+)/me', '\'#\' . ($1 - 1)', $trace);
	$trace = preg_replace_callback(
    	"/^#(\d+)/m",
    	function($m) { return "\#".($m[1]-1); },
    	$trace
	);
	
	$trace = "\r\n".$trace;
	return $trace;
}
/* *************** Fi Control d'errors *************** */

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($data, $required_fields) {
	// $environment = Environment::getInstance();
	$error = false;
	$error_fields = array ();
	
	/*
	 * $request_params = array();
	 * $request_params = $_REQUEST;
	 * // Handling PUT request params
	 * if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
	 * $app = \Slim\Slim::getInstance();
	 * parse_str($app->request()->getBody(), $request_params);
	 * }
	 */
	foreach ( $required_fields as $field ) {
		if (! isset ( $data->$field ) || strlen ( trim ( $data->$field ) ) <= 0) {
			$error_fields [] = $field;
		}
	}
	
	if (count ( $error_fields ) > 0) {
		$error = $error_fields;
	}
	
	return $error;
}
function verifyParams($data, $params) {
	$environment = Environment::getInstance ();
	
	if (($camps_error = verifyRequiredParams ( $data, $params )) !== false) {
		$camps = "";
		foreach ( $camps_error as $camp ) {
			$camps .= ($camps != "" ? ", " : "") . $camp;
			
			$environment->response->addErrorField ( $camp, ErrorCode::FieldRequired );
		}
		
		$environment->response->send ( Status::S4_BadRequest, ErrorCode::IncompleteData, 'Required field(s) ' . $camps . ' are missing or empty' );
	}
	return true;
}

/**
 * Validating email address
 */
function validateEmail($email) {
	$environment = Entorn::getInstance ();
	
	if (! filter_var ( $email, FILTER_VALIDATE_EMAIL )) {
		$environment->response->message = 'Email address is not valid';
		$environment->response->send ( \core\http\Status::S4_BadRequest );
	}
}

/**
 * Retorna en format HTML el missatge d'error
 *
 * @param string $horaerr        	
 * @param string $errno        	
 * @param string $errstr        	
 * @param string $trace        	
 * @return string
 */
function getHTmlErrorMsg($horaerr, $errno, $errstr, $trace) {
	return "<strong>" . $horaerr . "</strong><br/>\r\n<h2>" . $errno . "</h2>\r\n<strong>" . $errstr . "</strong>\r\n<br/>" . str_replace ( "\n", "<br/>\n", $trace );
}

/**
 * Retorna en format HTML el missatge d'error
 *
 * @param string $horaerr        	
 * @param string $errno        	
 * @param string $errstr        	
 * @param string $trace        	
 * @return string
 */
function getXMLErrorMsg($horaerr, $errno, $errstr, $trace) {
	return "<error_time>" . $horaerr . "</error_time>\r\n<error_code>" . $errno . "</error_code>\r\n<error_message>" . $errstr . "</error_message>\r\n<error_trace>" . $trace . "</error_trace>\r\n";
}

/**
 * Retorna en format TXT el missatge d'error
 *
 * @param string $horaerr        	
 * @param string $errno        	
 * @param string $errstr        	
 * @param string $trace        	
 * @return string
 */
function getTxtErrorMsg($horaerr, $errno, $errstr, $trace) {
	return "*** " . $horaerr . " " . $errno . "\r\n*** " . $errstr . "\r\n*** " . $trace . "\r\n";
}

/**
 * Obté un string amb la data actual en el format d-m-Y
 *
 * @return string
 */
function now() {
	return date ("Ymd\THisO");
}

/**
 * Obté un string amb la data i hora actual en el format d-m-Y H:i:s
 *
 * @return string
 */
function nowtime() {
	//return date ( "d-m-Y H:i:s", time () );
	return date("Ymd\THis\Z");
}

/**
 * Returns if a string starts with $needle
 *
 * @param string $haystack        	
 * @param string $needle        	
 * @return boolean
 */
function startsWith($haystack, $needle) {
	return $needle === "" || stripos ( $haystack, $needle ) === 0;
}

/**
 * Returns if a string ends with $needle
 *
 * @param string $haystack        	
 * @param string $needle        	
 * @return boolean
 */
function endsWith($haystack, $needle) {
	return $needle === "" || substr ( $haystack, - strlen ( $needle ) ) === $needle;
}
function datetoISO8601($date) {
	try {
		$dateTime = new \DateTime ( $date );
		
		return $dateTime->format ( \DateTime::ISO8601 );
	} catch ( \Exception $e ) {
		return $date;
	}
}
function sendEmail($title, $bodyHTML, $mailFROM, $mailTO, $mailCC = array(), $filesArray = array(), $isError = false) {
	$errmsg_envia = "";
	try {
		require_once $_SERVER ["DOCUMENT_ROOT"] . "/libs/PHPMailer/PHPMailerAutoload.php";
		
		$environment = Environment::getInstance ();
		
		$mail = new PHPMailer ( true ); // the true param means it will throw exceptions on errors, which we need to catch
		$mail->IsSMTP (); // telling the class to use SMTP
		$mail->Host = $environment->mail_host; // SMTP server
		                                       // $mail->Username = $site->site_mail; // SMTP account username
		                                       // $mail->Password = ""; // SMTP account password
		$mail->CharSet = "UTF-8";
		$mail->IsHTML ( true );
		
		foreach ( $mailTO as $mailAux ) {
			$mail->AddAddress ( $mailAux ["address"], $mailAux ["name"] );
		}
		
		foreach ( $mailCC as $mailAux ) {
			$mail->AddCC ( $mailAux ["address"], $mailAux ["name"] );
		}
		
		$mail->SetFrom ( $mailFROM ["address"], $mailFROM ["name"] );
		
		foreach ( $filesArray as $fileA ) {
			$mail->AddAttachment ( $fileA ['path'], $fileA ['name'] );
		}
		
		$mail->Subject = $title;
		$mail->MsgHTML ( $bodyHTML );
		$mail->Send ();
		
		// Neteja Dades
		$mail->ClearAllRecipients ();
	} catch ( phpmailerException $e ) {
		$errmsg_envia .= "Error notificant: " . $e->errorMessage ();
		echo $errmsg_envia . "<br />";
	} catch ( Exception $e ) {
		$errmsg_envia .= "Error notificant: " . $e->getMessage ();
		echo $errmsg_envia . "<br />";
	}
}

/**
 * Given an array, sets all key to upper/lower case (default lower).
 * @param array $arr
 * @param integer $tocase
 * @return array
 * @author phidalgo
 * @date 20150909
 */
function array_change_key_case_recursive($arr,$tocase = CASE_LOWER){
	return array_map(function($item) use($tocase){
		if(is_array($item))
			$item = array_change_key_case_recursive($item,$tocase);
		return $item;
	},array_change_key_case($arr,$tocase));
}


/**
 * Given an array of characters, returns if any character is found on the string.
 * Returns the character found.
 * 
 * @param string $haystack
 * @param array $needles
 * @param number $offset
 * @return string || false
 */
function multi_strpos($haystack, $needles, $offset = 0) {

	foreach ($needles as $n) {
		$of = stripos($haystack, $n, $offset);
		if ($of !== false)
			return $n;
	}
	return false;
}

function str_lreplace($search, $replace, $subject)
{
	$pos = strripos($subject, $search);

	if($pos !== false){
		$subject = substr_replace($subject, $replace, $pos, strlen($search));
	}

	return $subject;
}

/**
 * Write a file to the given path.
 * 
 * @param string $fname filename
 * @param string $fpath path
 * @param mixed $bin binary|string|array 
 * @return boolean
 */
function write_file_to_disk($fname,$fpath,$bin){
	return (file_put_contents($fpath.$fname, $bin)!==false ? true:false);
}


/**
 * Crea un directori amb els permisos especificats (per defecte 0777) sense tenir en compte el umask
 * 
 * @param string $dir
 * @param number $mode
 * @param boolean $recursive
 * @return boolean
 */
function makedir($dir,$mode = 0777 ,$recursive = false){
	$old_umask = umask(0);
	$result = mkdir($dir, $mode, $recursive);
	umask($old_umask);
	return $result;
}



class randomString{
	protected static $randomStrings;
	private static $rstring;
	private $length;
	
	private function __construct($length){
		if(self::$randomStrings==null){
			self::$randomStrings = array();
		}
		$this->length = $length;
	}
	
	public static function getRandom($length = 7){
		if(self::$rstring==null){
			self::$rstring = new self($length);
		}
		return self::$rstring->get();
	}
	
	private function get(){
		$str = $this->generateString();
		if(in_array($str,self::$randomStrings)){
			$str = $this->get();
		}
		else{
			self::$randomStrings[] = $str;
		}
		return $str;
	}
	
	private function generateString(){
		$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $this->length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
}




?>