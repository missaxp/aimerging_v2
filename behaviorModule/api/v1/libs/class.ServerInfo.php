<?php

/**
 * Gets Server Info.
 * 
 * @author phidalgo
 *
 */
namespace core;

class serverInfo {
	protected static $environment;
	private $cpuLoad = 0;
	private $memory = array ();
	private $apacheStatus;
	private $logInfo;
	function __construct() {
		/**
		 * :)
		 */
		self::$environment = Environment::getInstance ();
	}
	
	/**
	 * Returns the configuration of environment.
	 *
	 * @return array
	 */
	public function getEnvironmentConfig() {
		return self::$environment->configuration;
	}
	public function getInfo() {
		$a ["cpu"] = $this->getCPUInfo ();
		$a ["memory"] = $this->getSystemMemInfo ();
		$a ["apache"] = $this->getApacheStatus ();
		$a ["log"] = $this->getAccessLogInfo ();
		return $a;
	}
	private function getCPUInfo() {
		$cmd = "NUMCPUS=`grep ^proc /proc/cpuinfo | wc -l`; FIRST=`cat /proc/stat | awk '/^cpu / {print $5}'`; sleep 1; SECOND=`cat /proc/stat | awk '/^cpu / {print $5}'`; USED=`echo 2 k 100 \$SECOND \$FIRST - \$NUMCPUS / - p | dc`; echo \${USED}";
		$cpu = shell_exec ( $cmd );
		
		if ($cpu [0] == "-") {
			$cpu = "0";
		}
		if ($cpu [0] == '.') {
			$cpu = "0" . $cpu;
		}
		return trim ( $cpu );
	}
	private function getSystemMemInfo() {
		$data = shell_exec ( 'cat /proc/meminfo | head -2' );
		$data = explode ( chr ( 10 ), $data );
		unset ( $data [count ( $data ) - 1] );
		$meminfo = array ();
		foreach ( $data as $line ) {
			$line = trim ( $line );
			$b = explode ( ":", $line );
			$meminfo [$b [0]] = trim ( str_replace ( "kB", "", $b [1] ) );
		}
		return $meminfo;
	}
	private function getApacheStatus() {
		$apacheInfo = array ();
		
		$curl = curl_init ();
		curl_setopt_array ( $curl, array (
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => $_SERVER ['SERVER_NAME'] . '/server-status/?auto' 
		) );
		$data = curl_exec ( $curl );
		$data = explode ( chr ( 10 ), $data );
		unset ( $data [count ( $data ) - 1] );
		foreach ( $data as $line ) {
			$line = trim ( $line );
			$b = explode ( ":", $line );
			$b [0] = str_replace ( " ", "_", $b [0] );
			$apacheInfo [$b [0]] = trim ( $b [1] );
		}
		
		return $apacheInfo;
	}
	private function getAccessLogInfo() {
		date_default_timezone_set ( 'Europe/Madrid' );
		$requests = "awk '$4>\"[" . date ( 'd' ) . "/" . date ( "M" ) . "/" . date ( 'Y' ) . ":" . date ( 'H:i:s', strtotime ( '-5 seconds' ) ) . "\" && $1!=\"127.0.0.1\"' /var/log/apache2/idcpws/idcpws_access." . date ( "Ymd" ) . ".log | wc -l";
		$responsetime = "awk '$4>\"[" . date ( 'd' ) . "/" . date ( "M" ) . "/" . date ( 'Y' ) . ":" . date ( 'H:i:s', strtotime ( '-5 seconds' ) ) . "\" && $1!=\"127.0.0.1\" {print $6}' /var/log/apache2/idcpws/idcpws_access." . date ( "Ymd" ) . ".log";
		$responsebytes = "awk '$4>\"[" . date ( 'd' ) . "/" . date ( "M" ) . "/" . date ( 'Y' ) . ":" . date ( 'H:i:s', strtotime ( '-5 seconds' ) ) . "\" && $1!=\"127.0.0.1\" {print $7}' /var/log/apache2/idcpws/idcpws_access." . date ( "Ymd" ) . ".log";
		date_default_timezone_set ( 'UTC' );
		
		$req = shell_exec ( $requests );
		$resp = shell_exec ( $responsetime );
		$rb = shell_exec ( $responsebytes );
		
		$b = explode ( chr ( 10 ), $resp );
		$total_ms = 0;
		foreach ( $b as $ms ) {
			$total_ms += $ms;
		}
		
		$a = explode ( chr ( 10 ), $rb );
		$total_bytes = 0;
		foreach ( $a as $byte ) {
			$total_bytes += $byte;
		}
		
		$ret ["avgms"] = ($total_ms / 5 / 1000);
		$ret ["reqpersec"] = $req / 5;
		$ret ["respkbytes"] = (($total_bytes / 5) / 1024);
		
		return $ret;
	}
	public function getRequestedURLS() {
		date_default_timezone_set ( 'Europe/Madrid' );
		// $8 = HTTP STATUS | $3 = SESSION_TOKEN | $9 = HTTP METHOD | $10 = URL | $11 = QUERY STRING
		$restURLS = "awk '$4>\"[" . date ( 'd' ) . "/" . date ( "M" ) . "/" . date ( 'Y' ) . ":" . date ( 'H:i:s', strtotime ( '-60 minutes' ) ) . "\" && $1!=\"127.0.0.1\" && $9!=\"OPTIONS\"  && $10 ~ /v[0-9]/ && $10!~ \"/v[0-9]/management/[a-zA-Z0-9]\" && $10!~ \"/v[0-9]/login\" && $10!~ \"/me/isLogged\"  {print $8\"_%_\"$3\"_%_\"$9\"_%_\"$10\"_%_\"$11}' /var/log/apache2/idcpws/idcpws_access." . date ( "Ymd" ) . ".log";
		$rURLS = shell_exec ( $restURLS );
		date_default_timezone_set ( 'UTC' );
		
		$a = explode ( chr ( 10 ), $rURLS );
		$total_urls = 0;
		$arr_restURLS = array ();
		foreach ( $a as $line ) {
			$line = str_replace ( '"', '', $line );
			if ($line != "") {
				$data = explode ( '_%_', $line );
				$status = $data [0];
				$token = $data [1];
				$method = $data [2];
				$url = $data [3];
				$qs = $data [4];
				
				$s = explode ( "/", $url );
				$ws_version = $s[1];
				$module = $s[2];
				$detail = isset ( $s [3] ) ? $s [3] : "-";
				
				$concatenat = $ws_version . "_" . $status . "_" . $method . "_" . $module . "_" . $detail;
				if ($this->url_exists ( $arr_restURLS, $concatenat )) {
					foreach ( $arr_restURLS as &$value ) {
						if ($value->CDATA == $concatenat) {
							$value->COUNT = $value->COUNT + 1;
							break;
						}
					}
				} else {
					$userName = null;
					if ($token != null) {
						$sql = "SELECT u.USERNAME FROM USERS u, SESSION_TOKEN st WHERE u.ID=st.USER_ID AND st.SESSION_TOKEN=:SESSION_TOKEN";
						$rs = self::$environment->dbcon->execute ( $sql, array (
								"SESSION_TOKEN" => $token 
						));
						if ($rs->fetch ()) {
							$userName = $rs->getVal ( "USERNAME" );
						}
						$rs->close ();
					}
					array_push ( $arr_restURLS, ( object ) array (
							"CDATA" => $concatenat,
							"APIV" => $ws_version,
							"RMETHOD" => $method,
							"MODULE" => $module,
							"DETAIL" => $detail,
							"RQS" => $qs,
							"STATUS" => $status,
							"COUNT" => 1,
							"SESSION_TOKEN" => $token,
							"USERNAME" => $userName 
					) );
				}
				$total_urls += $url;
			}
		}
		return $arr_restURLS;
	}
	private function url_exists($arr, $c) {
		foreach ( $arr as &$value ) {
			if ($value->CDATA == $c) {
				return true;
			}
		}
		return false;
	}
}

?>