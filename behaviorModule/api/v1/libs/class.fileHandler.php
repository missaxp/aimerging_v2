<?php
/**
 * This class provides a helpfull methods to upload chunked files.
 * 
 * Class partially integrated with environment. This class uses environment to get and make the response.
 * 
 * Adapted and improved class UploadHandler.php
 * https://github.com/blueimp/jQuery-File-Upload
 * 
 * @author phidalgo
 * @created 20160120
 * @modified
 */
namespace file;

use core\Environment;
use core\http\Status;
use core\http\ErrorCode;
use core\Exception\AppException;
use common\Tables;

class fileHandler{
	protected static $environment;
	
	protected $options;
	
	protected $response;
	
	protected $file_id; //Current file id processing.
	
	private $main; 
	
	public $uploadDoneReturn;
	
	public function __construct($options = null){
		self::$environment = Environment::getInstance();
		
		$this->setConfig($options); //Sets class configuration.
		
		/**
		 * This enables or disables chunk upload, file DB tracking and all properties of advanced upload binary.
		 * if set to false, sets that this upload is a regular upload (POST without chunked and without tracking). Usefull for
		 * upload independent files, such as avatars, and saves on a custom tables.
		 * 
		 * In that case, once the file is uploaded, it will save to a SPECIFIED DB FIELD in a SPECIFIED TABLE. 
		 * @var boolean
		 */
		$this->main = $this->options["main"];
		
		switch (self::$environment->app->request()->getMethod()) {
			case 'OPTIONS':
			case 'HEAD':
				$this->head();
				break;
			case 'GET':
				$this->get();
				break;
			case 'PATCH':
			case 'PUT':
			case 'POST':
				$this->response = $this->post(false);
				break;
			case 'DELETE':
				$this->delete();
				break;
			default:
				self::$environment->response->send(Status::S4_MethodNotAllowed,ErrorCode::MethodNotAllowed);
		}
	}
	
	public function getResponse(){
		return $this->response;
	}
	
	private function setConfig($options){
		$this->options = array(
				'script_url' => $this->get_full_url().'/',
				'upload_dir' => dirname($this->get_server_var('SCRIPT_FILENAME')).'/files/',
				'upload_url' => $this->get_full_url().'/files/',
				'user_dirs' => false,
				'mkdir_mode' => 0755,
				'param_name' => 'files',
				// Set the following option to 'POST', if your server does not support
				// DELETE requests. This is a parameter sent to the client:
				'delete_type' => 'DELETE',
				'access_control_allow_origin' => '*',
				'access_control_allow_credentials' => false,
				'access_control_allow_methods' => array(
						'OPTIONS',
						'HEAD',
						'GET',
						'POST',
						'PUT',
						'PATCH',
						'DELETE'
				),
				'access_control_allow_headers' => array(
						'Content-Type',
						'Content-Range',
						'Content-Disposition'
				),
				// Enable to provide file downloads via GET requests to the PHP script:
				//     1. Set to 1 to download files via readfile method through PHP
				//     2. Set to 2 to send a X-Sendfile header for lighttpd/Apache
				//     3. Set to 3 to send a X-Accel-Redirect header for nginx
				// If set to 2 or 3, adjust the upload_url option to the base path of
				// the redirect parameter, e.g. '/files/'.
				'download_via_php' => false,
				// Read files in chunks to avoid memory limits when download_via_php
				// is enabled, set to 0 to disable chunked reading of files:
				'readfile_chunk_size' => 10 * 1024 * 1024, // 10 MiB
				// Defines which files can be displayed inline when downloaded:
				'inline_file_types' => '/\.(gif|jpe?g|png)$/i',
				// Defines which files (based on their names) are accepted for upload:
				'accept_file_types' => '/.+$/i',
				// The php.ini settings upload_max_filesize and post_max_size
				// take precedence over the following max_file_size setting:
				'max_file_size' => null,
				'min_file_size' => 1,
				// The maximum number of files for the upload directory:
				'max_number_of_files' => null,
				// Defines which files are handled as image files:
				'image_file_types' => '/\.(gif|jpe?g|png)$/i',
				// Use exif_imagetype on all files to correct file extensions:
				'correct_image_extensions' => false,
				// Image resolution restrictions:
				'max_width' => null,
				'max_height' => null,
				'min_width' => 1,
				'min_height' => 1,
				// Set the following option to false to enable resumable uploads:
				'discard_aborted_uploads' => true,
				// Set to 0 to use the GD library to scale and orient images,
				// set to 1 to use imagick (if installed, falls back to GD),
				// set to 2 to use the ImageMagick convert binary directly:
				'image_library' => 1,
				// Uncomment the following to define an array of resource limits
				// for imagick:
				/*
				 'imagick_resource_limits' => array(
				 		imagick::RESOURCETYPE_MAP => 32,
				 		imagick::RESOURCETYPE_MEMORY => 32
				 ),
		*/
				// Command or path for to the ImageMagick convert binary:
				'convert_bin' => 'convert',
				// Uncomment the following to add parameters in front of each
				// ImageMagick convert call (the limit constraints seem only
				// to have an effect if put in front):
				/*
				 'convert_params' => '-limit memory 32MiB -limit map 32MiB',
		*/
				// Command or path for to the ImageMagick identify binary:
				'identify_bin' => 'identify',
				'image_versions' => array(
						// The empty image version key defines options for the original image:
						'' => array(
								// Automatically rotate images based on EXIF meta data:
								'auto_orient' => true
						),
						// Uncomment the following to create medium sized images:
						/*
						 'medium' => array(
						 		'max_width' => 800,
						 		'max_height' => 600
						 ),
		*/
						'thumbnail' => array(
								// Uncomment the following to use a defined directory for the thumbnails
								// instead of a subdirectory based on the version identifier.
								// Make sure that this directory doesn't allow execution of files if you
								// don't pose any restrictions on the type of uploaded files, e.g. by
								// copying the .htaccess file from the files directory for Apache:
								//'upload_dir' => dirname($this->get_server_var('SCRIPT_FILENAME')).'/thumb/',
								//'upload_url' => $this->get_full_url().'/thumb/',
								// Uncomment the following to force the max
								// dimensions and e.g. create square thumbnails:
								//'crop' => true,
								'max_width' => 80,
								'max_height' => 80
						)
				)
		);
		
		if ($options) {
			$this->options = $options + $this->options;
		}
	}
	
	protected function get_full_url() {
		$https = !empty($_SERVER['HTTPS']) && strcasecmp($_SERVER['HTTPS'], 'on') === 0 ||
		!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
		strcasecmp($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') === 0;
		return
		($https ? 'https://' : 'http://').
		(!empty($_SERVER['REMOTE_USER']) ? $_SERVER['REMOTE_USER'].'@' : '').
		(isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ($_SERVER['SERVER_NAME'].
				($https && $_SERVER['SERVER_PORT'] === 443 ||
						$_SERVER['SERVER_PORT'] === 80 ? '' : ':'.$_SERVER['SERVER_PORT']))).
						substr($_SERVER['SCRIPT_NAME'],0, strrpos($_SERVER['SCRIPT_NAME'], '/'));
	}
	
	protected function get_server_var($id) {
		return isset($_SERVER[$id]) ? $_SERVER[$id] : '';
	}
	
	public function post($print_response = true) {
		$upload = isset($_FILES[$this->options['param_name']]) ? $_FILES[$this->options['param_name']] : null;
		// Parse the Content-Disposition header, if available:
		//$file_name = $this->get_server_var('HTTP_CONTENT_DISPOSITION') ? rawurldecode(preg_replace('/(^[^"]+")|("$)/','',$this->get_server_var('HTTP_CONTENT_DISPOSITION'))) : null;
		$file_name = $this->get_server_var('HTTP_CONTENT_DISPOSITION') ? rawurldecode(preg_replace('/(^[^"]+")|("$)/','', 
		self::$environment->request->headers('Content-Disposition'))) : null;
		// Parse the Content-Range header, which has the following form: Content-Range: bytes 0-524287/2000000
		$content_range = self::$environment->request->headers('Content-Range') ? preg_split('/[^0-9]+/', self::$environment->request->headers('Content-Range')) : null;
		//$content_range = $this->get_server_var('HTTP_CONTENT_RANGE') ? preg_split('/[^0-9]+/', $this->get_server_var('HTTP_CONTENT_RANGE')) : null;
		$size =  $content_range ? $content_range[3] : null;
		$files = array();
		if ($upload && is_array($upload['tmp_name'])) {
			// param_name is an array identifier like "files[]", $_FILES is a multi-dimensional array:
			foreach ($upload['tmp_name'] as $index => $value) {
				$files[] = $this->handle_file_upload(
					$upload['tmp_name'][$index],
					$file_name ? $file_name : $upload['name'][$index],
					$size ? $size : $upload['size'][$index],
					$upload['type'][$index],
					$upload['error'][$index],
					$index,
					$content_range
				);
			}
		} 
		else {
			// param_name is a single object identifier like "file", $_FILES is a one-dimensional array:
			$files[] = $this->handle_file_upload(
				isset($upload['tmp_name']) ? $upload['tmp_name'] : null,
				$file_name ? $file_name : (isset($upload['name']) ?	$upload['name'] : null),
				$size ? $size : (isset($upload['size']) ? $upload['size'] : self::$environment->request->headers('Content-length')),
				isset($upload['type']) ? $upload['type'] : self::$environment->request->headers('Content-type'),
				isset($upload['error']) ? $upload['error'] : null,
				null,
				$content_range
			);
		}
		return $this->generate_response(
			array($this->options['param_name'] => $files),$print_response);
	}
	
	private function getFileIDFromRequest(){
		if(isset($this->options["track_id"]) && $this->options["track_id"]!=null && $this->options["track_id"]!="null"){
			$file_id = $this->options["track_id"];
			$chunk_size = $this->options["chunk_size"];
		}
		else{
			throw new AppException(Status::S4_PreconditionFailed,ErrorCode::FILE_LOST_WHEN_UPLOADING); //The file ID is not sent on chunked upload.
		}
		return $file_id;
	}
	
	/**
	 * Saves new information to DATABASE on MASTER_FILE TABLE.
	 */
	protected function handle_file_init($file,$name,$upload_dir,$file_path,$file_size,$type,$content_range){
		$normalized_name = $this->clearName($name);
		//If we are uploading a file for a element which has not created yet, we do not assign the file to a any element.
		//TODO: In the future the cron will remove any file which has not a id_rel value.
		if($this->options["element"]!=null){
			$vars["ID_REL"] = $this->options["element"];
		}
		
		$vars["NAME"] = $name;
		$vars["NORMALIZED_NAME"] = $normalized_name;
		$vars["PATH"] = $file_path;
		$vars["MD5"] = "";
		$vars["HDD"] = self::$environment->filesDB? 'N':'Y';
		$vars["TYPE"] = $type;
		$vars["IS_UPLOADING"] = 'Y';
		$vars["UPLOAD_FINISH"] = 'N';
		if(self::$environment->authentication_required){
			$vars["USER_ID"] = self::$environment->user->id;
		}
		$vars["TABLE_ID"] = $this->options["source"];
		$vars["FILE_SIZE"] = $file_size;
		
		if($content_range!=null){ //If the file is chunked, save to DB that it is the first part of the file.
			$vars["CHUNKED_CURRENT_PART"] = 1; 
			$from_bytes = $content_range[1];
			$to_bytes = $content_range[2];
			$chunk_size = $to_bytes - $from_bytes;
			$vars["CHUNKED_PARTS"] = ceil($file_size/$chunk_size);
			$vars["CHUNKED_CURRENT_UPLOAD"] = $to_bytes+1; //Content range goes from 0 to total, in DB starts from 1, is for that we add 1.
			$vars["CHUNK_SIZE"] = ($to_bytes - $from_bytes)+1;
		}
		
		/**
		 * As the uploading process of files may take a long time, even the file is chunked, we should do 
		 * a commit here to set and block the new MASTER_FILE_PK_ID key.
		 * 
		 * There is no need to set again DEFAULT_EXEC_MODE because this transaction remains in TRANSACTION mode.
		 */
		$rs = self::$environment->dbcon->execute("SELECT IFNULL[max(id)+1,1] AS NID FROM MASTER_FILE");
		$rs->fetch();
		$nid = $rs->nid;
		$rs->close();
		$vars["ID"] = $nid;
		self::$environment->dbcon->add("MASTER_FILE",$vars);
		self::$environment->dbcon->commit();
		
		$file->id = $nid;
		$file->name = $normalized_name;
		//$file->creation_ts = $rs->creation_ts;
		$file->type = $type;
		$file->file_size = $file_size;
		$file->comments = "";
		
		$this->file_id = $nid;
		return $file;
	}
	
	/**
	 * Called if the file is upload in parts.
	 * @param array $content_range
	 * @throws AppException
	 */
	protected function handle_file_uploading($content_range){
		$this->file_id = $this->getFileIDFromRequest();
		
		$rs = self::$environment->dbcon->execute("SELECT CHUNKED_PARTS,CHUNKED_CURRENT_PART,CHUNK_SIZE,CHUNKED_CURRENT_UPLOAD FROM MASTER_FILE WHERE ID=:ID",array("ID" => $this->file_id));
		if(!$rs->fetch()){
			throw new AppException(Status::S4_NotFound,ErrorCode::FILE_LOST_WHEN_UPLOADING);
		}
		
		$vars["CHUNKED_CURRENT_PART"] = (int) $rs->CHUNKED_CURRENT_PART + 1;
		$vars["CHUNKED_CURRENT_UPLOAD"] = (int) $content_range[2] +1;
		$rs->close();
		
		$varsW['ID'] = $this->file_id;
		self::$environment->dbcon->update("MASTER_FILE",$vars,"ID=:ID",$varsW);
	}
	
	/**
	 * Updates DATABASE data.
	 * 
	 * @param unknown $file
	 * @param unknown $file_path
	 * @param unknown $file_size
	 * @throws AppException
	 */
	protected function handle_file_finish($file,$file_path,$file_size){
		if($this->main){
			//In case of the file is not upload chunked, file id is set on $file object. If is a chunked upload, file is get from the request ID.
			if(isset($file->id)){
				$this->file_id = $file->id;
				$chunked = false;
			}
			else{
				$this->file_id = $this->getFileIDFromRequest();
				$chunked = true;
			}
			
			$sql = "SELECT * FROM MASTER_FILE WHERE ID=:ID";
			$rs = self::$environment->dbcon->execute($sql,array("ID" => $this->file_id));
			unset($vars);
			if(!$rs->fetch()){
				@unlink($file_path);
				//The file is successfully uploaded but i can not track the file and i can not update the file status on DB.
				throw new AppException(Status::S4_NotFound,ErrorCode::FILE_LOST_WHEN_UPLOADING);
			}
			$file->id = $rs->id;
			$file->name = $rs->normalized_name;
			$file->creation_ts = $rs->creation_ts;
			$file->type = $rs->type;
			$file->file_size = $rs->file_size;
			$file->comments = $rs->comments;
			$rs->close();
			
			$vars["IS_UPLOADING"] = 'N';
			$vars["UPLOAD_FINISH"] = 'Y';
			$vars["MD5"] = md5_file($file_path);
			if(!$chunked){
				$vars["CHUNKED_PARTS"] = 1;
				$vars["CHUNKED_CURRENT_PART"] = 1;
				$vars["CHUNK_SIZE"] = $file->file_size;
				$vars["CHUNKED_CURRENT_UPLOAD"] = $file->file_size;
			}
			$varsW["ID"] = $file->id;
			
			self::$environment->dbcon->update("MASTER_FILE",$vars,"ID=:ID",$varsW); //UPDATE MASTER FILE TABLE
			self::$environment->dbcon->commit();
			unset($vars);
			
			if(is_callable($this->options["uploaddone"])){
				$this->uploadDoneReturn = $this->options["uploaddone"]($file->id,$file_path,$this->options["element"]);
			}
		}
		else{
			$tinfo = self::$environment->tables->_get($this->options['source']);
			$dbfield = $this->options['dbfield'];
			
			$fh = @fopen($file_path, 'r');
			$binary = fread($fh, filesize($file_path));
			fclose($fh);
		
			$this->handle_images($binary);
			
			
			$vW["ID"] = $this->options['element'];
			self::$environment->dbcon->writeLob((string) $binary, $dbfield,$tinfo->db_name, $tinfo->primary_key[0]."=:ID", $vW);
			self::$environment->dbcon->commit();
			
			@unlink($file_path);
		}
		return $file;
	}

	
	protected function handle_file_upload($uploaded_file, $name, $size, $type, $error, $index = null, $content_range = null) {
		$file = new \stdClass();
		$file->name = $this->get_file_name($uploaded_file, $name, $size, $type, $error,	$index, $content_range);
		$file->size = $this->fix_integer_overflow(intval($size));
		$file->type = $type;
		
		$file_path = $this->get_upload_path($file->name);
		$file->uuid = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', microtime())."_".md5($file->name).".".pathinfo($file_path,PATHINFO_EXTENSION);
		//$file->uuid = preg_replace('/(0)\.(\d+) (\d+)/', '$3$1$2', microtime())."_".md5($file->name);
		if ($this->validate($uploaded_file, $file, $error, $index)) {
			$this->handle_form_data($file, $index);
			$upload_dir = $this->get_upload_path();
			if (!is_dir($upload_dir)) {
				makedir($upload_dir, $this->options['mkdir_mode'], true);
			}
			$file_path = $this->get_upload_path($file->uuid);
			
			$append_file = $content_range && is_file($file_path) &&	$file->size > $this->get_file_size($file_path);
			if ($uploaded_file && is_uploaded_file($uploaded_file)) {
				// multipart/formdata uploads (POST method uploads)
				if ($append_file){
					file_put_contents($file_path,fopen($uploaded_file, 'r'),FILE_APPEND);
				} else {
					move_uploaded_file($uploaded_file, $file_path);
				}
			} else {
				// Non-multipart uploads (PUT method support)
				file_put_contents($file_path,fopen('php://input', 'r'),$append_file ? FILE_APPEND : 0);
			}
			$file_size = $this->get_file_size($file_path, $append_file);
			/**
			 * If content range equals 0 it means that is the first chunk part.
			 * Also if no content range is added to headers, it would mean that the file is not chunked.
			 */
			if($content_range[1] == "0" || $content_range == null){
				if($this->main){
					$file = $this->handle_file_init($file,$name,$upload_dir,$file_path,$file->size,$type,$content_range);
				}
			}
			else{
				if($this->main){
					$this->handle_file_uploading($content_range);
				}
			}
			/**
			 * 
			 * In that point if $file_size == $file->size it means that the upload has been finished. So we can update DB.
			 *  
			 */
			if ($file_size === $file->size) {
				$file = $this->handle_file_finish($file,$file_path,$file->size);
				$file->url = $this->get_download_url($file->name);
				/* We don't want to treat images so disable this check
				if ($this->is_valid_image_file($file_path)) {
					$this->handle_image_file($file_path, $file);
				}*/
			} else {
				$file->size = $file_size;
				if (!$content_range && $this->options['discard_aborted_uploads']) {
					unlink($file_path);
					$file->error = $this->get_error_message('abort');
				}
			}
			$this->set_additional_file_properties($file);
		}
		return $file;
	}
	
	/**
	 * Adds additional data to the response.
	 * @param unknown $file
	 */
	protected function set_additional_file_properties($file) {
		/*$file->deleteUrl = $this->options['script_url'].$this->get_query_separator($this->options['script_url']).$this->get_singular_param_name().'='.rawurlencode($file->name);
		$file->deleteType = $this->options['delete_type'];
		if ($file->deleteType !== 'DELETE') {
			$file->deleteUrl .= '&_method=DELETE';
		}
		if ($this->options['access_control_allow_credentials']) {
			$file->deleteWithCredentials = true;
		}*/
	}
	
	protected function get_upload_path($file_name = null, $version = null) {
		$file_name = $file_name ? $file_name : '';
		if (empty($version)) {
			$version_path = '';
		} else {
			$version_dir = @$this->options['image_versions'][$version]['upload_dir'];
			if ($version_dir) {
				return $version_dir.$this->get_user_path().$file_name;
			}
			$version_path = $version.'/';
		}
		return $this->options['upload_dir'].$this->get_user_path()
		.$version_path.$file_name;
	}
	
	protected function get_user_path() {
		if ($this->options['user_dirs']) {
			return $this->options['user_dirs'].'/';
		}
		return '';
	}
	
	protected function handle_form_data($file, $index) {
		// Handle form data, e.g. $_REQUEST['description'][$index]
	}
	
	protected function validate($uploaded_file, $file, $error, $index) {
		if ($error) {
			$file->error = $this->get_error_message($error);
			throw new AppException(Status::S5_InternalServerError,$file->error);
		}
		$content_length = $this->fix_integer_overflow(intval($this->get_server_var('CONTENT_LENGTH')));
		$post_max_size = $this->get_config_bytes(ini_get('post_max_size'));
		if ($post_max_size && ($content_length > $post_max_size)) {
			$file->error = $this->get_error_message('post_max_size');
			return false;
		}
		if (!preg_match($this->options['accept_file_types'], $file->name)) {
			$file->error = $this->get_error_message('accept_file_types');
			return false;
		}
		if ($uploaded_file && is_uploaded_file($uploaded_file)) {
			$file_size = $this->get_file_size($uploaded_file);
		} else {
			$file_size = $content_length;
		}
		if ($this->options['max_file_size'] && ($file_size > $this->options['max_file_size'] ||	$file->size > $this->options['max_file_size'])) {
			$file->error = $this->get_error_message('max_file_size');
			return false;
		}
		if ($this->options['min_file_size'] && $file_size < $this->options['min_file_size']) {
			$file->error = $this->get_error_message('min_file_size');
			return false;
		}
		//Ignore additional chunks of existing files:
		if (is_int($this->options['max_number_of_files']) && ($this->count_file_objects() >= $this->options['max_number_of_files']) && !is_file($this->get_upload_path($file->name))) {
			$file->error = $this->get_error_message('max_number_of_files');
			return false;
		}
		
		$max_width = @$this->options['max_width'];
		$max_height = @$this->options['max_height'];
		$min_width = @$this->options['min_width'];
		$min_height = @$this->options['min_height'];
		if (($max_width || $max_height || $min_width || $min_height) && preg_match($this->options['image_file_types'], $file->name)) {
			list($img_width, $img_height) = $this->get_image_size($uploaded_file);
		}
		if (!empty($img_width)) {
			if ($max_width && $img_width > $max_width) {
				$file->error = $this->get_error_message('max_width');
				return false;
			}
			if ($max_height && $img_height > $max_height) {
				$file->error = $this->get_error_message('max_height');
				return false;
			}
			if ($min_width && $img_width < $min_width) {
				$file->error = $this->get_error_message('min_width');
				return false;
			}
			if ($min_height && $img_height < $min_height) {
				$file->error = $this->get_error_message('min_height');
				return false;
			}
		}
		return true;
	}
	
	protected function get_file_size($file_path, $clear_stat_cache = false) {
		if ($clear_stat_cache) {
			if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
				clearstatcache(true, $file_path);
			} else {
				clearstatcache();
			}
		}
		return $this->fix_integer_overflow(filesize($file_path));
	}
	
	function get_config_bytes($val) {
		$val = trim($val);
		$last = strtolower($val[strlen($val)-1]);
		switch($last) {
			case 'g':
				$val *= 1024;
			case 'm':
				$val *= 1024;
			case 'k':
				$val *= 1024;
		}
		return $this->fix_integer_overflow($val);
	}
	
	protected function get_error_message($error) {
		switch($error){
			case '1':
				return ErrorCode::FILE_1;
			case '2':
				return ErrorCode::FILE_2;
			case '3':
				return ErrorCode::FILE_3;
			case '4':
				return ErrorCode::FILE_4;
			case '6':
				return ErrorCode::FILE_6;
			case '7':
				return ErrorCode::FILE_7;
			case '8':
				return ErrorCode::FILE_8;
			case 'post_max_size':
				return ErrorCode::FILE_POST_MAX_SIZE;
			case 'max_file_size':
				return ErrorCode::FILE_MAX_FILE_SIZE;
			case 'min_file_size':
				return ErrorCode::FILE_MIN_FILE_SIZE;
			case 'accept_file_types':
				return ErrorCode::FILE_ACCEPT_FILE_TYPES;
			case 'max_number_of_files':
				return ErrorCode::FILE_MAX_NUMBER_OF_FILES;
			case 'abort':
				return ErrorCode::FILE_ABORT;
			default:
				return ErrorCode::FILE_DEFAULT_ERROR;
		}
	}
	
	// Fix for overflowing signed 32 bit integers,
	// works for sizes up to 2^32-1 bytes (4 GiB - 1):
	protected function fix_integer_overflow($size) {
		if ($size < 0) {
			$size += 2.0 * (PHP_INT_MAX + 1);
		}
		return $size;
	}
	
	protected function get_file_name($file_path, $name, $size, $type, $error,$index, $content_range) {
		$name = $this->trim_file_name($file_path, $name, $size, $type, $error, $index, $content_range);
		return $this->get_unique_filename(
			$file_path,
			$this->fix_file_extension($file_path, $name, $size, $type, $error,$index, $content_range),
			$size,
			$type,
			$error,
			$index,
			$content_range
		);
	}
	
	protected function get_query_separator($url) {
		return strpos($url, '?') === false ? '?' : '&';
	}
	
	protected function get_singular_param_name() {
		return substr($this->options['param_name'], 0, -1);
	}
	
	protected function get_unique_filename($file_path, $name, $size, $type, $error, $index, $content_range) {
		while(is_dir($this->get_upload_path($name))) {
			$name = $this->upcount_name($name);
		}
		// Keep an existing filename if this is part of a chunked upload:
		$uploaded_bytes = $this->fix_integer_overflow(intval($content_range[1]));
		while(is_file($this->get_upload_path($name))) {
			if ($uploaded_bytes === $this->get_file_size(
					$this->get_upload_path($name))) {
						break;
					}
					$name = $this->upcount_name($name);
		}
		return $name;
	}
	

	/**
	 * Normalitza un nom de fitxer.
	 * @param string $string Cadena d'entrada
	 * @return string Cadena normalitzada.
	 */
	protected function clearName($string) {
		$extensio = substr($string, strrpos($string,".") + 1, strlen($string));
		$nomftx = substr($string, 0, strrpos($string,"."));
		if ($nomftx=="" && $extensio=="") {
			$nom="";
		} else {
			$nom=$this->normalizeName($nomftx).".".$extensio;
		}
		return($nom);
	}
	
	/**
	 * Normalitza una cadena per utilitzar-la en una URL.
	 *
	 * @param string $string Cadena d'entrada
	 * @param bool Indica si no es vol que es reeplaci el punt
	 * @param bool Indica si no es vol que es reeplaci la barra /
	 * @return string Cadena normalitzada
	 */
	protected function normalizeName($string, $no_punt_replace=false, $no_barra_replace=false) {
		$options = array();
	
		if ($no_punt_replace) {
			$options['no_punt_replace'] = true;
		}
	
		if ($no_barra_replace) {
			$options['no_barra_replace'] = true;
		}
	
		return $this->url_slug($string, $options);
	
	}
	
	protected function url_slug($str, $options = array()) {
		// Make sure string is in UTF-8 and strip invalid UTF-8 characters
		$str = mb_convert_encoding((string)$str, 'UTF-8', mb_list_encodings());
	
		$defaults = array(
				'delimiter' => '-',
				'limit' => null,
				'lowercase' => true,
				'replacements' => array(),
				'transliterate' => true,
				'no_punt_replace' => false,
				'no_barra_replace' => false
		);
	
		// Merge options
		$options = array_merge($defaults, $options);
	
		$char_map = array(
				// Latin
				'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
				'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
				'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
				'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
				'ß' => 'ss',
				'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
				'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
				'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
				'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
				'ÿ' => 'y',
	
				// Latin symbols
				'©' => '(c)',
	
				// Greek
				'Α' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'H', 'Θ' => '8',
				'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M', 'Ν' => 'N', 'Ξ' => '3', 'Ο' => 'O', 'Π' => 'P',
				'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'W',
				'Ά' => 'A', 'Έ' => 'E', 'Ί' => 'I', 'Ό' => 'O', 'Ύ' => 'Y', 'Ή' => 'H', 'Ώ' => 'W', 'Ϊ' => 'I',
				'Ϋ' => 'Y',
				'α' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'h', 'θ' => '8',
				'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => '3', 'ο' => 'o', 'π' => 'p',
				'ρ' => 'r', 'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'w',
				'ά' => 'a', 'έ' => 'e', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ή' => 'h', 'ώ' => 'w', 'ς' => 's',
				'ϊ' => 'i', 'ΰ' => 'y', 'ϋ' => 'y', 'ΐ' => 'i',
	
				// Turkish
				'Ş' => 'S', 'İ' => 'I', 'Ç' => 'C', 'Ü' => 'U', 'Ö' => 'O', 'Ğ' => 'G',
				'ş' => 's', 'ı' => 'i', 'ç' => 'c', 'ü' => 'u', 'ö' => 'o', 'ğ' => 'g',
	
				// Russian
				'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'Yo', 'Ж' => 'Zh',
				'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
				'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
				'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sh', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu',
				'Я' => 'Ya',
				'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',
				'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
				'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
				'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu',
				'я' => 'ya',
	
				// Ukrainian
				'Є' => 'Ye', 'І' => 'I', 'Ї' => 'Yi', 'Ґ' => 'G',
				'є' => 'ye', 'і' => 'i', 'ї' => 'yi', 'ґ' => 'g',
	
				// Czech
				'Č' => 'C', 'Ď' => 'D', 'Ě' => 'E', 'Ň' => 'N', 'Ř' => 'R', 'Š' => 'S', 'Ť' => 'T', 'Ů' => 'U',
				'Ž' => 'Z',
				'č' => 'c', 'ď' => 'd', 'ě' => 'e', 'ň' => 'n', 'ř' => 'r', 'š' => 's', 'ť' => 't', 'ů' => 'u',
				'ž' => 'z',
	
				// Polish
				'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'e', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'o', 'Ś' => 'S', 'Ź' => 'Z',
				'Ż' => 'Z',
				'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z',
				'ż' => 'z',
	
				// Latvian
				'Ā' => 'A', 'Č' => 'C', 'Ē' => 'E', 'Ģ' => 'G', 'Ī' => 'i', 'Ķ' => 'k', 'Ļ' => 'L', 'Ņ' => 'N',
				'Š' => 'S', 'Ū' => 'u', 'Ž' => 'Z',
				'ā' => 'a', 'č' => 'c', 'ē' => 'e', 'ģ' => 'g', 'ī' => 'i', 'ķ' => 'k', 'ļ' => 'l', 'ņ' => 'n',
				'š' => 's', 'ū' => 'u', 'ž' => 'z'
		);
	
		// Make custom replacements
		$str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);
	
	
	
		// Transliterate characters to ASCII
		if ($options['transliterate']) {
			$str = str_replace(array_keys($char_map), $char_map, $str);
		}
	
	
	
		// Replace non-alphanumeric characters with our delimiter
		// ORIGInAL:
		//	$str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);
	
		// NOU:
		// Elimina la codificació html ($quot;, ...)
		$str = html_entity_decode($str, ENT_QUOTES, "UTF-8");
	
		$str = preg_replace('/[= +_~%&´`^*:;,\{\}\[\]()\'\\\|]/', "-", $str);
		$str = preg_replace('/[\?"<>!@#$·\\¡¢£¤¥¦§¨©ª«¬­®¯°±²³µ¶·¸¹º»¼½¾¿Ð×ØÞßð÷øþ]/', "", $str);
		if (!$options['no_punt_replace']) {
			$str = preg_replace('/\./', "", $str);
		}
	
		if (!$options['no_barra_replace']) {
			$str = preg_replace('/\//', "", $str);
		}
	
		$str = preg_replace('/(-)+/', "-", $str);
		// FI NOU
	
	
		// Remove duplicate delimiters
		$str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);
	
		// Truncate slug to max. characters
		$str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');
	
		// Remove delimiter from ends
		$str = trim($str, $options['delimiter']);
	
	
		return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
	}
	
	protected function trim_file_name($file_path, $name, $size, $type, $error,$index, $content_range) {
		// Remove path information and dots around the filename, to prevent uploading
		// into different directories or replacing hidden system files.
		// Also remove control characters and spaces (\x00..\x20) around the filename:
		$name = trim(basename(stripslashes($name)), ".\x00..\x20");
		// Use a timestamp for empty filenames:
		if (!$name) {
			$name = str_replace('.', '-', microtime(true));
		}
		return $name;
	}
	
	protected function fix_file_extension($file_path, $name, $size, $type, $error, $index, $content_range) {
		// Add missing file extension for known image types:
		if (strpos($name, '.') === false &&	preg_match('/^image\/(gif|jpe?g|png)/', $type, $matches)) {
			$name .= '.'.$matches[1];
		}
		if ($this->options['correct_image_extensions'] && function_exists('exif_imagetype')) {
			switch(@exif_imagetype($file_path)){
				case IMAGETYPE_JPEG:
					$extensions = array('jpg', 'jpeg');
					break;
				case IMAGETYPE_PNG:
					$extensions = array('png');
					break;
				case IMAGETYPE_GIF:
					$extensions = array('gif');
					break;
			}
			// Adjust incorrect image file extensions:
			if (!empty($extensions)) {
				$parts = explode('.', $name);
				$extIndex = count($parts) - 1;
				$ext = strtolower(@$parts[$extIndex]);
				if (!in_array($ext, $extensions)) {
					$parts[$extIndex] = $extensions[0];
					$name = implode('.', $parts);
				}
			}
		}
		return $name;
	}
	
	protected function upcount_name_callback($matches) {
		$index = isset($matches[1]) ? intval($matches[1]) + 1 : 1;
		$ext = isset($matches[2]) ? $matches[2] : '';
		return ' ('.$index.')'.$ext;
	}
	
	protected function upcount_name($name) {
		return preg_replace_callback(
				'/(?:(?: \(([\d]+)\))?(\.[^.]+))?$/',
				array($this, 'upcount_name_callback'),
				$name,
				1
				);
	}
	
	protected function header($str) {
		header($str);
	}
	
	protected function send_content_type_header() {
		$this->header('Vary: Accept');
		if (strpos($this->get_server_var('HTTP_ACCEPT'), 'application/json') !== false) {
			$this->header('Content-type: application/json');
		} else {
			$this->header('Content-type: text/plain');
		}
	}
	
	protected function body($str) {
		echo $str;
	}
	
	protected function get_download_url($file_name, $version = null, $direct = false) {
		if (!$direct && $this->options['download_via_php']) {
			$url = $this->options['script_url']
			.$this->get_query_separator($this->options['script_url'])
			.$this->get_singular_param_name()
			.'='.rawurlencode($file_name);
			if ($version) {
				$url .= '&version='.rawurlencode($version);
			}
			return $url.'&download=1';
		}
		if (empty($version)) {
			$version_path = '';
		} else {
			$version_url = @$this->options['image_versions'][$version]['upload_url'];
			if ($version_url) {
				return $version_url.$this->get_user_path().rawurlencode($file_name);
			}
			$version_path = rawurlencode($version).'/';
		}
		return $this->options['upload_url'].$this->get_user_path().$version_path.rawurlencode($file_name);
	}
	
	protected function is_valid_image_file($file_path) {
		if (!preg_match($this->options['image_file_types'], $file_path)) {
			return false;
		}
		if (function_exists('exif_imagetype')) {
			return @exif_imagetype($file_path);
		}
		$image_info = $this->get_image_size($file_path);
		return $image_info && $image_info[0] && $image_info[1];
	}
	
	protected function get_image_size($file_path) {
		if ($this->options['image_library']) {
			if (extension_loaded('imagick')) {
				$image = new \Imagick();
				try {
					if (@$image->pingImage($file_path)) {
						$dimensions = array($image->getImageWidth(), $image->getImageHeight());
						$image->destroy();
						return $dimensions;
					}
					return false;
				} catch (Exception $e) {
					error_log($e->getMessage());
				}
			}
			if ($this->options['image_library'] === 2) {
				$cmd = $this->options['identify_bin'];
				$cmd .= ' -ping '.escapeshellarg($file_path);
				exec($cmd, $output, $error);
				if (!$error && !empty($output)) {
					// image.jpg JPEG 1920x1080 1920x1080+0+0 8-bit sRGB 465KB 0.000u 0:00.000
					$infos = preg_split('/\s+/', $output[0]);
					$dimensions = preg_split('/x/', $infos[2]);
					return $dimensions;
				}
				return false;
			}
		}
		if (!function_exists('getimagesize')) {
			error_log('Function not found: getimagesize');
			return false;
		}
		return @getimagesize($file_path);
	}
	
	public function head() {
		$this->header('Pragma: no-cache');
		$this->header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->header('Content-Disposition: inline; filename="files.json"');
		// Prevent Internet Explorer from MIME-sniffing the content-type:
		$this->header('X-Content-Type-Options: nosniff');
		if ($this->options['access_control_allow_origin']) {
			$this->send_access_control_headers();
		}
		$this->send_content_type_header();
	}
	
	protected function generate_response($content, $print_response = true) {
		if ($print_response) {
			$json = json_encode($content);
			$redirect = isset($_REQUEST['redirect']) ?
			stripslashes($_REQUEST['redirect']) : null;
			if ($redirect) {
				$this->header('Location: '.sprintf($redirect, rawurlencode($json)));
				return;
			}
			$this->head();
			if ($this->get_server_var('HTTP_CONTENT_RANGE')) {
				$files = isset($content[$this->options['param_name']]) ?
				$content[$this->options['param_name']] : null;
				if ($files && is_array($files) && is_object($files[0]) && $files[0]->size) {
					$this->header('Range: 0-'.(
							$this->fix_integer_overflow(intval($files[0]->size)) - 1
							));
				}
			}
			$this->body($json);
		}
		return $content;
	}
	
	/**
	 * Tracta la imatge segons els paràmetres de configuració:
	 * Mides màximes i qualitat.
	 *
	 * @param string $stream Stream de la imatge a tractar
	 * @param string $tipus [optional] Tipus de la imatge ("image/gif","image/png",..)
	 * @param int $width Amplada de la imatge
	 * @param int $height Alçada de la imatge
	 *
	 * @return string
	 */
	protected function handle_images($stream, $tipus=null, $width=null, $height=null) {
		//Obté els valors màxims de la configuració per si hem de reescalar i la qualitat
		$maxW = self::$environment->avatarMaxWidth;
		$maxH = self::$environment->avatarMaxHeight;
		$qualitat = self::$environment->avatarQuality;
		// Obté el tipus en cas que no s'hagi indicat
		if (!isset($tipus)) {
			$tipus = $this->imageType($stream);
			if ($tipus!=IMAGETYPE_GIF && $tipus!=IMAGETYPE_PNG && $tipus!=IMAGETYPE_JPEG) {
				return false;
			}
		} else {
			// Si s'ha indicat comprova que sigui correcte
			if ($tipus=="image/gif") {
				$tipus = IMAGETYPE_GIF;
			} else if ($tipus=="image/png" || $tipus=="image/x-png") {
				$tipus = IMAGETYPE_PNG;
			} else if ($tipus=="image/jpeg" || $tipus=="image/pjpeg") {
				$tipus = IMAGETYPE_JPEG;
			} else
				return false;
		}
	
		if (!isset($width) || !isset($height)) {
			// Agafa la imatge del stream
			$img = imagecreatefromstring($stream);
			if (!$img)
				return false;
	
				// Obté les dimensions de la imatge
				if (!isset($width)) {
					$width = imagesx($img);
				}
				if (!isset($height)) {
					$height = imagesy($img);
				}
		}
		
		// Comprova si s'han de tractar les imatges
		// També s'inclou la qualitat ja que si no es tracta perd la trasparència en els PNG
		if (($width>$maxW && $maxW>0) || ($height>$maxH && $maxH>0) || ($qualitat<100 && $tipus<>IMAGETYPE_GIF)) {
			// Copia la imatge en una variable auxiliar
			$im2 = $img;
	
			if ($maxW==0) $maxW = $width;
			if ($maxW==0) $maxH = $height;
	
			$ratioX = $maxW/$width;
			$ratioY = $maxH/$height;
	
			if (($width<=$maxW) && ($height<=$maxH) ){ // La mida és correcta
				$newWidth = $width;
				$newHeight = $height;
			} else if (($ratioX*$height)<$maxH) { // Escalem a X
				$newHeight = ceil($ratioX * $height);
				$newWidth = $maxW;
			} else { // Escalem a Y
				$newWidth = ceil($ratioY * $width);
				$newHeight = $maxH;
			}
			
			// Redimensiona imatge conservant proporcions
			// Si és GIF o PNG conserva transparències
			if ($tipus==IMAGETYPE_GIF) {
				$imgRes = imagecreatetruecolor($newWidth, $newHeight);
				setTransparency($imgRes, $im2);
				imagecopyresampled($imgRes, $im2, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
			}
			elseif ($tipus==IMAGETYPE_PNG) {
				$imgRes = imagecreatetruecolor($newWidth, $newHeight);
				imagesavealpha($im2, true);
				imagealphablending($im2, false);
				$color = imagecolortransparent($imgRes, imagecolorallocatealpha($imgRes, 0, 0, 0, 127));
				imagefill($imgRes, 0, 0, $color);
				imagesavealpha($imgRes, true);
				imagecopyresampled($imgRes, $im2, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
			}
			else {
				$imgRes = imagecreatetruecolor($newWidth, $newHeight);
				imagecopyresampled($imgRes, $im2, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
			}
	
			imagedestroy($im2);
			$img = $imgRes;
	
	
			// Si s'ha tractat les mides o la qualitat és inferior a 100
			// obté la imatge tractada
			ob_start();
			if ($tipus==IMAGETYPE_GIF) {
				// Per les imatges GIF no hi ha control sobre la qualitat
				imagegif($img);
			} else if ($tipus==IMAGETYPE_PNG) {
				$qualitat = ($qualitat - 100) / 11.111111;
				$qualitat = round(abs($qualitat));
				imagepng($img, null, $qualitat);
			}
			else if ($tipus==IMAGETYPE_JPEG) {
				imagejpeg($img, null, $qualitat);
			}
			$stream = ob_get_contents();
			ob_clean();
		}
		imagedestroy($img);
		return $stream;
	}
	
	/**
	 * @param string $stream Cadena que conté la imatge llegida de disc.
	 * @return int El tipus d'imatge codificat en la cadena $stream.
	 */
	protected function imageType($stream) {
		$tipus = -1;
		$arr = str_split(substr($stream,0,10),1);
	
		/* BYTES Llegits: 3 */
		if($arr[0] == 'G' && $arr[1] == 'I' && $arr[2] == 'F'){
			return IMAGETYPE_GIF;
		} else if(($arr[0] == chr(hexdec('0xFF'))) && ($arr[1] == chr(hexdec('0xD8'))) && ($arr[2] == chr(hexdec('0xFF')))) {
			return IMAGETYPE_JPEG;
		} else if(($arr[0] == chr(hexdec('0x89'))) && ($arr[1] == chr(hexdec('0x50'))) && ($arr[2] == chr(hexdec('0x4E'))) && ($arr[3] == chr(hexdec('0x47'))) && ($arr[4] == chr(hexdec('0x0D'))) && ($arr[5] == chr(hexdec('0x0A'))) && ($arr[6] == chr(hexdec('0x1A'))) && ($arr[7] == chr(hexdec('0x0A')))) {
			return IMAGETYPE_PNG;
		} else if(($arr[0] == 'F') && ($arr[1] == 'W') && ($arr[2] == 'S')) {
			return IMAGETYPE_SWF;
		} else if(($arr[0] == 'C') && ($arr[1] == 'W') && ($arr[2] == 'S')) {
			return IMAGETYPE_SWC;
		} else if(($arr[0] == '8') && ($arr[1] == 'B') && ($arr[2] == 'P') && ($arr[3] == 'S')) {
			return IMAGETYPE_PSD;
		} else if(($arr[0] == 'B') && ($arr[1] == 'M')) {
			return IMAGETYPE_BMP;
		} else if(($arr[0] == chr(hexdec('0xFF'))) && ($arr[1] == chr(hexdec('0x4F'))) && ($arr[2] == chr(hexdec('0xFF')))) {
			return IMAGETYPE_JPC;
		} else if(($arr[0] == 'I') && ($arr[1] == 'I') && ($arr[2] == chr(hexdec('0x2A'))) && ($arr[3] == chr(hexdec('0x00')))) {
			return IMAGETYPE_TIFF_II;
		} else if(($arr[0] == 'M') && ($arr[1] == 'M') && ($arr[2] == chr(hexdec('0x00'))) && ($arr[3] == chr(hexdec('0x2A')))) {
			return IMAGETYPE_TIFF_MM;
		} else if(($arr[0] == 'F') && ($arr[1] == 'O') && ($arr[2] == 'R') && ($arr[3] == 'M')) {
			return IMAGETYPE_IFF;
		} else if(($arr[0] == chr(hexdec('0x00'))) && ($arr[1] == chr(hexdec('0x00'))) && ($arr[2] == chr(hexdec('0x01')))) {
			return IMAGETYPE_ICO;
		} else if(($arr[0] == chr(hexdec('0x00'))) && ($arr[1] == chr(hexdec('0x00'))) && ($arr[2] == chr(hexdec('0x00'))) && ($arr[3] == chr(hexdec('0x0C'))) && ($arr[4] == chr(hexdec('0x6A'))) && ($arr[5] == chr(hexdec('0x50'))) && ($arr[6] == chr(hexdec('0x20'))) && ($arr[7] == chr(hexdec('0x20'))) && ($arr[4] == chr(hexdec('0x0D'))) && ($arr[5] == chr(hexdec('0x0A'))) && ($arr[6] == chr(hexdec('0x87'))) && ($arr[7] == chr(hexdec('0x0A')))) {
			return IMAGETYPE_JP2;
		} else {
			return $tipus;
		}
	}

}