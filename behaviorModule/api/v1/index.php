<?php


use core\Environment;
use core\AI;



$_SERVER["SCRIPT_URL"] = str_replace("/api/","/behaviorModule/api/", $_SERVER["SCRIPT_URL"]);
$_SERVER["SCRIPT_URI"] = str_replace("/api/","/behaviorModule/api/", $_SERVER["SCRIPT_URI"]);
$_SERVER["REQUEST_URI"] = str_replace("/api/","/behaviorModule/api/", $_SERVER["REQUEST_URI"]);
$_SERVER["SCRIPT_NAME"] = "/behaviorModule/api/v1/index.php";
$_SERVER["PHP_SELF"] = "/behaviorModule/api/v1/index.php";

define ( "BaseDirAPI", $_SERVER["DOCUMENT_ROOT"]."/behaviorModule/api/");
define ( "BaseDir", $_SERVER ["DOCUMENT_ROOT"] );

require BaseDir.'/AI.php';
require BaseDir.'/declare.php';
// require BaseDir. '/preprocessorModule/Preprocessor.php';


global $setup;
AI::getInstance($setup)->init();
define("FILEPATH", $setup->repository);

/**
 * Script which contains the environment configuration
 */
require BaseDirAPI.'/v1/declare.php';

/**
 * include Environment
 * Class Environment which sets the basic configuration of the WebService
 */
require "./core/class.Environment.php";

Environment::getInstance(); // WebService Dependences and Inicialization.
Environment::route(); // Route to request.
?>