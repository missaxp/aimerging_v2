<?php
// Reporta tots els errors de PHP
ini_set ( 'log_errors', false );
ini_set ( 'display_errors', true );
ini_set ( 'error_reporting', E_ALL );
// error_reporting(E_ALL);

// Comprova que la versió del PHP sigui la 5
if (0 > version_compare ( PHP_VERSION, '5' )) {
	die ( 'WebService only works with php version 5 or higher!' );
}
// Estableix la zona horaria
date_default_timezone_set ('UTC');


define ( "apiVersion", "/v1" );


require BaseDirAPI . apiVersion . '/libs/funcions.php';
require BaseDirAPI . '/libs/Slim/Slim.php';
require BaseDirAPI . apiVersion . '/core/class.AppException.php';
require BaseDirAPI . apiVersion . '/core/webservice/http/class.Status.php';
require BaseDirAPI . apiVersion . '/core/webservice/http/class.ErrorCode.php';

//Attach the shutdown execute function. (Always execute this function when the script ends).
register_shutdown_function("\core\Exception\AppException::fatalErrorHandler");

require BaseDirAPI.apiVersion.'/core/class.config.php';

use core\config\config;
use core\AI;

$config = new \stdClass();

$config->development = AI::getInstance()->getSetup()->development; // Sets development mode.
$config->maintenance = false; // If true, we will stop any WS transaction and we deny any new access and current access will be stopped
$config->doCommit = true; //Every INSERT,UPDATE or DELETE actions do in TRANSACTION mode. Setting this to false will help to debug sql and do not commit any transaction.
$config->starttime = AI::getInstance()->getSetup()->starttime;
$config->xmlfilename = "literals"; // i18n files
$config->webmaster_mail = AI::getInstance()->getSetup()->mail_error_to; // Error messages sent to this email
$config->error_from_mail = AI::getInstance()->getSetup()->mail_from; // Error messages from email
$config->web_name = "AI-API"; // Site Name
$config->site_mail = AI::getInstance()->getSetup()->mail_from; // Default email from address (for non error notifications)
$config->version = "v1";
$config->revision = "670";
$config->login_timeout = 20; // En minuts.
$config->token_expires = "PT120M"; // Token expires in 120 minutes. This value is DateInterval $interval PHP. More Information at: http://es1.php.net/manual/en/class.dateinterval.php
$config->DBuser = AI::getInstance()->getSetup()->db_user;
$config->DBpass = AI::getInstance()->getSetup()->db_passwd;
$config->conn_string = AI::getInstance()->getSetup()->db_host;
$config->conn_scheme = AI::getInstance()->getSetup()->db_scheme;
$config->avatarMaxWidth = 150;
$config->avatarMaxHeight = 250;
$config->avatarQuality = 90;
$config->maxRows = 500; // Max returned Rows
$config->dateTimeFormat = 'YYYYMMDD\"T\"hh24miss\"Z\"'; 
$config->timeZone = AI::getInstance()->getSetup()->timeZone; //Set server WORKING timeZone.
$config->headquartersTimeZone = "Europe/Madrid"; //Set in which timezone is the headquarters.
$config->database_type = AI::getInstance()->getSetup()->db_type;
$config->defaultContentType = "application/json"; //Default contenttype served.
$config->tmpDir = sys_get_temp_dir()."/";
if ($config->development) { // Development
	$config->mail_host = AI::getInstance()->getSetup()->mail_server; // Email server
	$config->storage = '/webs/L/';
	$config->filesDB = false; //Sets if the files are saved on DB or in HDD
	$config->documents = $_SERVER["DOCUMENT_ROOT"].'/v1/files/';
	$config->saveErrors = true; // Save errors to DB log
	$config->saveErrorStatus = "0+";
	$config->notify_error_by_email = false; // Notify by email if error happen
	$config->hide_error_information = false; // Show an error message or hides it
	$config->show_debug_trace = true;
	$config->authentication_required = true; // Authentication enabled or disabled, if disabled, full privileges are given for all users. Only available when development = true.
} else { // Production
	$config->mail_host = "cache2.idisc.es"; // Email server
	$config->storage = '/media/L/';
	$config->filesDB = false; //Sets if the files are saved on DB or in HDD
	$config->documents = $_SERVER["DOCUMENT_ROOT"].'/v1/files/';
	$config->saveErrors = true; // Save errors to DB log
	$config->saveErrorStatus = "[500-600)";
	$config->notify_error_by_email = true; // Notify by email if error
	$config->hide_error_information = false; // Show an error message or hides it
	$config->show_debug_trace = true;
	$config->authentication_required = true; // Authentication enabled or disabled, if disabled, full privileges are given for all users. Only available when development = true.
}



config::_SET($config); // Set Webservice config.
?>