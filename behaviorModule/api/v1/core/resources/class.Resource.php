<?php

namespace core\resources;

use core\Environment;
use common\Tables;
use api\crud\AttributesManagement;
use api\crud\DepartmentManagement;
use api\crud\UserManagement;
use api\crud\CountryManagement;
use api\crud\TagsManagement;
use api\crud\LanguagesManagement;
use api\crud\FileManagement;
use api\crud\GroupManagement;
use common\Table;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;
/**
 * General resources methods and properties.
 * 
 * @author phidalgo
 * @created 20160207
 */
class Resource{
	/**
	 * Current instance of environment
	 * @var Environment
	 */
	protected static $environment;
	
	private $table;
	protected $id;
	protected $table_id;
	protected $dynattr = array();
	protected $tags = array();
	protected $options;
	public $creation_ts;
	public $modification_ts;
	/**
	 * Departament d'aquest recurs.
	 * Marcarà la visibilitat d'aquest recurs en funció del departament de l'usuari (USERS.user_department).
	 * @var integer
	 */
	protected $department_id;
	/**
	 * Grup propietari del recurs.
	 * Determinarà els permisos segons el grup que pertanyi l'usuari que preten realitzar alguna acció sobre a aquest recurs.
	 * @var integer
	 */
	protected $owner_gid;
	
	/**
	 * Usuari propietari del recurs (id).
	 * Determinarà els permisos segons l'usuari que intenti accedir a aquest recurs (USERS.id).
	 * Com a usuari que tindré un grup (USERS.user_group), aquest grup tindrà una directiva de permisos per a aquest recurs.
	 * @var integer
	 */
	protected $owner_uid;
	
	/**
	 * Usuari propietari del recurs (nom)
	 * @var string
	 */
	protected $owner_uname;
	
	protected $toUpdate; //Vars sets to update this object. Once the object is DB updated, this update properties updates the main class properties.
	
	protected $new = true;
	
	/**
	 * Given a table name, gets the next valid ID from this table.
	 * @param string $table
	 */
	protected static function getNewDBId($table,$primary_key = "ID"){
		//L'ID no es genera així, però encara no s'ha parlat.
		$sql = "SELECT IFNULL[max($primary_key)+1,1] AS NID FROM ".$table;
		$rs = self::$environment->dbcon->execute($sql);
		$rs->fetch();
		$nid = $rs->nid;
		$rs->close();
		return $nid;
	}
	
	public function __construct($table_id){
		self::$environment = Environment::getInstance();
		$this->table = self::$environment->tables->_get($table_id);
		$this->toUpdate  = new \stdClass();
		
		/**
		 * Anem a comprovar si el departament de l'usuari permet visualitzar aquest registre en concret.
		 */
		if($this->id!=null && $this->table->hasDepartment && $this->id!=self::$environment->user->id){
			//Si el departament de l'usuari és null, té visibilitat de recursos de tots els departaments. Per tant no comprovem departament registre.
			if(self::$environment->user->user_department!=null){
				$resource_department_id = null;
				if($this->department_id==null){
					$department_db_name = Table::DEPARTMENT_COLUMN_NAME;
					$sql = "SELECT ".$department_db_name." FROM ".$this->table->db_name." WHERE ".$this->table->primary_key[0]."=:ID";
					$rs = self::$environment->dbcon->execute($sql,array("ID" => $this->id));
					if($rs->fetch()){
						$resource_department_id = $rs->$department_db_name;
					}
					$rs->close();
				}
				else{
					$resource_department_id = $this->department_id;
				}
				
				//Si el departament del registre és null, vol dir que és visible per a tots els departaments.
				if($resource_department_id!=null){
					//Si el departament del registre és igual al departament de l'usuari, pot veure el registre.
					if(self::$environment->user->user_department!=$resource_department_id){
						throw new AppException(Status::S4_NotFound,ErrorCode::RecordNotVisibleByDepartment);
					}
				}	
			}
		}
		
		if($this->id!=null && $this->table->hasGranularP){
			$rs = self::$environment->dbcon->execute("SELECT USERNAME FROM USERS WHERE ID=:ID",array("ID" => $this->owner_uid));
			if($rs->fetch()){
				$this->owner_uname = $rs->username;
			}
			$rs->close();
		}
	}
	
	/**
	 * Retorna totes les propietats públiques de l'objecte.
	 * En un futur, quan el tema dels rols estigui definit, s'haurà de cribar la informació en funció de les accions permeses pel rol.
	 * De moment, deixo la funció així, ja que totes les respostes fan un $class->get().
	 */
	public function get(){
		if($this->table->hasGranularP && ($this->table->id==Table::USERS && $this->id!=self::$environment->user->id || $this->table->id!=Table::USERS)){
			self::$environment->security->table($this->table->id)->access($this->id)->readable(); //En principi això ja hauria de funcionar. Si falla la comprovació return 401.
		}
		
		$rsp = array();
		$rfl = new \ReflectionObject($this);
		foreach ($rfl->getProperties(\ReflectionProperty::IS_PUBLIC) as $property){
			$propname = $property->name;
			$rsp[$propname] = $this->$propname;
		}
		
		if($this->table->hasDepartment){
			$rsp["department_id"] = $this->department_id;
		}
		
		if($this->table->hasGranularP){
			$rsp["owner_gid"] = $this->owner_gid;
			$rsp["owner_uid"] = $this->owner_uid;
			$rsp["owner_uname"] = $this->owner_uname;
		}
		
		if($this->table->hasDynAttr){
			$this->dynAttr = AttributesManagement::getAttributesValues($this->table->id,$this->id);
			$rsp["dynAttr"] = $this->dynAttr;
		}
		
		if($this->table->hasTags){
			$this->tags = TagsManagement::getTagsRelation($this->id, $this->table->id);
			$rsp["tags"] = $this->tags;
		}
		
		$this->getOptions();
		if($this->options!=null){
			$rsp["options"] = $this->options;
		}
		
		$rsp["id"] = $this->id;
		
		return $rsp;
	}
	
	/**
	 * Sets new values.
	 * 
	 * If one argument set -> array will add $key => $value foreach array element.
	 * If two arguments set -> $key => $value pairs added
	 * 
	 * Aqui passa una cosa molt rara... si trec l'argument $data falla el slim, si poso 2 arguments falla slim...
	 * @param mixed $data
	 */
	public function set($data){
		$argv = func_get_args();
		$argc = count($argv); 
		if($argc==1){
			$data = $argv[0];
			foreach($data as $prop => $value){
				$this->setToUpdate($prop, $value);
			}
		}
		elseif($argc == 2){
			$this->setToUpdate($argv[0], $argv[1]);
		}
		return $this;
	}
	
	/**
	 * Update toUpdate array
	 * @param string $prop property to update
	 * @param string $value value to update
	 */
	private function setToUpdate($prop,$value){
		if(property_exists(get_class($this),$prop)){
			$this->toUpdate->$prop = $value;
		}
		else{
			if(startsWith($prop, "dynattr_")){
				$daC = new \stdClass();
				$daC->id = str_replace("dynattr_","",$prop);
				$daC->value = $value;
				$this->toUpdate->dynAttr[] = $daC;
				unset($daC);
				
			}
			//Optionally in a new resource we could send the attachments ID uploaded before assign  a resource for this new resource.
			if(startsWith($prop, 'attachments')){
				$this->toUpdate->attachments_id = array_filter(explode(",",$value),'strlen'); //http://php.net/manual/en/function.explode.php#111650
			}
			
			if($this->table->hasSeries && $this->new && $prop=='serie_id'){
				$this->toUpdate->serie_id = $value;
			}
		}
	}
	
	/**
	 * Save to db new values.
	 *
	 * Return ID if is new resource.
	 */
	public function save(){
		$bf = (array) $this->toUpdate;
		if(!empty($bf)){ //If not empty... do db save.
			unset($bf);
			$var = array();
			$varW = array();
			foreach($this->toUpdate as $prop => $value){
				$prop = strtolower(($prop));
				if($prop!='tags' && $prop!='dynattr' && $prop!='attachments_id' && $prop!='owner_uid' && $prop!='owner_gid' && $prop!='avatar' && $prop!="serie_id"){
					$var[$prop] = $value;
				}
				if($prop=='department_id' || $prop=="user_department"){
					if($value!="ALL"){
						$var[$prop] = $value;
					}
					else{
						$var[$prop] = "";
					}
				}
				if($prop =='owner_uid'){
					$var['user_id'] = $value;
				}
				if($prop =='owner_gid'){
					$var['group_id'] = $value;
				}
			}
			
			if(count($var)>0){ //If vars is not empty... DO the action -> add or update the resource
				if($this->table->hasGranularP){ //If this table has granular permission system...
					/**
					 * Un registre acabat de crear té com a usuari propietari i grup propietari el creador del registre.
					 * Però encara no ho hem desat. Amb això hem de mirar si aquest usuari té permisos
					 * D'administració per als seus registres (pot canviar usuari propietari i grup propietari).
					 * Si té permisos d'administració: 
					 * 	USER 	C	R 	W 	D 	A
					 *	this	x	x	x	x	x
					 * Podrà canviar usuari prop i grup prop per tant hem de desar els que ens envïi des del client (si envia algo,... sino agafem el seu)
					 * 
					 * Si no té permisos d'administració:
					 * 	USER 	C	R 	W 	D 	A
					 *	this	x	x	x	x	-
					 *
					 * No mirem el que rebem del client referent a usuari_propietari i grup propiterai (si ho envia...) i posem el seu.
					 */
					if($this->new){
						self::$environment->security->table($this->table->id)->access()->creable(); //Llença throw si no té permisos.
						//Si és nou i tenim permisos d'administració, mirem si s'han enviat l'usuari i grup, si s'ha enviat es posa, sino, es posa els nostres.
						if(self::$environment->security->table($this->table->id)->check()->administrable()){
							$var["user_id"] = (isset($this->toUpdate->owner_uid)? $this->toUpdate->owner_uid:self::$environment->user->id);
							$var["group_id"] = (isset($this->toUpdate->owner_gid)? $this->toUpdate->owner_gid:self::$environment->user->user_group);
						}
						else{ //Si no tenim permisos d'administració, posem els nostres.
							$var["user_id"] = self::$environment->user->id;
							$var["group_id"] = self::$environment->user->user_group;
						}
					}
					else{
							self::$environment->security->table($this->table->id)->access($this->id)->writable();
							if(!self::$environment->security->table($this->table->id)->check($this->id)->administrable()){
								/**
								 * If $var["USER_ID"] or $var["GROUP_ID"] Exists, delete it.
								 * They should not exist, but to be sure...
								 */
								unset($var["user_id"]);
								unset($var["group_id"]);
							}
					}
				}
				
				if($this->table->hasDepartment){
					unset($var["department_id"]);
					$var["department_id"] = (self::$environment->user->user_department==null? "":self::$environment->user->user_department);
					//Només si l'usuari pertany a tots els departaments podrà canviar el departament per defecte del recurs.
					if(isset($this->toUpdate->department_id) && self::$environment->user->user_department==null){
						$var["department_id"] = ($this->toUpdate->department_id=="ALL"? "":$this->toUpdate->department_id);
					}
				}
				
				$primary_key = $this->table->primary_key[0];
				if($this->new){
					if($this->id==null){
						$sql = "SELECT nvl(max(".$primary_key.")+1,1) AS NID FROM ".$this->table->db_name;
						$rs = self::$environment->dbcon->execute($sql);
						$rs->fetch();
						$this->id = $rs->nid;
						$rs->close();
						$var[$primary_key] = $this->id;
						if($this->table->hasSeries && isset($this->toUpdate->serie_id) && $this->toUpdate->serie_id!='serie_manual'){
							$serie = null;
							foreach($this->table->series as $s){
								if($s->serie==$this->toUpdate->serie_id){
									$serie = $s;
								}
							}
							if($serie==null){
								throw new AppException(Status::S4_Conflict,ErrorCode::SerieNotFound);
							}
							$sfield = $this->table->getSerieField();
							$var[$sfield] = $serie->newValue();
							$this->toUpdate->$sfield = $var[$sfield];
							self::$environment->dbcon->add($this->table->db_name, $var);
							$serie->setValue();
						}
						else{
							self::$environment->dbcon->add($this->table->db_name, $var);
						}
					}
					else{ //En el cas que ja haguem inicialitzat el id, ...
						$var[$primary_key] = $this->id;
						self::$environment->dbcon->add($this->table->db_name, $var);
					}
				}
				else{
					$var["MODIFICATION_TS"] = self::$environment->getDate();
					$varW["ID"] = $this->id;
					
					//die(var_dump($varW));
					self::$environment->dbcon->update($this->table->db_name, $var,"$primary_key=:ID",$varW);
				}
			}
			
			//If this table has dynamic attributes...
			if($this->table->hasDynAttr){
				//Dynamic Attributes -> update it.
				if(isset($this->toUpdate->dynAttr)){
					foreach($this->toUpdate->dynAttr as $da){
						AttributesManagement::setAttributeValue($da->id, $da->value, $this->id, $this->table->id);
					}
				}	
			}
			
			//If this table has tags...
			if($this->table->hasTags){
				//Tags -> update it.
				if(isset($this->toUpdate->tags)){
					$vars["TP_ID"]= $this->id;
					$vars["TABLE_ID"] = $this->table->id;
					self::$environment->dbcon->execute("DELETE FROM TAGS_RELATION WHERE ELEMENT_ID=:TP_ID AND TABLE_ID=:TABLE_ID",$vars); //Remove all tags of this tp.
					foreach($this->toUpdate->tags as $tag){
						TagsManagement::addTagRelation($tag, $this->id, $this->table->id);
					}
				}	
			}
			
			//File relations -> update file relations.
			if(isset($this->toUpdate->attachments_id)){
				foreach($this->toUpdate->attachments_id as $att){
					FileManagement::setFileRelation($att,$this->id);
				}
			}
			
			if(isset($this->toUpdate->avatar) && $this->table->getColumn("AVATAR")!==false && $this->table->getColumn("AVATAR")->type=="BLOB"){
				self::$environment->dbcon->writeLob((string) base64_decode($this->toUpdate->avatar), "AVATAR", $this->table->db_name,$this->table->primary_key[0]."=:ID",array("ID" => $this->id));
			}
			
			//Update tp class with toUpdate data.
			foreach($this->toUpdate as $prop => $value){
				$this->$prop = $value;
			}
		}
	
		if($this->new){
			return array("id" => $this->id);
		}
	}

	/**
	 * Delete a RESOURCE.
	 * It is mandatory PROPERLY SET Foreign Keys and Primary keys on the database. Also on delete actions.
	 * DELETE statement checks for FK and return elements who has FK related element.
	 * DURING THE DEVELOPMENT PROCESS, PLEASE CHECK THAT IT'S REALLY HAPPENS.
	 */
	public function delete(){
		/**
		 * Multi Primary Key
		 */
		$pksql = "";
		$totPK = count($this->table->primary_key);
		$varsPK = array();
		for($i=0;$i<$totPK;$i++){
			$pksql = (($i==0)? "(":" AND ").(string) $this->table->primary_key[$i]."=:PK_$i".((($i+1)==$totPK)? ")":"");
			$varsPK["PK_$i"] = $this->id;
		}
		
		$exec_delete = self::$environment->dbcon->execute("DELETE FROM ".((string) $this->table->db_name)." WHERE $pksql",$varsPK);
		
		//Delete any files for this element (identified by id) in the MASTER FILE TABLE.
		$sql = "DELETE FROM MASTER_FILE WHERE TABLE_ID=:TID AND ID_REL=:IDR";
		self::$environment->dbcon->execute($sql,array("TID" => $this->table->id,"IDR" => $this->id));
		
		//Also do a physical delete of the element folder.
		$this->deleteDir(self::$environment->documents.$this->table->name."/".$this->id);
	}
	
	/**
	 * Load All available options.
	 * This method contains the basic options for all resources but it will be usually overwrite by the own method on the resource.
	 * the best way to do this is in the children class (the resource itself), create a method called getOptions and then do:
	 * parent::getOptions();
	 * 
	 *  and then your own code.
	 */
	protected function getOptions(){
		if($this->table->hasTags){
			$this->options["tags"] = TagsManagement::tagsFormatted($this->table->id);
		}
		if($this->table->hasDynAttr){
			$this->options["attributes"] = AttributesManagement::getTableAttributes($this->table->id);
		}
		
		if($this->table->hasGranularP){
			if(self::$environment->security->table($this->table->id)->check($this->id)->administrable()){
				$this->options["users"] = UserManagement::getUsers(false,array("ID","USERNAME"),false);
				$this->options["groups"] = GroupManagement::_get(false,array("ID","NAME"));
				if($this->table->id==Table::USERS){
					$this->options["departments"] = DepartmentManagement::getDepartments(false,array("ID","NAME"));
				}
			}
			else{
				//die(var_dump(self::$environment->security->table($this->table->id)->getUnixLike()));
			}
		}
		
		if(!isset($this->options["departments"]) && $this->table->hasDepartment && self::$environment->user->user_department==null){
			$this->options["departments"] = DepartmentManagement::getDepartments(false,array("ID","NAME"));
		}
	}
	
	private function deleteDir($path) {
		if (is_dir($path) === true)
	    {
	        $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path), \RecursiveIteratorIterator::CHILD_FIRST);
	
	        foreach ($files as $file)
	        {
	            if (in_array($file->getBasename(), array('.', '..')) !== true)
	            {
	                if ($file->isDir() === true)
	                {
	                    rmdir($file->getPathName());
	                }
	
	                else if (($file->isFile() === true) || ($file->isLink() === true))
	                {
	                    unlink($file->getPathname());
	                }
	            }
	        }
	
	        return rmdir($path);
	    }
	
	    else if ((is_file($path) === true) || (is_link($path) === true))
	    {
	        return unlink($path);
	    }
	
	    return false;
	}
}