<?php
namespace core;

/**
 *
 * This script has all module requires and it's also a general class of the API response routes.
 */

use core\Environment;
use core\http\Status;
use core\http\ErrorCode;
use core\Exception\AppException;


require './api/crud/class.UserManagement.php';
require './api/crud/class.DepartmentManagement.php';
require './api/crud/class.GroupManagement.php';
require './api/crud/class.RoleManagement.php';

class route {
	protected $authFunction;
	protected static $environment;
	
	/**
	 * The root URL space of the REST request.
	 *
	 * @var string
	 */
	protected $urlspace;
	
	/**
	 *
	 * @param string $urlspace        	
	 * @param boolean $needsAuth        	
	 */
	protected function __construct($urlspace, $needsAuth) {
		self::$environment = Environment::getInstance ();
		
		$this->authFunction = $needsAuth ? self::$environment->authentication : function () {
			return true;
		};
		
		$this->urlspace = $urlspace;
		/**
		 * If server is in maintenance mode, only response to OPTIONS HTTP request.
		 * Other requests returns a 503 http status.
		 */
		if (self::$environment->maintenance ()) {
			self::$environment->app->any('(:rute+)',function($route){
				$env = Environment::getInstance();
				if($env->app->request()->isOptions()){
					$env->app->response->setBody("Allow: NONE" ); //TODO: Canvi no testat, he canviat GET,POST,UPDATE per NONE 27/10/2015
				}
				else{
					$env->response->send(Status::S5_ServiceUnavailable, ErrorCode::ServerMaintenance);
				}
			});
		}
		
		/**
		 * Default OPTION return for this $urlspace
		 * url /*
		 * method OPTIONS
		 */
		self::$environment->app->options('(:rute+)', function ($rute) use ($urlspace) {
			$environment = Environment::getInstance();
			$environment->app->response()->headers()->set("Allow","GET,POST,UPDATE,DELETE");
			$environment->app->response->setBody($urlspace);
		});
	}
	
	protected function run() {
		$urlspace = $this->urlspace;
		/**
		 * Resource not found
		 */
		self::$environment->app->notFound ( function () {
			$environment = Environment::getInstance ();
			throw new AppException(Status::S4_NotFound,ErrorCode::RESTFULL_URI_NOT_FOUND);
		});
		
		//If the request method is POST (create), PUT (update) or DELETE (delete), we set DB to work in TRANSACTION MODE before start REST operations.
		if(in_array(self::$environment->request->getMethod(),array("POST","PUT","DELETE"))){
			self::$environment->dbcon->setDefaultExecMode(DBCONN_TRANSACTION);
		}
		self::$environment->app->run(); // Run SLIM.
	
		//If the request method is POST (create), PUT (update) or DELETE (delete), we do the COMMIT after finalize all the REST operations
		if(in_array(self::$environment->request->getMethod(),array("POST","PUT","DELETE")) && self::$environment->doCommit){
			self::$environment->dbcon->commit();
		}
	}
	
	/**
	 * Check required params for each request.
	 *
	 * @throws AppException
	 */
	public static function checkParams() {
		$required = func_get_args ();
		$params = self::$environment->app->request->params();
		
		
		//die(var_dump($params));
		if (self::isJson ($params)) {
			$params = json_decode($params);
		}
		
		$params = array_merge($params,self::unserialize());
		
		$error = false;
		foreach ( $required as $require ) {
			if (!isset ($params[$require])) {
				self::$environment->response->addErrorField ( $require, ErrorCode::FieldRequired );
				$error = true;
			}
		}
		if ($error) {
			throw new AppException(Status::S4_PreconditionFailed,ErrorCode::IncompleteData,'Required field(s) are missing');
		}
		return self::arrayToObject($params);
	}
	

	/**
	 * Check params formatted in .serializeArray() jQuery method.
	 */
	private static function unserialize(){
		$body = self::$environment->app->request->getBody();
		
		$rsp = array();
		if(self::isJson($body)){
			$d = json_decode($body,true);
			
			if($d==null) return $rsp;
			/*foreach($d as $arr){
				$array = array();
				die(var_dump($arr));
				foreach($arr as $key => $value){
					$array[$key] = $value;
				}
				array_push($rsp,$array);
			}*/
			return $d;
		}
		return $rsp;
	}
	
	
	private static function arrayToObject($array){
		function array_to_obj($array, &$obj){
			foreach ($array as $key => $value){
				if (is_array($value)){
					$obj->$key = new \stdClass();
					array_to_obj($value,$obj->$key);
				}
				else{
					if((strlen($value)==1 || (strlen($value)>1 && !startsWith($value, "0"))) && is_numeric($value)){ //If starts with 0 is NOT a number.
						$value =  (int) $value;
					}
					if(is_string($value) && (strToUpper($value)=="TRUE" || strToUpper($value)=="FALSE")){
						$value = (strToUpper($value)=="TRUE"? true:false);
					}
					$obj->$key = $value;
				}
			}
			return $obj;
		}
		$object = new \stdClass();
		return array_to_obj($array,$object);
	}
	
	
	private static function isJson($params) {
		if(is_array(($params))){
			return false;
		}
		try {
			$data = json_decode ( $params );
		} catch ( \Exception $e ) {
			return false;
		}
		
		return (json_last_error () == JSON_ERROR_NONE);
	}
}
?>