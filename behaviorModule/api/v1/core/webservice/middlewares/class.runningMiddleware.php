<?php
use core\Environment;

/**
 * Middleware for slimphp framework which sets a property of environment, running, to true when slim->run() is called.
 * @author phidalgo
 * @created 20151124
 *
 */
class runningMiddleware extends \Slim\Middleware
{
	public function call()
	{
		
		$this->next->call(); // Run inner middleware and application
		$env = Environment::getInstance(); // Get reference to application
		$env->running = true;
	}
}