<?php

namespace core\Middleware;

/**
 * Modify slim request before the Slim application is invoked.
 */
class getData extends \Slim\Middleware {
	
	/**
	 *
	 * @var array
	 */
	protected $contentTypes;
	
	/**
	 * Constructor
	 *
	 * @param array $settings        	
	 */
	public function __construct($settings = array()) {
		// Supported content types related to the processing functions
		$defaults = array (
				'application/x-www-form-urlencoded' => array (
						$this,
						'parseForm' 
				),
				'application/json' => array (
						$this,
						'parseJson' 
				),
				'application/xml' => array (
						$this,
						'parseXml' 
				),
				'text/xml' => array (
						$this,
						'parseXml' 
				),
				'text/csv' => array (
						$this,
						'parseCsv' 
				),
				'text/html' => array (
						$this,
						'parseHtml' 
				),
				'text/plain' => array (
						$this,
						'parseText' 
				) 
		);
		// Merge defaults content types and the settings content types in a array
		$this->contentTypes = array_merge ( $defaults, $settings );
	}
	
	/**
	 * Call
	 * Get request mediatype and parses the input into $env['slim.input'], bakuck teh original input into $env['slim.input_original']
	 */
	public function call() {
		$mediaType = $this->app->request ()->getMediaType ();
		
		// Sets default mediaType
		if (! $mediaType || $mediaType == "") {
			$mediaType = "application/json";
		}
		
		if ($mediaType) {
			$env = $this->app->environment ();
			$env ['slim.input_original'] = $env ['slim.input']; // Store original input
			$env ['slim.input'] = $this->parse ( $env ['slim.input'], $mediaType ); // Replace with our custom input parsed with related function
		}
		$this->next->call ();
	}
	
	/**
	 * Parse input
	 *
	 * This method will attempt to parse the request body
	 * based on its content type if available.
	 *
	 * @param string $input        	
	 * @param string $contentType        	
	 * @return mixed
	 */
	protected function parse($input, $contentType) {
		if (isset ( $this->contentTypes [$contentType] ) && is_callable ( $this->contentTypes [$contentType] )) {
			$result = call_user_func ( $this->contentTypes [$contentType], $input );
			if ($result) {
				return $result;
			}
		}
		
		return $input;
	}
	
	/**
	 * Parse JSON
	 *
	 * This method converts the raw JSON input
	 * into an associative array.
	 *
	 * @param string $input        	
	 * @return array|string
	 */
	protected function parseJson($input) {
		// $request_body = $this->app->request->getBody();
		// $data = json_decode($request_body);
		$data = json_decode ( $input );
		return $data;
	}
	protected function parseForm($input) {
		$request_body = json_encode ( $_REQUEST );
		$data = json_decode ( $request_body );
		
		return $data;
	}
	
	/**
	 * Parse XML
	 *
	 * This method creates a SimpleXMLElement
	 * based upon the XML input. If the SimpleXML
	 * extension is not available, the raw input
	 * will be returned unchanged.
	 *
	 * @param string $input        	
	 * @return \SimpleXMLElement|string
	 */
	protected function parseXml($input) {
		if (class_exists ( 'SimpleXMLElement' )) {
			try {
				$backup = libxml_disable_entity_loader ( true );
				$result = new \SimpleXMLElement ( $input );
				libxml_disable_entity_loader ( $backup );
				return $result;
			} catch ( \Exception $e ) {
				// Do nothing
			}
		}
		
		return $input;
	}
	
	/**
	 * Parse CSV
	 *
	 * This method parses CSV content into a numeric array
	 * containing an array of data for each CSV line.
	 *
	 * @param string $input        	
	 * @return array
	 */
	protected function parseCsv($input) {
		$temp = fopen ( 'php://memory', 'rw' );
		fwrite ( $temp, $input );
		fseek ( $temp, 0 );
		$res = array ();
		while ( ($data = fgetcsv ( $temp )) !== false ) {
			$res [] = $data;
		}
		fclose ( $temp );
		
		return $res;
	}
	
	/**
	 * Parse HTML
	 * Example from: http://www.slimframework.com/news/automatically-parse-request-by-content-type
	 */
	protected function parseHtml($input) {
		$dom = new \DOMDocument ();
		$dom->loadHTML ( $input );
		return $dom;
	}
	protected function parseText($input) {
		return $input;
	}
}
?>