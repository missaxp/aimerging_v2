<?php
use core\Environment;
class ContentLengthMiddleware extends \Slim\Middleware
{
	public function call()
	{
		$this->next->call();
		
		$env = Environment::getInstance(); // Get reference to application
		$res = $env->app->response();		

		if ($res->getStatus() == "200" && !$res->headers->has('Content-Length') && $res->getLength() > 0) {
			$res->headers->set('Content-Length', $res->getLength());
		}
	}
}
?>