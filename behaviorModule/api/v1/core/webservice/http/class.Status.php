<?php

namespace core\http;

/**
 * Classe abstracta amb els codis de status
 *
 * @author CTEMPORAL
 *        
 */
abstract class Status {
	// Correctes 2xx
	Const S2_OK = 200;
	Const S2_Created = 201;
	Const S2_Accepted = 202;
	Const S2_NonAuthoritative = 203;
	Const S2_NoContent = 204;
	Const S2_ResetContent = 205;
	Const S2_PartialContent = 206;
	
	// Redirecció 3xx
	Const S3_MultipleChoices = 300;
	Const S3_MovedPermanently = 301;
	Const S3_Found = 302;
	Const S3_SeeOther = 303;
	Const S3_NotModified = 304;
	Const S3_TemporaryRedirect = 307;
	
	// Error Client 4xx
	Const S4_BadRequest = 400;
	Const S4_Unauthorized = 401;
	Const S4_Forbidden = 403;
	Const S4_NotFound = 404;
	Const S4_MethodNotAllowed = 405;
	Const S4_NotAcceptable = 406;
	Const S4_RequestTimeout = 408;
	Const S4_Conflict = 409;
	Const S4_PreconditionFailed = 412;
	Const S4_RequestEntityTooLarge = 413;
	Const S4_UnprocessableEntity = 422;
	Const S4_Locked = 423;
	
	// Error Servidor 5xx
	Const S5_InternalServerError = 500;
	Const S5_NotImplemented = 501;
	Const S5_ServiceUnavailable = 503;
	
	/**
	 * Obté el text de l'error
	 *
	 * @param int $codi        	
	 * @return string
	 */
	public static function getMessage($codi) {
		switch ($codi) {
			case 100 :
				return "Continue";
			case 101 :
				return "Switching Protocols";
			case 200 :
				return "OK";
			case 201 :
				return "Created";
			case 202 :
				return "Accepted";
			case 203 :
				return "Non-Authoritative Information";
			case 204 :
				return "No Content";
			case 205 :
				return "Reset Content";
			case 206 :
				return "Partial Content";
			case 309 :
				return "Multiple Choices";
			case 301 :
				return "Moved Permanently";
			case 302 :
				return "Found";
			case 303 :
				return "See Other";
			case 304 :
				return "Not Modified";
			case 307 :
				return "Temporary Redirect";
			case 400 :
				return "Bad Request";
			case 401 :
				return "Unauthorized";
			case 403 :
				return "Forbidden";
			case 404 :
				return "Not Found";
			case 405 :
				return "Method Not Allowed";
			case 406 :
				return "Not Acceptable";
			case 408 :
				return "Request Timeout";
			case 409 :
				return "Conflict";
			case 412 :
				return "Precondition Failed";
			case 413 :
				return "Request Entity Too Large";
			case 422 :
				return "Unprocessable Entity";
			case 423 :
				return "Locked";
			case 500 :
				return "Internal Server Error";
			case 501 :
				return "Not Implemented";
			case 503 :
				return "Service Unavailable";
			default :
				return "Error not found";
		}
	}
}


abstract class HTTP_METHOD{
	const GET = "GET";
	const POST = "POST";
	const UPDATE = "UPDATE";
	CONST DELETE = "DELETE";
	CONST OPTIONS = "OPTIONS";
	CONST PUT = "PUT";
}
?>