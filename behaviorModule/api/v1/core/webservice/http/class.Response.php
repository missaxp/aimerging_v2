<?php

namespace core\http;

use core\Environment;
use Functions;

/**
 * Classe abstracta amb les constants del tipus de resposta
 * Adaptació classe resposta de D2WEB (però no té res a veure amb ella ja).
 * 
 * @author phidalgo && ctemporal
 * @created ??
 * @modified 20151128
 */
abstract class ResponseType {
	Const XML = "XML";
	Const HTML = "HTML";
	Const JSON = "JSON";
	Const TXT = "TXT";
}

/**
 * Classe resposta utilitzada en el manteniment
 */
class Response {
	/**
	 * Tipus de resposta: HTML, XML, JSON, TXT
	 *
	 * @var ResponseType
	 */
	public $type;
	
	/**
	 * Errors a concatenar en la sortida: [codi, camp, missatge]
	 *
	 * @var array
	 */
	public $error_fields = array ();
	
	/**
	 * Canvis de valors de camps en el retorn
	 *
	 * @var array
	 */
	public $changed_fields = array ();
	
	/**
	 * Ids dels registre borrats
	 *
	 * @var array
	 */
	public $deleted_entries = array ();
	
	/**
	 * Linies de retorn
	 *
	 * @var array
	 */
	public $lines = array ();
	
	/**
	 * Missatge que es mostrarà en el retorn
	 *
	 * @var string
	 */
	public $message = "";
	
	/**
	 * Codi de l'estat de la resposta
	 *
	 * @var int
	 */
	public $status;
	
	/**
	 * Indica si retornarà el tag error a true o false
	 *
	 * @var bool
	 */
	public $error = false;
	
	/**
	 * Codi d'error de l'aplicació
	 *
	 * @var ErrorCodis
	 */
	public $code_error = 0;
	
	/**
	 * Dades de la resposta
	 *
	 * @var array
	 */
	public $data = array ();
	
	/**
	 * If we had a paginated data ($data), we will add extra info related to the pagination.
	 *
	 * @var array
	 */
	public $paging = array ();
	public function __construct() {
		$environment = Environment::getInstance ();
		
		// Set the Response type based on Requesty Content-Type
		$contentype = $environment->defaultContentType;
		$accept = $environment->request->headers["Accept"];
		if (isset ( $environment->request )) {
			if($environment->request->getContentType()!=null){
				$contentype = $environment->request->getContentType();
			}
		}
		
		switch ($contentype) {
			case "application/x-www-form-urlencoded" :
			case "application/json" :
				$this->type = ResponseType::JSON;
				break;
			case "application/xml" :
			case "text/xml" :
				$this->type = ResponseType::XML;
				break;
			case "text/html" :
				$this->type = ResponseType::HTML;
				break;
			case "text/plain" :
				$this->type = ResponseType::TXT;
				break;
			default :
				// Sets default response type
				$this->type = ResponseType::JSON;
				break;
		}

		if($environment->request->isGet() && stripos($accept, "text/html") !== false){
			$this->type = ResponseType::HTML;
        } else if($environment->request->isGet() && stripos($accept, "application/json") !== false){
            $this->type = ResponseType::JSON;
        }
    }
	
	/**
	 * Afegeix errors de camps en la resposta.
	 * El client capturarà aquests codis per cada camp
	 * i mostrarà la informació a l'usuari.
	 *
	 * @param string $camp
	 *        	(optional) Camp en el que s'ha produït l'error
	 * @param ErrorCode $code
	 *        	(optional) Codi de l'error
	 * @param string $text
	 *        	(optional) Text amb l'error a mostrar
	 */
	public function addErrorField($camp = "", $code = "", $text = "") {
		$error = array ();
		
		$error ["field"] = $camp;
		$error ["code"] = $code;
		if (! Environment::getInstance ()->hide_error_information) {
			if ($text != "")
				$error ["message"] = $text;
			else
				$error ["message"] = ErrorCode::getMessage ( $code );
		}
		
		$this->error_fields [] = $error;
	}
	
	/**
	 * Afegeix dades en la resposta.
	 * Pot ser un string o un array amb dades de camps per la resposta.
	 *
	 * @param
	 *        	<string, array> $data
	 */
	public function addData($data) {
		$this->data [] = $data;
	}
	
	/**
	 * Id del registre a borrat.
	 * Per generar un retorn en les peticions DELETE dels registres.
	 *
	 * @param string $id        	
	 */
	public function deleteEntry($id) {
		$this->deleted_entries [] = $id;
	}
	
	/**
	 * Afegeix en la cadena de retorn els canvis realitzats sobre un camp.
	 * Per enviar al client canvis en les dades en el moment de la inserció en la BD.
	 *
	 * @param string $camp
	 *        	(optional)
	 * @param string $valor
	 *        	(optional)
	 * @param string $tipus
	 *        	(optional)
	 */
	public function addChange($camp = "", $valor = "", $tipus = "") {
		$canvi = array ();
		
		$canvi ["field"] = $camp;
		$canvi ["value"] = $text;
		$canvi ["type"] = $tipus;
		
		$this->changed_fields [] = $canvi;
	}
	
	/**
	 * Add a string message to message attribute
	 *
	 * @param string $message        	
	 */
	public function addMessage($message) {
		$this->message .= $message . "\r\n";
	}
	
	/**
	 * Realitza la resposta segons el tipus de petició
	 *
	 * @param Status $status
	 *        	(optional) Status de la resposta
	 * @param ErrorCodi $errorcodi
	 *        	(optional) Codi d'error de l'aplicacio
	 * @param string $messatge
	 *        	(optional) Error message
	 */
	public function send($status = Status::S2_OK, $errorcodi = null, $message = null) {
		$response = "";
		
		// Slim Send Disabled
		try {
			$enviroment = Environment::getInstance ();
		} catch ( Exception $e ) {
		}
		
		if (isset ( $message )) {
			$this->message = $message;
		}
		
		if (isset ( $status )) {
			$this->status = $status;
		}
		
		if (isset ( $errorcodi )) {
			$this->code_error = $errorcodi;
			if ($this->message == "")
				$this->message = ErrorCode::getMessage ( $this->code_error );
		}
		
		if ($enviroment->hide_error_information) {
			$this->message = "";
		}
		
		if ($this->code_error == 0 && $this->status >= 400) {
			$this->code_error = $this->status;
		}
		
		if ($this->status >= 400 || $this->error || $this->code_error > 0) {
			$this->error = true;
		}
		
		// Http response code
		if (isset ( $enviroment->app )) {
			$enviroment->app->status ( $this->status );
		} else {
			// Set HTTP Response
			header ( 'HTTP/1.1 ' . $this->status . ' ' . Status::getMessage ( $this->status ) );
		}
		
		// Generate response based on response type
		if ($this->type == ResponseType::XML) {
			// Estableix la resposta en format xml
			if (isset ( $enviroment->app )) {
				$enviroment->app->contentType ( 'text/xml; charset=utf-8' );
				$enviroment->app->response->headers->set ( "Cache-control", "private" );
			} else {
				header ( 'Content-Type: text/xml; charset=utf-8' );
				header ( "Cache-control: private" );
			}
			
			$xmlmiss = "";
			
			// Si el status és més gran de 400 el retorna com a error
			$xmlmiss .= "<error" . ($this->code_error > 0 ? " codi=\"" . $this->code_error . "\"" : "") . ">" . ($this->error ? "true" : "false") . "</error>\r\n";
			
			// Missatge de resposta
			if ($this->message != "") {
				$xmlmiss .= "<message><![CDATA[" . $this::XMLtreat ( $this->message ) . "]]></message>\r\n";
			}
			
			// Errors produïts per valors incorrectes en camps
			if (count ( $this->error_fields ) > 0) {
				$xmlmiss .= "<error_fields>\r\n";
				// Errors controlats de camps
				foreach ( $this->error_fields as $error ) {
					$xmlmiss .= "<fields" . ($error ["camp"] != "" ? " field=\"" . $this::treatXMLAttribute ( $error ["camp"] ) . "\"" : "") . "><![CDATA[" . $this::XMLtreat ( $error ["missatge"] ) . "]]></fields>\r\n";
				}
				$xmlmiss .= "</error_fields>\r\n";
			}
			
			// Camps que s'han modificat en el ws
			if (count ( $this->changed_fields ) > 0) {
				$xmlmiss .= "<changed_fields>\r\n";
				// Notifica de canvis en camps, per tal que es modifiquin en el formulari
				foreach ( $this->changed_fields as $canvi ) {
					$xmlmiss .= "<fields";
					$xmlmiss .= " name=\"" . $this::treatXMLAttribute ( $canvi ["camp"] ) . "\"";
					$xmlmiss .= " type=\"" . $this::treatXMLAttribute ( $canvi ["tipus"] ) . "\"";
					$xmlmiss .= "><![CDATA[";
					$xmlmiss .= $this::XMLtreat ( $canvi ["missatge"] );
					$xmlmiss .= "]]></fields>\r\n";
				}
				$xmlmiss .= "</changed_fields>\r\n";
			}
			
			
			// Registres eliminats en ws
			if (count ( $this->deleted_entries ) > 0) {
				$xmlmiss .= "<deleted_entries>\r\n";
				foreach ( $this->deleted_entries as $registre ) {
					$xmlmiss .= "<entry id=\"" . $this::treatXMLAttribute ( $registre ) . "\" \>\r\n";
				}
				$xmlmiss .= "</deleted_entries>\r\n";
			}
			
			// Dades de retorn
			if (count ( $this->data ) > 0) {
				$xmlmiss .= "<data>\r\n";
				// Cada registre en dades haurà d'estar formatat en XML
				$xmlmiss .= self::recourseXMLdata ( $this->data );
				/*
				 * foreach ($this->dades as $registre) {
				 * if (is_array($registre)) {
				 * foreach ($registre as $key=>$value) {
				 * $xmlmiss .= "<".$key."><![CDATA[".$this::XMLtreat($value)."]]></".$key.">\r\n";
				 * }
				 * } else {
				 * $xmlmiss .= "<![CDATA[".$this::XMLtreat($registre)."]]>\r\n";
				 * }
				 * }
				 */
				$xmlmiss .= "<\data>\r\n";
			}
			
			if (count ( $this->paging ) > 0) {
				$xmlmiss .= "<paging>\r\n";
				$xmlmiss .= $this::dadesrecorreXML ( $this->paging );
			}
			
			// Assign XML code to response
			$response = $this::sendXML ( $xmlmiss );
		}
		else if ($this->type == ResponseType::TXT) {
			
			// Estableix la resposta en format TXT
			if (isset ( $enviroment->app )) {
				$enviroment->app->contentType ( 'text/plain' );
			} else {
				header ( 'Content-Type: text/plain; charset=utf-8' );
			}
			
			$htmlmiss = "";
			
			// Si el status és més gran de 400 el retorn com a error
			$htmlmiss .= "ERROR:" . ($this->error ? "true" : "false") . "\r\n";
			if ($this->code_error > 0) {
				$htmlmiss .= "CODI ERROR:" . $this->code_error . "\r\n";
			}
			
			// Missatge de resposta
			if ($this->message != "") {
				$htmlmiss .= "" . $this->message . "\r\n";
			}
			
			// Errors produïts per valors incorrectes en camps
			if (count ( $this->error_fields ) > 0) {
				$htmlmiss .= "Error Fields\r\n";
				// Errors controlats de camps
				foreach ( $this->error_fields as $error ) {
					$htmlmiss .= "camp: " . $error ["camp"] . ", " . $error ["missatge"] . "\r\n";
				}
				$htmlmiss .= "\r\n";
			}
			
			// Camps que s'han modificat en el ws
			if (count ( $this->changed_fields ) > 0) {
				$htmlmiss .= "Camps Canvis\r\n";
				// Notifica de canvis en camps, per tal que es modifiquin en el formulari
				foreach ( $this->changed_fields as $canvi ) {
					$htmlmiss .= "camp: " . $error ["camp"] . ", tipus: " . $canvi ["tipus"] . ", " . $error ["missatge"] . "\r\n";
				}
				$htmlmiss .= "\r\n";
			}
			
			// Registres eliminats en ws
			if (count ( $this->deleted_entries ) > 0) {
				$htmlmiss .= "Registres Borrats\r\n";
				foreach ( $this->deleted_entries as $registre ) {
					$htmlmiss .= "id: " . $registre . "\r\n";
				}
				$htmlmiss .= "\r\n";
			}
			
			if (count ( $this->data ) > 0) {
				$htmlmiss .= "Dades\r\n";
				$htmlmiss .= $this::recourseTXTdata ( $this->data );
				$htmlmiss .= "\r\n";
			}
			
			if (count ( $this->paging ) > 0) {
				$htmlmiss .= "Paging\r\n";
				$htmlmiss .= $this::recourseTXTdata ( $this->paging );
				$htmlmiss .= "\r\n";
			}
			
			// Retorna les dades
			$response = $htmlmiss;
		}
		else if ($this->type == ResponseType::HTML) {
			
			// Estableix la resposta en format HTML
			if (isset ( $enviroment->app )) {
				$enviroment->app->contentType ( 'text/html; charset=utf-8' );
			} else {
				header ( 'Content-Type: text/html; charset=utf-8' );
			}
			
			$htmlmiss = "";
			
			// Retorna el codi derror
			if ($this->error) {
				$htmlmiss .= "<h3>ERROR"  . ($this->code_error > 0 ? " with code:" . $this->code_error : "") . "</h3>\r\n";
			}
			
			// Missatge de resposta
			if ($this->message != "") {
				$htmlmiss .= "<p>" . $this->message . "</p>\r\n";
			}
			
			// Errors produïts per valors incorrectes en camps
			if (count ( $this->error_fields ) > 0) {
				$htmlmiss .= "<h2>Error Fields</h2>\r\n";
				// Errors controlats de camps
				foreach ( $this->error_fields as $error ) {
					$htmlmiss .= "<p>camp: " . $error ["camp"] . ", " . $error ["missatge"] . "</p>\r\n";
				}
				$htmlmiss .= "<br />\r\n";
			}
			
			// Camps que s'han modificat en el ws
			if (count ( $this->changed_fields ) > 0) {
				$htmlmiss .= "<h2>Changed Fields</h2>\r\n";
				// Notifica de canvis en camps, per tal que es modifiquin en el formulari
				foreach ( $this->changed_fields as $canvi ) {
					$htmlmiss .= "<p>camp: " . $error ["camp"] . ", tipus: " . $canvi ["tipus"] . ", " . $error ["missatge"] . "</p>\r\n";
				}
				$htmlmiss .= "<br />\r\n";
			}
			
			// Registres eliminats en ws
			if (count ( $this->deleted_entries ) > 0) {
				$htmlmiss .= "<h2>Deleted Entries</h2>\r\n";
				foreach ( $this->deleted_entries as $registre ) {
					$htmlmiss .= "<p>id: " . $this::treatXMLAttribute ( $registre ) . "</p>\r\n";
				}
				$htmlmiss .= "<br />\r\n";
			}
			
			// Dades de retorn
			if (count ( $this->data ) > 0) {
				//Si no hay errorres no tiene caso mostrar un false, en la linea if ($this->error) {
//				$htmlmiss .= "<h2>Error</h2>\r\n";
//				$htmlmiss .= ($this->error? "true":"false")."\r\n";
				if(!empty($this->data)){
					$htmlmiss .= "<h2>Data</h2>\r\n<ul>";
					foreach ( $this->data as $renderable){
						if(!empty($renderable)){
							$htmlmiss .= self::renderHTMLElement($renderable);
						}
					}
					$htmlmiss .= "</ul>\r\n";
				}
			}
			
			// Paging return
			if (count ( $this->paging ) > 0) {
				// $htmlmiss .= "<h2>Paging</h2>\r\n";
				foreach ( $this->paging as $registre ) {
					$htmlmiss .= "<p>" . $registre . "</p>\r\n";
				}
				$htmlmiss .= "<br />\r\n";
			}
			
			// Retorna les dades
			$response = "<!DOCTYPE html><html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></head><body>";
			$response .= $htmlmiss;
			$response .= "</body></html>";
		} else if ($this->type == ResponseType::JSON) {
			$response = array ();
			
			// Estableix la resposta en format json
			if (isset ( $enviroment->app )) {
				$enviroment->app->contentType ( 'application/json' );
			} else {
				header ( 'Content-Type: application/json; charset=utf-8' );
			}
			
			// Error
			$response ["error"] = $this->error;
			
			if ($this->code_error > 0) {
				$response ["error_code"] = $this->code_error;
			}
			
			if ($this->message != "") {
				$response ["message"] = $this->message;
			}
			
			if (count ( $this->error_fields ) > 0)
				$response ["error_fields"] = $this->error_fields;
			
			if (count ( $this->deleted_entries ) > 0)
				$response ["deleted_entries"] = $this->deleted_entries;
			
			if (count ( $this->changed_fields ) > 0)
				$response ["changed_fields"] = $this->changed_fields;
			
			if (count ( $this->paging ) > 0) {
				if (count ( $this->paging ) == 1) {
					$response ["data"] = $this->paging [0];
				} else {
					$response ["data"] = $this->paging;
				}
			}
			
			if (count ( $this->data ) > 0) {
				if (count ( $this->data ) == 1) {
					if (count ( $this->paging ) > 0) {
						$response ["data"] ["results"] = $this->data [0];
					} else {
						$response ["data"] = $this->data [0];
					}
				} else {
					if (count ( $this->paging ) > 0) {
						$response ["data"] ["results"] = $this->data;
					} else {
						$response ["data"] = $this->data;
					}
				}
			}
			
			$response["ellapsed_time"] = (microtime(true) - $enviroment->starttime);
			
			$response = array_change_key_case_recursive($response, CASE_LOWER);
			// Convert array to json for response
			$response = @json_encode ($response);
		}

		echo $response;
	}
	
	/**
	 * Retorna la cadena XML en el buffer de sortida
	 *
	 * @param string $xmlText
	 *        	Cadena XML a enviar
	 * @param bool $no_root
	 *        	(optional) (default false) Si s'indica com a true no encapsula amb el tag root el contingut
	 */
	public static function sendXML($xmlText, $no_root = false) {
		$resposta = "";
		
		$resposta .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";
		$resposta .= ($no_root ? "" : "<response>\r\n");
		$resposta .= $xmlText;
		$resposta .= ($no_root ? "" : "</response>\r\n");
		
		return $resposta;
	}
	public static function recourseXMLdata($dades) {
		$xmlmiss = "";
		
		if (is_array ( $dades ) || is_object($dades)) {
			if(count($dades)>0){
				foreach ($dades as $key => $value) {
					$xmlmiss .= "<" . $key . ">" . self::recourseXMLdata ($value) . "</" . $key . ">\r\n";
				}
			}
		}
		if(is_string($dades) || is_numeric($dades)){
			$xmlmiss .= "<![CDATA[" . self::XMLtreat ( $dades ) . "]]>\r\n";
		}
		if(is_bool($dades)){
			$xmlmiss .= "<![CDATA[".self::XMLtreat ( ($dades? "true":"false"))."]]>\r\n";
		}
	
		return $xmlmiss;
	}
	public static function recourseTXTdata($dades) {
		$txtmiss = "";
		
		if (is_array ( $dades )|| is_object($dades)) {
			if(count($dades)>0){
				foreach ( $dades as $key => $value ) {
					$txtmiss .= $key . "=> " . self::recourseTXTdata ( $value ) . "\r\n";
				}	
			}
			
		}
		if(is_string($dades)){
			$txtmiss .= self::XMLtreat ( $dades );
		}
		if(is_bool($dades)){
			$txtmiss .= self::XMLtreat ( $dades? "true":"false" );
		}
		
		return $txtmiss;
	}
	
	/**
	 * Pren un text formatat en xml i fa les conversions necessàries per a que es mostri correctament.
	 *
	 * @param string $text
	 *        	El text xml a convertir.
	 * @return string La cadena de text xml amb les conversions.
	 */
	public static function XMLtreat($text) {
		$retorn = trim ( $text );
		$retorn = str_replace ( '&acute;', '\'', $retorn );
		// $retorn = str_replace('<', '&lt;', $retorn);
		// $retorn = str_replace('>', '&gt;', $retorn);
		
		return $retorn;
	}
	
	/**
	 * Formata un text per poder utilizar-lo en un atribut XML
	 *
	 * @param string $text        	
	 * @return string
	 */
	public static function treatXMLAttribute($text) {
		// En els atributs XML només accepta les entitats html:
		// &amp;, &quot;, &lt;, &gt;, &apos;
		
		// Obté la representació real dels caràcters html
		$retorn = html_entity_decode ( $text, 0, "UTF-8" );
		
		// Codifica els & per &amp;
		$retorn = str_replace ( '&', '&amp;', $retorn );
		
		// Substitueix els > i < per &gt; i &lt;
		$retorn = str_replace ( '<', '&lt;', $retorn );
		$retorn = str_replace ( '>', '&gt;', $retorn );
		
		// Substitueix els " per &quot;
		$retorn = str_replace ( '"', '&quot;', $retorn );
		
		return $retorn;
	}
	
	private static function renderHTMLElement($renderable){
		$return = "";
		if(is_array($renderable)){
			if(count($renderable) > 0){
				$return .= "<ul>";
				foreach($renderable as $prop => $value){
					$return .='<li><span style="color:#7F0055">"'.$prop.'"</span>: '.self::renderHTMLElement(json_decode(json_encode($value), true))."</li>\r\n";
				}
				$return .= "</ul>";
			}
			else{
				$return .= "array()";
			}
			
		} else if (is_object($renderable)) {
			$return .= self::renderHTMLElement(get_object_vars($renderable));
		}
		else if(is_string($renderable)){
			$return .= '<span style="color:#8B75FF">"'.$renderable.'"</span>';
		}
		else if(is_numeric($renderable)){
			$return .= '<span style="color:#00008B">'.$renderable.'</span>';
		}
		else if(is_bool($renderable)){
			$return .= $renderable? "true":"false";
		}
		else if(is_null($renderable)){
			$return .= "null";
		} else{
			$return .= "Undefinded type";
		}
		return $return;
	}
	
	/**
	 * En cas de XML crea el tag per indicar el logout i realitza l'enviament finalitzant l'execució de la pàgina.
	 *
	 * En cas contrari realitza una redirecció.
	 *
	 * @param string $param
	 *        	La url on es redireccionarà
	 * @param bool $tanca
	 *        	Per finestres emergents, indica si tanca l'actual
	 */
	public function logout($param = "", $tanca = false) {
		/*
		 * TODO
		 * if (!$tanca) {
		 * if ($param<>"") {
		 * $param = "pag=".urlencode($param);
		 * }
		 *
		 * $url = Resposta::$url_logout.($param<>""?"&".$param:"");
		 * } else {
		 * // En les finestres emergents, redirecciona a la mateixa pàgina
		 * // afegint el paràmetre tanca=S
		 * if ($param!="")
		 * $url = $param;
		 * else
		 * $url = $_SERVER["SCRIPT_URL"];
		 * $url .= (strpos($url, "?")===false?"?":"&");
		 * $url .= "tanca=S";
		 * }
		 *
		 *
		 * // Petició HTML d'ajax
		 * if ($this->type==ResponseType::JSON) {
		 * header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
		 * exit(0);
		 *
		 * // Petició XML
		 * } else if ($this->type==ResponseType::XML) {
		 * Resposta::enviaXML("<PHP_Estat>Error</PHP_Estat>\r\n<PHP_Logout>true</PHP_Logout><LogoutURL><![CDATA[".$this::XMLtreat($url)."]]></LogoutURL>");
		 *
		 * } else {
		 * header("Location: ".$url);
		 * die();
		 * }
		 */
	}
}
?>
