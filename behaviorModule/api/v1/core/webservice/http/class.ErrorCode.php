<?php

namespace core\http;

/**
 * Classe abstracta amb els codis d'error de l'aplicació
 *
 * @author CTEMPORAL
 *        
 */
abstract class ErrorCode {
	// Error Client 4xx
	Const BadRequest = 400;
	Const Unauthorized = 401;
	Const Forbidden = 403;
	Const NotFound = 404;
	Const MethodNotAllowed = 405;
	Const NotAcceptable = 406;
	Const RequestTimeout = 408;
	Const PreconditionFailed = 409;
	Const RequestEntityTooLarge = 413;
	Const UnprocessableEntity = 422;
	Const Locked = 423;
	Const SessionExpired = 450;
	Const UnknownIssuer = 451;
	Const SessionTimeout = 452;
	Const InvalidToken = 453;
	Const RESTFULL_URI_NOT_FOUND = 454;
	Const DataAuthenticityViolated = 455;
	
	// Error Servidor 5xx
	Const InternalServerError = 500;
	Const NotImplemented = 501;
	Const ServiceUnavailable = 503;
	Const DBConnectionProblem = 504;
	Const DBQueryError = 505;
	Const ServerMaintenance = 512;
	
	/**
	 * Erros PHP
	 */
	/*
	 * Fatal run-time errors.
	 * These indicate errors that can not be recovered from, such as a memory allocation problem.
	 * Execution of the script is halted.
	 */
	Const PHP_E_ERROR = 506;
	/*
	 * Fatal errors that occur during PHP's initial startup.
	 * This is like an	E_ERROR, except it is generated by the core of PHP.
	 */
	Const PHP_E_CORE_ERROR = 507;
	/*
	 * Fatal compile-time errors.
	 * This is like an E_ERROR, except it is generated by the Zend Scripting Engine.
	 */
	Const PHP_E_COMPILE_ERROR = 508;
	
	/*
	 * User-generated error message.
	 * This is like an E_ERROR, except it is generated in PHP code by using the PHP function trigger_error.
	 */
	Const PHP_E_USER_ERROR = 509;
	
	/*
	 * Compile-time parse errors.
	 * Parse errors should only be generated by the parser.
	 */
	const PHP_E_PARSE = 510;
	
	/*
	 * Can not generate a new token, or token is null.
	 */
	const TokenError = 511;
	
	
	
	/**
	 * Fi errors PHP
	 */
	Const noData = 600;
	Const UserAlreadyExists = 601;
	Const IncompleteData = 602;
	Const TokenNotFound = 603;
	Const CanNotDelete = 604;
	Const IncorrectUserPassword = 605;
	Const KICKYOURSELF = 606;
	Const FieldWrongId = 609;
	Const FieldRequired = 610;
	Const FieldWrong = 611;
	Const FieldMustbeNumber = 612;
	Const ResourceNotFound = 613;
	
	Const Q_FIELD_NAME_FOUND = 614;
	Const Q_FILTER_FOUND = 615;
	Const Q_ORDER_FOUND = 616;
	Const Q_DEFAULT_ERROR = 617;
	Const Q_I_TABLE_NOT_FOUND = 618;
	Const Q_FILTER_MANUAL = 619;
	
	/**
	 * Upload File ERROR
	 */
	Const FILE_LOST_WHEN_UPLOADING = 620;
	Const FILE_1 = 621;
	Const FILE_2 = 622;
	Const FILE_3 = 623;
	Const FILE_4 = 624;
	Const FILE_6 = 625;
	Const FILE_7 = 626;
	Const FILE_8 = 627;
	Const FILE_ABORT = 628;
	Const FILE_MAX_NUMBER_OF_FILES = 629;
	Const FILE_ACCEPT_FILE_TYPES = 630;
	Const FILE_MIN_FILE_SIZE = 631;
	Const FILE_MAX_FILE_SIZE = 632;
	Const FILE_POST_MAX_SIZE = 633;
	Const FILE_DEFAULT_ERROR = 634;
	
	Const LengthError = 640;
	Const T_NOT_FOUND = 641;
	Const TABLE_PK_NOT_FOUND = 642;
	
	// Errors de department i departmentManagement:
	Const DepartmentNotAnObject = 700;
	Const DepartmentIdNotValid = 701;
	Const DepartmentNotFound = 702;
	
	Const RecordNotVisibleByDepartment = 800;
	
	Const ParentProjectNotFound = 801;
	Const SerieNotFound = 802;
	
	/**
	 * ORA
	 */
	Const DB_FK_CHILD_RECORD_FOUND = 2292;
	
	/**
	 * AI ERRORS
	 */
	Const CONFIRM_EMAIL_TOKEN_NOT_FOUND = 3000;
	Const AI_ONDEMAND_EXECUTING = 3001;
	Const AI_ONDEMAND_EXEUCTION_ERROR = 3002;
	/**
	 * Obté el text de l'error
	 * 
	 * TODO -> S'ha de diferenciar entre RESTFULL URI NOT FOUND i RESOURCE NOT FOUND.
	 *
	 * @param int $codi        	
	 * @return string
	 * @created ??
	 * @modified 20151127
	 */
	public static function getMessage($code) {
		switch ($code) {
			case 400 :
				return "Bad Request";
			case 401 :
				return "Unauthorized";
			case 403 :
				return "Forbidden";
			case 404 :
				return "Resource Not found";
			case 405 :
				return "Method Not Allowed";
			case 406 :
				return "Not Acceptable";
			case 408 :
				return "Request Timeout";
			case 409 :
				return "Conflict";
			case 413 :
				return "Request Entity Too Large";
			case 422 :
				return "Unprocessable Entity";
			case 423 :
				return "Locked";
			case 450 : // D'aqui fins al 453 son errors d'usuari, però no se si han d'anar aqui...
				return "Session Expired, token is no more longer valid.";
			case 451 :
				return "Unknown Issuer, token is valid, but is send by another source.";
			case 452 :
				return "Session Timeout, token is no longer valid";
			case 453 :
				return "Invalid Token, client send a token, but this token does not exists (maybe it was deleted?)";
			case 454 : 
				return "REST FULL URI Not Found";
			case self::DataAuthenticityViolated:
				return "The private key used to check the data authenticity is not valid"; 
			case 500 :
				return "Internal Server Error";
			case 501 :
				return "Not Implemented";
			case 503 :
				return "Service Unavailable";
			case 504 :
				return "Database Unkown error";
			case 505 :
				return "Database Query Error";
			case 506 :
				return "PHP E_ERROR";
			case 507 :
				return "PHP E_CORE_ERROR";
			case 508 :
				return "PHP E_COMPILE_ERROR";
			case 509 :
				return "PHP E_USER_ERROR";
			case 510 :
				return "PHP E_PARSE";
			case 511 :
				return "Can not create a new token.";
			case 512 :
				return "Service in maintenance";
			
			case 600 :
				return "No data found";
			case 601 :
				return "User already exists";
			case 603 :
				return "Api key is misssing";
			case 604 :
				return "Can not delete this entry";
			case 605 :
				return "Login failed. Incorrect credentials";
			case 606 :
				return "You can not kick your self, dummy action!!";
			case 609 :
				return "Wrong Id";
			case 610 :
				return "Required Field";
			case 611 :
				return "Wrong parameter";
			case 612 :
				return "Must be a number";
			case 613 :
				return "Resource not found";
			case 614 :
				return "This field is already added to SQL query sentence.";
			case 615 :
				return "This filter field is already added to SQL query sentence";
			case 616 :
				return "This order field is already added to SQL query sentence";
			case 618 :
				return "Table not found";		
			case 619 :
				return "filter::manual argument has to be a function";
			case 620:
				return "The file is upload successfully but I can not track it on a DB. I've lost the file.";
			case 621:
				return 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
			case 622:
				return 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
			case 623:
				return 'The uploaded file was only partially uploaded';
			case 624:
				return 'No file was uploaded';
			case 625:  
				return 'Missing a temporary folder';
			case 626:
				return 'Failed to write file to disk';
			case 627: 
				return 'A PHP extension stopped the file upload';
			case 628:
				return 'File upload aborted';
			case 629:
				return 'Maximum number of files exceeded';
			case 630: 
				return 'Filetype not allowed';
			case 631:
				return 'File is too small';
			case 632:
				return 'File is too big';
			case 633:
				return 'The uploaded file exceeds the post_max_size directive in php.ini';
			case 634:
				return 'A error ocurred when uploading file.';
			case 640 :
				return "Max field length error";	
			case 641:
				return "Table not found. I can not initializate Resource class";
			case self::TABLE_PK_NOT_FOUND:
				return "The table does not have a primary key.";
			case 700 :
				return "Department must be a Department object.";
			case 701 :
				return "Error \$mixed_id does not have the right format (must be an id or  string name.";
			case 702 :
				return "No department found.";
			case self::RecordNotVisibleByDepartment:
				return "You want to see a record which is not visible due department settings";
			case self::ParentProjectNotFound:
				return "Parent Project not found. I can not assign this subproject to a non existing project";
			case self::SerieNotFound:
				return "Serie not found.";
			case self::DB_FK_CHILD_RECORD_FOUND:
				return "Integrity Constraint key violated. Child record found";
			default :
				return $code;
		}
	}
}
?>