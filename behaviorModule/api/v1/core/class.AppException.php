<?php

namespace core\Exception;

use core\Environment;
use core\http\Status;
use core\http\ErrorCode;
use api\crud\ErrorManagement;
use core\http\Response;
use core\config\config;
use core\Exception;
use common\Table;

/**
 * AppException
 *
 * Extends \Exception for App control, with our own errorcode.
 *
 * This class is environment indepentent. So if there is any errors on any classes, it will not crash inside.
 *
 * @author : Phidalgo
 *        
 */
class AppException extends \Exception {
	private $errorcode;
	protected static $environment;
	protected static $config;
	
	/**
	 * Contructor
	 *
	 * @param string $message
	 *        	Message for exception
	 * @param core\http\Status $statuscode
	 *        	Response status error code for the exception
	 * @param core\http\ErrorCode $errorcode
	 *        	Application error code for the exception
	 * @param string $previous        	
	 * @param string $file        	
	 * @param number $line        	
	 */
	public function __construct($statuscode = Status::S5_InternalServerError, $errorcode = null, $message = null, $previous = null, $file = null, $line = null,$sql = null,$type = 'API',$vars = null,$dbtracking = null) {
		if ($previous == - 1) {
			parent::__construct ( $message, $statuscode, null );
		} else {
			parent::__construct ( $message, $statuscode, $previous );
		}
		
		$file = ($file != null ? $file : $this->getFile ());
		$line = ($line != null ? $line : $this->getLine ());
		$trace = debug_backtrace();
		
		$criticalError = ((isset ( $trace ["function"] ) && $trace ["function"] == "fatalErrorHandler") ? true : false); // És error crític si prové de la funció fatalErrorHandler.
		
		$userid = '';
		$saveErrorsToDB = true;
		$hasToSave = true;
		self::$environment = null;
		self::$config = config::_GET ();
		
		
		//Check if the file which contains an error is from the API core. If is not from the core, we try to get the instance.
		if (strpos ( $file, '/core/' )!== false && strpos($file,'class.route.php')===false) {
			$criticalError = true;
			
		} else {
			try {
				self::$environment = Environment::getInstance ();
				$userid = (self::$environment->user != null ? self::$environment->user->id : '');
				$saveErrorsToDB = self::$environment->saveErrors;
				$hasToSave = $this->hasToSave ( $statuscode, self::$environment->saveErrorStatus );
				if(in_array(self::$environment->request->getMethod(),array("POST","PUT","DELETE"))){
					self::$environment->dbcon->rollback();
				}
			} catch ( \Exception $e ) {}
		}

		// Save, if specified, the error to the DB.
		if ($saveErrorsToDB && $hasToSave || $criticalError) {
			self::saveError($statuscode, $errorcode, $message, $file, $line, $userid,$type, $this->getTrace(), $vars,$dbtracking);
		}
		
		//Send the error response, well formatted, to the request client.
		self::send($statuscode, $errorcode,$message,$file,$line,$trace,$type,$sql,$vars,$dbtracking);
	}
	
	
	private static function send($status,$errorcode,$message,$file,$line,$trace,$type,$sql = null,$vars = null,$dbtracking = null){
		self::$config = config::_GET ();
		
		$data = array();
		if(self::$config->show_debug_trace){
			if ($message != null) {
				$data ["custom_message"] = $message;
			}
			
			if($sql != null){
				$data["sql"] = $sql;
				if($vars!=null){
					$data["vars"] = $vars;
				}
			}
			$data ["throw"] = array (
					"file" => $file,
					"line" => $line
			);
			//$dev ["trace"] = isset ( $trace ) ? $trace : array ();
		}
		
		
		
		if($errorcode==ErrorCode::DB_FK_CHILD_RECORD_FOUND && $dbtracking!=null){
			$data["track"] = $dbtracking;
		}
		
		if (isset ( $_SERVER ["HTTP_HOST"] )) {
			ob_clean ();
		}
		if (self::$environment == null) { // En el cas que no hi hagi Environment, creem nosaltres la resposta.
			header ( 'HTTP/1.1 ' . $status . ' ' . Status::getMessage ( $status ) );
			header ( 'Content-Type: application/json; charset=utf-8' );
			$rsp = array ();
			$rsp ["error"] = true;
			$rsp ["error_code"] = $errorcode;
			$rsp ["message"] = ErrorCode::getMessage ( $errorcode );
			if (count($data)>0) {
				$rsp ["data"] = $data;
			}
			echo json_encode ( $rsp );
			die ();
		} else { // If self::$environment exists, try to send it using $env.
			if (count($data)>0) {
				self::$environment->response->addData( $data );
			}
			self::$environment->response->send ($status,$errorcode);
			if(self::$environment->running){ //Error thrown after run. Run calls the middlewares (routes files), so the error/throw exception is on api folder.
				self::$environment->app->stop();
			}
			else{ //The error is thrown before run. It should be a PHP error (class not found, method not found,...bad configuration, syntax error...).
				header ( 'HTTP/1.1 ' . $status . ' ' . Status::getMessage ( $status ) );
				header ( 'Content-Type: application/json; charset=utf-8' );
				die();
			}
		}
	}
	
	private static function saveError($status,$errorcode,$message, $file,$line,$userid,$type,$trace = null, $vars=null,$dbtracking = null){
		require_once BaseDirAPI . apiVersion . '/api/crud/class.ErrorManagement.php';
			
		$restURL = '['.(isset ($_SERVER['REQUEST_METHOD'])? $_SERVER['REQUEST_METHOD']:"")."] ";
		$restURL .= isset ( $_SERVER ['REQUEST_URI'] ) ? $_SERVER ['REQUEST_URI'] : '';
			
		$error = new \stdClass();
		$error->code = $status;
		$error->message = ErrorCode::getMessage($errorcode);
		$error->customMessage = $message;
		$error->file = $file;
		$error->line = $line;
		$error->column = 0;
		$error->navigator = isset($_SERVER['HTTP_USER_AGENT'])? $_SERVER ['HTTP_USER_AGENT']:'';
		$error->userid = $userid;
		$error->type = $type;
		$error->php_trace = isset($trace)? json_encode($trace):'';
		$error->rest_url = $restURL;
		ErrorManagement::register($error); // Enviament d'errors
		
	}
	
	/**
	 * Check if current status has to be saved as an error to the DataBase.
	 *
	 * @param integer $status        	
	 * @param string $interval        	
	 * @return boolean
	 */
	private static function hasToSave($status, $interval) {
		$first = substr ( $interval, 0, 1 );
		$last = substr ( $interval, - 1 );
		$i = strpos ( $interval, "-" );
		
		$allfrom = strpos ( $interval, "+" );
		if ($allfrom !== false) {
			$interval = str_replace ( "[", "", $interval );
			$interval = str_replace ( "]", "", $interval );
			$interval = str_replace ( "(", "", $interval );
			$interval = str_replace ( ")", "", $interval );
			$from = substr ( $interval, 0, - 1 );
			if ($status >= $from) {
				return true;
			} else {
				return false;
			}
		}
		
		if ($i === false) {
			$from = substr ( $interval, 1, - 1 );
			$to = - 1;
		} else {
			$from = substr ( $interval, 1, $i - 1 );
			$to = substr ( $interval, $i + 1, strlen ( $interval ) - ($i + 1) - 1 );
		}
		
		if ($to == - 1) {
			if ($status == $from) {
				return true;
			} else {
				return false;
			}
		} else {
			$from = ($first == "(" ? ($from + 1) : $from);
			$to = ($last == ")" ? ($to - 1) : $to);
			
			if ($status >= $from && $status <= $to) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	/**
	 * Get the status code.
	 *
	 * @return s integer
	 */
	public function getStatusCode() {
		return $this->getCode ();
	}
	
	/**
	 * Sets status code
	 *
	 * @param integer $code        	
	 */
	public function setStatusCode($code) {
		$this->code = $code;
	}
	
	/**
	 * Get error code.
	 *
	 * @return s integer
	 */
	public function getErrorCode() {
		return $this->errorcode;
	}
	
	/**
	 * Sets error code.
	 *
	 * @param integer $code        	
	 */
	public function setErrorCode($code) {
		$this->errorcode = $code;
	}
	
	/**
	 * This function is set as a php register_shutdown_function().
	 * Called at the end of each script. If there is any error on error_get_last(), it calls itself to create a new exception.
	 */
	public static function fatalErrorHandler() {
		if ($error = error_get_last ()) {
			$customError = null;
			switch ($error ['type']) {
				case E_ERROR :
					$customError = ErrorCode::PHP_E_ERROR;
					break;
				case E_CORE_ERROR :
					$customError = ErrorCode::PHP_E_CORE_ERROR;
					break;
				case E_COMPILE_ERROR :
					$customError = ErrorCode::PHP_E_COMPILE_ERROR;
					break;
				case E_USER_ERROR :
					$customError = ErrorCode::PHP_E_USER_ERROR;
					break;
				case E_PARSE :
					$customError = ErrorCode::PHP_E_PARSE;
					break;
			}
			if ($customError != null) {
				if(ob_get_length()>0) ob_clean();
				new self ( Status::S5_InternalServerError, $customError, $error ['message'], null, $error ["file"], $error ["line"] );
			}
		}
	}
	
	/**
	 * Called when a catcheable error ocurrs but code does not catching them.
	 * set_error_handler attached function.
	 * 
	 * This argument can be an integer or an Exception object.
	 *
	 * @param mixed[\Exception|| integer] $e
	 */
	
	public static function errorHandler($e) {
		if (is_integer ($e)){ // 
			$args = func_get_args ();
			if(isset($args[1]) && strpos($args[1], "mysqli_stmt::")!==false){
				new self(Status::S5_InternalServerError, ErrorCode::InternalServerError, implode("\r\n", $args));
			}
			else{
				if(isset($args[4]["stm"])){ //Arg 4 is the context of the error. If exists stm property is Oracle error.
					//$oci_error = oci_error ($args[4]["stm"]);
					throw new \ErrorException ( $args [1], -1, 0, $args [2], $args [3] );
				}
				else{
					new self ( Status::S5_InternalServerError, ErrorCode::InternalServerError, $args [1], - 1, $args [2], $args [3] );
				}
			}
		} else if (is_object ( ($e) )) {
			if(get_class($e)=='core\Exception\OracleException'){ //if cames from OracleException
				/**@var $e \core\Exception\OracleException*/
				new self ($e->status,$e->errorCode,$e->oramessage,null,$e->trace["file"],$e->trace["line"],$e->orasql,"ORACLE",$e->vars,$e->db_tracking);
			}
			else{
				new self ( Status::S5_InternalServerError, ErrorCode::InternalServerError, $e->getMessage (), $e->getPrevious (), $e->getFile (), $e->getLine () );
			}
		}
	}
}

class MyException extends \Exception {
	public function __construct($msg, $code, $file, $line, $trace){
		$this->file = $file;
		$this->line = $line;
		parent::__construct($msg, $code, $trace);
	}
}

/**
 * OracleException.
 * 
 * Catchs OracleErrors on Dbcon, if the programmer does not use try catch statement, the error is catched on AppException.
 * If not, catchs on the catch statement 
 * 
 * @author phidalgo
 *
 */
class OracleException extends \Exception{
	public $oracode = null;
	public $oramessage = null;
	public $orasql = null;
	public $vars = null;
	
	public $status;
	public $errorCode;
	public $userid;
	public $trace;
	public $foreign_key = null;
	public $db_user = null;
	public $db_tracking = null;
	
	protected static $environment;
	protected static $config;
	
	
	public function __construct($oraError,\Exception $e = null,$vars = null){
		$this->trace = $this->getTrace();
		$this->trace = $this->trace[0];
		self::$config = config::_GET();
		$this->oracode = $oraError["code"];
		$this->oramessage = $oraError["message"];
		$this->orasql = $oraError["sqltext"];
		$oracode = "ORA-0".$this->oracode;
		
		
		$this->vars = $vars;
		try {
			self::$environment = Environment::getInstance ();
			$this->userid = (self::$environment->user != null ? self::$environment->user->id : '');
		} catch ( \Exception $e ) {}
		
		switch($this->oracode){
			case 2292: //Integritat de foreign key violada. Child record found.
				$findme   = "ORA-02292: integrity constraint (";
				$posi = strpos($this->oramessage, $findme);
				$cs = substr($this->oramessage, $posi+strlen($findme));
				$pose = strpos($cs,")");
				$fk = trim(substr($cs,0,$pose));
				$s = explode(".",$fk);
				$this->foreign_key = $s[1];
				$this->db_user = $s[0];
				
				
				$sql = "SELECT 
						ac.table_name,
         				column_name,
         				position,
         				ac.constraint_name,
         				constraint_type,
         				(SELECT ac2.table_name FROM all_constraints ac2 WHERE AC2.CONSTRAINT_NAME = AC.R_CONSTRAINT_NAME) fK_to_table
    					FROM all_cons_columns acc, all_constraints ac
   						WHERE 
						acc.constraint_name = ac.constraint_name
         				AND acc.table_name = ac.table_name
         				AND CONSTRAINT_TYPE IN ('P', 'R')
						AND ac.constraint_name=:fkn order by position asc"; 
				$rs = self::$environment->dbcon->execute($sql,array("fkn" => $this->foreign_key));
				//Busquem les columnes de la taula que conté el registre que volem borrar i no podem.
				//Si la FK és de varies columnes (pq l 'element que volem borrar té multiples PK)
				
				$columns = array();
				while($rs->fetch()){
					$tn = $rs->table_name; //Taula on hi ha aquesta FK
					array_push($columns,$rs->column_name); //Columna de la taula on hi ha aquesta FK
					$tm = $rs->fk_to_table; //Taula a la qual fa referencia la FK
				}
				$rs->close();
				
				/*
				 * Mirem la PK de la taula on fa referencia aquesta FK per veure si en el delete hi ha la PK per agafar el valor del element que volem eliminar i cercarlo en la taula on
				 * hi és.
				 * $table conté una propietat primary_keys on  hi ha totes les pks 
				 */
				$table = self::$environment->tables->_get($tm,false); //Taula on hi ha l'element que volem borrar i no podem
				if($table===false){
					$table = new Table($tm,false);//Si no hem trobat la taula, carreguem la info manualment
				}
				$tablef = self::$environment->tables->_get($tn,false); //Taula on hi ha una referencia a l'element que volem borrar i ens fa petar la FK.
				if($tablef===false){
					$tablef = new Table($tn,false);//Si no hem trobat la taula, carreguem la info manualment
				}
				
				//Començem a generar la resposta.
				$answer = array();
				if(self::$environment->development){
					$answer["DATABASE"]["FOREIGN_KEY"] = $this->foreign_key;
					$answer["DATABASE"]["QUERY"] = $this->orasql;
					$answer["DATABASE"]["VARS"] = $this->vars;
					$answer["DATABASE"]["FK_TABLE"]["NAME"] = $tablef->db_name;
					$answer["DATABASE"]["FK_TABLE"]["ID"] = $tablef->id;
					$answer["DATABASE"]["FK_TABLE"]["PARENT"] = array();
					if($tablef->hasParent){
						$answer["DATABASE"]["FK_TABLE"]["PARENT"]["NAME"] = $tablef->parent->db_name;
						$answer["DATABASE"]["FK_TABLE"]["PARENT"]["ID"] = $tablef->parent->id;
					}
					$answer["DATABASE"]["TABLE"]["NAME"] = $table->db_name;
					$answer["DATABASE"]["TABLE"]["ID"] = $table->id;
				}
				
				
				$answer["FK_OBJECT"]["FIELDS"] = $columns;
				if($tablef->hasParent){
					$answer["FK_OBJECT"]["NAME"] = $tablef->parent->name;
					$answer["FK_OBJECT"]["ID"] = $tablef->parent->id;
				}
				else{
					$answer["FK_OBJECT"]["NAME"] = $tablef->name;
					$answer["FK_OBJECT"]["ID"] = $tablef->id;
				}
				
				$answer["OBJECT"]["NAME"] = $table->name;
				$answer["OBJECT"]["ID"] = $table->id;
				
				
				$values = array();
				foreach($table->primary_key as $pk){
					//die($this->orasql);
					$pos = strpos($this->orasql,$pk);
					$a = substr($this->orasql,$pos);
					$exp = explode("=",$a);
					//die(var_dump($exp));
					$v = $exp[1];
					$arg = false;
					if(startsWith($v,":")){
						$arg = true;
						$v = substr($v,1);
					}
					
					$pos2 = strpos($v," ");
					
					if($pos2!==false){
						$v = substr($v,0,$pos2);
					}
					$assigned_var = $v;
					//die($assigned_var);
					if($arg){
						$value = $this->vars[$assigned_var];
					}
					else{
						$value = $assigned_var;
					}
					
					array_push($values,$value);
				}
				
				
				
				
				$table_alias = "a";
				$fields = "";
				
				
				//Agafem el PK com a ID de la taula on hi ha l'element que fa referencia a l'element que volem borrar.
				for($i=0,$j=count($tablef->primary_key);$i<$j;$i++){
					$fields.= $table_alias.".".$tablef->primary_key[$i]." AS id".$i.", "; //Sempre "," pq després venen els valors dels elements de la fk de la taula que volem borrar.
				}
				
				$where = "";
				$vars = array();
				for($i=0,$j=count($values);$i<$j;$i++){
					$where.= $table_alias.".".$columns[$i]."=:v".$i.($i+1<$j? " AND ":"");
					$fields .= $table_alias.".".$columns[$i]." as v".$i.($i+1<$j? ", ":"");
					$vars["v".$i] = $values[$i];
				}
				
				
				//Si la taula on peta per la FK és taula pare mirem si té columna name, si en té la retornem també.
				if(!$tablef->hasParent){
					if($tablef->getColumn("name")!==false){
						$fields.= ", ".$table_alias.".NAME";
					}
				}
				
				//Creem la consulta per a seleccionar els elements que contenen l'element que volem borrar.
				$sql = "SELECT $fields FROM $tn $table_alias WHERE ".$where;
				$rs = self::$environment->dbcon->execute($sql,$vars);
				$elements = $rs->getAll();
				$rs->close();
				
				
				//Si te pare, recollim els elements del pare.
				if($tablef->hasParent){
					$answer["ELEMENTS"] = array();
					$tpn = $tablef->parent->db_name; //Table parent name
					if($tablef->parent->getColumn("name")!==false){
						foreach($elements as $element){
							$where = "";
							$fields = "$table_alias.NAME AS name, ";
							$vars = array();
							for($i=0,$j=count($tablef->parent->primary_key);$i<$j;$i++){
								$fields .= $table_alias.".".$tablef->parent->primary_key[$i]." AS id".$i.($i+1<$j? ", ":"");
								$where.= " ".$tablef->parent->primary_key[$i]."=:PK$i".($i+1<$j? " AND ":"");
								$vars["PK$i"] = $element["id$i"];
							}
							$sql = "SELECT $fields FROM $tpn $table_alias WHERE $where";
							$rs = self::$environment->dbcon->execute($sql,$vars); //AQUI
							$pelements = $rs->getAll();
							$rs->close();
							$pelements = $pelements[0];
							array_push($answer["ELEMENTS"],$pelements);
						}
					}
				}
				else{
					$answer["ELEMENTS"] = $elements;
				}
				
				$this->db_tracking = $answer;
				//http://stackoverflow.com/questions/17884469/what-is-the-http-response-code-for-failed-http-delete-operation
				/**
					Yes, it would be 404.
					In general it will be a 400 series error if the request is wrong somehow, and a 500 series error if something goes awry on the server.
				*/
				$this->status = Status::S4_NotFound;
				$this->errorCode = ErrorCode::DB_FK_CHILD_RECORD_FOUND;
				break;
			case 1036: //Variable no o mal enllaçada en la consulta SQL.
			case 942: //Taula o vista no existeix
			case 1036: //Nombre o nom de variables incorrecte
			case 933: //SQL command not properly ended
			case 2291: //integrity constraint violated. Parent key NOT found.
			default:
				$this->status = Status::S5_InternalServerError;
				$this->errorCode = ErrorCode::DBQueryError;
		}
		
	}
}
?>
