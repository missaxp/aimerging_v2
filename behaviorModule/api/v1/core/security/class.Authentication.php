<?php

namespace core\security;

use core\http\Status;
use core\http\ErrorCode;
use api\crud\User;
use core\Exception\AppException;
use Slim\Route;
use Slim\Router;

/**
 * Gestió de l'autenticació d'usuaris.
 *
 * @author phidalgo
 */
class Authentication {
	protected static $environment;
	public function __construct() {
		self::$environment = \core\Environment::getInstance ();
	}
	
	/**
	 * Validate user.
	 * If there is any error (such as wrong user/password) it throws an AppException.
	 *
	 * @param
	 *        	string user
	 * @param
	 *        	string password
	 * @throws AppException
	 */
	public function checkCredentials($user, $password) {
		if(!self::$environment->authentication_required){
			return true;
		}
		$retorn = false;
		$vars ['username'] = $user;
		$vars ['password'] = $password;
		
		$sql = "select ID from USERS where (upper(username)=upper(:username) OR upper(email)=upper(:username)) and password=:password";
		$rs = self::$environment->dbcon->execute ( $sql, $vars );
		if (! $rs->fetch ()) {
			$rs->close ();
			/**
			 * 200,300, 400, 500 are all very generic.
			 * If you want generic, 400 is OK.
			 * 422 is used by an increasing number of APIs, and is even used by Rails out of the box.
			 * No matter which status code you pick for your API, someone will disagree.
			 * But I prefer 422 because I think of '400 + text status' as too generic.
			 * Also, you aren't taking advantage of a JSON-ready parser; in contrast, a 422 with a JSON response is very explicit, and a great deal of error information can be conveyed.
			 */
			throw new AppException ( Status::S4_UnprocessableEntity, ErrorCode::IncorrectUserPassword );
		}
		$rs->close ();
	}
	
	/**
	 * Retorna true o false en funció si les credencials són vàlides o no.
	 * Si és vàlid, com que ha fet una petició autenticada, actualitzem la data de la última crida a la API.
	 *
	 * @param
	 *        	string token
	 * @return true
	 * @throws AppException
	 */
	public function Validate($token) {
		if ($token == "")
			throw new AppException ( Status::S4_Unauthorized, ErrorCode::TokenNotFound );
		
		$dies_timeout = self::$environment->login_timeout / (24 * 60); // Mins to days.
		$vars ["SESSION_TOKEN"] = $token;
		$vars ["DIES_TIMEOUT"] = array (
				$dies_timeout,
				DBCONN_DATATYPE_FLOAT 
		);
		$vars ["REMOTE_ADDR"] = self::$environment->request->getIp ();
		
		$ecode = null;
		
		$sql = "SELECT
        		DECODE((current_date - EXPIRES) - abs(current_date - EXPIRES), 0, 1, 0) as EXPIRED,
        		DECODE((current_date-(:DIES_TIMEOUT) - LAST_ACCESS) - abs(current_date-(:DIES_TIMEOUT) - LAST_ACCESS), 0, 1, 0) as LOGOUT,
        		DECODE(REMOTE_ADDR,:REMOTE_ADDR,0,1) as SAME_LOC
        		FROM SESSION_TOKEN
        			WHERE SESSION_TOKEN=:SESSION_TOKEN";
		
		$rs = self::$environment->dbcon->execute ( $sql, $vars );
		if ($rs->fetch ()) {
			if ($rs->getVal ( "EXPIRED" ) == 1) {
				$ecode = ErrorCode::SessionExpired;
			} else if ($rs->getVal ( "SAME_LOC" ) == 1) {
				$ecode = ErrorCode::UnknownIssuer;
			} else if ($rs->getVal ( "LOGOUT" ) == 1) {
				$ecode = ErrorCode::SessionTimeout;
			} else {
				$ecode = null;
			}
		} else {
			$ecode = ErrorCode::InvalidToken;
		}
		$rs->close ();
		if ($ecode == null)
			return true;
		else
			throw new AppException ( Status::S4_Unauthorized, $ecode );
	}
	
	/**
	 * Retorna un token i la data d'expiració per a un usuari.
	 * En el cas que el token sigui vàlid, retorna el token actual, sino genera un nou token.
	 *
	 * @param
	 *        	string user
	 * @return array("api_key" => <NULL || string>, "expires" => <NULL || date>)
	 * @throws AppException
	 */
	public function getValidToken($username) {
		require BaseDirAPI . apiVersion . '/core/security/class.Token.php';
		$token = new Token ($username);
		$auth = $token->generateNewToken ();
		if ($auth->token != NULL) {
			if(self::$environment->authentication_required){
				$uid = User::getUserIDFromUserName($username);
				self::$environment->dbcon->execute("DELETE FROM SESSION_TOKEN WHERE USER_ID=:ID",array("ID" => $uid)); //Delete all previous tokens for this user.
				$vars ["USER_ID"] = $uid;
				$vars ["SESSION_TOKEN"] = $auth->token;
				$vars ["EXPIRES"] = $auth->expire;
				$vars ["LAST_ACCESS"] = self::$environment->getDate();
				$vars ["FIRST_ACCESS"] = self::$environment->getDate();
				$vars ["REMOTE_ADDR"] = self::$environment->request->getIp ();
				self::$environment->dbcon->add("SESSION_TOKEN", $vars );
				unset ( $vars );
				
				//$vars ["USER_ID"] = $uid;
				//$vars ["TIMEOUT"] = array (self::$environment->login_timeout / (24 * 60),DBCONN_DATATYPE_FLOAT); // Mins to days.
				
				// TODO: Make a CRON job who deletes invalid tokens every day, at least once a day
				// We check if this user has invalid tokens and delete it.
				// $sql = "DELETE FROM SESSION_TOKEN WHERE USER_ID=:USER_ID AND (current_date>EXPIRES OR (current_date-:TIMEOUT)>LAST_ACCESS)";
				// $affectedRows = self::$environment->dbcon->execute($sql,$vars);
			}
			return ( object ) array (
					'token' => $auth->token,
					'expires' => $auth->expire,
					"svr" => $auth->svr,
					"timeout" => (self::$environment->login_timeout * 60) 
			);
		} else {
			throw new AppException ( Status::S5_InternalServerError, ErrorCode::TokenError );
		}
	}
	
	
	
	/**
	 * Authentication Method for REST FULL API.
	 * Also check the user's permissions to access to a module.
	 *
	 * @author phidalgo
	 * @param  Route $route        	
	 * @throws AppException
	 * @return boolean
	 */
	function __invoke($route) {
		if (!self::$environment->authentication_required){
			self::$environment->user = new \core\Security\User(null);
			self::$environment->security = new \core\Security\Security();
			return true;
		}
		
		// Get authorization token. Null is returned if no authorization header is received.
		// WARNING: Afegir RewriteRule .? - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}] en el rewriter de la configuració del site
		$token = self::$environment->request->headers("Authorization");
		if (strtoupper ( $token ) == 'NULL') {
			$token = null;
		}
		// If there is not on the authorization header, tries to recover token by cookie
		// WARNING: Les cookies no funcionen en peticions entre dominis diferents
		if ($token == null)
			$token = self::$environment->app->getCookie ('session_token'); // L'intenta obtenir de la cookie
		
		if ($token != null) { // Verifying token session.
			$this->Validate ($token);
			self::$environment->user = new \core\Security\User($token); // If we successfull login to the webService, assign the user to the webservice request.
			self::$environment->security = new \core\Security\Security();
		} else { // session token is missing in header or cookie.
			throw new AppException ( Status::S4_Forbidden, ErrorCode::TokenNotFound );
		}
	}
}

?>