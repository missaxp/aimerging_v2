<?php

namespace core\security;

use core\Environment;
use core\Exception\AppException;
use core\http\ErrorCode;
use core\http\Status;
use core\security\SecurityFeatures;


/**
 * Gestió de Tokens d'autorització temporal.
 *
 * @author phidalgo
 */
class Token {
	protected static $environment;
	public $username = "";
	private $token;
	private $expire;
	private $svrdate;
	public function __construct($username) {
		self::$environment = Environment::getInstance ();
		
		if(self::$environment->authentication_required){
			if ($username == ""){
				throw new AppException ( Status::S5_InternalServerError, ErrorCode::TokenError, "Error, var \$username must not be empty" );
			}
			$this->username = $username;
		}
		else{
			$this->username = SecurityFeatures::SU_UNAME;
		}
		
		
	}
	
	/**
	 * Genera un nou token per accedir a la API.
	 *
	 * Si $token o $expire NULL implica error en servidor.
	 *
	 * El token es creat amb la concatenació d'un string de la següent manera:
	 * username:password:timestamp
	 * I d'aquest string, fem el hash (md5)
	 *
	 * Adapta la data amb la funció datetoISO8601
	 *
	 * @return (object) array("token" => token,"expire" => expire)
	 * @throws nothing
	 */
	public function generateNewToken() {
		if(!self::$environment->authentication_required){
			$fecha = new \DateTime ();
			$timestamp = $fecha->getTimestamp();
			$this->token = md5 (SecurityFeatures::SUPERUSER);
			$svr = $fecha->format ( 'Y-m-d H:i:s' );
			$fecha->add ( new \DateInterval ( self::$environment->token_expires ) );
			$this->expire = datetoISO8601 ( $fecha->format ( 'Y-m-d H:i:s' ) );
			$this->svrdate = datetoISO8601 ( $svr );
		}
		else{
			$vars ['USERNAME'] = $this->username;
			$sql = "select PASSWORD from USERS where (upper(username)=upper(:USERNAME) OR upper(email)=upper(:USERNAME))";
			$rs = self::$environment->dbcon->execute ( $sql, $vars );
			if (! $rs->fetch ()) {
				$this->token = NULL;
				$this->expire = NULL;
			} else {
				$fecha = new \DateTime ();
				$password = $rs->getVal ( "PASSWORD" );
				$timestamp = $fecha->getTimestamp ();
				$this->token = md5 ( $this->username . ":" . $password . ":" . $timestamp );
				$svr = $fecha->format ("Ymd\THis\Z");
				$fecha->add ( new \DateInterval(self::$environment->token_expires ) );
				$this->expire = $fecha->format("Ymd\THis\Z");
				$this->svrdate = $svr;
			}
		}
		
		return ( object ) array (
				"token" => $this->token,
				"expire" => $this->expire,
				"svr" => $this->svrdate 
		);
	}
}
?>