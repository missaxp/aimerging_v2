<?php
namespace core\security;

use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;
use core\Environment;

/**
 * Object LOGGED user.
 * 
 * @author phidalgo
 * @created 20160405
 */
class User{
	/**
	 * Instance of Environment.
	 * @var \core\Environment
	 */
	protected static $environment;
	/**
	 * Public user id.
	 * @var integer
	 */
	public $id;
	/**
	 * User name
	 * @var string
	 */
	public $username;
	/**
	 * User password (md5)
	 * @var string 
	 */
	public $password;
	/**
	 * Role ID
	 * @var integer
	 */
	public $role_id;
	/**
	 * Grup al qual pertany l'usuari
	 * Quan un usuari crei un recurs, serà el grup per defecte d'aquest recurs.
	 * @var integer
	 */
	public $user_group;
	/**
	 * Departament al qual pertany l'usuari.
	 * Quan aquest usuari crei un recurs, será el departament per defecte d'aquest recurs.
	 * @var integer
	 */
	public $user_department;
	
	/**
	 * Role Object
	 * @var \core\security\Role
	 */
	public $role;
	
	/**
	 * Has the current session info for this user.
	 * This var may be null if you want a User object which it has not logged into WS (perhaps a Users list, or something like that)
	 *
	 * @var \core\security\Session
	 */
	public $session = null;
	
	/**
	 * Session TOKEN.
	 * @var string
	 */
	public $session_token;
	
	/**
	 * User Interface Language
	 * @var string
	 */
	public $ui_language;
	
	public function __construct($session_token){
		self::$environment = Environment::getInstance();
		if($session_token==null){ //Super User (no authentication or authenticated with root)
			$this->id = -1;
			$this->role_id = -1;
			$this->user_department = null;
			$this->user_group = -1;
			$this->username = "root";
			$this->password = null;
			$this->ui_language = 'en';
		}
		else{
			$sql = "SELECT ID,ROLE_ID,USERNAME,PASSWORD, USER_ID as OWNER_UID,USER_GROUP,GROUP_ID as OWNER_GID,USER_DEPARTMENT,DEPARTMENT_ID,UI_LANGUAGE
			FROM USERS WHERE ID=(SELECT USER_ID FROM SESSION_TOKEN WHERE SESSION_TOKEN=:TOKEN_ID)";
			$rs = self::$environment->dbcon->execute($sql,array("TOKEN_ID" => $session_token));
			if($rs->fetch()){
				$this->id = $rs->id;
				$this->role_id = $rs->role_id;
				$this->user_department = $rs->user_department;
				$this->user_group = $rs->user_group;
				$this->username = $rs->username;
				$this->password = $rs->password;
				$this->ui_language = $rs->ui_language;
			}
			$rs->close();
			if($this->id==null){
				throw new AppException(Status::S5_InternalServerError,ErrorCode::InternalServerError,"@LITERAL no user with session token: $session_token");
			}
			
			$this->session_token = $session_token;
			$this->session = new \core\security\Session($this->session_token);
			$this->role = new \core\security\Role($this->role_id,true);
			$this->session->updateAccess(); //When a user is successfully login to the WebService, we check if we has access to the requested URI subspace.
		}
	}
	
	
}