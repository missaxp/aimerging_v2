<?php

namespace core\security;

use core\Environment;
use api\crud\Action;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;

/**
 * Checks the user's permission.
 *
 * @author phidalgo
 *        
 */
class Security {
	protected static $environment;
	
	/**
	 *
	 * @var \api\crud\Module
	 */
	private $currentModule;
	
	private $role;
	private $user;
	private $group;
	
	public function __construct() {
		self::$environment = Environment::getInstance();
		if(self::$environment->authentication_required){
			$this->user = self::$environment->user;
			$this->group = new group($this->user->user_group);
			$this->role = $this->user->role;
		}
		else{
			$this->group = new group();
		}
	}
	
	
	/**
	 * 
	 * @param integer $id
	 * @return \core\security\permission
	 * @throws AppException
	 */
	public function table($id){
		/* @var $permission \api\crud\Permission */
		foreach($this->group->permission() as $permission){
			if($permission->table_id==$id){
				return $permission;				
			}
		}
		throw new AppException(Status::S5_InternalServerError,ErrorCode::T_NOT_FOUND,"@LITERAL: Table $id NOT FOUND");
	}
	
	/**
	 * Check if the user has acces to the module
	 * Returns true or false.
	 * Throws AppException when module is not allowed.
	 *
	 * @param string $module_name        	
	 * @throws AppException
	 * @return bool
	 */
	public function hasAccessToModule() {
		/* @var $module \api\crud\Module */
		foreach ( $this->role->getAllowedModules () as $module ) {
			if ($module->name == self::$environment->rootURI) {
				$this->currentModule = $module;
				return true;
			}
		}
		// If user has not access to module, we supose that is not a programming logical error, and then we DELETE THE TOKEN.
		$this->deleteToken ( self::$environment->user->session->session_token );
				
		throw new AppException ( Status::S4_Unauthorized, ErrorCode::Unauthorized, "UNAUTORIZED ACCESS TO " . self::$environment->rootURI );
		return false;
	}
	
	/**
	 * Check if the user is allow to perform this action.
	 *
	 * @param integer $action_id        	
	 * @throws AppException
	 * @return boolean
	 */
	public function hasAccesToAction($action_id) {
		if (! isset ( $this->currentModule ) && self::$environment->authentication_required) return true;
		$this->currentModule->getRoleActions ( $this->role->id);
		
		$actions = $this->currentModule->actions;
		
		/* @var $action \api\crud\Action */
		foreach ( $actions as $action ) {
			if ($action->id == $action_id)
				return true;
		}
		// If user has not access to action, we supose that is not a programming logical error, and then we DELETE THE TOKEN.
		if (! self::$environment->development) {
			$this->deleteToken ( self::$environment->user->session->session_token );
		}
		
		$actionName = new Action($action_id); // Get action name
		                                         
		// Also throw an appexception which will be send the error to the client.
		throw new AppException ( Status::S4_Unauthorized, ErrorCode::Unauthorized, "You are not allowed to " . $actionName->name );
		return false;
	}
	
	/**
	 * If we provide a token, we delete this.
	 * If no token is provided, we will delete al tokens related to current user.
	 *
	 * Returns the number of deleted session_token.
	 *
	 * @return integer $affectedRows
	 * @param string $token        	
	 */
	private function deleteToken($token = null) {
		if ($token == null) {
			$user_id = self::$environment->user->id;
			$affectedRows = self::$environment->dbcon->execute ( "DELETE FROM SESSION_TOKEN WHERE USER_ID=:USER_ID", array (
					"USER_ID" => $user_id 
			) );
			return $affectedRows;
		} else {
			$arr ["SESSION_TOKEN"] = $token;
			$affectedRows = self::$environment->dbcon->execute ( "DELETE FROM SESSION_TOKEN WHERE SESSION_TOKEN=:SESSION_TOKEN", $arr );
			return $affectedRows;
		}
	}
}

abstract class SecurityFeatures{
	Const SUPERUSER = "SUPERUSER";
	Const SU_UNAME = "root";
	Const SU_ROLE = -1;
	Const SU_ID = -1;
	Const SU_TOKEN = "idcpws";
}


?>