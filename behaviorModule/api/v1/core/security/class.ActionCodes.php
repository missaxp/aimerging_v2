<?php

namespace api\Security;

/**
 * Abstract class with the relations between action code and action name.
 * This is used by improve the developments action.
 *
 * @author phidalgo
 * @namespace api\security
 *           
 */
abstract class ActionsCode {
	Const QUERIE_ADD = 0;
	Const QUERIE_MY = 1;
	Const LP_Portal = 2;
	Const QA_CHECK = 7;
	Const QA_MANAGEMENT = 8;
	Const QA_GENERAL_PERFORMANCE = 9;
	Const QA_COMMON_ISSUES = 10;
	Const MANAGEMENT_CONFIGURE_WS = 11;
	Const TOOLS_PARSING_TASK = 12;
	Const DOCUMENT_ADD = 13;
	Const DOCUMENT_VIEW = 14;
	Const DOCUMENT_MODIFY = 15;
	Const DOCUMENT_DELETE = 16;
	Const INVOICE_ADD = 17;
	Const INVOICE_VIEW = 18;
	Const INVOICE_MODIFY = 19;
	Const INVOICE_DELETE = 20;
	Const INVOICE_VIEW_MY = 21;
	Const USER_ADD = 22;
	Const USER_VIEW = 23;
	Const USER_MY_VIEW = 24;
	Const USER_MODIFY = 25;
	Const USER_DELETE = 26;
	Const USER_ECONOMICAL_DATA_VIEW = 27;
	Const USER_JOB_DATA_VIEW = 28;
	Const ROLE_ADD = 29;
	Const ROLE_VIEW = 30;
	Const ROLE_MODIFY = 31;
	Const ROLE_DELETE = 32;
	Const DEPARTMENT_ADD = 33;
	Const DEPARTMENT_VIEW = 34;
	Const DEPARTMENT_MY_VIEW = 35;
	Const DEPARTMENT_MODIFY = 36;
	Const DEPARTMENT_DELETE = 37;
	Const DEPARTMENT_ADD_USER = 42;
	Const USER_ADD_DEPARTMENT = 38;
	Const TASK_ADD = 3;
	Const TASK_MY_VIEW = 4;
	Const TASK_MANAGEMENT = 5;
	Const TASK_MY_PERFORMANCE = 6;
	Const TASK_MODIFY = 39;
	Const TASK_DELETE = 40;
	Const TASK_VIEW = 41;
}


?>