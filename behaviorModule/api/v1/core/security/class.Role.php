<?php

namespace core\security;

use core\Environment;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;
/**
 * Class Role
 *
 * @author phidalgo
 * @namespace api\crud
 */
class Role {
	
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	/**
	 * Role Identifier
	 *
	 * @var Integer
	 */
	public $id = null;
	
	/**
	 * Module Name
	 *
	 * @var string
	 */
	public $name = '';
	
	/**
	 * Array of modules with the available modules for this Role.
	 *
	 * @var array[Module]
	 */
	public $modules = array();
	
	public $options = array();
	
	/**
	 * Constructor.
	 *
	 * @param
	 *        	mixed_id
	 * @param
	 *        	bool loadAll
	 * @throws core\Exception\AppException
	 */
	public function __construct($mixed_id = null, $loadAll = false) {
		self::$environment = Environment::getInstance ();
		if ($mixed_id != null) {
			// Check role id
			if (! is_numeric ( $mixed_id ) && ! is_string ( $mixed_id ) && strlen ( $mixed_id ) == 0) {
				throw new AppException ( Status::S4_BadRequest, ErrorCode::FieldRequired, "Error \$id must be a number or a not empty string" );
			}
		
			// Check if exists
			if (is_numeric ( $mixed_id )) {
				$this->id = $mixed_id;
				$sql = "SELECT * FROM ROLES WHERE ID=:ID";
				$rs = self::$environment->dbcon->execute ( $sql,array("ID" => $this->id));
			} elseif (is_string ( $mixed_id ) || ! is_numeric ( $mixed_id )) {
				$this->name = $mixed_id;
				$sql = "SELECT * FROM ROLES WHERE NAME=:NAME";
				$rs = self::$environment->dbcon->execute ($sql, array ("NAME" => $this->name));
			}
			$fetch = $rs->getAll ();
			$rs->close ();
			if ($mixed_id!=null && count($fetch) == 0) {
				throw new AppException ( Status::S4_NotFound, ErrorCode::FieldWrongId, "Error Role $mixed_id not found" );
			}
			$this->id = $fetch[0]["id"];
			$this->name = $fetch[0]["name"];
			unset ($fetch);
			if ($loadAll) {
				$this->getAllowedModules();
				$this->getOptions();
			}
		}
		else{
			$this->getOptions();
		}
	}
	
	public function getOptions(){
		$this->options["modules"] = $this->getModulesActions();
	}
	
	/**
	 * Get allowed modules for this Role.
	 * If we already have the modules,
	 *
	 * @param
	 *        	bool reload
	 * @return array[Module]
	 * @throws core\Exception\AppException
	 */
	public function getAllowedModules() {
		$this->modules = $this->getModulesActionsByRole($this->id);
	}
	
	/**
	 * Set a new Module into this Role.
	 *
	 * @param
	 *        	Module
	 * @throws nothing
	 */
	public function setModule(Module $module) {
		array_push ($this->modules,$module);
	}
	
	/**
	 * Get all list of module-Actions.
	 */
	private function getModulesActions(){
		$rsp = array();
		$rs = self::$environment->dbcon->execute("SELECT NAME,ID FROM MODULES ORDER BY NAME");
		$modules = $rs->getAll();
		$rs->close();
		foreach($modules as $module){
			$ob = array("id" => $module["id"], "name" => $module["name"],"actions" => null);
			$rs = self::$environment->dbcon->execute("SELECT ID,DESCRIPTION,ALIAS_ID FROM ACTIONS WHERE MODULE_ID=:MID",array("MID" => $module["id"]));
			$actions = $rs->getAll();
			$rs->close();
			$ob["actions"] =$actions;
			$rsp[] = $ob;
		}
		return $rsp;
	}
	
	/**
	 * Get all list of module-Actions.
	 */
	private function getModulesActionsByRole(){
		$rsp = array();
		self::$environment = Environment::getInstance();
		$rs = self::$environment->dbcon->execute("SELECT NAME,ID FROM MODULES ORDER BY NAME");
		$modules = $rs->getAll();
		$rs->close();
		foreach($modules as $module){
			$ob = array("id" => $module["id"], "name" => $module["name"],"actions" => null);
			$rs = self::$environment->dbcon->execute("SELECT RA.ACTION_ID FROM ROLES_ACTIONS RA,ACTIONS A WHERE
			RA.ACTION_ID=A.ID AND A.MODULE_ID=RA.MODULE_ID AND RA.ROLE_ID=:RID AND RA.MODULE_ID=:MID",array("MID" => $module["id"], "RID" => $this->id));
			$actions = $rs->getAll();
			$rs->close();
			$ob["actions"] = $actions;
			if(count($actions)!=0){
				$rsp[] = $ob;
			}
		}
		return $rsp;
	}
}
?>