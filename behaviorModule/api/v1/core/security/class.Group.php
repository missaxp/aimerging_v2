<?php 
namespace core\security;

use core\Environment;
use common\Table;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;


/**
 * Class \core\Security\Group
 * 
 * This class process the permissions for each table for a instanciated group.
 * 
 * @author phidalgo
 *
 */
class group{
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	/**
	 * Group ID
	 * @var integer
	 */
	public $id;
	/**
	 * Group Name
	 * @var string
	 */
	public $name;
	public $creation_ts;
	public $modification_ts;
	
	
	/**
	 * 
	 * @var array[\core\security\permission]
	 */
	private $permission = array();
	
	public function __construct($gid = null,$config = null){
		self::$environment = Environment::getInstance();
		if($gid!=null){
			self::$environment = Environment::getInstance();
			$sql = "SELECT * FROM P_GROUPS WHERE ID=:ID";
			$rs = self::$environment->dbcon->execute($sql,array("ID" => $gid));
			if($rs->fetch()){
				$this->name = $rs->name;
				$this->creation_ts = $rs->creation_ts;
				$this->modification_ts = $rs->modification_ts;
				$this->id = $gid;
			}
			$rs->close();
			if($this->id == null){
				throw new AppException(Status::S4_NotFound,ErrorCode::NotFound,"@LITERAL Error: Group ($gid) not found");
			}
		}
		$tables = self::$environment->tables->_get();
		
		foreach($tables as $table){
			if($table->hasGranularP){ //If this table has granular prermission system...
				$this->permission[] = new permission($table,$this->id);
			}
		}
	}
	
	public function permission(){
		return $this->permission;
	}
}

/**
 * A permission is an object which collects permissions for a specific table.
 * 
 * Permissions can be given to belong a group or for the user.
 * 
 * Table: P_GROUPS_T -> Permissions given on a group foreach table.
 * Table: P_USERS_T -> Permissions given on a user foreach table.
 * 
 * A permission is created by using a system like UNIX has.
 * User|Group|Others.
 * 
 * Any user, group or users that belongs to other groups could have read,write or delete permissions.
 * 
 * * 
 * Additionaly 2 more permissions are added to this system.
 * 
 * The user, the group or other groups can have the administrate permission.
 * 
 * The user can have the create permission.
 * 
 * So in fact we have a permission system foreach table like:
 * 
 * user = ?c?r?w?d?a?
 * group = ?r?w?d?a?
 * others = ?r?w?d?a?
 * 
 * And it can be represented in a object with properties set true or false or in a binary mode like:
 * u = 11110
 * g = 01100
 * o = 00000
 * @author phidalgo
 *
 */
class permission{
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	public $table_id;
	/**
	 * Permissions for this table.
	 * @var Table*/
	private $table;
	
	/**
	 * Contains group permissions for this table.
	 * @var \core\security\permission_properties
	 */
	private $group_permissions;
	
	/**
	 * Contains user permissions for this table.
	 * @var \core\security\permission_properties
	 */
	private $user_permissions;
	
	/**
	 * Contains the addition for (Group+User) permissions for this table.
	 * @var \core\security\permission_properties
	 */
	private $global_permissions;
	
	
	/**
	 * Binary representation of the permissions.
	 * @var string
	 */
	private $user_unix_user = "00000";
	private $user_unix_group = "00000";
	private $user_unix_others = "00000";
	
	private $group_unix_user = "00000";
	private $group_unix_group = "00000";
	private $group_unix_others = "00000";
	
	private $global_unix_user = "00000";
	private $global_unix_group = "00000";
	private $global_unix_others = "00000";
	
	public function __construct(Table $table,$gid = null){
		self::$environment = Environment::getInstance();
		$this->table_id = $table->id;
		$this->table = $table;
		$this->group_permissions = new permission_properties();
		$this->user_permissions = new permission_properties();
		$this->global_permissions = new permission_properties();
		if($gid == null) return; //Default property values are set.
		
		
		//Here get and construct the permissions for this grup and this table.		
		$rs = self::$environment->dbcon->execute("SELECT * FROM P_GROUPS_T T WHERE  T.GROUP_ID=:ID AND T.TABLE_ID=:TID",array("TID" => $this->table_id,"ID" => $gid));
		if($rs->fetch()){
			if($rs->user_create=="Y"){ //User can create?
				$this->group_permissions->user_create = true;
				$this->group_unix_user{0} = "1"; //First position of the string is 1.
			}
			else{
				$this->group_permissions->user_create = false;
				$this->group_unix_user{0} = "0";
			}
			
			if($rs->user_read =='Y'){ //User can read?
				$this->group_permissions->user_read = true;
				$this->group_unix_user{1} = 1; //Second position of the string is 1
			}
			else{
				$this->group_permissions->user_read = false;
				$this->group_unix_user{1} = 0;
			}
			
			if($rs->user_write =='Y'){
				$this->group_permissions->user_write = true;
				$this->group_unix_user{2} = 1;
			}
			else{
				$this->group_permissions->user_write = false;
				$this->group_unix_user{2} = 0;
			}
			
			if($rs->user_delete =='Y'){
				$this->group_permissions->user_delete = true;
				$this->group_unix_user{3} = 1;
			}
			else{
				$this->group_permissions->user_delete = false;
				$this->group_unix_user{3} = 0;
			}
			
			if($rs->user_administrate =='Y'){
				$this->group_permissions->user_administrate = true;
				$this->group_unix_user{4} = 1;
			}
			else{
				$this->group_permissions->user_administrate = false;
				$this->group_unix_user{4} = 0;
			}

			//GROUP
			if($rs->group_read =='Y'){
				$this->group_permissions->group_read = true;
				$this->group_unix_group{1} = 1;
			}
			else{
				$this->group_permissions->group_read = false;
				$this->group_unix_group{1} = 0;
			}
			
			if($rs->group_write =='Y'){
				$this->group_permissions->group_write = true;
				$this->group_unix_group{2} = 1;
			}
			else{
				$this->group_permissions->group_write = false;
				$this->group_unix_group{2} = 0;
			}
			
			if($rs->group_delete =='Y'){
				$this->group_permissions->group_delete = true;
				$this->group_unix_group{3} = 1;
			}
			else{
				$this->group_permissions->group_delete = false;
				$this->group_unix_group{3} = 0;
			}
			
			if($rs->group_administrate =='Y'){
				$this->group_permissions->group_administrate = true;
				$this->group_unix_group{4} = 1;
			}
			else{
				$this->group_permissions->group_administrate = false;
				$this->group_unix_group{4} = 0;
			}
			
			
			
			//OTHER
			if($rs->other_g_read =='Y'){
				$this->group_permissions->other_g_read = true;
				$this->group_unix_others{1} = 1;
			}
			else{
				$this->group_permissions->other_g_read = false;
				$this->group_unix_others{1} = 0;
			}
				
			if($rs->other_g_write =='Y'){
				$this->group_permissions->other_g_write = true;
				$this->group_unix_others{2} = 1;
			}
			else{
				$this->group_permissions->other_g_write = false;
				$this->group_unix_others{2} = 0;
			}
			
			if($rs->other_g_delete =='Y'){
				$this->group_permissions->other_g_delete = true;
				$this->group_unix_others{3} = 1;
			}
			else{
				$this->group_permissions->other_g_delete = false;
				$this->group_unix_others{3} = 0;
			}
			
			if($rs->other_g_administrate =='Y'){
				$this->group_permissions->other_g_administrate = true;
				$this->group_unix_others{4} = 1;
			}
			else{
				$this->group_permissions->other_g_administrate = false;
				$this->group_unix_others{4} = 0;
			}
		}
		$rs->close();
		
		//Here get and construct the permissions for this user and this table.
		$sql = "SELECT * FROM P_USERS_T T WHERE T.USER_ID=:ID AND TABLE_ID=:TID";
		$rs = self::$environment->dbcon->execute($sql,array("TID" => $this->table_id,"ID" => self::$environment->user->id));
		if($rs->fetch()){
			if($rs->user_create=="Y"){
				$this->user_permissions->user_create = true;
				$this->user_unix_user{0} = "1";
			}
			else{
				$this->user_permissions->user_create = false;
				$this->user_unix_user{0} = "0";
			}
			
			if($rs->user_read =='Y'){
				$this->user_permissions->user_read = true;
				$this->user_unix_user{1} = 1;
			}
			else{
				$this->user_permissions->user_read = false;
				$this->user_unix_user{1} = 0;
			}
			
			if($rs->user_write =='Y'){
				$this->user_permissions->user_write = true;
				$this->user_unix_user{2} = 1;
			}
			else{
				$this->user_permissions->user_write = false;
				$this->user_unix_user{2} = 0;
			}
			
			if($rs->user_delete =='Y'){
				$this->user_permissions->user_delete = true;
				$this->user_unix_user{3} = 1;
			}
			else{
				$this->user_permissions->user_delete = false;
				$this->user_unix_user{3} = 0;
			}
			
			if($rs->user_administrate =='Y'){
				$this->user_permissions->user_administrate = true;
				$this->user_unix_user{4} = 1;
			}
			else{
				$this->user_permissions->user_administrate = false;
				$this->user_unix_user{4} = 0;
			}

			//GROUP
			if($rs->group_read =='Y'){
				$this->user_permissions->group_read = true;
				$this->user_unix_group{1} = 1;
			}
			else{
				$this->user_permissions->group_read = false;
				$this->user_unix_group{1} = 0;
			}
			
			if($rs->group_write =='Y'){
				$this->user_permissions->group_write = true;
				$this->user_unix_group{2} = 1;
			}
			else{
				$this->user_permissions->group_write = false;
				$this->user_unix_group{2} = 0;
			}
			
			if($rs->group_delete =='Y'){
				$this->user_permissions->group_delete = true;
				$this->user_unix_group{3} = 1;
			}
			else{
				$this->user_permissions->group_delete = false;
				$this->user_unix_group{3} = 0;
			}
			
			if($rs->group_administrate =='Y'){
				$this->user_permissions->group_administrate = true;
				$this->user_unix_group{4} = 1;
			}
			else{
				$this->user_permissions->group_administrate = false;
				$this->user_unix_group{4} = 0;
			}
			
			
			
			//OTHER
			if($rs->other_g_read =='Y'){
				$this->user_permissions->other_g_read = true;
				$this->user_unix_others{1} = 1;
			}
			else{
				$this->user_permissions->other_g_read = false;
				$this->user_unix_others{1} = 0;
			}
				
			if($rs->other_g_write =='Y'){
				$this->user_permissions->other_g_write = true;
				$this->user_unix_others{2} = 1;
			}
			else{
				$this->user_permissions->other_g_write = false;
				$this->user_unix_others{2} = 0;
			}
			
			if($rs->other_g_delete =='Y'){
				$this->user_permissions->other_g_delete = true;
				$this->user_unix_others{3} = 1;
			}
			else{
				$this->user_permissions->other_g_delete = false;
				$this->user_unix_others{3} = 0;
			}
			
			if($rs->other_g_administrate =='Y'){
				$this->user_permissions->other_g_administrate = true;
				$this->user_unix_others{4} = 1;
			}
			else{
				$this->user_permissions->other_g_administrate = false;
				$this->user_unix_others{4} = 0;
			}		
		}
		$rs->close();
		
		/**
		 * L'usuari pertany a un grup que no té permis de lectura en la taula 1.
		 * Però volem que aquest usuari en concret pugui tenir lectura en taula 1.
		 * Llavors l'usuari aquest tindrà permis de lectura en taula 1 i haurem de fer sumes de permisos de l'estil:
		 *
		 * G = Permisos de lectura d'usuari en taula 1 via grup
		 * U = Permisos de lectura d'usuari en taula 1 via especificant-ho a l'usuari
		 * R = Resultat dels permisos
		 * 0 = false
		 * 1 = true
		 * G + U = R
		 * 0 + 1 = 1
		 * 0 + 0 = 0
		 * 1 + 0 = 1
		 * 1 + 1 = 1
		 *
		 */
		$this->global_permissions->user_create = (($this->group_permissions->user_create || $this->user_permissions->user_create)? true:false);
		$this->global_unix_user{0} = $this->global_permissions->user_create? 1:0;
		
		$this->global_permissions->user_read = (($this->group_permissions->user_read || $this->user_permissions->user_read)? true:false);
		$this->global_unix_user{1} = $this->global_permissions->user_read? 1:0;
		
		$this->global_permissions->user_write = (($this->group_permissions->user_write || $this->user_permissions->user_write)? true:false);
		$this->global_unix_user{2} = $this->global_permissions->user_write? 1:0;
		
		$this->global_permissions->user_delete = (($this->group_permissions->user_delete || $this->user_permissions->user_delete)? true:false);
		$this->global_unix_user{3} = $this->global_permissions->user_delete? 1:0;
		
		$this->global_permissions->user_administrate = (($this->group_permissions->user_administrate || $this->user_permissions->user_administrate)? true:false);
		$this->global_unix_user{4} = $this->global_permissions->user_administrate? 1:0;
		
		$this->global_permissions->group_read = (($this->group_permissions->group_read || $this->user_permissions->group_read)? true:false);
		$this->global_unix_group{1} = $this->global_permissions->group_read? 1:0; 
		
		$this->global_permissions->group_write = (($this->group_permissions->group_write || $this->user_permissions->group_write)? true:false);
		$this->global_unix_group{2} = $this->global_permissions->group_write? 1:0;
		
		$this->global_permissions->group_delete = (($this->group_permissions->group_delete || $this->user_permissions->group_delete)? true:false);
		$this->global_unix_group{3} = $this->global_permissions->group_delete? 1:0;
		
		$this->global_permissions->group_administrate = (($this->group_permissions->group_administrate || $this->user_permissions->group_administrate)? true:false);
		$this->global_unix_group{4} = $this->global_permissions->group_administrate? 1:0;
		
		$this->global_permissions->other_g_read = (($this->group_permissions->other_g_read || $this->user_permissions->other_g_read)? true:false);
		$this->global_unix_others{1} = $this->global_permissions->other_g_read? 1:0;
		
		$this->global_permissions->other_g_write = (($this->group_permissions->other_g_write || $this->user_permissions->other_g_write)? true:false);
		$this->global_unix_others{2} = $this->global_permissions->other_g_write? 1:0;
		
		$this->global_permissions->other_g_delete = (($this->group_permissions->other_g_delete || $this->user_permissions->other_g_delete)? true:false);
		$this->global_unix_others{3} = $this->global_permissions->other_g_delete? 1:0;
		
		$this->global_permissions->other_g_administrate = (($this->group_permissions->other_g_administrate || $this->user_permissions->other_g_administrate)? true:false);
		$this->global_unix_others{4} = $this->global_permissions->other_g_administrate? 1:0;
	}
	
	/**
	 * $z can be:
	 *  "G" (For Group permissions)
	 *  "U" (For User permissions)
	 *	NULL (For Global permissions AKA G+U).
	 * 
	 * Return UNIX like string permission format (00000) (CRWDA -> Create | Read | Write | Delete | Administrate)
	 * 
	 * @return \stdClass (3 properties, u,g,o each property a 5 characters string)
	 * @param char $z
	 */
	public function getUnixLike($z = null){
		$ret = new \stdClass();
		$ret->u = "00000";
		$ret->g = "00000";
		$ret->o = "00000";
		switch($z){
			case "G":
				$ret->u = $this->group_unix_user;
				$ret->g = $this->group_unix_group;
				$ret->o = $this->group_unix_others;
				break;
			case "U":
				$ret->u = $this->user_unix_user;
				$ret->g = $this->user_unix_group;
				$ret->o = $this->user_unix_others;
				break;
			default:
				$ret->u = $this->global_unix_user;
				$ret->g = $this->global_unix_group;
				$ret->o = $this->global_unix_others;
				break;
		}
		
		return $ret;
	}


	/**
	 * Return user permissions.
	 */
	public function getUser(){
		return $this->user_permissions;
	}
	
	/**
	 * Return group permission.
	 */
	public function getGroup(){
		return $this->group_permissions;
	}
	
	/**
	 * Get Global (user + group) permissions.
	 */
	public function getGlobal(){
		return $this->global_permissions;
	}

	/**
	 * Checks which records can be shown for a current user.
	 * Return a string for READ action in UGO User=(0|1), Group = (0|1), Other = (0|1) 
	 * @return string UNIX like permission view. (binary).
	 */
	public function visibility(){
		$chmodb = "000";
		$chmodb{0} = ($this->global_permissions->user_read? "1":"0");
		$chmodb{1} = ($this->global_permissions->group_read? "1":"0");
		$chmodb{2} = ($this->global_permissions->other_g_read? "1":"0");
		return $chmodb;
	}

	/**
	 * Check Access to a table or record.
	 * Throws appException if access is denied. 
	 * @return \core\security\permission_answer
	 * @param unknown $rid
	 * @throws AppException
	 */
	public function access($rid = null){
		return $this->permission_answer($rid, true);
	}
	
	/**
	 * Check Access to a table or record.
	 * Return false if access is denied.
	 * @return \core\security\permission_answer
	 * @param unknown $rid
	 */
	public function check($rid = null){
		return $this->permission_answer($rid, false);
	}
	
	private function permission_answer($rid,$throw = true){
		//We are trying to access to a Table for create an element.
		if($rid == null){
			//Si es tracta d'un registre que no té id, és ad ir que és nou, comprovem els permisos com si l'usuari fos el propietari.
			$is = new permission_answer($this->global_permissions, $this->table,$throw,self::$environment->user->id,self::$environment->user->user_group);
		}
		else{ //We are trying to access to a Table's element. So we have to check if given a group permission, the user has the right to access on it.
			$owner_user = null;
			$owner_group = null;
			$sql = "SELECT USER_ID,GROUP_ID FROM ".(string) $this->table->db_name." WHERE ".(string) $this->table->primary_key[0]."=:ID";
			$rs = self::$environment->dbcon->execute($sql,array("ID" => $rid));
			if($rs->fetch()){
				$owner_user = $rs->user_id;
				$owner_group = $rs->group_id;
			}
			$rs->close();
			$is = new permission_answer($this->global_permissions,$this->table,$throw,$owner_user, $owner_group);
		}
		return $is;
	}
}

class permission_properties{
	public $user_create = false;
	public $user_read = false;
	public $user_write = false;
	public $user_delete = false;
	public $user_administrate = false;

	public $group_read = false;
	public $group_write = false;
	public $group_delete = false;
	public $group_administrate = false;

	public $other_g_read = false;
	public $other_g_write = false;
	public $other_g_delete = false;
	public $other_g_administrate = false;
}


class permission_answer{
	const CREABLE = "CREABLE";
	const READABLE = "READABLE";
	const WRITABLE = "WRITABLE";
	const DELETABLE = "DELETABLE";
	const ADMINISTRABLE = "ADMINISTRABLE";
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	private $owner_user;
	private $owner_group;
	/**@var permission_properties*/
	private $permissions;
	/**@var Table */
	private $table;
	
	private $throwable;
	
	public function __construct($permissions,$table,$throwable = true,$owner_user = null,$owner_group = null){
		self::$environment = Environment::getInstance();
		$this->permissions = $permissions;
		$this->owner_group = $owner_group;
		$this->owner_user = $owner_user;
		$this->table = $table;
		$this->throwable = $throwable;
	}
	
	/**
	 * Can create user? 
	 */
	public function creable(){
		return $this->access(self::CREABLE);
	}
	
	/**
	 * Can read the record.
	 */
	public function readable(){
		return $this->access(self::READABLE);
	}
	
	/**
	 * Can Write the record.
	 */
	public function writable(){
		return $this->access(self::WRITABLE);
	}
	
	/**
	 * Can delete the record.
	 */
	public function deletable(){
		return $this->access(self::DELETABLE); 
	}

	/**
	 * Can administrate the record (If this is true, user can also READ WRITE AND DELETE)
	 * Full Permission Settings are given if the user has ADMINISTRATOR rights.
	 */
	public function administrable(){
		return $this->access(self::ADMINISTRABLE);
	}
	
	
	private function access($action){
		if(!self::$environment->authentication_required) return true;
		$access = false;
		
		switch($action){
			case self::CREABLE:
				$uaction = 'user_create';
				break;
			case self::READABLE: //Can current user, or current users group, or others groups read this?
				$uaction = 'user_read';
				$gaction = 'group_read';
				$ogaction = 'other_g_read';
				break;
			case self::WRITABLE: //Can current user, or current users group, or others groups write this?
				$uaction = 'user_write';
				$gaction = 'group_write';
				$ogaction = 'other_g_write';
				break;
			case self::DELETABLE: //Can current user, or current users group, or others groups delete this?
				$uaction = 'user_delete';
				$gaction = 'group_delete';
				$ogaction = 'other_g_delete';
				break;
			case self::ADMINISTRABLE: //Can current user, or current users group, or others groups manage this?
				$uaction = 'user_administrate';
				$gaction = 'group_administrate';
				$ogaction = 'other_g_administrate';
				break;
		}

		if($action == self::CREABLE && $this->permissions->$uaction){
			$access = true;
		}
		else{
			/*if($this->table->id==2 && $action == self::ADMINISTRABLE){
				echo "1: (".$this->permissions->$uaction." && ".$this->owner_user." == ".self::$environment->user->id.")\r\n";
				echo "2: (".$this->permissions->$gaction." && ".$this->owner_group." == ".self::$environment->user->user_group.")\r\n";
				echo "3: (".$this->permissions->$ogaction." && ".$this->owner_group."!=".self::$environment->user->user_group.")\r\n";
				die();
			}*/
			
			//Check if user is owner, then check if has permission to do the "action" on this record. (USER)
			if($this->permissions->$uaction && $this->owner_user == self::$environment->user->id){
				$access = true;
			}
			//Check if user has a group which is the owner of the record, then check if has permission to do the "action" on this record. (GROUP)
			else if($this->permissions->$gaction && $this->owner_group == self::$environment->user->user_group){
				$access = true;
			}
			//Check if user has a group which is NOT the owner of the record, then check if has permission to do the "action" on this record. (OTHER GROUPS)
			else if($this->permissions->$ogaction && $this->owner_group!=self::$environment->user->user_group){
				$access = true;
			}
		}
		if(!$access){
			if($this->throwable){
				throw new AppException(Status::S4_Unauthorized,Status::S4_Unauthorized,"Action $action is not allowed on ".$this->table->db_name);
			}
		}
		return $access;
	}
}
?>