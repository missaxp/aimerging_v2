<?php

namespace core\security;

use core\Environment;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;

/**
 * This class has the session-user related info, such as expiring time, connection time...
 * etc.
 *
 * @author phidalgo
 *        
 */
class Session {
	
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	/**
	 * Current session token for this user and session
	 *
	 * @var string
	 */
	public $session_token = null;
	
	/**
	 * Date for the token request.
	 *
	 * @var DateTime
	 */
	public $first_access = null;
	
	/**
	 * Last access to the REST WS with this token.
	 *
	 * @var DateTime
	 */
	public $last_Access = null;
	
	/**
	 * Remote ADDR for the request.
	 *
	 * @var string
	 */
	public $remote_addr = null;
	
	/**
	 * Token's expire date
	 *
	 * @var DateTime
	 */
	public $expire = null;
	public function __construct($token = null) {
		self::$environment = Environment::getInstance ();
		
		if ($token == NULL) {
			throw new AppException ( Status::S5_InternalServerError, ErrorCode::InternalServerError, "Error. Token must be a md5 hash or user id, null given." );
		}
		if ((strlen ( $token ) == 32 && ctype_xdigit ( $token ))) {
			$sql = "SELECT * FROM SESSION_TOKEN WHERE SESSION_TOKEN=:SESSION_TOKEN";
			$rs = self::$environment->dbcon->execute ( $sql, array ("SESSION_TOKEN" => $token));
		} else if (is_numeric ( $token )) {
			$sql = "SELECT * FROM SESSION_TOKEN WHERE USER_ID=:USER_ID ORDER BY FIRST_ACCESS DESC";
			$rs = self::$environment->dbcon->execute ( $sql, array("USER_ID" => $token));
		} else {
			throw new AppException ( Status::S5_InternalServerError, ErrorCode::InternalServerError, "Error. var \$token must be a md5 hash or user id." );
		}
		
		$fetch = $rs->getAll ();
		$rs->close ();
		
		if (count ( $fetch ) == 0) { // User never logged in or token garbage collector are enabled. TODO:: Make token garbage collector.
		} else {
			$this->expire = $fetch[0]["expires"];
			$this->first_access = $fetch[0];
			$this->last_Access = $fetch[0]["last_access"];
			$this->remote_addr = $fetch[0]["remote_addr"];
			$this->session_token = $fetch[0]["session_token"];
		}
		
		unset ( $fetch );
	}
	
	/**
	 * Updates the LAST_ACCESS value when the users makes a request to the WebService.
	 *
	 */
	public function updateAccess() {
		// Actualitzem la data d'última petició amb autenticació (aquesta petició).
		$vars ["LAST_ACCESS"] = self::$environment->getDate();
		$varswhere ["SESSION_TOKEN"] = $this->session_token;
		self::$environment->dbcon->update("SESSION_TOKEN", $vars, "SESSION_TOKEN=:SESSION_TOKEN", $varswhere );
	}
}
?>