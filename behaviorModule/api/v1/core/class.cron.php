<?php
namespace core;

use core\Environment;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;
class cron{
	protected static $cron = null;
	
	protected static $environment;
	private $jobs;
	
	public static function execute(){
		if (!is_null ( self::$cron)) throw new AppException(Status::S5_InternalServerError);
		self::$cron = new self();
		return self::$cron->start();
	}
	
	private function __construct(){
		self::$environment = Environment::getInstance();
		if(!self::$environment->development && $this->environment->serveraddr!=$this->environment->remoteaddr){
			throw new AppException(Status::S4_Forbidden,ErrorCode::Forbidden);
		}
		
		$sql = "SELECT PROCESS FROM SCHEDULES WHERE ACTIVE = 'S' ORDER BY ORDER_PROCESS";
		$rsprog = self::$environment->dbcon->execute($sql);
		while($rsprog->fetch()){
			$this->jobs[] = $rsprog->PROCESS;
		}
		$rsprog->close();
	}
	
	protected static function start(){
		$scripts_job_path = $_SERVER["DOCUMENT_ROOT"]."/".self::$environment->version."/jobs/";
		$dbcon = self::$environment->dbcon;
		$rsp = array();
		foreach (self::$cron->jobs as $job){
			try{
				ob_clean(); //Always do a ob_clean to ensure that the included script will not print anything.
				$result = array();
				$sname = $scripts_job_path.$job.".php";
				$result["job"] = $job;
				$result["script"] = $sname;
				echo "**	START ".$job.": ".nowtime();
				if(!file_exists($sname)) throw new \Exception($job." does not have script in job folder");
				
				include $sname;
				$result["success"] = true;
			}
			catch(\Exception $e){
				$result["error"] = array(
					"message" => $e->getMessage(),
					"file" => $e->getFile(),
					"line" => $e->getLine()
				);
				$result["success"] = false;
			}
			catch(AppException $e){
				$result["error"] = array(
					"message" => $e->getMessage(),
					"file" => $e->getFile(),
					"line" => $e->getLine()
				);
				$result["success"] = false;
			}
			echo "**	END ".$job.": ".nowtime();
			$result["message"] = ob_get_contents();
			ob_clean(); //Always do a ob_clean to ensure that the included script will not print anything.
			$rsp[] = $result;
		}
		return $rsp;
	}
}