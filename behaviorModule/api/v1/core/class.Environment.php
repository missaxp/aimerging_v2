<?php

namespace core;

use core\security\Security;
use core\http\Status;
use core\http\ErrorCode;
use core\Exception\AppException;
use core\config\config;
use Slim\Slim;
use common\Tables;
use core\security\Authentication;
use dataAccess\dbConn as dbConn2;
use dbconn\dbConn;

/**
 * Conté la informació de la configuració general de l'entorn WS REST API
 *
 * @author ctemporal i phidalgo
 */
require BaseDirAPI . apiVersion . '/core/webservice/http/class.Response.php';

/**
 * Classe amb la informació de la configuració general de l'entorn
 */
class Environment {
	
	// ----- Constants -----
	const NODBCON_ERROR = 1;
	const LANG_ERROR = 2;
	protected static $environment;
	
	// ------ Atributs ------
	/**
	 * UI available langs.
	 */
	public $languages = array ();
	
	/**
	 * Conexió amb la BD (IDCPWS) - NOVA ESTRUCTURA DE DADES IDCP2014
	 *
	 * @var dbConn
	 */
	public $dbcon = null;
	
	/**
	 * Connexió amb la BD (WEBTRADUC) - ANTIC IDCP.
	 *
	 * @var dbConn
	 */
	public $dbcon_webtraduc;
	
	/**
	 * Connexió amb la BD (GESTIO) - DGEST
	 *
	 * @var dbConn
	 */
	public $dbcon_gestio;
	
	/**
	 * Connexió amb la DB Schema AI - MySQL
	 * @var dbConn
	 */
	public $dbcon_ai;
	
	/**
	 * Nom del servidor de correu per on s'enviaran els emails del gestor
	 *
	 * @var string
	 */
	public $mail_host = "";
	
	/**
	 * email del webmaster del site, on es reben les notificacions d'error
	 *
	 * @var string
	 */
	public $webmaster_mail = "";
	
	/**
	 * Email address from error messages
	 *
	 * @var string
	 */
	public $error_from_mail;
	
	/**
	 * E-mail d'origen pels e-mails enviats des del gestor
	 *
	 * @var string
	 */
	public $site_mail = "";
	
	/**
	 * Nom descriptiu de la web
	 *
	 * @var string
	 */
	public $web_name = "";
	
	/**
	 * Indica si la web s'està executant en mode desenvolupament
	 *
	 * @var bool
	 */
	public $development = true;
	
	/**
	 * Versió del WS
	 *
	 * @var string
	 */
	public $version = "";
	
	/**
	 * Notifica per email si s'ha produït algun error
	 *
	 * @var bool
	 */
	public $notify_error_by_email = false;
	
	/**
	 * Mostra l'error o l'oculta amb una pàgina d'error genèrica
	 *
	 * @var bool
	 */
	public $hide_error_information = false;
	
	/**
	 * Indica si es vol que netegi el buffer de resposta abans de mostrar un error
	 *
	 * @var bool
	 */
	public $clean_buffer_on_error = true;
	
	/**
	 * Objecte WS REST API SLIM
	 *
	 * @var \Slim\Slim
	 */
	public $app;
	
	/**
	 * Especifica si hi ha o no autenticació.
	 *
	 * @var bool
	 */
	public $authentication_required; // Si hi ha o no autenticació per part de l'usuari
	
	/**
	 * Especifica, en minuts, el temps per el qual un token quedarà sense validesa.
	 *
	 * @var int
	 */
	public $login_timeout; // En minuts.
	
	/**
	 * Especifica el temps màxim d'expiració d'un token.
	 *
	 * @var \DateInterval
	 */
	public $token_expires;
	
	/**
	 * Clase Authentication.
	 *
	 * @var Authentication
	 */
	public $authentication;
	
	/**
	 * Clase Response
	 * phidalgo.
	 *
	 * @var \core\http\Response
	 */
	public $response;
	
	/**
	 * Clase Slim Request
	 * Utilitzem la clase programada per \Slim, per aprofitar el codi.
	 *
	 * @var \Slim\Http\Request
	 */
	public $request;
	
	/**
	 * Usuari DB
	 *
	 * @var string
	 */
	public $DBuser;
	
	/**
	 * Contrasenya DB
	 *
	 * @var string
	 */
	public $DBpass;
	
	/**
	 * Cadena de connexió.
	 *
	 * @var string
	 */
	public $conn_string;
	
	/**
	 * Dades de la petició
	 *
	 * @var mixed Normalment dades descodificades del json en format $data->nom_parametre
	 */
	public $data;
	
	/**
	 * Determines the name of the default authorization function.
	 * Used by api class
	 *
	 * @var string
	 */
	public $authFunct;
	
	/**
	 * Current root URI (after ws version)
	 *
	 * @var string
	 */
	public $rootURI;
	
	/**
	 * Saves the user who makes the request to the webservice.
	 *
	 * @var \core\security\User
	 */
	public $user;
	
	/**
	 * Get's the current user access security to each Module and Action.
	 *
	 * @var Security
	 */
	public $security;
	
	/**
	 * Sets the max allowed rows to be returned on a SQL returnset.
	 *
	 * @var Integer
	 */
	public $maxRows = 0;
	
	/**
	 * Specifies if we will save errors to DB.
	 *
	 * @var boolean
	 */
	public $saveErrors;
	
	/**
	 * Indicates which HTTP status will be saved on DB (for example, only 500 errors).
	 * You can put any interval you want. Follow the interval math rules.
	 *
	 * [500-600) -> From 500 to 599
	 * [500-600] -> From 500 to 600
	 * (500-600) -> From 501 to 599
	 * (500-600] -> From 501 to 600
	 *
	 * And also you can do:
	 *
	 * 500+ -> Equal or greater than 500.
	 *
	 * @var string
	 */
	public $saveErrorStatus;
	
	/**
	 * saves if the environment is configurated
	 *
	 * @var bool
	 */
	private $configurated = false;
	public $configuration = array ();
	private $started = false;
	private $maintenance = false;
	public $serveraddr;
	public $remoteaddr;
	
	public $tables = array();
	
	//Set if slim is running or not.
	public $running = false;
	
	/**
	 * Sets if we do the commit on DB or not.
	 * @var boolean
	 */
	public $doCommit;
	
	/**
	 * Sets default ContentType
	 * @var string
	 */
	public $defaultContentType;
	
	/**
	 * Storage path.
	 * @var string
	 */
	public $storage;
	
	/**
	 * Documents storage path
	 * @var string
	 */
	public $documents;
	
	
	/**
	 * Temporary directory for uploaded files (and another files i should use in future temporary).
	 * @var string
	 */
	public $tmpDir;
	
	/**
	 * Files is saved on db = true, hdd = false;
	 * @var boolean
	 */
	public $filesDB;
	
	public $starttime;
	
	public $avatarMaxWidth;
	public $avatarMaxHeight;
	public $avatarQuality;
	
	/**
	 * Web Service response dateTime Format.
	 * @var string
	 */
	public $dateTimeFormat;
	
	/**
	 * Time Zone Used by send and receive dates.
	 */
	public $timeZone;
	
	/**
	 * HeadQuarters Time Zone.
	 * @var string
	 */
	public $headquartersTimeZone;
	
	
	/**
	 * Database Type, values DBCONN_MYSQL or DBCONN_ORACLE
	 * @var string
	 */
	public $database_type;
	
	/**
	 * Connection scheme (if DB type is MySQL).
	 * @var string
	 */
	public $conn_scheme;
	
	// ----- MÈTODES ------
	
	/**
	 * Si alguna de les classes o el mateix environment té algun error o falla l'execució, permet recolectar la informació de l'environment.
	 *
	 * @param \stdclass $config        	
	 */
	private function setConfig($config = null) {
		if (! is_null ( ($config) )) {
			foreach ( $config as $key => $value ) {
				$this->$key = $value;
			}
			$this->configurated = true;
			$this->configuration = $config;
		}
	}
	
	public static function start($config) {
		self::getInstance ( $config );
		self::$environment->started = true;
	}
	
	/**
	 * Obté la instancia d'entorn
	 *
	 * Crea o retorna una instancia d'entorn
	 * Utilitzant estructura Singleton - http://ca.wikipedia.org/wiki/Patr%C3%B3_singleton
	 *
	 * @param bool $refresh
	 *        	obte una nova instancia d'entorn.
	 * @return Environment
	 */
	public static function getInstance($config = null) {
		if (is_null ( self::$environment )) {
			new self ();
		}
		return self::$environment;
	}

	/**
	 * Constructor de la classe
	 */
	private function __construct() {
		$this->setConfig (config::_GET()); // Apliquem la configuració.
		Slim::registerAutoloader ();
		$this->app = new Slim ();
		$this->app->config ("debug",false); // Prevent the execution of Slim PrettyException Handler through our own custom error handling system.
		$this->app->error ("\core\Exception\AppException::errorHandler" ); // Attach the custom error handler function
		$this->app->config ('cookies.lifetime','2 days');
		$this->request = $this->app->request(); // Sets the request from Slim to Environment (alias)
		$this->rootURI = explode ( "/", $this->app->request->getResourceUri());
		$this->rootURI = $this->rootURI[1];
		$this->serveraddr = $_SERVER["SERVER_ADDR"];
		$this->remoteaddr = $_SERVER["REMOTE_ADDR"];
		$this->authentication_required = ($this->development? $this->authentication_required:true); //Auth required false only available when debug true.
		
		/*
		 * Slim Framework does not setup it's error handler until call to run() method.
		 * So before it, I should set the error handling, to catch anything wrong before run();
		 */
		set_error_handler ( "\core\Exception\AppException::errorHandler" );
		set_exception_handler ( "\core\Exception\AppException::errorHandler" );
		
		self::$environment = $this;
		$this->response = new http\Response();
		$this->app->error("\core\Exception\AppException::errorHandler");
		
		require BaseDirAPI.'/'.$this->version.'/core/security/class.Authentication.php';
		require BaseDirAPI.'/'.$this->version.'/core/security/class.ActionCodes.php';
		require BaseDirAPI.'/'.$this->version.'/core/security/class.Group.php';
		require BaseDirAPI.'/'.$this->version.'/core/security/class.Role.php';
		require BaseDirAPI.'/'.$this->version.'/core/security/class.Session.php';
		require BaseDirAPI.'/'.$this->version.'/core/security/class.User.php';
		require BaseDirAPI.'/'.$this->version.'/core/security/class.Security.php';
		
		require BaseDirAPI.'/'.$this->version.'/core/webservice/class.route.php';
		require BaseDirAPI.'/'.$this->version.'/core/resources/class.Resource.php';
		require BaseDirAPI.'/'.$this->version.'/core/webservice/middlewares/class.runningMiddleware.php';
		require BaseDirAPI.'/'.$this->version.'/core/webservice/middlewares/class.contentLengthMiddleware.php';
		
		$this->app->add(new \runningMiddleware());
		$this->app->add(new \ContentLengthMiddleware());
		
		self::loadCommonScripts();
		self::loadObjects();
				
		$this->setdbConn();
		$this->authentication = new Authentication();
		$this->tables = new Tables();
		
		$rs = $this->dbcon->execute("SELECT * FROM UI_LANGUAGES ORDER BY SORT ASC");
		$this->languages =  $rs->getAll();
		$rs->close();
	}
	
	
	public function maintenance() {
		return $this->maintenance;
	}
	
	public function getDate(){
		return date("Ymd\THis\Z");
	}
	
	/**
	 * Load all scripts from /v{n}/objects folder
	 */
	private static function loadObjects(){
		$env = self::getInstance();
		$objects = glob(BaseDirAPI."/".$env->version."/api/objects/*"); // get all file names
		foreach($objects as $object){ // iterate files
			if(is_file($object)){
				require $object;
			}
		}
	}
	
	/**
	 * Load all scripts on /common folder.
	 */
	private static function loadCommonScripts(){
		$scripts = self::rglob(BaseDirAPI."/common/*.php"); // get all file names
		foreach($scripts as $script){ // iterate files
			if(is_file($script)){
				require $script;
			}
		}
	}
	
	private static function rglob($pattern, $flags = 0) {
		$files = glob($pattern, $flags);
		foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
			$files = array_merge($files, self::rglob($dir.'/'.basename($pattern), $flags));
		}
		return $files;
	}
	
	/**
	 * Estableix la conexió a la BD
	 */
	private function setdbConn() {
		try {
// 			$this->dbcon_ai = new \dataAccess\dbConn($this->database_type, $this->DBuser, $this->DBpass, $this->conn_string, $this->conn_scheme);
			$this->dbcon = new dbConn(DBCONN_ORACLE, "AI", "AI","(DESCRIPTION =(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(Host = cesi.iit.idisc.es)(Port = 1521)))(CONNECT_DATA = (SID = idisc) (SERVER = POOLED)))");
			$this->dbcon_webtraduc = new dbConn( DBCONN_ORACLE, "webtraduc", "webtraduc", "(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = oxigen.iit.idisc.es)(PORT = 1521)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = GESTIO.IDISC.ES)))");
			$this->dbcon_gestio = new dbConn( DBCONN_ORACLE, "gestio", "gestio", "(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = oxigen.iit.idisc.es)(PORT = 1521)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = GESTIO.IDISC.ES)))" );
			$this->dbcon_AI = new dbConn($this->database_type, $this->DBuser, $this->DBpass, $this->conn_string, $this->conn_scheme);
		} catch ( \Exception $e ) {
			//throw new AppException ( Status::S5_ServiceUnavailable, ErrorCode::DBConnectionProblem, $e->getMessage (), null, $e->getFile (), $e->getLine () );
			throw new AppException ( Status::S5_ServiceUnavailable, ErrorCode::DBConnectionProblem); //20151202 canvio throw linia superior per a aquest.
		}
	}
	
	/**
	 * En funció de la ruta, crida el script correcte.
	 */
	public static function route() {
		$env = self::getInstance ();
		$script = "./api/routes/route.".$env->rootURI.".php";
		if (file_exists ( $script )) {
			$class = 'api\route\\'.$env->rootURI;
			require $script;
			$space = new $class ($env->rootURI);
		} else {
			require "./api/routes/route.root.php";
			$space = new \api\route\root($env->rootURI);
		}
		$space->run();
	}
} /* end of class Environment */
?>