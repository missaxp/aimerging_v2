<?php

namespace core\config;

/**
 * This script is a no dependent script which allows to get webservice config in any case.
 * If any thing goes wrong, this pretty simple config script will never fail and
 * If we need to construct a response (error response) in case of everything fails, we could do it using the right configuration settings.
 *
 * This may happen when a core script (such as Environment or Auth,... response,...token,...etc) fails and AppException needs to create an HTTP response and read the correct configuration.
 *
 * So it is MANDATORY to use _SET() and _GET() methods in order to get and set the configuration of this environment.
 * 
 * @author phidalgo
 *        
 */
class config {
	protected static $config;
	private $configuration;

	
	/**
	 * Sets the config, or returns false if config is already set.
	 * Only can be inicializated one time.
	 * 
	 * @param \stdClass $c
	 * @return boolean
	 */
	public static function _SET($c){
		if(is_null(self::$config)){
			self::$config = new self($c);
			return true;
		}
		return false;
	}
	private function __construct($c) {
		$this->configuration = $c;
	}
	
	
	/**
	 * Returns webservice config.
	 */
	public static function _GET(){
		return self::$config->configuration;
	}
}
?>