<?php

namespace api\route;

use core\Environment;
use core\http\Status;
use core\Exception\AppException;
use core\http\ErrorCode;
use core\route;
use dataAccess\dao\TaskDAO;

class tasks extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, $needsAuth );
	}
	
	public function run() {
		$authFunction = $this->authFunction;
		$environment = Environment::getInstance ();
		// GET route
		self::$environment->app->group ( '/'.$this->urlspace, function () use($environment) {
			$environment->app->group ( '/confirm', function () use ($environment) {
				$environment->app->get ( '/:token', function ($token) use ($environment) {
					//try{
					include_once BaseDir.'/behaviorModule/actionsModule/confirmTaskService.php';
					$rsp = getTokenMail($token);
					if($rsp===false){
						throw new AppException(Status::S4_NotFound,ErrorCode::CONFIRM_EMAIL_TOKEN_NOT_FOUND);
					}
					
					$environment->response->addData ($rsp);
					$environment->response->send();
					//}
					//catch(\Exception $e){
					
					//}
					
				});
			});
				
				$environment->app->get ( '/log', function () use ($environment) {
					$environment->response->addData(file_get_contents("/ai/logs/ai.log"));
					$environment->response->send();
				});
					
				$environment->app->get ( '/:usid', function ($usid) use ($environment) {
					$tdao = new TaskDAO();
					$task = $tdao->getTask($usid);
					
					if($task->getUniqueSourceId()==""){
						$environment->response->send(Status::S4_NotFound);
					}
					
					$taskArray = $task->toArray();
					
					//Recorro todos los status history porque necessito la fecha de cambio de estado y al ser propiedad privada, el método toArray no accee a ella.
					$taskArray["statushistory"] = array();
					
					foreach($task->getStatusHistory() as $state){
						$stateMount["statename"] = $state->getStateName();
						$stateMount["idstate"] = $state->getIdState();
						$stateMount["description"] = $state->getDescription();
						$stateMount["timestamp"] = $state->getTimeStamp()->format('Y/m/d H:i:s');
						$taskArray["statushistory"][] = $stateMount;
					}
					
					$environment->response->addData($taskArray);
					$environment->response->send();
					
				});
						
						
						
						
		});
			parent::run(); // Run parent default uri control
	}
}

?>