<?php

namespace api\route;

use core\Environment;
use core\route;

class status extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, $needsAuth );
	}
	public function run() {
		$authFunction = $this->authFunction;
		$environment = Environment::getInstance ();
		// GET route
		
		self::$environment->app->group ( '/'.$this->urlspace, function () use($environment, $authFunction) {
			
			$environment->app->get('/ping',function() use($environment){
				$environment->response->addData("pong");
				$environment->response->send();
			});
			
			
			self::$environment->app->group ( '/', function () use ($environment,$authFunction) {
				$environment->app->get('', function () use ($environment) {
					$rsp = array (
							"development" => $environment->development,
							"database" => $environment->conn_string,
							"maintenance" => $environment->maintenance(),
							"authentication_required" => $environment->authentication_required,
							"commiting" => $environment->doCommit,
							"version" => $environment->version,
							"revision" => $environment->revision,
							"timezone" => $environment->timeZone,
							"hqtz" => $environment->headquartersTimeZone,
							"time" => $environment->getDate()
					);
					$environment->response->addData($rsp);
					$environment->response->send();
				});
					
					$environment->app->get('/:property',$authFunction, function ($property) use ($environment) {
						if (isset($environment->$property)){
							$environment->response->addData(array ("$property" => $environment->$property));
							$environment->response->send();
						} else {
							$environment->response->send ( Status::S4_NotFound );
						}
					});
			});
		});
			
		parent::run(); // Run parent default uri control
	}
}
?>