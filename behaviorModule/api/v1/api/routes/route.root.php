<?php

namespace api\route;

use core\http\ResponseType;
use core\http\Status;
use core\Environment;
use core\cron;
use core\route;
use api\crud\UserManagement;
use api\crud\DepartmentManagement;
use api\crud\GroupManagement;

class root extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, $needsAuth );
	}
	public function run() {
		$authFunction = $this->authFunction;
		$environment = Environment::getInstance ();
		// GET route
		self::$environment->app->get ( '/', function () use ($environment) {
			$environment->response->type = ResponseType::HTML;
			$environment->response->addData ("IDCP Web Service version " . $environment->version);
			$environment->response->send();
		});
		
		$environment->app->get('/ping',function() use($environment){
			$environment->response->addData("pong");
			$environment->response->send();
		});
		
		
		$environment->app->delete('/fk',function() use($environment){
			$environment->dbcon->execute("DELETE FROM TAULA_PROVA1 WHERE PK_1=:PK_1 AND PK_2=2",array("PK_1" => 1));
		});

		$environment->app->get('/dbsize',function() use($environment){
			/*$sql = "SELECT 
			U.COLUMN_NAME,
			U.DATA_TYPE,
			U.DATA_LENGTH,
			U.TABLE_NAME,
			U.data_type
			FROM USER_TAB_COLS U where data_type in('BLOB','CLOB')";
			
			
			
			$rsp = array();
			$rs = $environment->dbcon_gestio->execute($sql);
			$i = 0;
			$sum = 0;
			while($rs->fetch()){
				$table = array();
				$sql = "SELECT sum(dbms_lob.getlength(".$rs->column_name.")/1024/1024) AS S FROM ".$rs->table_name;
				$rs2 = $environment->dbcon_gestio->execute($sql);
				if($rs2->fetch()){
					$table["SIZE"] = $rs2->s;
				}
				$rs2->close();
				if($table["SIZE"] > 100){
					$table["TABLE_NAME"] = $rs->table_name;
					$table["FIELD"] = $rs->column_name;
					$table["DATA_TYPE"] = $rs->data_type;
					//$table["SIZE"] = "SELECT sum(dbms_lob.getlength(".$rs->column_name.")/1024/1024) AS S FROM ".$rs->table_name;
					$rsp[] = $table;
				}
				if($table["SIZE"]!=null){
					$sum = $sum + $table["SIZE"];
				}
			}
			$rsp["TOTAL"] = $sum;
			$rs->close();
			$environment->response->addData($rsp);*/
			$environment->response->send();
		});
		
		self::$environment->app->group ( '/status', function () use ($environment,$authFunction) {
			$environment->app->get('', function () use ($environment) {
				$rsp = array (
						"development" => $environment->development,
						"database" => $environment->conn_string,
						"maintenance" => $environment->maintenance(),
						"authentication_required" => $environment->authentication_required,
						"commiting" => $environment->doCommit,
						"version" => $environment->version,
						"revision" => $environment->revision,
						"timezone" => $environment->timeZone,
						"hqtz" => $environment->headquartersTimeZone,
						"time" => $environment->getDate()
				);
				$environment->response->addData($rsp);
				$environment->response->send();
			});
	
			$environment->app->get('/:property',$authFunction, function ($property) use ($environment) {
				if (isset($environment->$property)){
					$environment->response->addData(array ("$property" => $environment->$property));
					$environment->response->send();
				} else {
					$environment->response->send ( Status::S4_NotFound );
				}
			});
		});
			
		self::$environment->app->get ('/cron',$authFunction, function() use ($environment) {
			require './core/class.cron.php';
			$rsp = cron::execute();
			$environment->response->addData ($rsp);
			$environment->response->send();
		});
		
		/**
		 * Obte la configuració bàsica del webservice, com taules,...
		 */
		self::$environment->app->get('/configuration',$authFunction,function() use ($environment){
			$rsp = array(
				"tables" => $environment->tables->_getBasic()
			);
			if($environment->user->user_department==null){
				$rsp["departments"] = DepartmentManagement::getDepartments(false,array("ID","NAME"));
			}
			require './api/crud/class.AttributesManagement.php';
			$rsp["users"] = UserManagement::getUsers(false,array("ID","USERNAME"));
			
			$rsp["groups"] = GroupManagement::_get(false,array("ID","NAME"));
			
			$environment->response->addData($rsp);
			$environment->response->send();
		});
	
		parent::run(); // Run parent default uri control
	}
}
?>