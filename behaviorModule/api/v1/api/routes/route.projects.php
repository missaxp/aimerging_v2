<?php

namespace api\route;


use core\Environment;
use api\Security\ActionsCode;
use core\route;
use api\crud\ProjectsManagement;

require './api/crud/class.ProjectsManagement.php';
require './api/crud/class.AttributesManagement.php';
require './api/crud/class.LanguagesManagement.php';
require './api/crud/class.CountryManagement.php';
require './api/crud/class.TagsManagement.php';
require './api/crud/class.TeamsManagement.php';
require './api/crud/class.FileManagement.php';

class projects extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, $needsAuth );
	}
	public function run() {
		$authFunction = $this->authFunction;
		
		self::$environment->app->group ( '/' . $this->urlspace,$authFunction, function (){
			$environment = Environment::getInstance ();
			
			
			/**
			 * Check if code already exist
			 * method GET
			 */
			$environment->app->get('/checkCode/:ccid',function($ccid) use ($environment){
				$environment->response->addData(ProjectsManagement::checkCode($ccid));
				$environment->response->send();
			});
			
			/**
			* Search a contact
			* Can accept ?q=
			* method GET
			*/
			$environment->app->get('/contacts',function() use ($environment){
				$data = route::checkParams();
				$environment->response->addData(ProjectsManagement::searchContact(isset($data->q)? $data->q:null));
				$environment->response->send();
			});
			
			
			/**
			 * Get project
			 * url /project/customers
			 * method GET
			 */
			$environment->app->get ('/customers', function () use($environment) {
				$data = route::checkParams();
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData(ProjectsManagement::getCustomers(isset($data->q)? $data->q:null));
				$environment->response->send();
			});
			
			/**
			 * Get final clients
			 * url /project/thirdparty
			 * method GET
			 */
			$environment->app->get ('/thirdparty', function () use($environment) {
				$data = route::checkParams();
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData(ProjectsManagement::getThirdParty(isset($data->q)? $data->q:null));
				$environment->response->send();
			});
			
			/**
			 * Get final client products
			 * url /project/thirdparty/:tpid/products
			 * method GET
			 */
			$environment->app->get ('/thirdparty/:tpid/products', function ($tpid) use($environment) {
				$data = route::checkParams();
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData(ProjectsManagement::getTPProducts($tpid));
				$environment->response->send();
			});
			
			/**
			 * Get project options
			 * url /project/options
			 * method GET
			 */
			$environment->app->get ('/options', function () use($environment) {
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData(ProjectsManagement::getOptions()->get());
				$environment->response->send();
			});
			
			/**
			 * List projects
			 * url /projects
			 * method GET
			 */
			$environment->app->get ('', function () use($environment) {
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData(ProjectsManagement::_get());
				$environment->response->send();
			});
			
			/**
			 * Get project
			 * url /project/:pid
			 * method GET
			 */
			$environment->app->get ('/:pid', function ($pid) use($environment) {
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData(ProjectsManagement::_getById($pid)->get());
				$environment->response->send();
			});
			
			/**
			 * Update project
			 * url /project/:pid
			 * method PUT
			 */
			$environment->app->put ('/:pid', function ($pid) use($environment) {
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData(ProjectsManagement::_update($pid,route::checkParams()));
				$environment->response->send();
			});
			
			/**
			 * Create project
			 * url /project/:pid
			 * method POST
			 */
			$environment->app->post ('', function () use($environment) {
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData(ProjectsManagement::_new(route::checkParams()));
				$environment->response->send();
			});
			
			/**
			 * Delete project
			 * url /project/:pid
			 * method DELETE
			 */
			$environment->app->delete('/:pid', function ($pid) use($environment) {
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData(ProjectsManagement::_delete($pid));
				$environment->response->send();
			});
			
			/**
			 * Get Documents from a Project
			 * Method GET
			 * url /project/:id/documents
			 */
			$environment->app->get('/:id/documents',function($id) use ($environment){
				$rsp = ProjectsManagement::getDocuments($id);
				$environment->response->addData($rsp);
				$environment->response->send();
			});
						
			/**
			 * Add documents on a new (uncreated) Project.
			 * This method is created to upload files while the user is filling the TP form.
			 *
			 * Method POST
			 * url /project/uploadDocuments
			 */
			$environment->app->post('/uploadDocuments',function() use ($environment){
				$data = route::checkParams();
				$rsp = ProjectsManagement::uploadDocuments(null,$data);
				$environment->response->addData($rsp);
				$environment->response->send();
			});
				
			/**
			 * Add files to a Project.
			 * Method POST
			 * url /project/:id/uploadDocuments
			 */
			$environment->app->post('/:id/uploadDocuments',function($id) use ($environment){
				$data = route::checkParams();
				$rsp = ProjectsManagement::uploadDocuments($id,$data);
				$environment->response->addData($rsp);
				$environment->response->send();
			});
		});
		// Run parent default uri control
		parent::run();
	}
}
?>