<?php

namespace api\route;

use api\crud\User;
use core\Environment;
use core\http\Status;
use api\crud\UserManagement;
use api\Security\ActionsCode;
use core\route;
use common\Table;
use api\crud\TagsManagement;

require './api/crud/class.AttributesManagement.php';
require './api/crud/class.CountryManagement.php';
require './api/crud/class.TagsManagement.php';
require './api/crud/class.LanguagesManagement.php';
require './api/crud/class.QualificationsManagement.php';
require './api/crud/class.ModuleManagement.php';
require './api/crud/class.ChapterManagement.php';

class users extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, $needsAuth );
	}
	public function run() {
		$authFunction = $this->authFunction;
		self::$environment->app->group ( '/' . $this->urlspace,$authFunction, function () {
			$environment = Environment::getInstance ();
			
			/**
			 * Users User Interface Configuration.
			 * UI API'S.
			 * @param :uid user_id
			 */
			//Route params in group could be placed in the callback from their childs:
			//http://stackoverflow.com/questions/31329696/slim-route-group-not-passing-url-variable-to-the-callback
			$environment->app->group('/interface/:iid',function () use($environment) {
				/**
				 * Updates UI setup
				 * url - /users/interface/:namespace/:iid
				 * method - PUT
				 */
				$environment->app->put('', function($iid) use($environment) {
					$data = route::checkParams();
					UserManagement::updateInterfaceSetup($iid,$data);
					$environment->response->send();
				});
			});
			
			/** GROUP TAGS */
			$environment->app->group('/tags',function() use($environment){
				/**
				 * Get available tags from users elements
				 */
				$environment->app->get('',function() use($environment){
					$environment->response->addData (TagsManagement::getTagsList(Table::USERS));
					$environment->response->send();
				});
						
				$environment->app->post('',function() use($environment){
					$data = route::checkParams ('id_parent','name'); // Check params and also returns _get && _post object params
					$environment->response->addData(TagsManagement::addTag($data,Table::USERS));
					$environment->response->send(Status::S2_Created);
				});
			});
			
			
			/**
			 * List Users 
			 * url /users
			 * method GET
			 */
			$environment->app->get ( '', function () use($environment) {
				$environment->response->addData (UserManagement::getUsers()); // Return data
				$environment->response->send();
			});
			
			/**
			 * Get all available options to assign a User. (available countries, available tags,...)
			 * Method GET
			 * url /users/options
			 */
			$environment->app->get('/options',function() use ($environment){
				$environment->response->addData(UserManagement::getUserbyId()->get());
				$environment->response->send();
			});
			
			/**
			 * Get a user
			 * url /users/:id
			 * method GET
			 */
			$environment->app->get ( '/:id', function ($id) use($environment) {
				if ($environment->authentication_required && ($id == "me" || $id == $environment->user->username)) {
					$user = UserManagement::getUserbyId($environment->user->id)->get();
				} else {
					$user = UserManagement::getUserbyId($id)->get();
				}
				// Return data
				$environment->response->addData($user);
				$environment->response->send();
			});
			
			/**
			 * New User
			 * url - /users
			 * method - POST
			 * params - name, email, password
			 */
			$environment->app->post ('', function () use($environment) {
				$data = route::checkParams(); // Check params and also returns _get && _post object params
				$rsp = UserManagement::setUser(null, $data);
				$environment->response->addData($rsp);
				$environment->response->send (Status::S2_Created);
			});
			
			/**
			 * Update User.
			 * url /users/:id
			 * method PUT
			 */
			$environment->app->put ( '/:id', function ($id) use($environment){
				if($id=='me'){
					$id = $environment->user->id;
				}
				$data = route::checkParams(); // Check params and also returns _get && _post object params
				UserManagement::setUser($id,$data);
				$environment->response->send();
			});
						
			/**
			 * Delete multiple users
			 * url /users
			 * method DELETE
			 */
			$environment->app->delete ('', function () use($environment) {
				$data = route::checkParams('uids');
				$environment->security->hasAccesToAction (ActionsCode::USER_DELETE); // Check permissions
				UserManagement::deleteUsers ($data); // Delete user
				$environment->response->send(); // Return data
			});
							
			/**
			 * Delete a user
			 * url /users/:id
			 * method DELETE
			 */
			$environment->app->delete ('/:id', function ($id) use($environment) {
				UserManagement::deleteUser ($id); // Delete user
				$environment->response->send(); // Return data
			});
			
			/**
			 * Gets Basic User Information
			 * url /users/:id/basic
			 * method GET
			 */
			$environment->app->get ('/:id/basic', function ($id) use($environment) {
				if ($environment->authentication_required && ($id == "me" || $id == $environment->user->username)) {
					$user = UserManagement::getUserbyId($environment->user->id,false);
					$rsp = $user->loadBasic();
				} else {
					$user = UserManagement::getUserbyId($id,false);
					$rsp = $user->loadBasic();
				}
				// Return data
				$environment->response->addData($rsp);
				$environment->response->send();
			});
			
			/**
			 * Check if username is already in use.
			 * Method GET
			 * url /users/exists/:id
			 */
			
			$environment->app->get('/exist/:uname' , function ($uname) use($environment) {
				$environment->response->addData(User::checkIfUsernameIsValid($uname));
				$environment->response->send();
			});
			
			/**
			 * Upload avatar
			 * url - /users/:uid/avatar
			 * method - POST
			 */
			$environment->app->post ('/:uid/avatar', function ($uid) use($environment) {
				$data = route::checkParams();
				$rsp = UserManagement::uploadAvatar($uid,$data);
				$environment->response->addData($rsp);
				$environment->response->send(Status::S2_Created);
			});
			
			/**
			 * Users curriculums api.
			 * CURRICULUMS API'S.
			 */
			//Route params in group could be placed in the callback from their childs: http://stackoverflow.com/questions/31329696/slim-route-group-not-passing-url-variable-to-the-callback
			$environment->app->group('/:uid/curriculums',function () use($environment) {
				require './api/crud/class.FileManagement.php';
				/**
				 * List curriculums
				 * url - /users/:uid/curriculums
				 * method - GET
				 */
				$environment->app->get ('', function ($uid) use($environment) {
					$environment->response->addData(UserManagement::getCurriculums($uid));
					$environment->response->send();
				});
				
				/**
				 * New Curriculum
				 * url - /users/:uid/curriculums
				 * method - POST
				 */
				$environment->app->post ('', function ($uid) use($environment) {
					$data = route::checkParams();
					$rsp = UserManagement::uploadCurriculums($uid,$data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_Created);
				});
			});
			
			/**
			 * Check if the user is logged.
			 * url /users/:id/isLogged
			 * method POST
			 */
			$environment->app->post ( '/:id/isLogged', function ($id) use($environment) {
				// Check permissions
				if ($id == 'me' || $environment->user->id == $id) {
					$environment->security->hasAccesToAction ( ActionsCode::USER_MY_VIEW );
					$user = $environment->user;
				} else {
					$environment->security->hasAccesToAction ( ActionsCode::USER_VIEW );
					$user = new User ( $id );
				}
				if($environment->authentication_required){
					$data ["expires"] = $user->session->expire;
					$data ["svr"] = $environment->getDate();
					$data ["timeout"] = $environment->login_timeout * 60;
				}
				else{
					$fecha = new \DateTime();
					$fecha->add(new \DateInterval('P6M'));
					$data["expires"] = $fecha->format($environment->dateTimeFormat);
					$data["svr"] = $environment->getDate();
					$data["timeout"] = 10000*60;
				}
				$environment->response->addData ( $data );
				$environment->response->send();
				
			})->conditions (array('id' => '[0-9]+|me'));
		});
		
		// Run parent default uri control
		parent::run ();
	}
}
?>