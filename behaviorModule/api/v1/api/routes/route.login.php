<?php
namespace api\route;

/**
 * Class API Login.
 * 
 * Performs the RESTFULL requests to the \login URL
 * @author phidalgo
 *
 */

use core\Environment;
use core\route;

class login extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, false );
	}
	public function run() {
		/**
		 * User Login
		 * url - /login
		 * method - POST
		 * params - email, password
		 */
		self::$environment->app->post ( '/login', $this->authFunction, function () {
			$environment = Environment::getInstance ();
			$data = route::checkParams ( 'user', 'password' ); // Check params and also returns _get && _post object params
			$environment->authentication->checkCredentials( $data->user, $data->password ); // check for correct email and password
			$valid = $environment->authentication->getValidToken ($data->user);
			$environment->app->setCookie('session_token', $valid->token );
			$environment->response->addData($valid);
			$environment->response->send ();
		});
		
		// Run parent default uri control
		parent::run ();
	}
}
?>