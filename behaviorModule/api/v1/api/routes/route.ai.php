<?php

namespace api\route;


use core\Environment;
use core\route;
use api\crud\AIManagement;
use dataAccess\dao\ProcessDAO;
use dataAccess\dao\RuleDAO;
use behaviorModule\Behavior;
use model\Task;
use dataAccess\dao\TaskDAO;
use core\AI as AICore;
use dataAccess\dao\SourceDAO;
// use api\crud\ProcessDAO;
// use api\crud\RuleDAO;


require './api/crud/class.AIManagement.php';
// require './api/crud/class.ProcessDAO.php';

class ai extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, $needsAuth );
	}
	public function run() {
		$authFunction = $this->authFunction;
		
		self::$environment->app->group ( '/' . $this->urlspace, $authFunction, function (){
			$environment = Environment::getInstance ();
			
			/**
			 * List source
			 * url /source
			 * method GET
			 */
			$environment->app->get ('/source', function () use($environment) {
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData(AIManagement::_getSources());
				$environment->response->send();
			});
			
			
			/**
			 * List errors
			 * url /error
			 * method GET
			 */
			$environment->app->get ('/error', function () use($environment) {
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData(AIManagement::_getErrors());
				$environment->response->send();
			});
			
			/**
			 * List log
			 * url /log
			 * method GET
			 */
			$environment->app->get ('/log', function () use($environment) {
				$opt = array("timeout"=>30,"useragent"=>'IDISC_CONNECTOR',"connecttimeout"=>5);
				$method = "GET";
				$url = "ai.idisc.es/api/v1/tasks/log";
				//$this->auth = $auth;
				$ch = curl_init();
				
				curl_setopt($ch,CURLOPT_URL,$url);
				curl_setopt($ch,CURLOPT_USERAGENT,"iDISC Connector");
				curl_setopt($ch,CURLOPT_TIMEOUT,"30");
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
				$output = curl_exec($ch); //Enviem la consulta.
				
				$output = json_decode($output, true);
				//return $output["data"];
				
				
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData(nl2br($output["data"]));
				$environment->response->send();
			});
			
			/**
			 * List tasks
			 * url /tasks
			 * method GET
			 */
			$environment->app->get ('/task', function () use($environment) {
				
				$environment->response->addData(AIManagement::_getTasks());
				$environment->response->send();

				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				
			});
			
			/**
			 * Get a task
			 * url /task/:usid
			 * method GET
			 */
			$environment->app->get ('/task/:usid', function ($usid) use($environment) {
				$tdao = new TaskDAO();
				$task = $tdao->getTask($usid);
				
				if($task->getUniqueSourceId()==""){
					$environment->response->send(Status::S4_NotFound);
				}

				
				$taskArray = $task->toArray();
				
				//Recorro todos los status history porque necessito la fecha de cambio de estado y al ser propiedad privada, el método toArray no accee a ella.
				$taskArray["statushistory"] = array();
				
				
				
				foreach($task->getStatusHistory() as $state){
					$stateMount["statename"] = $state->getStateName();
					$stateMount["idstate"] = $state->getIdState();
					$stateMount["description"] = $state->getDescription();
					$stateMount["timestamp"] = $state->getTimeStamp()->format('Y/m/d H:i:s');
					$taskArray["statushistory"][] = $stateMount;
				}
				
				$taskArray["duedate"] = $task->getDueDate();
				
				$environment->response->addData($taskArray);
				$environment->response->send();
			});
			
			$environment->app->group('/process',function () use ($environment){
				/**
				* List process
				* url /process
				* method GET
			 	*/
				$environment->app->get ('', function () use($environment) {
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
					$environment->response->addData(AIManagement::_getProcess());
					$environment->response->send();
				});

				$environment->app->delete('/:idProcess',function ($idProcess) use ($environment){
// 					$processDAO = new ProcessDAO();
					$processDAO = new \dataAccess\dao\ProcessDAO(true);
					$result = $processDAO->deleteProcess($idProcess);
					// die(print("<pre>".print_r($result, true)."</pre>"));
					$environment->response->addData($result);
					$environment->response->send();

				});

				$environment->app->post('/clone/:idProcess',function($idProcess) use ($environment){
					$data = route::checkParams ();
					$processDAO = new \dataAccess\dao\ProcessDAO(true);
					$result = $processDAO->cloneProcess($idProcess,$data->process);
					// die(print("<pre>".print_r($result, true)."</pre>"));
					$environment->response->addData($result);
					$environment->response->send();
				});
			
				/**
				 * Get the Fuzzies from the selected TMS
				 * url /process/fuzzies
				 * method GET
				 */
				$environment->app->get ('/fuzzies', function () use($environment) {
					$task = new Task();
					$tms = Behavior::tms($task, AICore::getInstance()->getSetup()->tms_develoment);
					$environment->response->addData($tms->getFuzziesTMS());
					$environment->response->send();
				});

				/**
				 * Get the Fuzzies from the selected TMS
				 * url /process/fuzzies
				 * method GET
				 */
				$environment->app->get ('/filecategories', function () use($environment) {
					$task = new Task();
					$tms = Behavior::tms($task, AICore::getInstance()->getSetup()->tms_develoment);
					$environment->response->addData($tms->getFileCategoriesTMS());
					$environment->response->send();
				});

				/**
				* Get a process
				* url /process/:pid
				* method GET
				*/
				$environment->app->get ('/:pid', function ($pid) use($environment) {
					//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
// 					$processDAO = new ProcessDAO();
					$processDAO = new \dataAccess\dao\ProcessDAO(true);
					$languageDAO = new \dataAccess\dao\languageDAO();
					$task = new Task();
					
					
					$tms = Behavior::tms($task, AICore::getInstance()->getSetup()->tms_develoment);
					$process = $processDAO->getProcess($pid);
					if (isset($process)) {
						$process = $process->toArray();
						$process['languages'] = $languageDAO->getAllLanguage();

						$process['taskproperties'] = $processDAO->getPropertyRules((isset($process['idSource'])?$process['idSource']:1));
						// $process['propertiesvalues'] = AIManagement::_getTMSProperties();
						$process['propertiesvalues'] =$tms->getTmsProperties();
						$process['ActionCreateProperties'] = $processDAO->getAditionalPropertiesForActionCreate();
				
						if(isset($process['actions']) && count($process['actions']) > 0){
							$count = 0;
							$validation = false;
							foreach ($process['actions'] as $action){
								if(isset($action['idCreate'])){
									$validation = true;
									break;
								}
								$count++;
							}
							if ($validation) {
								foreach ($process['actions'][$count]['propertyActions'] as &$property) {
									if ($property['propertyName'] == 'folderType' || $property['propertyName'] == 'estat' || $property['propertyName'] == 'folderCreationNames'){
						                $property['propertyValue'] = $processDAO->getValueProperty($property['propertyName'],$property['propertyValue']);
						            }
								}
								$process['actions'][$count]['aditionalProperties'] = $processDAO->getAditionalPropertiesForActionCreate();
							}
						}
						
						$sdao = new SourceDAO();
						
						$source = $sdao->getSourceById($process["idSource"]);
						
						$process["sourceName"] = "";
						if($source!=null){
							$process["sourceName"] = $source->getSourceName();
						}
						
					}else{
						$process = array();
					    $process['taskproperties'] = $processDAO->getPropertyRules((isset($process['idSource'])?$process['idSource']:1));
						$process['propertiesvalues'] = AIManagement::_getTMSProperties();
						$process['ActionCreateProperties'] = $processDAO->getAditionalPropertiesForActionCreate();
					}
					
					
					
// 					$process['actions]
// 					$process['propertiesvalues'] = AIManagement::_getTMSProperties();
					$environment->response->addData($process);
					$environment->response->send();
				});

				/**
				* Save a process
				* url /process
				* method POST
				*/
				$environment->app->post('', function () use($environment) {
					$data = route::checkParams ();
// 					$processDAO = new ProcessDAO(); 
					// $processDAO = new \dataAccess\dao\ProcessDAO(true);

					// $result = $processDAO->saveProcess($data);
					// $newProcess = $result['data'];
					// if (isset($newProcess)) {
					// 	$newProcess = $newProcess->toArray();
					// 	$newProcess['taskproperties'] = $processDAO->getPropertyRules((isset($newProcess['idSource'])?$newProcess['idSource']:1));
					// 	$newProcess['propertiesvalues'] = AIManagement::_getTMSProperties();
					// 	$newProcess['ActionCreateProperties'] = $processDAO->getAditionalPropertiesForActionCreate();
					// 	if(isset($newProcess['actions']) && count($newProcess['actions']) > 0){
					// 		$count = 0;
					// 		$validation = false;
					// 		foreach ($newProcess['actions'] as $action){
					// 			if(isset($action['idCreate'])){
					// 				$validation = true;
					// 				break;
					// 			}
					// 			$count++;
					// 		}
					// 		if ($validation) {
					// 			foreach ($newProcess['actions'][$count]['propertyActions'] as &$property) {
					// 				if ($property['propertyName'] == 'folderType' || $property['propertyName'] == 'estat'){
					// 	                $property['propertyValue'] = $processDAO->getValueProperty($property['propertyName'],$property['propertyValue']);
					// 	            }
					// 			}
					// 			$newProcess['actions'][$count]['aditionalProperties'] = $processDAO->getAditionalPropertiesForActionCreate();
					// 		}
					// 	}
					// }
					
					// $result['data'] = $newProcess;
					// $result['valor'] = count($newProcess['actions']) > 0;
					$environment->response->addData($data);
					$environment->response->send();
				});

				$environment->app->put('/:processId', function ($processId) use ($environment){
					$data = route::checkParams();
					$processDAO = new ProcessDAO(true);
					$result = $processDAO->updateProcess($data,$processId);
					$environment->response->addData($result);
					$environment->response->send();
				});

				$environment->app->put('/enabled/:idProcess',function ($idProcess) use ($environment){
					$data = route::checkParams();
					$processDAO = new ProcessDAO();
					$result = $processDAO->enabledProcess($idProcess,$data);

					$environment->response->addData($result);
					$environment->response->send();
				});

				$environment->app->group('/rule',function () use ($environment){
					$environment->app->post('', function () use ($environment){
// 						$ruleDAO = new RuleDAO();
						$ruleDAO = new RuleDAO();
						$newRule = route::checkParams();
						$result = $ruleDAO->saveRule($newRule);

						$environment->response->addData($result);
						$environment->response->send();
					});

					$environment->app->put('/:idRule', function ($idRule) use ($environment){
						$ruleDAO = new RuleDAO();
						$rule = route::checkParams();
						$result = $ruleDAO->updateRule($rule);

						$environment->response->addData($result);
						$environment->response->send();
					});
					$environment->app->delete('/:idRule', function ($idRule) use ($environment){
						$ruleDAO = new RuleDAO();
						$rule = route::checkParams();

						$result = $ruleDAO->deleteRule($idRule);
						$environment->response->addData($result);
						$environment->response->send();
					});
				});

				$environment->app->group('/action', function () use ($environment){

					$environment->app->post('', function() use ($environment){
						$data = route::checkParams ();
						$processDAO = new ProcessDAO(true);
						$result = $processDAO->saveActions($data);
						$environment->response->addData($result);
						$environment->response->send();
					});

					$environment->app->put('/:idAction', function($idAction) use ($environment){
						$data = route::checkParams ();
						$processDAO = new ProcessDAO(true);
						$result = $processDAO->updateActions($data);
						$environment->response->addData($result);
						$environment->response->send();
					});

					$environment->app->delete('/:idAction',function ($idAction) use ($environment){
						$data = route::checkParams();
						$processDAO = new ProcessDAO(true);
						$result = $processDAO->deleteAction($data);
						
						$environment->response->addData($result);
						$environment->response->send();
					});
				});
			});
		});
		// Run parent default uri control
		parent::run();
	}
}
?>