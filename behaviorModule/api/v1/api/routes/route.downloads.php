<?php

namespace api\route;

use core\Environment;
use api\crud\User;
use core\auth\Authentication;
use api\crud\memoq;
use core\route;

require_once './api/crud/class.memoq.php';
class downloads extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, false );
	}
	public function run() {
		$authFunction = $this->authFunction;
		
		self::$environment->app->group ( '/' . $this->urlspace, function () use($authFunction) {
			$environment = Environment::getInstance ();
			
			/**
			 * Download a file
			 * url /downloads/requestDownload
			 * method GET
			 */
			$environment->app->get ( '/:auth/:dall/:id', $authFunction, function ($auth, $dall, $id) use($environment) {
				$aut = new Authentication ();
				$aut->Validate ( $auth );
				$environment->user = new User ( $auth, true ); // If we successfull login to the webService, assign the user to the webservice request.
				
				$data = memoq::downloadMemoqFile ( $id );
				
				header ( 'Content-Type : multipart/form-data' );
				header ( 'Pragma : public' );
				header ( "Expires : 0" ); // set expiration time
				header ( "Cache-Control : must-revalidate, post-check=0, pre-check=0" );
				header ( 'Content-disposition : attachment; filename=' . $data->name );
				header ( 'Content-Transfer-Encoding : binary' );
				header ( 'Content-Length : ' . $data->size );
				print $data->file;
				flush ();
			} );
			
			$environment->app->group ( '/exports', function () use($authFunction, $environment) {
				$environment = Environment::getInstance ();
				
				$environment->app->get ( '/excel/:auth/:filename', $authFunction, function ($auth, $filename) use($environment) {
					$aut = new Authentication ();
					$aut->Validate ( $auth );
					$environment->user = new User ( $auth, true ); // If we successfull login to the webService, assign the user to the webservice request.
					
					$buffer = file_get_contents ( $_SERVER ["DOCUMENT_ROOT"] . "/tmp/" . $filename );
					
					header ( 'Content-Type : application/vnd.ms-excel' );
					header ( 'Pragma : public' );
					header ( "Expires : 0" ); // set expiration time
					header ( "Cache-Control : must-revalidate, post-check=0, pre-check=0" );
					header ( 'Content-disposition : attachment; filename=' . $filename );
					header ( 'Content-Transfer-Encoding : binary' );
					header ( 'Content-Length : ' . strlen ( $buffer ) );
					print $buffer;
					flush ();
				} );
			} );
		} );
		// Run parent default uri control
		parent::run ();
	}
}

?>