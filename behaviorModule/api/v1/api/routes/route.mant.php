<?php

namespace api\route;

use core\http\Status;
use core\route;
use api\crud\AttributesManagement;
use api\crud\TagsManagement;
use api\crud\MantManagement;
use core\Environment;

use api\crud\LanguagesManagement;
use api\crud\CurrencyManagement;
use api\crud\BanksManagement;
use api\crud\NatureManagement;
use api\crud\HoldingsManagement;
use api\crud\DocumentTypesManagement;
use api\crud\ContactSourcesManagement;
use api\crud\ContactTypesManagement;
use api\crud\ContactIndustriesManagement;
use api\crud\CountryManagement;
use api\crud\QualificationsManagement;
use api\crud\ChapterManagement;

require './api/crud/class.MantManagement.php';
require './api/crud/class.LanguagesManagement.php';
require './api/crud/class.BanksManagement.php';
require './api/crud/class.NatureManagement.php';
require './api/crud/class.HoldingsManagement.php';
require './api/crud/class.CountryManagement.php';
require './api/crud/class.CurrencyManagement.php';
require './api/crud/class.ContactSourcesManagement.php';
require './api/crud/class.ContactTypesManagement.php';
require './api/crud/class.ContactIndustriesManagement.php';
require './api/crud/class.DocumentTypesManagement.php';
require './api/crud/class.AttributesManagement.php';
require './api/crud/class.TagsManagement.php';
require './api/crud/class.QualificationsManagement.php';

class mant extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, $needsAuth );
	}
	public function run() {
		$authFunction = $this->authFunction;
		$environment = self::$environment;
		
		/**
		 * RESTRICTED AREA MODULE -> MANT.
		 */
		self::$environment->app->group ('/'.$this->urlspace,$authFunction, function() use($environment) {
			/** LANGUAGES  **/			
			$environment->app->group ( '/languages', function (){
				$environment = Environment::getInstance ();
					
				/**
				 * Get list of languages
				 * Method GET
				 * url /languages
				*/
				$environment->app->get ('' , function () use($environment) {
					$rsp = LanguagesManagement::getLanguages();
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
								
			
				/**
				 * Get language by id
				 * Method GET
				 * url /languages/id
				*/
				$environment->app->get ('/:id' , function ($id) use($environment) {
					$rsp = LanguagesManagement::getLanguageById($id);
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
				
				
				/**
				 * Check primary key language
				 * Method GET
				 * url /languages/exists/id
				 */
				
				 $environment->app->get ('/exist/:id' , function ($id) use($environment) {
					$environment->response->addData (LanguagesManagement::languagesNotExist($id));										
					$environment->response->send();
				});
				
						
				/**
				 * Add NEW language
				 * Method POST
				 * url /languages
				*/
				$environment->app->post ('' , function () use($environment) {
					$data = route::checkParams('culture_code', "name");
					$rsp = LanguagesManagement::addLanguage(null, $data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_Created);
				});
			
							
							
				/**
				 * Add UPDATE language
				 * Method PUT
				 * url /languages/id
				*/
				$environment->app->put ('/:id' , function ($id) use($environment) {
					$data = route::checkParams();
					$rsp =  LanguagesManagement::addLanguage($id,$data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_OK);
				});
			
				/**
				 * DELETE multiple language
				 * Method DELETE
				 * url /languages
				 * post array
				*/
					
				$environment->app->delete('', function () use ($environment) {
					$data = route::checkParams();					
					$rsp  = LanguagesManagement::deleteLanguages($data);					
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_OK);

				});

				/**
				 * DELETE language
				 * Method DELETE
				 * url /languages/id
				*/
				$environment->app->delete ( '/:id', function ($id) use($environment) {
					LanguagesManagement::deleteLanguageById($id);
					$environment->response->send ( Status::S2_OK );
				});
			
			});
			
			/** BANKS **/				
			$environment->app->group ( '/banks', function (){
				$environment = Environment::getInstance ();
					
				/**
				 * Get list of banks
				 * Method GET
				 * url /banks
				*/
				$environment->app->get ('' , function () use($environment) {
					$rsp = BanksManagement::getBanks();					
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
			
						
				/**
				 * Get banks by id
				 * Method GET
				 * url /banks/id
				*/
				$environment->app->get ('/:id' , function ($id) use($environment) {
					$rsp = BanksManagement::getBankById($id);
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
			
			
				/**
				 * Check primary key bank
				 * Method GET
				 * url /banks/exists/id
				*/
	
				$environment->app->get ('/exist/:id' , function ($id) use($environment) {
					$rsp = BanksManagement::bankNotExist($id);
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
			
			
				/**
				 * Add NEW bank
				 * Method POST
				 * url /banks
				*/
				$environment->app->post ('' , function () use($environment) {
					$data = route::checkParams('id', "name");
					$rsp = BanksManagement::addBank(null, $data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_Created);
				});
									
									
									
				/**
				 * Add UPDATE bank
				 * Method PUT
				 * url /banks/id
				*/
				$environment->app->put ('/:id' , function ($id) use($environment) {
					$data = route::checkParams();
					$rsp =  BanksManagement::addBank($id,$data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_OK);
				});
										
				/**
				 * DELETE multiple bank
				 * Method DELETE
				 * url /banks
				 * post array
				*/
					
				$environment->app->delete('', function () use ($environment) {
					$data = route::checkParams();
					$rsp  = BanksManagement::deleteBanks($data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_OK);

				});
			
				/**
				 * DELETE bank
				 * Method DELETE
				 * url /banks/id
				*/
				$environment->app->delete ( '/:id', function ($id) use($environment) {
					BanksManagement::deleteBankById($id);
					$environment->response->send ( Status::S2_OK );
				});
												
			});
			
			/** CURRENCY **/				
			$environment->app->group ( '/currency', function (){
				$environment = Environment::getInstance ();
						
					/**
					 * Get list of currency
					 * Method GET
					 * url /currency
					*/
					$environment->app->get ('' , function () use($environment) {
						$rsp = CurrencyManagement::getCurrencies();						
						$environment->response->addData ($rsp);
						$environment->response->send(Status::S2_OK);
					});
				
							
					/**
					 * Get currency by id
					 * Method GET
					 * url /curreny/id
					*/
					$environment->app->get ('/:id' , function ($id) use($environment) {
						$rsp = CurrencyManagement::getCurrencyById($id);
						$environment->response->addData ($rsp);
						$environment->response->send(Status::S2_OK);
					});
				
				
					/**
					 * Check primary key currency
					 * Method GET
					 * url /currency/exists/id
					*/
		
					$environment->app->get ('/exist/:id' , function ($id) use($environment) {
						$rsp = CurrencyManagement::currencyNotExist($id);
						$environment->response->addData ($rsp);
						$environment->response->send(Status::S2_OK);
					});
		
		
					/**
					 * Add NEW currency
					 * Method POST
					 * url /currency
					*/
					$environment->app->post ('' , function () use($environment) {
						$data = route::checkParams('id', "name");
						$rsp = CurrencyManagement::addCurrency(null, $data);
						$environment->response->addData($rsp);
						$environment->response->send(Status::S2_Created);
					});
								
										
										
					/**
					 * Add UPDATE currency
					 * Method PUT
					 * url /currency/id
					*/
					$environment->app->put ('/:id' , function ($id) use($environment) {
						$data = route::checkParams();
						$rsp =  CurrencyManagement::addCurrency($id,$data);
						$environment->response->addData($rsp);
						$environment->response->send(Status::S2_OK);
					});
							
					/**
					 * DELETE multiple currency
					 * Method DELETE
					 * url /currency
					 * post array
					*/
						
					$environment->app->delete('', function () use ($environment) {
						$data = route::checkParams();
						$rsp  = CurrencyManagement::deleteCurrencies($data);
						$environment->response->addData($rsp);
						$environment->response->send(Status::S2_OK);

					});
				
					/**
					 * DELETE currency
					 * Method DELETE
					 * url /currency/id
					*/
					$environment->app->delete ( '/:id', function ($id) use($environment) {
						CurrencyManagement::deleteCurrencyById($id);
						$environment->response->send ( Status::S2_OK );
					});
													
				});
			
			/** NATURE **/
			$environment->app->group ( '/nature', function (){
				$environment = Environment::getInstance ();
			
				/**
				 * Get list of nature
				 * Method GET
				 * url /nature
				*/
				$environment->app->get ('' , function () use($environment) {
					$rsp = NatureManagement::getNatures();
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
			
						
				/**
				 * Get nature by id
				 * Method GET
				 * url /nature/id
				*/
				$environment->app->get ('/:id' , function ($id) use($environment) {
					$rsp = NatureManagement::getNatureById($id);
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
		
		
				/**
				 * Check primary key nature
				 * Method GET
				 * url /nature/exists/id
				*/
	
				$environment->app->get ('/exist/:id' , function ($id) use($environment) {
					$rsp = NatureManagement::natureNotExist($id);
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
	
	
				/**
				 * Add NEW nature
				 * Method POST
				 * url /nature
				*/
				$environment->app->post ('' , function () use($environment) {
					$data = route::checkParams('id', "name");
					$rsp = NatureManagement::addNature(null, $data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_Created);
				});



				/**
				 * Add UPDATE nature
				 * Method PUT
				 * url /nature/id
				*/
				$environment->app->put ('/:id' , function ($id) use($environment) {
					$data = route::checkParams();
					$rsp =  NatureManagement::addNature($id,$data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_OK);
				});
						
				/**
				 * DELETE multiple nature
				 * Method DELETE
				 * url /nature
				 * post array
				*/

				$environment->app->delete('', function () use ($environment) {
					$data = route::checkParams();
					$rsp  = NatureManagement::deleteNatures($data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_OK);

				});

				/**
				 * DELETE nature
				 * Method DELETE
				 * url /nature/id
				*/
				$environment->app->delete ( '/:id', function ($id) use($environment) {
					NatureManagement::deleteNatureById($id);
					$environment->response->send ( Status::S2_OK );
				});
												
			});
						
			/** HOLDINGS **/
			$environment->app->group ( '/holdings', function (){
				$environment = Environment::getInstance ();
					
				/**
				 * Get list of holding
				 * Method GET
				 * url /holdings
				*/
				$environment->app->get ('' , function () use($environment) {
					$rsp = HoldingsManagement::getHoldings();
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
						
			
				/**
				 * Get holding by id
				 * Method GET
				 * url /holdings/id
				*/
				$environment->app->get ('/:id' , function ($id) use($environment) {
					$rsp = HoldingsManagement::getHoldingById($id);
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
			
			
				/**
				 * Check primary key holding
				 * Method GET
				 * url /holdings/exists/id
				*/
	
				$environment->app->get ('/exist/:id' , function ($id) use($environment) {
					$rsp = HoldingsManagement::holdingNotExist($id);
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
			
			
				/**
				 * Add NEW holdings
				 * Method POST
				 * url /holding
				*/
				$environment->app->post ('' , function () use($environment) {
					$data = route::checkParams('id', "name");
					$rsp = HoldingsManagement::addHolding(null, $data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_Created);
				});

			
			
				/**
				 * Add UPDATE holdings
				 * Method PUT
				 * url /holdings/id
				*/
				$environment->app->put ('/:id' , function ($id) use($environment) {
					$data = route::checkParams();
					$rsp =  HoldingsManagement::addHolding($id,$data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_OK);
				});

				/**
				 * DELETE multiple holdings
				 * Method DELETE
				 * url /holdings
				 * post array
				*/

				$environment->app->delete('', function () use ($environment) {
					$data = route::checkParams();
					$rsp  = HoldingsManagement::deleteHoldings($data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_OK);

				});

					/**
					 * DELETE nature
					 * Method DELETE
					 * url /holdings/id
					*/
					$environment->app->delete ( '/:id', function ($id) use($environment) {
						HoldingsManagement::deleteHoldingById($id);
						$environment->response->send ( Status::S2_OK );
					});
			
			});
			
			/** DOCUMENT TYPE **/
			$environment->app->group ( '/documenttypes', function (){
				$environment = Environment::getInstance ();
					
				/**
				 * Get list of document Type
				 * Method GET
				 * url /documentstypes
				*/
				$environment->app->get ('' , function () use($environment) {
					$rsp = DocumentTypesManagement::getDocumentTypes();					
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
			
				/**
				 * Get documents type by id
				 * Method GET
				 * url /documentstypes/id
				*/
				$environment->app->get ('/:id' , function ($id) use($environment) {
					$rsp = DocumentTypesManagement::getDocumentTypeById($id);
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
													
				/**
				 * Check primary key documentstype
				 * Method GET
				 * url /documentstypes/exists/id
				*/
	
				$environment->app->get ('/exist/:id' , function ($id) use($environment) {
					$rsp = DocumentTypesManagement::documentTypeNotExist($id);
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
															
				/**
				 * Add NEW documentstype
				 * Method POST
				 * url /documentstypes
				*/
				$environment->app->post ('' , function () use($environment) {
					$data = route::checkParams('id', "name");
					$rsp = DocumentTypesManagement::addDocumentType(null, $data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_Created);
				});
																		
				/**
				 * Add UPDATE documentstype
				 * Method PUT
				 * url /documentstypes/id
				*/
				$environment->app->put ('/:id' , function ($id) use($environment) {
					$data = route::checkParams();
					$rsp =  DocumentTypesManagement::addDocumentType($id,$data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_OK);
				});
			
				/**
				 * DELETE multiple documentstypes
				 * Method DELETE
				 * url /documentstypes
				 * post array
				*/

				$environment->app->delete('', function () use ($environment) {
					$data = route::checkParams();
					$rsp  = DocumentTypesManagement::deleteDocumentTypes($data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_OK);
				});

				/**
				 * DELETE documents type
				 * Method DELETE
				 * url /documentstypes/id
				*/
				$environment->app->delete ( '/:id', function ($id) use($environment) {
					DocumentTypesManagement::deleteDocumentTypeById($id);
					$environment->response->send ( Status::S2_OK );
				});
											
			});
						
			/** COUNTRY **/
			$environment->app->group ( '/countries', function (){
				$environment = Environment::getInstance ();
					
				/**
				 * Get list of contries
				 * Method GET
				 * url /contries
				*/
				$environment->app->get ('' , function () use($environment) {													
					$rsp = CountryManagement::getCountries();
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
						
				/**
				 * Get contry by id
				 * Method GET
				 * url /countries/id
				*/
				$environment->app->get ('/:id' , function ($id) use($environment) {
					$rsp = CountryManagement::getCountryById($id);
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
						
				/**
				 * Check primary key country
				 * Method GET
				 * url /countries/exists/id
				*/
	
				$environment->app->get ('/exist/:id' , function ($id) use($environment) {
					$rsp = CountryManagement::countryNotExist($id);
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
							
				/**
				 * Add NEW country
				 * Method POST
				 * url /countries
				*/
				$environment->app->post ('' , function () use($environment) {
					$data = route::checkParams('id', "name");
					$rsp = CountryManagement::addCountry(null, $data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_Created);
				});

				/**
				 * Add UPDATE country
				 * Method PUT
				 * url /countries/id
				*/
				$environment->app->put ('/:id' , function ($id) use($environment) {
					$data = route::checkParams();
					$rsp =  CountryManagement::addCountry($id,$data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_OK);
				});
							
				/**
				 * DELETE multiple countries
				 * Method DELETE
				 * url /countries
				 * post array
				*/

				$environment->app->delete('', function () use ($environment) {
					$data = route::checkParams();
					$rsp  = CountryManagement::deleteCountries($data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_OK);
				});

				/**
				 * DELETE country
				 * Method DELETE
				 * url /countries/id
				*/
				$environment->app->delete ( '/:id', function ($id) use($environment) {
					CountryManagement::deleteCountryById($id);
					$environment->response->send ( Status::S2_OK );
				});
												
			});
							
			/** CONTACT SOURCE **/
			$environment->app->group ( '/contactsources', function (){
				$environment = Environment::getInstance ();
					
				/**
				 * Get list of contact sources
				 * Method GET
				 * url /contactsources
				*/
				$environment->app->get ('' , function () use($environment) {					
					$rsp = ContactSourcesManagement::getContactSources();										
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
				
				/**
				 * Get contact source by id
				 * Method GET
				 * url /contactsources/id
				*/
				$environment->app->get ('/:id' , function ($id) use($environment) {
					$rsp = ContactSourcesManagement::getContactSourceById($id);
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
		
				/**
				 * Check primary key contact source
				 * Method GET
				 * url /contactsources/exists/id
				*/
	
				$environment->app->get ('/exist/:id' , function ($id) use($environment) {
					$rsp = ContactSourcesManagement::contactSourceNotExist($id);
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
						
				/**
				 * Add NEW contact source
				 * Method POST
				 * url /contactsources
				*/
				$environment->app->post ('' , function () use($environment) {
					$data = route::checkParams("name");
					
					$rsp = ContactSourcesManagement::addContactSource(null, $data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_Created);
				});

				/**
				 * Add UPDATE contact source
				 * Method PUT
				 * url /contactsources/id
				*/
				$environment->app->put ('/:id' , function ($id) use($environment) {
					$data = route::checkParams();
					$rsp =  ContactSourcesManagement::addContactSource($id,$data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_OK);
				});
							
				/**
				 * DELETE multiple contact source
				 * Method DELETE
				 * url /contactsources
				 * post array
				*/

				$environment->app->delete('', function () use ($environment) {
					$data = route::checkParams();
					$rsp  = ContactSourcesManagement::deleteContactSources($data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_OK);
				});

				/**
				 * DELETE contact source
				 * Method DELETE
				 * url /contactsources/id
				*/
				$environment->app->delete ( '/:id', function ($id) use($environment) {
					ContactSourcesManagement::deleteContactSourceById($id);
					$environment->response->send ( Status::S2_OK );
				});
			
			});
			
			/** CONTACT TYPE **/
			$environment->app->group ( '/contacttypes', function (){
				$environment = Environment::getInstance ();
					
				/**
				 * Get list of contact types
				 * Method GET
				 * url /contacttypes
				*/
				$environment->app->get ('' , function () use($environment) {
					$rsp = ContactTypesManagement::getContactTypes();					
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
			
				/**
				 * Get contact type by id
				 * Method GET
				 * url /contacttypes/id
				*/
				$environment->app->get ('/:id' , function ($id) use($environment) {
					$rsp =ContactTypesManagement::getContactTypeById($id);
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
			
				/**
				 * Check primary key contact type
				 * Method GET
				 * url /contacttypes/exists/id
				*/
	
				$environment->app->get ('/exist/:id' , function ($id) use($environment) {
					$rsp = ContactTypesManagement::contactTypeNotExist($id);
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
			
				/**
				 * Add NEW contact type
				 * Method POST
				 * url /contacttypes
				*/
				$environment->app->post ('' , function () use($environment) {
					$data = route::checkParams("name");
					$rsp = ContactTypesManagement::addContactType(null, $data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_Created);
				});

				/**
				 * Add UPDATE contact type
				 * Method PUT
				 * url /contacttypes/id
				*/
				$environment->app->put ('/:id' , function ($id) use($environment) {
					$data = route::checkParams();
					$rsp =  ContactTypesManagement::addContactType($id,$data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_OK);
				});
										
				/**
				 * DELETE multiple contact type
				 * Method DELETE
				 * url /contacttypes
				 * post array
				*/

				$environment->app->delete('', function () use ($environment) {
					$data = route::checkParams();
					$rsp  = ContactTypesManagement::deleteContactTypes($data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_OK);
				});

				/**
				 * DELETE contact type
				 * Method DELETE
				 * url /contacttypes/id
				*/
				$environment->app->delete ( '/:id', function ($id) use($environment) {
					ContactTypesManagement::deleteContactTypeById($id);
					$environment->response->send ( Status::S2_OK );
				});
												
			});
				
			/** CONTACT INDUSTRY **/
			$environment->app->group ( '/contactindustries', function (){
				$environment = Environment::getInstance ();
						
				/**
				 * Get list of contact industries
				 * Method GET
				 * url /contactindustries
				*/
				$environment->app->get ('' , function () use($environment) {					
					$rsp = ContactIndustriesManagement::getContactIndustries();
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
							
				/**
				 * Get contact type by id
				 * Method GET
				 * url /contactindustries/id
				*/
				$environment->app->get ('/:id' , function ($id) use($environment) {									
					$rsp =ContactIndustriesManagement::getContactIndustryById($id);
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
								
				/**
				 * Check primary key contact type
				 * Method GET
				 * url /contactindustries/exists/id
				*/
	
				$environment->app->get ('/exist/:id' , function ($id) use($environment) {
					$rsp = ContactIndustriesManagement::contactIndustryNotExist($id);
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_OK);
				});
									
				/**
				 * Add NEW contact type
				 * Method POST
				 * url /contactindustries
				*/
				$environment->app->post ('' , function () use($environment) {
					$data = route::checkParams("name");
					$rsp = ContactIndustriesManagement::addContactIndustry(null, $data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_Created);
				});
				
				/**
				 * Add UPDATE contact type
				 * Method PUT
				 * url /contactindustries/id
				*/
				$environment->app->put ('/:id' , function ($id) use($environment) {
					$data = route::checkParams();
					$rsp =  ContactIndustriesManagement::addContactIndustry($id,$data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_OK);
				});
				
				/**
				 * DELETE multiple contact type
				 * Method DELETE
				 * url /contactindustries
				 * post array
				*/

				$environment->app->delete('', function () use ($environment) {
					$data = route::checkParams();
					$rsp  = ContactIndustriesManagement::deleteContactIndustries($data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_OK);
				});
				
				/**
				 * DELETE contact type
				 * Method DELETE
				 * url /contactindustries/id
				*/
				$environment->app->delete ( '/:id', function ($id) use($environment) {
					ContactIndustriesManagement::deleteContactIndustryById($id);
					$environment->response->send ( Status::S2_OK );
				});
				
			});
				
			/**
			 * GROUP TAGS
			 * GET/POST TAGS API ARE WRITTEN ON THE SPECIFIC RESOURCE ROUTE, BECAUSE TAGS ARE FILTERED BY RESOURCE TYPE BY THE REQUEST URL.
			 */
			$environment->app->group('/tags',function() use($environment){
				/**
				 * Delete  TAG and it's childrens.
				 * method DELETE
				 * url /mant/tags/:id
				 */
				$environment->app->delete('/:id',function($id) use ($environment){
					TagsManagement::deleteTag($id);
					$environment->response->send(Status::S2_NoContent); //DELETE request 204 no content.
				});
	
				/**
				 * Reorder tags..
				 * method POST
				 * url /mant/tags/reorder
				 */
				$environment->app->post('/reorder',function() use ($environment){
					$data = route::checkParams();
					TagsManagement::reorder($data);
					$environment->response->send(Status::S2_NoContent); //POST request 204 no content.
				});
			});
			
			/**
			 * GROUP ATTRIBUTES.
			 */
			$environment->app->group ( '/attributes', function () use($environment) {
				/**
				 * List attributes.
				 * url /mant/attributes
				 * method GET
				 */
				$environment->app->get ( '/', function () use($environment) {
					// TODO: Do action permision check
					$environment->response->addData (AttributesManagement::getAttributes());
					$environment->response->send ( Status::S2_OK);
				});
				
				/**
				 * Get  attribute options
				 * url /mant/attributes
				 * method GET
				 */
				$environment->app->get ( '/options', function () use($environment) {
					// TODO: Do action permision check
					$environment->response->addData (AttributesManagement::getAttributeOptions());
					$environment->response->send(Status::S2_OK);
				});
			
				/**
				 * Get  attribute by id.
				 * url /mant/attributes
				 * method GET
				*/
				$environment->app->get ( '/:id', function ($id) use($environment) {
					// TODO: Do action permision check
					$environment->response->addData (AttributesManagement::getAttribute($id));
					$environment->response->send(Status::S2_OK);
				})->conditions ( array (
					'id' => '[0-9]+' 
				));
				
		
				/**
				 * New attribute
				 * url /mant/attributes
				 * method POST
				*/
				$environment->app->post ( '/', function () use($environment) {
					$data = route::checkParams ( 'name', 'type'); // Check params and also returns _get && _post object params
					$rsp = AttributesManagement::attribute(null,$data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_Created);
				});
		
				/**
				 * Update  attribute by id.
				 * url /mant/attributes
				 * method PUT
				*/
				$environment->app->put ( '/:id', function ($id) use($environment) {
					// TODO: Do action permision check
					$data = route::checkParams(); // Check params and also returns _get && _post object params
					AttributesManagement::attribute($id,$data);
					$environment->response->send (Status::S2_NoContent ); //For a PUT request: HTTP 200 or HTTP 204 should imply "resource updated successfully".
				})->conditions ( array (
					'id' => '[0-9]+' 
				));
		
				/**
				 * Delete attribute by id.
				 * url /mant/attributes
				 * method DELETE
				 */
				$environment->app->delete ( '/:id', function ($id) use($environment) {
					// TODO: Do action permision check
					AttributesManagement::deleteAttribute($id);
					$environment->response->send(Status::S2_NoContent); //DELETE request 204 no content.
				});
			});
		
			
			/**
			 * GROUP TABLES.
			 */
			$environment->app->group('/tables',function() use($environment){
				/**
				 * Get list of tables.
				 * method GET
				 */
				$environment->app->get('',function() use ($environment){
					$environment->response->addData(MantManagement::tables());
					$environment->response->send(Status::S2_OK);
				});
			
				/**
				 * Get a table.
				 * method GET
				 */
				$environment->app->get('/:id',function($id) use ($environment){
					$environment->response->addData(MantManagement::tables($id));
					$environment->response->send(Status::S2_OK);
				});
				
				/**
				 * Check if serie exist.
				 * method GET
				 */
				$environment->app->get('/:id/serie/exist/:csn',function($id,$csn) use ($environment){
					$environment->response->addData(MantManagement::checkSerieName($id,$csn));
					$environment->response->send(Status::S2_OK);
				});
			
			
				/**
				 * Update table.
				 * @param $id idtable.
				 * method PUT
					*/
				$environment->app->put('/:id',function($id) use ($environment){
					$data = route::checkParams();
					MantManagement::updateTable($id,$data);
					$environment->response->send();
				});
			});
			
			/**
			 * GROUP CHAPTERS.
			 */
			$environment->app->group('/skillTool',function() use($environment){
				require './api/crud/class.ChapterManagement.php';
				/**
				 * Get list of chapters-skill-tool.
				 * method GET
				 */
				$environment->app->get('',function() use ($environment){
					$environment->response->addData(ChapterManagement::listChapter());
					$environment->response->send();
				});
				
				/**
				 * Get a chapter-skill-tool.
				 * method GET
				 */
				$environment->app->get('/options',function() use ($environment){
					$environment->response->addData(ChapterManagement::getChapterOptions());
					$environment->response->send();
				});
						
				/**
				 * Get a chapter-skill-tool.
				 * method GET
				 */
				$environment->app->get('/:id',function($id) use ($environment){
					$environment->response->addData(ChapterManagement::getChapter($id));
					$environment->response->send();
				});
					
				/**
				 * Add chapter-skill-tool.
				 * @param $id idchapter.
				 * method PUT
				 */
				$environment->app->post('',function() use ($environment){
					$data = route::checkParams('name');
					$rsp = ChapterManagement::addChapter($data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_Created);
				});
						
				/**
				 * Update chapter-skill-tool.
				 * @param $id idtable.
				 * method PUT
				 */
				$environment->app->put('/:id',function($id) use ($environment){
					$data = route::checkParams();
					ChapterManagement::updateChapter($id,$data);
					$environment->response->send();
				});
				
				$environment->app->delete('/:id',function($id) use ($environment){
					ChapterManagement::deleteChapter($id);
					$environment->response->send();
				});
				
				$environment->app->group('/:cid/elements',function() use($environment){
					/**
					 * Get list of chapters elements.
					 * method GET
					 */
					$environment->app->get('',function($cid) use ($environment){
						$environment->response->addData(ChapterManagement::listChapterElements($cid));
						$environment->response->send();
					});
					
					/**
					 * Add chapter element.
					 * method POST
					 */
					$environment->app->post('',function($cid) use ($environment){
						$data = route::checkParams('name');
						ChapterManagement::addChapterElement($cid,$data);
						$environment->response->send(Status::S2_Created);
					});
					
					/**
					 * Update chapter element.
					 * method PUT
					 */
					$environment->app->put('/:ceid',function($cid,$ceid) use ($environment){
						$data = route::checkParams();
						ChapterManagement::updateChapterElement($cid,$ceid,$data);
						$environment->response->send(Status::S2_Created);
					});
					
					/**
					 * Delete multiple chapter element.
					 * method DELETE
					 */
					$environment->app->delete('',function($cid) use ($environment){
						$data = route::checkParams('ceid');
						ChapterManagement::deleteChapterElement($cid,$data);
						$environment->response->send(Status::S2_Created);
					});
				});
			});
		
			/**
			 * GROUP QUALIFICATIONS.
			 */
			$environment->app->group('/qualifications',function() use($environment){
				/**
				 * Get list of qualifications.
				 * method GET
				 */
				$environment->app->get('',function() use ($environment){
					$environment->response->addData(QualificationsManagement::_get());
					$environment->response->send();
				});
				
				/**
				 * Qualification options.
				 * method GET
				 */
				$environment->app->get('/options',function() use ($environment){
					$environment->response->addData(QualificationsManagement::options());
					$environment->response->send(Status::S2_OK);
				});
				
				/**
				 * Get a qualification.
				 * method GET
				 */
				$environment->app->get('/:qid',function($qid) use ($environment){
					$environment->response->addData(QualificationsManagement::_getById($qid));
					$environment->response->send(Status::S2_OK);
				});
					
				/**
				 * Create qualification.
				 * method POST
				 */
				$environment->app->post('',function() use ($environment){
					$data = route::checkParams('description');
					$rsp = QualificationsManagement::_add($data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_Created);
				});
							
				/**
				 * Update qualification.
				 * @param $id qualification_id.
				 * method PUT
				 */
				$environment->app->put('/:qid',function($qid) use ($environment){
					$data = route::checkParams();
					QualificationsManagement::_update($qid, $data);
					$environment->response->send();
				});
				
				/**
				 * Delete qualification.
				 * @param $id qualification_id.
				 * method DELETE
				 */
				$environment->app->delete('/:qid',function($qid) use ($environment){
					QualificationsManagement::_delete($qid);
					$environment->response->send();
				});
			});
			
			/**
			 * Get list of cities.
			 * Can accept ?q=
			 * method GET
			 */
			$environment->app->get('/cities',function() use ($environment){
				$data = route::checkParams();
				$environment->response->addData(MantManagement::cities(isset($data->q)? $data->q:null));
				$environment->response->send(Status::S2_OK);
			});

			/**
			 * Get list of regions.
			 * Can accept ?q=
			 * method GET
			 */
			$environment->app->get('/regions',function() use ($environment){
				$data = route::checkParams();
				$environment->response->addData(MantManagement::regions(isset($data->q)? $data->q:null));
				$environment->response->send(Status::S2_OK);
			});
			
			/**
			 * Get list of provinces.
			 * Can accept ?q=
			 * method GET
			 */
			$environment->app->get('/provinces',function() use ($environment){
				$data = route::checkParams();
				$environment->response->addData(MantManagement::provinces(isset($data->q)? $data->q:null));
				$environment->response->send(Status::S2_OK);
			});
			
			/**
			 * Get list of countries.
			 * Can accept ?q=
			 * method GET
			 */
			/*$environment->app->get('/countries',function() use ($environment){
				$data = route::checkParams();
				$environment->response->addData(MantManagement::countries(isset($data->q)? $data->q:null));
				$environment->response->send(Status::S2_OK);
			});*/
		});
		
		// Run parent default uri control
		parent::run ();
	}
}
?>