<?php
namespace api\route;

use core\Environment;
use core\route;
use common\Table;
use api\crud\FileManagement;
use api\crud\memoqManagement;
use api\crud\MbrandManagement;


require './api/crud/class.FileManagement.php';
require './api/crud/class.MbrandManagement.php';
require './api/crud/class.AttributesManagement.php';
require './api/crud/class.LanguagesManagement.php';

class mbrand extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, $needsAuth );
	}
	public function run() {
		$authFunction = $this->authFunction;
		$environment = Environment::getInstance ();
		
		self::$environment->app->group ( '/'.$this->urlspace,$authFunction, function () use($environment) {
			
			/**
			 * Get Documents from a TP
			 * Method GET
			 * url /crm/thirdparty/:id/documents
			 */
			$environment->app->get('/dgest_thirdparty',function() use ($environment){
				$rsp = MbrandManagement::_getdgest_thirdparty();
				$environment->response->addData($rsp);
				$environment->response->send();
			});
			
			// GET route
			$environment->app->get ('/', function () use ($environment) {
				$environment->response->addData(MbrandManagement::_get());
				$environment->response->send();
			});
			
			
			/**
			 * Get all available options to assign a TP. (available countries, available tags,...)
			 * Method GET
			 * url /thirdparty/options
			 */
			$environment->app->get('/:id',function($id) use ($environment){
				$environment->response->addData(MbrandManagement::_getById($id)->get());
				$environment->response->send();
			});
			
			/**
			 * Create mbrand
			 * Method POST
			 * url /thirdparty/options
			 */
			$environment->app->post('',function() use ($environment){
				$environment->response->addData(MbrandManagement::_set(null,route::checkParams()));
				$environment->response->send();
			});
			
			/**
			 * delete mbrand
			 * Method DELETE
			 * url /thirdparty/options
			 */
			$environment->app->post('',function() use ($environment){
				$environment->response->addData(MbrandManagement::_set(null,route::checkParams()));
				$environment->response->send();
			});
			
			/**
			 * Create asp translation task
			 * Method POST
			 * url /mbrand/translate
			 */
			$environment->app->post('/translate/:id',function($id) use ($environment){
				$environment->response->addData(MbrandManagement::_addtranslationtask($id,route::checkParams()->combo));
				$environment->response->send();
			});
			
			
			
			/**
			 * Add documents on a new (uncreated) TP.
			 * This method is created to upload files while the user is filling the TP form.
			 *
			 * Method POST
			 * url /crm/thirdparty/uploadDocuments
			 */
			$environment->app->post('/uploadDocuments',function() use ($environment){
				$rsp = MbrandManagement::_addDocument(null,route::checkParams());
				$environment->response->addData($rsp);
				$environment->response->send();
			});
			
			
			/**
			 * Update TP
			 * Method Put
			 * url /thirdparty/:id
			 */
			$environment->app->put('/:id',function($id) use ($environment){
				$environment->response->addData(MbrandManagement::_set($id, route::checkParams()));
				$environment->response->send();
			});
			
			/**
			 * Add files to a TP.
			 * Method POST
			 * url /crm/thirdparty/:id/uploadDocuments
			 */
			$environment->app->post('/:id/uploadDocuments',function($id) use ($environment){
				$rsp = MbrandManagement::_addDocument($id,route::checkParams());
				$environment->response->addData($rsp);
				$environment->response->send();
			});
			
			/**
			 * Delete TP
			 * Method Delete
			 * url /crm/thirdparty
			 */
			$environment->app->delete('/:id',function($id) use ($environment){
				MbrandManagement::_delete($id);
				$environment->response->send();
			});
			
			/**
			 * Get Documents from a TP
			 * Method GET
			 * url /crm/thirdparty/:id/documents
			 */
			$environment->app->get('/:id/documents',function($id) use ($environment){
				$rsp = MbrandManagement::_getDocuments($id);
				$environment->response->addData($rsp);
				$environment->response->send();
			});
		});
		
		
		
		
		
		parent::run(); // Run parent default uri control
	}
}