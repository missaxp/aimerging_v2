<?php

namespace api\route;


use core\Environment;
use core\http\Status;
use api\Security\ActionsCode;
use core\route;
use api\crud\GroupManagement;


class groups extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, $needsAuth );
	}
	public function run() {
		$authFunction = $this->authFunction;
		
		self::$environment->app->group ( '/' . $this->urlspace, function () use($authFunction) {
			$environment = Environment::getInstance ();
			/**
			 * List groups
			 * url /groups
			 * method GET
			 */
			$environment->app->get ('', $authFunction, function () use($environment) {
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData (GroupManagement::_get());
				$environment->response->send();
			} );
			
			/**
			 * get a group
			 * url /groups/:id
			 * method GET
			 */
			$environment->app->get ( '/:id', $authFunction, function ($id) use($environment) {
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData(GroupManagement::_getById($id));
				$environment->response->send(Status::S2_OK);
			})->conditions (array('id' => '[0-9]+'));
			
			/**
			 * Adds a New group
			 * url /groups
			 * method POST
			 */
			$environment->app->post ( '', $authFunction, function () use($environment) {
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_ADD );
				$data = route::checkParams ( 'name' ); // Check params and also returns _get && _post object params
				//DepartmentManagement::addDepartment ( $department );
				$group = GroupManagement::_add($data);
				$environment->response->addData($group);
				$environment->response->send(Status::S2_Created);
			});
			
			/**
			 * Update a group
			 * url /groups/:id
			 * method PUT
			 */
			$environment->app->put ('/:id', $authFunction, function ($id) use($environment) {
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_MODIFY );
				GroupManagement::_update($id,route::checkParams());
				$environment->response->send (Status::S2_OK);
			});
			
			/**
			 * Delete a group
			 * url /groups/:id
			 * method DELETE
			 */
			$environment->app->delete ( '/:id', $authFunction, function ($id) use($environment) {
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_DELETE );
				GroupManagement::_delete($id);
				$environment->response->send( Status::S2_OK );
			})->conditions (array('id' => '[0-9]+'));
			
			/**
			 * Delete multiple groups
			 * url /groups
			 * method DELETE
			 */
			$environment->app->delete ('', $authFunction, function () use($environment) {
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_DELETE );
				$data = route::checkParams ('gids'); // Check params and also returns _get && _post object params
				GroupManagement::_delete($data->gids);
				$environment->response->send(Status::S2_OK);
			});
		} );
		// Run parent default uri control
		parent::run();
	}
}
?>