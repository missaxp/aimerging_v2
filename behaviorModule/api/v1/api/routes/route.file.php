<?php

namespace api\route;

use core\Environment;
use core\route;
use api\crud\FileManagement;
use api\crud\User;

require './api/crud/class.FileManagement.php';
class file extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, $needsAuth );
	}
	public function run() {
		$authFunction = $this->authFunction;
		
		self::$environment->app->group ( '/'.$this->urlspace, function () use($authFunction) {
			$environment = Environment::getInstance ();
			
			/**
			 * Update file resource comments
			 * url /file/id
			 * method PUT
			 */
			$environment->app->put ('/:fid', $authFunction, function ($fid) use($environment) {
				// TODO: Do action permision check
				$data = route::checkParams('comment');
				FileManagement::updateFileComments($fid, $data->comment);
				$environment->response->send();
			});
			
			/**
			 * Delete file resource
			 * url /file/id
			 * method DELETE
			 */
			$environment->app->delete('/:fid',$authFunction,function($fid) use ($environment){
				FileManagement::deleteFile($fid);
				$environment->response->send();
			});
			
			/**
			 * This method not use default auth method, instead of that, it needs the apptoken on the URI.
			 * 
			 * Get file binary
			 * url /file/id
			 * method GET
			 * 
			 */
			$environment->app->get('/:auth/:fid',function($auth,$fid) use ($environment){
				if($environment->authentication_required){
					$environment->authentication->Validate($auth);
					$environment->user = new \core\Security\User($auth); // If we successfull login to the webService, assign the user to the webservice request.
				}
				FileManagement::getBinary($fid);
				//getBinary executes die(); after file flush.
			});
			
			/**
			 * This method not use default auth method, instead of that, it needs the apptoken on the URI.
			 *
			 * Get temporary created file. The temporary files are files dynamically generated for the only purpose to download it.
			 * Once the file is download, it will be deleted.
			 * url /tmp/:auth/:tid
			 * method GET
			 */
			$environment->app->get('/temporary/:auth/:fid',function($auth,$tid) use ($environment){
				if($environment->authentication_required){
					$environment->authentication->Validate($auth);
				}
				FileManagement::getTemporaryBinary($tid);
				//getBinary executes die(); after file flush.
			});
		});
		
		// Run parent default uri control
		parent::run ();
	}
}
?>