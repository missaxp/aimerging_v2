<?php

namespace api\route;


use core\Environment;
use api\Security\ActionsCode;
use core\route;
use api\crud\TeamsManagement;

require './api/crud/class.TeamsManagement.php';

class teams extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, $needsAuth );
	}
	public function run() {
		$authFunction = $this->authFunction;
		
		self::$environment->app->group ( '/' . $this->urlspace,$authFunction, function (){
			$environment = Environment::getInstance ();
			
			/**
			 * List teams
			 * url /teams
			 * method GET
			 */
			$environment->app->get ('', function () use($environment) {
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData(TeamsManagement::_get());
				$environment->response->send();
			});
			
			/**
			 * Get teams
			 * url /teams/:tid
			 * method GET
			 */
			$environment->app->get ('/:tid', function ($tid) use($environment) {
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData(TeamsManagement::_getById($tid)->get());
				$environment->response->send();
			});
			
			/**
			 * Update teams
			 * url /teams/:tid
			 * method PUT
			 */
			$environment->app->put ('/:tid', function ($tid) use($environment) {
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData(TeamsManagement::_update($tid,route::checkParams()));
				$environment->response->send();
			});
			
			/**
			 * Create teams
			 * url /teams
			 * method POST
			 */
			$environment->app->post ('', function () use($environment) {
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData(TeamsManagement::_new(route::checkParams('name')));
				$environment->response->send();
			});
			
			/**
			 * Delete team
			 * url /teams/:tid
			 * method DELETE
			 */
			$environment->app->delete('/:tid', function ($tid) use($environment) {
				//$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData(TeamsManagement::_delete($tid));
				$environment->response->send();
			});
		});
		// Run parent default uri control
		parent::run();
	}
}
?>