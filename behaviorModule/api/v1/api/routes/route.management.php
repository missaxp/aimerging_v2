<?php

namespace api\route;

use core\http\Status;
use core\Environment;
use core\serverInfo;
use api\crud\ErrorManagement;
use core\route;

require './api/crud/class.AttributesManagement.php';
require_once './api/crud/class.ErrorManagement.php';

class management extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, $needsAuth );
	}
	public function run() {
		$authFunction = $this->authFunction;
		
		self::$environment->app->group ( '/' . $this->urlspace,$authFunction, function () use($authFunction) {
			$environment = Environment::getInstance ();
		
			/**
			 * Get Server Status
			 * url /management/getServerStatus
			 * method POST
			 */
			$environment->app->post ( '/getServerStatus', $authFunction, function () use($environment) {
				// TODO: Do action permision check
				require_once './libs/class.ServerInfo.php';
				$status = new serverInfo ();
				$environment->response->addData ( $status->getInfo () );
				$environment->response->send ();
			} );
			
			/**
			 * Get last requested URLS
			 * url /management/getRequestedURLS
			 * method POST
			 */
			$environment->app->post ( '/getRequestedURLS', $authFunction, function () use($environment) {
				// TODO: Do action permision check
				require_once './libs/class.ServerInfo.php';
				$status = new serverInfo ();
				$environment->response->addData ( $status->getRequestedURLS () );
				$environment->response->send ();
			} );
			
			/**
			 * management/errorHandler GROUP
			 */
			$environment->app->group ( '/errorHandler', function () use($authFunction, $environment) {
				/**
				 * Register a new Error
				 * url /management/errorHandler/register
				 * method POST
				 */
				$environment->app->post ( "/register", $authFunction, function () use($environment) {
					// TODO: Do action permission check
					ErrorManagement::register(route::checkParams('code','message','customMessage','line','column','navigator','type'));
					$environment->response->send(Status::S2_Created);
				} );
				
				$environment->app->get("",$authFunction,function() use ($environment){
					$environment->response->addData(ErrorManagement::getErrors());
					$environment->response->send();
				});
			} );
			
			$environment->app->post ( '/getEnvironmentConfig', $authFunction, function () use($environment) {
				// TODO: Do action permision check
				require_once './libs/class.ServerInfo.php';
				$status = new serverInfo ();
				$environment->response->addData($status->getEnvironmentConfig());
				$environment->response->send (Status::S2_OK);
			} );
		} );
		
		// Run parent default uri control
		parent::run ();
	}
}
?>