<?php

namespace api\route;


use core\Environment;
use core\http\Status;
use api\crud\Department;
use api\crud\DepartmentManagement;
use api\Security\ActionsCode;
use core\route;

class departments extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, $needsAuth );
	}
	public function run() {
		$authFunction = $this->authFunction;
		
		self::$environment->app->group ( '/' . $this->urlspace, function () use($authFunction) {
			$environment = Environment::getInstance ();
			
			/**
			 * Adds a New Department
			 * url /departments
			 * method POST
			 */
			$environment->app->post ( '', $authFunction, function () use($environment) {
				$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_ADD );
				$data = route::checkParams ( 'name' ); // Check params and also returns _get && _post object params
				$department = new Department ();
				$department->name = $data->name;
				
				DepartmentManagement::addDepartment ( $department );
				
				// Return data
				$environment->response->addData ( $department );
				$environment->response->send ( Status::S2_Created );
			} );
			
			/**
			 * List departments
			 * url /departments
			 * method GET
			 */
			$environment->app->get ( '', $authFunction, function () use($environment) {
				$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData (DepartmentManagement::getDepartments());
				$environment->response->send ();
			} );
			
			/**
			 * List a department
			 * url /departments/:id
			 * method GET
			 */
			$environment->app->get ( '/:id', $authFunction, function ($id) use($environment) {
				$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$department = DepartmentManagement::getDepartmentbyId ( $id );
				$environment->response->addData ( $department );
				$environment->response->send ( Status::S2_OK );
			} )->conditions ( array (
					'id' => '[0-9]+' 
			) );
			
			/**
			 * List a users from specified (:id) department
			 * url /departments/:id
			 * method POST
			 */
			$environment->app->post( '/:id/getUsers', $authFunction, function ($id) use($environment) {
				$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_VIEW );
				$environment->response->addData (DepartmentManagement::getUsersPerDepartment($id));
				$environment->response->send ();
			} )->conditions ( array (
					'id' => '[0-9]+' 
			) );
			
			/**
			 * Delete a department
			 * url /departments/:id
			 * method DELETE
			 */
			$environment->app->delete ( '/:id', $authFunction, function ($id) use($environment) {
				$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_DELETE );
				DepartmentManagement::deleteDepartment ( $id );
				$environment->response->send ( Status::S2_OK );
			} )->conditions ( array (
					'id' => '[0-9]+' 
			) );
			
			/**
			 * Update a department
			 * url /departments/:id
			 * method PUT
			 */
			$environment->app->put ( '/:id', $authFunction, function ($id) use($environment) {
				$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_MODIFY );
				$data = $environment->request->put (); // TODO veryfirequiredparams funció de CTEMPORAL s'ha de revisar pq no funciona en ->put();
				DepartmentManagement::updateDepartment ( $id, $data );
				$environment->response->send ( Status::S2_OK );
			} );
			
			/**
			 * Adds a user to department
			 * url /departments/:id/addUser
			 * method POST
			 */
			$environment->app->post ( '/:id/addUser', $authFunction, function ($id) use($environment) {
				$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_ADD_USER );
				$data = $environment->request->post (); // TODO veryfirequiredparams funció de CTEMPORAL s'ha de revisar pq no funciona en ->put();
				DepartmentManagement::addUserToDepartment ( $id, $data );
				$environment->response->send ( Status::S2_OK );
			} );
			
			/**
			 * Removes a user to department
			 * url /departments/:id/deleteUser
			 * method POST
			 */
			$environment->app->post ( '/:id/deleteUser', $authFunction, function ($id) use($environment) {
				$environment->security->hasAccesToAction ( ActionsCode::DEPARTMENT_ADD_USER );
				$data = $environment->request->post (); // TODO: Afegir funcion verifyParams
				DepartmentManagement::removeUserToDepartment ( $id, $data );
				$environment->response->send ( Status::S2_OK );
			} );
		} );
		// Run parent default uri control
		parent::run ();
	}
}
?>