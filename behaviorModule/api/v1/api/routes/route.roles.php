<?php

namespace api\route;

require './api/crud/class.ModuleManagement.php';

use api\crud\RoleManagement;
use core\Environment;
use core\http\Status;
use api\Security\ActionsCode;
use api\crud\ModuleManagement;
use core\route;
use api\crud\Role;

class roles extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, $needsAuth );
	}
	
	public function run() {
		$authFunction = $this->authFunction;
		
		self::$environment->app->group ( '/' . $this->urlspace, $authFunction,function () use($authFunction) {
			$environment = Environment::getInstance ();
			
			/**
			 * Get all Roles
			 * url /roles
			 * Method GET
			 */
			$environment->app->get ( '', function () use($environment) {
				$environment->security->hasAccesToAction ( ActionsCode::ROLE_VIEW );
				$environment->response->addData (RoleManagement::getRoles());
				$environment->response->send ();
			});
			
			/**
			 * Get all available options to assign a Role
			 * Method GET
			 * url /roles/options
			 */
			$environment->app->get('/options',function() use ($environment){
				$environment->response->addData(RoleManagement::getRolebyId());
				$environment->response->send();
			});
			
			/**
			 * Adds a New Role
			 * url /roles
			 * method POST
			 */
			$environment->app->post ( '',function () use($environment) {
				$environment->security->hasAccesToAction ( ActionsCode::ROLE_ADD );
				$data = route::checkParams ('name'); // Check params and also returns _get && _post object params
				$rid = RoleManagement::addRole($data);
				$environment->response->addData($rid);
				$environment->response->send (Status::S2_Created);
			} );
			
			/**
			 * Updates Role
			 * url /roles
			 * method PUT
			 */
			$environment->app->put ( '/:id', function ($id) use($environment) {
				$environment->security->hasAccesToAction ( ActionsCode::ROLE_MODIFY );
				$data = route::checkParams();
				RoleManagement::updateRole($id,$data);
				$environment->response->send (); // Return data
			} )->conditions(array ('id' => '[0-9]{0,}'));
			
			/**
			 * Get a Role by id
			 * url /roles/:id
			 * Method GET
			 */
			$environment->app->get ( '/:id', function ($id) use($environment) {
				$environment->security->hasAccesToAction(ActionsCode::ROLE_VIEW );
				$environment->response->addData (RoleManagement::getRolebyId ($id)); // Return data
				$environment->response->send();
			} )->conditions(array ('id' => '[0-9]{0,}'));
			
			/**
			 * Delete a Role by id
			 * url /roles/:id
			 * Method DELETE
			 */
			$environment->app->delete ( '/:id', function ($id) use($environment) {
				$environment->security->hasAccesToAction ( ActionsCode::ROLE_DELETE );
				$roles = RoleManagement::deleteRole ( $id );
				$environment->response->send();
			} )->conditions ( array ('id' => '[0-9]{0,}'));
			
			/**
			 * Get a list of modules and actions
			 * url /roles/modules
			 * Method GET
			 */
			$environment->app->get ( '/modules', function () use($environment) {
				$environment->security->hasAccesToAction ( ActionsCode::ROLE_ADD );
				$environment->response->addData (ModuleManagement::getModules());
				$environment->response->send();
			} );
		} );
		// Run parent default uri control
		parent::run ();
	}
}
?>