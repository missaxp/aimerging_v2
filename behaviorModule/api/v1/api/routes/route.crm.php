<?php
namespace api\route;

use core\route;
use api\crud\ThirdPartyManagement;
use core\http\Status;
use api\crud\ContactsManagement;
use common\Table;
use api\crud\TagsManagement;


require './api/crud/class.TagsManagement.php';
require './api/crud/class.LanguagesManagement.php';
require './api/crud/class.AttributesManagement.php';
require './api/crud/class.FileManagement.php';

class crm extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, $needsAuth );
	}
	
	public function run() {
		$authFunction = $this->authFunction;
		$environment = self::$environment;
		/**
		 * RESTRICTED AREA!!
		 * CRM | CONTENT RESOURCE MANAGEMENT.
		 * 
		 * @Todo : CHECK ACTIONS.
		 */
		self::$environment->app->group ( '/'.$this->urlspace,$authFunction, function () use($environment) {
			/**
			 * @START
			 * CRM/THIRDPARTY ROUTES
			 */
			$environment->app->group('/thirdparty', function () use($environment) {
				require './api/crud/class.CountryManagement.php';
				require './api/crud/class.CurrencyManagement.php';
				require './api/crud/class.ThirdPartyManagement.php';
				require './api/crud/class.BanksManagement.php';
				
				
				/**
				 * List all thirdparty members.
				 * Method GET
				 * url /crm/thirdparty
				 */
				$environment->app->get('', function () use($environment) {
					$environment->response->addData (ThirdPartyManagement::listThirdParty());
					$environment->response->send();
				});
				
				/**
				 * Get all available options to assign a TP. (available countries, available tags,...)
				 * Method GET
				 * url /thirdparty/options
				 */
				$environment->app->get('/options',function() use ($environment){
					$environment->response->addData(ThirdPartyManagement::getThirdParty()->get());
					$environment->response->send();
				});
				
				/** GROUP TAGS */
				$environment->app->group('/tags',function() use($environment){
					/**
					 * Get available tags from users elements
					 */
					$environment->app->get('',function() use($environment){
						$environment->response->addData (TagsManagement::getTagsList(Table::THIRD_PARTY));
						$environment->response->send();
					});
					
					$environment->app->post('',function() use($environment){
						$data = route::checkParams ('id_parent','name'); // Check params and also returns _get && _post object params
						$environment->response->addData(TagsManagement::addTag($data,Table::THIRD_PARTY));
						$environment->response->send(Status::S2_Created);
					});
				});
					
				/**
				 * get thirdparty by id.
				 * Method GET
				 * url /thirdparty/:tp_id
				 */
				$environment->app->get('/:id',function($id) use ($environment){
					$environment->response->addData (ThirdPartyManagement::getThirdParty($id)->get());
					$environment->response->send();
				});
				
				/**
				 * Add new TP
				 * Method POST
				 * url /crm/thirdparty
				 */
				$environment->app->post('',function() use ($environment){
					$data = route::checkParams('name');
					$rsp = ThirdPartyManagement::setTP(null,$data);
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_Created);
				});
				
				
				
				/**
				 * Update TP
				 * Method Put
				 * url /thirdparty/:id
				 */
				$environment->app->put('/:id',function($id) use ($environment){
					$data = route::checkParams();
					ThirdPartyManagement::setTP($id, $data);
					$environment->response->send();
				});
					
				/**
				 * Delete TP
				 * Method Delete
				 * url /crm/thirdparty
				 */
				$environment->app->delete('/:id',function($id) use ($environment){
					ThirdPartyManagement::delTP($id);
					$environment->response->send();
				});
				
				/**
				 * Upload logo
				 * url - /crm/thirdparty/:id/logo
				 * method - POST
				 */
				/*$environment->app->post ('/:id/logo', function ($id) use($environment) {
					$data = route::checkParams();
					$rsp = ThirdPartyManagement::uploadLogo($id,$data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_Created);
				});*/
				
				/**
				 * Get Documents from a TP
				 * Method GET
				 * url /crm/thirdparty/:id/documents
				 */
				$environment->app->get('/:id/documents',function($id) use ($environment){
					$rsp = ThirdPartyManagement::getDocuments($id);
					$environment->response->addData($rsp);
					$environment->response->send();
				});
			
				/**
				 * Add documents on a new (uncreated) TP.
				 * This method is created to upload files while the user is filling the TP form.
				 *
				 * Method POST
				 * url /crm/thirdparty/uploadDocuments
				 */
				$environment->app->post('/uploadDocuments',function() use ($environment){
					$data = route::checkParams();
					$rsp = ThirdPartyManagement::uploadDocuments(null,$data);
					$environment->response->addData($rsp);
					$environment->response->send();
				});
				
				/**
				 * Add files to a TP.
				 * Method POST
				 * url /crm/thirdparty/:id/uploadDocuments
				 */
				$environment->app->post('/:id/uploadDocuments',function($id) use ($environment){
					$data = route::checkParams();
					$rsp = ThirdPartyManagement::uploadDocuments($id,$data);
					$environment->response->addData($rsp);
					$environment->response->send();
				});
				

					
				/**
				 * Get banks from a third party
				 * Method GET
				 * url /crm/thirdparty/:tp_id/banks
				 */
				$environment->app->get ('/:id/banks/' , function ($id) use($environment) {
					$rsp = ThirdPartyManagement::getBanks($id);
					$environment->response->addData($rsp);
					$environment->response->send();
				});
				
				/**
				 * Add bank from a third party
				 * Method POST
				 * url /crm/thirdparty/:tp_id/banks
				 */
				$environment->app->post ('/:id/banks' , function ($id) use($environment) {
					$data = route::checkParams();
					$rsp = ThirdPartyManagement::addBank($id,$data);
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_Created);
				});
				
				/**
				 * Update bank from a third party
				 * Method PUT
				 * url /crm/thirdparty/:tp_id/banks/:bid
				 */
				$environment->app->put ('/:id/banks/:bid' , function ($id,$bid) use($environment) {
					$data = route::checkParams();
					$rsp = ThirdPartyManagement::updateBank($id,$bid,$data);
					$environment->response->send();
				});
				
				/**
				 * Delete bank from a third party
				 * Method DELETE
				 * url /crm/thirdparty/:tp_id/banks/:bid
				 */
				$environment->app->delete ('/:id/banks/:bid' , function ($id,$bid) use($environment) {
					$rsp = ThirdPartyManagement::deleteBank($id,$bid);
					$environment->response->send();
				});
				
				/**
				 * Delete list of banks from a third party
				 * Method DELETE
				 * url /crm/thirdparty/:tp_id/banks/:bid
				 */
				$environment->app->delete ('/:id/banks' , function ($id) use($environment) {
					$bids = route::checkParams(); 
					$rsp = ThirdPartyManagement::deleteBank($id,$bids);
					$environment->response->send();
				});
				
				/**
				 * Get contacts from a third party
				 * Method GET
				 * url /crm/thirdparty/:tp_id/contacts
				 */
				$environment->app->get ('/:id/contacts' , function ($id) use($environment) {
					$rsp = ThirdPartyManagement::getContacts($id);
					$environment->response->addData ($rsp);
					$environment->response->send();
				});
			});
			/**
			 * @END
			 * CRM/THIRDPARTY ROUTES
			 */

			/**
			 * @START
			 * CRM/CONTACTS ROUTES
			 */
			$environment->app->group ( '/contact', function () use($environment) {
				require './api/crud/class.ContactsManagement.php';
				require './api/crud/class.ContactSourcesManagement.php';
				require './api/crud/class.ContactTypesManagement.php';
				require './api/crud/class.ContactIndustriesManagement.php';
				/**
				 * Get contacts list
				 * Method GET
				 * url /crm/contact
				 */
				$environment->app->get ('' , function () use($environment) {
					$rsp = ContactsManagement::listContacts();
					$environment->response->addData($rsp);
					$environment->response->send();
				});
				
				/**
				 * Get all available options to assign a TP. (available countries, available tags,...)
				 * Method GET
				 * url /crm/contact/options
				 */
				$environment->app->get('/options',function() use ($environment){
					$environment->response->addData(ContactsManagement::getContact()->get());
					$environment->response->send();
				});
				
				/** GROUP TAGS */
				$environment->app->group('/tags',function() use($environment){
					/**
					 * Get available tags from users elements
					 */
					$environment->app->get('',function() use($environment){
						$environment->response->addData (TagsManagement::getTagsList(Table::CONTACTS));
						$environment->response->send();
					});
							
					$environment->app->post('',function() use($environment){
						$data = route::checkParams ('id_parent','name'); // Check params and also returns _get && _post object params
						$environment->response->addData(TagsManagement::addTag($data,Table::CONTACTS));
						$environment->response->send(Status::S2_Created);
					});
				});
						
				
				/**
				 * Get a contact
				 * Method GET
				 * url /crm/contact/:id
				 */
				$environment->app->get ('/:id' , function ($id) use($environment) {
					$rsp = ContactsManagement::getContact($id)->get();
					$environment->response->addData($rsp);
					$environment->response->send();
				});
				
				/**
				 * Add new contact
				 * Method POST
				 * url /crm/contact
				 */
				$environment->app->post('',function() use ($environment){
					$data = route::checkParams('name');
					$rsp = ContactsManagement::setCT(null,$data);
					$environment->response->addData ($rsp);
					$environment->response->send(Status::S2_Created);
				});
					
				/**
				 * Update contact
				 * Method Put
				 * url /crm/contact/:id
				 */
				$environment->app->put('/:id',function($id) use ($environment){
					$data = route::checkParams();
					ContactsManagement::setCT($id, $data);
					$environment->response->send();
				});
								
				/**
				 * Delete contact
				 * Method Delete
				 * url /crm/contact
				 */
				$environment->app->delete('/:id',function($id) use ($environment){
					ContactsManagement::delCT($id);
					$environment->response->send();
				});
				
				/**
				 * Upload photo
				 * url - /crm/contact/:id/photo
				 * method - POST
				 */
				$environment->app->post ('/:id/photo', function ($id) use($environment) {
					$data = route::checkParams();
					$rsp = ContactsManagement::uploadLogo($id,$data);
					$environment->response->addData($rsp);
					$environment->response->send(Status::S2_Created);
				});
				
				
				/**
				 * Get contact emails
				 * Method GET
				 * url /crm/contact/:id/emails
				 */
				$environment->app->get ('/:id/emails' , function ($id) use($environment) {
					$rsp = ContactsManagement::getContactEmails($id);
					$environment->response->addData($rsp);
					$environment->response->send();
				});
				
				/**
				 * Add contact email
				 * Method POST
				 * url /crm/contact/:id/emails
				 */
				$environment->app->post ('/:id/emails' , function ($id) use($environment) {
					$data = route::checkParams('email');
					ContactsManagement::addContactEmails($id,$data);
					$environment->response->send(Status::S2_Created);
				});
				
				/**
				 * Update contact email
				 * Method PUT
				 * url /crm/contact/:id/emails/:eid
				 */
				$environment->app->put ('/:id/emails/:eid' , function ($id,$eid) use($environment) {
					$data = route::checkParams('email');
					ContactsManagement::updateContactEmails($id,$eid,$data);
					$environment->response->send();
				});
				
				/**
				 * Delete a contact email
				 * Method DELETE
				 * url /crm/contact/:id/emails/:eid
				 */
				$environment->app->delete ('/:id/emails/:eid' , function ($id,$eid) use($environment) {
					ContactsManagement::deleteContactEmails($id,$eid);
					$environment->response->send();
				});
				
				/**
				 * Accepts eids array in body on request.
				 * 
				 * Delete multiple contact email
				 * Method DELETE
				 * url /crm/contact/:id/emails
				 */
				$environment->app->delete ('/:id/emails' , function ($id) use($environment) {
					$data = route::checkParams('eids');
					ContactsManagement::deleteContactEmails($id,$data);
					$environment->response->send();
				});
			});
			/**
			 * @END
			 * CRM/CONTACTS ROUTES
			 */
		});
		// Run parent default uri control
		parent::run();
	}
}

?>