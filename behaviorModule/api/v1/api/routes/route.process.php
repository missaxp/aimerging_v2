<?php

namespace api\route;

use core\Environment;
use core\route;
use core\http\Status;
use dataAccess\dao\ProcessDAO;

class process extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, $needsAuth );
	}
	
	
	
	public function run() {
		$authFunction = $this->authFunction;
		$environment = Environment::getInstance ();
		// GET route
		self::$environment->app->group ( '/'.$this->urlspace, function () use($environment) {
				$environment->app->get ( '/:pid', function ($pid) use ($environment) {
				$pdao = new ProcessDAO();
				$process = $pdao->getProcess($pid);
				
				if($process->getIdProcess()==""){
					$environment->response->send(Status::S4_NotFound);
				}
				else{
					$environment->response->addData($process->toArray());
					$environment->response->send();
				}
				
				
				
				
			});
		});
		parent::run(); // Run parent default uri control
	}
}
?>