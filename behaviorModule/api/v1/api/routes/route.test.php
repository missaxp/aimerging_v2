<?php
namespace api\route;

use core\Environment;
use core\route;

class test extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, $needsAuth );
	}
	public function run() {
		$authFunction = $this->authFunction;
		$environment = Environment::getInstance ();
		// GET route
		self::$environment->app->get ('/'.$this->urlspace, function () use ($environment) {
			//$environment->dbcon->execute("SELECT FIELD1 FROM THIRD_PARTY WHERE (");
			
			
			$lang = 'es';
			
			$lids = array('615077',
					'615190',
					'615309',
					'615376',
					'616191',
					'616461',
					'616600',
					'616601',
					'616608',
					'616703',
					'616421',
					'617022',
					'617106',
					'617482',
					'617504',
					'617527',
					'617532',
					'617538',
					'617540',
					'617243',
					'617476',
					'617502',
					'617546',
					'617549',
					'617553',
					'617559',
					'617573',
					'617610',
					'617611',
					'617617',
					'617671',
					'617674',
					'617703',
					'617716',
					'617717',
					'617790',
					'617804',
					'617813',
					'617818',
					'617822',
					'617924',
					'618003',
					'617797',
					'617803',
					'618009',
					'618107',
					'618115',
					'617839',
					'617971',
					'618045',
					'618132',
					'618160',
					'618482',
					'618146',
					'618147',
					'618153',
					'618122',
					'618141',
					'618530',
					'618613',
					'618634',
					'618414',
					'618773',
					'618775');
			
			/* //lids GL
			 * $lids = array('616421',
					'617037',
					'617032',
					'617502',
					'618482',
					'618146',
					'618147',
					'618153',
					'617995',
					'618392',
					'618567',
					'618568',
					'618773',
					'618775');*/
			
			
			/* //LIDS CA
			 * $lids = array('615077',
					'615551',
					'615564',
					'615590',
					'615828',
					'616191',
					'616750',
					'616949',
					'616421',
					'617022',
					'617028',
					'617037',
					'617072',
					'617209',
					'616639',
					'617112',
					'616918',
					'616938',
					'617032',
					'617141',
					'617235',
					'617444',
					'617482',
					'617504',
					'617527',
					'617532',
					'617495',
					'617502',
					'617546',
					'617559',
					'617566',
					'617573',
					'617765',
					'617542',
					'617543',
					'617431',
					'617432',
					'617437',
					'617438',
					'617596',
					'617605',
					'617609',
					'617610',
					'617611',
					'617612',
					'617667',
					'617669',
					'617670',
					'617671',
					'617672',
					'617674',
					'617716',
					'617804',
					'617813',
					'617837',
					'617924',
					'618003',
					'618117',
					'618009',
					'618107',
					'617982',
					'618045',
					'618160',
					'618478',
					'618482',
					'618492',
					'618146',
					'618147',
					'618153',
					'618122',
					'618141',
					'617992',
					'617995',
					'618554',
					'618559',
					'618579',
					'618567',
					'618568',
					'484633',
					'618648',
					'618773',
					'618775',
					'618826');*/

			$environment->response->type = 'HTML';
			foreach($lids as $lid){
				$dbcon = $environment->dbcon_webtraduc;
				$sql = "select id_tasca,descripcio,desti,tipus_tasca from tasques_all where 
				projecte in 
				(select codi from sygesprojectes where ter_codi in 
				(select codi from sygestercer where nom like '%LIONBRIDGE%' and sa_codsa='50' 
				and nom not like '(%') 
				and upper(fabricant) like '%GOOGLE%')
				and descripcio like '%$lid%-$lang%' and tipus_tasca in ('Translation','ProofReading')";

				$translator = "-";
				$proofer = "-";
				$description = "";
				$found = false;
				$rs = $dbcon->execute($sql);
				while($rs->fetch()){
					
					$found = true;
					if($rs->tipus_tasca=='Translation'){
						$description = $rs->descripcio;
						$translator = $rs->desti;
					}
					else{
						if($description==""){
							$description = $rs->descripcio;
						}
						$proofer = $rs->desti;
					}
				}
				$rs->close();
				if($found){
					echo $description.",".$translator.','.$proofer.'<br />';
				}
				else{
					echo '-,-,-<br />';
				}
			}
			die();
			
			
			$environment->response->send();
		});

		parent::run(); // Run parent default uri control
	}
}