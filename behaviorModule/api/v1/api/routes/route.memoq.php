<?php

namespace api\route;

use core\Environment;
use core\http\Status;
use api\crud\memoq as memoqM;
use api\crud\Excel;
use core\route;

class memoq extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, $needsAuth );
	}
	public function run() {
		$authFunction = $this->authFunction;
		
		self::$environment->app->group ( '/' . $this->urlspace, function () use($authFunction) {
			$environment = Environment::getInstance ();
			/**
			 * Create new MemoQ task on current IDCP DATABASE (ASP IDCP).
			 * url /memoq/task
			 * Method POST
			 */
			$environment->app->post ( '/task', $authFunction, function () use($environment) {
				$data = memoqM::createTask ();
				$environment->response->addData ( $data );
				$environment->response->send ( Status::S2_OK );
			} );
			
			$environment->app->get ( '/task', $authFunction, function () use($environment) {
				$tasks = memoqM::listMAECTasks ();
				$environment->response->addData ( $tasks );
				$environment->response->send ( Status::S2_OK );
			} );
			
			$environment->app->get ( '/hasFiles/:id', $authFunction, function ($id) use($environment) {
				$hf = memoqM::hasFiles ( $id );
				$environment->response->addData ( $hf );
				$environment->response->send ( Status::S2_OK );
			} );
			
			$environment->app->get ( '/po', $authFunction, function () use($environment) {
				require_once './common/class.Excel.php';
				$fn = Excel::createMaecPO ();
				$environment->response->addData ( $fn );
				$environment->response->send ( Status::S2_OK );
			} );
		} );
		// Run parent default uri control
		parent::run ();
	}
}
?>