<?php
namespace api\route;

use core\Environment;
use core\route;
use common\Table;
use api\crud\FileManagement;

require './api/crud/class.FileManagement.php';

class maec extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, $needsAuth );
	}
	public function run() {
		$authFunction = $this->authFunction;
		$environment = Environment::getInstance();

		$environment->app->get( '/'.$this->urlspace,function() use ($environment){
			$environment->response->addData("HOLA");
			$environment->response->send();
		});
		
		/**
		 * Upload a new document to be processed by memoQ
		 *
		 * Method POST
		 * url /maec/uploadDocuments
		 */
		$environment->app->post( '/'.$this->urlspace.'/uploadDocuments',$authFunction,function() use ($environment){
			$data = route::checkParams();
			$rsp = FileManagement::setBinary(null, 'maec', Table::THIRD_PARTY,$data);
			$environment->response->addData($rsp);
			$environment->response->send();
		});

		parent::run(); // Run parent default uri control
	}
}