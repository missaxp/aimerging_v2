<?php

namespace api\route;


use core\Environment;
use core\route;
use core\http\Status;
use core\http\ErrorCode;
use core\Exception\AppException;
use api\crud\CoreManagement;
use core\AI;

require './api/crud/class.CoreManagement.php';

class core extends route {
	public function __construct($urlspace, $needsAuth = true) {
		parent::__construct ( $urlspace, $needsAuth );
	}
	
	/**
	 * Trick invoke.
	 * Dona un grau de securització adicional al permetre només peticions internes del mateix servidor.
	 * @return boolean
	 * @return
	 */
	public function __invoke(){
		if(self::$environment->development){
			return true;
		}
		
		$uniqueToken ="AAAAAABBB";
		//If it is production environment &&  If request comes from the same server, go ahead && auth special token is sent...
		if(!(self::$environment->app->request()->headers("Authorization")==$uniqueToken && self::$environment->request->getIp()==$_SERVER['SERVER_ADDR'])){
			throw new AppException ( Status::S4_Unauthorized, ErrorCode::Unauthorized);
		}
		
		return true;
		
	}
	
	private function checkDataAuthenticity($params){
		if(!isset($params["authHash"])){
			throw new AppException(Status::S4_PreconditionFailed,ErrorCode::NotAcceptable);
		}
		
		$data = $params;
		$receivedAuth = $data["authHash"];
		unset($data["authHash"]);
		$data["cryptToken"] = AI::getInstance()->getSetup()->apiPrivateToken;
		
		$expectedAuth = sha1(http_build_query($data));
		
		return ($receivedAuth===$expectedAuth);
	}
	
	public function run() {
		$authFunction = $this;
		
		self::$environment->app->group ( '/' . $this->urlspace, $authFunction, function (){
			$environment = Environment::getInstance ();
			
			/**
			 * Async download files
			 * url /core/helper
			 * GROUP
			 */
			self::$environment->app->group('/helper', function(){
				$environment = Environment::getInstance ();
				
				/**
				 *
				 * url /downloadFile
				 * method POST
				 */
				$environment->app->post ('/downloadFile', function () use($environment) {
					parse_str($environment->request->getBody(), $params);
					
					
					if(!$this->checkDataAuthenticity($params)){
						throw new AppException(($environment->development? Status::S4_PreconditionFailed:Status::S4_NotFound), ($environment->development? ErrorCode::DataAuthenticityViolated:ErrorCode::NotFound));
					}
					
					$environment->response->addData(CoreManagement::downloadFile($params));
					$environment->response->send(Status::S2_Accepted);
				});
			});

		});
		// Run parent default uri control
		parent::run();
	}
}
?>