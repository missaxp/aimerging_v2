<?php

namespace api\route;

use common\exceptions\AIException;
use core\Environment;
use core\route;
use core\http\Status;
use core\http\ErrorCode;
use core\Exception\AppException;
use core\AI;
use dataAccess\dao\SourceDAO;
use dataAccess\SQLException;
use Functions;
use harvestModule\Harvest;
use model\Rule;
use model\Source;
use model\Task;
use Throwable;

register_shutdown_function(function(){
    //ob_clean();
    if ($error = error_get_last ()) {
        $customError = "Error";
        switch ($error ['type']) {
            case E_ERROR :
                $customError = "Fatal run-time errors.";
                break;
            case E_CORE_ERROR :
                $customError = "Fatal errors that occur during PHP's initial startup.";
                break;
            case E_COMPILE_ERROR :
                $customError = "Fatal compile-time errors.";
                break;
            case E_USER_ERROR :
                $customError = "User-generated error message.";
                break;
            case E_PARSE :
                $customError = "Compile-time parse errors.";
                break;
            case E_WARNING:
                $customError = "Run-time warnings (non-fatal errors).";
                break;
            case E_COMPILE_WARNING:
                $customError = "Compile-time warnings (non-fatal errors).";
                break;
            default:
                $customError = "Error php";
                break;
        }

        if(isset($_COOKIE["idExecution"])){
            $ai = AI::getInstance();
            $ai->getAIDAO()->saveEndExecution($_COOKIE["idExecution"], Functions::currentDate(), AI::FAIL);
        }
        \Functions::addLog($customError, \Functions::SEVERE, json_encode($error));

        //TODO :: Deberiamos crear un proceso que determine si es encesario hacer un clean/reinicializar aquellos procesos que se han quedado en un estado no valido, limpiar ficheros, etc...


        Functions::console("AI:: Fatal error: $customError. System exit. PID: ".$_COOKIE["idExecution"]);
        die();
        //die(var_dump($error));
    }

});

class ondemand extends route {
    /**
     * @var AI
     */
    private $ai = null;

    public function __construct($urlspace, $needsAuth = true) {
        parent::__construct ( $urlspace, $needsAuth );
    }

    private function initializeAI(){
        $this->ai = AI::getInstance();
        $this->ai->init();
        $this->ai->setStartExecution(Functions::currentDate());

        $idExecution = $this->ai->getAIDAO()->saveExecution($this->ai->getStartExecution(), AI::ONDEMAND);
        $_COOKIE["idExecution"] = $idExecution;
        $this->ai->setIdExecution($idExecution);
        //Functions::console("AI::Hello. Start executing system. PID: ".$idExecution);
    }

    private function isValidSource($source){
        switch($source){
            case Source::NLG:
            case Source::APPLE_LIOX_WS:
            case Source::APPLE_WELOCALIZE_WS:
            case Source::GOOGLE_SDL:
            case Source::JIRA:
            case Source::SYMFONIEMORAVIA:
            case Source::WELOCALIZE:
            case Source::ARGOS:
                return true;
            default:
                return false;

        }
    }
    public function run() {
        $authFunction = $this->authFunction;
        $environment = Environment::getInstance ();
        // GET route
        self::$environment->app->group ( '/'.$this->urlspace, function () use($environment) {
            $environment->app->group ( '/execute', function () use ($environment) {
                $environment->app->get ( '/:source', function ($sourceName) use ($environment) {
                    $this->initializeAI();
                    try{
                        Functions::console("AI::Start Ondemand for ".$sourceName);
                        $this->ai->ondemand(strtoupper($sourceName));
                        Functions::console("AI::End Ondemand");
                    }catch (\Exception $e){
                        $this->ai->getAIDAO()->saveEndExecution($this->ai->getIdExecution(), Functions::currentDate(), AI::FAIL);
//                        \Functions::addLog($e->getMessage(), \Functions::ERROR, json_encode($e->getTrace()));
                        throw new AppException(Status::S5_InternalServerError, ErrorCode::AI_ONDEMAND_EXEUCTION_ERROR,$e->getMessage());
                    }
                    $this->ai->getAIDAO()->saveEndExecution($this->ai->getIdExecution(), Functions::currentDate(), AI::SUCCESSFULL);
                    Functions::console("AI::Stopping system. Bye");

                    $this->ai->getAIDAO()->closeConnection();

                    $rsp = array("ondemand" => true);

                    $environment->response->addData ($rsp);
                    $environment->response->send();
                });
                $environment->app->get ( '/:source/getTasks', function ($sourceName) use ($environment) {
                    if(!$this->isValidSource($sourceName)){
                        ob_end_clean();
                        $environment->response->error = true;
                        $environment->response->message = "Source $sourceName not available or it is not defined";
                        $environment->response->send();
                        return;
                    }
                    $this->initializeAI();
                    //construyendo la instancia del source
                    $sourceDAO = new SourceDAO();
                    $res = array();
                    $aiStatus = AI::SUCCESSFULL;
                    try{
                        $source = $sourceDAO->getSourceByName($sourceName);
                        $users = $sourceDAO->getUsersSources($source->getIdSource());
                        $structureSource = Harvest::createObjectSource($source, $users);

                        $res = $structureSource->getAvailableTask();


                    }
                    catch(SQLException $eSql){
                        $aiex = AIException::createInstanceFromSQLException($eSql)->construct(__METHOD__, __NAMESPACE__, $f = func_get_args());
                        Functions::logException($aiex->getMessage(), Functions::ERROR, __CLASS__, $aiex);$aiex = AIException::createInstanceFromSQLException($eSql)->construct(__METHOD__, __NAMESPACE__, $f = func_get_args());
                        ob_end_clean();
                        $aiStatus = AI::FAIL;
                        $environment->response->error = true;
                        $environment->response->code_error = 500;
                        $environment->response->message = $aiex->getMessage();
                        $environment->response->send();
                        return;
                    } catch(AIException $aie){
                        Functions::logException($aie->getMessage(), Functions::ERROR, __CLASS__, $aie);
                        ob_end_clean();
                        $aiStatus = AI::FAIL;
                        $environment->response->error = true;
                        $environment->response->code_error = 500;
                        $environment->response->message = $aie->getMessage();
                        $environment->response->send();
                        return;
                    }
                    catch (Throwable $t){
                        $aiex = AIException::createInstanceFromThrowable($t)->construct(__METHOD__, __NAMESPACE__, $f = func_get_args());
                        Functions::logException($aiex->getMessage(), Functions::ERROR, __CLASS__, $aiex);
                        ob_end_clean();
                        $aiStatus = AI::FAIL;
                        $environment->response->error = true;
                        $environment->response->code_error = 500;
                        $environment->response->message = $aiex->getMessage();
                        $environment->response->send();
                        return;
                    }
                    finally{
                        $this->ai->getAIDAO()->saveEndExecution($this->ai->getIdExecution(), Functions::currentDate(), $aiStatus);
                    }

                    if(!empty($res)){
                        $urlParams = route::checkParams();
                        $urlParams = get_object_vars($urlParams);
                        if(!empty($urlParams)){
                            $rules = $this->createRules($urlParams);
                            $res = $this->filterTask($res, $rules);
                        }
                    }
                    ob_end_clean();
                    $environment->response->addData ($res );
                    $environment->response->send();
                });

                /**
                 *
                 */
                $environment->app->get ( '/:source/:idClientTask', function ($sourceName,$idClientTask) use ($environment) {
                    $this->initializeAI();
                    $ai = AI::getInstance();
                    $ai->getBehavior()->addOndemandConfiguration("link","/ondemand/execute/$sourceName/$idClientTask");
                    $ai->getBehavior()->addOndemandLog($idClientTask,array());
                    try{
                        $ai->ondemand($sourceName,array($idClientTask));
                    }catch (\Exception $ex){
                        $ai->getBehavior()->getOndemandLog()[$idClientTask]["state"] = false;
                    }
                    ob_end_clean();
                    $environment->response->addData($ai->getBehavior()->getOndemandLog());
                    $environment->response->send();
                });

                /**
                 *
                 */
                $environment->app->get ( '/:source/:idClientTask/:TMSAssignToId', function ($sourceName,$idClientTask,$idUser) use ($environment){
                    $this->initializeAI();
                    $ai = AI::getInstance();

                    $idcp = $ai->getBehavior()::tms(new Task(),AI::getInstance()->getSetup()->development);

                    if($idcp->checkifIsValidTMSUser($idUser)){
                        $ai->getBehavior()->addOndemandConfiguration("link","/ondemand/execute/$sourceName/$idClientTask");
                        $ai->getBehavior()->addOndemandConfiguration("translator",$idUser);
                        $ai->getBehavior()->addOndemandLog($idClientTask,array());
                        try{
                            $ai->ondemand($sourceName,array($idClientTask));
                        }catch (\Exception $ex){
                            $ai->getBehavior()->getOndemandLog()[$idClientTask]["state"] = false;
                        }
                    }else{
                        $environment->response->error = true;
                        $environment->response->code_error = 401;
                        $environment->response->message = "User not found";
                    }
                    ob_end_clean();
                    $environment->response->addData($ai->getBehavior()->getOndemandLog());
                    $environment->response->send();
                });

                /**
                 *
                 *
                 */
                $environment->app->post ( '/:source/collectTasks', function ($sourceName) use ($environment){
                    $data = route::checkParams ();
                    $this->initializeAI();
                    $ai = AI::getInstance();
                    $idcp = $ai->getBehavior()::tms(new Task(),AI::getInstance()->getSetup()->development);
                    if(isset($data->tmsUser) && isset($data->idtasks)){
                        if($idcp->checkifIsValidTMSUser($data->tmsUser)){
                            $ai->getBehavior()->addOndemandConfiguration("translator",$data->tmsUser);
                            $ai->getBehavior()->addOndemandConfiguration("link","/ondemand/execute/$sourceName/collectTasks");

                            foreach($data->idtasks as $idClientTask){
                                    $ai->getBehavior()->addOndemandLog($idClientTask,array());
                                try{
                                    $ai->ondemand($sourceName,array($idClientTask));
                                }catch (\Exception $ex){
                                    $ai->getBehavior()->getOndemandLog()[$idClientTask]["state"] = false;
                                }
                            }
                            
                        }else{
                            $environment->response->error = true;
                            $environment->response->code_error = 401;
                            $environment->response->message = "User not found";
                        }
                    }else{
                        $environment->response->error = true;
                        $environment->response->code_error = 401;
                        $environment->response->message = "Request fail";
                    }
                    ob_end_clean();
                    $environment->response->addData($ai->getBehavior()->getOndemandLog());
                    $environment->response->send();
                });
            });
        });
        parent::run(); // Run parent default uri control
    }

    private function filterTask(array $tasks, array $rules) {
        $res = array();
        foreach ($tasks as $task){
            foreach ($rules as $rule){
                if(!$rule->check($task->asTask())) {
                    continue 2;
                }
            }
            $res[] = $task;
        }
        return $res;
    }

    private function createRules($urlParams){
        $res = array();
        $propertiesToFilter = array_keys ($urlParams);
        foreach ($propertiesToFilter as $property){
            $paramData = explode("|", $urlParams[$property]);
            if(count($paramData) != 2){
                continue;
            }
            $operator = $this->getOperator($paramData[0]);
            $value = $paramData[1];
            $rule = new Rule();
            $rule->setOperator($operator);
            $rule->setProperty($property);
            $rule->setValue($value);
            $res[] = $rule;
        }
        return $res;
    }
    private function getOperator($string){
        switch($string){
            case Rule::EQUALS: return Rule::EQUALS;
            case Rule::LESS: return Rule::LESS;
            case Rule::LESSEQUALS: return Rule::LESSEQUALS;
            case Rule::GREATER: return Rule::GREATER;
            case Rule::GREATEREQUALS: return Rule::GREATEREQUALS;
            case Rule::CONTAINS: return Rule::CONTAINS;
            case Rule::NOTCONTAINS: return Rule::NOTCONTAINS;
            default: return Rule::CONTAINS;
        }
    }
}
?>