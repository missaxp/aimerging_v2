<?php

/**
 * Fields management class
 *
 * @author phidalgo
 * @namespace api\crud
 */
namespace api\crud;

use core\Environment;
use query\Query;

abstract class EventsManagement {
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	public static function getStaticAndDynamicEvents(){
		$env = Environment::getInstance();
		$var["LANG"] = $env->user->ui_language;
		$sql = "SELECT TE.ID,(SELECT TITLE FROM TASK_EVENT_LANG WHERE E_ID=TE.ID AND LANGUAGE=:LANG) AS LABEL,TE.NAME,TE.TYPE FROM TASK_EVENTS TE ORDER BY NAME ASC";
		$rs = $env->dbcon->execute($sql,$var);
		$ae = $rs->getAll();
		$rs->close();
		return $ae;
	}
	
	public static function _get($paging = true,$defaultReturn = null){
		$env = Environment::getInstance();
		$query = new Query();
		$query->defaultReturn($defaultReturn); //Set default return if any.
		
		$query->select("TE.ID,TE.SUCCESS_EVENT,TE.FAIL_EVENT,TE.CONFIRM_TO_NEXT_STEP,TE.NAME");
		$query->from("TASK_EVENTS TE,TASK_EVENT_LANG EDL");
		$query->order("NAME", "TE.NAME");
		$query->where("TE.TYPE='D'");
		
		if($paging){
			return $query->paging();
		}
		else{
			return $query->execute();
		}
	}
	

	public static function _getById($eid){
		if(strtoupper($eid)=='OPTIONS'){
			$e = new Event();
		}
		else{
			$e = new Event($eid);
		}
		return $e->get();
	}
	
	public static function _update($eid,$data){
		$e = new Event($eid);
		return $e->set($data)->save();
	}
	
	public static function _add($data){
		$e = new Event();
		return $e->set($data)->save();
	}
	
	public static function _delete($eid){
		$e = new Event($eid);
		return $e->delete();
	}
	
}
/* end of abstract class ProjectsManagement */
?>