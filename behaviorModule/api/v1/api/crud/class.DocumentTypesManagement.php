<?php
namespace api\crud;

use query\Query;
use core\Environment;
use query\_OO;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;

/**
 * Class DocumentsTypesManagement
 *
 * @author phidalgo
 * @namespace api\crud
 */
abstract class DocumentTypesManagement {

	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
		
	public static function getDocumentTypes(){
		$query = new Query();
	
		$query->select("ID", "DT.ID")
		->select("NAME");
		$query->from("DOCUMENT_TYPE DT");
		$query->filter("NAME","H.NAME")->operator(_OO::LIKE);
		$query->filter("ID","H.ID");
		$query->order("NAME","H.NAME");
	
		return $query->paging();
	}
	
	public static function addDocumentType($id = null, $data){
		self::$environment = Environment::getInstance();
	
		$rsp = array();		
		$rsp["FormError"] = false;
		$rsp["ErrorMsg"] = "";
			
			
		$vars = array();		
		foreach($data as $prop => $value){
			$vars[$prop] = $value;
		
			$lenValue = strlen($value);
		
			if ($prop == "id") $maxLength = 1;
			if ($prop == "name") $maxLength = 80;
					
			if ($lenValue > $maxLength) throw new AppException(Status::S4_PreconditionFailed,ErrorCode::LengthError,"Field $prop maximum length error. Max : $maxLength. Length :  $lenValue.");
		
		}
	
		if (is_null ($id)) {
										
			$rs = self::$environment->dbcon->execute("SELECT * FROM DOCUMENT_TYPE WHERE ID=:DTYPE_ID",array("DTYPE_ID" => $data->id));
			$fetch = $rs->getAll();
			$rs->close();					
			if(count($fetch)==0){			
				$rsp["id"] = $data->id;
				$vars = array("ID" => $data->id, "NAME" => $data->name);
				self::$environment->dbcon->add("DOCUMENT_TYPE", $vars);							
			} else {
				$cCode = $fetch[0]['ID'];
				$cName = $fetch[0]['NAME'];				
				throw new AppException(Status::S4_PreconditionFailed, ErrorCode::NotAcceptable, "Document type  $cName ( $cCode ) exist");								
			}			
			
		} else {
			$rsp["id"] = $id;
			$varsW["DOCTYPE_ID"] = $id;
			self::$environment->dbcon->update("DOCUMENT_TYPE", $vars,"ID=:DOCTYPE_ID",$varsW);
		}	
				
		return $rsp;			
	}
		
		public static function documentTypeNotExist($id){
			self::$environment = Environment::getInstance();
			
			$rs = self::$environment->dbcon->execute("SELECT * FROM DOCUMENT_TYPE WHERE ID=:DOCTYPE_ID",array("DOCTYPE_ID" => $id));
			$fetch = $rs->getAll();
			$rs->close();
			if(count($fetch)==0){
				return true;
			} 	
			return false;		
		}
			
		public static function getDocumentTypeById($id){
			self::$environment = Environment::getInstance();
			
			$rs = self::$environment->dbcon->execute("SELECT * FROM DOCUMENT_TYPE WHERE ID=:DOCTYPE_ID",array("DOCTYPE_ID" => $id));
			$fetch = $rs->getAll();
			$rs->close();
			if(count($fetch)==0){
				throw new AppException(Status::S4_NotFound,ErrorCode::NotFound,"Document type resource $id not found");
			}
			$fetch = $fetch[0];
			return $fetch;
			
		}
		
		
		public static function deleteDocumentTypeById($id){
			$environment = Environment::getInstance ();
			
			$num = 0;
			$sql = "SELECT count(1) AS NUM FROM THIRD_PARTY WHERE DOCUMENT_ID=:ID";
			$rs = $environment->dbcon->execute ( $sql, array (
					"ID" => $id 
			) );
			if ($rs->fetch ()) {
				$num = $rs->getVal ("NUM");
			}
			$rs->close ();
			
			if ($num > 0)
				throw new AppException ( Status::S4_PreconditionFailed, ErrorCode::CanNotDelete, "@LITERAL: Error there are $num thirtdpary in this id ($id) document type. Can not delete it." );
			
			try {
				$afectedRows = $environment->dbcon->execute ( "DELETE FROM DOCUMENT_TYPE WHERE ID=:DOCTYPE_ID", array (
						"DOCTYPE_ID" => $id 
				) );
			} catch ( \Exception $e ) {
				throw new AppException ( Status::S5_InternalServerError, ErrorCode::DBQueryError, $e->getMessage () );
			}
			
			if ($afectedRows === true)
				throw new AppException ( Status::S4_NotFound, ErrorCode::CanNotDelete, "@LITERAL: Can not delete because this document type does not exists." );
		
		}
		
		public static function deleteDocumentTypes($data){
			$environment = Environment::getInstance ();
						
			$totalItem = 0;
			$itemsDeleted = 0;
			$itemsNotDeleted = 0;
					
		
			foreach($data as $prop => $values){
				
				if ($prop == "arrayIds") {
					foreach($values as $value){					
						$num = 0;
						$totalItem++;
						$sql = "SELECT count(1) AS NUM FROM THIRD_PARTY WHERE DOCUMENT_ID=:DOCTYPE_ID";
						$rs = $environment->dbcon->execute ( $sql, array (
								"DOCTYPE_ID" => $value
						) );
						if ($rs->fetch ()) {
							$num = $rs->getVal ("NUM");
						}
						$rs->close ();
	
						if ($num > 0) {
							// No es pot borrar						
							$itemsNotDeleted++;
							$rsp["NoDeletedItemsCode"][] = $value;
							
						} else {
							// Borrar													
							$afectedRows = $environment->dbcon->execute ( "DELETE FROM DOCUMENT_TYPE WHERE ID=:DOCTYPE_ID", array (
										"DOCTYPE_ID" => $value
							) );							
							$itemsDeleted++;
							$rsp["DeletedItemsCode"][] = $value;						
						}															
					}				
				}			
			}
			$rsp["TotalItems"] = $totalItem;		
			$rsp["DeletedItems"] = $itemsDeleted;
			$rsp["NoDeletedItems"] = $itemsNotDeleted;
			
			return $rsp;
			
		}
		
}