<?php

namespace api\crud;


use core\Environment;
use query\Query;
use api\system\Qualification;

abstract class QualificationsManagement {
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	public static function _get($paging = true){
		$query = new Query();
		$query->select("ID","Q.ID");
		$query->select("DESCRIPTION","Q.DESCRIPTION");
		$query->select("CREATED", "Q.CREATION_TS");
		$query->order("ID","Q.ID");
		$query->order("CREATED","Q.CREATION_TS");
		$query->order("DESCRIPTION","Q.DESCRIPTION");
		$query->from("QUALIFICATIONS Q");
		return ($paging? $query->paging():$query->execute());
	}
	
	/**
	 * Get the available chapters for the user filtered by department.
	 * @param integer|null $user_department
	 */
	public static function getQualificationsForUser($user_department = null){
		$environment = Environment::getInstance();
		$vars = array();
		$dptSQL = "";
		if($user_department!=null){
			$dptSQL= "WHERE (DEPARTMENT_ID=:DPT_UID OR DEPARTMENT_ID is null) ";
			$vars["DPT_UID"] = $user_department;
		}
	
		$rs = $environment->dbcon->execute("SELECT ID,DESCRIPTION FROM QUALIFICATIONS $dptSQL ORDER BY DESCRIPTION",$vars);
		$qualifications = $rs->getAll();
		$rs->close();
		return $qualifications;
	}
	
	public static function options(){
		$fetch["departments"] = array();
		$fetch["options"]["departments"] = DepartmentManagement::getDepartments(false);
		return $fetch;
	}
	
	public static function _getById($qid){
		$qualification = new Qualification($qid);
		return $qualification->get();
	}
	
	public static function _add($data){
		$qualification = new Qualification();
		$qualification->set($data);
		return $qualification->save();
	}
	
	public static function _update($qid,$data){
		$qualification = new Qualification($qid);
		$qualification->set($data);
		return $qualification->save();
	}
	
	public static function _delete($qid){
		$qualification = new Qualification($qid);
		return $qualification->delete();
	}
	
	public static function _getByUser($uid){
		self::$environment = Environment::getInstance();
		$rs = self::$environment->dbcon->execute("SELECT Q.QUALIFICATION_ID,Q.APPROVED_TS,
		(SELECT USERNAME FROM USERS WHERE ID=Q.APPROVED_BY) AS APPROVED_BY,
		Q.APPROVED FROM USERS_QUALIFICATIONS Q	WHERE Q.USER_ID=:USER_ID",array("USER_ID" => $uid));
		$qa = $rs->getAll();
		$rs->close();
		foreach($qa as &$q){
			$q["approved"] = ($q["approved"]=="Y"? true:false);
		}
		return $qa;
	}
	
	public static function addUserQualification($uid,$q){
		self::$environment = Environment::getInstance();
		if($q->value=="Y"){
			$vars["USER_ID"] = $uid;
			$vars["QUALIFICATION_ID"] = $q->id;
			$vars["APPROVED"] = "N";
			self::$environment->dbcon->add("USERS_QUALIFICATIONS",$vars);
		}
		elseif ($q->value=="N"){
			$vars["QUALIFICATION_ID"] = $q->id;
			self::$environment->dbcon->execute("DELETE FROM USERS_QUALIFICATIONS WHERE QUALIFICATION_ID=:QUALIFICATION_ID",$vars);
		}
	}
	
	public static function approveQualification($uid,$qid){
		self::$environment = Environment::getInstance();
		$vars["APPROVED_BY"] = $uid;
		$vars["APPROVED"] = "Y";
		$vars["APPROVED_TS"] = self::$environment->getDate();
		$vW["QID"] = $qid;
		
		self::$environment->dbcon->update("USERS_QUALIFICATIONS",$vars,"QUALIFICATION_ID=:QID",$vW);
	}
}
?>