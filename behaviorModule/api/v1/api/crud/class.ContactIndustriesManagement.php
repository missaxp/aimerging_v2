<?php
namespace api\crud;

use query\Query;
use core\Environment;
use query\_OO;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;

/**
 * Class ContactIndustriesManagement
 *
 * @author phidalgo
 * @namespace api\crud
 */
abstract class ContactIndustriesManagement {

	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
		
	public static function getContactIndustries($paging = true){
		$query = new Query();
				
		$query->select("ID","CI.ID")
		->select("NAME","CI.NAME");		
		$query->from("CONTACT_INDUSTRY CI");
		$query->filter("NAME","CI.NAME")->operator(_OO::LIKE);
		$query->filter("ID","CI.ID");
		$query->order("NAME","CI.NAME");
		if($paging){
			return $query->paging();
		}
		else{
			return $query->execute();
		}
		
	}
	
	public static function addContactIndustry($id = null, $data){
		self::$environment = Environment::getInstance();
	
		$rsp = array();		
		$rsp["FormError"] = false;
		$rsp["ErrorMsg"] = "";
			
		
		$vars = array();		
		foreach($data as $prop => $value){
			$vars[$prop] = $value;
		
			$lenValue = strlen($value);
		
			if ($prop == "id") $maxLength = 10;
			if ($prop == "name") $maxLength = 100;
					
			if ($lenValue > $maxLength) throw new AppException(Status::S4_PreconditionFailed,ErrorCode::LengthError,"Field $prop maximum length error. Max : $maxLength. Length :  $lenValue.");
		
		}
			
		if (is_null ($id)) {
										
				$rs = self::$environment->dbcon->execute("SELECT max(ID) AS ID FROM CONTACT_INDUSTRY");
				$fetch = $rs->getAll();
				$rs->close();
				
				if ($fetch[0]["id"] == '') {
					$idcontact = 1;
				} else {
					$idcontact = $fetch[0]["id"] + 1;
				}
							
				$rsp["id"] = $idcontact;
				$vars = array("ID" => $idcontact, "NAME" => $data->name);
				self::$environment->dbcon->add("CONTACT_INDUSTRY", $vars);									
		} else {
			$rsp["id"] = $id;
			$varsW["CID"] = $id;
			self::$environment->dbcon->update("CONTACT_INDUSTRY", $vars,"ID=:CID",$varsW);
		}	
				
		return $rsp;			
	}
						   
	public static function contactIndustryNotExist($id){
		self::$environment = Environment::getInstance();
		
		$rs = self::$environment->dbcon->execute("SELECT * FROM CONTACT_INDUSTRY WHERE ID=:CID",array("CID" => $id));
		$fetch = $rs->getAll();
		$rs->close();
		if(count($fetch)==0){
			return true;
		} 	
		return false;		
	}
		
	public static function getContactIndustryById($id){
		self::$environment = Environment::getInstance();
		
		if (!is_numeric($id)) {
			throw new AppException(Status::S4_NotFound,ErrorCode::NotFound,"Contact industry resource $id not found");
		} 
		
		
		$rs = self::$environment->dbcon->execute("SELECT * FROM CONTACT_INDUSTRY WHERE ID=:CID",array("CID" => $id));
		$fetch = $rs->getAll();
		$rs->close();
		if(count($fetch)==0){
			throw new AppException(Status::S4_NotFound,ErrorCode::NotFound,"Contact industry resource $id not found");
		}
		$fetch = $fetch[0];
		return $fetch;
		
	}
	
	
	public static function deleteContactIndustryById($id){
		$environment = Environment::getInstance ();
		
		$num = 0;
		$sql = "SELECT count(1) AS NUM FROM CONTACT WHERE INDUSTRY_ID=:CSID";
		$rs = $environment->dbcon->execute ( $sql, array (
				"CSID" => $id 
		) );
		if ($rs->fetch ()) {
			$num = $rs->getVal ("NUM");
		}
		$rs->close ();
		
		if ($num > 0)
			throw new AppException ( Status::S4_PreconditionFailed, ErrorCode::CanNotDelete, "@LITERAL: Error there are $num contact in this id ($id) nature. Can not delete it." );
		
		try {
			$afectedRows = $environment->dbcon->execute ( "DELETE FROM CONTACT_INDUSTRY WHERE ID=:CID", array (
					"CID" => $id 
			) );
		} catch ( \Exception $e ) {
			throw new AppException ( Status::S5_InternalServerError, ErrorCode::DBQueryError, $e->getMessage () );
		}
		
		if ($afectedRows === true)
			throw new AppException ( Status::S4_NotFound, ErrorCode::CanNotDelete, "@LITERAL: Can not delete because this contact industry does not exists." );
	
	}
	
	public static function deleteContactIndustries($data){
		$environment = Environment::getInstance ();
					
		$totalItem = 0;
		$itemsDeleted = 0;
		$itemsNotDeleted = 0;
				
	
		foreach($data as $prop => $values){
			
			if ($prop == "arrayIds") {
				foreach($values as $value){					
					$num = 0;
					$totalItem++;
					
					$sql = "SELECT count(1) AS NUM FROM CONTACT WHERE INDUSTRY_ID=:CSID";
					$rs = $environment->dbcon->execute ( $sql, array (
							"CSID" => $value
					) );
										
					if ($rs->fetch ()) {
						$num = $rs->getVal ("NUM");
					}
					$rs->close ();

					if ($num > 0) {
						// No es pot borrar						
						$itemsNotDeleted++;
						$rsp["NoDeletedItemsCode"][] = $value;
						
					} else {
						// Borrar													
						$afectedRows = $environment->dbcon->execute ( "DELETE FROM CONTACT_INDUSTRY WHERE ID=:CSID", array (
									"CSID" => $value
						) );							
						$itemsDeleted++;
						$rsp["DeletedItemsCode"][] = $value;						
					}															
				}				
			}			
		}
		$rsp["TotalItems"] = $totalItem;		
		$rsp["DeletedItems"] = $itemsDeleted;
		$rsp["NoDeletedItems"] = $itemsNotDeleted;
		
		return $rsp;
		
	}
		
}