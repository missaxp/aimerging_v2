<?php
namespace api\crud;

use api\crud\mbrand;
use query\Query;
use common\Table;
use core\Environment;

abstract class MbrandManagement{
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	/**
	 * Get all mbrand projects.
	 *
	 * @access public
	 * @return array Query
	 * @throws core\Exception\AppException
	 */
	public static function _get($paging = true,$defaultReturn = null,$gps = true) {
		$query = new Query();
		$query->defaultReturn($defaultReturn); //Set default return if any.
		$query->gps($gps);
		$query->select("M.ID,M.NAME,M.CREATION_TS,M.TASK_ID,M.CONFIRMED")
		->select("M.SOURCE_LANG")
		->select("PRE_DL","M.PREFERED_DEADLINE")
		->select("TP_NAME","(SELECT NOM FROM sygestercer@gestio.idisc.es WHERE CODI=M.tp_id AND SA_CODSA='50')");
		
		$query->from("MBRAND_PROJECTS M");
		$query->order("NAME","M.NAME");
		
		return ($paging? $query->paging():$query->execute());
	}
	
	public static function _getdgest_thirdparty($paging = true,$defaultReturn = null){
		$query = new Query(null,Environment::getInstance()->dbcon_gestio);
		$query->defaultReturn($defaultReturn); //Set default return if any.
		$query->select("T.CODI AS CODE,T.NOM AS NAME");
		
		$query->where("T.NOM NOT LIKE '(%'");
		
		$query->from("TERCER T");
		$query->filter("NAME","T.NOM");
		$query->filter("CODE","T.CODI");
		$query->order("NAME","T.NOM");
		
		return ($paging? $query->paging():$query->execute());
	}
	
	public static function _getById($id = null){
		return new mbrand(($id!='options'? $id:null));
	}
	
	public static function _set($id = null,$data){
		$mbrand = new mbrand($id);
		return $mbrand->set($data)->save();
	}
	
	public static function _delete($id){
		$mbrand = new mbrand($id);
		return $mbrand->delete();
	}
	
	public static function _addtranslationtask($mid,$combo = true){
		$mbrand = new mbrand($mid);
		return $mbrand->create_localization_task($combo);
	}
	
	/**
	 * Get documents for a third party
	 * @param string $id third_party_id
	 */
	public static function _getDocuments($id){
		return FileManagement::getFiles($id, Table::MBRAND);
	}
	
	public static function _addDocument($mid = null,$data){
		
		$rsp = FileManagement::setBinary($mid, 'mbrand/'.$mid, Table::MBRAND,$data,function($file,$path,$id_rel){
		
			$environment = Environment::getInstance();

			
			$source_lang_memoq = "spa";
			$idcp_pm = "phidalgo";
			$client_ref = "mBrands";
			$target_langs_memoq = array("EN" => "eng");
			$params = array (
					"CLIENT" => $client_ref,
					"PROJECT_NAME" 			=> "mBrands",
					"Project" 				=> "mBrands",
					"SourceLanguageCode" 	=> "eng",
					"TargetLanguageCode" 	=>	$target_langs_memoq
			);
			$userMemoq = array("phidalgo");
			$guidProject = memoqManagement::projectExist ( $params );
			if ($guidProject == "") {
				unset ( $parmas );
				$params = array (
						"CLIENT" => $client_ref,
						//"PROJECT_ID" => $idProject,
						"NAME_PROJECT" => $name_project,
						"SOURCE_LANG" => $source_lang_memoq,
						"TARGET_LANG" => $target_langs_memoq,
						"USERS" => $userMemoq,
						//"TM_NAME" => $tm_memoq
				);
				$guidProject = memoqManagement::addProject ( $params );
			}
			unset ( $params );
			$params = array (
					"GUID_PROJECT" => $guidProject,
					"FILE_PATH" => $path,
					"TARGET_LANG" => $target_langs_memoq
			);
			$filesGuid = memoqManagement::uploadFileToProject ( $params );
			$params = array (
					"GUID_PROJECT" => $guidProject,
					"GUIDS_FILE" => $filesGuid,
					"TARGET_LANG" => $target_langs_memoq
			);
			$stadisticResult = memoqManagement::stadisticDocValue($params);
				
			//@unlink($path);
				
			$rsp = array();
			$rsp["memoq"]["stadisticResult"] = $stadisticResult;
				
			$vars["CM"] = $stadisticResult["EN"]["CM"];
			$vars["F100"] = $stadisticResult["EN"]["100%"];
			$vars["F95"] = $stadisticResult["EN"]["95%"];
			$vars["F85"] = $stadisticResult["EN"]["85%"];
			$vars["F75"] = $stadisticResult["EN"]["75%"];
			$vars["F50"] = $stadisticResult["EN"]["50%"];
			$vars["FNM"] = $stadisticResult["EN"]["News"];
			$vars["FREP"] = $stadisticResult["EN"]["Repes"];
			$vars["FID"] = $file;
			$vars["M_ID"] = $id_rel;
				
			$environment->dbcon->add("MBRAND_PROJECT_FILES",$vars);
				
			return $rsp;
		});
	
		$rs = Environment::getInstance()->dbcon->execute("SELECT * FROM MBRAND_PROJECT_FILES WHERE FID=:FID",array("FID" => $rsp["files"][0]->id));
		$sta = $rs->getAll();
		$rs->close();
	
		$rsp["files"][0]->wc = $sta[0];
		
		return $rsp;
	}

}