<?php

namespace api\crud;

use query\Query;
use core\Environment;
use query\_OO;
use query\_OU;
use common\Table;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;

/**
 * Class UserManagement
 *
 * @author phidalgo & ctemporal
 * @namespace api\crud
 */
abstract class UserManagement {
	
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	/**
	 * Get all users
	 * Aquest mètode és com tots els altres de llistat però té una particularitat...
	 * A veure, els usuaris com ja hem dit en altres llocs són tant registres com elements del sistema de permisos.
	 * És a dir, un usuari és un registre que té un departament propietari, que determina la visibilitat global, i té un grup propietari i un usuari propietari,
	 * amb el que es controla el sistema de permisos i permet realitzar diferents accions.
	 * I és l'usuari qui té assignat un grup al qual pertany i un departament al qual pertany.
	 * 
	 * Quan un usuari (jo) demana accés a un registre d'una taula amb GPS (Granular Permission System), el sistema determina les accions que podrà
	 * realitzar. Lectura, Escriptura, Eliminació i Administració. En el cas que jo pugui administrar el registre, podré canviar l'usuari propietari, el grup
	 * propietari i el departament propietari. Haig de poder veure tots els usuaris. Per seleccionar aquell usuari que vull que sigui el propietari. 
	 *
	 * @param boolean $paging Si volem paginació o \NoLicenseFault
	 * @param array $defaultReturn, elements de retorn si cridem el metode internament, no mirarà $_PARAM
	 * @param boolean $gps, si habilitem o no el gps, en cas que la taula mirada tingui GPS (defecte true).
	 * @return array[User]; 
	 */
	public static function getUsers($paging = true,$defaultReturn = null,$gps = true) {
		$environment = Environment::getInstance ();
		$query = new Query();
		$query->defaultReturn($defaultReturn); //Set default return if any.
		$query->gps($gps);
		
		$query->select("CONNECTED", "validsessions(U.ID,'".$environment->login_timeout."')");
		$query->select("ID", "U.ID");
		$query->select("USERNAME", "U.USERNAME");
		$query->select("RNAME", "R.NAME");
		$query->select("NAME", "U.NAME");
		$query->select("SURNAME", "U.SURNAME");
		$query->select("DNAME","(SELECT NAME FROM DEPARTMENTS WHERE ID=U.USER_DEPARTMENT)");
		$query->select("GNAME","(SELECT NAME FROM P_GROUPS WHERE ID=U.USER_GROUP)");
		$query->select("ROLE_ID", "U.ROLE_ID");
		$query->select("CREATED","U.CREATION_TS");
		
		/**
		 * This is the ALLOWED FIELDS of the WHERE Clause.
		 */
		$query->filter("CONNECTED","hasValidToken(u.id,'$environment->login_timeout')");
		$query->filter("NAME","u.NAME");
		$query->filter("USERNAME",'u.USERNAME');
		$query->filter("RNAME",'u.ROLE_ID',RoleManagement::getRoles(false,array("ID","NAME")));
		$query->filter("RNAME2",'r.NAME');
		$query->filter("SURNAME",'r.NAME');
		$query->filter("USER_DEPARTMENT","U.USER_DEPARTMENT",DepartmentManagement::getDepartments(false,array("ID","NAME")));
		$query->filter("GNAME","U.USER_GROUP",GroupManagement::_get(false,array("ID","NAME")));
	
		/**
		 * This is the ALLOWED FIELDS of the ORDER BY Clause
		 */
		$query->order("NAME",'u.NAME');
		$query->order("RNAME",'r.NAME');
		$query->order("CONNECTED",'CONNECTED');
		$query->order("USERNAME",'u.USERNAME');
		$query->order("SURNAME",'u.SURNAME');
	
		$query->from("USERS U,ROLES R");
		$query->where("U.ROLE_ID=R.ID");
		
		return ($paging? $query->paging():$query->execute());
	}
	
	/**
	 * Get User by ID
	 * If $id = null, load options.
	 *
	 * @param Integer id
	 * @return User
	 */
	public static function getUserbyId($id = null,$loadAll = true) {
		$user = new User($id,$loadAll);
		return $user;
	}
	
	/**
	 * Update User
	 * 
	 * If $id is null -> new user.
	 * 
	 * @param string $id USER_ID
	 * @param \stdClass $data
	 * @return boolean
	 */
	public static function setUser($id,$data){
		$user = new User($id);
		$user->set($data);
		return $user->save();
	}
	
	/**
	 * Delete user by ID
	 * 
	 * Database should have correct FK in order to delete cascade.
	 *
	 * @param Integer id
	 * @return bool
	 */
	public static function deleteUser($id) {
		$user = new User($id);
		$environment = Environment::getInstance ();
		$environment->security->table(Table::USERS)->access($id)->deletable(); //En principi això ja hauria de funcionar. Si falla la comprovació return 401.
		return $user->delete();
	}
	
	public static function deleteUsers($data){
		foreach($data->uids as $uid){
			$environment = Environment::getInstance ();
			$environment->security->table(Table::USERS)->access($uid)->deletable(); //En principi això ja hauria de funcionar. Si falla la comprovació return 401.
			self::deleteUser($uid);
		}
	}
	
	
	/**
	 * List user curriculums
	 * @param unknown $uid
	 */
	public static function getCurriculums($uid){
		return FileManagement::getFiles($uid, Table::USERS);
	}
	
	/**
	 * Upload Documents for a USER.
	 * @param string $id user_id
	 */
	public static function uploadCurriculums($id = null,$data){
		return FileManagement::setBinary($id, 'users/'.$id.'/curriculums', Table::USERS,$data,function($file_id) use($id){
			//Once the curriculum is uploaded, indexing it.
			$environment = Environment::getInstance ();
			$vars["FILE_ID"] = $file_id;
			$vars["USER_ID"] = $id;
			$environment->dbcon->add("USERS_CURRICULUMS",$vars); //CREATE SPECIFIC TABLE RELATION.
			$sql = "SELECT path FROM MASTER_FILE WHERE ID=:ID";
			$rs = $environment->dbcon->execute($sql, array("ID" => $file_id));
			$rs->fetch();
			$path = $rs->path;
			$rs->close();
			
			$vw["ID"] = $file_id;
			// Begin Read file
			$fh = fopen($path, 'r');
			if (!$fh) {
				throw new AppException(Status::S5_InternalServerError,ErrorCode::FILE_DEFAULT_ERROR);
			}
			$contingut = fread($fh, filesize($path));
			fclose($fh);
			//End read file
			
			$environment->dbcon->writeLob((string) $contingut, "BINARY", "MASTER_FILE", "ID=:ID", $vw); //Write file to DB.
			$sql = "begin doc_content(".$file_id."); end;"; //Get file text si es pot
			$environment->dbcon->execute($sql);
			$sql = "begin ctx_ddl.sync_index('USERS_CURRICULUM_TEXT_IX'); end;"; //Sync index.
			$environment->dbcon->execute($sql);
			$environment->dbcon->execute("UPDATE MASTER_FILE SET BINARY=empty_blob() WHERE ID=:ID",array("ID" => $file_id)); //Clear file blob.
		});
	}
	
	/**
	 * Upload User's avatar.
	 * @param string $id user_id
	 */
	public static function uploadAvatar($id = null,$data){
		return FileManagement::setAvatar($id,Table::USERS,'PHOTO',$data);
	}
	
	public static function getAvatar($id = null){
		return FileManagement::getAvatar($id,Table::USERS,'PHOTO');
	}
	
	/**
	 * Kicks the selected user from the WEBSERVICE TOKEN SERVICE
	 *
	 * @param integer $id        	
	 */
	public static function kick($id) {
		$environment = Environment::getInstance ();
		$environment->dbcon->execute("DELETE FROM SESSION_TOKEN WHERE USER_ID=:USER_ID", array("USER_ID" => $id));
	}
	
	/**
	 * List user's TOKEN's
	 */
	public static function tokenInfo() {
		$environment = Environment::getInstance ();
		/**
		 * This is the FIELDS of the RETURNED RECORDSET, this will be part of:
		 * SELECT $allow_fields FROM .
		 *
		 * ..
		 *
		 * If you need to add a specific var on a field, you must set the array like this:
		 * $allow_fields["CONNECTED"] = (object) array("field" => "validsessions(u.user_id,:TIMEOUT) AS CONNECTED",'vars' => array("TIMEOUT" => $environment->login_timeout));
		 *
		 * And put on 'vars' the array with all the vars
		 */
		
		$query = new Query();
		
		$query->select("USERNAME","U.USERNAME")->
		select("ID_USER","U.ID")->
		select("EXPIRES","OUTDATETIME[ST.EXPIRES]")->
		select("RNAME","R.NAME")->
		select("LAST_ACCESS","OUTDATETIME[ST.LAST_ACCESS]")->
		select("FIRST_ACCESS","OUTDATETIME[ST.FIRST_ACCESS]")->
		select("REMOTE_ADDR","ST.REMOTE_ADDR")->
		select("SESSION_TOKEN","ST.SESSION_TOKEN");
	
		$query->filter("USERNAME","u.USERNAME");
	
		
		$query->order("USERNAME","u.USERNAME");
		$query->order("FIRST_ACCESS","ST.FIRST_ACCESS");
		$query->order("LAST_ACCESS","ST.LAST_ACCESS");
		$query->order("EXPIRES","ST.EXPIRES");
		$query->order("RNAME","R.NAME");
		
		$query->from("SESSION_TOKEN ST,USERS U, ROLES R");
		
		$query->where("U.ID = ST.USER_ID");
		$query->where("R.ID = U.ROLE_ID");
		$query->where("(ST.LAST_ACCESS+$environment->login_timeout)")->operator(_OO::LESS)->compareTo("CURRENT_DATE")->union(_OU::OR_)
		->where("CURRENT_DATE")->operator(_OO::LESS)->compareTo("ST.EXPIRES");
		return $query->paging();
	}
	
	public static function updateInterfaceSetup($iid,$data){
		self::$environment = Environment::getInstance();
		if(!self::$environment->authentication_required){
			return true;
		}
		$uid = self::$environment->user->id;
		$rs = self::$environment->dbcon->execute("SELECT CONFIG FROM UI_USER_CONFIG WHERE USER_ID=:USER_ID",array("USER_ID" => $uid));
		$usetup = null; 
		if($rs->fetch()){
			$usetup = json_decode($rs->config,true);
		}
		$rs->close();
		
		if($usetup==null){
			$usetup = array();
			$usetup[$data->namespace] = array();
		}

		if(!isset($usetup[$data->namespace][$data->id])){
			$usetup[$data->namespace][$data->id] = array();
		}
		
		if(!isset($usetup[$data->namespace][$data->id][$data->subspace])){
			$usetup[$data->namespace][$data->id][$data->subspace] = array();
		}
		
		if(!isset($data->values) || (isset($data->values) && $data->values == null)){
			unset($usetup[$data->namespace][$data->id][$data->subspace]);
			if(count($usetup[$data->namespace][$data->id])==0){
				unset($usetup[$data->namespace][$data->id]);
			}
			if(count($usetup[$data->namespace])==0){
				unset($usetup[$data->namespace]);
			}
			if(count($usetup)==0){
				$usetup = "";
			}
		}
		else{
			$usetup[$data->namespace][$data->id][$data->subspace] = $data->values;
		}
		
		$vw["USER_ID"] = $uid;
		$enc = ($usetup==""? "":json_encode($usetup));
		
		$sql = "SELECT * FROM UI_USER_CONFIG WHERE APP_ID=:IID AND USER_ID=:USER_ID";
		$rs = self::$environment->dbcon->execute($sql,array("IID" => $iid,"USER_ID" => $uid));
		if(!$rs->fetch()){
			$var["APP_ID"] = $iid;
			$var["USER_ID"] = $uid;
			self::$environment->dbcon->add("UI_USER_CONFIG",$var);
		}
		$rs->close();
		self::$environment->dbcon->writeLob($enc, "CONFIG", "UI_USER_CONFIG", "USER_ID=:USER_ID",$vw);
		
		
	}
}

/* end of abstract class UserManagement */
?>