<?php

namespace api\crud;

use query\Query;
use core\Environment;

abstract class ModuleManagement {
	protected static $environment;
	public static function getModules($paging = true){
		$query = new Query();
		$query->select("ID","M.ID");
		$query->from("MODULES M");
		if($paging){
			return $query->paging();
		}
		return $query->execute();
	}

	/**
	 * Get all list of module-Actions.
	 */
	public static function getModulesActions(){
		$rsp = array();
		self::$environment = Environment::getInstance();
		$rs = self::$environment->dbcon->execute("SELECT NAME,ID FROM MODULES ORDER BY NAME");
		$modules = $rs->getAll();
		$rs->close();
		foreach($modules as $module){
			$ob = array("id" => $module["id"], "name" => $module["name"],"actions" => null);
			$rs = self::$environment->dbcon->execute("SELECT ID,NAME,DESCRIPTION FROM ACTIONS WHERE MODULE_ID=:MID",array("MID" => $module["id"]));
			$actions = $rs->getAll();
			$rs->close();
			$ob["actions"] =$actions;
			$rsp[] = $ob;
		}
		return $rsp;
	}
	
	/**
	 * Get all list of module-Actions.
	 */
	public static function getModulesActionsByRole($rid){
		$rsp = array();
		self::$environment = Environment::getInstance();
		$rs = self::$environment->dbcon->execute("SELECT NAME,ID FROM MODULES ORDER BY NAME");
		$modules = $rs->getAll();
		$rs->close();
		foreach($modules as $module){
			$ob = array("id" => $module["id"], "name" => $module["name"],"actions" => null);
			$rs = self::$environment->dbcon->execute("SELECT RA.ACTION_ID FROM ROLES_ACTIONS RA,ACTIONS A WHERE 
			RA.ACTION_ID=A.ID AND A.MODULE_ID=:MID AND RA.ROLE_ID=:RID",array("MID" => $module["id"], "RID" => $rid));
			$actions = $rs->getAll();
			$rs->close();
			$ob["actions"] = $actions;
			if(count($actions)!=0){
				$rsp[] = $ob;
			}
		}
		return $rsp;
	}
}
?>