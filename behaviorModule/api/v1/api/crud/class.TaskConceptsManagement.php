<?php

/**
 * Role management class
 *
 * @author phidalgo
 * @namespace api\crud
 */
namespace api\crud;

use core\Environment;
use query\Query;

abstract class TaskConceptsManagement {
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	public static function checkCode($code){
		self::$environment = Environment::getInstance();
		$rs = self::$environment->dbcon->execute("SELECT * FROM TASK_CONCEPTS WHERE CODE=:CODE",array("CODE" => $code));
		$notfound = false;
		if(!$rs->fetch()){
			$notfound = true;
		}
		return $notfound;
	}
	
	public static function _get($paging = true,$defaultReturn = null){
		$env = Environment::getInstance();
		$query = new Query();
		$query->defaultReturn($defaultReturn); //Set default return if any.
		
		$query->select("TC.ID,TC.CODE");
		$query->select("LABEL","TCL.TEXT");
		$query->from("TASK_CONCEPTS TC,TASK_CONCEPTS_LANG TCL");
		$query->where("TCL.LANGUAGE")->compareTo("'".$env->user->ui_language."'");
		$query->where("TC.ID=TCL.TC_ID");
		$query->order("CODE", "TC.CODE");
		
		if($paging){
			return $query->paging();
		}
		else{
			return $query->execute();
		}
	}
	
	public static function _getById($tid){
		if(strtoupper($tid)=='OPTIONS'){
			$f = new TaskConcept();
		}
		else{
			$f = new TaskConcept($tid);
		}
		return $f->get();
	}
	
	public static function _add($data){
		$t = new TaskConcept();
		return $t->set($data)->save();
	}
	
	public static function _update($tid,$data){
		$t = new TaskConcept($tid);
		return $t->set($data)->save();
	}
	public static function _delete($tid){
		$t = new TaskConcept($tid);
		return $t->delete();
	}
	
}
/* end of abstract class ProjectsManagement */
?>