<?php

/**
 * Role management class
 *
 * @author phidalgo & ctemporal
 * @namespace core\security
 */
namespace api\crud;

use core\Environment;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;
use query\Query;

abstract class RoleManagement {
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	/**
	 * Get all roles.
	 * Returns array of Roles
	 *
	 * @access public
	 * @return array Query
	 * @throws core\Exception\AppException
	 */
	public static function getRoles($paging = true,$defaultReturn = null) {
		$query = new Query();
		$query->defaultReturn($defaultReturn); //Set default return if any.
		
		$query->select("R.ID");
		$query->select("R.NAME");
		$query->select("USERS","(SELECT COUNT(1) FROM USERS WHERE ROLE_ID=R.ID)");
	
		$query->filter("NAME","NAME");
		$query->order("NAME","R.NAME");
		$query->order("USERS","USERS");
		$query->from("ROLES R");
		if($paging){
			return $query->paging();
		}
		else{
			return $query->execute();
		}
	}
	
	/**
	 * Adds a new Role.
	 *
	 * @param
	 *        	api\crud\Role
	 * @return s bool
	 * @throws core\Exception\AppException
	 */
	public static function addRole($data) {
		$role = new Role();
		$role->name = $data->name;
		self::$environment = Environment::getInstance();
		$rs = self::$environment->dbcon->execute ( "SELECT IFNULL[max(ID)+1,0] AS NEWID FROM ROLES" );
		if(!$rs->fetch()){
			throw new AppException (Status::S5_InternalServerError, ErrorCode::DBQueryError);
		}
		$role->id = $rs->newid;
		$rs->close();
		$var ["ID"] = $role->id;
		$var ["NAME"] = $role->name;
		self::$environment->dbcon->add('ROLES', $var );
		if(isset($data->actions)){
			self::addActions($role->id, $data->actions);
		}
		return array("id" => $role->id);
	}
	
	public static function addActions($rid, $actions_id) {
		$environment = Environment::getInstance ();
		foreach ( $actions_id as $action ){
			$var ["ID"] = $rid;
			$var ["ACTION_ID"] = $action;
			$var["MODULE_ID"] = 
			$environment->dbcon->add("ROLES_ACTIONS",$var);
		}
	}
	public static function updateRole($rid, $data) {
		$environment = Environment::getInstance ();
		//Delete all previous role actions relation.
		$environment->dbcon->execute("DELETE FROM ROLES_ACTIONS WHERE ROLE_ID=:RID",array("RID" => $rid));

		//Update Role name.
		$var["name"] = $data->name;
		$vw["RID"] = $rid;
		$environment->dbcon->update("ROLES",$var,"ID=:RID",$vw);
		unset($var);unset($vw);
		
		foreach ($data->modules as $module){
			foreach($module->actions as $action){
				if($action->allowed){
					$var["ROLE_ID"] = $rid;
					$var["ACTION_ID"] = $action->id;
					$var["MODULE_ID"] = $module->id;
					$environment->dbcon->add("ROLES_ACTIONS",$var);
					unset($var);
				}
			}
		}
	}
	
	/**
	 * Deletes role by id.
	 *
	 * @param
	 *        	Integer
	 * @throws core\Exception\AppException
	 */
	public static function deleteRole($id) {
		$environment = Environment::getInstance ();
		$sql = "SELECT count(1) AS USERS FROM USERS WHERE ID=:ID";
		$rs = $environment->dbcon->execute($sql,array("ID" => $id));
		
		$rs->fetch (); // Always true
		$users = $rs->getVal ( "users" );
		$rs->close ();
		if ($users > 0) {
			throw new AppException ( Status::S4_NotAcceptable, ErrorCode::CanNotDelete, "@LITERAL: Can not delete because there is $users users with this ROLE" );
		}
		
		$environment->dbcon->execute ( "DELETE FROM ROLES_ACTIONS WHERE ROLE_ID=:ID",array("ID" => $id));
		$environment->dbcon->execute ( "DELETE FROM ROLES WHERE ID=:ID",array("ID" => $id));
	}
	
	/**
	 * Get role by id
	 *
	 * @param
	 *        	Integer id
	 * @return \api\crud\Role
	 * @throws core\Exception\AppException
	 */
	public static function getRolebyId($id = null) {
		$role = new Role($id,true);
		
		return (array) $role;
	}
} /* end of abstract class RoleManagement */

?>