<?php
namespace api\crud;

use query\Query;
use core\Environment;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;

/**
 * Class AttributesManagement
 *
 * @author phidalgo
 * @namespace api\crud
 */
abstract class AttributesManagement {

	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	/**
	 * Adds or updates an attribute.
	 * 
	 * @param integer $id
	 * @param \stdClass $data
	 * @throws AppException
	 */
	public static function attribute($id = null,$data){
		$environment = Environment::getInstance ();
		$new = false;
		if($id == null){
			$new = true;
			$sql = "SELECT IFNULL[max(id)+1,0] as ID FROM ATTRIBUTES";
			$rs = $environment->dbcon->execute($sql);
			if(!$rs->fetch()){
				throw new AppException(Status::S5_InternalServerError,ErrorCode::DBQueryError,"@LITERAL Error: Cannot get new id for a new attribute");
			}
			$id = $rs->id;
			$rs->close();

			$vars["NAME"] = $data->name;
			$vars["TYPE"] = $data->type;
			$vars["ID"] = $id;
			if($environment->authentication_required){
				$vars["DEPARTMENT_ID"] = $environment->user->user_department;
			}
			$environment->dbcon->add("ATTRIBUTES",$vars);
		}
		else{
			$environment->dbcon->execute("DELETE FROM ATTRIBUTES_LANG WHERE ATTR_ID=:ID",array("ID" => $id));
		}
		
		foreach($data->labels as $label){
			$var["ATTR_ID"] = $id;
			$var["LANGUAGE"] = strtoupper($label->lang);
			$var["VALUE"] = $label->value;
			$environment->dbcon->add("ATTRIBUTES_LANG",$var);
			unset($var);
		}
		
		foreach($data->tables as $table){
			if($table->add){
				self::addAttributeRel($table->id,$id);
			}
			else{
				$environment->dbcon->execute("DELETE FROM ATTRIBUTES_REL WHERE ATTR_ID=:ATTR_ID AND TABLE_ID=:TID",array("ATTR_ID" => $id,"TID" => $table->id));
			}
		}
		if($new){
			return array("ID" => $id);
		}
	}
	
	/**
	 * Get the available options to create a new attribute.
	 */
	public static function getAttributeOptions(){
		$environment = Environment::getInstance();
		$rsp = array();
		$rsp["options"]["tables"] = $environment->tables->getTables();
		return $rsp;
	}
	
	/**
	 * Adds a new relation between table and attribute.
	 * 
	 * @param integer $table_id
	 * @param integer $attr
	 */
	public static function addAttributeRel($table_id,$attr){
		$env = Environment::getInstance ();
		$sql = "SELECT * FROM ATTRIBUTES_REL WHERE TABLE_ID=:TID and ATTR_ID=:ATTR_ID";
		$rs = $env->dbcon->execute($sql,array("TID" => $table_id,"ATTR_ID" => $attr));
		if(!$rs->fetch()){
			$sql = "SELECT IFNULL[max(order_p)+1,1] as order_p FROM ATTRIBUTES_REL WHERE TABLE_ID=:TID";
			$rs2 = $env->dbcon->execute($sql,array("TID" => $table_id));
			$rs2->fetch();
			$order_p = $rs2->order_p;
			$arr = array(
					"ATTR_ID" => $attr,
					"TABLE_ID" => $table_id,
					"ORDER_P" => $order_p
			);
			$env->dbcon->add("ATTRIBUTES_REL",$arr);
			$order_p++;
		}
		$rs->close();
		return true;
	}
	
	/**
	 * Deletes a relation between attribute and table.
	 * 
	 * Can delete attributes array or delete all minus attributes array, using the third function argument.
	 * 
	 * @param integer $table_id
	 * @param array $attributes
	 * @param boolean $in
	 */
	public static function deleteAttributeRel($table_id,$attributes,$in = true){
		$env = Environment::getInstance();
		$attributes = (array) $attributes;
		$ts = implode(",", $attributes);
		$sql = "DELETE FROM ATTRIBUTES_REL WHERE ".(count($attributes)>0? 'ID_ATTR '.($in? '':'NOT').' IN ('.$ts.') AND':'')." TABLE_ID=:ID";
		$vars = array("ID" => $table_id);
		$env->dbcon->execute($sql,$vars);
	}
	
	/**
	 * Deletes an attribute.
	 * @param integer $id
	 * @throws AppException
	 */
	public static function deleteAttribute($id){
		$environment = Environment::getInstance ();
		$environment->dbcon->execute("DELETE FROM ATTRIBUTES WHERE ID=:ID",array("ID" => $id));
	}
	
	/**
	 * Get attributes.
	 */
	public static function getAttributes(){
		$query = new Query();
		$query->select("ID", "ID");
		$query->select("NAME", "NAME");
		$query->select("TYPE", "TYPE");
		$query->order("NAME","NAME");
		$query->order("TYPE","TYPE");
		$query->from("ATTRIBUTES");
		return $query->paging();
	}
	
	/**
	 * Get attribute by id. Throws an exception if no attribute found.
	 * @param integer $id
	 * @throws AppException
	 */
	public static function getAttribute($id){
		$environment = Environment::getInstance ();
		$sql = "SELECT * FROM ATTRIBUTES WHERE ID=:ID";
		$rs = $environment->dbcon->execute($sql,array("ID" => $id));
		if(!$rs->fetch()){
			throw new AppException(Status::S4_NotFound,ErrorCode::ResourceNotFound,"@Literal ERROR -> Attribute not found");
		}
		$rsp["id"] = $rs->id;
		$rsp["name"] = $rs->name;
		$rsp["type"] = $rs->type;
		$rs->close();
		
		$sql = "SELECT * FROM ATTRIBUTES_REL WHERE ATTR_ID=:ID";
		$rs = $environment->dbcon->execute($sql,array("ID" => $id));
		$f = $rs->getAll();
		$rs->close();
		$rsp["assignee"] = array();
		foreach($f as $rel){
			$rsp["assignee"][] = $rel["table_id"];
		}
	
		$rsp["langs"] = array();
		$sql = "SELECT * FROM ATTRIBUTES_LANG WHERE ATTR_ID=:ATTR_ID";
		$rs = $environment->dbcon->execute($sql,array("ATTR_ID" => $id));
		while($rs->fetch()){
			$lang = array();
			$lang["attr_id"] = $rs->attr_id;
			$lang["lang"] = $rs->language;
			$lang["value"] = $rs->value;
			array_push($rsp["langs"],$lang);
		}
		$rs->close();
		
		$rsp["options"]["tables"] = $environment->tables->_get();
		
		return $rsp;
	}


	/**
	 * Sets attribute value
	 * @param integer $attr_id attribute id
	 * @param string $attr_val attribute value
	 * @param string $resource_id resource_id
	 * @param integer $table_id table_id
	 */
	public static function setAttributeValue($attr_id,$attr_val,$resource_id,$table_id){
		self::$environment = Environment::getInstance();
		$attr_val = trim($attr_val);
		if($attr_val!=""){
			$rs = self::$environment->dbcon->execute("SELECT * FROM ATTRIBUTES_VALUES WHERE TABLE_ID=:TID AND ATTR_ID=:AID AND ELEMENT_ID=:ELEMENT_ID",array("ELEMENT_ID" => $resource_id,"AID" => $attr_id,"TID" => $table_id));
			if($rs->fetch()){
				$vw["EID"] = $resource_id;
				$vw["ATTR_ID"] = $attr_id;
				$vw["IDT"] = $table_id;
				$vars["VALUE"] = $attr_val;
				self::$environment->dbcon->update("ATTRIBUTES_VALUES",$vars,"ELEMENT_ID=:EID AND ATTR_ID=:ATTR_ID AND TABLE_ID=:IDT",$vw);
			}
			else{
				$vars["ELEMENT_ID"] = $resource_id;
				$vars["ATTR_ID"] = $attr_id;
				$vars["VALUE"] = $attr_val;
				$vars["TABLE_ID"] = $table_id;
				self::$environment->dbcon->add("ATTRIBUTES_VALUES",$vars);
			}
			$rs->close();
		}
		else{ //If DynAttribute is null or empty, we will delete the entry on the DB.
			$vars["ATTR_ID"] = $attr_id;
			$vars["TABLE_ID"] = $table_id;
			$vars["EID"] = $resource_id;
			$sql = "DELETE FROM ATTRIBUTES_VALUES WHERE ATTR_ID=:ATTR_ID AND ELEMENT_ID=:EID AND TABLE_ID=:TABLE_ID";
			self::$environment->dbcon->execute($sql,$vars);
		}
	}
	
	/**
	 * Get dynamic attributes and attributes values for a given table id.
	 * 
	 * If resource_id is null, gives all attributes.
	 * 
	 * @param integer $table_id
	 * @param integer $resource_id
	 * @return array
	 */
	public static function getAttributesValues($table_id,$resource_id = null){
		self::$environment = Environment::getInstance();
		if($resource_id!=null){
			$sql = "SELECT A.NAME,A.TYPE,AR.ATTR_ID,
			(SELECT VALUE FROM ATTRIBUTES_VALUES WHERE TABLE_ID=AR.TABLE_ID AND ATTR_ID=AR.ATTR_ID AND ELEMENT_ID=:ELEMENT_ID) AS VALUE
			FROM ATTRIBUTES_REL AR,ATTRIBUTES A
			WHERE AR.TABLE_ID=:TID AND A.ID=AR.ATTR_ID  ORDER BY AR.ORDER_P";
			$rs = self::$environment->dbcon->execute($sql,array("ELEMENT_ID" => $resource_id,"TID" => $table_id));
		}
		else{
			$sql = "SELECT A.NAME,A.TYPE,AR.ATTR_ID,'' as VALUE
			FROM ATTRIBUTES_REL AR, ATTRIBUTES A
			WHERE AR.TABLE_ID=:TID AND AR.ATTR_ID=A.ID ORDER BY AR.ORDER_P";
			$rs = self::$environment->dbcon->execute($sql,array("TID" => $table_id));
		}
		$tpa = $rs->getAll();
		$rs->close();
		return $tpa;
	}


	/**
	 * Get available attributes for a specific table.
	 * @param unknown $table_id
	 */
	public static function getTableAttributes($table_id,$lang = null){
		self::$environment = Environment::getInstance();
		if($lang==null){
			$lang = self::$environment->user->ui_language;
		}
		$sql = "SELECT AR.ATTR_ID,AL.VALUE AS LABEL FROM ATTRIBUTES_REL AR,ATTRIBUTES_LANG AL WHERE upper(AL.LANGUAGE)=upper(:LANG) 
		AND AR.ATTR_ID=AL.ATTR_ID AND TABLE_ID=:ID ORDER BY AR.ORDER_P";
		$rs = self::$environment->dbcon->execute($sql,array("ID" => $table_id,"LANG" => $lang));
		$attr = $rs->getAll();
		$rs->close();
		return $attr;
	}
}