<?php

/**
 * Role management class
 *
 * @author phidalgo & ctemporal
 * @namespace core\security
 */
namespace api\crud;

use core\Environment;
use core\Exception\AppException;
use query\Query;

abstract class TeamsManagement {
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;

	/**
	 * Get all projects.
	 *
	 * @access public
	 * @return array Query
	 * @throws core\Exception\AppException
	 */
	public static function _get($paging = true,$defaultReturn = null) {
		$query = new Query();
		$query->defaultReturn($defaultReturn); //Set default return if any.
		
		$query->select("T.ID,T.NAME,CREATION_TS AS CREATED");
			
		$query->filter("NAME","T.NAME");

		$query->from("WORK_TEAMS T");
		$query->order("NAME", "T.NAME");
		$query->where("T.USER_ID")->compareTo(Environment::getInstance()->user->id);
		
		if($paging){
			return $query->paging();
		}
		else{
			return $query->execute();
		}
	}
	
	/**
	 * Get Team options.
	 */
	public static function getOptions(){
		return new Team();
	}
	
	/**
	 * Get Team by id
	 * @param integer $pid
	 */
	public static function _getById($tid){
		return new Team($tid);
	}
	
	/**
	 * Get all teams (and resources) by user
	 */
	public static function _getMyTeams(){
		self::$environment = Environment::getInstance();
		$sql = "SELECT ID FROM WORK_TEAMS WHERE USER_ID=:USER_ID";
		$rs = self::$environment->dbcon->execute($sql, array("USER_ID" => self::$environment->user->id));
		$rsp = array();
		while($rs->fetch()){
			$t = new Team($rs->id);
			$rsp[] = $t->get();
		}
		$rs->close();
		
		return $rsp;
	}
	
	/**
	 * Add new team
	 * @param \stdClass $data
	 */
	public static function _new($data){
		$t = new Team();
		return $t->set($data)->save();
	}
	
	/**
	 * Update team
	 * @param integer $pid
	 * @param \stdClass $data
	 */
	public static function _update($tid,$data){
		$t = new Team($tid);
		return $t->set($data)->save();
	}
	
	public static function _delete($tid){
		$t = new Team($tid);
		return $t->delete();
	}
	
}
/* end of abstract class TeamsManagement */
?>