<?php

/**
 * Role management class
 *
 * @author phidalgo
 * @namespace api\crud
 */
namespace api\crud;

use core\Environment;
use query\Query;

abstract class TaskTypesManagement {
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	public static function _get($paging = true,$defaultReturn = null){
		$query = new Query();
		$query->defaultReturn($defaultReturn); //Set default return if any.
		
		$query->select("T.ID,T.AUTO_IMPUTATION,T.FULL_GRID,T.RESOURCE_MANDATORY,T.NAME");
		$query->from("TASK_TYPE T");
		$query->order("NAME", "T.NAME");
		
		if($paging){
			return $query->paging();
		}
		else{
			return $query->execute();
		}
	}
	
	public static function _getById($tid){
		if(strtoupper($tid)=='OPTIONS'){
			$t = new TaskType();
		}
		else{
			$t = new TaskType($tid);
		}
		return $t->get();
	}
	
	public static function _add($data){
		$t = new TaskType();
		return $t->set($data)->save();
	}
	
	public static function _update($tid,$data){
		$t = new TaskType($tid);
		return $t->set($data)->save();
	}
	public static function _delete($tid){
		$t = new TaskType($tid);
		return $t->delete();
	}
	
}
/* end of abstract class ProjectsManagement */
?>