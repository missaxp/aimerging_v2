<?php
namespace api\crud;

use core\Environment;
use dbconn\dbConn;
use core\config\config;
use query\Query;

/**
 * This class has static methods whichs are Environment non-dependent.
 *
 * This is like this because if anything goes wrong on a core class, we will also catch the error.
 *
 * Saves to the DB the current error. It might be an API error or Javascript error from the front end.
 *
 * @author phidalgo
 *        
 */
abstract class ErrorManagement {
	protected static $config;
	
	public static function getErrors(){
		$query = new Query();
		
		$query->select("CODE", "EL.CODE");
		$query->select("TYPE", "EL.TYPE");
		$query->select("MESSAGE", "EL.MESSAGE");
		$query->select("CUSTOM_MESSAGE", "EL.CUSTOM_MESSAGE");
		$query->select("USERNAME", "U.USERNAME");
		$query->select("DATETIME", "OUTDATETIME[EL.DATETIME]");
		$query->select("IP", "EL.REMOTE_ADDR");
		$query->select("COUNTER", "EL.COUNTER");
		$query->select("FILEE", "EL.FILE_ERROR");
		$query->select("LINE", "EL.LINE_ERROR");
		$query->select("URL", "EL.REST_URL");
		
		/**
		 * This is the ALLOWED FIELDS of the WHERE Clause.
		*/
		
		$query->filter("CODE", 'EL.CODE');
		$query->filter("TYPE",'EL.TYPE');
		$query->filter("USERNAME",'EL.USER_ID',UserManagement::getUsers(false,array("ID","USERNAME")));
		$query->filter("DATETIME",'EL.DATETIME');
		$query->filter("FILEE",'EL.FILE_ERROR');
		$query->filter("URL",'EL.REST_URL');
		
		/**
		 * This is the ALLOWED FIELDS of the ORDER BY Clause
		*/
		$query->order("CODE",'EL.CODE');
		$query->order("TYPE",'EL.TYPE');
		$query->order("USERID",'EL.USER_ID');
		$query->order("COUNTER",'EL.COUNTER');
		$query->order("FILEE",'EL.FILE_ERROR');
		$query->order("URL",'EL.REST_URL');
		$query->order("DATETIME", "EL.DATETIME");
		$query->order("USERNAME","U.USERNAME");
		
		
		$query->from("ERROR_LOG EL,USERS U");
		$query->where("EL.USER_ID=U.ID(+)");
		return $query->paging();
	}
	
	public static function register($data) {
		require_once (BaseDir . "/dataAccess/dbConn.php");
		self::$config = config::_GET ();
		
		/*try {
			$dbConn = new \dataAccess\dbConn( DBCONN_ORACLE, self::$config->DBuser, self::$config->DBpass, self::$config->conn_string );
		} catch ( \Exception $e ) {
			// TODO ultima sol·lució -> enviar email.
			return false;
		}
	
		$e = $data;
		$vars["CODE"] = $e->code;
		$vars["MESSAGE"] = $e->message;
		$vars["CUSTOM_MESSAGE"] = $e->customMessage;
		$vars["FILE_ERROR"] = (isset($e->file)? str_replace($_SERVER["DOCUMENT_ROOT"],'',$e->file):"");
		$vars["LINE_ERROR"] = $e->line;
		$vars["COLUMN_ERROR"] = $e->column;
		$vars["REMOTE_ADDR"] = isset ( $_SERVER ["REMOTE_ADDR"] ) ? $_SERVER ["REMOTE_ADDR"] : '';
		$vars["COUNTER"] = 0;
		$vars["NAVIGATOR"] = $e->navigator;
		$vars["TYPE"] = $e->type;
		if(isset($e->userid)){
			$vars["USER_ID"] = $e->userid;
		}
		if (isset ( $e->php_trace )) {
			$vars["PHP_TRACE"] = $e->php_trace;
		}
		if (isset ( $e->rest_url )) {
			$vars["REST_URL"] = $e->rest_url;
		}
		
		$v1 ["CODE"] = $vars ["CODE"];
		$v1 ["FILE_ERROR"] = $vars ["FILE_ERROR"];
		$v1 ["LINE_ERROR"] = $vars ["LINE_ERROR"];
		$v1 ["COLUMN_ERROR"] = $vars ["COLUMN_ERROR"];
		$v1 ["MESSAGE"] = $vars["MESSAGE"];
		$v1["TYPE"] = $vars["TYPE"];
		
		try{
			$sql = "SELECT COUNTER FROM ERROR_LOG WHERE CODE=:CODE AND FILE_ERROR=:FILE_ERROR AND LINE_ERROR=:LINE_ERROR AND COLUMN_ERROR=:COLUMN_ERROR AND (current_date-datetime<1) AND TYPE=:TYPE AND MESSAGE=:MESSAGE";
			$rs = $dbConn->execute ($sql, $v1);
			if ($rs->fetch ()) {
				$c = ( int ) $rs->getVal ( "COUNTER" );
				$v ["COUNTER"] = $c + 1;
				$varsWhere ["CODE"] = ( int ) $vars ["CODE"];
				$varsWhere ["LINE_ERROR"] = ( int ) $vars ["LINE_ERROR"];
				$varsWhere ["FILE_ERROR"] = $vars ["FILE_ERROR"];
				$varsWhere ["COLUMN_ERROR"] = ( int ) $vars ["COLUMN_ERROR"];
				$varsWhere["TYPE"] = $vars["TYPE"];
				$varsWhere["MESSAGE"] = $vars["MESSAGE"];
				$dbConn->update("ERROR_LOG", $v, "CODE=:CODE AND FILE_ERROR=:FILE_ERROR AND LINE_ERROR=:LINE_ERROR AND COLUMN_ERROR=:COLUMN_ERROR AND (sysdate-datetime<1) AND TYPE=:TYPE AND MESSAGE=:MESSAGE", $varsWhere );
			} else {
				$dbConn->add("ERROR_LOG", $vars );
			}
			$rs->close ();
		}
		catch(\Exception $e){
			//TODO -> enviar email.
		}
		
		
		unset ( $vars );
		unset ( $varsWhere );
		unset ( $v );
		unset ( $v1 );
		*/
		
		
		return true;
	}
}
?>