<?php

/**
 * Role management class
 *
 * @author phidalgo & ctemporal
 * @namespace core\security
 */
namespace api\crud;

use core\Environment;
use core\Exception\AppException;
use query\Query;

abstract class GroupManagement {
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	/**
	 * Get all groups.
	 *
	 * @access public
	 * @return array Query
	 * @throws core\Exception\AppException
	 */
	public static function _get($paging = true,$defaultReturn = null) {
		$query = new Query();
		$query->defaultReturn($defaultReturn); //Set default return if any.
		
		$query->select("G.ID");
		$query->select("G.NAME");
		$query->select("USERS","(SELECT count(1) FROM USERS WHERE USER_GROUP=G.ID)");
		$query->select("CREATED","G.CREATION_TS");
			
		$query->filter("NAME","G.NAME");
		$query->from("P_GROUPS G");
		$query->order("NAME", "G.NAME");
		
		if($paging){
			return $query->paging();
		}
		else{
			return $query->execute();
		}
	}
	
	public static function _getById($gid){
		$group = new Group($gid);
		return $group->get();
		
	}
	
	/**
	 * Adds a new Group.
	 *
	 * @param \stdClass $data
	 * @returns bool
	 * @throws core\Exception\AppException
	 */
	public static function _add($data) {
		$group = new group(null);
		return $group->save($data);
	}
	
	public static function _update($gid, $data) {
		$group = new Group($gid);
		return $group->save($data);
	}
	
	/**
	 * Deletes groups by id.
	 *
	 * @param Integer
	 * @throws core\Exception\AppException
	 */
	public static function _delete($gids) {
		return group::deleteGroup($gids);
	}
}
/* end of abstract class GroupsManagement */
?>