<?php
namespace api\crud;

use query\Query;
use core\Environment;
use query\_OO;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;

/**
 * Class NatureManagement
 *
 * @author phidalgo
 * @namespace api\crud
 */
abstract class NatureManagement {

	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
		
	public static function getNatures(){
		$query = new Query();
	
		$query->select("ID","N.ID")
		->select("NAME","N.NAME");		
		$query->from("NATURE N");
		$query->filter("NAME","N.NAME")->operator(_OO::LIKE);
		$query->filter("ID","N.ID");
		$query->order("NAME","N.NAME");
	
		return $query->paging();
	}
	
	public static function addNature($id = null, $data){
		self::$environment = Environment::getInstance();
	
		$rsp = array();		
		$rsp["FormError"] = false;
		$rsp["ErrorMsg"] = "";
			
			
		$vars = array();		
		foreach($data as $prop => $value){
			$vars[$prop] = $value;
		
			$lenValue = strlen($value);
		
			if ($prop == "id") $maxLength = 4;
			if ($prop == "name") $maxLength = 50;
					
			if ($lenValue > $maxLength) throw new AppException(Status::S4_PreconditionFailed,ErrorCode::LengthError,"Field $prop maximum length error. Max : $maxLength. Length :  $lenValue.");
		
		}
	
		if (is_null ($id)) {
										
			$rs = self::$environment->dbcon->execute("SELECT * FROM NATURE WHERE ID=:CID",array("CID" => $data->id));
			$fetch = $rs->getAll();
			$rs->close();					
			if(count($fetch)==0){			
				$rsp["id"] = $data->id;
				$vars = array("ID" => $data->id, "NAME" => $data->name);
				self::$environment->dbcon->add("NATURE", $vars);							
			} else {
				$cCode = $fetch[0]['ID'];
				$cName = $fetch[0]['NAME'];				
				throw new AppException(Status::S4_PreconditionFailed, ErrorCode::NotAcceptable, "Nature  $cName ( $cCode ) exist");								
			}			
			
		} else {
			$rsp["id"] = $id;
			$varsW["CID"] = $id;
			self::$environment->dbcon->update("NATURE", $vars,"ID=:CID",$varsW);
		}	
				
		return $rsp;			
	}
		
		public static function natureNotExist($id){
			self::$environment = Environment::getInstance();
			
			$rs = self::$environment->dbcon->execute("SELECT * FROM NATURE WHERE ID=:CID",array("CID" => $id));
			$fetch = $rs->getAll();
			$rs->close();
			if(count($fetch)==0){
				return true;
			} 	
			return false;		
		}
			
		public static function getNatureById($id){
			self::$environment = Environment::getInstance();
			
			$rs = self::$environment->dbcon->execute("SELECT * FROM NATURE WHERE ID=:CID",array("CID" => $id));
			$fetch = $rs->getAll();
			$rs->close();
			if(count($fetch)==0){
				throw new AppException(Status::S4_NotFound,ErrorCode::NotFound,"Nature Resource $id not found");
			}
			$fetch = $fetch[0];
			return $fetch;
			
		}
		
		
		public static function deleteNatureById($id){
			$environment = Environment::getInstance ();
			
			$num = 0;
			$sql = "SELECT count(1) AS NUM FROM THIRD_PARTY WHERE NATURE_ID=:ID";
			$rs = $environment->dbcon->execute ( $sql, array (
					"ID" => $id 
			) );
			if ($rs->fetch ()) {
				$num = $rs->getVal ("NUM");
			}
			$rs->close ();
			
			if ($num > 0)
				throw new AppException ( Status::S4_PreconditionFailed, ErrorCode::CanNotDelete, "@LITERAL: Error there are $num thirtdpary in this id ($id) nature. Can not delete it." );
			
			try {
				$afectedRows = $environment->dbcon->execute ( "DELETE FROM NATURE WHERE ID=:CID", array (
						"CID" => $id 
				) );
			} catch ( \Exception $e ) {
				throw new AppException ( Status::S5_InternalServerError, ErrorCode::DBQueryError, $e->getMessage () );
			}
			
			if ($afectedRows === true)
				throw new AppException ( Status::S4_NotFound, ErrorCode::CanNotDelete, "@LITERAL: Can not delete because this nature does not exists." );
		
		}
		
		public static function deleteNatures($data){
			$environment = Environment::getInstance ();
						
			$totalItem = 0;
			$itemsDeleted = 0;
			$itemsNotDeleted = 0;
					
		
			foreach($data as $prop => $values){
				
				if ($prop == "arrayIds") {
					foreach($values as $value){					
						$num = 0;
						$totalItem++;
						$sql = "SELECT count(1) AS NUM FROM THIRD_PARTY WHERE NATURE_ID=:CID";
						$rs = $environment->dbcon->execute ( $sql, array (
								"CID" => $value
						) );
						if ($rs->fetch ()) {
							$num = $rs->getVal ("NUM");
						}
						$rs->close ();
	
						if ($num > 0) {
							// No es pot borrar						
							$itemsNotDeleted++;
							$rsp["NoDeletedItemsCode"][] = $value;
							
						} else {
							// Borrar													
							$afectedRows = $environment->dbcon->execute ( "DELETE FROM NATURE WHERE ID=:CID", array (
										"CID" => $value
							) );							
							$itemsDeleted++;
							$rsp["DeletedItemsCode"][] = $value;						
						}															
					}				
				}			
			}
			$rsp["TotalItems"] = $totalItem;		
			$rsp["DeletedItems"] = $itemsDeleted;
			$rsp["NoDeletedItems"] = $itemsNotDeleted;
			
			return $rsp;
			
		}
		
}