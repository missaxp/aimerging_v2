<?php
namespace api\crud;

use query\Query;
use core\Environment;
use query\_OO;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;

/**
 * Class CountryManagement
 *
 * @author phidalgo
 * @namespace api\crud
 */
abstract class CountryManagement {

	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	/**
	 * Accept boolean $paging to return paginated response or not.
	 * @param boolean $paging
	 */		
	public static function getCountries($paging = true){
		$query = new Query();
		$query->select("ID","C.ID")
		->select("NAME","C.NAME");		
		$query->from("COUNTRY C");
		$query->filter("NAME","C.NAME")->operator(_OO::LIKE);
		$query->filter("ID","C.ID");		
		$query->order("NAME","C.NAME");
		if($paging){
			return $query->paging();
		}
		else{
			return $query->execute();
		}
	}
	
	public static function addCountry($id = null, $data){
		self::$environment = Environment::getInstance();

		$rsp = array();		
		$rsp["FormError"] = false;
		$rsp["ErrorMsg"] = "";
		
		
		$vars = array();		
		foreach($data as $prop => $value){
			$vars[$prop] = $value;
		
			$lenValue = strlen($value);
		
			if ($prop == "id") $maxLength = 2;
			if ($prop == "name") $maxLength = 50;
						
			if ($lenValue > $maxLength) throw new AppException(Status::S4_PreconditionFailed,ErrorCode::LengthError,"Field $prop maximum length error. Max : $maxLength. Length :  $lenValue.");
		
		}
	
		if (is_null ($id)) {
										
			$rs = self::$environment->dbcon->execute("SELECT * FROM COUNTRY WHERE ID=:CID",array("CID" => $data->id));
			$fetch = $rs->getAll();
			$rs->close();					
			if(count($fetch)==0){			
				$rsp["id"] = $data->id;
				$vars = array("ID" => $data->id, "NAME" => $data->name);
				self::$environment->dbcon->add("COUNTRY", $vars);							
			} else {
				$cCode = $fetch[0]['ID'];
				$cName = $fetch[0]['NAME'];				
				throw new AppException(Status::S4_PreconditionFailed, ErrorCode::NotAcceptable, "Country  $cName ( $cCode ) exist");								
			}			
			
		} else {
			$rsp["id"] = $id;
			$varsW["CID"] = $id;
			self::$environment->dbcon->update("COUNTRY", $vars,"ID=:CID",$varsW);
		}	
				
		return $rsp;			
	}
	
	public static function countryNotExist($id){
		self::$environment = Environment::getInstance();
		
				
		$rs = self::$environment->dbcon->execute("SELECT * FROM COUNTRY WHERE ID=:CID",array("CID" => $id));
		$fetch = $rs->getAll();
		$rs->close();
		if(count($fetch)==0){
			return true;
		} 	
		return false;		
	}
		
	public static function getCountryById($id){
		self::$environment = Environment::getInstance();
		
		$rs = self::$environment->dbcon->execute("SELECT * FROM COUNTRY WHERE ID=:CID",array("CID" => $id));
		$fetch = $rs->getAll();
		$rs->close();
		if(count($fetch)==0){
			throw new AppException(Status::S4_NotFound,ErrorCode::NotFound,"Country Resource $id not found");
		}
		$fetch = $fetch[0];
		return $fetch;
		
	}
	
	
	public static function deleteCountryById($id){
		$environment = Environment::getInstance ();
		$afectedRows = $environment->dbcon->execute ( "DELETE FROM COUNTRY WHERE ID=:CID", array ("CID" => $id));
	}
	
	public static function deleteCountries($data){
		$environment = Environment::getInstance ();
					
		$totalItem = 0;
		$itemsDeleted = 0;
		$itemsNotDeleted = 0;
				
	
		foreach($data as $prop => $values){
			
			if ($prop == "arrayIds") {
				foreach($values as $value){					
					$num = 0;
					$totalItem++;
					$sql = "SELECT count(1) AS NUM FROM THIRD_PARTY WHERE COUNTRY_ID=:CID";
					$rs = $environment->dbcon->execute ( $sql, array (
							"CID" => $value
					) );
					if ($rs->fetch ()) {
						$num = $rs->getVal ("NUM");
					}
					$rs->close ();

					if ($num > 0) {
						// No es pot borrar						
						$itemsNotDeleted++;
						$rsp["NoDeletedItemsCode"][] = $value;
						
					} else {
						// Borrar												
						$afectedRows = $environment->dbcon->execute ( "DELETE FROM COUNTRY WHERE ID=:CID", array (
									"CID" => $value
						) );						
						$itemsDeleted++;
						$rsp["DeletedItemsCode"][] = $value;						
					}															
				}				
			}			
		}
		$rsp["TotalItems"] = $totalItem;		
		$rsp["DeletedItems"] = $itemsDeleted;
		$rsp["NoDeletedItems"] = $itemsNotDeleted;
		
		return $rsp;
		
	}
		
}