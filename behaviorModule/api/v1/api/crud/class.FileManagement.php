<?php
namespace api\crud;

use query\Query;
use core\Environment;
/**
 * Class FileManagement
 *
 * @author phidalgo
 * @namespace api\crud
 */

abstract class FileManagement {

	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	/**
	 * Get file list for a specific id_rel
	 * @param string  $table_id
	 * @param string $id_rel
	 */
	public static function getFiles($id_rel,$table_id){
		$query = new Query();
		$query->
		select("MF.ID")->
		select("NAME","MF.NORMALIZED_NAME")->
		select("MF.CREATION_TS")->
		select("MF.TYPE")->
		select("MF.FILE_SIZE")->
		select("MF.COMMENTS");
		$query->from("MASTER_FILE MF");
		$query->where("MF.TABLE_ID='$table_id'");
		$query->where("MF.ID_REL='$id_rel'");
		$query->where("MF.UPLOAD_FINISH='Y'");
		
		
		
		$response = $query->paging();
		
		foreach ($response["results"] as $key => $file) {
		$file["exists"] = File::checkIfFileExist($file["id"]);
			if(!File::checkIfFileExist($file["id"])){
				//El fitxer existeix en MASTER_FILE pero no en el DISC.
				unset($response["results"][$key]);
			}
		}
		return $response;
	}
	
	/**
	 * Add/Update file comments for a specific file id
	 * @param integer $fid
	 * @param string $comment
	 * @return boolean
	 */
	public static function updateFileComments($fid,$comment){
		$file = new File($fid);
		$file->updateComment($comment);
		return true;
	}
	
	/**
	 * Delete a file by file_id
	 * @param unknown $fid
	 */
	public static function deleteFile($fid){
		$file = new File($fid);
		$file->delete();
	}
	
	/**
	 * Adds new file.
	 * 
	 * When the file is upload, call $callback
	 * 
	 * @param string|null $id_rel
	 * @param string $user_dir
	 * @param string $source
	 * @param \stdClass $data
	 * @param function $callback
	 */
	public static function setBinary($id_rel,$user_dir,$source,$data,$callback = null){
		$file = new File();
		return $file->setBinary($id_rel,$user_dir,$source,$data,$callback);
	}
	
	public static function setAvatar($id_rel,$source,$dbfield,$data){
		$file = new File();
		return $file->setAvatar($id_rel, $source, $dbfield, $data);
		
	}
	
	public static function getAvatar($id_rel,$table_id,$dbfield){
		$file = new File();
		return $file->getAvatar($id_rel,$table_id, $dbfield);
	}
	
	/**
	 * Downloads a file
	 * @param integer $fid
	 */
	public static function getBinary($fid){
		$file = new File($fid);
		return $file->getBinary();
	}
	
	/**
	 * Set file relation given a file id and a relation id.
	 * @param unknown $fid
	 * @param unknown $id_rel
	 */
	public static function setFileRelation($fid,$id_rel){
		$file = new File($fid);
		return $file->setFileRelation($id_rel);
	}
	
	public static function getTemporaryBinary($tid){
		$file = new File($tid,true);
		return $file->getBinary();
	}
	
}