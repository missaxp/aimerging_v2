<?php

namespace api\crud;

use core\Environment;
use query\Query;
use common\Table;



/**
 * Class ContactsManagement
 *
 * @author phidalgo
 * @namespace api\crud
 */
abstract class ContactsManagement {


	/**
	 * Get List of contacts.
	 *
	 * @return Query
	 */
	public static function listContacts(){
		$environment = Environment::getInstance ();
		$query = new Query();
		$query->select("ID", "C.ID");
		$query->select("NAME", "C.NAME");
		$query->select("SURNAME", "C.SURNAME");
		$query->select("TP_NAME", "(SELECT NAME FROM THIRD_PARTY WHERE ID=C.TP_ID)");
		$query->select("WORKPHONE", "C.WORK_PHONE");
		$query->select("CHARGE", "C.CHARGE");
		$query->from("CONTACT C");
		$query->filter("DELIVERY","C.AUTO_DELIVERY_NOTE");
		$query->filter("TAGS","C.ID")->in()->query("SELECT ELEMENT_ID FROM TAGS_RELATION WHERE TABLE_ID=".\common\Table::CONTACTS)->filter("TAG_ID")->in();
		
		//Filter foreach dynamic Attribute.
		$dynAttr = AttributesManagement::getTableAttributes(Table::CONTACTS);
		foreach($dynAttr as $attr){
			$query->filter("DYNATTR_".$attr["attr_id"],"C.ID")->
			in()->
			query("SELECT ELEMENT_ID FROM ATTRIBUTES_VALUES AV WHERE AV.ELEMENT_ID=C.ID AND AV.TABLE_ID=".Table::CONTACTS." AND AV.ATTR_ID=".$attr["attr_id"])->
			filter("AV.VALUE")->like();
		}
		$query->filter("USER","C.USER_ID");
		$query->filter("COUNTRY","C.COUNTRY_ID");
		$query->filter("ID","C.ID");
		$query->filter("NAME","C.NAME");
		$query->filter("WORKPHONE","C.WORK_PHONE");
		$query->filter("REGION","C.REGION");
		$query->filter("PROVINCE","C.PROVINCE");
		$query->order("NAME", "C.NAME");
		return $query->paging();
	}
	
	/**
	 * Get a contact
	 * @param integer $id
	 */
	public static function getContact($id = null){
		return new Contact($id,true);
	}
	
	/**
	 * Update Contact
	 * @param string $id CT_ID
	 * @param \stdClass $data
	 * @return boolean
	 */
	public static function setCT($id,$data){
		$ct = new Contact($id);
		return $ct->set($data)->save();
	}
	
	/**
	 * Delete Contact.
	 * @param string $id CT_ID
	 * @return boolean
	 */
	public static function delCT($id){
		$tp = new Contact($id);
		return $tp->delete();
	}
	
	/**
	 * Get list of emails for this contact.
	 * @param string $id CT_ID
	 * @return Query
	 * @param unknown $id
	 */
	public static function getContactEmails($id){
		$query = new Query();
		$query->
		select("ID","CE.ID")->
		select("EMAIL","CE.EMAIL")->
		select("PRIMARY","CE.PRIMARY")->
		select("OPTED_OUT","CE.OPTED_OUT")->
		select("INVALID","CE.INVALID");
		$query->from("CONTACT_EMAILS CE");
		$query->where("CT_ID='$id'");
		$query->order("PRIMARY","CE.PRIMARY");
		$query->order("EMAIL","CE.EMAIL");
		$query->order("OPTED_OUT","CE.OPTED_OUT");
		$query->order("INVALID","CE.INVALID");
		return $query->paging();
	}
	
	private static function setPrimaryEmail($primary,$ct_id){
		$env = Environment::getInstance();
		//ONLY CAN EXIST ONE PRIMARY EMAIL FOR EACH CONTACT
		//If this new Email is a primary email, update the another ones to set each other email to primary 'N' (false).
		if($primary=="Y"){
			$v = array('primary' => 'N');
			$vvw = array("CT_ID" => $ct_id);
			$env->dbcon->update('CONTACT_EMAILS',$v,"CT_ID=:CT_ID",$vvw);
		}
	}
	
	public static function addContactEmails($id,$data){
		$env = Environment::getInstance();
		$rs = $env->dbcon->execute("SELECT IFNULL[max(id)+1,0] AS NID FROM CONTACT_EMAILS");
		$rs->fetch();
		$nid = $rs->nid;
		$rs->close();
		
		foreach($data as $prop => $value){
			$var[$prop] = $value;
		}
		
		self::setPrimaryEmail($var["primary"], $id);
		
		$var["CT_ID"] = $id;
		$var["ID"] = $nid;
		
		$env->dbcon->add('CONTACT_EMAILS',$var);
	}
	
	public static function updateContactEmails($id,$eid,$data){
		$env = Environment::getInstance();
		foreach($data as $prop => $value){
			$var[$prop] = $value;
		}
		$vw["ID"] = $eid;
		
		self::setPrimaryEmail($var["primary"], $id);
		
		$env->dbcon->update('CONTACT_EMAILS',$var,"ID=:ID",$vw);
	}
	
	public static function deleteContactEmails($id,$eid){
		$env = Environment::getInstance();
		if(is_object($eid)){ //Delete multiple eids
			$in = "";
			foreach($eid->eids as $eid){
				$in .= "'".$eid."',";
			}
			$in = rtrim($in,",");
			$var["CT_ID"] = $id;
			$env->dbcon->execute("DELETE FROM CONTACT_EMAILS WHERE CT_ID=:CT_ID and ID IN ($in)",$var);
		}
		if(is_numeric($eid) || is_string(($eid))){ //Delete one eid
			$var["ID"] = $eid;
			$var["CT_ID"] = $id;
			$env->dbcon->execute("DELETE FROM CONTACT_EMAILS WHERE CT_ID=:CT_ID AND ID=:ID",$var);
		}
	}
	
	/**
	 * Upload contact photo
	 * @param string $id tpid
	 */
	public static function uploadLogo($id = null,$data){
		return FileManagement::setAvatar($id,Table::CONTACTS,'PHOTO',$data);
	}
}