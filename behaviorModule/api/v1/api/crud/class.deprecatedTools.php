<?php

namespace api\crud;

use dbconn\dbConn;
use core\Environment;

abstract class tools {
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	// protected static $base_dir = '/webs/idcpws/L/';
	protected static $base_dir = '/media/L/';
	protected static $backupPhase = 3;
	protected static $backupFolder = '/webs/idcpws/backupFolder/';
	
	// protected static $splitSize = '104857600'; //bytes
	protected static $splitSize = '1887436800'; // bytes (1.8GB)
	protected static $compressionLevel = 0;
	protected static $folders = array (
			"01_JANUARY",
			"02_FEBRUARY",
			"03_MARCH",
			"04_APRIL",
			"05_MAY",
			"06_JUNE",
			"07_JULY",
			"08_AUGUST",
			"09_SEPTEMBER",
			"10_OCTOBER",
			"11_NOVEMBER",
			"12_DECEMBER" 
	);
	
	/**
	 * Get a list of compressable projects.
	 *
	 * @return Ambigous <\api\crud\array:, multitype:>
	 */
	public static function getProjectsToCompress($showAllProjects = false) {
		$projects = self::getProjectsFromFolder ( self::$base_dir );
		$toTar = array ();
		
		foreach ( $projects as &$project ) {
			$project ["tar"] = self::getFoldersToTar ( $project );
			if ($project ["tar"] != null) {
				array_push ( $toTar, $project );
			}
		}
		
		if ($showAllProjects) {
			return $projects;
		} else {
			return $toTar;
		}
	}
	
	/**
	 * Compress projects if any.
	 *
	 * @return number
	 */
	public static function compressProjects() {
		$start_compression = microtime ( true );
		$projects = self::getProjectsToCompress ();
		foreach ( $projects as &$project ) {
			foreach ( $project ["tar"] as &$compressable ) {
				if ($project ["open"] == "Z") { // Si el projecte es tancat... full backup. Ho comprimim tot.
					$tar_file_name = $project ['id'] . "_FULL";
					$project ["full_backup"] = true; // Sets to true to specify that is a full backup.
				} elseif ($project ["open"] == "A") {
					$tar_file_name = $project ["id"] . "_" . $compressable ["folder"]; // Si esta obert, concatenem el zip amb el nom del mes.
				} elseif ($project ["open"] == "N") {
					$tar_file_name = $project ["id"] . "_PROJECT_NOT_FOUND_ON_DGEST"; // Si el projecte no existeix en el DGEST... de moment, el comprimim igual..
					$project ["full_backup"] = true;
				}
				
				$data = self::getDataBackup ( $project, $compressable );
				
				if ($data ["is_tar"] || $data ["dvd_code"] || $data ["exists_backup"]) {
					$project ["actions"] ["manual_delete"] = true;
					$project ["actions"] ["message"] = "Please delete folder: " . $project ["id"] . " from " . self::$base_dir . " manually. Thanks";
					$project ["actions"] ["status"] = $data;
					$project ["tar"] = array ();
					continue;
				}
				$result = self::execute7z ( $compressable ["path"], $tar_file_name, self::$backupFolder );
				$compressable ["command"] = $result ["command"];
				$compressable ["is_tar"] = $result ["success"];
				$compressable ['status'] = $result ["status"];
				$compressable ['tar_files'] = $result ["tar_files"];
				$compressable ['splitted'] = $result ["splitted"];
				$compressable ["size"] = $result ["size"];
				
				self::saveDataToDB ( $project, $compressable );
			}
		}
		$end_compression = microtime ( true );
		
		$start_createDVD = microtime ( true );
		
		$dvds = self::createDVD ( $projects );
		
		$end_createDVD = microtime ( true );
		
		$projects ["dvd"] = $dvds;
		
		$projects ["status"] ["ellapsed_time"] ["compression"] = $end_compression - $start_compression;
		$projects ["status"] ["ellapsed_time"] ["dvd"] = $end_createDVD - $start_createDVD;
		$projects ["status"] ["available_space"] = disk_total_space ( self::$base_dir );
		return $projects;
	}
	
	/**
	 * Get a list of folders to compress for each project.
	 *
	 * @param array $project        	
	 * @return array:
	 */
	protected static function getFoldersToTar(&$project) {
		$folders = array ();
		
		if ($project ['open'] == "N") { // In case of the current folder is not a D-GEST project, do nothing.
			array_push ( $folders, array (
					"is_tar" => false,
					"is_delete" => false,
					"folder" => $project ['id'],
					"size" => 0,
					"status" => null,
					"splitted" => false,
					"command" => null,
					"path" => $project ["unix_path"],
					"tar_files" => null 
			) );
		} else if ($project ['open'] == "Z") { // In case of the current folder is from a closed project, zip and delete all project.
			array_push ( $folders, array (
					"is_tar" => false,
					"is_delete" => false,
					"folder" => $project ['id'],
					"size" => 0,
					"status" => null,
					"splitted" => false,
					"path" => $project ["unix_path"],
					"tar_files" => null 
			) );
		} else if ($project ['open'] == "A") { // Si esta obert... obtenim les carpetes dels mesos...
			$current = strtoupper ( date ( "m_F" ) );
			$index = array_search ( $current, self::$folders ) - self::$backupPhase;
			for($i = $index; $i >= 0; $i --) {
				if (is_dir ( $project ["unix_path"] . self::$folders [$i] )) {
					array_push ( $folders, array (
							"is_tar" => false,
							"is_delete" => false,
							"folder" => self::$folders [$i],
							"size" => 0,
							"status" => null,
							"splitted" => false,
							"path" => $project ["unix_path"] . self::$folders [$i] . "/",
							"tar_files" => null 
					) );
				}
			}
		}
		return $folders;
	}
	
	/**
	 * Reads the self::$base_dir and returns an array of project folders.
	 *
	 * @param unknown $folder        	
	 * @return array:
	 */
	protected static function getProjectsFromFolder($folder) {
		$folder = substr ( $folder, - 1 ) === '/' ? $folder : $folder . '/'; // Adds a last slash if needed.
		$projects = array ();
		$results = scandir ( $folder );
		
		$results = array_slice ( $results, 0, 100 ); // TODO: Descomentar aquesta linia quan producció.
		
		foreach ( $results as $result ) {
			if ($result === '.' or $result === '..' or strpos ( $result, '.' ) === 0)
				continue;
			if (is_dir ( $folder . $result ) && is_numeric ( $result ) && strlen ( $result ) == 6) { // code to use if project
				$data = self::getDGESTInfo ( $result );
				array_push ( $projects, array (
						"id" => $result,
						"unix_path" => $folder . $result . '/',
						"ms_path" => str_replace ( '/', '\\', str_replace ( self::$base_dir, "\\\\xeno\\\\traduccio\\", $folder ) . $result . "\\" ),
						"full_backup" => false,
						"open" => $data ["open"],
						"actions" => null,
						"tar" => null 
				) );
			} else {
				$full_path = $folder . $result;
				if (is_dir ( $full_path ) && $folder == self::$base_dir) {
					$sub_proj = self::getProjectsFromFolder ( $full_path );
					foreach ( $sub_proj as $sub ) {
						array_push ( $projects, $sub );
					}
				}
			}
		}
		return $projects;
	}
	
	/**
	 * Gets the directory size.
	 *
	 * @param unknown $path        	
	 * @return number (bytes)
	 */
	protected static function getDirectorySize($path) {
		$bytestotal = 0;
		$path = realpath ( $path );
		if ($path !== false) {
			foreach ( new \RecursiveIteratorIterator ( new \RecursiveDirectoryIterator ( $path, \FilesystemIterator::SKIP_DOTS ) ) as $object ) {
				$bytestotal += $object->getSize ();
			}
		}
		return $bytestotal;
	}
	
	/**
	 * Execute the compress process 7z
	 *
	 * @param string $source        	
	 * @param string $tar_file_name        	
	 * @param string $dest        	
	 * @return array
	 */
	protected static function execute7z($source, $tar_file_name, $dest) {
		$d = array (
				"success" => false,
				"status" => null,
				"command" => null,
				"tar_files" => null,
				"splitted" => false,
				"size" => 0 
		);
		
		$cmd = "7z a \"" . $dest . $tar_file_name . ".7z\" -mx" . self::$compressionLevel . " -v" . self::$splitSize . "b  \"$source*\"";
		$check = shell_exec ( $cmd );
		
		$status = self::parse7zStatus ( $check );
		
		$d ["command"] = $cmd;
		$d ["success"] = $status ["valid"];
		$d ["status"] = $status ["status"];
		
		if ($d ["success"]) {
			$array_of_7z = array ();
			
			$files = scandir ( $dest );
			foreach ( $files as $file ) {
				if (strpos ( $file, $tar_file_name ) !== false) {
					array_push ( $array_of_7z, $dest . $file );
				}
			}
			
			if (count ( $array_of_7z ) == 1) {
				$name = str_replace ( ".001", "", $array_of_7z [0] );
				rename ( $array_of_7z [0], $name );
				$array_of_7z [0] = $name;
			} else {
				$d ["splitted"] = true;
			}
			
			$d ["tar_files"] = $array_of_7z;
			
			$fsize = 0;
			foreach ( $array_of_7z as $compressed ) {
				$fsize += filesize ( $compressed );
			}
			$d ["size"] = $fsize;
		}
		
		return $d;
	}
	
	/**
	 * Parse the STDOUT from 7z to check if the compression process ends ok.
	 *
	 * @param string $status        	
	 * @return array
	 */
	protected static function parse7zStatus($status) {
		$r = array (
				"valid" => false,
				"status" => null 
		);
		
		if (strpos ( $status, "Everything is Ok" ) !== false) {
			$r ["valid"] = true;
			$r ["status"] = "Everything is Ok";
		} else if (strpos ( $status, "System error: File exists" ) !== false) {
			$r ["status"] = "File exists";
		} else {
			$r ["status"] = $status;
		}
		return $r;
	}
	
	/**
	 * Obté la informació DGEST del projecte.
	 *
	 * @param integer $project_id        	
	 * @return Ambigous <unknown, multitype:\dbconn\mixed , multitype:, NULL>
	 */
	protected static function getDGESTInfo($project_id) {
		dbConn::setConnex ( DBCONN_ORACLE, "webtraduc", "webtraduc", "(DESCRIPTION =(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(Host = oxigen.iit.idisc.es)(Port = 1521)))(CONNECT_DATA = (SERVICE_NAME = gestio.idisc.es) (SERVER = DEDICATED)))" );
		// dbConn::setConnex(DBCONN_ORACLE, "webtraduc", "webtraduc", "(DESCRIPTION =(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(Host = cesi.iit.idisc.es)(Port = 1521)))(CONNECT_DATA = (SID = idisc) (SERVER = POOLED)))");
		$dbc = new dbConn ();
		
		$sql = "SELECT obert FROM SYGESPROJECTES WHERE codi=:CODI";
		$vars ["CODI"] = $project_id;
		$rs = $dbc->execute ( $sql, $vars );
		if ($rs->fetch ()) {
			$open = $rs->getVal ( "obert" );
		} else {
			$open = "N";
		}
		$rs->close ();
		
		$r = array (
				"open" => $open 
		);
		return $r;
	}
	
	/**
	 * Saves the process data to the DB.
	 *
	 * @param array $project        	
	 * @param array $compressable        	
	 */
	protected static function saveDataToDB($project, $compressable) {
		dbConn::setConnex ( DBCONN_ORACLE, "webtraduc", "webtraduc", "(DESCRIPTION =(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(Host = oxigen.iit.idisc.es)(Port = 1521)))(CONNECT_DATA = (SERVICE_NAME = gestio.idisc.es) (SERVER = DEDICATED)))" );
		// dbConn::setConnex(DBCONN_ORACLE, "webtraduc", "webtraduc", "(DESCRIPTION =(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(Host = cesi.iit.idisc.es)(Port = 1521)))(CONNECT_DATA = (SID = idisc) (SERVER = POOLED)))");
		$dbc = new dbConn ();
		
		$rs = $dbc->execute ( "SELECT nvl(max(id)+1,0) as ID FROM PROJECT_BACKUP" );
		
		if ($rs->fetch ()) {
			$id = $rs->getVal ( "ID" );
		} else {
			$id = 0;
		}
		
		$rs->close ();
		
		$vars ["ID"] = $id;
		$vars ["PATH"] = $project ["unix_path"];
		$vars ["FULL_BACKUP"] = $project ["full_backup"] ? "1" : "0";
		$vars ["IS_COMPRESSED"] = $compressable ["is_tar"];
		$vars ["IS_DELETED"] = $compressable ["is_delete"] ? "1" : "0";
		$vars ["FOLDER"] = $compressable ["folder"];
		$vars ["DGEST_CODE"] = $project ["id"];
		$vars ["IS_SPLITTED"] = $compressable ["splitted"] ? "1" : "0";
		$vars ["SPLITTED_PARTS"] = count ( $compressable ["tar_files"] );
		$vars ["BYTES"] = $compressable ["size"];
		$dbc->setValues ( "C", "PROJECT_BACKUP", $vars );
		
		unset ( $vars );
		
		$vars ["COMPRESSED_FILE"] = json_encode ( $compressable );
		
		$varsW ["ID"] = $id;
		
		$dbc->setValues ( "M", "PROJECT_BACKUP", $vars, "ID=:ID", $varsW );
	}
	
	/**
	 * Gets the process data from the DB
	 *
	 * @param array $project        	
	 * @param array $compressable        	
	 * @return array
	 */
	protected static function getDataBackup($project, $compressable) {
		$r = array (
				"is_tar" => false,
				"is_delete" => false,
				"exists_backup" => false,
				"full_backup" => false,
				"folder" => null,
				"dvd_code" => null,
				"path" => null 
		);
		
		dbConn::setConnex ( DBCONN_ORACLE, "webtraduc", "webtraduc", "(DESCRIPTION =(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(Host = oxigen.iit.idisc.es)(Port = 1521)))(CONNECT_DATA = (SERVICE_NAME = gestio.idisc.es) (SERVER = DEDICATED)))" );
		// dbConn::setConnex(DBCONN_ORACLE, "webtraduc", "webtraduc", "(DESCRIPTION =(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(Host = cesi.iit.idisc.es)(Port = 1521)))(CONNECT_DATA = (SID = idisc) (SERVER = POOLED)))");
		$dbc = new dbConn ();
		
		$sql = "SELECT * FROM PROJECT_BACKUP WHERE DGEST_CODE=:DGEST_CODE AND FOLDER=:FOLDER";
		
		$vars ["DGEST_CODE"] = $project ["id"];
		$vars ["FOLDER"] = $compressable ["folder"];
		
		$rs = $dbc->execute ( $sql, $vars );
		
		if ($rs->fetch ()) {
			$compressed = json_decode ( $rs->getVal ( "COMPRESSED_FILE" ) );
			$r ["is_tar"] = $compressed->is_tar;
			$r ["is_delete"] = $compressed->is_delete;
			$r ["exists_backup"] = self::checkIfFilesExist ( $compressed->tar_files );
			$r ["full_backup"] = $rs->getVal ( "FULL_BACKUP" ) == "1" ? true : false;
			$r ["folder"] = $compressed->folder;
			$r ["dvd_code"] = $rs->getVal ( "DVD_CODE" );
			$r ["path"] = $rs->getVal ( "PATH" );
			$r ["splitted"] = $compressed->splitted;
			$r ["status"] = $compressed->status;
		}
		$rs->close ();
		
		return $r;
	}
	
	/**
	 * Check if all files of array exists.
	 *
	 * @param array $files        	
	 * @return boolean
	 */
	protected static function checkIfFilesExist($files) {
		if ($files == null)
			return false;
		foreach ( $files as $file ) {
			if (! file_exists ( $file )) {
				return false;
			}
		}
		return true;
	}
	protected static function createDVD($projects) {
		dbConn::setConnex ( DBCONN_ORACLE, "webtraduc", "webtraduc", "(DESCRIPTION =(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(Host = oxigen.iit.idisc.es)(Port = 1521)))(CONNECT_DATA = (SERVICE_NAME = gestio.idisc.es) (SERVER = DEDICATED)))" );
		// dbConn::setConnex(DBCONN_ORACLE, "webtraduc", "webtraduc", "(DESCRIPTION =(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(Host = cesi.iit.idisc.es)(Port = 1521)))(CONNECT_DATA = (SID = idisc) (SERVER = POOLED)))");
		$dbc = new dbConn ();
		
		$arr_dvds = array ();
		
		$sql = "SELECT * FROM PROJECT_BACKUP WHERE DVD_CODE IS NULL";
		$rs = $dbc->execute ( $sql );
		
		$projects = $rs->getAll ();
		$rs->close ();
		
		if (count ( $projects ) > 0) {
			$dvd = new dvd ();
		}
		
		foreach ( $projects as $project ) {
			if (! $dvd->add ( $project )) {
				$dvd->export ( $arr_dvds );
				
				$dvd = new dvd ();
				$dvd->add ( $project );
			}
		}
		
		if (count ( $projects ) > 0) {
			$dvd->close (); // Close the last dvd if needed.
			$dvd->export ( $arr_dvds );
		}
		
		return $arr_dvds;
	}
}
class dvd {
	private $dvdName = "BKP-";
	public $total_size = 0;
	public $count = 0;
	public $number = 0;
	public $size = 0;
	public $parts = 0;
	public $base_dvd_dir = '/webs/idcpws/backupFolder/DVDS/';
	public $dvd_dir;
	private $projects = array ();
	private $files = array ();
	private $closed = false;
	private $dbc;
	public function __construct() {
		dbConn::setConnex ( DBCONN_ORACLE, "webtraduc", "webtraduc", "(DESCRIPTION =(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(Host = oxigen.iit.idisc.es)(Port = 1521)))(CONNECT_DATA = (SERVICE_NAME = gestio.idisc.es) (SERVER = DEDICATED)))" );
		// dbConn::setConnex(DBCONN_ORACLE, "webtraduc", "webtraduc", "(DESCRIPTION =(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(Host = cesi.iit.idisc.es)(Port = 1521)))(CONNECT_DATA = (SID = idisc) (SERVER = POOLED)))");
		$this->dbc = new dbConn ();
		
		$this->total_size = 4509715660.8;
		$this->number = $this->getNewDVDNumber ();
		$this->dvd_dir = $this->base_dvd_dir . $this->dvdName . $this->number . "/";
		
		if (! is_dir ( $this->base_dvd_dir )) {
			mkdir ( $this->base_dvd_dir, 0770 );
		}
		
		mkdir ( $this->dvd_dir, 0770 );
	}
	public function add($project) {
		$compress = json_decode ( $project ["COMPRESSED_FILE"] );
		$exists = $this->checkIfFilesExist ( $compress->tar_files );
		
		if (! $exists)
			return true; // TODO: Sino existeix algun fitxer... s'haurà de fer algo.
		
		$fsize = 0;
		foreach ( $compress->tar_files as $file ) {
			$fsize += filesize ( ($file) );
		}
		
		if ($this->size + $fsize < $this->total_size || $this->size == 0) {
			$current_files = array ();
			
			foreach ( $compress->tar_files as $file_path ) {
				$fname = basename ( $file_path );
				rename ( $file_path, $this->dvd_dir . $fname );
				array_push ( $this->files, $fname );
				array_push ( $current_files, $fname );
			}
			array_push ( $this->projects, array (
					"dgest_code" => $project ["DGEST_CODE"],
					"folder" => $project ["FOLDER"],
					"full_backup" => $project ["FULL_BACKUP"] == "1" ? true : false,
					"files" => $current_files 
			) );
			$this->size += $fsize;
			$r = true;
		} else {
			$this->close ();
			$r = false;
		}
		return $r;
	}
	public function close() {
		if (! $this->closed) {
			$this->parts = ceil ( $this->size / $this->total_size );
			$this->updateDB ();
		}
	}
	private function updateDB() {
		foreach ( $this->projects as $project ) {
			$vars ["DVD_CODE"] = $this->dvdName . $this->number;
			
			$varsW ["DGEST_CODE"] = $project ["dgest_code"];
			$varsW ["FOLDER"] = $project ["folder"];
			
			$this->dbc->setValues ( "M", "PROJECT_BACKUP", $vars, "DGEST_CODE=:DGEST_CODE AND FOLDER=:FOLDER", $varsW );
		}
		$this->closed = true;
		
		$vars ["BYTES"] = $this->size;
		$vars ["PROJECTS"] = json_encode ( $this->projects );
		$vars ["DVD_DIR"] = $this->dvd_dir;
		$vars ["PARTS"] = $this->parts;
		
		$varsW ["DVD_NUMBER"] = $this->number;
		
		$this->dbc->setValues ( "M", "DVD_NUMBER", $vars, "DVD_NUMBER=:DVD_NUMBER", $varsW );
	}
	public function export(&$arr) {
		$a = array (
				"dvd_number" => $this->number,
				"projects" => $this->projects,
				"size" => $this->size,
				"dvd_dir" => $this->dvd_dir,
				"parts" => $this->parts,
				"closed" => $this->closed 
		);
		array_push ( $arr, $a );
	}
	private function getNewDVDNumber() {
		$rs = $this->dbc->execute ( "SELECT nvl(max(DVD_NUMBER)+1,1000) AS NUM FROM DVD_NUMBER" );
		$rs->fetch ();
		$dvd_number = $rs->getVal ( "NUM" );
		$vars ["DVD_NUMBER"] = $dvd_number;
		$vars ["DESCRIPTION"] = "DVD Description...";
		$this->dbc->setValues ( "C", "DVD_NUMBER", $vars );
		return $dvd_number;
	}
	private function checkIfFilesExist($files) {
		if ($files == null)
			return false;
		
		foreach ( $files as $file ) {
			if (! file_exists ( $file )) {
				return false;
			}
		}
		return true;
	}
}

