<?php
namespace api\crud;

use query\Query;
use api\system\Chapter;
use core\Environment;

/**
 * Class ChapterManagement
 *
 * @author phidalgo
 * @namespace api\crud
 */
abstract class ChapterManagement {
	public static function listChapter($paging = true){
		$query = new Query();
		$query->select("C.ID,C.NAME");
		$query->select("DEPARTMENT","(SELECT NAME FROM DEPARTMENTS WHERE ID=C.DEPARTMENT_ID)");
		$query->from("CHAPTERS C");
		$query->filter("C.NAME");
		$query->order("NAME","C.NAME");
		return ($paging? $query->paging():$query->execute());
	}
	
	public static function addChapter($data){
		$chapter = new Chapter();
		return $chapter->set($data)->save();
	}
	
	public static function updateChapter($id,$data){
		$chapter = new Chapter($id);
		return $chapter->set($data)->save();
	}
	
	public static function deleteChapter($id){
		$chapter = new Chapter($id);
		$chapter->delete();
	}
	
	public static function getUsersChapter($uid){
		$environment = Environment::getInstance();
		$rs = $environment->dbcon->execute("SELECT CHAPTER_ID,ELEMENT_ID FROM USERS_CHAPTERS WHERE USER_ID=:USER_ID",array("USER_ID" => $uid));
		$fetch = $rs->getAll();
		$rs->close();
		return $fetch;
	}
	
	public static function addUserChapters($uid,$cid,$ceid){
		$environment = Environment::getInstance();
		$vars["USER_ID"] = $uid;
		$vars["CHAPTER_ID"] = $cid;
		$vars["ELEMENT_ID"] = $ceid;
		$environment->dbcon->add("USERS_CHAPTERS",$vars);
	}
	
	/**
	 * Get the available chapters for the user filtered by department.
	 * @param integer|null $user_department
	 */
	public static function getChapterAndElements($user_department = null){
		$environment = Environment::getInstance();
		$vars = array();
		$dptSQL = "";
		if($user_department!=null){
			$dptSQL= "WHERE (DEPARTMENT_ID=:DPT_UID OR DEPARTMENT_ID is null) ";
			$vars["DPT_UID"] = $user_department;
		}
		
		$rs = $environment->dbcon->execute("SELECT ID,NAME FROM CHAPTERS $dptSQL ORDER BY NAME",$vars);
		$chapters = $rs->getAll();
		$rs->close();
		foreach($chapters as &$chapter){
			$rs = $environment->dbcon->execute("SELECT ID,NAME FROM CHAPTERS_ELEMENTS WHERE CHAPTER_ID=:CID",array("CID" => $chapter["id"]));
			$el = $rs->getAll();
			$chapter["elements"] = $el;
		}
		return $chapters;
	}
	
	public static function listChapterElements($cid,$paging = true){
		$query = new Query();
		$query->select("ID,NAME")->from("CHAPTERS_ELEMENTS");
		$query->filter("NAME");
		$query->order("NAME","NAME");
		$query->where("CHAPTER_ID='$cid'");
	
		if($paging){
			return $query->paging();
		}
		return $query->execute();
	}
	
	public static function addChapterElement($cid,$data){
		$chapter = new Chapter($cid);
		$chapter->addElement($data);
	}
	
	public static function updateChapterElement($cid,$ceid,$data){
		$chapter = new Chapter($cid);
		$chapter->updateElement($ceid, $data);
	}
	
	public static function deleteChapterElement($cid,$data){
		foreach($data->ceid as $ceid){
			$chapter = new Chapter($cid);
			$chapter->deleteElement($ceid);
		}
		return true;
	}
	
	public static function getChapterOptions(){
		$chapter = new Chapter();
		return $chapter->get();
	}
	
	/**
	 * Get a chapter by id
	 * @param integer $cid
	 */
	public static function getChapter($cid){
		$chapter = new Chapter($cid);
		return $chapter->get();
	}
}