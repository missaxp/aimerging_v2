<?php

/**
 * Fields management class
 *
 * @author phidalgo
 * @namespace api\crud
 */
namespace api\crud;

use core\Environment;
use query\Query;

abstract class FieldsManagement {
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	public static function _get($paging = true,$defaultReturn = null){
		$env = Environment::getInstance();
		$query = new Query();
		$query->defaultReturn($defaultReturn); //Set default return if any.
		
		$query->select("F.ID,F.TARIFABLE,F.TYPE,F.NAME");
		$query->select("LABEL","FL.TEXT");
		$query->from("FIELDS F,FIELDS_LANGS FL");
		$query->where("F.ID=FL.F_ID");
		$query->where("FL.LANGUAGE")->compareTo("'".$env->user->ui_language."'");
		$query->order("NAME", "FL.NAME");
		
		if($paging){
			return $query->paging();
		}
		else{
			return $query->execute();
		}
	}
	

	public static function _getById($fid){
		if(strtoupper($fid)=='OPTIONS'){
			$f = new Field();
		}
		else{
			$f = new Field($fid);
		}
		
		return $f->get();
	}
	
	public static function _update($fid,$data){
		$f = new Field($fid);
		return $f->set($data)->save();
	}
	
	public static function _add($data){
		$f = new Field();
		return $f->set($data)->save();
	}
	
	public static function _delete($fid){
		$f = new Field($fid);
		return $f->delete();
	}
	
}
/* end of abstract class ProjectsManagement */
?>