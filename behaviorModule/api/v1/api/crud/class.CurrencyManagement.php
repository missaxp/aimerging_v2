<?php
namespace api\crud;

use query\Query;
use core\Environment;
use query\_OO;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;

/**
 * Class CurrencyManagement
 *
 * @author phidalgo
 * @namespace api\crud
 */
abstract class CurrencyManagement {

	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
		
	public static function getCurrencies($paging = true){
		$query = new Query();
		$query->select("ID","C.ID")
		->select("NAME","C.NAME")
		->select("DECIMALS","C.DECIMALS")
		->select("ALIAS","C.ALIAS");
		$query->from("CURRENCY C");
		$query->filter("NAME","C.NAME")->operator(_OO::LIKE);
		$query->filter("CODE","C.ID");
		$query->filter("ALIAS","C.ALIAS")->operator(_OO::LIKE);
		$query->order("NAME","C.NAME");
		
		return ($paging? $query->paging():$query->execute());
	}
	
	public static function addCurrency($id = null, $data){
		self::$environment = Environment::getInstance();

		$rsp = array();		
		$rsp["FormError"] = false;
		$rsp["ErrorMsg"] = "";
		
		
		$vars = array();		
		foreach($data as $prop => $value){
			$vars[$prop] = $value;
		
			$lenValue = strlen($value);
		
			if ($prop == "id") $maxLength = 3;
			if ($prop == "name") $maxLength = 37;
			if ($prop == "decimals") $maxLength = 1;
			if ($prop == "alias") $maxLength = 3;
		
			if ($lenValue > $maxLength) throw new AppException(Status::S4_PreconditionFailed,ErrorCode::LengthError,"Field $prop maximum length error. Max : $maxLength. Length :  $lenValue.");
		
		}
	
		if (is_null ($id)) {
										
			$rs = self::$environment->dbcon->execute("SELECT * FROM CURRENCY WHERE ID=:CID",array("CID" => $data->id));
			$fetch = $rs->getAll();
			$rs->close();					
			if(count($fetch)==0){			
				$rsp["id"] = $data->id;
				$vars = array("ID" => $data->id, "NAME" => $data->name, "DECIMALS" => $data->decimals, "ALIAS" => $data->alias);
				self::$environment->dbcon->add("CURRENCY", $vars);							
			} else {
				$cCode = $fetch[0]['ID'];
				$cName = $fetch[0]['NAME'];				
				throw new AppException(Status::S4_PreconditionFailed, ErrorCode::NotAcceptable, "Currency  $cName ( $cCode ) exist");								
			}			
			
		} else {
			$rsp["id"] = $id;
			$varsW["CID"] = $id;
			self::$environment->dbcon->update("CURRENCY", $vars,"ID=:CID",$varsW);
		}	
				
		return $rsp;			
	}
	
	public static function currencyNotExist($id){
		self::$environment = Environment::getInstance();
		
		$rs = self::$environment->dbcon->execute("SELECT * FROM CURRENCY WHERE ID=:CID",array("CID" => $id));
		$fetch = $rs->getAll();
		$rs->close();
		if(count($fetch)==0){
			return true;
		} 	
		return false;		
	}
		
	public static function getCurrencyById($id){
		self::$environment = Environment::getInstance();
		
		$rs = self::$environment->dbcon->execute("SELECT * FROM CURRENCY WHERE ID=:CID",array("CID" => $id));
		$fetch = $rs->getAll();
		$rs->close();
		if(count($fetch)==0){
			throw new AppException(Status::S4_NotFound,ErrorCode::NotFound,"Currency Resource $id not found");
		}
		$fetch = $fetch[0];
		return $fetch;
		
	}
	
	
	public static function deleteCurrencyById($id){
		return Environment::getInstance()->dbcon->execute ( "DELETE FROM CURRENCY WHERE ID=:CID", array ("CID" => $id));
	}
	
	public static function deleteCurrencies($data){
		$environment = Environment::getInstance ();
					
		$totalItem = 0;
		$itemsDeleted = 0;
		$itemsNotDeleted = 0;
				
	
		foreach($data as $prop => $values){
			
			if ($prop == "arrayIds") {
				foreach($values as $value){					
					$num = 0;
					$totalItem++;
					$sql = "SELECT count(1) AS NUM FROM THIRD_PARTY WHERE CURRENCY_ID=:CID";
					$rs = $environment->dbcon->execute ( $sql, array (
							"CID" => $value
					) );
					if ($rs->fetch ()) {
						$num = $rs->getVal ("NUM");
					}
					$rs->close ();

					if ($num > 0) {
						// No es pot borrar						
						$itemsNotDeleted++;
						$rsp["NoDeletedItemsCode"][] = $value;
						
					} else {
						// Borrar												
						$afectedRows = $environment->dbcon->execute ( "DELETE FROM CURRENCY WHERE ID=:CID", array (
									"CID" => $value
						) );						
						$itemsDeleted++;
						$rsp["DeletedItemsCode"][] = $value;						
					}															
				}				
			}			
		}
		$rsp["TotalItems"] = $totalItem;		
		$rsp["DeletedItems"] = $itemsDeleted;
		$rsp["NoDeletedItems"] = $itemsNotDeleted;
		
		return $rsp;
		
	}
		
}