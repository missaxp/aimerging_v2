<?php
namespace api\crud;

use core\Exception\AppException;
use core\Environment;
use core\http\ErrorCode;
use core\http\Status;
use query\Query;

/**
 * Class MantManagement
 *
 * @author phidalgo
 * @namespace api\crud
 */
abstract class MantManagement {
	public static function cities($name = null){
		$env = Environment::getInstance();
		$sql = "SELECT NAME FROM AUX_CITIES WHERE upper(name) LIKE :NAME||'%'";
		$rs = $env->dbcon->execute($sql,array("name" => $name));
		$c = $rs->getAll();
		$rs->close();
		return $c;
	}
	
	public static function regions($name = null){
		$env = Environment::getInstance();
		$sql = "SELECT NAME FROM AUX_REGIONS WHERE upper(name) LIKE :NAME||'%'";
		$rs = $env->dbcon->execute($sql,array("name" => $name));
		$c = $rs->getAll();
		$rs->close();
		return $c;
	}
	
	public static function provinces($name = null){
		$env = Environment::getInstance();
		$sql = "SELECT NAME FROM AUX_PROVINCES WHERE upper(name) LIKE :NAME||'%'";
		$rs = $env->dbcon->execute($sql,array("name" => $name));
		$c = $rs->getAll();
		$rs->close();
		return $c;
	}
		
	/**
	 * Get tables o table.
	 * @param integer $id
	 */
	public static function tables($id = null){
		$rsp = null;
		if($id==null){
			$query = new Query("SELECT * FROM TABLES WHERE VISIBLE='Y' ORDER BY ID");
			$rsp = $query->paging(); 
		}
		else{
			$env = Environment::getInstance();
			$rsp["table"] = $env->tables->_get($id);
			$rsp["attributes"] = AttributesManagement::getTableAttributes($id);
			$rsp["options"]["attributes"] = AttributesManagement::getAttributes();
		}
		return $rsp; 
	}
	
	
	/**
	 * Update table.
	 * 
	 * @param integer $id
	 * @param \stdClass $data
	 */
	public static function updateTable($id,$data){

		$env = Environment::getInstance();
		foreach($data as $key => $value){
			if(strpos($key,"attr_")!==false){
				$atrid = str_replace("attr_","",$key);
				if($value=="Y"){
					AttributesManagement::addAttributeRel($id,$atrid);
				}
				else if ($value=="N"){
					$env->dbcon->execute("DELETE FROM ATTRIBUTES_REL WHERE ATTR_ID=:ID AND TABLE_ID=:IDT",array("ID" => $atrid,"IDT" => $id));
				}
			}
			if($key=='enable_series'){
				$vars["SERIES"] = $value=="Y"? "Y":"N";
				$vw["ID"] = $id;
				$env->dbcon->update("TABLES",$vars,"ID=:ID",$vw);
			}
			if($key == 'series'){
				$series = $value;
				$table = $env->tables->_get($id);
				foreach($table->series as $serie){
					$foundSerie = false;
					foreach($series as $ns){
						if($serie->serie==$ns->serie){
							$foundSerie = true;
							break;
						}
					}
					
					if(!$foundSerie){
						$env->dbcon->execute("DELETE FROM TABLES_SERIES WHERE TABLE_ID=:TID AND SERIE=:SERIE",array("TID"=>$id,"SERIE" =>$serie->serie ));
					}
				}
				
				foreach($series as $serie){
					$rs = $env->dbcon->execute("SELECT serie from TABLES_SERIES WHERE TABLE_ID=:TID AND SERIE=:SERIE",array("TID" => $id,"SERIE" => $serie->serie));
					if($rs->fetch()){
						$vars["NAME"] = $serie->name;
						$vars["COUNTER"] = $serie->counter;
						$vars["DEPARTMENT_ID"] = ($serie->department_id=="ALL"? null:$serie->department_id); 
						$vw["TID"] = $id;
						$vw["SERIE"] = $serie->serie;
						
						$env->dbcon->update("TABLES_SERIES",$vars,"TABLE_ID=:TID AND SERIE=:SERIE",$vw);
					}
					else{
						$vars["NAME"] = $serie->name;
						$vars["SERIE"] = $serie->serie;
						$vars["TABLE_ID"] = $id;
						$vars["COUNTER"] = $serie->counter;
						$vars["DEPARTMENT_ID"] = ($serie->department_id=="ALL"? null:$serie->department_id);
						$env->dbcon->add("TABLES_SERIES",$vars);
					}
				}
			}
		}
	}
	
	public static function checkSerieName($tid,$ccid){
		$env = Environment::getInstance();
		$rs = $env->dbcon->execute("SELECT SERIE FROM TABLES_SERIES WHERE TABLE_ID=:TID AND SERIE=:SERIE",array("TID" => $tid,"SERIE" => $ccid));
		$fetch = $rs->getAll();
		$rs->close();
		if(count($fetch)==0){
			return true;
		}
		return false;
	}
}