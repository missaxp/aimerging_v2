<?php
namespace api\crud;

use core\Environment;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;

/**
 * Class TagsManagement
 * 
 * Creates/updates/deletes and work with TAGS. 
 * For use internal and as a API response.
 *
 * @author phidalgo
 * @namespace api\crud
 */
abstract class TagsManagement {
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	
	/**
	 * Get a list of tags filtered by TABLE_ID
	 * @param integer $table_id
	 */
	public static function getTagsList($table_id){
		self::$environment = Environment::getInstance ();
		return self::getTags(null,$table_id);
	}
	
	private static function getTags($startTag = null,$table_id){
		$sql = "SELECT ID,ID_PARENT,NAME,POSITION FROM TAGS 
		WHERE ".($startTag!=null? "ID_PARENT=:ID_PARENT":"ID_PARENT is NULL")." AND TABLE_ID=:TABLE_ID ORDER BY ID_PARENT DESC,POSITION";
		$vars["TABLE_ID"] = $table_id;
		if($startTag!=null){
			$vars["ID_PARENT"] = $startTag;
		}
		$rs = self::$environment->dbcon->execute($sql,$vars);
		$tags = $rs->getAll();
		$rs->close();
		foreach($tags as &$tag){
			$tag["CHILDRENS"] = self::getTags($tag["id"],$table_id);
		}
		return $tags;
	}
	
	/**
	 * Adds a TAG
	 * @param \stdClass $tag
	 * @param integer $table_id
	 * @throws AppException
	 */
	public static function addTag($tag,$table_id){
		self::$environment = Environment::getInstance();
		$sql = "SELECT (IFNULL[max(id),0]+1) as ID FROM TAGS";
		$rs = self::$environment->dbcon->execute($sql);
		if(!$rs->fetch()){
			$rs->close();
			throw new AppException(Status::S5_InternalServerError,ErrorCode::DBQueryError);
		}
		$vars["ID"] = $rs->id;
		$rs->close();
		if($tag->id_parent!=""){
			$vars["ID_PARENT"] = $tag->id_parent;
		}
		$vars["NAME"] = $tag->name;
		if(self::$environment->authentication_required){
			$vars["USER_ID"] = self::$environment->user->id;
			$vars["DEPARTMENT_ID"] = self::$environment->user->user_department;
		}
		$vars["TABLE_ID"] = $table_id;
		
		//Get position for new element.
		if($tag->id_parent!=""){
			$sql = "SELECT (count(1)+1) as POSITION FROM TAGS WHERE ID_PARENT=".$vars["ID_PARENT"];
		}
		else{
			$sql = "SELECT (count(1)+1) as POSITION FROM TAGS WHERE ID_PARENT IS NULL";
		}
		$rs = self::$environment->dbcon->execute($sql);
		if(!$rs->fetch()){
			$rs->close();
			throw new AppException(Status::S5_InternalServerError,ErrorCode::DBQueryError);
		}
		$vars["POSITION"] = $rs->POSITION;
		$rs->close();
		
		$rsp = array(
			"ID" => $vars["ID"],
			"NAME" => $vars["NAME"],
			"POSITION" => $vars["POSITION"]
		);
		self::$environment->dbcon->add("TAGS",$vars); //Create element on DB.
		
		return $rsp;
	}
	
	/**
	 * Delete a TAG
	 * @param integer $id
	 */
	public static function deleteTag($id){
		self::$environment = Environment::getInstance();
		self::$environment->dbcon->execute("DELETE FROM TAGS WHERE ID=:ID",array("ID" => $id));
		return true;
	}
	
	public static function reorder($data){
		self::$environment = Environment::getInstance();
		$position = 1;
		foreach($data as $tag){
			self::doReorder($tag,$position);
			$position++;
		}
	}
	
	private static function doReorder($tag,$position,$id_parent=null){
		$vars["POSITION"] = $position;
		$vars["ID_PARENT"] = $id_parent;
		
		$varsW["ID"] = $tag->id_tag;
		
		self::$environment->dbcon->update("TAGS",$vars,"ID=:ID",$varsW);
		
		if(isset($tag->children)){
			$pos = 1;
			foreach($tag->children as $obj){
				self::doReorder($obj, $pos,$tag->id_tag);
				$pos++;
			}
		}
	}
	
	public static function tagsFormatted($table_id){
		return self::format(self::getTagsList($table_id));
	}
	
	private static function format($tags){
		$rsp = array();
		$r = "";
		foreach($tags as $tag){
			array_push($rsp,array(
				"id" => $tag["id"],
				"name" => $tag["name"]
			));
			if(count($tag["CHILDRENS"])>0){
				foreach($tag["CHILDRENS"] as $children){
					$chd = self::format(array($children));
					foreach($chd as $c){
						array_push($rsp,array(
							"id" => $c["id"],
							"name" => $tag["name"]." > ".$c["name"]
						));
					}
				}	
			}
		}
		return $rsp;
	}
	
	public static function inverseFormat($tid){
		self::$environment = Environment::getInstance ();
		return rtrim(self::getFullName($tid)," >");
	}
	
	private static function getFullName($tid){
		$name = "";
		$sql = "SELECT NAME, ID_PARENT FROM TAGS WHERE ID=:ID";
		$rs = self::$environment->dbcon->execute($sql,array("ID" => $tid));
		if($rs->fetch()){
			$name = $rs->NAME." > ".$name;
			$idp = $rs->ID_PARENT;
			$name = self::getFullName($idp).$name;
		}
		$rs->close();
		return $name;
	}
	
	/**
	 * Set tag relation given a tag_id and a relation id and table_id.
	 * @param integer $tag_id
	 * @param string $element_id
	 * @param integer $table_id
	 */
	public static function addTagRelation($tag_id,$element_id,$table_id){
		self::$environment = Environment::getInstance();
		$vars["ELEMENT_ID"] = $element_id;
		$vars["TAG_ID"] = $tag_id;
		$vars["TABLE_ID"] = $table_id;
		self::$environment->dbcon->add("TAGS_RELATION",$vars);
	}
	
	public static function getTagsRelation($element_id,$table_id){
		$rsp = array();
		self::$environment = Environment::getInstance();
		$rs = self::$environment->dbcon->execute("SELECT * FROM TAGS_RELATION WHERE ELEMENT_ID=:ID AND TABLE_ID=:TABLE_ID",array("ID" => $element_id,"TABLE_ID" => $table_id));
		$tpt = $rs->getAll();
		$rs->close();
		foreach($tpt as $tag){
			array_push($rsp,array("id" => $tag["tag_id"]));
		}
		return $rsp;
	}
	
	
}