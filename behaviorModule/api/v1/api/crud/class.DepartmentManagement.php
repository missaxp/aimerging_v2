<?php

namespace api\crud;

use core\Exception\AppException;
use core\Environment;
use core\http\ErrorCode;
use core\http\Status;
use query\Query;

/**
 * Class DepartmentManagement
 *
 * @author phidalgo & ctemporal
 * @namespace api\crud
 */
abstract class DepartmentManagement {
	/**
	 * List departments
	 *
	 * @return Array
	 */
	public static function getDepartments($paging = true,$defaultReturn = null) {
		$query = new Query();
		$query->defaultReturn($defaultReturn); //Set default return if any.
		
		$query->select("ID", "D.ID");
		$query->select("NAME", "D.NAME");
		//$query->select("USERS", "(select count(1) from (SELECT DEPARTMENT_ID FROM USERS UNION SELECT DEPARTMENT_ID FROM USERS_DEPARTMENTS ) WHERE DEPARTMENT_ID=D.ID)");
		$query->select("USERS","(select count(1) from USERS WHERE DEPARTMENT_ID=D.ID)");
		$query->order("DNAME","users");
		$query->order("USERS","users");
		$query->filter("NAME","D.ID");
		$query->from("DEPARTMENTS D");
		if($paging){
			return $query->paging();
		}
		return $query->execute();
	}
	
	/**
	 * Add a new department
	 *
	 * @param
	 *        	api\crud\Department department
	 * @throws core\Exception\AppException
	 */
	public static function addDepartment(Department $department) {
		$environment = Environment::getInstance ();
		
		if (! is_object ( $department ))
			throw new AppException ( Status::S5_InternalServerError, ErrorCode::DepartmentNotAnObject );
			
			// If no id is provided, we suposse that it's a new department. if a id is provided, we suposse that other script has make the max(id)+1.
		if ($department->id == null) {
			$rs = $environment->dbcon->execute ( "SELECT IFNULL[max(ID)+1,0] AS NEWID FROM DEPARTMENTS" );
			$fetch = $rs->getAll ();
			$rs->close ();
			if (count ( $fetch ) == 0)
				throw new AppException ( Status::S5_InternalServerError, ErrorCode::DBConnectionProblem );
			
			$department->id = $fetch [0] ["newid"];
		}
		
		$var ["id"] = $department->id;
		$var ["name"] = $department->name;
		
		$environment->dbcon->add("DEPARTMENTS",$var);
	}
	
	/**
	 * Get a department by Id
	 *
	 * @param Integer id
	 * @return api\crud\Department
	 */
	public static function getDepartmentbyId($id) {
		$department = new Department ( $id );
		$department->fillUsers();
		return $department;
	}
	
	/**
	 * Given a department id, it returns all the users for this department
	 *
	 * @param integer $id        	
	 */
	public static function getUsersPerDepartment($id) {
		$environment = Environment::getInstance ();
		$query = new Query();
		
		$query->select("USERNAME","U.USERNAME");
		$query->select("ID_USER","DPT.USER_ID");
		$query->select("MAIN","DPT.MAIN");
		$query->filter("USERNAME","u.USERNAME");
		$query->order("USERNAME","U.USERNAME");
		$query->from("DEPARTMENTS_USERS DPT, USERS U");
		$query->where("DEPARTMENT_ID")->compareTo($id);
		$query->where("DPT.USER_ID=U.ID");

		return $query->paging();
	}
	
	/**
	 * Delete a Department
	 *
	 * @param
	 *        	Integer id
	 * @throws core\Exception\AppException
	 */
	public static function deleteDepartment($id) {
		$environment = Environment::getInstance ();
		$environment->dbcon->execute ("DELETE FROM DEPARTMENTS WHERE ID=:ID", array("ID" => $id));
		/*$users = 0;
		$sql = "SELECT count(1) AS USERS FROM USERS_DEPARTMENTS WHERE DEPARTMENT_ID=:DEPARTMENT_ID";
		$rs = $environment->dbcon->execute ( $sql, array("DEPARTMENT_ID" => $id));
		if ($rs->fetch ()) {
			$users = $rs->users;
		}
		$rs->close ();
		
		if ($users > 0)
			throw new AppException ( Status::S4_PreconditionFailed, ErrorCode::CanNotDelete, "@LITERAL: Error there are $users users in this id($id) department. Can not delete it." );
		*/
		
	}
	
	/**
	 * Update department
	 *
	 * @param Integer $id        	
	 * @param Array $data        	
	 */
	public static function updateDepartment($id, $data) {
		$environment = Environment::getInstance ();
		
		$vars = array ("name" => $data["name"]);
		$varsWhere = array ("ID" => $id);
		$environment->dbcon->setValues ( "M", "DEPARTMENTS", $vars, "ID=:ID_DEPARTMENT", $varsWhere );
	}
	
	/**
	 * Adds a user to this department.
	 *
	 * @param integer $id        	
	 * @param array $data        	
	 */
	public static function addUserToDepartment($id, $data) {
		$environment = Environment::getInstance ();
		
		$users = $data["users"];
		
		foreach ( $users as $user_id ) {
			$vars ["ID"] = $id;
			$vars ["USER_ID"] = $user_id;
			$rs = $environment->dbcon->execute ("SELECT IN_DEPARTMENT(:USER_ID,:ID) AS ES FROM DUAL", $vars );
			$rs->fetch ();
			if ($rs->es!= 1) {
				$rs2 = $environment->dbcon->execute ("SELECT * FROM DEPARTMENTS_USERS WHERE USER_ID=:USER_ID AND MAIN='S'", array("USER_ID" => $user_id));
				$v["USER_ID"] = $user_id;
				$v["DEPARTMENT_ID"] = $id;
				if(!$rs2->fetch()) {
					$v ["MAIN"] = "S";
				} else {
					$v ["MAIN"] = "N";
				}
				$rs2->close ();
				$environment->dbcon->setValues ( "C", "DEPARTMENTS_USERS", $v );
			}
			$rs->close ();
		}
	}
	
	/**
	 * Removes user to this department.
	 *
	 * @param integer $id        	
	 * @param array $data        	
	 */
	public static function removeUserToDepartment($id, $data) {
		$environment = Environment::getInstance ();
		$users = $data["users"];
		foreach ( $users as $user_id ) {
			$environment->dbcon->execute ( "DELETE FROM DEPARTMENTS_USERS WHERE USER_ID=:USER_ID AND DEPARTMENT_ID=:DEPARTMENT_ID", array (
					"USER_ID" => $user_id,
					"DEPARTMENT_ID" => $id 
			) );
		}
	}
}
/* end of abstract class DepartmentManagement */
?>