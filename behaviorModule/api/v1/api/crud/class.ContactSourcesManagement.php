<?php
namespace api\crud;

use query\Query;
use core\Environment;
use query\_OO;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;

/**
 * Class ContactSourcesManagement
 *
 * @author phidalgo
 * @namespace api\crud
 */
abstract class ContactSourcesManagement {

	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
		
	public static function getContactSources($paging = true){
		$query = new Query();
				
		$query->select("ID","CS.ID")
		->select("NAME","CS.NAME");		
		$query->from("CONTACT_SOURCE CS");
		$query->filter("NAME","CS.NAME")->operator(_OO::LIKE);
		$query->filter("ID","CS.ID");
		$query->order("NAME","CS.NAME");
		
		if($paging){
			return $query->paging();
		}
		else{
			return $query->execute();
		}
		
	}
	
	public static function addContactSource($id = null, $data){
		self::$environment = Environment::getInstance();
	
		$rsp = array();		
		$rsp["FormError"] = false;
		$rsp["ErrorMsg"] = "";
			
		
		$vars = array();		
		foreach($data as $prop => $value){
			$vars[$prop] = $value;
		
			$lenValue = strlen($value);
		
			if ($prop == "id") $maxLength = 10;
			if ($prop == "name") $maxLength = 100;
					
			if ($lenValue > $maxLength) throw new AppException(Status::S4_PreconditionFailed,ErrorCode::LengthError,"Field $prop maximum length error. Max : $maxLength. Length :  $lenValue.");
		
		}
			
		if (is_null ($id)) {
				$rs = self::$environment->dbcon->execute("SELECT max(ID) AS ID FROM CONTACT_SOURCE");
				$fetch = $rs->getAll();
				$rs->close();
				
				if ($fetch[0]["id"] == '') {
					$idcontact = 1;
				} else {
					$idcontact = $fetch[0]["id"] + 1;
				}
							
				$rsp["id"] = $idcontact;
				$vars = array("ID" => $idcontact, "NAME" => $data->name);
				self::$environment->dbcon->add("CONTACT_SOURCE", $vars);									
		} else {
			$rsp["id"] = $id;
			$varsW["CID"] = $id;
			self::$environment->dbcon->update("CONTACT_SOURCE", $vars,"ID=:CID",$varsW);
		}	
				
		return $rsp;			
	}
		
	public static function contactSourcesNotExist($id){
		self::$environment = Environment::getInstance();
		
		if (!is_numeric($id)) {
			throw new AppException ( Status::S4_NotFound, ErrorCode::CanNotDelete, "@LITERAL: Can not delete because this contact source does not exists." );
		}
		
		$rs = self::$environment->dbcon->execute("SELECT * FROM CONTACT_SOURCE WHERE ID=:CID",array("CID" => $id));
		$fetch = $rs->getAll();
		$rs->close();
		if(count($fetch)==0){
			return true;
		} 	
		return false;		
	}
		
	public static function getContactSourceById($id){
		self::$environment = Environment::getInstance();
		
		if (!is_numeric($id)) {
			throw new AppException ( Status::S4_NotFound, ErrorCode::CanNotDelete, "@LITERAL: Can not delete because this contact source does not exists." );
		}
		
		$rs = self::$environment->dbcon->execute("SELECT * FROM CONTACT_SOURCE WHERE ID=:CID",array("CID" => $id));
		$fetch = $rs->getAll();
		$rs->close();
		if(count($fetch)==0){
			throw new AppException(Status::S4_NotFound,ErrorCode::NotFound,"Contact source Resource $id not found");
		}
		$fetch = $fetch[0];
		return $fetch;
		
	}
	
	
	public static function deleteContactSourceById($id){
		$environment = Environment::getInstance ();
		
		if (!is_numeric($id)) {
			throw new AppException ( Status::S4_NotFound, ErrorCode::CanNotDelete, "@LITERAL: Can not delete because this contact source does not exists." );
		}
		
		$num = 0;
		$sql = "SELECT count(1) AS NUM FROM CONTACT WHERE SOURCE_ID=:ID";
		$rs = $environment->dbcon->execute ( $sql, array (
				"ID" => $id 
		) );
		if ($rs->fetch ()) {
			$num = $rs->getVal ("NUM");
		}
		$rs->close ();
		
		if ($num > 0)
			throw new AppException ( Status::S4_PreconditionFailed, ErrorCode::CanNotDelete, "@LITERAL: Error there are $num contact in this id ($id) source. Can not delete it." );
		
		try {
			$afectedRows = $environment->dbcon->execute ( "DELETE FROM CONTACT_SOURCE WHERE ID=:CID", array (
					"CID" => $id 
			) );
		} catch ( \Exception $e ) {
			throw new AppException ( Status::S5_InternalServerError, ErrorCode::DBQueryError, $e->getMessage () );
		}
		
		if ($afectedRows === true)
			throw new AppException ( Status::S4_NotFound, ErrorCode::CanNotDelete, "@LITERAL: Can not delete because this contact source does not exists." );
	
	}
	
	public static function deleteContactSources($data){
		$environment = Environment::getInstance ();
					
		$totalItem = 0;
		$itemsDeleted = 0;
		$itemsNotDeleted = 0;
				
	
		foreach($data as $prop => $values){
			
			if ($prop == "arrayIds") {
				foreach($values as $value){					
					$num = 0;
					$totalItem++;
					
					$sql = "SELECT count(1) AS NUM FROM CONTACT WHERE SOURCE_ID=:CSID";
					$rs = $environment->dbcon->execute ( $sql, array (
							"CSID" => $value
					));									
					if ($rs->fetch ()) {
						$num = $rs->getVal ("NUM");
					}
					$rs->close ();
					
														
					if ($num > 0) {
						// No es pot borrar						
						$itemsNotDeleted++;
						$rsp["NoDeletedItemsCode"][] = $value;
						
					} else {
						// Borrar													
						$afectedRows = $environment->dbcon->execute ( "DELETE FROM CONTACT_SOURCE WHERE ID=:CSID", array (
									"CSID" => $value
						) );							
						$itemsDeleted++;
						$rsp["DeletedItemsCode"][] = $value;						
					}															
				}				
			}			
		}
		$rsp["TotalItems"] = $totalItem;		
		$rsp["DeletedItems"] = $itemsDeleted;
		$rsp["NoDeletedItems"] = $itemsNotDeleted;
		
		return $rsp;
		
	}
		
}