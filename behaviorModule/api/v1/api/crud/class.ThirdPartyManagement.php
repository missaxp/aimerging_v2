<?php
namespace api\crud;

use core\Environment;
use query\Query;
use common\Table;



/**
 * Class ThirdPartyManagement
 *
 * @author phidalgo
 * @namespace api\crud
 */
abstract class ThirdPartyManagement {	
	
	/**
	 * List THIRD PARTY resources.
	 * 
	 * @return Query
	 */
	public static function listThirdParty() {
		$environment = Environment::getInstance ();
		
		$query = new Query();				
		$query
		->select("TP.ID AS CODE, TP.NAME AS NAME,TP.CIF AS CIF,TP.ADDRESS AS ADDRESS, TP.TOWN AS TOWN, TP.ZIP_CODE as ZIP, TP.PHONE as TEL")
		->select("TP.WEB AS URL, TP.ENABLED AS ENABLED,TP.REAL_CUSTOMER AS RC, TP.REAL_PROVIDER AS RP,TP.POTENTIAL_CUSTOMER AS PC, TP.POTENTIAL_PROVIDER AS PP")
		->select("TP.AGENT AS AG")
		->select("TP.POTENTIALITY AS POTENTIALITY")
		->from("THIRD_PARTY TP");
		$query->filter("NAME","TP.NAME")->like();
		$query->filter("ENABLED", "TP.ENABLED");
		
		$query->filter("USER","TP.USER_ID");
		$query->filter("COUNTRY","TP.COUNTRY_ID");
		$query->filter("RC","TP.REAL_CUSTOMER");
		$query->filter("RP","TP.REAL_PROVIDER");
		$query->filter("PC","TP.POTENTIAL_CUSTOMER");
		$query->filter("PP","TP.POTENTIAL_PROVIDER");
		$query->filter("AG","TP.AGENT");
		$query->filter("CIF","TP.CIF");
		$query->filter("CODE","TP.ID");
		$query->filter("TEL","TP.PHONE");
		$query->filter("REGION","TP.REGION");
		$query->filter("PROVINCE","TP.PROVINCE")->like();
		$query->filter("NATURE","TP.NATURE");
		$query->filter("HOLDING","TP.HOLDING");
		$query->filter("CURRENCY","TP.HOLDING");
		$query->filter("SOURCE","TP.SOURCE");
		$query->filter("TP_TYPE")->manual(function($values){
			$rsp = array();
			foreach ($values as $val){
				switch($val){
					case "RP":
						array_push($rsp,array("field" => "TP.REAL_PROVIDER", "value" => "Y"));
						break;
					case "PP":
						array_push($rsp,array("field" => "TP.POTENTIAL_PROVIDER", "value" => "Y"));
						break;
					case "RC":
						array_push($rsp,array("field" => "TP.REAL_CUSTOMER", "value" => "Y"));
						break;
					case "PC":
						array_push($rsp,array("field" => "TP.POTENTIAL_CUSTOMER", "value" => "Y"));
						break;
					case "AG":
						array_push($rsp,array("field" => "TP.AGENT", "value" => "Y"));
						break;
				}
			}
			return $rsp;
		});
		$query->order("NAME","TP.NAME");
		return $query->paging();
	}
	
	/**
	 * Get Contact List by TP
	 * @param string $id TP_ID
	 * 
	 * @return Query
	 */
	public static function getContacts($id){
		$query = new Query();
		$query->
		select("ID","C.ID")->
		select("FULL_NAME", "(C.NAME ||' '|| C.SURNAME)")->
		select("CHARGE", "C.CHARGE")->
		select("WPHONE", "C.WORK_PHONE")->
		select("LANGUAGE", "(SELECT NAME FROM LANGUAGES WHERE CULTURE_CODE=C.LANGUAGE_ID)")->
		select("MEMAIL","(SELECT EMAIL FROM CONTACT_EMAILS CE WHERE CE.CT_ID=C.ID AND CE.PRIMARY='Y')");
		$query->from("CONTACT C");
		$query->where("C.TP_ID='$id'");
		$query->order("FULL_NAME", "(C.NAME ||' '|| C.SURNAME)");
		return $query->paging();
	}
		
	/**
	 * Get THIRD PARTY by ID.
	 * @param string $id TP_ID
	 * @return ThirdParty
	 */
	public static function getThirdParty($id = null){
		return new ThirdParty($id,true);
	}
	
	/**
	 * Add/Update Third Party
	 * 
	 * If $id is null -> new TP.
	 * 
	 * @param string $id TP_ID
	 * @param \stdClass $data
	 * @return boolean
	 */
	public static function setTP($id,$data){
		$tp = new ThirdParty($id);
		$tp->set($data);
		return $tp->save();
	}
	
	/**
	 * Delete third party.
	 * @param string $id TP_ID
	 * @return boolean
	 */
	public static function delTP($id){
		$tp = new ThirdParty($id);
		return $tp->delete();
	}
	
	/**
	 * Get list of banks on this TP.
	 * @param string $id TP_ID
	 * @return Query
	 */
	public static function getBanks($id){
		$query = new Query();
		$query->select("CODE")->
		select("ID")->
		select("IBAN")->
		select("MANDATE_SEPA")->
		select("OWNER");
		$query->from("THIRD_PARTY_BANKS TPB");
		$query->order("CODE","TPB.CODE");
		$query->where("TP_ID='$id'");
		return $query->paging();
	}
	
	/**
	 * Add new bank on TP.
	 * @param string $id TP_ID
	 * @param \stdClass $data
	 */
	public static function addBank($id,$data){
		$environment = Environment::getInstance();
		foreach($data as $prop => $value){
			$var[$prop] = $value;
		}
		$var["TP_ID"] = $id;
		$sql = "SELECT IFNULL[max(id)+1,0] AS NID FROM THIRD_PARTY_BANKS";
		$rs = $environment->dbcon->execute($sql);
		$rs->fetch();
		$nid = $rs->nid;
		$rs->close();
		$var["ID"] = $nid;
		$environment->dbcon->add("THIRD_PARTY_BANKS",$var);
	}
	
	/**
	 * Update bank on TP.
	 * @param string $id TP_ID
	 * @param string $bid BANK_ID
	 * @param \stdClass $data
	 */
	public static function updateBank($id,$bid,$data){
		$environment = Environment::getInstance();
		foreach($data as $prop => $value){
			$var[$prop] = $value;
		}
		$vw["ID"] = $bid;
		$environment->dbcon->update("THIRD_PARTY_BANKS", $var,"ID=:ID",$vw);
	}
	
	/**
	 * Delete a bank from a thirdparty given a ID or an array of objects. 
	 * 
	 * @param string $tp_id ThirdParty ID.
	 * @param mixed $bid
	 */
	public static function deleteBank($tp_id,$bid){
		$environment = Environment::getInstance ();
		if(is_object($bid)){ //Delete multiple bids
			$in = "";
			foreach($bid->bids as $bid){
				$in .= "'".$bid."',";
			}
			$in = rtrim($in,",");
			$var["TP_ID"] = $tp_id;
			$environment->dbcon->execute("DELETE FROM THIRD_PARTY_BANKS WHERE TP_ID=:TP_ID and ID IN ($in)",$var);
		}
		if(is_numeric($bid) || is_string(($bid))){ //Delete one bid
			$var["ID"] = $bid;
			$var["TP_ID"] = $tp_id;
			$environment->dbcon->execute("DELETE FROM THIRD_PARTY_BANKS WHERE TP_ID=:TP_ID AND ID=:ID",$var);
		}
	}
	
	/**
	 * Get documents for a third party
	 * @param string $id third_party_id
	 */
	public static function getDocuments($id){
		return FileManagement::getFiles($id,Table::THIRD_PARTY);
	}
	
	/**
	 * Upload Documents for a TP.
	 * @param string $id third_party_id
	 */
	public static function uploadDocuments($id = null,$data){
		return FileManagement::setBinary($id, 'thirdparty/'.$id, Table::THIRD_PARTY,$data);
	}
	
	/**
	 * Upload TP logo.
	 * @param string $id tpid
	 */
	public static function uploadLogo($id = null,$data){
		return FileManagement::setAvatar($id,Table::THIRD_PARTY,'LOGOTYPE',$data);
	}
}