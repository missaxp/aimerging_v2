<?php
namespace api\crud;

use query\Query;
use core\Environment;
use query\_OO;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;

/**
 * Class BanksManagement
 *
 * @author jballus
 * @namespace api\crud
 */
abstract class BanksManagement {

	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
		
	public static function getBanks(){
		$query = new Query();
	
		$query->select("ID", "F.ID")
		->select("NAME", "F.NAME")
		->select("BIC", "F.BIC");
		$query->from("BANKS F");
		$query->filter("ID","F.ID");
		$query->filter("NAME","F.NAME")->operator(_OO::LIKE);
		$query->order("NAME","F.NAME");
		return $query->paging();
	}
	
	
	public static function addBank($id = null, $data){
		self::$environment = Environment::getInstance();
	
		$rsp = array();
		$rsp["FormError"] = false;
		$rsp["ErrorMsg"] = "";
	
	
		$vars = array();
		foreach($data as $prop => $value){
			$vars[$prop] = $value;
	
			$lenValue = strlen($value);
	
			if ($prop == "id") $maxLength = 4;
			if ($prop == "name") $maxLength = 50;
			if ($prop == "bic") $maxLength = 20;
	
			if ($lenValue > $maxLength) throw new AppException(Status::S4_PreconditionFailed,ErrorCode::LengthError,"Field $prop maximum length error. Max : $maxLength. Length :  $lenValue.");
	
		}
	
		if (is_null ($id)) {	
			$rs = self::$environment->dbcon->execute("SELECT * FROM BANKS WHERE ID=:BANK_ID",array("BANK_ID" => $data->id));
			$fetch = $rs->getAll();
			$rs->close();
			if(count($fetch)==0){
				$rsp["id"] = $data->id;
				$vars = array("ID" => $data->id, "NAME" => $data->name, "BIC" => $data->bic);
				self::$environment->dbcon->add("BANKS", $vars);
			} else {
				$bank_Code = $fetch[0]['ID'];
				$bank_Name = $fetch[0]['NAME'];
				throw new AppException(Status::S4_PreconditionFailed, ErrorCode::NotAcceptable, "Bank  $bank_Name ( $bank_Code ) exist");
			}
				
		} else {
			$rsp["id"] = $id;
			$varsW["BANK_ID"] = $id;
			self::$environment->dbcon->update("BANKS", $vars,"ID=:BANK_ID",$varsW);
		}
	
		return $rsp;
	}
	
	
	
	
	public static function BankNotExist($id){
		self::$environment = Environment::getInstance();
		
		$rs = self::$environment->dbcon->execute("SELECT * FROM BANKS WHERE ID=:BANK_ID",array("BANK_ID" => $id));
		$fetch = $rs->getAll();
		$rs->close();
		if(count($fetch)==0){
			return true;
		} 	
		return false;		
	}
		
	public static function getBankById($id){
		self::$environment = Environment::getInstance();
		
		$rs = self::$environment->dbcon->execute("SELECT * FROM BANKS WHERE ID=:BANK_ID",array("BANK_ID" => $id));
		$fetch = $rs->getAll();
		$rs->close();
		if(count($fetch)==0){
			throw new AppException(Status::S4_NotFound,ErrorCode::NotFound,"Bank Resource $id not found");
		}
		$fetch = $fetch[0];
		return $fetch;
		
	}
	
	
	public static function deleteBankById($id){
		return Environment::getInstance()->dbcon->execute("DELETE FROM BANKS WHERE ID =:ID_BANK", array ("ID_BANK" => $id));
	}
	
	public static function deleteBanks($data){
		$environment = Environment::getInstance ();
					
		$totalItem = 0;
		$itemsDeleted = 0;
		$itemsNotDeleted = 0;
				
	
		foreach($data as $prop => $values){
			
			if ($prop == "arrayIds") {
				foreach($values as $value){					
					$num = 0;
					$totalItem++;
					$sql = "SELECT count(1) AS NUM FROM THIRD_PARTY_BANKS WHERE CODE=:ID_BANK";
					$rs = $environment->dbcon->execute ( $sql, array (
							"ID_BANK" => $value
					) );
					if ($rs->fetch ()) {
						$num = $rs->getVal ("NUM");
					}
					$rs->close ();

					if ($num > 0) {
						// No es pot borrar						
						$itemsNotDeleted++;
						$rsp["NoDeletedItemsCode"][] = $value;
						
					} else {
						// Borrar						
						try {
							$afectedRows = $environment->dbcon->execute ( "DELETE FROM BANKS WHERE ID=:ID_BANK", array (
									"ID_BANK" => $value
							) );
						} catch ( \Exception $e ) {
							throw new AppException ( Status::S5_InternalServerError, ErrorCode::DBQueryError, $e->getMessage () );
						}
						$itemsDeleted++;
						$rsp["DeletedItemsCode"][] = $value;						
					}															
				}				
			}			
		}
		$rsp["TotalItems"] = $totalItem;		
		$rsp["DeletedItems"] = $itemsDeleted;
		$rsp["NoDeletedItems"] = $itemsNotDeleted;
		
		return $rsp;
		
	}
}