<?php

/**
 * Role management class
 *
 * @author phidalgo & ctemporal
 * @namespace core\security
 */
namespace api\crud;

use core\Environment;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;
use query\Query;
use dataAccess\dao\TaskDAO;
use dataAccess\dao\SourceDAO;

abstract class AIManagement {
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;

	/**
	 * Tasca tipus TP (TRAD & REV)
	 */
	Const _TP = "TP";
	/**
	 * Tasca tipus Translation
	 */
	Const _TR = "Translation";
	
	/**
	 * Tasca tipus ProofReading
	 */
	Const _PR = "ProofReading";
	
	/**
	 * Tasca Tipus PostEditing
	 */
	Const _PE = "PostEditing";
	
	/**
	 * Tasca Tipus Miscellaneous
	 */
	Const _MIS = "Miscellaneous";
	
	/**
	 * Tasca Tipus Management
	 */
	Const _MNG = "Management";
	
	/**
	 * Tasca Tipus Engineering
	 */
	Const _ENG = "Engineering";
	
	/**
	 * Tasca Tipus Engineering
	 */
	Const _DTP = "DTP";
	
	/**
	 * Get all Process.
	 * Returns array 
	 *
	 * @access public
	 * @return array Query
	 * @throws AppException
	 */
	public static function _getProcess($paging = true, $defaultReturn = null) {
		$query = new Query(null, Environment::getInstance()->dbcon_AI);
		$query->defaultReturn($defaultReturn); //Set default return if any.
		
		$query->select("t.idProcess, t.processName as NAME, t.enabled, t.description as DESCRIPTION, t.creator, t.timeStamp, s.sourceName as SOURCE, t.priority");
		$query->select("tasksProcessed", "(SELECT count(1) From Task where idProcess=t.idProcess)");
		$query->filter("NAME","t.processName");
		$query->filter("SOURCE","s.idSource", self::getAISources());
		$query->order("TASKSPROCESSED","tasksProcessed");
		$query->order("PRIORITY","t.priority");
		//$query->order("TIMESTAMP","t.timeStamp");
		$query->from("Process t, Source s");
		$query->where("t.idSource=s.idSource");
		
		return ($paging? $query->paging():$query->execute());
	}

	public static function _getTMSProperties($paging = true, $defaultReturn = null){
		$properties = array();
		$env = Environment::getInstance();
		$sql = "SELECT * FROM AREAS ORDER BY CODI ASC";
		$q = $env->dbcon_webtraduc->execute($sql);
		while($q->fetch()){
			$properties['area'][] = array("id"=>$q->CODI, "name"=>$q->CODI);
			$properties['area2'][] = array("id"=>$q->CODI, "name"=>$q->CODI);
		}
		$q->close();

		// $properties['area2'] = $properties['area'];
		$sql = "SELECT * FROM GRUPS ORDER BY DESCRIPCIO ASC";
		$q = $env->dbcon_webtraduc->execute($sql);
		while($q->fetch()){
			$properties['id_grup'][] = array("id"=>$q->ID, "name"=>$q->DESCRIPCIO);
		}
		$q->close();

		$sql = "select codi_recurs,nom from dades_recurs where codi_recurs in (select codi from sygesrecursos where codi=codi_recurs) and prioritat<=6 order by codi_recurs asc";
		$q = $env->dbcon_webtraduc->execute($sql);
		while($q->fetch()){
			$properties['translator'][] = array("id"=>$q->codi_recurs, "name"=>$q->codi_recurs);
			$properties['proofer'][] = array("id"=>$q->codi_recurs, "name"=>$q->codi_recurs);
		}
		$q->close();

		$sql = "select codi,descripcio,area,a_prop_area from sygesprojectes where nivell='1' and obert='A' and departament like '%T%' order by codi asc";
		$q = $env->dbcon_webtraduc->execute($sql);
		while($q->fetch()){
			$properties['projecte'][] = array("id"=>$q->codi, "name"=>$q->descripcio);
		}
		$q->close();

		$sql = "select codi_recurs,nom from dades_recurs WHERE tipus_recurs in ('P','A','SA') and prioritat<9 order by codi_recurs asc";
		$q = $env->dbcon_webtraduc->execute($sql);
		while($q->fetch()){
			$properties['origin'][] = array("id"=>$q->codi_recurs, "name"=>$q->codi_recurs);
		}
		$q->close();

		$properties['tipus_tasca'][] = array("id"=> AIManagement::_DTP, "name" => AIManagement::_DTP);
		$properties['tipus_tasca'][] = array("id"=> AIManagement::_ENG, "name" => AIManagement::_ENG);
		$properties['tipus_tasca'][] = array("id"=> AIManagement::_MNG, "name" => AIManagement::_MNG);
		$properties['tipus_tasca'][] = array("id"=> AIManagement::_MIS, "name" => AIManagement::_MIS);
		$properties['tipus_tasca'][] = array("id"=> AIManagement::_PE, "name" => AIManagement::_PE);
		$properties['tipus_tasca'][] = array("id"=> AIManagement::_PR, "name" => AIManagement::_PR);
		$properties['tipus_tasca'][] = array("id"=> AIManagement::_TR, "name" => AIManagement::_TR);
		$properties['tipus_tasca'][] = array("id"=> AIManagement::_TP, "name" => AIManagement::_TP);

		$properties['tarifa'][] = array("id"=> "N", "name" => "N");
		$properties['tarifa'][] = array("id"=> "X", "name" => "X");


		$properties['estat'][] = array("id" => 1, "name" => "ASSIGNED");
		$properties['estat'][] = array("id" => 2, "name" => "READY");
		$properties['estat'][] = array("id" => 3, "name" => "WORKING");
		$properties['estat'][] = array("id" => 4, "name" => "DELIVERED");
		$properties['estat'][] = array("id" => 5, "name" => "RECEIVED");
		$properties['estat'][] = array("id" => 6, "name" => "CLOSED");

		$properties['folderType'][] = array("id" => 0, "name" => "NORMAL");
		$properties['folderType'][] = array("id" => 1, "name" => "ESPECIAL");

		$properties['inici_previst'][] = array("id"=> "startDate", "name" => "START DATE");


		return $properties;
	}
	
	public static function _getaProcess($pid){
		//$opt = array("timeout"=>30,"useragent"=>'IDISC_CONNECTOR',"connecttimeout"=>5);
		//$method = "GET";
		$url = "ai.idisc.es/api/v1/process/".$pid;
		//$this->auth = $auth;
		$ch = curl_init();
		
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_USERAGENT,"iDISC Connector");
		curl_setopt($ch,CURLOPT_TIMEOUT,"30");
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		$output = curl_exec($ch); //Enviem la consulta.
		
		$output = json_decode($output, true);
		
		//TODO Recuperar de AI.
		$taskProperties = array();
		$taskProperties[] = array("name" => "title", "vartype" => "string", "type" => "S");
		$taskProperties[] = array("name" => "dueDate", "vartype" => "date", "type" => "S");
		$taskProperties[] = array("name" => "sourceLanguage", "vartype" => "string", "type" => "S");
		$taskProperties[] = array("name" => "targetLanguage", "vartype" => "string", "type" => "S");
		$taskProperties[] = array("name" => "maxWords", "vartype" => "int", "type" => "S");
		$taskProperties[] = array("name" => "platformType", "vartype" => "string", "type" => "C");
		$taskProperties[] = array("name" => "contentType", "vartype" => "string", "type" => "C");
		
		$output["data"]["taskProperties"] = $taskProperties;
		
		
		return $output["data"];
	}
	
	/**
	 * Get all tasks.
	 *
	 * @access public
	 * @return array Query
	 * @throws AppException
	 */
	public static function _getTasks($paging = true, $defaultReturn = null) {
		$query = new Query(null, Environment::getInstance()->dbcon_AI);
		$query->defaultReturn($defaultReturn); //Set default return if any.
		
		//date_format(t.timeStamp,'%Y%m%d%H%i%sZ') as timeStamp
		$query->select("t.idClientTask, t.title, t.uniqueSourceId, t.uniqueTaskId, s.stateName, d.sourceName as SOURCE");
		$query->select("timeStamp", "date_format(t.timeStamp,'%Y%m%dT%H%i%sZ')");
		$query->select("dueDate", "date_format(t.dueDate,'%Y%m%dT%H%i%sZ')");
		$query->select("startDate", "date_format(t.startDate,'%Y%m%dT%H%i%sZ')");
		
	
		$query->filter("TITLE","t.title");
		$query->filter("STATE","t.idState",self::getAITaskStatus());
		$query->filter("SOURCE","d.idSource", self::getAISources());
		$query->filter("IDCLIENTTASK","t.idClientTask");
		$query->filter("UNIQUESOURCEID","t.uniqueSourceId");
		$query->order("timeStamp","t.timeStamp");
		$query->order("STATENAME","s.stateName");
		$query->from("Task t,TaskStates s, TaskSource d");
		$query->where("t.idState=s.idState");
		$query->where("t.uniqueSourceId=d.uniqueSourceId");
		
		return ($paging? $query->paging():$query->execute());
	}
	
	
	public static function _getaTask($usid){
		$tdao = new TaskDAO();
		
		return $tdao->getTask($usid)->toArray();
	}
	
	
	/**
	 * Return sources.
	 * @return array|\model\Source[]
	 * @return
	 */
	public static function _getSources(){
		$sdao = new SourceDAO();
		
		$rsp = array();
		
		$sources = $sdao->getSources();
		foreach($sources as $source){
			$rsp[] = array(
					"name" => $source->getSourceName(),
					"id" => $source->getIdSource()
			);
		}
		return $rsp;
	}
	
	/**
	 * Get all tasks.
	 *
	 * @access public
	 * @return array Query
	 * @throws AppException
	 */
	public static function _getErrors($paging = true, $defaultReturn = null) {
		$query = new Query(null, Environment::getInstance()->dbcon_AI);
		$query->defaultReturn($defaultReturn); //Set default return if any.
		
		$query->select("e.errorType as TYPE, e.description AS DESCRIPTION, e.errorDate AS EDATE, e.class AS CLASS");
		
		$query->filter("TYPE","e.errorType", self::getAIErrorType());
		$query->filter("EDATE","e.errorDate");
		$query->order("EDATE","e.errorDate");
		$query->order("TYPE","e.errorType");
		$query->from("ErrorLog e");
		
		
		return ($paging? $query->paging():$query->execute());
	}
	
	private static function getAIErrorType(){
		return array("RESULTS" => array(
				array("NAME" => "Warning", "ID" => "Warning"),
				array("NAME" => "Severe", "ID" => "Severe"),
				array("NAME" => "Error", "ID" => "Error")
		));
	}
	
	private static function getAISources(){
		$rsp = array();
		$rsp["RESULTS"] = array();
		$sql = "SELECT idSource, sourceName FROM Source order by priority";
		$q = Environment::getInstance()->dbcon_AI->execute($sql);
		while($q->fetch()){
			$rsp["RESULTS"][] = array("NAME" => $q->sourcename, "ID" => $q->idsource);
		}
		$q->close();
		
		return $rsp;
	}
	
	private static function getAITaskStatus(){
		return array("RESULTS" => array(
			array("NAME" => "COLLECTED", "ID" => 1),
			array("NAME" => "FILTERED", "ID" => 2),
			array("NAME" => "EXECUTINGACTIONS", "ID" => 3),
			array("NAME" => "PROCESSED", "ID" => 4),
			array("NAME" => "REJECTED", "ID" => 5),
			array("NAME" => "RETRY", "ID" => 6),
			array("NAME" => "PENDINGCONFIRMATION", "ID" => 7),
			array("NAME" => "PROCESSEDWITHWARNING", "ID" => 8),
			array("NAME" => "ERROR", "ID" => 9),
			array("NAME" => "CONFIRMED", "ID" => 10),
			array("NAME" => "REJECTEDBYCONFIRMTIMEOUT", "ID" => 11)
		));
	}
		
} /* end of abstract class RoleManagement */




?>