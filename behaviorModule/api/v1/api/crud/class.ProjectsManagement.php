<?php

/**
 * Role management class
 *
 * @author phidalgo & ctemporal
 * @namespace core\security
 */
namespace api\crud;

use core\Environment;
use core\Exception\AppException;
use query\Query;
use common\Table;

abstract class ProjectsManagement {
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	
	public static function checkCode($ccid){
		self::$environment = Environment::getInstance();
		$rs = self::$environment->dbcon->execute("SELECT CODE FROM PROJECTS WHERE CODE=:CCID",array("CCID" => $ccid));
		$fetch = $rs->getAll();
		$rs->close();
		if(count($fetch)==0){
			return true;
		} 	
		return false;		
	}
	
	/**
	 * Get all projects.
	 *
	 * @access public
	 * @return array Query
	 * @throws core\Exception\AppException
	 */
	public static function _get($paging = true,$defaultReturn = null) {
		$query = new Query();
		$query->defaultReturn($defaultReturn); //Set default return if any.
		
		$query->select("P.ID,P.FULL_CODE AS CODE,P.NAME");
		$query->select("CNAME","(SELECT NAME FROM THIRD_PARTY WHERE ID=P.TP_ID)");
		$query->select("FCNAME","(SELECT NAME FROM THIRD_PARTY WHERE ID=P.FINAL_CLIENT)");
		$query->select("CREATED","P.CREATION_TS");
			
		$query->filter("NAME","P.NAME");
		$query->filter("CODE","P.CODE");
		$query->filter("ID","P.ID");
		$query->from("PROJECTS P");
		$query->order("NAME", "P.NAME");
		$query->where("ROOT_ID is null");
		
		if($paging){
			return $query->paging();
		}
		else{
			return $query->execute();
		}
	}
	
	/**
	 * Get Project options.
	 */
	public static function getOptions(){
		return new Project();
	}
	
	/**
	 * Get project by id
	 * @param integer $pid
	 */
	public static function _getById($pid){
		return new Project($pid);
	}
	
	/**
	 * Add new project
	 * @param \stdClass $data
	 */
	public static function _new($data){
		$p = new Project();
		return $p->set($data)->save();
	}
	
	/**
	 * Update project
	 * @param integer $pid
	 * @param \stdClass $data
	 */
	public static function _update($pid,$data){
		$p = new Project($pid);
		return $p->set($data)->save();
	}
	
	public static function _delete($pid){
		$p = new Project($pid);
		return $p->delete();
	}
	
	/**
	 * List THIRD PARTY resources which are CUSTOMERS.
	 *
	 * @return Query
	 */
	public static function getCustomers($q) {
		$env = Environment::getInstance();
		$q = strtoupper($q);
		$rsp = array();
		$vars = array();
		$sql = "SELECT TP.ID,TP.NAME,TP.PHONE,TP.AVATAR
		FROM THIRD_PARTY TP WHERE upper(TP.NAME) LIKE '%'||:Q||'%' OR TP.ID LIKE '%'||:Q||'%' AND TP.ENABLED='Y' AND TP.REAL_CUSTOMER='Y'";
		
		if($env->user->user_department!=null){
			$sql .= " AND DEPARTMENT_ID=:DID";
			$vars["DID"] = $env->user->user_department;
		}
		$vars["q"] = $q;
		$rs = $env->dbcon->execute($sql,$vars);
		while($rs->fetch()){
			$avatar = $rs->avatar;
			$tp = array();
			$tp["id"] = $rs->id;
			$tp["name"] = $rs->name;
			$tp["phone"] = $rs->phone;
			if($avatar==""){
				$tp["avatar"] = null;
			}
			else{
				$tp["avatar"] = base64_encode($avatar);
			}
			$rsp[] = $tp;
		}
		$rs->close();
		
		return $rsp;
	}
	
	/**
	 * List THIRD PARTY resources which are CUSTOMERS.
	 *
	 * @return Query
	 */
	public static function getThirdParty($q) {
		$env = Environment::getInstance();
		$q = strtoupper($q);
		$rsp = array();
		$vars = array();
		$sql = "SELECT TP.ID,TP.NAME,TP.PHONE,TP.AVATAR
		FROM THIRD_PARTY TP WHERE upper(TP.NAME) LIKE '%'||:Q||'%' OR TP.ID LIKE '%'||:Q||'%' AND TP.ENABLED='Y'";
		
		if($env->user->user_department!=null){
			$sql .= " AND DEPARTMENT_ID=:DID";
			$vars["DID"] = $env->user->user_department;
		}
		$vars["q"] = $q;
		$rs = $env->dbcon->execute($sql,$vars);
		while($rs->fetch()){
			$avatar = $rs->avatar;
			$tp = array();
			$tp["id"] = $rs->id;
			$tp["name"] = $rs->name;
			$tp["phone"] = $rs->phone;
			if($avatar==""){
				$tp["avatar"] = null;
			}
			else{
				$tp["avatar"] = base64_encode($avatar);
			}
			$rsp[] = $tp;
		}
		$rs->close();
		
		return $rsp;
	}
	
	public static function getTPProducts($tpid){
		$env = Environment::getInstance();
		$rs = $env->dbcon->execute("SELECT NAME FROM THIRD_PARTY_PRODUCTS WHERE TP_ID=:TPID",array("TPID" => $tpid));
		$prod = $rs->getAll();
		$rs->close();
		return $prod;
	}
	
	public static function searchContact($q = null){
		$env = Environment::getInstance();
		$q = strtoupper($q);
		$rsp = array();
		$vars = array();
		$sql = "SELECT ID,NAME,SURNAME,AVATAR,WORK_PHONE,(SELECT EMAIL FROM CONTACT_EMAILS WHERE CT_ID=C.ID) AS PEMAIL 
		FROM CONTACT C WHERE upper(C.NAME)||' '||upper(C.SURNAME) LIKE '%'||:Q||'%'";
		if($env->user->user_department!=null){
			$sql .= " AND (C.DEPARTMENT_ID=:DID OR C.DEPARTMENT_ID IS NULL)";
			$vars["DID"] = $env->user->user_department;
		}
		
		//die($sql);
		$vars["q"] = $q;
		$rs = $env->dbcon->execute($sql,$vars);
		while($rs->fetch()){
			$avatar = $rs->avatar;
			$ct = array();
			$ct["id"] = $rs->id;
			$ct["name"] = (($rs->surname!="")? $rs->surname.', '.$rs->name:$rs->name);
			if($avatar==""){
				$ct["avatar"] = null;
			}
			else{
				$ct["avatar"] = base64_encode($avatar);
			}
			$ct["wphone"] = $rs->work_phone;
			$ct["email"] = $rs->pemail;
			$rsp[] = $ct;
		}
		$rs->close();
		
		return $rsp;
	}
	
	/**
	 * Get documents for a third party
	 * @param string $id third_party_id
	 */
	public static function getDocuments($id){
		return FileManagement::getFiles($id,Table::PROJECTS);
	}
	
	/**
	 * Upload Documents for a TP.
	 * @param string $id third_party_id
	 */
	public static function uploadDocuments($id = null,$data){
		return FileManagement::setBinary($id, 'projects/'.$id, Table::PROJECTS,$data);
	}
}
/* end of abstract class ProjectsManagement */
?>