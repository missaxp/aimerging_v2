<?php
namespace api\crud;

use query\Query;
use core\Environment;
use query\_OO;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;
use core\Exception\OracleException;

/**
 * Class LanguagesManagement
 *
 * @author phidalgo
 * @namespace api\crud
 */
abstract class LanguagesManagement {

	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
		
	public static function getLanguages($paging = true,$defaultReturn = null,$gps = true){
		$query = new Query();
		$query->defaultReturn($defaultReturn); //Set default return if any.
		$query->gps($gps);
		
		$query->select("CODE","L.CULTURE_CODE")
		->select("NAME","L.NAME")
		->select("ISO","L.ISO_639");
		$query->from("LANGUAGES L");
		$query->filter("NAME","L.NAME")->operator(_OO::LIKE);
		$query->filter("CODE","L.CULTURE_CODE");
		$query->order("NAME","L.NAME");
		return ($paging? $query->paging():$query->execute());
	}
	
	public static function addLanguage($id = null, $data){
		self::$environment = Environment::getInstance();

		$rsp = array();		
		$rsp["FormError"] = false;
		$rsp["ErrorMsg"] = "";
		
		
		$vars = array();		
		foreach($data as $prop => $value){
			$vars[$prop] = $value;
		
			$lenValue = strlen($value);
		
			if ($prop == "culture_code") $maxLength = 8;
			if ($prop == "name") $maxLength = 100;
			if ($prop == "iso_639") $maxLength = 3;
		
			if ($lenValue > $maxLength) throw new AppException(Status::S4_PreconditionFailed,ErrorCode::LengthError,"Field $prop maximum length error. Max : $maxLength. Length :  $lenValue.");
		
		}
	
		if (is_null ($id)) {
			$rs = self::$environment->dbcon->execute("SELECT * FROM LANGUAGES WHERE CULTURE_CODE=:CCODE",array("CCODE" => $data->culture_code));
			$fetch = $rs->getAll();
			$rs->close();					
			if(count($fetch)==0){			
				$rsp["id"] = $data->culture_code;
				$vars = array("CULTURE_CODE" => $data->culture_code, "NAME" => $data->name, "ISO_639" => $data->iso_639);
				self::$environment->dbcon->add("LANGUAGES", $vars);							
			} else {
				$cCode = $fetch[0]['CULTURE_CODE'];
				$cName = $fetch[0]['NAME'];				
				throw new AppException(Status::S4_PreconditionFailed, ErrorCode::NotAcceptable, "Language  $cName ( $cCode ) exist");								
			}			
			
		} else {
			$rsp["id"] = $id;
			$varsW["CULTURE_CODE"] = $id;
			self::$environment->dbcon->update("LANGUAGES", $vars,"CULTURE_CODE=:CULTURE_CODE",$varsW);
		}	
				
		return $rsp;			
	}
	
	public static function languagesNotExist($id){
		self::$environment = Environment::getInstance();
		
		$rs = self::$environment->dbcon->execute("SELECT * FROM LANGUAGES WHERE CULTURE_CODE=:CCODE",array("CCODE" => $id));
		$fetch = $rs->getAll();
		$rs->close();
		if(count($fetch)==0){
			return true;
		} 	
		return false;		
	}
		
	public static function getLanguageById($id){
		self::$environment = Environment::getInstance();
		
		$rs = self::$environment->dbcon->execute("SELECT * FROM LANGUAGES WHERE CULTURE_CODE=:CCODE",array("CCODE" => $id));
		$fetch = $rs->getAll();
		$rs->close();
		if(count($fetch)==0){
			throw new AppException(Status::S4_NotFound,ErrorCode::NotFound,"Language Resource $id not found");
		}
		$fetch = $fetch[0];
		return $fetch;
		
	}
	
	
	public static function deleteLanguageById($id){
		$environment = Environment::getInstance ();
		$afectedRows = $environment->dbcon->execute ( "DELETE FROM LANGUAGES WHERE CULTURE_CODE=:CULTURE_CODE", array (	"CULTURE_CODE" => $id));
	}
	
	public static function deleteLanguages($data){
		$environment = Environment::getInstance ();
					
		$totalItem = 0;
		$itemsDeleted = 0;
		$itemsNotDeleted = 0;
				
	
		foreach($data as $prop => $values){
			if ($prop == "arrayIds") {
				foreach($values as $value){					
					$num = 0;
					$totalItem++;
					// Delete
					try{
						$afectedRows = $environment->dbcon->execute("DELETE FROM LANGUAGES WHERE CULTURE_CODE=:CC", array ("CC" => $value));
						$itemsDeleted++;
						$rsp["DeletedItemsCode"][] = $value;
					}
					catch(OracleException $e){
						// Can't Delete
						$itemsNotDeleted++;
						$rsp["NoDeletedItemsCode"][] = array(
								"value" => $value,
								"tracking" => $e->db_tracking
								);
						//die(var_dump($e->db_tracking));
					}
					
					
					
				}				
			}			
		}
		$rsp["TotalItems"] = $totalItem;		
		$rsp["DeletedItems"] = $itemsDeleted;
		$rsp["NoDeletedItems"] = $itemsNotDeleted;
		
		return $rsp;
		
	}
		
}