<?php

/**
 * Role management class
 *
 * @author phidalgo & ctemporal
 * @namespace core\security
 */
namespace api\crud;


use core\Environment;
use core\AI;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;
use dataAccess\dao\TaskDAO;

abstract class CoreManagement {
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	public static function downloadFile($params){
		$finalUrl = ((isset($params["url"]) && trim($params["url"])!="")? trim($params["url"]):null);
		
		$fileName = ((isset($params["fileName"]) && trim($params["fileName"])!="")? trim($params["fileName"]):null);
		
		$downloadPath = ((isset($params["downloadPath"]) && trim($params["downloadPath"])!="")? trim($params["downloadPath"]):null);
		
		if($finalUrl==null || $fileName==null || $downloadPath==null){
			throw new AppException(Status::S4_PreconditionFailed,ErrorCode::PreconditionFailed);
		}
		
		$params["timeStamp"] = date('Y/m/d H:i:s');
		
		$asyncHash = sha1(json_encode($params));
		
		unset($params["timeStamp"]);
		
		ob_end_clean();
		// Buffer all upcoming output...
		ob_start();
		
		// Send your response.
		header('Content-Type: application/json');
		echo json_encode(array("success" => true, "data" => array("message" => "Successful request to download file.", "asyncHash" => $asyncHash)));
		
		// Get the size of the output.
		$size = ob_get_length();
		
		// Disable compression (in case content length is compressed).
		header("Content-Encoding: none");
		
		// Set the content length of the response.
		header("Content-Length: {$size}");
		
		header('HTTP/1.1 202 Accepted');
		
		// Close the connection.
		header("Connection: close");
		
		// Flush all output.
		ob_end_flush();
		ob_flush();
		flush();
		
		
		
		$allowedMethods = array("GET", "POST");
		$defaultMethod = "GET";
		if(in_array(strtoupper($params["method"]), $allowedMethods)){
			$defaultMethod = strtoupper($params["method"]);
		}
		
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $defaultMethod);
		curl_setopt($ch, CURLOPT_URL, $finalUrl);

		
		if(isset($params["headers"])  && is_array($params["headers"]) && count($params["headers"])>0){
			curl_setopt($ch, CURLOPT_HTTPHEADER, $params["headers"]);
		}
		
		if($defaultMethod!="GET" && isset($params["body"]) && is_array($params["body"]) && count($params["body"])>0){
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params["body"]));
		}
		
		
		$filePath = AI::getInstance()->getSetup()->repository.$downloadPath;
		if (! file_exists($filePath)) {
			@mkdir($filePath, 0760, true);
		}
		
		$newFile = fopen($filePath.$fileName, "w+");
		curl_setopt($ch, CURLOPT_FILE, $newFile);
		
		
		curl_exec($ch);
		fclose($newFile);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		
		
		$curlError = null;
		if(curl_errno($ch)!==0){
			$curlError = curl_error($ch);
		}
		
		curl_close($ch);
		$taskDAO = new TaskDAO();
		if($httpCode!=200){
			$taskDAO->updateTaskFileAsyncDownload(array(
					"asyncHash" => $asyncHash,
					"downloadPath" => $finalUrl,
					"isDownloading" => false,
					"isDownloaded" => false,
					"filePath" => $filePath.$fileName
			));
			exit();
		}else{
			$taskDAO->updateTaskFileAsyncDownload(array(
					"asyncHash" => $asyncHash,
					"downloadPath" => $finalUrl,
					"isDownloading" => false,
					"isDownloaded" => file_exists($filePath.$fileName),
					"filePath" => $filePath.$fileName
			));
		}

		/**
		 * En BM antes de recoger todas las tareas en COLLECTED o RETRY deberemos buscar aquellas tareas en COLLECTING.
		 * De aquellas tareas en COLLECTING deberemos comprobar si todos los ficheros se han descargado correctamente. Hay que comprobar que se ha descargado correctamente y que además 
		 * NO se está descargando sino que YA se ha descargado.
		 * Si todos los ficheros de la tarea en COLLECTING se han descargado, actualizar el estado a COLLECTED.
		 */
	
		
		exit();
	}
	
} /* end of abstract class CoreManagement */




?>