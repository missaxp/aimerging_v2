<?php
namespace api\crud;

use core\Environment;
use core\Exception\AppException;
use common\Table;
use core\resources\Resource;
use core\http\Status;
use core\http\ErrorCode;

/**
 * Class Field
 * @author phidalgo
 * @created 20160513
 * @updated 
 */
class Field extends Resource{
	private static $tarifableTypes = array("c" => "checkbox","sl" => "static-list","dl" => "dynamic-list");
	private static $noTarifableTypes = array("c" => "checkbox","sl" => "static-list","dl" => "dynamic-list","t"=> "text","rt" => "rich-text","fu" => "file-upload");
	
	public $tarifable;
	public $type;
	public $name;
	public $table_name;
	public $table_id;
	public $table_text;
	public $labels;
	public $staticList = array();
	
	public static function getTableCols(){
		$rsp = array();
		//return self::$environment->tables->_get();
		foreach(self::$environment->tables->_get() as $table){
			$t = array();
			$t["name"] = $table->name;
			$t["db_name"] = $table->db_name;
			$t["columns"] = array();
			foreach($table->getColumn() as $col){
				$t["columns"][] = $col->name;
			}
			
			$rsp[] = $t;
		}
		return $rsp;
	}

	/**
	 * @param integer $id
	 * @param boolean $loadAll
	 * @throws AppException
	 */
	public function __construct($id = null,$loadBasic = false){
		self::$environment = Environment::getInstance ();
		if($id!=null){
			$rs = self::$environment->dbcon->execute("SELECT * FROM FIELDS WHERE ID=:ID",array("ID" => $id));
			if($rs->fetch()){
				$this->id = $rs->id;
				$this->tarifable = ($rs->tarifable=="Y"? true:false);
				$this->type = $rs->type;
				$this->department_id = $rs->department_id;
				$this->table_name = $rs->table_name;
				$this->table_id = $rs->table_id;
				$this->table_text = $rs->table_text;
				$this->name = $rs->name;
			}
			$rs->close();
			
			if ($this->id==null) {
				throw new AppException ( Status::S4_NotFound, ErrorCode::NotFound, "Error FIELD NOT FOUND. var \$id: $id" );
			}
			
			if(strtoupper($this->type)=="SL"){
				$rs = self::$environment->dbcon->execute("SELECT * FROM FIELDS_VALUES WHERE F_ID=:FID ORDER BY SORT ASC",array("FID" => $this->id));
				$this->staticList = $rs->getAll();
				$rs->close();
			}
			
			$rs = self::$environment->dbcon->execute("SELECT * FROM FIELDS_LANGS WHERE F_ID=:FID",array("FID" => $this->id));
			while($rs->fetch()){
				$this->labels[$rs->language] = $rs->text;
			}
			$rs->close();
			
			$this->new = false; //Sets new property to false. Is a get/set operation.
		}
		else{
			$this->new = true;
		}
		
		parent::__construct(Table::FIELDS);
		if(!$loadBasic){
			$this->getOptions();
		}
	}
	
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \core\resources\Resource::set()
	 */
	public function set($data){
		foreach($data as $prop => $value){
			if(startsWith($prop,"label_")){
				$t = new \stdClass();
				$t->lang = str_replace("label_","",$prop);
				$t->text = $value;
				$this->toUpdate->translations[] = $t;
				unset($data->$prop);
			}
		}
		
		return parent::set($data);
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \core\resources\Resource::save()
	 */
	public function save(){
		$translations = array();
		if(isset($this->toUpdate->translations)){
			$translations = $this->toUpdate->translations;
			unset($this->toUpdate->translations);
		}
		
		$staticList = array();
		if(isset($this->toUpdate->staticList)){
			$staticList = $this->toUpdate->staticList;
			unset($this->toUpdate->staticList);
		}
		
		if($this->new && !isset($this->toUpdate->tarifable)){
			$this->toUpdate->tarifable = 'N';
		}
		
		if(!$this->new && isset($this->toUpdate->type) && strtoupper($this->toUpdate->type)!="SL"){ //Si no és llista estàtica, ens petem els valors de la llista estàtica.
			self::$environment->dbcon->execute("DELETE FROM FIELDS_VALUES WHERE F_ID=:FID",array("FID" => $this->id));
		}
		
		if(!$this->new && isset($this->toUpdate->type) && strtoupper($this->toUpdate->type)!="DL"){ //Si no és llista estàtica, ens petem els valors de la llista estàtica.
			$sql = "UPDATE FIELDS SET TABLE_NAME=null,TABLE_ID=null,TABLE_TEXT=null WHERE ID=:FID";
			self::$environment->dbcon->execute($sql,array("FID" => $this->id));
		}
		
		parent::save();
		
		if($this->new && count($translations)!=count(self::$environment->languages)){
			$tb = array();
			foreach (self::$environment->languages as $lang){
				$found = false;
				foreach($translations as $t){
					if($t->lang==$lang["id"]){
						$found = true;
					}
				}
				if(!$found){
					$t2 = new \stdClass();
					$t2->lang = $lang["id"];
					$t2->text = $this->name;
					$tb[] = $t2;
				}
				else{
					$tb[] = $t;
				}
			}
			$translations = $tb;
			unset($tb);
		}
		
		if(count($translations)>0){
			foreach($translations as $t){
				$rs = self::$environment->dbcon->execute("SELECT * FROM FIELDS_LANGS WHERE F_ID=:FID AND LANGUAGE=:LANG",array("FID" => $this->id,"LANG" => $t->lang));
				$text = ($t->text==""? $this->name:$t->text);
				if($rs->fetch()){ //update
					$vw["FID"] = $this->id;
					$vw["LANG"] = $t->lang;
					$var["TEXT"] = $text;
					self::$environment->dbcon->update("FIELDS_LANGS",$var,"F_ID=:FID AND LANGUAGE=:LANG",$vw);
				}
				else{ //Insert
					$var["F_ID"] = $this->id;
					$var["LANGUAGE"] = $t->lang;
					$var["TEXT"] = $text;
					self::$environment->dbcon->add("FIELDS_LANGS",$var);
				}
			}
		}
		
		if(count($staticList)>0){
			if(!$this->new){ //Si no és nou recurs...
				self::$environment->dbcon->execute("DELETE FROM FIELDS_VALUES WHERE F_ID=:FID",array("FID" => $this->id));
			}
			$sort = 1;
			foreach($staticList as $element){
				$sql = "INSERT INTO FIELDS_VALUES (F_ID,VALUE,SORT) VALUES(:FID,:VALUE,:SORT)";
				$vars["FID"] = $this->id;
				$vars["VALUE"] = $element->value;
				$vars["SORT"] = $sort;
				self::$environment->dbcon->execute($sql,$vars);
				$sort++;
			}
		}
		
		if($this->new){
			return array("ID" => $this->id);
		}
	}
	
	
	
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \core\resources\Resource::getOptions()
	 */
	public function getOptions(){
		parent::getOptions();
		$this->options["tables"] = self::getTableCols();
		$this->options["types"]["tarifable"] = self::$tarifableTypes;
		$this->options["types"]["notarifable"] = self::$noTarifableTypes;
		
	}
	
}