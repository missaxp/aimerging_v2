<?php

namespace api\crud;

use file\fileHandler;
use core\Exception\AppException;
use core\Environment;
use core\http\Status;
use core\http\ErrorCode;
use common\Tables;

/**
 * Class File
 * 
 * Provides a basic interaction with files.
 * 
 * This class is currently in development, some change may be added.
 *
 * @access public
 * @author phidalgo
 * @created 20162801
 * @namespace api\crud
 */
class File {
	
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	public $id = null;
	public $id_rel;
	public $name;
	public $normalized_name;
	public $path;
	public $md5;
	public $hdd;
	public $is_uploading;
	public $upload_finish;
	public $error;
	public $creation_ts;
	public $modification_ts;
	public $user_id;
	public $table_id;
	public $file_size;
	public $type;
	public $comments;
	private $temporary;
	
	public static function checkIfFileExist($fid){
		$file_path = null;
		self::$environment = Environment::getInstance ();
		$rs = self::$environment->dbcon->execute("SELECT PATH FROM MASTER_FILE WHERE ID=:ID",array("ID" => $fid));
		if($rs->fetch()){
			$file_path = $rs->path;
		}
		$rs->close();
		
		if($file_path==null){
			return false;
		}
		
		if(file_exists($file_path)){
			return true;
		}
		return false;
		
	}
	
	
	public function __construct($fid = null,$temporary = false) {
		self::$environment = Environment::getInstance ();
		
		//if file id is null, new file is ready to be upload.
		if($fid==null){
			return;
		}
		
		$this->temporary = $temporary;
		
		if(!$this->temporary){
			if (!is_numeric ($fid)){
				throw new AppException ( Status::S4_PreconditionFailed, ErrorCode::FILE_DEFAULT_ERROR );
			}
			$this->id = $fid;
			$sql = "SELECT * FROM MASTER_FILE WHERE ID=:ID";
			$rs = self::$environment->dbcon->execute ( $sql, array("ID" => $this->id));
		
			if(!$rs->fetch()){
				throw new AppException ( Status::S4_NotFound, ErrorCode::NotFound, "Error File $this->id not found." );
			}
			$this->id = $rs->id;
			$this->id_rel = $rs->id_rel;
			$this->name = $rs->name;
			$this->normalized_name = $rs->normalized_name;
			$this->path = $rs->path;
			$this->md5 = $rs->md5;
			$this->hdd = $rs->hdd;
			$this->is_uploading = $rs->is_uploading;
			$this->upload_finish = $rs->upload_finish;
			$this->error = $rs->error;
			$this->creation_ts = $rs->creation_ts;
			$this->modification_ts = $rs->modification_ts;
			$this->user_id = $rs->user_id;
			$this->table_id = $rs->table_id;
			$this->file_size = $rs->file_size;
			$this->type = $rs->type;
			$this->comments = $rs->comments;
			$rs->close ();
		}
		else{
			$this->path = self::$environment->tmpDir.$fid;
			$this->name = $fid;
			if(!file_exists($this->path)){
				throw new AppException(Status::S5_InternalServerError,ErrorCode::NotFound,"The temporary file you have requested is not found");
			}
		}
		
	}
	
	/**
	 * Updates file comments
	 * @param string $comment
	 */
	public function updateComment($comment){
		$vars["COMMENTS"] = $comment;
		$vars["MODIFICATION_TS"] = self::$environment->getDate();
		$varsW["ID"] = $this->id;
		self::$environment->dbcon->update('MASTER_FILE',$vars,"ID=:ID",$varsW);
	}
	
	/**
	 * Returns binary.
	 * If this temporary file, removes file after flush.
	 * //TODO:: S'haurà de fer un cron per eliminar els fitxers temporals amb 5 minuts o més desde la data de creació, per exemple.
	 * @throws AppException
	 */
	public function getBinary(){
		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename='.$this->name);
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Pragma: public');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Content-Length: ' . filesize($this->path));
		header('Content-Type: ' . mime_content_type($this->path));
		if ($this->hdd=="Y" || $this->temporary){
			ini_set('output_buffering', 0);
			ini_set('zlib.output_compression', 0);
			if (ob_get_level()) ob_end_clean();
			ob_end_flush();
			readfile($this->path);
		}
		else{
			throw new AppException(Status::S5_InternalServerError);
			//$contents = "FILE IS ALWAYS IN HDD. SAVING ON DB IS NOT YET DEVELOPED";
		}
		if($this->temporary){
			@unlink($this->path);
		}
		die();
	}
	
	public function getAvatar($id_rel,$table_id,$dbfield){
		$tinfo = self::$environment->tables->_get($table_id);
		
		$sql = "SELECT $dbfield FROM $tinfo->db_name WHERE $tinfo->primary_key[0]=:ID_REL";
		$rs = self::$environment->dbcon->execute($sql,array("ID_REL" => $id_rel));
		if(!$rs->fetch()){
			
		}
		$binary = $rs->$dbfield;		
		$rs->close();
		
		$finfo = new \finfo(FILEINFO_MIME);
		$b64 = base64_encode($binary);
		return array(
			"base64" => $b64,
			"mime" => $finfo->buffer($binary)
		);
		
	}
	
	/**
	 * Adds binary
	 * @param integer $id_rel
	 * @param string $user_dir
	 * @param integer $source
	 * @param \stdClass $data
	 * @param callable $callback
	 */
	public function setBinary($id_rel,$user_dir,$source,$data,$callback = null){
		require($_SERVER["DOCUMENT_ROOT"].'/v1/libs/class.fileHandler.php');
		$fhandler = new fileHandler(array(
				"access_control_allow_headers" => array(),
				"access_control_allow_methods" => array(),
				"access_control_allow_origin" => '',
				"main" => true, //If we set and tracked on MASTER_FILE TABLE or not
				"upload_dir" => self::$environment->documents,
				"user_dirs" => $user_dir,
				"element" => $id_rel,
				"source" => $source,
				"track_id" => (isset($data->fid)? $data->fid:null),
				"chunk_size" => (isset($data->chunk_size)? $data->chunk_size:null),
				"uploaddone" => $callback
		));
		return $fhandler->getResponse();
	}
	
	/**
	 * Adds avatar.
	 * Mostly same as setBinary but it tracks and stores in a different way.
	 * 
	 * @param string $id_rel
	 * @param string $source
	 * @param string $dbfield
	 * @param \stdClass $data
	 * @param callable $callback
	 */
	public function setAvatar($id_rel = null,$source,$dbfield,$data,$callback = null){
		require($_SERVER["DOCUMENT_ROOT"].'/v1/libs/class.fileHandler.php');
		$fhandler = new fileHandler(array(
				"access_control_allow_headers" => array(),
				"access_control_allow_methods" => array(),
				"access_control_allow_origin" => '',
				"main" => false, //If we set and tracked on MASTER_FILE TABLE or not
				"upload_dir" => self::$environment->tmpDir,
				"element" => $id_rel,
				"source" => $source,
				"dbfield" => $dbfield,
				"param_name" => 'avatar',
				"chunk_size" => (isset($data->chunk_size)? $data->chunk_size:null),
				"uploaddone" => $callback
		));
		return $fhandler->getResponse();
	}
	
	public function delete(){
		self::$environment->dbcon->execute("DELETE FROM MASTER_FILE WHERE ID=:ID",array("ID" => $this->id));
		if(file_exists($this->path)){
			@unlink($this->path);
		}
	}

	/**
	 * Set file relation
	 * TODO : Cal també moure fisicament el fitxer, i actualitzar la ruta.
	 * @param string $id_rel
	 */
	public function setFileRelation($id_rel){
		//Ara hem de moure físicament el fitxer.
		$sql = "SELECT * FROM MASTER_FILE WHERE ID=:ID";
		$rs = self::$environment->dbcon->execute($sql,array("ID" => $this->id));
		if(!$rs->fetch()){
			throw new AppException(Status::S5_InternalServerError,ErrorCode::T_NOT_FOUND);
		}
		
		$initial_file_path = $rs->path;
		$tname = self::$environment->tables->_get($rs->table_id)->name; //Table name
		$rs->close();
		$element_folder = self::$environment->documents."/".$tname."/".$id_rel; //Element folder /table_name_folder/element_id_fodler
		$final_file_path = $element_folder."/".basename($initial_file_path);
		
		if(!file_exists($element_folder)){
			makedir($element_folder, 0755, true);
		}
		rename($initial_file_path,$final_file_path);
		
		
		$vars["ID_REL"] = $id_rel;
		$vars["PATH"] = $final_file_path;
		$vW["ID"] = $this->id;
		self::$environment->dbcon->update("MASTER_FILE",$vars,"ID=:ID",$vW);
		$this->id_rel = $id_rel;
		
		return true;
	}
}

/* end of class File */

?>