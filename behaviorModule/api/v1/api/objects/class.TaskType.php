<?php
namespace api\crud;

use core\Environment;
use core\Exception\AppException;
use common\Table;
use core\resources\Resource;
use core\http\Status;
use core\http\ErrorCode;

/**
 * Class TaskType
 * @author phidalgo
 * @created 20160513
 * @updated 
 */
class TaskType extends Resource{
	
	public $auto_imputation;
	public $full_grid;
	public $resource_mandatory;
	public $name;
	
	protected $status;
	public $fields;
	public $events;
	public $concepts;
	/**
	 * @param integer $id
	 * @param boolean $loadAll
	 * @throws AppException
	 */
	public function __construct($id = null,$loadBasic = false){
		self::$environment = Environment::getInstance ();
		if($id!=null){
			$rs = self::$environment->dbcon->execute("SELECT * FROM TASK_TYPE WHERE ID=:ID",array("ID" => $id));
			if($rs->fetch()){
				$this->id = $rs->id;
				$this->auto_imputation = ($rs->auto_imputation=="Y"? true:false);
				$this->full_grid = ($rs->full_grid=="Y"? true:false);
				$this->resource_mandatory = ($rs->resource_mandatory=="Y"? true:false);
				$this->name = $rs->name;
				$this->department_id = $rs->department_id;
			}
			$rs->close();
			
			if ($this->id==null) {
				throw new AppException ( Status::S4_NotFound, ErrorCode::NotFound, "Error FIELD NOT FOUND. var \$id: $id" );
			}
			
			$rs = self::$environment->dbcon->execute("SELECT F_ID as FID FROM TASK_TYPE_FIELDS WHERE TT_ID=:TTID ORDER BY SORT ASC",array("TTID" => $this->id));
			$this->fields = $rs->getAll();
			$rs->close();
			
			$rs = self::$environment->dbcon->execute("SELECT * FROM TASK_TYPE_EVENTS WHERE TT_ID=:TTID ORDER BY SORT ASC",array("TTID" => $this->id));
			while($rs->fetch()){
				$e = new \stdClass();
				$e->init_status = $rs->init_status;
				$e->end_status = $rs->end_status;
				$e->event = $rs->te_id;
				$e->id = $rs->id;
				$this->events[] = $e;
			}
			$rs->close();
			
			$rs = self::$environment->dbcon->execute("SELECT * FROM TASK_TYPE_CONCEPTS WHERE TT_ID=:TTID ORDER BY SORT ASC",array("TTID" => $this->id));
			while($rs->fetch()){
				$p = $rs->percentage;
				$c = new \stdClass();
				$c->computed = $rs->computed;
				$c->percentage = startsWith($p,".")? "0".$p:$p;
				$c->tc_id = $rs->tc_id;
				$c->tcc_id = $rs->tcc_id;
				$this->concepts[] = $c;
			}
			$rs->close();
			
			$this->new = false; //Sets new property to false. Is a get/set operation.
		}
		else{
			$this->new = true;
		}
		
		parent::__construct(Table::TASKTYPE);
		if(!$loadBasic){
			$this->getOptions();
		}
		
		$rs = self::$environment->dbcon->execute("select * from TASK_STATUS order by id asc");
		$this->status =  $rs->getAll();
		$rs->close();
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \core\resources\Resource::save()
	 */
	public function save(){
		$fields = array();
		if(isset($this->toUpdate->fields)){
			$fields = $this->toUpdate->fields;
			unset($this->toUpdate->fields);
		}
		
		$events = array();
		if(isset($this->toUpdate->events)){
			$events = $this->toUpdate->events;
			unset($this->toUpdate->events);
		}
		
		$concepts = array();
		if(isset($this->toUpdate->concepts)){
			$concepts = $this->toUpdate->concepts;
			unset($this->toUpdate->concepts);
		}
		
		if($this->new){
			if(!isset($this->toUpdate->auto_imputation)){
				$this->toUpdate->auto_imputation = 'N';
			}
			if(!isset($this->toUpdate->full_grid)){
				$this->toUpdate->full_grid = 'N';
			}
			if(!isset($this->toUpdate->resource_mandatory)){
				$this->toUpdate->resource_mandatory = 'N';
			}
		}
		
		parent::save(); //If new resource, save and assign new id.
		
		if(count($fields)>0){
			if(!$this->new){ //Primer eliminarem els camps eliminats, comparant-los amb els camps que tenia fins ara aquest tipus de tasca.
				$to_delete = "";
				foreach($this->fields as $field){
					$found = false;
					foreach($fields as $f){
						if($f->fid == $field["fid"]){
							$found = true;
						}
					}
					if(!$found){
						$to_delete.= "'".$field["fid"]."',";
					}
				}
				$to_delete = rtrim($to_delete,",");
				if($to_delete!=""){
					self::$environment->dbcon->execute("DELETE FROM TASK_TYPE_FIELDS WHERE TT_ID=:TTID AND F_ID IN ($to_delete)",array("TTID" => $this->id));
				}	
			}
			$sort = 1;
			foreach($fields as $f){
				$rs = self::$environment->dbcon->execute("SELECT * FROM TASK_TYPE_FIELDS WHERE TT_ID=:TTID AND F_ID=:FID",array("TTID" => $this->id,"FID" => $f->fid));
				if($rs->fetch()){ //Update
					$var["SORT"] = $sort;
					$vw["TTID"] = $this->id;
					$vw["FID"] = $f->fid;
					self::$environment->dbcon->update("TASK_TYPE_FIELDS",$var,"TT_ID=:TTID AND F_ID=:FID",$vw);
				}
				else{ //Insert
					$var["TT_ID"] = $this->id;
					$var["F_ID"] = $f->fid;
					$var["SORT"] = $sort;
					self::$environment->dbcon->add("TASK_TYPE_FIELDS",$var);
				}
				$rs->close();
				$sort++;
			}
		}
		
		if(count($events)>0){
			$sort = 1;
			if(!$this->new){
				self::$environment->dbcon->execute("DELETE FROM TASK_TYPE_EVENTS WHERE TT_ID=:TTID",array("TTID" => $this->id));
			}
			foreach($events as $e){
				$sql = "INSERT INTO TASK_TYPE_EVENTS (ID,TT_ID,INIT_STATUS,END_STATUS,TE_ID,SORT) values((SELECT nvl(max(id)+1,1) FROM TASK_TYPE_EVENTS),:TTID,:IST,:EST,:TEID,:SORT)";
				self::$environment->dbcon->execute($sql,array("TTID" => $this->id,"IST" => $e->init_status,"TEID" => $e->event,"EST" => $e->end_status,"SORT" => $sort));
				$sort++;
			}
		}
		
		if(count($concepts)>0){
			if(!$this->new){ //Primer eliminarem els camps eliminats, comparant-los amb els camps que tenia fins ara aquest tipus de tasca.
				$to_delete = "";
				foreach($this->concepts as $concept){
					$found = false;
					foreach($concepts as $c){
						if($c->tc_id == $concept->tc_id){
							$found = true;
						}
					}
					if(!$found){
						$to_delete.= "'".$concept->tc_id."',";
					}
				}
				$to_delete = rtrim($to_delete,",");
				if($to_delete!=""){
					self::$environment->dbcon->execute("DELETE FROM TASK_TYPE_CONCEPTS WHERE TT_ID=:TTID AND F_ID IN ($to_delete)",array("TTID" => $this->id));
				}
			}
			$sort = 1;
			foreach($concepts as $c){
				$rs = self::$environment->dbcon->execute("SELECT * FROM TASK_TYPE_CONCEPTS WHERE TT_ID=:TTID AND TC_ID=:TCID",array("TTID" => $this->id,"TCID" => $c->tc_id));
				if($rs->fetch()){ //Update
					$var["computed"] = $c->computed;
					$var["TCC_ID"] = $c->tcc_id;
					$var["PERCENTAGE"] = $c->percentage;
					$var["SORT"] = $sort;
					$vw["TTID"] = $this->id;
					$vw["TCID"] = $c->tc_id;
					
					self::$environment->dbcon->update("TASK_TYPE_CONCEPTS",$var,"TT_ID=:TTID AND TC_ID=:TCID",$vw);
				}
				else{ //Insert
					$var["computed"] = $c->computed;
					$var["TCC_ID"] = $c->tcc_id;
					$var["PERCENTAGE"] = $c->percentage;
					$var["TT_ID"] = $this->id;
					$var["TC_ID"] = $c->tc_id;
					$var["SORT"] = $sort;
					self::$environment->dbcon->add("TASK_TYPE_CONCEPTS",$var);
				}
				$rs->close();
				$sort++;
			}
		}
		
		if($this->new){
			return array("ID" => $this->id);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \core\resources\Resource::getOptions()
	 */
	public function getOptions(){
		parent::getOptions();
		$this->options["events"] = EventsManagement::getStaticAndDynamicEvents();
		$this->options["concepts"] = TaskConceptsManagement::_get(false);
		$this->options["fields"] = FieldsManagement::_get(false);
		$this->options["status"] = $this->status;
		
	}
}