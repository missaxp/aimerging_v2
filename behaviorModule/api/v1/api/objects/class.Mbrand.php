<?php 
namespace api\crud;

use core\resources\Resource;
use core\http\Status;
use core\http\ErrorCode;
use core\Environment;
use common\Table;


/**
 * Class Project.
 *
 * @author phidalgo
 * @namespace api\crud
 * @created 20160421
 * @modified 20160421
 */
class mbrand extends Resource{
	public $name;
	public $files;
	public $task_ids = array();
	public $source_lang;
	public $target_langs = array();
	public $tp_id;
	public $tp_name;
	public $f100;
	public $fcm;
	public $frep;
	public $f95;
	public $f85;
	public $f75;
	public $f50;
	public $fnm;
	public $prefered_deadline;
	public $c_cm;
	public $c_seo;
	public $c_rs;
	public $c_rt;
	public $c_or;
	public $c_st;
	public $c_dtp;
	public $tasks = array();
	
	public function __construct($mid = null,$loadBasic = false){
		self::$environment = Environment::getInstance ();
		if($mid != null){ //Existing Project
			$fields = "ID,
			NAME,
			USER_ID,
			GROUP_ID,
			SOURCE_LANG,
			FCM,
			F100,
			FREP,
			F95,
			F85,
			F75,
			F50,
			FNM,
			TP_ID,
			prefered_deadline,
			C_SEO,
			C_CM,
			C_RS,
			C_RT,
			C_OR,
			C_ST,
			C_DTP,
			DEPARTMENT_ID,
			CREATION_TS,
			MODIFICATION_TS
			";
			
			$sql = "SELECT $fields FROM MBRAND_PROJECTS WHERE ID=:ID";
			$rs = self::$environment->dbcon->execute($sql,array("ID" => $mid));
			$dat = $rs->getAll();
			$rs->close();

			if(isset($dat[0])){
				$pr = $dat[0];
				$this->id = $pr["id"];
				$this->name = $pr["name"];
				
				$this->creation_ts = $pr["creation_ts"];
				$this->modification_ts = $pr["modification_ts"];
				$this->owner_gid = $pr["group_id"]; //Record owner group
				$this->owner_uid = $pr["user_id"]; //Record owner user id
				$this->department_id = $pr["department_id"];
				$this->source_lang = $pr["source_lang"];
				$this->fcm = $pr["fcm"];
				$this->frep = $pr["frep"];
				$this->f100 = $pr["f100"];
				$this->f95 = $pr["f95"];
				$this->f85 = $pr["f85"];
				$this->f75 = $pr["f75"];
				$this->f50 = $pr["f50"];
				$this->tp_id = $pr["tp_id"];
				$this->fnm = $pr["fnm"];
				$this->prefered_deadline = $pr["prefered_deadline"];
				$this->c_cm = ($pr["c_cm"]=="Y"? true:false);
				$this->c_seo = ($pr["c_seo"]=="Y"? true:false);
				$this->c_rs = ($pr["c_rs"]=="Y"? true:false);
				$this->c_rt = ($pr["c_rt"]=="Y"? true:false);
				$this->c_or = ($pr["c_or"]=="Y"? true:false);
				$this->c_st = ($pr["c_st"]=="Y"? true:false);
				$this->c_dtp = ($pr["c_dtp"]=="Y"? true:false);
				
				if($this->tp_id!=""){
					$rs = self::$environment->dbcon_gestio->execute("SELECT NOM FROM TERCER WHERE SA_CODSA='50' AND CODI=:CODI",array("CODI" => $this->tp_id));
					if($rs->fetch()){
						$this->tp_name = $rs->nom;
					}
					$rs->close();
				}
				
				$sql = "SELECT lang FROM MBRAND_PROJECTS_TARGET_L WHERE M_ID=:MID";
				$rs = self::$environment->dbcon->execute($sql,array("MID" => $this->id));
				while($rs->fetch()){
					array_push($this->target_langs,$rs->lang);
				}
				$rs->close();
								
				$this->getTasks();
				
				if(!$loadBasic){
					$rs = self::$environment->dbcon->execute("SELECT * FROM MBRAND_PROJECT_FILES WHERE M_ID=:MID",array("MID" => $this->id));
					$this->files = $rs->getAll();
					$rs->close();
				}
			}
			
			if ($this->id==null) {
				throw new AppException ( Status::S4_NotFound, ErrorCode::NotFound, "Error MBRAND PROJECT NOT FOUND. var \$id: $mid" );
			}
			$this->new = false; //Sets new property to false. Is a get/set operation.
		}
		else{ //New project
			$this->new = true;
		}
	
		parent::__construct(Table::MBRAND);
		if(!$loadBasic){
			$this->getOptions();
		}
	}
	
	
	private function getTasks(){
		$sql = "SELECT task_id FROM MBRAND_TASK_IDS WHERE M_ID=:MID";
		$rs = self::$environment->dbcon->execute($sql,array("MID" => $this->id));
		while($rs->fetch()){
			array_push($this->task_ids,$rs->task_id);
		}
		$rs->close();
		
		foreach($this->task_ids as $tid){
			$rs = self::$environment->dbcon_webtraduc->execute("SELECT ID_TASCA, ORIGEN_LANG || ' > ' || DESTI_LANG AS LANG, HORES_PREVIST,FINAL_PREVIST,(SELECT RESPONSABLE FROM SYGESPROJECTES WHERE CODI=PROJECTE) AS RESPONSABLE,ESTAT FROM TASQUES WHERE ID_TASCA=:TID",array("TID" => $tid));
			if($rs->fetch()){
				array_push($this->tasks,
						array(
								"pm" 		=> $rs->RESPONSABLE,
								"deadline" 	=> $rs->FINAL_PREVIST,
								"forecast" 	=> $rs->HORES_PREVIST,
								"status" 	=> $rs->ESTAT,
								"lang" 		=> $rs->lang,
								"task_id"	=> $rs->id_tasca
						)
						);
			}
				
		}
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \core\resources\Resource::getOptions()
	 */
	public function getOptions(){
		parent::getOptions();
		//$this->options["languages"] = LanguagesManagement::getLanguages(false,array("CODE","NAME"));
		//Cas especial, aquest formulari ha d'ensenyar els idiomes de l'IDCP ASP, de moment.
		$rs = self::$environment->dbcon_webtraduc->execute("SELECT CODI FROM IDIOMES ORDER BY CODI");
		$this->options["idcp_asp_languages"] = $rs->getAll();
		$rs->close();
		
		
		/*$rs = self::$environment->dbcon_gestio->execute("select codi,nom from tercer WHERE SA_CODSA='50' AND NOM NOT LIKE '(%' order by NOM");
		$this->options["dgest_third_party"] = $rs->getAll();
		$rs->close();*/
	}
	
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \core\resources\Resource::save()
	 */
	public function save(){
		/**
		 * En aquest cas, la data preferida desde el formulari de mBran no és date time sino només date, el frontend ho envia
		 * Com un rang de dates, cosa que és correte ja que les dates es desen en Zulu time, hem de definir que és un dia.
		 * A Espanya, ara mateix amb DST ON, el dia d'avui en local expressat com a 13/09/2016 s'expressa i ha de ser escrit com a :
		 * 20160913T000000+0200-20160913T235959+0200 que és el mateix que: 20160913T220000Z-20160913T215959Z.
		 * Llavors si desde el front end nosaltres diem que volem la tasca pel dia 16/09/2016, en realitat l'usuari l'esta demanant per a un interval que és valid entre (en hora local)
		 * Les 00:00:00 del dia 16/09/2016 i les 23:59:59 del mateix dia.
		 * 
		 * La cosa és que només tenim un camp de base de dades, i l'idcp asp necessita data i hora així que agafarem un dels valors de data i hi fixarem la hora.
		*/
		if(isset($this->toUpdate->prefered_deadline)){
			$ranges = explode("-",$this->toUpdate->prefered_deadline);
			$this->toUpdate->prefered_deadline = $ranges[1]; //Serà la data que sigui amb hora 000000Z
		}
		
		$target_langs = array();
		if(isset($this->toUpdate->target_langs)){
			if(!$this->new){
				self::$environment->dbcon->execute("DELETE FROM MBRAND_PROJECTS_TARGET_L WHERE M_ID=:MID",array("MID" => $this->id));
			}
			$target_langs = $this->toUpdate->target_langs;
			unset($this->toUpdate->target_langs);
		}
		
		parent::save();
		
		if(count($target_langs)){
			foreach($target_langs as $tlang){
				$vars["M_ID"] = $this->id;
				$vars["LANG"] = $tlang;
				self::$environment->dbcon->add("MBRAND_PROJECTS_TARGET_L",$vars);
			}
		}
		
		//Si és nou registre, doncs actualitzem la taula mbrand_proejct_files afegint el id del element als seus fitxers
		if($this->new && isset($this->toUpdate->attachments_id)){
			foreach($this->toUpdate->attachments_id as $att){
				$vars["M_ID"] = $this->id;
				$varsW["FID"] = $att;
				self::$environment->dbcon->update("MBRAND_PROJECT_FILES",$vars,"FID=:FID",$varsW);
			}	
		}
		
		//Sempre retornem ell mateix.
		if($this->new){
			$p = new self($this->id);
			return $p->get();
		}
	}
	
	/**
	 * We need to also delete the MBRAND_PROJECT_FILES records for this element
	 * {@inheritDoc}
	 * @see \core\resources\Resource::delete()
	 */
	public function delete(){
		self::$environment->dbcon->execute("DELETE FROM MBRAND_PROJECT_FILES WHERE M_ID=:MID",array("MID" => $this->id));
		parent::delete(); //Execute master delete.
		return true;
	}
	
	public function create_localization_task($combo = true){
		
		/*
		 * Clases genèriques de connexió amb l'IDCP.
		 */
		require_once $_SERVER["DOCUMENT_ROOT"].'/libs/old_idcp/class.projecte.php';
		require_once $_SERVER["DOCUMENT_ROOT"].'/libs/old_idcp/class.tasca.php';
		require_once $_SERVER["DOCUMENT_ROOT"].'/libs/old_idcp/class.tipus.php'; //Classes Abstractes amb constants en elles.
		
		foreach($this->target_langs as $tlang){
			$aspt = new \tasca(self::$environment->dbcon_webtraduc, '215154');
			
			
			$aspt->estat 			= \TipusEstat::ASSIGNED;
			$aspt->tipustasca 		= ($combo? \TipusTasca::COMBO:\TipusTasca::TRANSLATION);
			$aspt->tipusCarpeta 	= \TipusCarpeta::NORMAL;
			$aspt->data_entrega 	= $this->prefered_deadline;
			$aspt->desti1 			= $aspt->pm;
			$aspt->desti2  			= $aspt->pm;
			$aspt->idisource 		= $this->source_lang;
			$aspt->iditarget 		= $tlang;
			$aspt->client 			= $this->tp_id;
			$aspt->descripcio 		= $this->name;
			$aspt->rate				= "N";
			$aspt->creador 			= "IDCPWS";
			$aspt->origen 			= "IDCPWS";
			$aspt->area 			= "IT, COMPUTER";
			$aspt->aroute 			= true;
			$aspt->tws 				= false;
			$aspt->memoq 			= false;
			$aspt->instruccions 	= "test";
			$aspt->grup 			= false;
			$aspt->idgrup 			= "";
			$aspt->comptatge->ice 	= $this->fcm;
			$aspt->comptatge->rep 	= $this->frep;
			$aspt->comptatge->f100 	= $this->f100;
			$aspt->comptatge->f95 	= $this->f95;
			$aspt->comptatge->f85 	= $this->f85;
			$aspt->comptatge->f75 	= $this->f75;
			$aspt->comptatge->f50 	= $this->f50;
			$aspt->comptatge->nm 	= $this->fnm;
			
			if($aspt->creaTasca()){
				foreach($this->files as $mbrandfile){
					$file = new File($mbrandfile["fid"]);
					$aspt->uploadFile($file->path, "S",true,false,$file->name);
				}
				$vars["TASK_ID"] = $aspt->idtasca;
				$vars["M_ID"] = $this->id;
				self::$environment->dbcon->add("MBRAND_TASK_IDS",$vars); 
				
			}
			else{
				die(var_dump($aspt->resposta["missatge"]));
			}
		}
		
		$this->getTasks();
		return $this->tasks;
	}
	

}
?>