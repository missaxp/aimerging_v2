<?php
namespace api\crud;

use core\Environment;
use core\Exception\AppException;
use common\Table;
use core\resources\Resource;
use core\http\Status;
use core\http\ErrorCode;

/**
 * Class Field
 * @author phidalgo
 * @created 20160513
 * @updated 
 */
class Event extends Resource{
	private static $field_types = array("T" => "TEXT","RT" => "RICH_TEXT","C" => "CHECKBOX","SL" => "STATIC_LIST");
	
	public $name;
	public $interactive;
	public $fields = array();
	public $event_type;
	public $on_success;
	public $on_fail;
	
	
	protected $steps = array();
	public $titles;
	public $descriptions;
	
	
	/**
	 * @param integer $id
	 * @param boolean $loadAll
	 * @throws AppException
	 */
	public function __construct($id = null,$loadBasic = false){
		self::$environment = Environment::getInstance ();
		if($id!=null){
			$rs = self::$environment->dbcon->execute("SELECT * FROM TASK_EVENTS WHERE ID=:ID AND TYPE=:TYPE",array("ID" => $id,"TYPE" => 'D'));
			if($rs->fetch()){
				$this->id = $rs->id;
				$this->name = $rs->name;
				$this->interactive = $rs->interactive;
				$this->on_success = $rs->on_success;
				$this->on_fail = $rs->on_fail;
				$this->event_type = $rs->event_type;
				$this->creation_ts = $rs->creation_ts;
				$this->modification_ts = $rs->modification_ts;
			}
			$rs->close();
			
			if ($this->id==null) {
				throw new AppException ( Status::S4_NotFound, ErrorCode::NotFound, "Error EVENT NOT FOUND. var \$id: $id" );
			}
			
			$rs = self::$environment->dbcon->execute("SELECT * FROM TASK_EVENT_LANG WHERE E_ID=:EID",array("EID" => $this->id));
			while($rs->fetch()){
				$this->descriptions[$rs->language] = $rs->description;
				$this->titles[$rs->language] = $rs->title;
			}
			$rs->close();
			
			$rs = self::$environment->dbcon->execute("SELECT id,required,type FROM TASK_EVENTS_FIELDS WHERE E_ID=:EID ORDER BY SORT ASC",array("EID" => $this->id));
			while($rs->fetch()){
				$f = array();
				$f["id"] = $rs->id;
				$f["required"] = ($rs->required=='Y'? true:false);
				$f["type"] = $rs->type;
				$this->fields[] = $f;
			}
			$rs->close();
			foreach($this->fields as &$field){
				$rs = self::$environment->dbcon->execute("SELECT language,label FROM TASK_EVENT_FIELDS_LANG WHERE F_ID=:FID",array("FID" => $field["id"]));
				while($rs->fetch()){
					$field["label_".$rs->language] = $rs->label;
				}
				$rs->close();
				
				if(strtoupper($field["type"])=="SL"){
					$rs = self::$environment->dbcon->execute("SELECT VALUE FROM TASK_EVENTS_FIELDS_LIST WHERE F_ID=:FID",array("FID" => $field["id"]));
					$field["list"] = $rs->getAll();
					$rs->close();
				}
			}
			
			
			$this->new = false; //Sets new property to false. Is a get/set operation.
		}
		else{
			$this->new = true;
		}
		
		parent::__construct(Table::TASK_EVENTS);
		if(!$loadBasic){
			$this->getOptions();
		}
	}
	
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \core\resources\Resource::set()
	 */
	public function set($data){
		if($this->new){ //Per defecte tots els events configurables són events interactius.
			$this->toUpdate->interactive = 'Y';
		}
		
		foreach($data as $prop => $value){
			if(startsWith($prop,"description_")){
				$t = new \stdClass();
				$t->lang = str_replace("description_","",$prop);
				$t->text = $value;
				$this->toUpdate->descriptions[] = $t;
				unset($data->$prop);
			}
			
			if(startsWith($prop,"title_")){
				$t = new \stdClass();
				$t->lang = str_replace("title_","",$prop);
				$t->text = $value;
				$this->toUpdate->titles[] = $t;
				unset($data->$prop);
			}
			
			if($prop == 'type'){
				$this->toUpdate->event_type = $value;
				unset($data->$prop);
			}
			
			if($prop == 'form_elements'){
				$this->toUpdate->form_elements = $value;
				unset($data->$prop);
			}
		}
		
		return parent::set($data);
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \core\resources\Resource::save()
	 */
	public function save(){
		$descriptions = array();
		if(isset($this->toUpdate->descriptions)){
			$descriptions = $this->toUpdate->descriptions;
			unset($this->toUpdate->descriptions);
		}
		
		$titles = array();
		if(isset($this->toUpdate->titles)){
			$titles = $this->toUpdate->titles;
			unset($this->toUpdate->titles);
		}
		
		if($this->new && !isset($this->toUpdate->event_type)){
			$this->toUpdate->event_type = 'C';
		}
		
		$form_elements = null;
		if(isset($this->toUpdate->form_elements)){
			$form_elements = $this->toUpdate->form_elements;
			unset($this->toUpdate->form_elements);
		}
		
		parent::save();
		
		if($this->event_type=="C"){
			$sql = "DELETE FROM TASK_EVENTS_FIELDS WHERE E_ID=:EID";
			self::$environment->dbcon->execute($sql,array("EID" => $this->id));
		}
		else{
			if($form_elements!=null){
				foreach($this->fields as $field){
					$found = false;
					foreach($form_elements as $element){
						if(isset($element->id) && $element->id==$field["id"]){
							$found = true;
							break;
						}
					}
					if(!$found){
						$var = array("EID" => $this->id,"ID" => $field["id"]);
						self::$environment->dbcon->execute("DELETE FROM TASK_EVENTS_FIELDS WHERE E_ID=:EID AND ID=:ID",$var);
						unset($var);
					}
				}
				
				$sort = 1;
				foreach($form_elements as $element){
					$labels = array();
					foreach (self::$environment->languages as $lang){
						$name = 'label_'.$lang["id"];
						if(isset($element->$name) && $element->$name!=""){
							$labels[] = array("lang" => $lang["id"], "value" => $element->$name);
						}
						else{
							throw new AppException(Status::S4_PreconditionFailed,ErrorCode::FieldRequired,"A label string must be send for ".$lang["id"]);
						}
					}
					if(isset($element->id)){
						$var["SORT"] = $sort;
						$vw["EID"] = $this->id;
						$vw["ID"] = $element->id;
						self::$environment->dbcon->update("TASK_EVENTS_FIELDS",$var,"E_ID=:EID AND ID=:ID",$vw);
						unset($var);
						unset($vw);
					}
					else{
						$rs = self::$environment->dbcon->execute("SELECT nvl(max(id)+1,0) as FID FROM TASK_EVENTS_FIELDS");
						$rs->fetch();
						$fid = $rs->fid;
						$sql = "INSERT INTO TASK_EVENTS_FIELDS (E_ID,TYPE,REQUIRED,ID,SORT) VALUES (:EID,:TYPE,:REQ,:FID,:SORT)";
						$var = array("EID" => $this->id,"TYPE" => strtoupper($element->type),"REQ" => ($element->required? 'Y':'N'),"FID" => $fid,"SORT" => $sort);
						self::$environment->dbcon->execute($sql,$var);
						unset($var);
							
						if(strtoupper($element->type)=="SL"){ //if this element form is a Static list...
							foreach($element->list as $lv){
								$vars["F_ID"] = $fid;
								$vars["VALUE"] = $lv->value;
								self::$environment->dbcon->add("TASK_EVENTS_FIELDS_LIST", $vars);
							}
						}
						
						foreach($labels as $label){
							$var["F_ID"] = $fid;
							$var["LANGUAGE"] = $label["lang"];
							$var["LABEL"] = $label["value"];
							self::$environment->dbcon->add("TASK_EVENT_FIELDS_LANG",$var);
						}
					}
					$sort++;
				}
			}
		}
		
		if(count($descriptions)>0){
			foreach($descriptions as $t){
				$rs = self::$environment->dbcon->execute("SELECT * FROM TASK_EVENT_LANG WHERE E_ID=:EID AND LANGUAGE=:LANG",array("EID" => $this->id,"LANG" => $t->lang));
				$text = ($t->text==""? $this->name:$t->text);
				if($rs->fetch()){ //update
					$vw["EID"] = $this->id;
					$vw["LANG"] = $t->lang;
					$var["DESCRIPTION"] = $text;
					self::$environment->dbcon->update("TASK_EVENT_LANG",$var,"E_ID=:EID AND LANGUAGE=:LANG",$vw);
				}
				else{ //Insert
					$var["E_ID"] = $this->id;
					$var["LANGUAGE"] = $t->lang;
					$var["DESCRIPTION"] = $text;
					self::$environment->dbcon->add("TASK_EVENT_LANG",$var);
				}
				$rs->close();
			}
		}
		
		if($this->new && count($titles)!=count(self::$environment->languages)){
			$tb = array();
			foreach (self::$environment->languages as $lang){
				$found = false;
				foreach($titles as $t){
					if($t->lang==$lang["id"]){
						$found = true;
					}
				}
				if(!$found){
					$t2 = new \stdClass();
					$t2->lang = $lang["id"];
					$t2->text = $this->name;
					$tb[] = $t2;
				}
				else{
					$tb[] = $t;
				}
			}
			$titles = $tb;
			unset($tb);
		}
		
		if(count($titles)>0){
			foreach($titles as $t){
				$rs = self::$environment->dbcon->execute("SELECT * FROM TASK_EVENT_LANG WHERE E_ID=:EID AND LANGUAGE=:LANG",array("EID" => $this->id,"LANG" => $t->lang));
				$text = ($t->text==""? $this->name:$t->text);
				if($rs->fetch()){ //update
					$vw["EID"] = $this->id;
					$vw["LANG"] = $t->lang;
					$var["TITLE"] = $text;
					self::$environment->dbcon->update("TASK_EVENT_LANG",$var,"E_ID=:EID AND LANGUAGE=:LANG",$vw);
				}
				else{ //Insert
					$var["E_ID"] = $this->id;
					$var["LANGUAGE"] = $t->lang;
					$var["TITLE"] = $text;
					self::$environment->dbcon->add("TASK_EVENT_LANG",$var);
				}
				$rs->close();
			}
		}
		
		if($this->new){
			return array("ID" => $this->id);
		}
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \core\resources\Resource::getOptions()
	 */
	public function getOptions(){
		$this->options["types"] = self::$field_types;
		parent::getOptions();
	}
	
}