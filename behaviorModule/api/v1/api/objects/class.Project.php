<?php 
namespace api\crud;

use core\resources\Resource;
use core\http\Status;
use core\http\ErrorCode;
use core\Environment;
use common\Table;


/**
 * Class Project.
 *
 * @author phidalgo
 * @namespace api\crud
 * @created 20160421
 * @modified 20160421
 */
class Project extends Resource{
	public $name;
	public $code;
	public $full_code;
	public $description;
	public $tp_id;
	public $tp_name;
	public $final_client;
	public $final_client_name;
	public $tp_code;
	public $contacts; //Project contacts.
	public $product;
	public $common_instructions;
	public $tree;
	/**
	 * Project team.
	 * No confondre amb objecte Team. 
	 * Podem assignar "Teams" a projecte, pero el que es desarà seràn tots els usuaris
	 * d'aquell projecte, i no la relació del projecte amb el team
	 * @var array
	 */
	public $team;
	/**
	 * Root Project.
	 * @var Project
	 */
	public $root;
	/**
	 * Parent Project.
	 * @var Project
	 */
	public $parent; //Conté objecte project del pare (si en té).
	public $hasParents = false;
	public $hasChildrens = false;
	public $isSubProject = false;
	
	protected $parent_id;
	protected $root_id;
	private $parent_code;
	private $childrens; //Relació de projectes fills (jeràrquic).
	private $parents; //Relació de projectes pare (no-jerarquic. Array unidimensional).
	private $childrens_1d; //Relació de projectes fills (no-jerarquic. Array unidimensional).
	private $tree_1d; //Relació de tots els projectes des de l'arrel (no-jerarquic. Array unidimensional).
	
	private static function getParent($pid){
		$found = false;
		$sql = "SELECT ID,PARENT_ID,CODE,ROOT_ID,NAME,FULL_CODE FROM PROJECTS
		WHERE ID=:PID";
		$rs = self::$environment->dbcon->execute($sql,array("PID" => $pid));
		if($rs->fetch()){
			$p = new \stdClass();
			$p->code = $rs->code;
			$p->full_code = $rs->full_code;
			$p->pid = $rs->parent_id;
			$p->rid = $rs->root_id;
			$p->name = $rs->name;
			$p->id = $rs->id;
			$found = true;
		}
		$rs->close();
		return ($found? $p:null);
	}

	private static function getFullTree($rid){
		if($rid==null) return null;
		$vars = array();
		//No cal mirar department id pq quan hem obtingut les dades del projecte inicial, ja hem
		//mirat si pertanyia al meu departament. I si aquest ho pertany, tots els altres projectes (pares o fills) també 
		//seràn del mateix departament.
		$sql = "SELECT ID,PARENT_ID,CODE,ROOT_ID,NAME,FULL_CODE FROM PROJECTS
		WHERE ".($rid!=null? "PARENT_ID=:ID":"PARENT_ID is NULL");
		
		if($rid!=null){
			$vars["ID"] = $rid;
		}
		$rsp = array();
		$rs = self::$environment->dbcon->execute($sql,$vars);
		$sps = $rs->getAll();
		$rs->close();
		foreach($sps as $sp){
			$p = new \stdClass();
			$p->code = $sp["code"];
			$p->full_code = $sp["full_code"];
			$p->pid = $sp["parent_id"];
			$p->rid = $sp["root_id"];
			$p->name = $sp["name"];
			$p->id = $sp["id"];
			$p->childrens = self::getFullTree($sp["id"]);
			if(count($p->childrens)==0){
				$p->childrens = null;
			}
			$rsp[$sp["full_code"]] = $p;
		}
		return $rsp;
	}
	
	private static function getParentTree($parent_id,$code = null){
		if($code!=null){
			$tree = $code;
		}
		else{
			$tree = ".";
		}
		
		$environment = Environment::getInstance();
		
		while($parent_id!=null){
			$rs = $environment->dbcon->execute("SELECT CODE,PARENT_ID FROM PROJECTS WHERE ID=:PID",array("PID" => $parent_id));
			if($rs->fetch()){
				$parent_id = $rs->parent_id;
				$tree = $rs->code.".".$tree;
			}
			else{
				$parent_id = null;
			}
			$rs->close();
		}
		return $tree;
	}
	
	public static function getFullCode($pid){
		$fullcode = "";
		$environment = Environment::getInstance();
		while($pid!=null){
			$rs = $environment->dbcon->execute("SELECT CODE,PARENT_ID FROM PROJECTS WHERE ID=:PID",array("PID" => $pid));
			if($rs->fetch()){
				$pid = $rs->parent_id;
				if($fullcode==""){
					$fullcode = $rs->code;
				}
				else{
					$fullcode = $rs->code.".".$fullcode;
				}
			}
			else{
				$pid = null;
			}
			$rs->close();
		}
		return $fullcode;
	}
	
	public function __construct($pid = null,$loadBasic = false){
		self::$environment = Environment::getInstance ();
		if($pid != null){ //Existing Project
			$fields = "ID,
				PARENT_ID,
				ROOT_ID,
				CODE,
				NAME,
				DESCRIPTION,
				COMMON_INSTRUCTIONS,
				TP_ID,
				(SELECT NAME FROM THIRD_PARTY WHERE ID=TP_ID) AS TP_NAME,
				FINAL_CLIENT,
				(SELECT NAME FROM THIRD_PARTY WHERE ID=FINAL_CLIENT) AS FINAL_CLIENT_NAME,
				USER_ID,
				GROUP_ID,
				DEPARTMENT_ID,
				TP_CODE,
				PRODUCT,
				FULL_CODE,
				CREATION_TS,
				MODIFICATION_TS
				";
			
			if(is_numeric($pid)){
				$sql = "SELECT $fields FROM PROJECTS WHERE ID=:ID";
				$rs = self::$environment->dbcon->execute($sql,array("ID" => $pid));
				$pr = $rs->getAll();
				$rs->close();
			}

			if(!isset($pr[0])){
				$sql = "SELECT $fields FROM PROJECTS WHERE FULL_CODE=:ID";
				$rs = self::$environment->dbcon->execute($sql,array("ID" => $pid));
				$pr = $rs->getAll();
				$rs->close();
				
			}
			
			if(isset($pr[0])){
				$pr = $pr[0];
				$this->id = $pr["id"];
				$this->name = $pr["name"];
				$this->code = $pr["code"];
				$this->parent_id = $pr["parent_id"];
				$this->root_id = $pr["root_id"];
				$this->description = $pr["description"];
				$this->common_instructions = $pr["common_instructions"];
				$this->tp_id = $pr["tp_id"];
				$this->tp_name = $pr["tp_name"];
				$this->final_client = $pr["final_client"];
				$this->final_client_name = $pr["final_client_name"];
				$this->tp_code = $pr["tp_code"];
				$this->creation_ts = $pr["creation_ts"];
				$this->modification_ts = $pr["modification_ts"];
				$this->owner_gid = $pr["group_id"]; //Record owner group
				$this->owner_uid = $pr["user_id"]; //Record owner user id
				$this->department_id = $pr["department_id"];
				$this->product = $pr["product"];
				$this->full_code = $pr["full_code"];
				
				
				if(!$loadBasic){
					$rs = self::$environment->dbcon->execute("SELECT * FROM PROJECTS_CONTACTS WHERE PJ_ID=:PID",array("PID" => $this->id));
					$this->contacts = $rs->getAll();
					$rs->close();
					foreach($this->contacts as &$contact){
						if($contact["ct_id"]!=null){
							$sql = "SELECT C.NAME,(SELECT EMAIL FROM CONTACT_EMAILS WHERE PRIMARY='Y' AND CT_ID=C.ID) as EMAIL FROM CONTACT C WHERE C.ID=:ID";
							$rs = self::$environment->dbcon->execute($sql,array("ID" => $contact["ct_id"]));
							if($rs->fetch()){
								$contact["name"] = $rs->name;
								$contact["email"] = $rs->email;
							}
							else{
								unset($contact);
							}
							$rs->close();
						}
					}
					
					if($this->parent_id!=null){
						$this->hasParents = true;
						$this->isSubProject = true;
						$this->parent = new self($this->parent_id,true);
						if($this->parent_id!=$this->root_id){
							$this->root = new self($this->root_id,true);
						}
						else{
							$this->root = $this->parent;
						}
					}
					else{
						$this->isSubProject = false;
						$this->hasParents = false;
					}
					
					if($this->isSubProject){
						$parent_id = $this->parent_id;
						do{
							$p = self::getParent($parent_id);
							// die(var_dump($p));
							if($p!==false){
								if($p->pid!=null){
									$parent_id = $p->pid;
									$this->parents[] = $p;
								}
								else{
									$parent_id = null;
									$this->parents[] = self::getParent($this->root_id);
								}
							}
							else{
								$parent_id = null;
							}
						}
						while($parent_id!=null);
					}
					
					$p = (($this->isSubProject)? $this->root:$this);
					$this->tree[$p->full_code] = new \stdClass();
					$this->tree[$p->full_code]->childrens = self::getFullTree($p->id);
					$this->tree[$p->full_code]->code = $p->code;
					$this->tree[$p->full_code]->full_code = $p->full_code;
					$this->tree[$p->full_code]->name = $p->name;
					$this->tree[$p->full_code]->pid = $p->parent_id;
					$this->tree[$p->full_code]->rid = $p->root_id;
					$this->tree[$p->full_code]->id = $p->id;
					unset($p);
					
					$this->tree_1d = array();
					$this->tree_uniDimensional('tree_1d',$this->tree);
					
					$this->childrens = self::getFullTree($this->id);
					if(count($this->childrens)>0){
						$this->hasChildrens = true;
						$this->childrens_1d = array();
						$this->tree_uniDimensional('childrens_1d',$this->childrens);
					}
					
					//Get project team.
					$fields = "PT.USER_ID AS ID,(SELECT USERNAME FROM USERS WHERE ID=PT.USER_ID) AS NAME,PT.TYPE,PT.INHERIT";
					$sql = "SELECT $fields FROM PROJECTS_TEAM PT WHERE PJ_ID=:PID";
					$rs = self::$environment->dbcon->execute($sql,array("PID" => $this->id));
					$this->team = $rs->getAll();
					$rs->close();
				
				}
			}
			
			if ($this->id==null) {
				throw new AppException ( Status::S4_NotFound, ErrorCode::NotFound, "Error PROJECT NOT FOUND. var \$id: $pid" );
			}
			$this->new = false; //Sets new property to false. Is a get/set operation.
		}
		else{ //New project
			$this->new = true;
		}
	
		parent::__construct(Table::PROJECTS);
		if(!$loadBasic){
			$this->getOptions();
		}
	}
	
	private function tree_uniDimensional($property,$childrens){
		if(count($childrens)==0) return;
		foreach($childrens as $sp){
			if(count($sp->childrens)>0){
				$this->tree_uniDimensional($property, $sp->childrens);
			}
			array_push($this->$property,$sp);
		}
	}
	
	/**
	 * Some project properties inherit from parent projects.
	 * Some of this properties are mandatory inherit, but others can be modified.
	 * Also, there are properties that must be inherited for all of the project subprojects.
	 * This method updates this properties from the parent project to all children projects.
	 */
	private function inheritFromParent(){
		//Hi ha certes propietats que només s'han d'assignar en el nou projecte. Si el projecte està creat, el que farem és borrar 
		//aquestes propietats (si és que les han enviat).
		if($this->new){
			//Si és pare, carreguem tota la informació del projecte pare, ja que la necessitem. Ho assignem a this->parent.
			$this->parent = new self($this->parent->id);
			$this->toUpdate->root_id  = ($this->parent->isSubProject? $this->parent->root_id:$this->parent->id); //Afegim root_id al nou sub projecte
			$this->toUpdate->owner_gid = $this->parent->owner_gid; //El grup s'hereda del pare sempre. No hi ha possiblitat de canaviar-ho
			$this->toUpdate->owner_uid = $this->parent->owner_uid; //L'usuari s'hereda del pare sempre. No hi ha possiblitat de canviar-ho.
			$this->toUpdate->tp_id = $this->parent->tp_id; //El client s'hereda del pare sempre. No hi ha possiblitat de canviar-ho.
			if(!isset($this->toUpdate->final_client)){
				$this->toUpdate->final_client = $this->parent->final_client; //Si no hem especificat client final, l'heredem del pare.
			}
			if(!isset($this->toUpdate->contacts)){ //Si no hem especificat contactes, els heredarem del projecte pare.
				if(count($this->parent->contacts)>0){
					$this->toUpdate->contacts = array();
					foreach($this->parent->contacts as $contact){
						$ct = new \stdClass();
						$ct->ct_id = $contact["ct_id"];
						$ct->name = $contact["name"];
						$ct->email = $contact["email"];
						$this->toUpdate->contacts[] = $ct;
					}
				}
			}
			if(!isset($this->toUpdate->department_id)){
				$this->toUpdate->department_id = $this->parent->department_id; //Per defecte el dpt del subprojecte és el dpt del pare.
			}
		}
		else{
			unset($this->toUpdate->root_id);
			unset($this->toUpdate->parent_code);
			unset($this->toUpdate->owner_uid);
			unset($this->toUpdate->owner_uid);
			unset($this->toUpdate->tp_id);
		}
	}
	
	private function getChildrens(){
		foreach($this->tree as $project){
			if($project->id==$this->id){
				return $project->childrens;
			}
		}
		return null;
	}
	
	private function propagateToChildrens($field,$value){
		function updateChildren($subprojects,$field,$value){
			if(count($subprojects)==0) return;
			foreach($subprojects as $sp){
				$vars = array();
				$vars["VALUE"] = $value;
				$vars["ID"] = $sp->id;
				Environment::getInstance()->dbcon->execute("UPDATE PROJECTS SET $field=:VALUE WHERE ID=:ID",$vars);
				updateChildren($sp->childrens,$field,$value);
			}
		}
		updateChildren($this->childrens, $field, $value);
	}
	/**
	 * $a -> 'A' -> ADD, 'U' -> update
	 * $pid -> project id
	 * $uid -> user_id
	 * $type -> user type (M -> Manager, S -> Specialist, R -> Resource, MR -> ManagerRead, SR -> SpecialistRead, RR -> ResourceRead)
	 * $inherit -> true -> if comes from parent, false -> if added by user.
	 * 
	 * @param char $a
	 * @param integer $pid
	 * @param integer $uid
	 * @param string $type
	 * @param boolean $inherit
	 */
	private function addResource($a,$pid,$uid,$type,$inherit){
		$var = array("PID" => $pid,"USER_ID"=> $uid,"TYPE" => $type);
		if($a=="U"){
			$sql = "UPDATE PROJECTS_TEAM SET TYPE=:TYPE WHERE PJ_ID=:PID AND USER_ID=:USER_ID";
		}
		else{
			$var["INHERIT"] = ($inherit? 'Y':'N');
			$sql = "INSERT INTO PROJECTS_TEAM (PJ_ID,USER_ID,TYPE,INHERIT) VALUES(:PID,:USER_ID,:TYPE,:INHERIT)";
		}
		self::$environment->dbcon->execute($sql,$var);
	}
	
	private function updateResourceDependences($resources){
		$resourcesToAdd = (array) $resources; //Recursos a AFEGIR
		$resourcesToDelete = array(); //Recursos a ELIMINAR
		$toDeleteIN = "";
		/*
		 * En el cas que actualitzem el projecte...
		 * Primer comparem els recursos enviats per l'usuari amb els que té el projecte i així trobarem quins recursos ha eliminat l'usuari.
		 */
		if(!$this->new){
			if($this->team!=null){
				foreach($this->team as $res){
					if(strlen($res["type"])==2){
						continue; //Si el recurs es hereat (és un MR,SR o RR) no podem afegirlo al array per a eliminar.
					}
					//Tots els recursos de $resources s'han d'incloure/actualitzar en aquest projecte i els seus fills. Els recursos que estiguin en $this->team
					//I no aqui, s'han d'eliminar.
					$foundRes = false;
					foreach($resourcesToAdd as $resource){
						if($resource->user_id==$res["id"]){
							$foundRes = true;
							break;
						}
					}
					if(!$foundRes){
						$resourcesToDelete[] = $res;
						$toDeleteIN.= "'".$res["id"]."',";
					}
				}
			}
			
			if(count($resourcesToDelete)>0){ //Si s'han eliminat usuaris... els eliminem de la DB.
				$toDeleteIN = rTrim($toDeleteIN,",");
				$sql = "DELETE FROM PROJECTS_TEAM WHERE PJ_ID=:PID AND USER_ID IN ($toDeleteIN)"; //Eliminem els usuaris del projecte actual...
				self::$environment->dbcon->execute($sql,array("PID"=> $this->id));
				if($this->hasChildrens){ //I de tots els projectes fills d'aquest projecte...
					foreach($this->childrens_1d as $project){
						$sql = "DELETE FROM PROJECTS_TEAM WHERE PJ_ID=:PID AND USER_ID IN ($toDeleteIN)";
						self::$environment->dbcon->execute($sql,array("PID" => $project->id));
					}
				}
			}
		}
		
		
		foreach($resourcesToAdd as $resource){
			if($this->new){ //Si és nou i és subprojecte, mirem si el recurs ja el conté el pare, si el conté, l'afegirem com a heredat.
				if($this->isSubProject){
					$foundOnParent = false;
					if($this->parent->team!=null){
						foreach($this->parent->team as $r){
							if($resource->user_id==$r["id"] && $resource->user_type==$r["type"]){
								$foundOnParent = true;
								break;
							}
						}
					}
					
					if($foundOnParent){ //Si l'hem trobat al parent, afegim el recurs com a heredat.
						$this->addResource("A", $this->id, $resource->user_id, $resource->user_type, true);
					}
					else{ //Sino, l'afegim com a no heredat
						$this->addResource("A", $this->id, $resource->user_id, $resource->user_type, false);
					}
				}
			}
			else{ //Si no és nou, mirem si el recurs ja existeix, i bé l'actualitzem o el creem en funció del q sigui.
				$foundRes = false;
				if($this->team!=null){
					foreach($this->team as $res){
						if($res["id"]==$resource->user_id){
							$foundRes = $res;
							break;
						}
					}
				}
				
				//Si hem trobat el recurs en aquest projecte... l'actualitzem, sino el creem.
				if($foundRes!==false){
					$this->addResource("U", $this->id, $resource->user_id, $resource->user_type,false);
				}
				else{
					$this->addResource("A", $this->id, $resource->user_id, $resource->user_type,false);
				}
			}
			
			
			
			
			//Si no és nou i té fills... (si és nou... no té fills, evidentment).
			if(!$this->new && $this->hasChildrens){
				if(strlen($resource->user_type)==2){
					continue; //Recursos de lectura no s'han de propagar als fills.
				}
				//Ara fem el mateix per a tots els fills. Com que per als fills d'aquest projecte no tinc el $this->team, faig el select a la DB.
				foreach($this->childrens_1d as $project){
					$sql = "SELECT * FROM PROJECTS_TEAM WHERE PJ_ID=:PID AND USER_ID=:USER_ID";
					$rs = self::$environment->dbcon->execute($sql,array("PID" => $project->id,"USER_ID"=> $resource->user_id));
					if($rs->fetch()){
						if(strlen($rs->type)==2){
							$this->addResource("U", $project->id, $resource->user_id, $resource->user_type,true);
						}
						else{
							if($resource->user_type=="M"){
								$this->addResource("U", $project->id, $resource->user_id, $resource->user_type,true);
							}
							else if($rs->type=="R" && $resource->user_type=="S"){
								$this->addResource("U", $project->id, $resource->user_id, $resource->user_type,true);
							}
						}
					}
					else{
						$this->addResource("A", $project->id, $resource->user_id, $resource->user_type,true);
					}
					$rs->close();
				}
			}
		}
	
		//Dels recursos eliminats, hem de comprovar si el pare d'aquest projecte tenia algun d'ells en R (MR,SR o RR), si el té, el podem eliminar.
		/**
		 * Pot donar-se el seguent cas:
		 *
		 *             ___ P1.1.1
		 *            /
		 *           /
		 *        __/ P1.1
		 *       /  \
		 *      /    \
		 *   __/ P1   \___ P1.1.2
		 *     \
		 *      \
		 *       \__  P1.2
		 *
		 *
		 * "AAAA" és un recurs creat en P1.1 com a Gestor, però no en P1. Quan es va crear "AAAA" en P1.1, es va crear també "AAAA" en P1 com a MR (Manager Read).
		 * En el P1.1 eliminem l'usuari "AAAA" que és Gestor.
		 *
		 * Accions:
		 * 	1- Eliminem "AAAA" dels subprojectes P1.1.1 i P1.1.2 i P1.1
		 *  2- Mirar en tots els fills del projecte pare (P1) per veure si algun dels fills conté "AAAA" com a gestor.
		 *  	-Si no el conté, podem eliminar el recurs "AAAA" que és MR.
		 *  	-Si el conté, no l'eliminem.
		 *  3- Repetir 2 per al pare del pare.
		 */
		if($this->isSubProject){
			$prs = array(); //Array de projectes pares.
			$parent_id = $this->parent->id;
			do{
				$pr = new self($parent_id); //Ho carreguem tot pq necessitem tots els fills.
				if($pr->hasParents){
					$parent_id = $pr->parent->id;
				}
				else{
					$parent_id = null;
				}
				$prs[] = $pr;
			}
			while($parent_id!=null);
			//$prs[] = new self($this->root->id); //Afegim el projecte arrel.
			//Ja tenim tots els pares.
			foreach($resourcesToDelete as $resource){
				/**@var $parent Project*/
				foreach($prs as $parent){
					$foundResource = false;
					foreach($parent->childrens_1d as $child){
						if($child->id==$this->id){ //No ens interessa si som nosaltres mateixos.
							continue;
						}
						$sql = "SELECT * FROM PROJECTS_TEAM WHERE PJ_ID=:PID";
						$rs = self::$environment->dbcon->execute($sql,array("PID" => $child->id));
						$rc = $rs->getAll();
						$rs->close();
						foreach($rc as $r){
							if($r["user_id"]==$resource["id"] && $r["type"]==$resource["type"]){
								$foundResource = $resource;
								break;
							}
						}
						if($foundResource!==false){
							break;
						}
					}
					if($foundResource===false){ //Si no hem trobat el recurs... podem borrar-lo
						$sql = "DELETE FROM PROJECTS_TEAM WHERE PJ_ID=:PID AND USER_ID=:USER_ID";
						self::$environment->dbcon->execute($sql,array("PID" => $parent->id,"USER_ID"=> $resource["id"]));
					}
				}
			}
			
			/**
			 * Si som subprojecte, i s'han creat nous recursos, hem de mirar si els nostres pares el ténen. i Sino, crear-los en mode lectura.
			 * TODO ??? Preguntar:
			 * 
			 * P1 té A,B i C com a recursos. A és M, B és S i C és R. Creem P1.1, subprojecte de P1 i s'hereden A,B i C. A P1.1 eliminem C com a R i afegim C com a M.
			 * Mirem si el pare de P1.1 té C. Té C com a R. Creem C com a MR? o deixem C com a R?
			 */
			foreach($resourcesToAdd as $res){
				$foundRes = false;
				/**@var $parent Project*/
				foreach($prs as $parent){
					foreach($parent->team as $pres){
						if($pres["id"] == $res->user_id){
							$foundRes = true;
							break;
						}
					}
					if(!$foundRes){
						$var["PID"] = $parent->id;
						$var["USER_ID"] = $res->user_id;
						$var["TYPE"] = $res->user_type."R";
						$sql = "INSERT INTO PROJECTS_TEAM (PJ_ID,USER_ID,TYPE) VALUES(:PID,:USER_ID,:TYPE)";
						self::$environment->dbcon->execute($sql,$var);
					}
				}
			}
		}
	}
	
	/**
	 * Given a new resource type and the current resource type,
	 * returns which type has to be this resource.
	 * 
	 * @param string $new
	 * @param string $current
	 */
	private function selectResourceType($new,$current){
		if($current == null){
			switch($type){
				case "M":
					return "MR";
				case "S":
					return "SR";
				case "R":
					return "RR";
			}
			return null;
		}
		return $current;	
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \core\resources\Resource::save()
	 */
	public function save(){
		if($this->new && isset($this->toUpdate->parent_code) && $this->toUpdate->parent_code!=null && $this->toUpdate->parent_code!=""){
			$this->isSubProject = true;
			$this->parent = new self($this->toUpdate->parent_code,true);
			$this->toUpdate->parent_id = $this->parent->id;
			if(!isset($this->toUpdate->code) || (isset($this->toUpdate->code) && $this->toUpdate->code=="")){
				$this->toUpdate->code = $this->newSubProjectID();
			}
			else{
				$this->toUpdate->code = substr($this->toUpdate->code, 0,3);
			}
		}
		unset($this->toUpdate->parent_code);
		//If this is a subproject, there are some properties that has to be inherit from the parent project.
		if($this->isSubProject){
			$this->inheritFromParent();
		}
		$contacts = array();
		if(isset($this->toUpdate->contacts)){
			$contacts = $this->toUpdate->contacts;
			unset($this->toUpdate->contacts);
		}
		
		$team = array();
		$updateTeam = false;
		if(isset($this->toUpdate->team)){
			$updateTeam = true;
			$team = $this->toUpdate->team;
			unset($this->toUpdate->team);
		}
		
		parent::save(); //If new resource, save and assign new id.
		
		if($this->new){
			if($this->isSubProject){
				$this->full_code = self::getFullCode($this->id);
				if(!$updateTeam){ //Si no enviem team, afegim tots els del pare com a heredats.
					if($this->parent->team!=null){
						foreach($this->parent->team as $resource){
							$this->addResource("A", $this->id, $resource["id"], $resource["type"], true);
						}	
					}
				}
			}
			else{
				$this->full_code = $this->code;
			}
			$var["FULL_CODE"] = $this->full_code;
			$vw["ID"] = $this->id;
			self::$environment->dbcon->update("PROJECTS",$var,"ID=:ID",$vw);	
		}
		else{
			/**
			 * Alerta amb el departament.
			 * Si és nou, està clar que no tindrà subprojectes.
			 * Propaga el departament, si el departament del projecte ha canviat.
			 */
			if(isset($this->toUpdate->department_id)){
				//No puc canviar el projecte si el departament del pare no és "TOTS".
				if(($this->isSubProject && $this->parent->department_id==null) || (!$this->isSubProject)){
					$this->propagateToChildrens("DEPARTMENT_ID",($this->toUpdate->department_id=="ALL"? null:$this->toUpdate->department_id));
				}
			}
			
			/**
			 * Si és projecte arrel i s'ha modificat el tercer, actualitzar a tots els fills.
			 */
			if(isset($this->toUpdate->tp_id) && !$this->isSubProject){
				$this->propagateToChildrens("TP_ID",$this->toUpdate->tp_id);
			}
		}
		
		if($updateTeam){
			$this->updateResourceDependences($team);
		}
		
		if(count($contacts)>0){
			if(!$this->new){ //Si no és nou recurs...
				self::$environment->dbcon->execute("DELETE FROM PROJECTS_CONTACTS WHERE PJ_ID=:PID",array("PID" => $this->id));
			}
			foreach($contacts as $contact){
				$sql = "INSERT INTO PROJECTS_CONTACTS (ID,CT_ID,NAME,EMAIL,PJ_ID) VALUES((SELECT nvl(max(ID)+1,1) FROM PROJECTS_CONTACTS),:CT_ID,:NAME,:EMAIL,:PID)";
				if($contact->ct_id==""){
					$vars["NAME"] = $contact->name;
					$vars["EMAIL"] = $contact->email;
					$vars["CT_ID"] = null;
				}
				else{
					$vars["CT_ID"] = $contact->ct_id;
					$vars["NAME"] = "";
					$vars["EMAIL"] = "";
				}
				$vars["PID"] = $this->id;
				self::$environment->dbcon->execute($sql,$vars);
			}
		}

		//Sempre retornem ell mateix.
		$p = new self($this->id);
		return $p->get();
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \core\resources\Resource::getOptions()
	 */
	public function getOptions(){
		parent::getOptions();
		$this->options["products"] = ProjectsManagement::getTPProducts($this->final_client); //Enviem el llistat de productes actuals... estarà buit si el TP no té productes...
		$this->options["myteams"] = TeamsManagement::_getMyTeams();
	}
	
	private function newSubProjectID(){
		$sql = "SELECT count(1) AS SP_ID FROM PROJECTS WHERE PARENT_ID=:PID";
		$rs = self::$environment->dbcon->execute($sql,array("PID" => $this->parent->id));
		$rs->fetch();
		$n = (int)$rs->sp_id;
		$sl = strlen($rs->sp_id);
		$rs->close();
		$available_counter_length = 3;
		$newSerie = false;
		do{
			$n++;
			$string = $this->getCountString($available_counter_length, $n);
			$newSerie = $this->checkSerie($string);
		}
		while($newSerie===false);
		return $string;
	}
	
	private function checkSerie($serie){
		$validSerie = false;
		$sql = "SELECT CODE FROM PROJECTS WHERE CODE=:SERIE AND PARENT_ID=:PID";
		$rs = self::$environment->dbcon->execute($sql,array("SERIE" => $serie, "PID" => $this->parent->id));
		if(!$rs->fetch()){
			$validSerie = true;
		}
		$rs->close();
		return $validSerie;
	}
	
	private function getCountString($length,$counter){
		$string = "";
		$cl = strlen((string) $counter);
		for($i = 0;$i<$length;$i++){
			if($i<($length-$cl)){
				$string.= "0";
			}
			else{
				$cs = (string)$counter;
				$string.= $cs{($length-($i+1))};
			}
		}
		return $string;
	}
}
?>