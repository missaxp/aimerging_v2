<?php
namespace api\crud;

use core\Environment;
use core\Exception\AppException;
use common\Table;
use core\resources\Resource;
use core\http\Status;
use core\http\ErrorCode;

/**
 * Class TaskConcept
 * @author phidalgo
 * @created 20160513
 * @updated 
 */
class TaskConcept extends Resource{

	public $name;
	public $labels;
	public $code;
	
	/**
	 * @param integer $id
	 * @param boolean $loadAll
	 * @throws AppException
	 */
	public function __construct($code = null,$loadBasic = false){
		self::$environment = Environment::getInstance ();
		if($code!=null){
			$rs = self::$environment->dbcon->execute("SELECT * FROM TASK_CONCEPTS WHERE CODE=:CODE",array("CODE" => $code));
			if($rs->fetch()){
				$this->id = $rs->id;
				$this->code = $rs->code;
				$this->department_id = $rs->department_id;
			}
			$rs->close();
			
			if ($this->id==null) {
				throw new AppException ( Status::S4_NotFound, ErrorCode::NotFound, "Error Task Concept NOT FOUND. var \$code: $code" );
			}
			

			$rs = self::$environment->dbcon->execute("SELECT * FROM TASK_CONCEPTS_LANG WHERE TC_ID=:TCID",array("TCID" => $this->id));
			while($rs->fetch()){
				$this->labels[$rs->language] = $rs->text;
			}
			$rs->close();
			
			$this->new = false; //Sets new property to false. Is a get/set operation.
		}
		else{
			$this->new = true;
		}
		
		parent::__construct(Table::TASKCONCEPTS);
		if(!$loadBasic){
			$this->getOptions();
		}
	}
	
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \core\resources\Resource::set()
	 */
	public function set($data){
		foreach($data as $prop => $value){
			if(startsWith($prop,"label_")){
				$t = new \stdClass();
				$t->lang = str_replace("label_","",$prop);
				$t->text = $value;
				$this->toUpdate->translations[] = $t;
				unset($data->$prop);
			}
		}
		return parent::set($data);
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \core\resources\Resource::save()
	 */
	public function save(){
		$translations = array();
		if(isset($this->toUpdate->translations)){
			$translations = $this->toUpdate->translations;
			unset($this->toUpdate->translations);
		}
		parent::save();
		
		if($this->new && count($translations)!=count(self::$environment->languages)){
			$tb = array();
			foreach (self::$environment->languages as $lang){
				$found = false;
				foreach($translations as $t){
					if($t->lang==$lang["id"]){
						$found = true;
					}
				}
				if(!$found){
					$t2 = new \stdClass();
					$t2->lang = $lang["id"];
					$t2->text = $this->code;
					$tb[] = $t2;
				}
				else{
					$tb[] = $t;
				}
			}
			$translations = $tb;
			unset($tb);
		}
		
		if(count($translations)>0){
			foreach($translations as $t){
				$rs = self::$environment->dbcon->execute("SELECT * FROM TASK_CONCEPTS_LANG WHERE TC_ID=:TCID AND LANGUAGE=:LANG",array("TCID" => $this->id,"LANG" => $t->lang));
				$text = ($t->text==""? $this->name:$t->text);
				if($rs->fetch()){ //update
					$vw["TCID"] = $this->id;
					$vw["LANG"] = $t->lang;
					$var["TEXT"] = $text;
					self::$environment->dbcon->update("TASK_CONCEPTS_LANG",$var,"TC_ID=:TCID AND LANGUAGE=:LANG",$vw);
				}
				else{ //Insert
					$var["TC_ID"] = $this->id;
					$var["LANGUAGE"] = $t->lang;
					$var["TEXT"] = $text;
					self::$environment->dbcon->add("TASK_CONCEPTS_LANG",$var);
				}
			}
		}
		
		if($this->new){
			return array("ID" => $this->id);
		}
	}
}