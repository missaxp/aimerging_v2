<?php

namespace api\crud;

use core\Environment;
use query\_OO;
use query\Query;

/**
 * Class memoq
 *
 * @author phidalgo & ctemporal
 * @namespace api\crud
 */
abstract class memoq {
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	protected static $langs = array (
			"EN",
			"FR" 
	);
	
	/**
	 * Crea tasca a taula DTASKS i desrpés el cron de JBALLUS la recull i crea tasca memoq i dps al IDCP ASP.
	 *
	 * @return boolean
	 */
	public static function createTask() {
		self::$environment = Environment::getInstance ();
		
		$sql = "select IFNULL[max(id_memoq)+1,0] as id_memoq FROM DTASKS";
		$rs = self::$environment->dbcon->execute ( $sql );
		if ($rs->fetch ()) {
			$memoq_id = $rs->getVal ( "id_memoq" );
		}
		$rs->close ();
		
		$type = $_POST ["type"];
		foreach ( $_FILES as $file ) {
			foreach ( self::$langs as $lang ) {
				// Per nou IDCP (user DB: IDCPWS)
				$rs = self::$environment->dbcon->execute ( "SELECT IFNULL[MAX(ID)+1,0] AS id FROM DTASKS" );
				$nid = 0;
				if ($rs->fetch ()) {
					$nid = $rs->getVal ( "id" );
				}
				$rs->close ();
				
				// Per IDCP ACTUAL (USER DB: WEBTRADUC)
				$rs = self::$environment->dbcon_webtraduc->execute ( "SELECT IFNULL[max(ID_ENTRADA)+1,1] as NID FROM TASQUES_ENTRADES" );
				if ($rs->fetch ()) {
					$anid = $rs->getVal ( "NID" );
				} else {
					$anid = - 1;
				}
				$rs->close ();
				
				// Creem l'objecte data
				$data ["subject"] = $file ["name"] [0];
				$data ["type"] = $type;
				$data ["path"] = "";
				$data ["html"] = "";
				$data ["attachments"] = $file ["name"] [0];
				$data ["anid"] = $anid;
				$data ["data"] = "FROM NEW IDCP";
				
				// Desem la info a DB user IDCPWS
				$v ["ID"] = $nid;
				$v ["REQUEST_ID"] = "FROM NEW IDCP";
				$v ["TASK_NAME"] = $file ["name"] [0];
				$v ["TRIES"] = 1;
				$v ["STATUS"] = "NEW";
				$v ["CLIENT"] = "MAEC";
				$v ["MLV"] = "IDISC";
				$v ["TASK_STATUS"] = "NEW";
				$v ["LANG"] = $lang;
				$v ["ID_MEMOQ"] = $memoq_id;
				self::$environment->dbcon->setValues ( "C", "DTASKS", $v );
				self::$environment->dbcon->writeLob ( json_encode ( $data ), "DATA", "DTASKS", "ID=" . $nid );
				unset ( $v );
				
				// Desem la info a DB user WEBTRADUC
				$v ["ID_ENTRADA"] = $anid;
				$v ["NOM_FITXER_EMAIL"] = "FROM NEW IDCP";
				$v ["NOM_TASCA"] = $file ["name"] [0];
				$v ["INTENTS"] = 1;
				$v ["ESTAT"] = "NOVA";
				$v ["CLIENT"] = "MAEC";
				$v ["MLV"] = "IDISC";
				self::$environment->dbcon_webtraduc->setValues ( "C", "TASQUES_ENTRADES", $v );
				self::$environment->dbcon_webtraduc->writeLob ( json_encode ( $data ), "FITXER_EMAIL", "TASQUES_ENTRADES", "ID_ENTRADA=" . $anid );
				
				// Llegim el fitxer
				$buffer = null;
				$fp = fopen ( $file ["tmp_name"] [0], "r" );
				$buffer = fread ( $fp, filesize ( $file ["tmp_name"] [0] ) );
				fclose ( $fp );
				// unlink($file["tmp_name"][0]);
				
				// I l'adjuntem a l'user IDCPWS
				$v ["DTASKS_ID"] = $nid;
				$v ["FILENAME"] = $file ["name"] [0];
				self::$environment->dbcon->setValues ( "C", "DTASKS_ATTACHMENTS", $v );
				self::$environment->dbcon->writeLob ( $buffer, "ATTACH", "DTASKS_ATTACHMENTS", "DTASKS_ID=" . $nid );
			}
		}
		return true;
	}
	public static function downloadMemoqFile($id) {
		self::$environment = Environment::getInstance ();
		$dbc = self::$environment->dbcon_webtraduc;
		
		$sql = "SELECT * FROM FITXERS WHERE ID_TASCA='" . $id . "' AND TIPUS='T'";
		$rs = $dbc->execute ( $sql );
		$buffer = null;
		$nom = null;
		if ($rs->fetch ()) {
			// Recupera el fitxer de la BD
			$buffer = $rs->getVal ( "ARXIU" );
			$nom = $rs->getVal ( "NOM" );
		}
		$rs->close ();
		
		$obj = new \stdClass ();
		$obj->file = $buffer;
		$obj->name = $nom;
		$obj->size = strlen ( $obj->file );
		
		$finfo = new \finfo ( FILEINFO_MIME );
		$obj->mime = $finfo->buffer ( $buffer );
		
		return $obj;
	}
	public static function hasFiles($task_id) {
		self::$environment = Environment::getInstance ();
		$dbc = self::$environment->dbcon_webtraduc;
		$sql = "SELECT IFNULL[count(1),0] AS FITXERS FROM FITXERS WHERE ID_TASCA='" . $task_id . "'";
		$rs = $dbc->execute ( $sql );
		$fitxers = 0;
		if ($rs->fetch ()) {
			$fitxers = $rs->getVal ( "FITXERS" );
		}
		if ($fitxers > 0)
			return true;
		
		return false;
	}
	
	public static function listMAECTasks() {
		self::$environment = Environment::getInstance();
		$dbc = self::$environment->dbcon_webtraduc;
		
		$query = new Query(null,$dbc);
		$query->select("TID","t.ID_TASCA");
		$query->select("FREP","t.COMP_REP");
		$query->select("F100","t.COMP_100");
		$query->select("F50","t.COMP_50");
		$query->select("F75","t.COMP_75");
		$query->select("F85","t.COMP_85");
		$query->select("F95","t.COMP_95");
		$query->select("F0","t.COMP_0");
		$query->select("FORECAST","t.HORES_PREVIST");
		$query->select("DESCRIPTION","t.DESCRIPCIO");
		$query->select("STATUS","t.ESTAT");
		$query->select("END_DATE","t.FINAL_PREVIST");
		$query->select("DESTINATION","t.DESTI");
		$query->select("TASK_TYPE","t.TIPUS_TASCA");
		$query->select("WEIGHTED_WORDS",'(((t.COMP_REP+t.COMP_100)*0.06)+(t.COMP_95*0.1)+((t.COMP_85+t.COMP_75)*0.6)+((t.COMP_50+T.COMP_0)*1))');
		$query->select("SOURCE","T.ORIGEN");
		
		$query->filter("END_DATE","t.FINAL_PREVIST"); //END_DATE equal than
		$query->filter("STATUS","t.ESTAT");
		$query->filter("DESCRIPTION","t.DESCRIPCIO");
		$query->filter("TID","t.ID_TASCA");
		$query->filter("TASK_TYPE","T.TIPUS_TASCA");
		$query->filter("SOURCE","T.ORIGEN",self::getIDCPASPPMs());
		
		
		$query->order("END_DATE","t.FINAL_PREVIST");
		$query->order("DESCRIPTION","t.DESCRIPCIO");
		$query->order("STATUS","t.ESTAT");
		$query->order("FORECAST","t.HORES_PREVIST");
		$query->order("TASK_TYPE","T.TIPUS_TASCA");
		
		$query->from("TASQUES T");
		//$query->where("T.ORIGEN")->operator(_OO::EQUAL)->compareTo("'KMT'");
		//$query->where("t.id_tasca")->in()->compareTo("(select ID_TASCA from TASQUES_ENTRADES where client='MAEC' and MLV='IDISC' and ID_TASCA is not null)");
		return $query->paging();
	}
	
	public static function getIDCPASPPMs(){
		self::$environment = Environment::getInstance();
		$dbc = self::$environment->dbcon_webtraduc;
		$query = new Query(null,$dbc);
		$query->select("DR.CODI_RECURS as ID, DR.NOM as NAME");
		$query->order("ID","DR.CODI_RECURS");
		$query->where("DR.PRIORITAT<=6");
		$query->where("DR.TIPUS_RECURS IN ('P','A')");
		$query->from("DADES_RECURS DR");
		
		$query->defaultOrder("+ID");
		return $query->execute();
	}
}