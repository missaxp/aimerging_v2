<?php 
namespace api\system;

use core\Exception\AppException;
use core\resources\Resource;
use common\Table;
use core\Environment;


/**
 * Class chapter which has methods and properties
 * to create/update/delete a chapter and also 
 * create/update/delete a chapter element (such as a tool or skill)
 * @author phidalgo
 * @created 20161702
 */
class Chapter extends Resource{
	/**
	 * Chapter name
	 * @var string
	 */
	public $name;
	
	/**
	 * 
	 * @param integer $cid
	 * @throws AppException
	 */
	public function __construct($cid = null){
		self::$environment = Environment::getInstance();
		if($cid!=null){
			if(!is_numeric($cid)){
				throw new AppException ( Status::S5_InternalServerError, ErrorCode::FieldMustbeNumber, "Chapter ($cid) must be a number");
			}
			$rs = self::$environment->dbcon->execute("SELECT * FROM CHAPTERS WHERE ID=:ID",array("ID" => $cid));
			if($rs->fetch()){
				$this->id = $rs->id;
				$this->name = $rs->name;
				$this->creation_ts = $rs->creation_ts;
				$this->modification_ts = $rs->modification_ts;
				$this->department_id = $rs->department_id;
			}
			$rs->close();
			if($this->id==null){
				throw new AppException ( Status::S5_InternalServerError, ErrorCode::NotFound, "Chapter ($cid) not found");
			}
			$this->new = false;
		}
		else{
			$this->new = true;
		}
	
		parent::__construct(Table::CHAPTERS);
	}
	
	/**
	 * Adds chapter element
	 * @param \stdClass $data
	 */
	public function addElement($data){
		$nid = self::getNewDBId("CHAPTERS_ELEMENTS");
		$vars["ID"] = $nid;
		$vars["CHAPTER_ID"] = $this->id;
		$vars["NAME"] = $data->name;
		self::$environment->dbcon->add("CHAPTERS_ELEMENTS",$vars);
		return true;
	}
	
	/**
	 * Updates chapter element
	 * @param integer $ceid
	 * @param \stdClass $data
	 */
	public function updateElement($ceid,$data){
		$vW["ID"] = $ceid;
		$vars["CHAPTER_ID"] = $this->id;
		$vars["NAME"] = $data->name;
		self::$environment->dbcon->update("CHAPTERS_ELEMENTS",$vars,"ID=:ID",$vW);
		return true;
	}
	
	/**
	 * Delete chapter element
	 * @param integer $ceid
	 */
	public function deleteElement($ceid){
		self::$environment->dbcon->execute("DELETE FROM CHAPTERS_ELEMENTS WHERE ID=:ID",array("ID" => $ceid));
		return true;
	}
}
?>