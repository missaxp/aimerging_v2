<?php

namespace api\crud;

use core\Environment;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;

/**
 * Get,Create,update or delete a group.
 * @author phidalgo
 *
 */
class Group{
	protected static $environment;
	
	public $name;
	
	public $id;
	
	public $creation_ts;
	
	public $modification_ts;
	
	public $new;
	
	/**
	 * 
	 * @var \core\security\group
	 */
	private $securityGroup;
	
	/**
	 * Delete a single group or a multiples groups.
	 * @param integer|array $gids
	 * @return true on success
	 */
	public static function deleteGroup($gids){
		self::$environment = Environment::getInstance();
		if(is_object($gids)){
			foreach($gids as $gid){
				self::$environment->dbcon->execute("DELETE FROM P_GROUPS WHERE ID=:GID",array("GID" => $gid));
			}
		}
		if(is_numeric($gids)){
			self::$environment->dbcon->execute("DELETE FROM P_GROUPS WHERE ID=:GID",array("GID" => $gids));
		}
		return true;
	}
	
	public function __construct($gid = null){
		self::$environment = Environment::getInstance();
		if($gid == null){
			$this->new = true;
			$this->securityGroup = new \core\security\group($gid);
			
			$sql = "SELECT IFNULL[max(id)+1,1] as NID FROM P_GROUPS";
			$rs = self::$environment->dbcon->execute($sql);
			if($rs->fetch()){
				$this->id = $rs->nid;
			}
			$rs->close();
			if($this->id==null){
				throw new AppException(Status::S5_InternalServerError,ErrorCode::InternalServerError,"@LITERAL Can not get new id for new group");
			}
		}
		else{
			$this->id = $gid;
			$this->new = false;
			$this->securityGroup = new \core\security\group($gid);
		}
	}
	
	public function get(){
		$this->creation_ts = $this->securityGroup->creation_ts;
		$this->modification_ts = $this->securityGroup->modification_ts;
		$this->name = $this->securityGroup->name;
		
		$permissions = array();
		
		foreach($this->securityGroup->permission() as $permission){
			$permissions[] = array(
					"table_id" => $permission->table_id,
					"table_name" => self::$environment->tables->_get($permission->table_id)->name,
					"group_p" => $permission->getUnixLike()
			);
		}
		
		return array(
			"id" => $this->id,
			"name" => $this->name,
			"created" => $this->creation_ts,
			"modified" => $this->modification_ts,
			"permissions" => $permissions
		);
	}
	
	public function save($data){
		$var["NAME"] = $data->name;
		if($this->new){
			$var["ID"] = $this->id;
			self::$environment->dbcon->add("P_GROUPS",$var);
		}
		else{
			$vw["GID"] = $this->id;
			self::$environment->dbcon->update("P_GROUPS",$var,"ID=:GID",$vw);
		}
		
		foreach($data->permissions as $table){
			$tableObj = self::$environment->tables->_get($table->table_id); //Fem això pq si el ID no existeix, llença throw.
			self::$environment->dbcon->execute("DELETE FROM P_GROUPS_T WHERE GROUP_ID=:GID AND TABLE_ID=:TID",array("GID" => $this->id,"TID" => $table->table_id));
			$u = (string) $table->p_group->user;
			$g = (string) $table->p_group->group;
			$o = (string) $table->p_group->others;
			
			$var["USER_CREATE"] 		= $u{0}=="1"? "Y":"N";
			$var["USER_READ"] 			= $u{1}=="1"? "Y":"N";
			$var["USER_WRITE"] 			= $u{2}=="1"? "Y":"N";
			$var["USER_DELETE"] 		= $u{3}=="1"? "Y":"N";
			$var["USER_ADMINISTRATE"] 	= $u{4}=="1"? "Y":"N";
			
			$var["GROUP_READ"] 			= $g{1}=="1"? "Y":"N";
			$var["GROUP_WRITE"] 		= $g{2}=="1"? "Y":"N";
			$var["GROUP_DELETE"] 		= $g{3}=="1"? "Y":"N";
			$var["GROUP_ADMINISTRATE"] 	= $g{4}=="1"? "Y":"N";
			
			$var["OTHER_G_READ"] 		= $o{1}=="1"? "Y":"N";
			$var["OTHER_G_WRITE"] 		= $o{2}=="1"? "Y":"N";
			$var["OTHER_G_DELETE"] 		= $o{3}=="1"? "Y":"N";
			$var["OTHER_G_ADMINISTRATE"]= $o{4}=="1"? "Y":"N";
			
			$var["TABLE_ID"] = $table->table_id;
			$var["GROUP_ID"] = $this->id;
			
			self::$environment->dbcon->add("P_GROUPS_T",$var);
			
		}
		return array(
			"ID" => $this->id
		);
	}
	
	public function delete(){
		return self::deleteGroup($this->id);
	}
}
?>