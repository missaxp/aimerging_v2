<?php

namespace api\crud;

use core\Environment;
use core\resources\Resource;
use common\Table;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;

/**
 * Get,Create,update or delete a group.
 * @author phidalgo
 *
 */
class Team extends Resource{
	protected static $environment;
	
	public $name;
	public $user_id;
	public $username;
	public $resources;
	
	public static function getTeamUsersByTeamId($tid){
		$resources = array();
		$fields = "W.USER_ID AS ID,(SELECT USERNAME FROM USERS WHERE ID=W.USER_ID) AS NAME,W.TYPE";
		$rs = self::$environment->dbcon->execute("SELECT $fields FROM WORK_TEAM_USERS W WHERE W.WT_ID=:WID",array("WID" => $tid));
		$resources = $rs->getAll();
		$rs->close();
		return $resources;
	}

	public function __construct($tid = null){
		self::$environment = Environment::getInstance();
		if($tid!=null){
			$fields = "ID,NAME,USER_ID,(SELECT USERNAME FROM USERS WHERE ID=USER_ID) AS USERNAME,CREATION_TS,MODIFICATION_TS";
			$rs = self::$environment->dbcon->execute("SELECT $fields FROM WORK_TEAMS WHERE ID=:ID",array("ID" => $tid));
			if($rs->fetch()){
				$this->id = $rs->id;
				$this->name = $rs->name;
				$this->user_id = $rs->user_id;
				$this->username = $rs->username;
				$this->creation_ts = $rs->creation_ts;
				$this->modification_ts = $rs->modification_ts;
			}
			$rs->close();
			if($this->id==null){
				throw new AppException(Status::S4_NotFound,ErrorCode::NotFound,"@Literal Error: Workteam $tid not found");
			}
			$this->new = false;
			$this->resources = self::getTeamUsersByTeamId($this->id);
			
		}
		parent::__construct(Table::TEAMS);
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \core\resources\Resource::save()
	 */
	public function save(){
		$resources = array();
		if(isset($this->toUpdate->resources)){
			$resources = $this->toUpdate->resources;
			unset($this->toUpdate->resources);
		}
		
		if($this->new){
			$this->toUpdate->user_id = self::$environment->user->id; //Els teams son únics per usuari.
		}
		
		parent::save(); //Assigna nou id a $this->id si és nou recurs.
		
		if(count($resources)>0){
			$sql = "DELETE FROM WORK_TEAM_USERS WHERE WT_ID=:WID";
			self::$environment->dbcon->execute($sql,array("WID" => $this->id));
			foreach($resources as $res){
				$sql = "INSERT INTO WORK_TEAM_USERS (ID,WT_ID,USER_ID,TYPE) VALUES ((SELECT nvl(max(id)+1,1) FROM WORK_TEAM_USERS),:WTID,:USER_ID,:TYPE)";
				self::$environment->dbcon->execute($sql,array("WTID" => $this->id,"USER_ID" => $res->user_id,"TYPE" => $res->user_type));
			}	
		}
		
		if($this->new){
			return array("id" => $this->id);
		}
	}
	
}
?>