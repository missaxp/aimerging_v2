<?php

namespace api\crud;

use core\http\ErrorCode;
use core\http\Status;
use core\Exception\AppException;
use core\Environment;
use common\Table;
use core\resources\Resource;


/**
 * Class User.
 *
 * @author phidalgo
 * @namespace api\crud
 * @created ????
 * @modified 20160207
 */
class User extends Resource{
	public $name;
	public $surname;
	public $email;
	public $password;
	public $role_id;
	public $username;
	public $country_id;
	public $city;
	public $about;
	public $phone_number;
	public $cell_number;
	public $birth_date;
	public $address;
	public $zip_code;
	public $vat;
	public $country_birth_id;
	public $curriculum;
	public $curriculum_text;
	public $curriculum_date;
	public $url;
	public $driving_license;
	public $car;
	public $priority;
	public $status;
	public $nda;
	public $nda_date;
	public $observations;
	public $tp_id;
	public $tp_name;
	public $qualifications = array();
	public $chapters = array();
	public $avatar;
	public $timezone;
	public $ui_setup;
	public $ui_language;
	public $ui_home_page;
	/**
	 * Grup al qual pertany l'usuari
	 * Quan un usuari crei un recurs, serà el grup per defecte d'aquest recurs.
	 * @var integer
	 */
	public $user_group;
	/**
	 * Departament al qual pertany l'usuari.
	 * Quan aquest usuari crei un recurs, será el departament per defecte d'aquest recurs.
	 * @var integer
	 */
	public $user_department;
	
	/**
	 * Check if the string name is already an username
	 * 
	 * @param string $username
	 * @return boolean
	 */
	 public static function checkIfUsernameIsValid($username){
		self::$environment = Environment::getInstance ();
		$sql = "SELECT USERNAME FROM USERS WHERE USERNAME=:UNAME";
		$rs = self::$environment->dbcon->execute($sql,array("UNAME" => $username));
		$rsp = false;
		if(!$rs->fetch()){
			$rsp = true;
		}
		$rs->close();
		return $rsp;
	}
	
	/**
	 * Instead of get all users data, this static function only returns USER_ID. It's more faster than get all uers data.
	 * @param integer $id
	 * @throws AppException
	 */
	public static function getUserIDFromUserName($uname){
		self::$environment = Environment::getInstance ();
		$rs = self::$environment->dbcon->execute("SELECT ID FROM USERS WHERE USERNAME=:USERNAME",array("USERNAME" => $uname));
		if(!$rs->fetch()){
			throw new AppException(Status::S5_InternalServerError,ErrorCode::NotAcceptable,"USER NOT FOUND");
		}
		$uid = $rs->ID;
		$rs->close();
		return $uid;
	}
	
	// --- OPERATIONS ---
	
	/**
	 * Constructor of User class.
	 * It can works with user id or api_key or user name.
	 * Optional param loadall allows to load all the user's information (departments, role permissions, actions,...)
	 *
	 * @param mixed $mixed_id        	
	 * @param bool $loadAll        	
	 *
	 */
	public function __construct($id = null, $loadAll = false) {
		self::$environment = Environment::getInstance ();
		if ($id != null) {
			$fields = "ID,
					NAME,
					SURNAME,
					EMAIL,
					ROLE_ID,
					USERNAME,
					USER_ID as OWNER_UID,
					(SELECT USERNAME FROM USERS WHERE ID=USER_ID) AS OWNER_UNAME,
					USER_GROUP,
					GROUP_ID as OWNER_GID,
					USER_DEPARTMENT,
					DEPARTMENT_ID,
					UI_LANGUAGE,
					UI_HOME_PAGE";
			if(is_numeric($id)){
				$sql = "SELECT $fields FROM USERS WHERE ID=:ID";
				$vars = array("ID" => $id);
			}
			else if(is_string(($id))){
				$sql = "SELECT $fields FROM USERS WHERE USERNAME=:UNAME";
				$vars = array("UNAME" => $id);
			}
			
			
			$rs = self::$environment->dbcon->execute($sql,$vars);
			if($rs->fetch()){
				$this->id = $rs->id; //User iD
				$this->username = $rs->username; //user name
				$this->name = $rs->name; //name
				$this->surname = $rs->surname; //surname
				$this->email = $rs->email; //email
				$this->role_id = $rs->role_id; //User ROLE
				$this->user_group = $rs->user_group; //User GROUP
				$this->user_department = $rs->user_department; //User Department
				$this->department_id = $rs->department_id; //Record Department
				$this->owner_gid = $rs->owner_gid; //Record owner group
				$this->owner_uid = $rs->owner_uid; //Record owner user id
				$this->owner_uname = $rs->owner_uname; //Record owner user name.
				$this->ui_language = $rs->ui_language; //UI Language.
				$this->ui_home_page = $rs->ui_home_page; //UI home page.
			}
			$rs->close();
			
			if ($this->id==null) {
				throw new AppException ( Status::S4_NotFound, ErrorCode::NotFound, "Error USER NOT FOUND. var \$id: $id" );
			}
			$this->new = false; //Sets new property to false. Is a get/set operation.
			
			parent::__construct(Table::USERS);
			if ($loadAll) {
				$this->loadAll();
			}
		}
		else{
			parent::__construct(Table::USERS);
			$this->getOptions();
		}
	}
	
	
	
	/**
	 * Load All available user config.
	 */
	public function loadAll(){
		$this->getOptions();
		$this->getAdditionalData();
		$this->getQualifications();
		$this->getChapters();
	}
	
	/**
	 * Load BASIC UserInformation.
	 * It's usefull to get basic user config, not all. Config like the user UI setup, avatar, role...
	 * @return array
	 */
	public function loadBasic(){
		$this->loadInterfaceSetup(); //Get UI setup.
		$this->getAvatar(); //Get Avatar
		$this->getTimeZone(); //Get User's TZ.
		return array(
			"id" => $this->id,
			"email" => $this->email,
			"department_id" => $this->department_id,
			"name" => $this->name,
			"owner_gid" => $this->owner_gid,
			"owner_uid" => $this->owner_uid,
			"owner_uname" => $this->owner_uname,
			"avatar" => $this->avatar,
			"surname" => $this->surname,
			"timezone" => $this->timezone,
			"ui_setup" => $this->ui_setup,
			"ui_language" => $this->ui_language,
			"ui_home_page" => $this->ui_home_page,
			"user_group" => $this->user_group,
			"user_department" => $this->user_department,
			"username" => $this->username,
			"role" => self::$environment->user->role,
			"group" => $this->getGroup()
			
		);
	}
	
	private function getGroup(){
		$group = new Group($this->user_group);
		return $group->get();
	
	}
	
	private function getTimeZone(){
		$sql = "SELECT TIMEZONE FROM USERS WHERE ID=:USER_ID";
		$rs = self::$environment->dbcon->execute($sql,array("USER_ID" => $this->id));
		if($rs->fetch()){
			$this->timezone = $rs->timezone;
		}
		if($this->timezone==""){
			$this->timezone = null;
		}
		$rs->close();
	}
	
	private function loadInterfaceSetup(){
		$sql = "SELECT CONFIG FROM UI_USER_CONFIG WHERE USER_ID=:USER_ID";
		$rs = self::$environment->dbcon->execute($sql,array("USER_ID" => $this->id));
		if($rs->fetch()){
			$this->ui_setup = $rs->config;
		}
		if($this->ui_setup==""){
			$this->ui_setup = null;
		}
		
		$rs->close();
		return true;
	}
	
	private function getAvatar(){
		$sql = "SELECT AVATAR FROM USERS WHERE ID=:ID";
		$rs = self::$environment->dbcon->execute($sql, array("ID" => $this->id));
		if($rs->fetch()){
			$this->avatar = $rs->avatar;
			if($this->avatar==""){
				$this->avatar = null;
			}
			else{
				$this->avatar = base64_encode($this->avatar);
			}
		}
		$rs->close();
		return true;
	}
	
	/**
	 * Load all user info not only name, username...
	 * Based on user privileges, not all data has to be sent with the response.
	 */
	private function getAdditionalData(){
		$sql = "SELECT COUNTRY_ID,CITY,ABOUT,PHONE_NUMBER,CELL_NUMBER,BIRTH_DATE,ADDRESS,ZIP_CODE,VAT,COUNTRY_BIRTH_ID,DRIVING_LICENSE,CAR,STATUS,OBSERVATIONS,TP_ID,
		PRIORITY,AVATAR,TIMEZONE FROM USERS WHERE ID=:ID";
		$rs = self::$environment->dbcon->execute($sql, array("ID" => $this->id));
		$fetch = $rs->getAll();
		$rs->close();
		foreach ($fetch[0] as $prop => $value){
			if(property_exists(get_class($this),$prop)){
				if($prop=='avatar'){
					if($value==""){
						$this->$prop = null;
					}
					else{
						$this->$prop = base64_encode($value);
					}
				}
				else{
					$this->$prop = $value;
				}
			}
			else{
				throw new AppException ( Status::S5_InternalServerError, ErrorCode::NotImplemented, "Class api\crud\User does not have property $prop, but on database exist this field");
			}
		}
		
		if($this->tp_id!=null){
			$rs = self::$environment->dbcon->execute("SELECT NAME FROM THIRD_PARTY WHERE ID=:TPID",array("TPID" => $this->tp_id));
			if($rs->fetch()){
				$this->tp_name = $rs->name;
			}
			$rs->close();
		}
	}
	
	private function getQualifications(){
		$this->qualifications = QualificationsManagement::_getByUser($this->id);
	}
	
	private function getChapters(){
		$this->chapters = ChapterManagement::getUsersChapter($this->id);
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \core\resources\Resource::getOptions()
	 */
	public function getOptions(){
		parent::getOptions();
		$this->options["languages"] = LanguagesManagement::getLanguages(false);
		$this->options["countries"] = CountryManagement::getCountries(false);
		$this->options["qualifications"] = QualificationsManagement::getQualificationsForUser($this->user_department);
		$this->options["roles"] = RoleManagement::getRoles(false);
		$this->options["chapters"] = ChapterManagement::getChapterAndElements($this->user_department);
		$this->options["groups"] = GroupManagement::_get(false,array("ID","NAME")); //L'usuari ha de tenir això com a opció perque s'ha d'escollir el departament al que pertany l'usuari.
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \core\resources\Resource::set()
	 */
	public function set($data){
		self::$environment->security->table(Table::USERS)->access($this->id)->writable(); //En principi això ja hauria de funcionar. Si falla la comprovació return 401.
		foreach($data as $prop => $value){
			//Comprovem si hem rebut camps del formulari que comencin amb qualification_ si és així, el format és:
			//qualification_:qid = value
			if(startsWith($prop,"qualification_")){
				$daC = new \stdClass();
				$daC->id = str_replace("qualification_","",$prop);
				$daC->value = $value;
				$this->toUpdate->qualifications[] = $daC;
			}	
			if(startsWith($prop,"q_approve_")){
				$daC = new \stdClass();
				$daC->id = str_replace("q_approve_","",$prop);
				$daC->value = $value;
				$this->toUpdate->qualifications_approved[] = $daC;
			}
			if(startsWith($prop,"chapter_")){
				$daC = new \stdClass();
				$daC->id = str_replace("chapter_","",$prop);
				$daC->value = $value;
				$this->toUpdate->chapters[] = $daC;
			}
		}
		
		return parent::set($data);
	}
	
	/**
	 * Override function save.
	 * {@inheritDoc}
	 * @see \core\resources\Resource::save()
	 */
	public function save(){
		$qualifications_save = array();
		$qualifications_approved_save = array();
		$chapters_save = array();
		
		if(isset($this->toUpdate->qualifications)){
			$qualifications_save = $this->toUpdate->qualifications; 
			unset($this->toUpdate->qualifications);
		}
		
		
		if(isset($this->toUpdate->qualifications_approved)){
			$qualifications_approved_save = $this->toUpdate->qualifications_approved; 
			unset($this->toUpdate->qualifications_approved);
		}
		
		
		if(isset($this->toUpdate->chapters)){
			$chapters_save = $this->toUpdate->chapters; 
			unset($this->toUpdate->chapters);
		}
		
		parent::save(); //If new resource, save assign new id.
		
		//Qualifications -> save it.
		foreach($qualifications_save as $q){
			QualificationsManagement::addUserQualification($this->id, $q);
		}
		
		//Approved Qualifications -> save it.
		foreach($qualifications_approved_save as $qid){
			QualificationsManagement::approveQualification(self::$environment->user->id,$qid->id);
		}
		
		//chapters -> save it.
		if(count($chapters_save)>0){
			foreach($chapters_save as $chapter){
				self::$environment->dbcon->execute("DELETE FROM USERS_CHAPTERS WHERE USER_ID=:USER_ID AND CHAPTER_ID=:CID",array("USER_ID" => $this->id,"CID" => $chapter->id));
				foreach($chapter->value as $element){
					$vars = array("USER_ID" => $this->id,"CHAPTER_ID" => $chapter->id,"ELEMENT_ID" => $element);
					self::$environment->dbcon->add("USERS_CHAPTERS",$vars);
				}
				
			}	
		}
		
		if($this->new){
			//If it is a new user, creates a blank insert on UI_USER_CONFIG.
			$vars = array("USER_ID" => $this->id,"APP_ID" => "IDCP_WEB");
			self::$environment->dbcon->add("UI_USER_CONFIG",$vars);
			return array("username" => $this->username,"id" => $this->id);
		}
	}
} /* end of class User */

?>