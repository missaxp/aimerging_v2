<?php
namespace api\crud;

use core\Environment;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;
use common\Table;
use core\resources\Resource;

/**
 * Class thirdParty.
 * This Object contains all the properties of a third party object.
 * Can be also initializated as null and it will return all the properties and available options needed to create a new third party.
 *
 * Contains the properties that describes a Third Party Member of this project.
 * Third Party objects can be a REAL CUSTOMERS or REAL PROVIDERS which adds extra needed properties through creating two additional classes (provider and customer classes).
 * 
 * This class can save updated data or save new data for a unique third party.
 * 
 * @author phidalgo
 * @created 20150910
 * @modified 20160405
 */
class ThirdParty extends Resource{
	public $alias = null;
	public $name = null; //Can not be null
	public $enabled = false;
	public $real_customer = 'N';
	public $potential_customer = 'N';
	public $real_provider = 'N';
	public $potential_provider = 'N';
	public $agent = 'N';
	public $document_id = null;
	public $cif = null;
	public $iva_regime = null;
	public $type = null;
	public $address = null;
	public $zip_code = null;
	public $town = null;
	public $province = null;
	public $region = null;
	public $country_id = null;
	public $phone = null;
	public $fax = null;
	public $web = null;
	public $holding_id = null;
	public $nature_id = null;
	public $source_id = null;
	public $potentiality = 2;
	public $currency_id = null;
	public $attachments_id = array();
	public $avatar;
	public $products = array();
	
	/**@var $customer customer*/
	public $customer;
	/**@var $provider provider*/
	public $provider;
	
	/**
	 * @param integer $id
	 * @param boolean $loadAll
	 * @throws AppException
	 */
	public function __construct($id = null,$loadAll = false){
		parent::__construct(Table::THIRD_PARTY);
		self::$environment = Environment::getInstance ();
		if ($id != null) {
			if ($id == '' || strlen ( $id ) == 0) {
				throw new AppException ( Status::S5_InternalServerError, ErrorCode::InternalServerError, "Error, var \$id empty." );
			}
				
			$rs = self::$environment->dbcon->execute("SELECT * FROM THIRD_PARTY WHERE ID=:ID",array("ID" => $id));
			$fetch = $rs->getAll();
			$rs->close();
			
			if (count($fetch)==0) {
				throw new AppException ( Status::S4_NotFound, ErrorCode::NotFound, "Error ThirdParty NOT FOUND. var \$id: $id" );
			}
			$this->new = false; //Sets new property to false. Is a get/set operation.
			$tp = array_change_key_case_recursive($fetch[0],CASE_LOWER);
			foreach ($tp as $prop => $value){
				if(property_exists('api\crud\ThirdParty',$prop)){
					if($prop=='avatar'){
						if($value==""){
							$this->$prop = null;
						}
						else{
							$this->$prop = base64_encode($value);
						}
					}
					else{
						$this->$prop = $value;
					}
				}
				else{
					if($prop == 'user_id'){
						$this->owner_uid = $value;
					}
					elseif ($prop == 'group_id'){
						$this->owner_gid = $value;
					}
					else{
						throw new AppException ( Status::S5_InternalServerError, ErrorCode::NotImplemented, "Class api\crud\ThirdParty does not have property $prop, but on database exist this field");
					}
				}
			}
			
			$rs = self::$environment->dbcon->execute("SELECT * FROM THIRD_PARTY_PRODUCTS WHERE TP_ID=:ID",array("ID" => $this->id));
			while($rs->fetch()){
				$this->products[] = array("id" => $rs->id,"name" => $rs->name);
			}
			$rs->close();
			
			if ($loadAll) {
				$this->getOptions();
			}
		}
		else{
			$this->getOptions();
		}
		$this->getThirdPartyData();
	}
	
	/**
	 * Get third party full data.
	 */
	public function getThirdPartyData(){
		$this->customer = new customer($this->id);
		$this->provider = new provider($this->id);
		return true;
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \core\resources\Resource::getOptions()
	 */
	public function getOptions(){
		parent::getOptions();
		$this->options["banks"] = BanksManagement::getBanks();
		$this->options["languages"] = LanguagesManagement::getLanguages(false);
		$this->options["countries"] = CountryManagement::getCountries(false);
		$this->options["currency"] = CurrencyManagement::getCurrencies(false);
	}
	

	/**
	 * 
	 * {@inheritDoc}
	 * @see \core\resources\Resource::set()
	 */
	public function set($data){
		foreach($data as $prop => $value){
			if(startsWith($prop,"ct_")!==false){ //This is a customer prop => value.
				$this->customer->set(str_replace("ct_","",$prop), $value);
			}
			elseif(startsWith($prop, "pr_")!==false){ //This is a provider prop => value 
				$this->provider->set(str_replace("pr_","",$prop), $value);
			}
			else{ //This is a Third Party prop => value.
				if(property_exists(get_class($this),$prop)){
					$this->toUpdate->$prop = $value;
				}
				else{ //Check if property starts with dyn_attr. In this case, this prop is a dynamic attribute and it needs to be saved in a different way.
					//Optionally in a new thirdparty we could send the attachments ID uploaded before assign an ID for this new TP.
					if(startsWith($prop, 'attachments')){
						$this->toUpdate->attachments_id = array_filter(explode(",",$value),'strlen'); //http://php.net/manual/en/function.explode.php#111650
					}
				}
			}
		}
		parent::set($data);
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \core\resources\Resource::save()
	 */
	public function save(){
		$products_save = array();
		
		//Isset torna false si $var is null
		if(isset($this->toUpdate->products) || (isset($this->toUpdate->products) &&  is_null($this->toUpdate->products))){
			$products_save = $this->toUpdate->products;
		}
		unset($this->toUpdate->products);
		parent::save(); //If new resource, save assign new id.
		
		if(count($products_save)>0 || is_null($products_save)){
			if(!$this->new){
				self::$environment->dbcon->execute("DELETE FROM THIRD_PARTY_PRODUCTS WHERE TP_ID=:TP_ID",array("TP_ID" => $this->id));
			}
			
			if(!is_null($products_save)){
				foreach($products_save as $product){
					self::$environment->dbcon->execute("INSERT INTO THIRD_PARTY_PRODUCTS (ID,TP_ID,NAME) VALUES((SELECT nvl(max(ID)+1,1) FROM THIRD_PARTY_PRODUCTS),:TP_ID,:NAME)",array("NAME" => $product,"TP_ID" => $this->id));
				}	
			}
		}
		
		//Set the ID in case of we have created a new Third Party, to the customer and provider classes.
		$this->customer->setID($this->id);
		$this->provider->setID($this->id);

		//Only if it is a REAL CUSTOMER, we save it. If not, we delete the customer entry.
		if($this->real_customer=="Y") $this->customer->save();
		else $this->customer->delete();

		//Only if it is a REAL PROVIDER, we save it. If not, we delete the provider entry.
		if($this->real_provider=="Y") $this->provider->save();
		else $this->provider->delete();
		
		if($this->new){
			return array("id" => $this->id);
		}
	}
	
	
	/**
	 * Add multiple Documents to TP
	 * @param array $files
	 */
	public function addDocuments($data){
		return FileManagement::setBinary($this->id, 'thirdparty/'.$this->id, "THIRD_PARTY_DOCUMENTS",$data);
	}
}


/**
 * Class REAL CUSTOMER.
 * Contains the needed properties to add or update a Third Party which it is a customer.
 * @author phidalgo
 * @created 20151112
 * @modified 20160405
 */
class customer extends Resource{
	public $cta_account; //Can not be null
	public $inverse_account;
	public $payment_method; //Can not be null
	public $pay_day1;
	public $pay_day2;
	public $pay_day3;
	public $rate;
	public $rate_mc;
	public $agent_id;
	public $sales_discount = 0; //Default 0
	public $ep_discount = 0; //Default 0
	public $send_address;
	public $send_zip;
	public $send_city;
	public $send_province;
	public $send_country;
	public $print_language; //Can not be null.
	public $section; //Can not be null.
	public $inv_obs;
	public $mant_contract;
	
	public function __construct($tp_id=null){
		if($tp_id!=null){
			$rs = self::$environment->dbcon->execute("SELECT * FROM THIRD_PARTY_CUSTOMER WHERE TP_ID=:ID",array("ID" => $tp_id));
			if($rs->fetch()){
				$this->id = $rs->tp_id;
				$this->cta_account = $rs->cta_account;
				$this->inverse_account = $rs->inverse_account;
				$this->payment_method = $rs->payment_method;
				$this->pay_day1 = $rs->pay_day1;
				$this->pay_day2 = $rs->pay_day2;
				$this->pay_day3 = $rs->pay_day3;
				$this->rate = $rs->rate;
				$this->rate_mc = $rs->rate_mc;
				$this->agent_id = $rs->agent_id;
				$this->sales_discount = $rs->sales_discount;
				$this->ep_discount = $rs->ep_discount;
				$this->send_address = $rs->send_address;
				$this->send_zip = $rs->send_zip;
				$this->send_city = $rs->send_city;
				$this->send_province = $rs->send_province;
				$this->send_country = $rs->send_country;
				$this->print_language = $rs->print_language;
				$this->section = $rs->section;
				$this->inv_obs = $rs->inv_obs;
				$this->mant_contract = $rs->mant_contract;
				$this->new = false;
			}
			else{
				$this->new = true;
			}
			$rs->close();
		}
		parent::__construct(Table::CUSTOMERS);
	}
	
	public function setID($id){
		$this->id = $id;
	}
}


/**
 * Class REAL PROVIDER.
 * Contains the needed properties to add or update a Third Party which it is a provider.
 * @author phidalgo
 * @modified 20160405
 */
class provider extends Resource{
	public $cta_account; //Can not be null
	public $inverse_account;
	public $payment_method; //Can not be null
	public $pay_day1;
	public $pay_day2;
	public $pay_day3;
	public $bussiness_payment = "Y";
	public $section;
	public $gov_tax_box = "N";
	
	public function __construct($tp_id=null){
		if($tp_id!=null){
			$rs = self::$environment->dbcon->execute("SELECT * FROM THIRD_PARTY_PROVIDER WHERE TP_ID=:ID",array("ID" => $tp_id));
			if($rs->fetch()){
				$this->id = $rs->tp_id;
				$this->cta_account = $rs->cta_account;
				$this->inverse_account = $rs->inverse_account;
				$this->payment_method = $rs->payment_method;
				$this->pay_day1 = $rs->pay_day1;
				$this->pay_day2 = $rs->pay_day2;
				$this->pay_day3 = $rs->pay_day3;
				$this->bussiness_payment = $rs->bussiness_payment;
				$this->section = $rs->section;
				$this->gov_tax_box = $rs->gov_tax_box;
				$this->creation_ts = $rs->creation_ts;
				$this->modification_ts = $rs->modification_ts;
				$this->new = false;
			}
			else{
				$this->new = true;
			}
			$rs->close();
		}
		parent::__construct(Table::PROVIDERS);
	}
	
	public function setID($id){
		$this->id = $id;
	}	
}