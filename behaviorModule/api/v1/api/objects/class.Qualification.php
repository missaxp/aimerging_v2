<?php 
namespace api\system;

use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;
use core\resources\Resource;
use common\Table;
use core\Environment;

/**
 * Class qualification which has methods and properties
 * @author phidalgo
 * @created 20160406
 */
class Qualification extends Resource{
	/**
	 * Chapter description
	 * @var string
	 */
	public $description;
	/**
	 * @param integer $cid
	 * @throws AppException
	 */
	public function __construct($qid = null){
		self::$environment = Environment::getInstance();
		if($qid!=null){
			if(!is_numeric($qid)){
				throw new AppException ( Status::S5_InternalServerError, ErrorCode::FieldMustbeNumber, "Qualification ($qid) must be a number");
			}
			$rs = self::$environment->dbcon->execute("SELECT * FROM QUALIFICATIONS WHERE ID=:ID",array("ID" => $qid));
			if($rs->fetch()){
				$this->id = $rs->id;
				$this->description = $rs->description;
				$this->creation_ts = $rs->creation_ts;
				$this->modification_ts = $rs->modification_ts;
				$this->department_id = $rs->department_id;
			}
			$rs->close();
				
			if($this->id==null){
				throw new AppException ( Status::S5_InternalServerError, ErrorCode::NotFound, "Quailfication ($qid) not found");
			}
				
			$this->new = false;
		}
		else{
			$this->new = true;
		}
		parent::__construct(Table::QUALIFICATIONS);
	}
}
?>