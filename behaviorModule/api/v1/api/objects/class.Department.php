<?php

namespace api\crud;

use core\http\Status;
use core\Environment;
use core\http\ErrorCode;
use core\Exception\AppException;

/**
 * Short description of class Department
 *
 * @author phidalgo & ctemporal
 * @namespace api\crud
 */
class Department {
	
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	
	/**
	 * Department Id
	 *
	 * @var Integer
	 */
	public $id = null;
	
	/**
	 * Department name
	 *
	 * @var String
	 */
	public $name = null;
	
	/**
	 * When loadAll bool is true, it will fill this array with the users.
	 *
	 * @var array[User]
	 */
	private $department_users = null;
	public $users_list = null;
	
	// --- OPERATIONS ---
	/**
	 * Gets the Department Information
	 *
	 * mixed_id determines the Department by id or name.
	 * loadAll all is an optional param that determines if we will load automatically all the settings or only instance the class.
	 *
	 * @param mixed $mixed_id        	
	 * @param
	 *        	bool loadAll
	 * @throws AppException
	 */
	public function __construct($mixed_id = null, $loadAll = false) {
		self::$environment = Environment::getInstance ();
		
		if ($mixed_id != null) {
			if (is_numeric ( $mixed_id )) {
				$this->id = $mixed_id;
				$sql = "SELECT * FROM DEPARTMENTS WHERE ID=:ID";
				$rs = self::$environment->dbcon->execute ( $sql, array (
						"ID" => $this->id 
				) );
			} else if (is_string ( $mixed_id )) {
				$this->name = $mixed_id;
				$sql = "SELECT * FROM DEPARTMENTS WHERE NAME=:NAME";
				$rs = self::$environment->dbcon->execute ( $sql, array (
						"NAME" => $this->name 
				) );
			} else {
				throw new AppException ( Status::S5_InternalServerError, ErrorCode::DepartmentIdNotValid );
			}
			
			$fetch = $rs->getAll ();
			$rs->close ();
			if (count ( $fetch ) == 0)
				throw new AppException ( Status::S4_NotFound, ErrorCode::DepartmentNotFound );
			
			$this->id = $fetch[0]["id"];
			$this->name = $fetch[0]["name"];
			
			unset ( $fetch );
			
			if ($loadAll)
				$this->department_users = $this->getUsers ();
		}
	}
	
	/**
	 * Fill $users_list with array of users and id's (not an array of user's object).
	 */
	public function fillUsers() {
		$sql = "SELECT DPT.USER_ID, U.USERNAME,R.NAME AS ROLE_NAME FROM USERS_DEPARTMENTS DPT, USERS U,ROLES R WHERE DEPARTMENT_ID=:DEPARTMENT_ID AND DPT.USER_ID=U.ID AND U.ROLE_ID=R.ID";
		$rs = self::$environment->dbcon->execute ( $sql, array (
				"DEPARTMENT_ID" => $this->id 
		) );
		$fetch = $rs->getAll ();
		$this->users_list = array_change_key_case_recursive($fetch, CASE_LOWER);
		unset ( $fetch );
	}
	
	/**
	 * Get an array of users for this department.
	 * Only available when id is provided on the constructor.
	 *
	 * @return array[User]
	 */
	public function getUsers() {
		if ($this->id != null) {
			$this->department_users = array ();
			
			$sql = "SELECT USER_ID FROM DEPARTMENTS_USERS WHERE DEPARTMENT_ID=:DEPARTMENT_ID";
			$rs = self::$environment->dbcon->execute ( $sql, array (
				"DEPARTMENT_ID" => $this->id 
			));
			
			$fetch = $rs->getAll ();
			
			foreach ( $fetch as $row ) {
				$user = new User ( $row ["USER_ID"] );
				array_push ( $this->department_users, $user );
			}
			$rs->close ();
			
			return $this->department_users;
		}
	}
}
/* end of class Department */

?>