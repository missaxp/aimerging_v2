<?php

namespace api\crud;

use dbconn\dbConn;
use core\Environment;
use core\Exception\AppException;
use core\http\Status;
use query\Query;
use query\_OO;

/**
 * Class Tasks
 *
 * @author phidalgo & ctemporal
 * @namespace api\crud
 */
abstract class Tasks {
	
	/**
	 * Current instance of environment
	 *
	 * @var Environment
	 */
	protected static $environment;
	public static function cesiProjects() {
		self::$environment = Environment::getInstance ();
		
		dbConn::setConnex ( DBCONN_ORACLE, "webtraduc", "webtraduc", "(DESCRIPTION =(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(Host = cesi.iit.idisc.es)(Port = 1521)))(CONNECT_DATA = (SID = idisc) (SERVER = POOLED)))" );
		$dbc = new dbConn ();
		
		$sql = "SELECT CODI FROM SYGESPROJECTES WHERE DEPARTAMENT=:DEPARTAMENT AND OBERT!=:OBERT";
		
		$vars ["DEPARTAMENT"] = "T";
		$vars ["OBERT"] = "Z";
		
		$rs = $dbc->execute ( $sql, $vars );
		
		$fetch = $rs->getAll ();
		$rs->close ();
		
		return $fetch;
	}
	public static function addTask($data) {
		return "hola";
	}
	public static function detail($id) {
		self::$environment = Environment::getInstance ();
		
		//dbConn::setConnex ( DBCONN_ORACLE, "webtraduc", "webtraduc", "(DESCRIPTION =(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(Host = cesi.iit.idisc.es)(Port = 1521)))(CONNECT_DATA = (SID = idisc) (SERVER = POOLED)))" );
		$dbc = self::$environment->dbcon_webtraduc;
		
		$filter_vars ["ID_TASCA"] = $id;
		$sql = "SELECT t.ID_TASCA,t.DESTI, t.ORIGEN,p.CODI,t.ESTAT,t.HORES_PREVIST,t.DESCRIPCIO,OUTDATETIME[t.FINAL_PREVIST] as FINAL_PREVIST FROM TASQUES t,SYGESPROJECTES p";
		$sql .= " where p.CODI=t.projecte AND ID_TASCA=:ID_TASCA";
		
		// Obtain the ROW on the SQL request
		$rs = $dbc->execute ( $sql, $filter_vars );
		$fetch = $rs->getAll ();
		$rs->close ();
		
		if (count ( $fetch ) == 0) {
			throw new AppException ( Status::S4_NotFound, Status::S4_NotFound, "Task not found" );
		}
		
		return $fetch;
	}
	public static function getCurrentIDCPTasks(User $user = null) {
		self::$environment = Environment::getInstance ();
		$query = new Query(null,self::$environment->dbcon_webtraduc);
		
		$query->select("ID_TASCA", "t.ID_TASCA");
		$query->select("DESTI", "t.DESTI");
		$query->select("ORIGEN", "t.ORIGEN");
		$query->select("CODI", "p.CODI");
		$query->select("ESTAT", "t.ESTAT");
		$query->select("HORES_PREVIST", "OUTFLOAT[t.HORES_PREVIST]");
		$query->select("DESCRIPCIO", "t.DESCRIPCIO");
		$query->select("FINAL_PREVIST", "OUTDATETIME[t.FINAL_PREVIST]");
		$query->select("CREADOR", "t.CREADOR");
		$query->select("F0", "t.COMP_0");
		$query->select("F100", "t.COMP_100");
		$query->select("FREP", "t.COMP_REP");
		$query->select("F50", "t.COMP_50");
		$query->select("F75", "t.COMP_75");
		$query->select("F85", "t.COMP_85");
		$query->select("F95", "t.COMP_95");
		$query->select("WEIGHTED_WORDS","sum((t.COMP_0+t.COMP_50)*1+(t.COMP_REP+t.COMP_100)*0.06+(t.COMP_75)*0.6+(t.COMP_85+t.COMP_95)*0.3)");
		
		/**
		 * This is the ALLOWED FIELDS of the WHERE Clause.
		*/
		$query->filter("ORIGEN", 't.ORIGEN');
		$query->filter("DESTI",'t.DESTI');
		$query->filter("PROJECTE",'p.CODI');
		$query->filter("ESTAT",'t.ESTAT');
		$query->filter("HORES_PREVIST",'t.HORES_PREVIST');
		$query->filter("DESCRIPCIO",'t.DESCRIPCIO');
		$query->filter("INSTRUCCIONS",'t.INSTRUCCIONS');
		$query->filter("TIPUS_TASCA",'t.TIPUS_TASCA');
		$query->filter("FINAL_PREVIST",'t.FINAL_PREVIST');
		
		/**
		 * This is the ALLOWED FIELDS of the ORDER BY Clause
		*/
		$query->order("FINAL_PREVIST",'t.FINAL_PREVIST');
		$query->order("DESCRIPCIO",'t.DESCRIPCIO');
		$query->order("ESTAT",'t.ESTAT');
		$query->order("CODI",'p.CODI');
		$query->order("DESTI",'t.DESTI');
		$query->order("HORES_PREVIST",'t.HORES_PREVIST');
		$query->order("CREADOR","t.CREADOR");
		
		$query->from("TASQUES t,SYGESPROJECTES p");
		$query->where("P.CODI")->operator(_OO::EQUAL)->compareTo("t.PROJECTE");
		$query->where("p.obert")->operator(_OO::NOT_EQUAL)->compareTo("'Z'");
		$query->where("p.departament='T'");
		$query->where("p.CODI='214331'");
		
		$query->groupBy("t.ID_TASCA,t.DESTI,t.ORIGEN,p.CODI,t.ESTAT,t.HORES_PREVIST,t.DESCRIPCIO,t.FINAL_PREVIST,t.CREADOR,t.COMP_0,t.COMP_50,t.COMP_75,t.COMP_85,t.COMP_95,t.COMP_100,t.COMP_REP");
	
		return $query->paging();
	}
}
?>