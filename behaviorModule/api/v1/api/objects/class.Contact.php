<?php
namespace api\crud;

use core\Environment;
use core\Exception\AppException;
use core\http\Status;
use core\http\ErrorCode;
use common\Table;
use core\resources\Resource;

/**
 * Class Contact.
 * This Object contains all the properties of a Contact object.
 * Can be also initializated as null and it will return all the properties and available options needed to create a new contact.
 *
 * Contains the properties that describes a Contact Member of this project.
 *
 * This class can save updated data or save new data for a unique contact.
 *
 * @author phidalgo
 * @created 20151130
 * @updated 
 */
class Contact extends Resource{
	protected static $environment = null;
	
	public $tp_id;
	public $tp_name;
	public $name;
	public $surname;
	public $charge;
	public $work_phone;
	public $extension;
	public $cell_phone;
	public $home_phone;
	public $web;
	public $gender;
	public $language_id;
	public $source_id;
	public $type_id;
	public $industry_id;
	public $auto_delivery_note;
	public $username;
	public $password;
	public $address;
	public $zip;
	public $town;
	public $province;
	public $region;
	public $country_id;
	public $avatar;
	public $departments = array();
	
	/**
	 * @param integer $id
	 * @param boolean $loadAll
	 * @throws AppException
	 */
	public function __construct($id = null,$loadAll = false){
		parent::__construct(Table::CONTACTS);
		self::$environment = Environment::getInstance ();
		if ($id != null) {
			if ($id == '' || strlen ( $id ) == 0) {
				throw new AppException ( Status::S5_InternalServerError, ErrorCode::InternalServerError, "Class Contact Error, var \$id empty." );
			}
			
			$fields = "ID,TP_ID,
			(SELECT NAME FROM THIRD_PARTY WHERE ID=TP_ID) AS TP_NAME,
			NAME,SURNAME,CHARGE,WORK_PHONE,EXTENSION,CELL_PHONE,HOME_PHONE,WEB,GENDER,LANGUAGE_ID,SOURCE_ID,INDUSTRY_ID,AUTO_DELIVERY_NOTE,
			USERNAME,PASSWORD,CREATION_TS,MODIFICATION_TS,USER_ID,DEPARTMENT_ID,ADDRESS,TOWN,ZIP,PROVINCE,REGION,COUNTRY_ID,TYPE_ID,AVATAR,GROUP_ID";
			
			
			$rs = self::$environment->dbcon->execute("SELECT $fields FROM CONTACT WHERE ID=:ID",array("ID" => $id));
			$fetch = $rs->getAll();
			$rs->close();
				
			if (count($fetch)==0) {
				throw new AppException ( Status::S4_NotFound, ErrorCode::NotFound, "Error Contact resource NOT FOUND. var \$id: $id" );
			}
			$this->new = false; //Sets new property to false. Is a get/set operation.
			$tp = array_change_key_case_recursive($fetch[0],CASE_LOWER);
			foreach ($tp as $prop => $value){
				if(property_exists('api\crud\Contact',$prop)){
					if($prop=='avatar'){
						if($value==""){
							$this->$prop = null;
						}
						else{
							$this->$prop = base64_encode($value);
						}
					}
					else{
						$this->$prop = $value;
					}
				}
				else{
					if($prop == 'user_id'){
						$this->owner_uid = $value;
					}
					elseif ($prop == 'group_id'){
						$this->owner_gid = $value;
					}
					else{
						throw new AppException ( Status::S5_InternalServerError, ErrorCode::NotImplemented, "Class api\crud\Contact does not have property $prop, but on database exist this field");
					}
				}
			}
			if ($loadAll) {
				$this->getOptions();
			}
		}
		else{
			$this->getOptions();
		}
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see \core\resources\Resource::getOptions()
	 */
	public function getOptions(){
		parent::getOptions();
		$this->options["source"] = ContactSourcesManagement::getContactSources(false);
		$this->options["type"] = ContactTypesManagement::getContactTypes(false);
		$this->options["industries"] = ContactIndustriesManagement::getContactIndustries(false);
		$this->options["languages"] = LanguagesManagement::getLanguages(false);
		$this->options["countries"] = CountryManagement::getCountries(false);
	}

}