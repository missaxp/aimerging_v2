<?php

use model\Task;
use core\AI;
use dataAccess\dao\TaskDAO;
use model\Action;
use model\State;

/**
* @author phidalgo
* @version 1.0
* created on 19 jul. 2018
*/
class AcceptTask extends Action {
	
	
	/**
	 * Executes the behavior of the task, this method should be overwritten by its subclasses. This superclass represents
	 * the accept task action hence, this method will accept a task.
	 * @param Task $task
	 * @return
	 */
	public function executeAction(Task $task){
		$ai = AI::getInstance();
 		$success = $ai->getHarvest()->acceptTask($task);
		
		if($success == false){
			$this->setError("Can not accept task into Source : ".$task->getDataSource()->getSourceName());
		}
		$this->executed = true;
		return $success;
	}
}