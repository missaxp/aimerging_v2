<?php

/**
 *
 * @author zroque
 * @version 1.0
 *          created on 28 jun. 2018
 */
namespace model;

use service\EmailManager;
use core\AI;
use dataAccess\dao\ProcessDAO;
use dataAccess\dao\SourceDAO;

include_once BaseDir . "/model/NotificationMailAssets.php";
require_once BaseDir . '/common/EmailManager.php';
require_once BaseDir.'/dataAccess/dao/TaskDAO.php';
class ConfirmEmailAction extends Action {
	protected $addBasicTaskInfo;
	protected $addSourceFile;
	protected $idConfirmEmail;
	protected $sendTo;
	protected $subject;
	private $emailManager;
	private $domain;
	const SUBJECT = "Confirmation required for this task";
	public $content = "Your confirmation is required for this task<br>
					Please go to the following link to confirm the task <br>";
	private $mailType = NotificationMailType::NEUTRAL;

	public function __construct(){

		$this->emailManager = new EmailManager();
		$this->domain = AI::getInstance()->getSetup()->webServiceDomain;
		$this->content .= $this->domain;
	}

	/**
	 * 
	 * @return bool
	 */
	public function getAddBasicTaskInfo(){

		return $this->addBasicTaskInfo;
	}

	/**
	 *
	 * @return bool
	 */
	public function getAddSourceFile(){

		return $this->addSourceFile;
	}

	public function setAddBasicTaskInfo($addBasicTaskInfo){

		$this->addBasicTaskInfo = $addBasicTaskInfo;
	}

	public function setAddSourceFile($addSourceFile){

		$this->addSourceFile = $addSourceFile;
	}

	/**
	 * 
	 * @return int
	 */
	public function getIdConfirmEmail(){

		return $this->idConfirmEmail;
	}

	public function setIdConfirmEmail($idConfirmEmail){

		$this->idConfirmEmail = $idConfirmEmail;
	}

	/**
	 * 
	 * @return string
	 */
	public function getSendTo(){

		return $this->sendTo;
	}

	public function setSendTo($sendTo){

		$this->sendTo = $sendTo;
	}

	/**
	 * 
	 * @return string
	 */
	public function getSubject(){

		return $this->subject;
	}

	public function setSubject($subject){
		$this->subject = $subject;
	}

	public function executeAction(Task $task){
		
		$result = false;
		$sendArray = explode(",", $this->sendTo);
		$basicInfo = "";
		$processDao = new ProcessDAO();
		$startDate = $task->getStartDate();
		$startDate->setTimezone(new \DateTimeZone(AI::getInstance()->getSetup()->timeZone_TMS));
		$startDate = $startDate->format("d/m/Y H:i");
		$dueDate = $task->getDueDate();
		$dueDate->setTimezone(new \DateTimeZone(AI::getInstance()->getSetup()->timeZone_TMS));
		$dueDate =$dueDate->format("d/m/Y H:i");
		$resultDb = $processDao->saveTaskConfirmation($task->getUniqueSourceId(), $this->idConfirmEmail, 
		$this->sendTo);
		$mailColor = NotificationMailAssets::getColorByMailType($this->mailType);
		if ($resultDb) {
			$basicInfo = "";
			if ($this->addBasicTaskInfo) {
				$bodyConstruct = "<html lang='en'><head>";
        		$bodyConstruct .= "</head><body style='background-color: #ddd'>";
        		$bodyConstruct .= "<center><div class=\"container\" width='600px' style='width: 600px;display: block;margin: 0 auto;background-color: #fff; text-align: left'><p class=\"header\" style='text-align: center;background-color: $mailColor;margin: 0;'>";
        		$bodyConstruct .= "<img style='padding: 5px;background-color: white;border-radius: 50%;position: relative;top: 25px;' width='50px' height='50px' src='" . NotificationMailAssets::getImageByMailType($this->mailType) . "'>";
        	$bodyConstruct .= "</p><div class=\"wrapper\" style='font-family: Arial, serif;padding: 1em;'>";

       
			$bodyConstruct .= "<h2 style='text-align: center;'>Confirmation required for this task</h2>";
			$bodyConstruct .= "<p style='text-align: center;' id=\"idcpLink\">";
            $bodyConstruct .= "<span style=\"background-color: $mailColor;font-weight: bold;display: inline-block;padding: .5em;border-radius: 3px;\">";
            $bodyConstruct .= "<a style='color: white;text-decoration: none;' href=#confirmation_link#>Confirmation link</a>";
            $bodyConstruct .= "</span>";
            $bodyConstruct .= "</p>";
            $bodyConstruct .= "<h3 style='text-align: center;'>Your confirmation is required for this task<br>
			Please go to the following link to confirm the task <br></h3>";
			$bodyConstruct .= "<h4>Task info:</h4>";

            $bodyConstruct .= "<div class=\"taskProperties\" style='text-align: center;'>";
            $bodyConstruct .= "<table style='display: inline-block;border-spacing: 10px 5px;'>";
            $bodyConstruct .= "<tr><td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: .8em;'>Title:</td><td>" . $task->getTitle() . "</td></tr>";
            $bodyConstruct .= "<tr><td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: .8em;'>Start date:</td><td>" . $startDate . "</td></tr>";
            $bodyConstruct .= "<tr><td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: .8em;'>Due date:</td><td>" . $dueDate . "</td></tr>";
            $bodyConstruct .= "<tr><td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: .8em;'>Source language:</td><td>" . $task->getSourceLanguage()->getLanguageName() . "</td></tr>";
            $targets = "";
            foreach($task->getTargetsLanguage() as $target) {
                $targets .= $target->getLanguageName() . "<br>";
            }
            $bodyConstruct .= "<tr><td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: .8em;'>Target language:</td><td>" . $targets . "</td></tr>";
            $bodyConstruct .= "</table>";
            $bodyConstruct .= "</div>";
            foreach($task->getTargetsLanguage() as $target) {
                $bodyConstruct .= "<h4>Word count for " . $target->getLanguageName() . ":</h4>";
                $bodyConstruct .= "<div class=\"taskProperties\" style='text-align: center;'>";
                $bodyConstruct .= "<table style='display: inline-block;border-spacing: 0;'>";
                $bodyConstruct .= $this->getRowsFromAnalisys($target->getAnalysis());
                $bodyConstruct .= "</table>";
                $bodyConstruct .= "</div>";
            }
			if ($this->addSourceFile) {
				foreach($task->getFilesToTranslate() as $file) {
					$this->emailManager->addAttachment($file->getPath(), $file->getFileName());
				}
			}
			foreach($sendArray as $send) {
				$token = md5($task->getUniqueSourceId() . $send);
				$confirmationUrl = (string)$this->domain . $token; 
				$bodyConstruct = str_replace("#confirmation_link#",$confirmationUrl,$bodyConstruct);
				$result = $this->emailManager->sendMail($send, $this->parseTaskWithWildCards($this->getSubject(),$task), $bodyConstruct);
			}
		}
		else{
			$this->setError("Can not save Task Confirmation into DataBase");
		}
		$this->executed = true;
		if(!$result){
		    $this->setError("Unexpected error while sending email");
        }
		return $result;

}
}

private function getRowsFromAnalisys(Analysis $analysis){
	$string = "";


	$string .= "<tr style='background-color: #ffffff; border-bottom: 1px solid black;'>";
	$string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "ICE:" . "</td>";
	$string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getPercentage_101() . "</td>";
	$string .= "</tr>";

	$string .= "<tr style='background-color: #ededed; border-bottom: 1px solid black;'>";
	$string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "Repetitions:" . "</td>";
	$string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getRepetition() . "</td>";
	$string .= "</tr>";

	$string .= "<tr style='background-color: #ffffff; border-bottom: 1px solid black;'>";
	$string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "100%:" . "</td>";
	$string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getPercentage_100() . "</td>";
	$string .= "</tr>";

	$string .= "<tr style='background-color: #ededed; border-bottom: 1px solid black;'>";
	$string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "95%:" . "</td>";
	$string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getPercentage_95() . "</td>";
	$string .= "</tr>";

	$string .= "<tr style='background-color: #ffffff; border-bottom: 1px solid black;'>";
	$string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "85%:" . "</td>";
	$string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getPercentage_85() . "</td>";
	$string .= "</tr>";


	$string .= "<tr style='background-color: #ededed; border-bottom: 1px solid black;'>";
	$string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "75%:" . "</td>";
	$string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getPercentage_75() . "</td>";
	$string .= "</tr>";

	$string .= "<tr style='background-color: #ffffff; border-bottom: 1px solid black;'>";
	$string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "50%:" . "</td>";
	$string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getPercentage_50() . "</td>";
	$string .= "</tr>";


	$string .= "<tr style='background-color: #ededed; border-bottom: 1px solid black;'>";
	$string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "Not Match:" . "</td>";
	$string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getNotMatch() . "</td>";
	$string .= "</tr>";

	$string .= "<tr style='background-color: #ffffff; border-bottom: 1px solid black;'>";
	$string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "Minutes:" . "</td>";
	$string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getMinute() . "</td>";
	$string .= "</tr>";

	$string .= "<tr style='background-color: #ededed; border-bottom: 1px solid black;'>";
	$string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "Total words:" . "</td>";
	$string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getTotalWords() . "</td>";
	$string .= "</tr>";

	$string .= "<tr style='background-color: #ffffff; border-bottom: 1px solid black;'>";
	$string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "Weighted words:" . "</td>";
	$string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getWeightedWord() . "</td>";
	$string .= "</tr>";

	return $string;
}

}
?>