<?php

/**
 *
 * @author zroque
 * @version 1.0
 *          created on 5 jul. 2018
 */
namespace behaviorModule\actionsModule;

use model\Action;
use model\Task;
use service\EmailManager;

require_once BaseDir. '/model/Action.php';
require_once BaseDir . '/common/EmailManager.php';
//require_once $_SERVER["DOCUMENT_ROOT"] . "/PHPMailer_5.2.0/class.phpmailer.php";

require_once $_SERVER["DOCUMENT_ROOT"] . "/PHPMailer/src/PHPMailer.php";
require_once BaseDir.'/dataAccess/dao/TaskDAO.php';
class EmailAction extends Action {
	protected $content;
	protected $sendTo;
	protected $subject;
	protected $idEmailAction;
	protected $idProcess;
	protected $idActions;
	protected $addBasicTaskInfo;
	protected $addSourceFile;
	private $emailManager;
	

	public function __construct(){

		$this->emailManager = new EmailManager();
		parent::__construct();
	}

	/**
	 * 
	 * @return string
	 */
	public function getContent(){

		return $this->content;
	}

	/**
	 *
	 * @return string
	 */
	public function getSendTo(){

		return $this->sendTo;
	}

	/**
	 *
	 * @return string
	 */
	public function getSubject(){

		return $this->subject;
	}

	/**
	 * 
	 * @return int
	 */
	public function getIdEmailAction(){

		return $this->idEmailAction;
	}

	/**
	 *
	 * @return int
	 */
	public function getIdProcess(){

		return $this->idProcess;
	}

	/**
	 *
	 * @return int
	 */
	public function getIdActions(){

		return $this->idActions;
	}

	public function setContent($content){

		$this->content = $content;
	}

	public function setSendTo($sendTo){

		$this->sendTo = $sendTo;
	}

	public function setSubject($subject){

		$this->subject = $subject;
	}

	public function setIdEmailAction($idEmailAction){

		$this->idEmailAction = $idEmailAction;
	}

	public function setIdProcess($idProcess){

		$this->idProcess = $idProcess;
	}

	public function setIdActions($idActions){

		$this->idActions = $idActions;
	}

	/**
	 * 
	 * @return bool
	 */
	public function getAddBasicTaskInfo(){

		return $this->addBasicTaskInfo;
	}

	/**
	 *
	 * @return bool
	 */
	public function getAddSourceFile(){

		return $this->addSourceFile;
	}

	public function setAddBasicTaskInfo($addBasicTaskInfo){

		$this->addBasicTaskInfo = $addBasicTaskInfo;
	}

	public function setAddSourceFile($addSourceFile){

		$this->addSourceFile = $addSourceFile;
	}

	/**
	 *
	 * @return bool
	 */
	private function checkIntegrity(){

		return (isset($this->content) && isset($this->sendTo) && isset($this->subject));
	}

	public function executeAction(Task $task){
		$basicInfo = "";
		$success = false;
		if($this->addBasicTaskInfo){
			$basicInfo = "<p>Task info: </p>";
			$basicInfo .= "<p>Title: ".$task->getTitle()."</p>";
			$basicInfo .= "<p>Start date: ".$task->getStartDate()->format("Y-m-d H:i:s")."</p>";
			$basicInfo .= "<p>Due date: ".$task->getDueDate()->format("Y-m-d H:i:s")."</p>";
			$basicInfo .= "<p>Source language: ".$task->getSourceLanguage()->getLanguageName()."</p>";
			$targets = "";
			foreach($task->getTargetsLanguage() as $target){
				$targets .= $target->getLanguageName();
			}
			$basicInfo .= "<p>Target language: ".$targets."</p>";
			
		}
		if($this->addSourceFile){
			foreach($task->getFilesToTranslate() as $file){
				$this->emailManager->addAttachment($file->getPath(), $file->getFileName());
				
			}
		}
		$success = $this->emailManager->sendMail($this->sendTo, $this->subject, $this->content.$basicInfo);
		
		if(!$success){
			$this->setError("Can not sent the email");
		}
		
		$this->executed = true;
		return $success;
	}
}
?>