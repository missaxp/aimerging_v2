<?php
use dataAccess\dao\ProcessDAO;
use behaviorModule\Behavior;
use dataAccess\dao\TaskDAO;
use core\AI;

/**
 *
 * @author zroque
 * @version 1.0
 *          created on 11 jul. 2018
 */
include_once BaseDir . '/dataAccess/dao/ProcessDAO.php';
include_once BaseDir . '/dataAccess/dao/TaskDAO.php';
include_once BaseDir . '/behaviorModule/Behavior.php';

/**
 * Receives a token, saves the confirmation and resumes the actions of the process.
 * @param string $token
 * @return string
 */
function getTokenMail(string $token){
	try{
		$processDao = new ProcessDAO();
		$result = $processDao->getTokenEmail($token);
		$answer = false;
		$rsp = "";
		if ($result !== false) {
			$answer = $processDao->confirmTask($result["uniqueSourceId"], $result["idConfirmEmail"], $result["tokenEmail"]);
			if ($answer["answer"]) {
				$behavior = AI::getInstance()->getBehavior();
				$tDao = new TaskDAO();
				$task = $tDao->getTask($result["uniqueSourceId"]);
				if ($task->getUniqueSourceId() !== null) {
					$behavior->resumeProcessActions($task);
					$rsp = $answer["description"];
				}
			} else {
				$rsp = $answer["description"];
			}
		}
	}
	catch(\dataAccess\SQLException $de){
		die(var_dump($e));
	}
	catch(Exception $e){
		die(var_dump($e));	
	}
	
	
	return $rsp;
}
?>