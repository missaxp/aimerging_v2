<?php

namespace model;

include_once BaseDir . '/dataAccess/dao/AIDAO.php';
/**
 *
 * @author Ljimenez
 * @version 1.0
 * created on 22 jun. 2018
 */
use behaviorModule\Behavior;
use common\exceptions\AIException;
use dataAccess\dao\ActionCreateTMSResponseDAO;
use dataAccess\dao\ProcessDAO;
use dataAccess\dao\SourceDAO;
use core\AI;
use dataAccess\dao\AIDAO;
use dataAccess\ConnectionDB;
use dataAccess\SQLException;
use Exception;
use Functions;
use Throwable;

class ActionCreate extends Action {
	protected $idCreate;
	protected $nameTask;
	protected $dueDate;
	protected $enableOverWrite;
	protected $maxWords;
	/**
	 *
	 * @var PropertyAction[]
	 */
	protected $propertyActions;

	/**
	 *
	 * @var Analysis
	 */
	protected $overWrite;
	protected $description;
	protected $uploadOriginalMessageAsReferenceFile;
	protected $appendWCProofForecast;
	protected $appendMachineTransTo;
	protected $addIceWC;
	protected $fuzzyOverwrite;
	protected $fileOverwrite;
	


	/**
	 * @return mixed
	 */
	public function getFileOverwrite(){
		return $this->fileOverwrite;
	}

	/**
	 * @param mixed $fileOverwrite
	 */
	public function setFileOverwrite($fileOverwrite){
		$this->fileOverwrite = $fileOverwrite;
	}

	
    /**
     * @return mixed
     */
    public function getFuzzyOverwrite()
    {
        return $this->fuzzyOverwrite;
    }

    /**
     * @param mixed $fuzzyOverwrite
     */
    public function setFuzzyOverwrite($fuzzyOverwrite)
    {
        $this->fuzzyOverwrite = $fuzzyOverwrite;
    }


	public function __construct(){

	}

	public function getDescription(){

		return $this->description;
	}

	public function setDescription($description){

		$this->description = $description;
	}

	/**
	 *
	 * @return mixed
	 */
	public function getIdCreate(){

		return $this->idCreate;
	}

	/**
	 *
	 * @return mixed
	 */
	public function getNameTask(){

		return $this->nameTask;
	}

	/**
	 *
	 * @return mixed
	 */
	public function getDueDate(){

		return $this->dueDate;
	}

	/**
	 *
	 * @return mixed
	 */
	public function isOverWrite(){

		return $this->enableOverWrite;
	}

	/**
	 *
	 * @param mixed $idCreate
	 */
	public function setIdCreate($idCreate){

		$this->idCreate = $idCreate;
	}

	/**
	 *
	 * @param mixed $nameTask
	 */
	public function setNameTask($nameTask){

		$this->nameTask = $nameTask;
	}

	/**
	 *
	 * @param mixed $dueDate
	 */
	public function setDueDate($dueDate){

		$this->dueDate = $dueDate;
	}

	/**
	 *
	 * @param mixed $enableOW
	 */
	public function enableOverwrite($enableOW){

		$this->enableOverWrite = $enableOW;
	}

	public function getEnableOverWrite(){

		return $this->enableOverWrite;
	}

	public function getPropertyActions(){

		return $this->propertyActions;
	}

	public function getOverWrite(){

		return $this->overWrite;
	}

	public function setEnableOverWrite($enableOverWrite){

		$this->enableOverWrite = $enableOverWrite;
	}

	public function setPropertyActions($propertyActions){

		$this->propertyActions = $propertyActions;
	}

	public function setOverWrite($overWrite){

		$this->overWrite = $overWrite;
	}

	public function getMaxWords(){

		return $this->maxWords;
	}

	public function setMaxWords($maxWords){

		$this->maxWords = $maxWords;
	}

	/**
	 *
	 * @return bool
	 */
	public function getUploadOriginalMessageAsReferenceFile(){

		return $this->uploadOriginalMessageAsReferenceFile;
	}

	/**
	 *
	 * @param bool $uploadOriginalMessageAsReferenceFile
	 */
	public function setUploadOriginalMessageAsReferenceFile($uploadOriginalMessageAsReferenceFile){

		$this->uploadOriginalMessageAsReferenceFile = $uploadOriginalMessageAsReferenceFile;
	}

	/**
	 *
	 * @return mixed
	 */
	public function getAppendWCProofForecast(){

		return $this->appendWCProofForecast;
	}

	/**
	 *
	 * @param mixed $appendWCProofForecast
	 */
	public function setAppendWCProofForecast($appendWCProofForecast){

		$this->appendWCProofForecast = $appendWCProofForecast;
	}

	/**
	 *
	 * @return string
	 */
	public function getAppendMachineTransTo(){

		return $this->appendMachineTransTo;
	}

	/**
	 *
	 * @param string $appendMachineTransTo
	 */
	public function setAppendMachineTransTo($appendMachineTransTo){

		$this->appendMachineTransTo = $appendMachineTransTo;
	}

	/**
	 *
	 * @return bool
	 */
	public function getAddIceWC(){

		return $this->addIceWC;
	}

	/**
	 *
	 * @param bool $addIceWC
	 */
	public function setAddIceWC($addIceWC){

		$this->addIceWC = $addIceWC;
	}

	public function executeAction(Task $task){

		// TODO:: TMS throws tmsException, la acción tal vez debería controlar esta excepción y si ocurre, set error en la classe Actions.
		// TODO:: Los warnings y errors de TMS deberían pasasrse a Warnings y errors del objeto actions.
		$tms = Behavior::tms($task, AI::getInstance()->getSetup()->tms_develoment);

		// Propiedades genericas de tarea para cualquier TMS.
		$createActionTaskname = $this->parseTaskWithWildCards($this->nameTask, $task);
		// $createActionTaskMessage = urldecode($task->getMenssage());
		$createActionDescription = $this->parseTaskWithWildCards($this->description, $task);
		$tms->setTitle($createActionTaskname); // TODO:: El titulo no debe cogerlo de la configuración del createAction para este proceso?
		$tms->setDescription($createActionDescription); // La descripción no debe cogerlo de la configuración del createAction para este proceso? NOTA:: getMenssage está mal escrito.
		$tms->setSourceLanguage($task->getSourceLanguage());
		$tms->setTargetLanguages($task->getTargetsLanguage());
		$tms->setDueDate($this->calculateDueDate($task->getStartDate(), $task->getDueDate()));

		// Propiedades específicas de este TMS.
		foreach($this->propertyActions as &$property) {
			$propValue = $property->getPropetyValue();

			// Una propiedad dinámica es aquella propiedad específica del TMS que queremos configurar con algun valor de la tarea.
			// Por ejemplo, en el TMS IDCP la Task START_DATE, propiedad del TMS-IDCP, queremos que su valor sea el START_DATE de la tarea.
			if ($property->getPropertyType() == PropertyAction::_TYPE_DYNAMIC) {

				if (strpos($propValue, ".") !== false) {
					$dprop = explode(".", $propValue);
				} else {
					$dprop = array(
							$propValue
					);
				}

				$propertyFound = false;
				if (count($dprop) > 1) {
					foreach($task->getAditionalProperties() as $aprop) {
						if ($aprop->getPropertyName() == $dprop[1]) {
							$propertyFound = true;
							$propValue = $aprop->getPropertyValue();
							break;
						}
					}
				} else {
					$getPropName = "get" . ucfirst($dprop[0]);
					if (method_exists($task, $getPropName)) {
						$propValue = $task->$getPropName();
						$propertyFound = true;
					} else {
						$propertyFound = false;
					}
				}

				// TODO:: Comprovar que $propValue es un string.

				if ($propertyFound) {
                    if ($propertyFound instanceof DateTime) {
                        $tms->setProperty($property->getPropertyName(), $propValue);
					} else {
						$tms->setProperty($property->getPropertyName(), $propValue);
					}
				} else {
					$this->setWarning("TMS Dynamic Property $propValue Not found");
				}
			} else { // Si la propiedad es manual, asignaremos la propiedad del TMS-IDCP al valor guardado en BD.
				$tms->setProperty($property->getPropertyName(), $propValue);
			}
		}
		if ($this->enableOverWrite && ($this->maxWords > $task->getTargetsLanguage()[0]->getAnalysis()->getWeightedWord())) {
			$tms->setWordCount($this->overWrite);
		} else {
			// Append Machine translation words to toher fuzzy

			if ($this->fuzzyOverwrite!= null) {
				foreach($task->getTargetsLanguage() as $targetLanguage) {
					$targetLanguage->getAnalysis()->appendFuzzyRelations($this->fuzzyOverwrite);
				}
			}

			if ($this->fileOverwrite!= null) {
				$discarded_files = $task->overwriteFileRelations($this->fileOverwrite);
			}
			
			
			if($this->addIceWC){
				$tms->setAddIceWC(true);
			}
			// TODO:: Al yo programar la clase TMS cometí un error al no tener en cuenta que los contages son por IDIOMA. Actualizaré la clase IDCP para que contemple multiples analisis uno para cada idioma.
			$tms->setWordCount();
		}
		$tms->setFilesToTranslate($task->getFilesToTranslate());
		if(isset($discarded_files)){
			$tms->setDiscardedFiles($discarded_files);
		}	
		$referenceFiles = array_merge($task->getFilesForReference(), $task->getTranslationMemories());
        $task->setFilesForReference($referenceFiles);

		

		$instructions = null;
		if ($this->uploadOriginalMessageAsReferenceFile && !empty($task->getMenssage())) {
			$referenceFiles = $task->getFilesForReference();
			$instructions = $this->createMessageAsReferenceFile($this->description, $task->getMenssage());
			$referenceFiles[] = $instructions;
		}

		/* check if the property is true and set it to the TMS */
		if ($this->appendWCProofForecast) {
			$tms->setAppendWCProofForecast(true);
		}

		$tms->setFilesForReference($referenceFiles);
		// die(var_dump($tms));
		$result = $tms->create();
		// $result = true;
		if ($tms->hasErrors()) {
			foreach($tms->getError() as $error) {
				$this->setError($error);
			}
		}

		if ($tms->hasWarnings()) {
			foreach($tms->getWarning() as $warning) {
				$this->setWarning($warning);
			}
		}
		// delete the instructions file
		if ($instructions != null && $this->uploadOriginalMessageAsReferenceFile) {
			@unlink($instructions->getPath());
		}

		if (count($tms->getTMSResponse()) > 0) {
            $tmsResponse = implode(",", $tms->getTMSResponse());
            $dao = new ActionCreateTMSResponseDAO();
            try {
                $dao->saveActionCreateTMSResponse($this->idCreate, $task->getUniqueSourceId(), $tmsResponse);
            } catch (AIException $e) {
                $this->setWarning("AI::Cannot connect to IDCP. Internal Error.");
                Functions::logException("AI::Create Action. Can not add TMS response into DB. message: " . $e->getMessage(), Functions::SEVERE, __CLASS__, $e);
            }

            if($this->proceessHasRuleWorkgroupLoad()){
                $this->addToGlobalWorkgroupLoad($task, $this->idProcess);
            }
        }

		$this->executed = true;
		return $result;
	}

	private function addToGlobalWorkgroupLoad($task, $idProcess){
        $processDao = new ProcessDAO();
        $currentLoad = $processDao->getGlobalWorkLoad();
        $forecast = Rule::calculateForecast($task, $idProcess);
        $possibleLoad = $currentLoad + $forecast;
        try {
            $processDao->setGlobalWorkLoadCount($possibleLoad);
        } catch (AIException $e) {
            Functions::logException($e->getMessage(), Functions::WARNING, __CLASS__, $e);
        }

    }

    private function proceessHasRuleWorkgroupLoad(){
        $processDao = new ProcessDAO();
        return $processDao->processHasWorkgroupMaxLoadRule($this->idProcess);
    }


	/**
	 *
	 * @param mixed $start
	 * @param mixed $end
	 * @return \DateTime
	 */
	public function calculateDueDate($start, $end){
		$start = date_format($start, 'Y-m-d H:i:s');
		$end = date_format($end, 'Y-m-d H:i:s');
		if (is_numeric($this->dueDate)) {
			$start = strtotime($start);
			$end = strtotime($end);
			$total = ($end - $start);
			$operator = substr($this->dueDate, 0, 1);
			$due = substr($this->dueDate, 1);
			$sub = ($due * $total) / 100;
			$interval = 0;
			if ($operator == "-") {
				$interval = $end - $sub;
			} else {
				$interval = $end + $sub;
			}

			$newDate = new \DateTime(date('Y/m/d H:i:s', $interval));
			return $newDate;
		} else {
			$newDate = new \DateTime($end);
			$operator = substr($this->dueDate, 0, 1);
			$interval = substr($this->dueDate, 1);
			switch ($operator) {
				case "-" :
					$newDate->sub(new \DateInterval($interval));
					break;
				case "+" :
					$newDate->add(new \DateInterval($interval));
					break;
			}
			return $newDate;
		}
	}

	/**
	 * Creates an HTML file with the content of the original task message
	 *
	 * @param string $taskDescription
	 * @param string $message
	 * @return \model\File
	 * @return
	 */
	private function createMessageAsReferenceFile($taskDescription, $message){

		$data = urldecode($message);
		

		
		$path = AI::getInstance()->getSetup()->repository . "/instructions.html";
		$file = fopen($path, "w+");
		if ($file != null) {
			$html_file = '<!DOCTYPE html>
                                      <html lang="en">
                                        <head>
                                            <meta charset="utf-8" />
                                            <title>Instructions for ' . $taskDescription . '</title>
                                        </head>
                                        <body>
                                            <div>' . $data . '</div>
                                        </body>
                                      </html>';
			fputs($file, $html_file);
			fclose($file);
			$newFile = new File();
			$newFile->setPath($path);
			$newFile->setSuccessfulDownload(1);
		} else {
			$AIDao = new AIDAO();
			$AIDao->saveError(Functions::WARNING, "The file could not be created", "File => " . __FILE__ . "    Method => " . __METHOD__ . "    Line => " . __LINE__);
		}
		return $newFile;
	}
}
?>