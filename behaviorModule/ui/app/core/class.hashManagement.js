(function(win){
	function idcp_hashManagement(){
		this.process = function(e){
			//console.log("this.process CALLED: " +window.location.hash);
			idcp.showingError=false; //Every page changes, we put this var on false to say to the app that show the error if any.
			$('#content_loading').removeClass("no-visible").addClass("visible");
			$('#loading-icon-nav-var').css("opacity","1");
			
			idcp.routing._get() //Get map route and download required files if needed.
			.done(function(map,vars){
				idcp.history._add();
				idcp.breadcrumb.update(); //Update Aria Bar.
				idcp.require(map,idcp.UI.layout_content,vars).done(function(){
					idcp.menu.openMenu();
					//idcp.loading.remove();
					$('#content_loading').removeClass("visible").addClass("no-visible");
					$i.intervals.app._add('NAV_VAR_ICON',function(){
						$('#loading-icon-nav-var').css("opacity","0");
						$i.intervals.app.clear('NAV_VAR_ICON');
					},1000);
				});
			});
		}
		win.onhashchange = this.process; //Add hash event listener controller.
		
		/**
		 * Change the current hash.
		 * @param string hash -> new hash
		 * @para boolean goTo -> if false, set hash but not follow it (default true). 
		 */
		this._set = function(hash,goTo){
			goTo = typeof(goTo)!=='undefined'? goTo:true;
			if(goTo && win.onhashchange==null){
				console.log("Hem de seguir el link...");
				win.onhashchange = this.process;
			}
			else if(!goTo){
				win.onhashchange = null;
				console.log("No hem de seguir el link...");
			}
			
			if(hash.substr(0,2)=='./'){
				hash = hash.replace('./','');
				if(document.location.hash.substr(document.location.hash.length-1,1)!='/'){
					hash = document.location.hash + "/" + hash;
				}
				else{
					hash = document.location.hash + hash;
				}
			}
			else if(hash.substr(0,1)=='/'){ //Removes the last hash element and adds the new one.
				hash = hash.replace('/','');
				var ha = document.location.hash.split('/');
				var hr = "";
				for(var i=0;i<ha.length-1;i++){
					console.log(ha[i]);
					hr += ha[i]+"/";
				}
				hr += hash;
				hash = hr;
			}
			if(hash.substr(0,1)=='#'){
				hash = hash.substr(1,hash.length-1);
			}
			
			document.location.href = '#' + hash;
		};
		
		/**
		 * Update last part of hash and  replaces with hash
		 * @param hash string
		 * @param goTo boolean
		 */
		this._updateLast = function(hash,goTo){
			goTo = typeof(goTo)!=='undefined'? goTo:true;
			var currentHash = document.location.hash;
			var hsplit = currentHash.split('/');
			hsplit[hsplit.length-1] = hash;
			this._set(hsplit.join('/'),goTo);
		}
		
		/**
		 * Update hash and follow it.
		 * Change title.
		 * Add vars.
		 * @param link 
		 * @param title
		 * @author phidalgo
		 */
		this.follow = function(link, title,vars,ariaName){
			var t = title || null;
			if(t != null && typeof(t)==='string'){
				idcp.documentTitle(title);
			}
			var v = vars || null;
			if(v!=null){
				window['POST'] = vars;
			}
			
			this._set(link);
		};
		
		this._get = function(){
			return window.location.hash;
		}
	}
	
	idcp.hash = new idcp_hashManagement();
})(window);
