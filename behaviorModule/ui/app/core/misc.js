/**
 * Returns a random string.
 * 
 * @param integer id_length (default = 8)
 * @returns string
 * @author phidalgo
 */
var idscreated = [];
jQuery.makeId = function(id_length){
	var idl = id_length || 8;
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for( var i=0; i < idl; i++ ){
    	text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
};

/**
 * Generate a random string with specified length (8 by default)
 * 
 * push the random string into an array.
 * We will create a random string until the current are not created.
 * 
 * @param integer length
 * @author phidalgo
 * @returns string
 */
idcp.generator = function(length){
	id = $.makeId(length);
	if($.inArray2( id, idscreated )){
		return idcp.generator(length);
	}
	else{
		idscreated.push(id);
		return id;
	}
};

/**
 * Like inArray php.
 * @author phidalgo
 */
jQuery.inArray2 = function (needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
};

/**
 * Like $_GET php function. Get values from URL. No for querystring but also from map routes. /VAR1/VAR2/VAR3/VAR_N
 * 
 * @param string k
 * @returns mixed bool || array | string
 * @author phidalgo
 * 
 * Returns false if there is NO vars for this page.
 * Returns string if the "k" is found on page vars array
 * Returns array if no "k" set and vars found for this page.
 * 
 */
idcp._GET = function(k) {
	var key = k || null;
	var vars = window[idcp.pages.find.visible().pageHash];
	
	if (key!=null && typeof(vars)!=='undefined'){
		var response = false;
		$.each(vars,function(key,value){
			//console.log(key+"=>" + value);
			if(key==k){
				response = value;
				return false;
			}
			
		});
		return response;
	}
	return ((typeof(vars)==='undefined')? false:vars);
};

/**
 * Its a function to pass vars from script to script.
 * Once we request the vars, this vars will be deleted.
 * 
 * @returns POST DATA || false
 */
idcp._POST = function(){
	if(typeof(window['POST'])!=='undefined'){
		var v = window['POST'];
		delete window['POST'];
		return v;
	}
	else{
		return false;
	}
	
};

navigator.sayswho = (function(){
    var ua= navigator.userAgent, tem,
    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
    return M.join(' ');
})();

/*$i.loading = {
	add : function(){
		$('#content_loading').removeClass("no-visible").addClass("visible");
		$('#loading-icon-nav-var').css("opacity","1");
	},
	remove : function(){
		$('#content_loading').removeClass("visible").addClass("no-visible");
		$i.intervals.app._add('NAV_VAR_ICON',function(){
			$('#loading-icon-nav-var').css("opacity","0");
			$i.intervals.app.clear('NAV_VAR_ICON');
		},1000);
	}
};*/

/**
 * Adds a subtitle on the aria bar.
 */
idcp.addSubTittle = function (subtitle) {
	var html = "";
	html += "<small>";
	html += " <i class=\"ace-icon fa fa-angle-double-right\"></i> ";
	html += subtitle;
	html += "</small>";
	
	//$("#page-title").append(html);
	
};

/**
 * Changes the document title.
 * Always adds a titleprefix (see @config).
 * @param title
 */
idcp.documentTitle = function(title){
	document.title = $i._get('titlePrefix') + title;
};



/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/

;( function( $, window, document, undefined )
{
var s = document.body || document.documentElement, s = s.style, prefixAnimation = '', prefixTransition = '';

if( s.WebkitAnimation == '' )	prefixAnimation	 = '-webkit-';
if( s.MozAnimation == '' )		prefixAnimation	 = '-moz-';
if( s.OAnimation == '' )		prefixAnimation	 = '-o-';

if( s.WebkitTransition == '' )	prefixTransition = '-webkit-';
if( s.MozTransition == '' )		prefixTransition = '-moz-';
if( s.OTransition == '' )		prefixTransition = '-o-';

$.fn.extend(
{
	onCSSAnimationEnd: function( callback )
	{
		var $this = $( this ).eq( 0 );
		$this.one( 'webkitAnimationEnd mozAnimationEnd oAnimationEnd oanimationend animationend', callback );
		if( ( prefixAnimation == '' && !( 'animation' in s ) ) || $this.css( prefixAnimation + 'animation-duration' ) == '0s' ) callback();
		return this;
	},
	onCSSTransitionEnd: function( callback )
	{
		var $this = $( this ).eq( 0 );
		$this.one( 'webkitTransitionEnd mozTransitionEnd oTransitionEnd otransitionend transitionend', callback );
		if( ( prefixTransition == '' && !( 'transition' in s ) ) || $this.css( prefixTransition + 'transition-duration' ) == '0s' ) callback();
		return this;
	}
});
})( jQuery, window, document );