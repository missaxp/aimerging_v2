/**
 * Get the logged user information
 * 
 * @returns promise
 */
(function(){
	function idcp_user_management(){
		var dataError = false;
		var basicLoaded = false;
		var fullLoaded = false;
		var userObject = null;
		
		var _this = this;
		var get = function(){
			var externalCallbackFunction = null;
			var getter = function(){
				if(userObject==null){
					return {};
				}
				return userObject;
			};
			
			getter.basic = function(){
				getUserInfo(true);
				return getter;
			};
			
			getter.full = function(){
				getUserInfo(false);
				return getter;
			}
			
			var getUserInfo = function(basic){
				var url = basic? 'users/me/basic':'users/me';
				$i.promise._GET(url)
				.done(function(data){
					var udata = data.data;
					if(udata.ui_setup){
						try{
							udata.ui_setup = JSON.parse(data.data.ui_setup);
						}
						catch(e){
							udata.ui_setup = null;
						}
					}
					responseReceived(udata,basic);
				})
				.fail(function(){
					dataError = true;
					responseReceived(null,basic);
				});
				return true;
			};
			
			function responseReceived(udata,basic){
				if(dataError){
					userObject = null;
				}
				else{
					if(basic) basicLoaded = true;
					else fullLoaded = true;
					userObject = udata;
				}
			}
			
			var iid = null;
			getter.done = function(callback){
				if(!$i._get('webservice').authentication_required){
					callback();
				}
				else{
					if(dataError){
						if(iid!=null){
							clearInterval(iid);
						}
						return callback(false);
					}
					if(basicLoaded || fullLoaded){
						callback(userObject);
					}
					else{
						if(userObject==null){
							getter.basic();
						}
						iid = setInterval(function(){
							if(basicLoaded || fullLoaded){
								clearInterval(iid);
								callback(userObject);
							}
						},50);
					}
				}
			};
			return getter;
		}
		
		this._get = new get();

		
		this._set = function(prop,value){
			if(typeof(userObject[prop])!=='undefined'){
				userObject[prop] = value;
				//console.log("Prop "+prop+", set to :"+ value);
			}
		}
		
		if(idcp.logged){
			this._get.basic();
		}
	}
	
	idcp.user = new idcp_user_management();
	//console.log("class idcp.user ready to be used.");
})();

/**
 * Set page config on the subspace. Page may contain several different kind of data, and the subspace 
 * determines in which property the value will be set.
 * Also save it to WebService and when webservice returns SUCCESS, update $i.user._get().ui_setup[page][subspace];
 * @param subspace string
 * @param value mixed
 * @param callback function
 */
$i.setPageUISetup = function(subspace,value,callbackF){
	if($i._get('webservice').authentication_required && $i.user._get().id){
		var page = $i.pages.find.visible();
		var pageHash = page.pageHash;
		var urlHash = page.urlHash;
		var setup = page.ui_setup;
		if(setup==null){
			setup = {};
		}
		setup[subspace] = {};
		setup[subspace] = value;
		
		var obj = {};
		obj.id = urlHash; //Identifier of namespace element.
		obj.namespace = 'pages'; //Primary Namespace to determine what we are saving.
		obj.subspace = subspace; //Secondary Namespace to determine what we are saving on namespace.
		obj.values = value; //Saving values.
		
		$i.promise._PUT('users/interface/'+$i._get('appName'),obj).done(function(){
			var ui = $i.user._get().ui_setup;
			//Update config on userInfo global ui_setup.
			if(ui==null){
				ui = {};
			}
			
			if(typeof(ui.pages)==='undefined'){
				ui.pages = {};
			}
			if(typeof(ui.pages[page.urlHash])==='undefined'){
				ui.pages[page.urlHash] = {};
			}
			ui.pages[page.urlHash][subspace] = {};
			ui.pages[page.urlHash][subspace] = value;
			$i.user._get().ui_setup = ui;
			//Update config on page.
			page.ui_setup = setup;
			if(typeof(callbackF)==='function'){
				callbackF();
			}
			//console.log("OK");
		});
	}
	return true;
}


/**
 * Get page UI Setup config for the user.
 */
$i.getPageUISetup = function(hash){
	var page = null;
	if($i.user._get().ui_setup!=null){
		$.each($i.user._get().ui_setup.pages,function(url,value){
			if(url==hash){
				page = value;
				return true;
			}
		});
	}
	return page;
};

