$.browser = {msie: (navigator.appName === "Microsoft Internet Explorer")};
/**
 * IDCP Class
 * Declaration of idcp = $i
 * 
 * This is the main application object and it has usefull methods to work with the app.
 * 
 * @author phidalgo
 * @param win
 * @return object
 */

var idcp = $i =  (function(win,idcp_config) {
	/**
	 * Some basic default configuration...
	 * READ ONLY.
	 */
	var idcp_configuration = {
		debug 				: false,
		api 				: null,
		langs 				: ["en"],
		titlePrefix 		: 'App - ',
		timeoutWarning 		: 30000, //Default 30 seconds
		pageCache 			: 50,
		version 			: '0.00',
		apiTimeout 			: 10000, //Default 10 seconds
		gritterTime 		: 5000,
		fileSize 			: 15728640, //Default 15 mb
		chunkSize 			: 10000000, // default 10 MB
		dateFormat 			: 'YYYY-MM-DD', // default 'YYYY-MM-DD'
		serverTimeFormat 	: null, //Server response Date time format.
		timePattern			: null,
		notificationType	: 'notification', //values : notification/gritter 
		checkAPIStatusEvery : 60000, //time in ms to check API status.
		defaultLang			: 'en', //Default language.
		appName				: 'IDCP-WEB'
	};
	
	for (var prop in idcp_config) {
		if(typeof(idcp_configuration[prop])==='undefined'){
			console.warn("Property '"+prop+"' is not defined on idcp_configuration. You can now get this value, but please add it on idcp_configuration object");
		}
		idcp_configuration[prop] = idcp_config[prop]; 
	}
	
	/**
	 * PROPERTIES DECLARATION.
	 * Can be read and written.
	 */
	var idcp_properties = {
		logged : false,
		webservice : null,
		configuration : null,
		time : {
			hq : null,
			local : null,
			server : null,
			os : null,
			localFormat : null
		}
	};
	
	var getjQueryObjectFromPage = function(selector,context){
		var sel = selector || null;
		if (sel==null){return;}
		var con = context || idcp.pages.find.me();

		sel = sel.replace(/\s{2,}/g, ' ').split(' ');
		
		var obj = {};
		if(sel[0].substr(0,1)=='#'){
			try{
				sel[0] = sel[0].replace('#','');
				obj = con.find('[data-id="'+sel[0]+'"]');
			}
			catch(e){
				console.log(con);
			}
		}
		else{
			obj = con.find(sel[0]);
		}
		
		if(obj.length==0){
			return null;
		}
		for(var i=1;i<sel.length;i++){
			obj = obj.find(sel[i]);
		}
		return  obj;
	}

	/**
	 * idcp base selector. Works like ($) jQuery selector but instead use id for # it uses # for get data-id.
	 * @param string selector
	 * @param jQuery object context
	 * @return jQuery object.
	 */
	function selector(selector,context){
		$obj = getjQueryObjectFromPage(selector, context);
		if($obj == null){
			/**
			 * Si no hem trobat l'element en la pàgina actual, pot ser degut per dues coses.
			 * - O bé l'element data-id no existeix en aquesta plana.
			 * - O bé hem canviat de plana abans que es necessités l'objecte jQuery i al estar en una altra plana el $i('#element') (que busca en el div pageHash xxx)
			 * No el troba en la plana actual però sí hauria d'estar en la plana anterior.
			 */
			var allPages = $i.pages._getAll();
			for(i=0,j=allPages.length;i<j;i++){
				page = allPages[i];
				$obj = getjQueryObjectFromPage(selector,$('div[data-pagehash="'+page.pageHash+'"'));
				if($obj!=null && $obj.length>0){
					return $obj;
				}
			}
			return $i.pages.find.me().find(selector);
		}
		return $obj;
	};
	
	/**
	 * $i jQuery integration selector.
	 * Customized jQuery selector to work with data-id parameters on a visible page or 
	 * also you can add a second argument to selector to specify the context.
	 */
	var idcp = selector;
	

	/**
	 * METHODS DECLARATION.
	 * Each method should have their class file on app/core
	 */

	idcp.cookies = null; //Static class app/core/class.cookies.js
	idcp.times = null; //Class app/core/class.dateTime.js
	idcp.history = null; //Class app/core/class.history.js
	idcp.intervals = null; //Class app/core/class.intervals.js
	idcp.notification = null; //Class app/core/class.notifications.js
	idcp.routing = null; //Class app/core/class.maprouting.js
	idcp.pages = null; //Class app/core/class.page.js
	idcp.promise = null; //Class app/core/class.promise.js
	idcp.user = null; //Class app/core/class.user.js
	idcp.menu = null; //Class app/core/class.menu.js
	idcp.hash = null; //Class app/core/class.hashManagement.js
	idcp.security = null;
	
	/**
	 * Breadcrumb management. (alias -> Fil d'ariadna). https://ca.wikipedia.org/wiki/Fil_d%27Ariadna
	 */
	idcp.breadcrumb = new (function(){
		/**
		 * Append a new breadcrumb. Can pass an url to make the breadcrumb as anchor.
		 * Before add an anchor, check if the route exist on $i.routing.
		 */
		this._add = function(string,url){
			var url = typeof(url)==='undefined'? window.location.hash:url;
			var hasLink = $i.routing.exists(url);
			if(!hasLink){
				$i.UI.breadcrumb.append('<li>'+string+'</li>');
			}
			else{
				$i.UI.breadcrumb.append('<li><a href="'+url+'">' + string + '</a></li>');
			}
		}
		
		/**
		 * Update breadcrumb and appends this element.
		 */
		this._set = function(string){
			this.update(); //Set the aria bar until last MENU element.
			this._add(string); //Add a new aria element after last aria element.
		};
		
		/**
		 * Clear breadcrumb and add to it all the menu points.
		 * Also updates the document title.
		 */
		this.update = function(){
			var fullUrl = window.location.hash.replace('#','');
			$i.UI.breadcrumb.html("");
			if(fullUrl==""){
				idcp.documentTitle(idcp.i18n("menu:home")); // Sets document title
			}
			else{
				var url = '';
				var stages = fullUrl.split('/');
				for(var i=0;i<stages.length;i++){
					url += stages[i]+'/';
					var rU = 'menu:'+url.slice(0,-1);
					if(idcp.i18n.exists(rU)){
						this._add(idcp.i18n(rU),'#'+url.slice(0,-1));
					}
				}
				if(idcp.i18n.exists("menu:"+fullUrl)){
					idcp.documentTitle($i.i18n("menu:"+fullUrl)); // Sets document title
				}
			}
		};
		
		
	});
	
	/**
	 * Load sequentially the files specified.
	 * 
	 * @author phidalgo
	 * @param files Files that you want to load sequencially
	 * @param layout Target of the loaded files if there are html 
	 * @param vars Variables sensed to all specified files if there are js 
	 * @throws Exception if problems with layout.
	 * @returns {jQuery.loadFiles}
	 * 
	 * 
	 * This method changes their behaviour regarding the file type you want to load.
	 * 
	 * You can call this method if you want to load a list of js files sequentially by:
	 * 
	 * 		$i.require(["file1.js","file2.js","file3.js",...]);
	 * 
	 * 		The first file of array will be load first, the second one, the second... and the same until last.
	 * 
	 * You can call this method if you want to load an html file into the specified div like:
	 * 		$i.require(["file1.html","file1.js"],$('#wrapper'));
	 * 
	 * 		You have to put the html files on the first position because, when the html file is downloaded, this method will print it on the specified layout. 
	 * 		THE METHOD WILL NOT WAIT TO DOWNLOAD ALL FILES, WHEN THE HTML FILE IS DOWNLOADED, THE METHOD WILL PRINT IT!
	 * 		
	 * 		This is like this due to avoid to print it after the js file is loaded... :)
	 * 
	 * 		IMPORTANT: If the layout is the layout_content ($i.UI.layout_content) jqueryt object, it will do additional features:
	 * 
	 * 			add the downloaded html to the page caching system. (the $i.pages methods & objects).
	 * 
	 * 			If this method detects that the html file is on the page system, it will STOP to download any file (ANY FILE), and will call the show method to show the content.
	 * 
	 * 			pages will call to the callback function and execute its code.
	 * 
	 * 		You, as a programmer could interest you that some page does not add to this page caching system, it easy to do this. 
	 * 
	 * 		On the page main div, you write:
	 * 			<div data-nocache></div> OR <div data-nocache="true"></div>
	 * 		If you want to work normally ...
	 * 				<div></div> OR <div data-nocache="false"></div>
	 * 
	 *   	If you work with data-nocache, every time the user goes to this page, the system will download, again, the html files, the js files and reload it.
	 *   
	 *   
	 *  If you call this method, with html file and also add a vars, this vars will be added to window["PAGE_HASH"], to recover it later, maybe in your page js file? :), later to do your
	 *  purposes. You can get the vars by call $._GET(); function (like PHP).
	 *  
	 *  This class has also another methods... like ".done()", ".fail()" or ".html()";
	 *  
	 *  .done() is like $jquery .done() for ajax method.
	 *  
	 *  .fail() or .html() DISABLES THE REGULAR behaviour of this method. 
	 *  		If you call .html() this method will delegate you the work to apply on a specific layout and add to the current page system, for example
	 *  		If you call .fail() the same as .html(), you have to call throw if you need... or do what you want.
	 * 		
	 * 		
	 */
	var idcp_require = function(files,layout,vars,sequential){
		this.load = load;
		this.done = done;
		this.html = html;
		this.fail = fail;
		

		this.files = files;
		this.layout = layout || null;
		this.vars = vars || null;
		/**
		 * Determina si les peticions seràn seqüencials o no.
		 * Per defecte, true.
		 */
		this.sequential = (typeof(sequential)==='boolean'? sequential:true);
		this.loadedFiles = 0; //Si la petició no és seqüencial, això desa quants fitxers ja hem obtingut de la petició.
		this.doneF;
		this.htmlF;
		this.failF;
		
		this.doneF = function(){
			//console.log("No function attached to done method on require.js");
		}
		
		function getDataType(ext){
			switch(ext){
				case "js":
					return "script";
				case "html":
				case "htm":
				case "php":
					return "html";
				case "json":
					return "json";
				case "xml":
					return "xml";
				case "txt":
					return "text";
				case "css":
					return "css";
				default:
					return "Intelligent Guess";
			}
		}

		function done(f){
			this.doneF = f;
		};
		
		function html(f){
			this.htmlF = f;
		};
		
		function fail(f){
			this.failF = f;
		}
		
		if(!this.sequential){
			//console.log("Carrega NO Seqüencial!")
		}
		
		this.load(this.files,0);
		
		function load(files, index){
			var _this = this;
			if (files.length!=0 && index < files.length && files[index]) {
				var ext = files[index].split('.').pop();
				var dt = getDataType(ext);
				if(!this.sequential){
					var options = {
						    dataType: dt,
						    cache: !idcp._get('debug'),
						    url: files[index]
						};
					$.ajax(options)
					.always(function(){
						//console.log("Fitxer : "+files[_this.loadedFiles]+" carregat NO seqüencialment (current: "+_this.loadedFiles+", total: "+(files.length-1));
						var sext = getDataType(files[_this.loadedFiles].split('.').pop());
						if(sext=='css'){
							console.lo
							if (!$("link[href='"+files[_this.loadedFiles]+"]").length){
								$('<link href="'+files[_this.loadedFiles]+'" rel="stylesheet">').appendTo("head");
							}
						}
						
						_this.loadedFiles++;
						if(_this.loadedFiles==files.length){
							if(typeof(_this.doneF)==='function'){
								//console.log("Tots els fitxers carregats correctament!");
					    		_this.doneF();
					    	}
						}
					})
					.done(function(){
						
					})
					.fail(function(jqXHR, textStatus, errorThrown){
						
					});
					//Download the next file
					//console.log("Petició ajax per al fitxer: "+files[index]+", current: "+index);
					if (files.length!=0 && index < files.length && files[index]) {
						index++;
					    _this.load(files,index);
					}
	  			}
				else{
					if(dt=='css'){
						if (!$("link[href='"+files[index]+"]").length){
							$('<link href="'+files[index]+'" rel="stylesheet">').appendTo("head");
						}
						index++;
					    this.load(files,index);
					}
					else{
						var hasDiv = false;
						if((dt=="html" || dt=='text') && this.layout==idcp.UI.layout_content){
							hasDiv = idcp.pages.inCache();
						}
						if(hasDiv){
							idcp.pages.hide();
							idcp.pages.show();
							this.doneF();
						}
						else{
							var options = {
							    dataType: dt,
							    cache: !idcp._get('debug'),
							    url: files[index]
							};
							if((index-1)>=0 && files[index-1].split('.').pop()=="html" && this.layout==idcp.UI.layout_content){
								options.dataType = "text";
							}
							
							$.ajax(options)
							.done(function (data,textstatus,jqxhr){
								if((index-1)>=0 && files[index-1].split('.').pop()=="html" && files[index].split('.').pop()=="js" && _this.layout==idcp.UI.layout_content){
									var page = idcp.pages.find.visible();
									if(page!==false){
										page.ecmafile = files[index];
										var $ecma_script = $('<script type="text/javascript">idcp.pages.find.visible().script ='+data+'</script>');
										page.$.append($ecma_script);
										$ecma_script.remove();
										/*page.script =  eval(data);*/
										page.object = new page.script(page);
										if(page.autoshow){
											page.$.fadeTo('fast',1);
										}
										
									}
								}
								if(dt=="html" || dt=='text'){
									if(typeof(_this.htmlF)=='function'){
										_this.htmlF(data);
										//Download the next file
										index++;
									    _this.load(files,index);
									}
									else{
										if(_this.layout!=idcp.UI.layout_content){ //Si és root
											var $r = $(data);
											$r.i18n();
											_this.layout.html($r);
											//Download the next file
											index++;
										    _this.load(files,index);
										}
										else if(_this.layout==idcp.UI.layout_content){
											if(dt=='text'){
												data = "<div data-nocache>"+data.replace(/\r/g,"<br />")+"</div>";
												var dates = data.match(/\d\d\/\d\d\/\d\d\d\d/g);
												if(typeof(dates)=="object"){
													for(var i=0;i<dates.length;i++){
														data = data.replace(dates[i],"<strong>"+dates[i]+"</strong>");
													}
												}
											}
											/**
											 * If not contain data-nocache or its set to true, do not cache it.
											 * If contain data-nocache or contains data-nocache and its set to false, do cache
											 * Example nocache page:
											 * 
											 * <div data-nocache>
											 * OR
											 * <div data-nocache="true">
											 * 
											 * Example cache page:
											 * 
											 * <div>
											 * 
											 * OR
											 * 
											 * <div data-nocache="false">
											 */
											var html = $(data);
											var cache = true;
											if(typeof(html.data('nocache'))!=='undefined'){
												if(html.data('nocache')==''){
													cache = false;
												}
												else{
													cache = !html.data('nocache');
												}
											}
											pageHash = idcp.generator();
											
											html.attr('style',"visible");
											html.attr('data-pagehash',pageHash);
											//idcp.loading.add(); //TODO ???? Cal????
											
											var cbf = null; //This code (try catch) is to get name of the file to use it as callback function.
											try{
												cbf = files[index].split('/');
												//console.log(cbf);
												var langNamespace = cbf[1]; //La ruta ha de ser content/MODUL/... el namespace a carregar és el nom del MODUL (array[1]):
												cbf = cbf[cbf.length-1].split('.')[0];
											}
											catch(err){
												cbf = null;
											}
											
											function loadNext(){
												idcp.pages.hide();
												html.i18n(); //Traduim l'HTML.
												html.css('opacity',0); //Set opacity to 0
												_this.layout.append(html);
												
												idcp.pages._add({
													pageHash: pageHash, //Unique session page identifier.
													urlHash: window.location.hash, //URL has
													html: files[index], //HTML (template) file loaded.
													intervals: [], //Page intervals array
													cache: cache, //If this page can be cached.
													ui_setup : idcp.getPageUISetup(window.location.hash), //UI setup object.
													ecmafile : null, //Javascript file of this page (template).
													script : null, //Javascript Code
													object : null, //Javascript object
													//buttons : $i.setPageButtons(pageHash), //Page buttons
													callback: ((cbf!=null)? 'callback_'+cbf:null), //Callback function (TODO:: Deprecated. Now javascript object has a method called callback).
													visits : 1, //Number of visits of this page (only works if the page is in caching system.
													visible: true, //If the page is now visible.
													autoshow : true, //If the page shows inmediatelly after get the html or wait until page.loaded is called.
													$ : html
												});

												if(_this.vars!=null && dt=='html'){
													window[pageHash] = _this.vars;
												}
												$i.pages._update();
												
												//Download the next file
												index++;
											    _this.load(files,index);
											}
											i18n.loadNamespace(langNamespace,loadNext); //Async load of text translations. The name of the localization file has to be the same name as .js script.
											
										}
										else{ //Si no sé q és, error.
											throw new Exception(4);
										}
									}
								}
								else{
									//Download the next file
									index++;
								    _this.load(files,index);
								}
							    
							})
							.fail(function(jqXHR, textStatus, errorThrown){
								if(typeof(_this.failF)=='function'){
									_this.failF(files[index],jqXHR, textStatus, errorThrown);
								}
								else{
									console.warn(files[index]+": "+errorThrown);
									if(jqXHR.status==200 && textStatus=='parsererror'){
										$("head").append($('<script />',{html: jqXHR.responseText}));
									}
									else{
										throw new AppException(jqXHR.status,files[index]);
									}
								}
							});
						}
					}
				}
			}
			else{
		    	if(files.length==0){
		    		setTimeout(function(){
						if(typeof(_this.doneF)==='function'){
							_this.doneF();
				    	}
					},50);
		    	}
		    	else{
		    		/**
		    		 * Per a pàgines html sense js, posem l'opacity a 1 sense esperar a res més.
		    		 */
		    		if(files.length==1 && (files[0].endsWith('.html') || files[0].endsWith('.txt'))){
		    			var page = idcp.pages.find.visible();
		    			page.$.fadeTo('fast',1);
		    		}
		    		
		    	  	if(typeof(_this.doneF)==='function'){
			    		this.doneF();
			    	}
		    	}
		    }
		}
	};
	
	
	idcp.require = function(files,layout,vars,sequential){
		return new idcp_require(files,layout,vars,sequential);
	};
	
	

	/**
	 * Set an IDCP property (idcp_properties object).
	 * @param prop string property
	 * @param value string value
	 * @return boolean (always true).
	 */
	idcp._set = function(prop,value){
		if(typeof(idcp_properties[prop])==='undefined'){
			console.warn("You can set/get "+prop+" property now but does not exist on idcp_properties. Please add it.");
		}
		idcp_properties[prop] = value;
		return true;
	}
	
	/**
	 * Get IDCP property
	 * @param string property
	 * @return string.
	 */
	idcp._get = function(prop){
		if(typeof(idcp_configuration[prop])==='undefined'){
			if(typeof(idcp_properties[prop])==='undefined'){
				return null;
			}
			else{
				return idcp_properties[prop];
			}
		}
		else{
			return idcp_configuration[prop];
		}
	}
	
	/**
	 * Check if the client session_token is a valid session_token.
	 * TODO -> això ha d'anar o bé fora d'aqui o bé instanciar-ho de forma diferent. No m'agrada.
	 *  @returns jQuery Promise
	 */
	idcp.checkToken = function() {
		var promise =  $i.promise._POST("users/me/isLogged")
		.fail(function ( jqXHR, textStatus ) {
			if ($i._get('debug') && jqXHR.responseJSON.message){
				console.log(jqXHR.responseJSON.message + ":" + $i.cookies._get("session_token"));
			}
		});
		return promise;
	};

	
	idcp.objects = new (function(){
		/**
		 * Get a object by id or name
		 * @return object || false.
		 */
		this._get = function(value){
			var objects = idcp_properties.configuration.tables;
			for(var i=0,j=objects.length;i<j;i++){
				if(objects[i].id == value || objects[i].name == value){
					return objects[i];
				}
			}
			return false;
		}
	});
	
	
	/**
	 * App localization.
	 * This class autoload the needed library.
	 */
	idcp.i18n = new (function(){
		var i18n_lib_path = 'lib/external-lib/i18next-1.10.3.min.js';
		var initializated = false;
		var lang = "";
		var iid = null;
		
		/**
		 * Async Return i18next localization options
		 * 
		 * @returns JSON {...}
		 */
		var i18nsetup = { 
			resGetPath					: 'locales/__lng__/__ns__.json',
			detectLngQS					: 'lang',
			cookieName					: 'lang',
			fallbackLng					: idcp._get('defaultLang'), //	language to lookup key if not found on set language
			load						: 'current',
			lng 						: 'ca', //	language to set (disables user language detection)
			lowerCaseLng				: true, // to lowercase countryCode in requests, eg. to 'en-us' 
			useLocalStorage				: !idcp._get('debug'), // false by default, turn true for production enviroment
			useDataAttrOptions			: true,
			interpolationPrefix 		: '__', //http://i18next.com/pages/doc_features.html | You can find this properties to change prefix and suffix strings to vars, but it does 
			interpolationSuffix 		: '__', // not work. If you change it, it crashes (20151111)
			localStorageExpirationTime	: (idcp._get('debug')? 0:86400000), // in ms, default 1 week
			debug						: false, // If something went wrong you might find some helpful information on console log
			ns							: 'default', // Default namespace
			fallbackToDefaultNS			: true,
			getAsync					: true // The init function will now be blocking until all resources are loaded!!!
		};

		/**
		 * Returns a localized string if exists. Or the original string in []
		 */
		var i18nO = function(toLocalize,context){
			var localizedString = $.t(toLocalize,context);
			if(localizedString == toLocalize){
				var a = toLocalize.split(':');
				if(a.length==2){
					localizedString = $.t(a[1],context);
				}
				if(localizedString == a[1]){
					return '['+localizedString+']';
				}
				else{
					return localizedString;
				}
			}
			return localizedString;
		};
		
		/**
		 * Check if exists a translation key for the string
		 * @param string string.
		 * @return boolean.
		 */
		i18nO.exists = function(string){
			return i18n.exists(string);
		}
		
		
		
		/**
		 * Load multiple namespaces sequentially.
		 * 
		 * This function adds a functionallity to i18next to load multiple namespaces in one function using one callback.
		 */
		i18nO.loadNamespaces = function(array,cbk){
			var i = 0;
			loadNS(array,i,nsloaded);
			
			function nsloaded(){
				//console.log('i18n NameSpace loaded: ' + array[i]);
				if(i<array.length-1){
					i++;
					loadNS(array,i,nsloaded);
				}
				else{
					cbk();
				}
			}

			function loadNS(array,position,cbk){
				i18n.loadNamespace(array[position],cbk);
			}
		}
		
		/**
		 * If called before the i18n-next library is initialized, waits until is initialized.
		 * If called after initialization, directly callback the function.
		 */
		i18nO.init = function(callback){
			if(initializated){
				callback();
			}
			else{
				iid = setInterval(function(){
					if(initializated){
						clearInterval(iid);
						callback();
					}
				},50);
			}
		}
		
		//Load required Lib and initialize i18n-next class.
		idcp.require([i18n_lib_path]).done(function(){
			i18n.init(i18nsetup,function(){
				i18nO.langs = i18n.languages; //Available Front End languages list.
				i18nO.currentLang = i18n.lng();
				initializated = true;
			});
		 });
		
		i18nO.currentLang = null; //Current language.
		i18nO.langs = null; //Available Languages.
		
		return i18nO;
	});
	
	
	/**
	 * idcp User Interface class.
	 * Contains properties and methods.
	 */
	idcp.UI = new (function(data){
		var UILoaded = false;
		this.layout = null;
		this.layout_root = null;
		this.rootObject = null;
		this.usersPhoto = null;
		this.usersName = null;
		this.mainMenu = null;
		this.footer = null;
		this.layout_modal = null; // Modal layout
		this.layout_content = null;
		this.breadcrumb = null;
		var userInterface = this;
		
		/**
		 * Here starts everything...
		 */
		this.autoLoad = function(){
			idcp.require([ //First: Load all needed libraries needed to show LOGIN modal.
			            "lib/external-lib/moment.js",
			            "lib/external-lib/moment-timezone.js",
			            "lib/external-lib/jquery.md5.js",
			            "app/core/misc.js",
			            "app/core/throw.js",
			            "app/core/class.intervals.js",
			            "app/core/class.cookies.js",
			            "app/core/class.promise.js"
			            ],null,null,false)
			.done(function(){
				//Set al layouts.
				userInterface.layout = $('body');
				userInterface.layout_root = $("#wrapper"); //Main layout;
				userInterface.layout_modal = $("#loadmodal"); // Modal layout
				idcp.i18n.init(function(){ //Initialize translation engine.
					/*$i.intervals.app._add('ping',function(){
						idcp.promise._GET('ping');
					},10000);*/
					
					
					idcp.promise._GET("status").done(function(rsp){ //Check the connection to WebService 
						idcp._set('webservice',rsp.data); //Add connection status to a idcp property
						/**
						 * Start checking authentication method.
						 */
						if(!rsp.data.authentication_required){ //If webservice is configured to no authentication needed to login
							console.warn("Authentication token is not needed.");
							idcp.cookies._delete("session_token"); //Delete previous saved token.
							userInterface.application.load(); //Load Application.
						}
						else{ //If webservice needs a token...
							if(idcp.cookies._get("session_token")){ //Check if client has a cookie with the auth token.
								idcp.checkToken() //Check if the stored token is valid...
								.done(function(data){
									idcp._set('logged',true); //Set idcp property logged to true.
									userInterface.application.load(data,false); //If is valid, load application.
								})
								.fail(function(jqXHR, textStatus){ //If the stored token is not valid...
									idcp._set('logged',false);
									idcp.cookies._delete("session_token"); //Delete previous saved token.
									userInterface.login.load(); //Load login screen.
								});	
							}
							else{ //If client does not have a stored token...
								idcp._set('logged',false);
								userInterface.login.load(); //Load login screen.
							}
						}
					}).fail(function(a){ //If the web service is not responding after timeout setting or if there is any error with it...
						idcp.cookies._delete("session_token"); //Delete previous saved token.
						userInterface.login.load(null,function(){ //Load login screen and show the error.
							$("#login-response-msg").removeClass("hidden alert-info")
							.addClass("alert-danger").html("<i class='fa fa-exclamation-triangle bigger-125'></i>" + $i.i18n("idcp:error_" + a.status));
						});
					});
				});
			});
		}
		
		/**
		 * Class which contains application general methods and properties.
		 */
		this.application = new (function(){
			/**
			 * Loads App UI.
			 * @param data -> user's token validity, expiring time...etc.
			 * @param fromLogin -> if the load petition is from login screen or not.
			 */
			this.load = function(data,fromLogin){
				if(!fromLogin){
					var iLoad = '<div class="initial_load">'+
					'<div style="position:absolute;left:50%;top:50%;margin-top:-50px;margin-left:-50px;width:100px;height:100px;">'+
						'<i class="fa fa-circle-o-notch fa-spin red bigger-300"></i>'+
					'</div>'+
				'</div>';
					userInterface.layout.append(iLoad);
					$('.initial_load').css("visibility","visible");
				}
				idcp._set('logged',true); //Set idcp property logged to true.
				idcp.promise._GET('configuration') //Get webservice config.
				   .done(function(configuration){
					   idcp._set('configuration',configuration.data);
					   idcp.require(["app/core/class.hashManagement.js",
								        "app/core/class.user.js",
								        "app/core/class.sessionManagement.js",
					    	            "app/base/layout.html",
					    	            "app/base/js/layout.js"
					    	            ],userInterface.layout_root)
						.html(function(html){
							if(idcp._get('webservice').authentication_required){
								idcp.sessionManagement(data);
							}
							userInterface.rootObject = $(html);
							userInterface.usersPhoto = userInterface.rootObject.find('#menu-userPhoto');
							userInterface.mainMenu = userInterface.rootObject.find('#nav-list');
							userInterface.footer = userInterface.rootObject.find('#application-services-info');
							userInterface.usersName = userInterface.rootObject.find('#menu-username');
							userInterface.breadcrumb = userInterface.rootObject.find('#breadcrumb')
							UILoaded = true;
						});
					})
					.fail(function(){
						console.log("FAIL?");
						/*idcp.cookies._delete("session_token"); //Delete previous saved token.
						userInterface.login.load(null,function(){ //Load login screen and show the error.
							$("#login-response-msg").removeClass("hidden alert-info")
							.addClass("alert-danger").html("<i class='fa fa-exclamation-triangle bigger-125'></i>" + $i.i18n("idcp:error_getting_ws_configuration"));
						});*/
					});
				
			}
		})();
		
		this.login = new (function(){
			/**
			 * Loads Login. 
			 * @param message -> notice/warning/error message to the user.
			 * @param callback -> function to process when login is done.
			 */
			this.load = function(message,cbk) {
				idcp.i18n.loadNamespaces(['login'],function(){
					message =  (typeof message === "undefined" || message==null) ? '' : message;
					$("body").attr("class","login-layout"); //clean class attr and add login-layout class
					idcp.require(["app/base/login.html","app/base/js/login.js"],userInterface.layout_root)
					.done(function(){
						if (message!='') {
							$("#login-response-msg").removeClass("hidden alert-info").addClass("alert-danger").html("<i class='fa fa-exclamation-triangle bigger-125'></i>" + message);
						}
						if(typeof(cbk)==='function'){
							cbk();
						}
					});
				});
			};
		})();
	})();
	
	return idcp;
})(window,idcp_config);