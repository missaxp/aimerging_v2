/**
 * Class cookies.
 * 
 * Cookie management.
 */

(function(){
	function idcp_cookies_management(){
		/**
		 * Set cookie value
		 * 
		 * @param name
		 * @param value
		 * @param exdate
		 */
		this._set = function(name,value,exdate){
			//If exdate exists then pass it as a new Date and convert to UTC format
			expireDate = moment(exdate,$i._get('serverTimeFormat')).utc().format("ddd, DD MMM YYYY HH:mm:ss UTC");
		    var c_value = escape(value) + ((exdate === null || exdate === undefined) ? "" : "; expires=" + expireDate +"; path=/");
		    console.log(name+"="+c_value);
		    document.cookie = name + "=" + c_value;
		};
		
		/**
		 * Get cookie value
		 * 
		 * @param name
		 * @return Cookie value or null if not found
		 */
		this._get = function(name){
			var cookie = document.cookie, e, p = name + "=", b;
			if ( !cookie )
				return null;
			b = cookie.indexOf("; " + p);
			if ( b == -1 ) {
				b = cookie.indexOf(p);
				if ( b != 0 )
					return null;
			} else {
				b += 2;
			}
			e = cookie.indexOf(";", b);
			if ( e == -1 )
				e = cookie.length;

			return decodeURIComponent( cookie.substring(b + p.length, e) );
		}
		
		/**
		 * Delete cookie
		 * 
		 * @param name
		 */
		this._delete = function(name){
			document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
			return true;
		};
	}
	
	idcp.cookies = new idcp_cookies_management();
	//console.log("class idcp.cookies ready to be used.");
})();