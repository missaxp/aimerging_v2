/**
 * @author phidalgo
 * @created 20160125
 * @modified 20160125
 * 
 * notification is an object with this properties and methods:
 * 
 * notification {
 * 	id : random unique string
 * 	date : sysdate
 * 	flag : Notification to view o already view
 * 	render : Notification is rendered on the UI or not.
 * 	badge : number of badges to mark on this notification
 * 	link : notification anchor link to go when click.
 * 	icon : notification icon
 * 	title : notification title
 * }
 * 
 * Notifications can be seen on the top right UI (bell icon) or in the "See all notifications" window.
 * When user click on the bell icon, it appears a drop down list with the last [show_last_notification_number] notifications.
 * On "See all notifications" will appear all the notifications in this session. 
 */

(function(){
	function idcp_notification_system(){
		var $list = $('#notifications-list',$i.UI.rootObject);
		var $title = $('#notifications-title',$i.UI.rootObject);
		var $badge = $('#notifications-badge',$i.UI.rootObject);
		var $badge_number = $('#notifications-badge > span',$i.UI.rootObject);
		
		var $animation = $('<i class="ace-icon fa fa-bell icon-animated-bell"></i>');
		var notifications = [];
		
		var badge_count = 0;
		
		$badge.prepend($animation);
		
		$badge.on('click',function(){
			$badge_number.html('');
			badge_count = 0;
			$.each(notifications,function(){
				this.flag = false;
			});
		});
		
		/**
		 * This function calculates how many notifications are new.
		 */
		var calculate_badge =  function(){
			badge_count = 0;
			$.each(notifications,function(){
				if(this.flag){
					badge_count++;
				}
			});
			return badge_count;
		}
		
		var updateGlobalBadge = function(){
			$badge_number.html(calculate_badge());
			$badge.find('i').remove();
			$badge.prepend($animation);
		}
		
		this.flashBell = function(color,duration){
			$badge.find('i').css('color',color+' !important');
		}
		
		/**
		 * adds a notification.
		 */
		this.add = function(notification){
			notification.date = new Date();
			notification.flag = true;
			notification.id = $i.generator();
			notification.render = true;
			
			var bkg_color = 'btn-success';
			if(notification.type && notification.type!=""){
				switch(notification.type){
					case 'success':
						break;
					case 'warning':
						bkg_color = 'btn-warning';
						break;
					case 'error':
						bkg_color = 'btn-danger';
						break;
				}
			}
			
			var $html = $('<li data-id="not-id-'+notification.id+'">'+
			'<a href="'+notification.link+'">'+
				'<div class="clearfix">'+
					'<span class="pull-left" data-id="not-data">'+
						'<i class="btn btn-xs no-hover '+bkg_color+' fa '+notification.icon+'"></i>&nbsp;&nbsp;'+
						notification.title+
					'</span>'+
					'<span class="pull-right badge badge-info" data-id="not-badge">'+((notification.badge && notification.badge!='') ? '+':'')+notification.badge+'</span>'+
				'</div>'+
			'</a>'+
			'<div data-id="notification-message" class="notification-message">'+notification.message+'</div>'+
			'</li>');
			$html.prependTo($list);
			
			if(!notification.link || notification.link==null){
				$html.on('click',function(){
					return false;
				});
			}
			
			
			notifications.push(notification);
			updateGlobalBadge();
			
			if(notification.flash && notification.flash!=""){
				//this.flashBell(notification.flash);
			}
			return notification.id;
		};
		
		/**
		 * Updates a notification.
		 * @param nid notification id 
		 */
		this.update = function(nid,notification){
			$.each(notifications,function(){
				if(this.id == nid){
					not = $list.find('li[data-id="not-id-'+nid+'"]');
					if(notification.link){
						not.find('a').attr('href',notification.link);
					}
					data = '<i class="btn btn-xs no-hover btn-pink fa '+(notification.icon? notification.icon:this.icon)+'"></i>  '+(notification.title? notification.title:this.title);
					not.find('span[data-id="not-data"]').html(data);
					bn = (notification.badge? notification.badge:this.badge) 
					not.find('span[data-id="not-badge"]').html((bn!=''? '+':'')+bn);
					if(notification.flag){
						this.flag = true;
						updateGlobalBadge();
					}
				}
			});
		};
	}
	
	idcp.notification = new idcp_notification_system();
	//console.log("class idcp.notification ready to be used.");
})();