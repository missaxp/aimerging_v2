/**
 * Sets the app intervals functions that will manage the user's session.
 * 
 *   When you call this function and pass as a arg the right variables ( a object with server session information), this function will restore the
 *   right interval, given a expire date and a session timeout. 
 * 
 * @param object data
 * @author phidalgo
 */
(function(){
	function idcp_session_management(data){
		if(!$i._get('webservice').authentication_required){
			return true;
		}
		
		$i.serverTimeNow = data.data.svr;
		
		var expiresTime = moment.tz(data.data.expires,$i.serverTimeFormat,$i._get('webservice').timezone);
		var serverTime = moment.tz(data.data.svr,$i.serverTimeFormat,$i._get('webservice').timezone);
		
		var sessionDuration = expiresTime.diff(serverTime); //Get the difference between server time and expires Time in ms.
		var timeoutMilliSeconds = (data.data.timeout*1000) - $i._get('timeoutWarning'); //Get the default timeout session and subtract the timeout warning to advice the user.
		
		if($i._get('debug')){
			console.log('TOKEN INFO Expire ms: ' + sessionDuration + ', timeout ms: ' + timeoutMilliSeconds);
		}
	 
		//Clear all app intervals
		$i.intervals.app.clear();
		
		$i.intervals.app._add('session_timeout',
			function(){
				console.log("session_timeout interval CALLED");
				// Hide modal-login if exists
				$('#modal-login').modal('hide');
				$i.require(["app/base/stimeout_modal.html","app/base/js/stimeout_modal.js"],$i.UI.layout_modal)
				.done(function(){ // Shows login modal
					$('#modal-login').modal();
				});
				$i.intervals.app.clear('session_timeout');
				$i.intervals.app.clear('token_expires');
				$i.intervals.app.clear('checkServerStatus');
			},
			timeoutMilliSeconds
		);
		
		$i.intervals.app._add('token_expires',
			function(){
				console.log("token_expires interval CALLED");
				// Hide modal-login if exists
				$('#modal-login').modal('hide');
				$i.require(["app/base/loginmodal.html","app/base/js/loginmodal.js"],$i.UI.layout_modal)
				.done(function(){ // Shows login modal
					$('#modal-login').modal();
				});
				$i.intervals.app.clear('token_expires');
				$i.intervals.app.clear('session_timeout');
				$i.intervals.app.clear('checkServerStatus');
			},
			sessionDuration
		);
		
		var times = $i._get('time');
		$i.intervals.app._add('checkServerStatus',function(){
		   console.log("checkServerStatus!!");
		   $i.promise._GET({restURL: "status",clearTimeout : false}).done(function(rsp){
			   $i._set('webservice',rsp.data); 
			   times.hq = $i.times(rsp.data.time).toTZ(rsp.data.hqtz).toLocalFormat();
			   times.local = $i.times(rsp.data.time).toUserTZ().toLocalFormat();
			   times.server = $i.times($i._get('webservice').time).toServerTZ().toLocalFormat();
			   times.os = moment();
		   });
		   
	   },$i._get('checkAPIStatusEvery'));
		   
	   $i.intervals.app._add('hq_clock',function(){
		   if(times.hq!=null){
			   //Update time
			   times.hq.getMoment().add(1,'second');
			   times.local.getMoment().add(1,'second');
			   times.server.getMoment().add(1,'second');
			   times.os.add(1,'second');
			   //Set time.
			   $('#local_time').html(times.local.render());
			   $('#hq_time').html(times.hq.render());
			   if($i._get('debug')){
				   $('#server_time').html(times.server.render());
				   $('#local_time_os').html(times.os.format(times.localFormat));
			   }
		   }
	   },1000);
	   
	 //Apply
	   $i.intervals.app.apply();
	}
	
	idcp.sessionManagement = idcp_session_management;
	//console.log("function idcp.sessionManagement ready to be called.");
})();