/**
 * App intervals management
 * 
 * @author phidalgo
 * 
 * This object has to be used to control ALL functions and methods that it has to be asyncrounously executed.
 * 
 * This object has two main uses:
 * 
 * $i.intervals.pages -> all methods related to async functions on PAGES.
 * $i.intervals.app -> all methods related to async functions on the whole APP.
 * 
 * $i.intervals.pages:
 * 		Used when you want to add an async function to be executed on the loaded page. 
 * 		i.e. : You want to check, every 2 seconds if we have new mail on server, on the app mail page.
 * 			In your page script, you have to call to: $i.intervals.pages.add(), and specify the next:
 * 			First argument has to be a free name to identify the async function.
 * 			Second argument has to be the function.
 * 			Third argument has to be the period (in milliseconds) to execute the function.
 * 
 * 			$i.intervals.pages.add(
 * 				'TEST_ASYNC_HELLO',
 * 				function(){console.log("HELLOOO :)");},
 *				2000	
 * 			); 
 * 
 * 		And that's it!. The page object management will do the rest for you.
 * 		When the page changes, the system will stop the async function and when you come back to this page, the system will be start it again :)
 * 
 * 		Of course you can ALWAYS start or stop or clear your function by using the methods:
 * 			$i.intervals.pages.clear:
 * 				For the current page, stops the async functions.
 * 
 * $i.intervals.app:
 * 		Used wen you want to add an async function to be executed on the whole app. It will be always executing until you call to
 * 			$i.intervals.app.clear(_IntervalName_);
 * 		You might also do, whenever you want, apply again the interval, as your needs become, by using:
 * 			$i.intervals.app.apply(_IntervalName_);
 * 
 * 
 * 
 * The use of both objects are mostly the same, but on the app object, you must know the interval name to clear. If you call a method, for example, $i.intervals.app.clear(); and
 * you do not pass any arg, the method will clear all intervals on app.
 * 
 */
(function(){
	function idcp_intervals_management(){
		var pages = function(){
			this._add = function (identifier,f,period){
				var page = $i.pages.find.visible();
				var val = setInterval(f,period);
				
				
				var interval = this._get(identifier);
				if(interval === false){
					page.intervals.push({name: identifier, intervalId: val,func: f,p: period});
					if($i._get('debug')){
						//console.log("Interval: " + identifier + " added on page: " + page.pageHash+', period: '+period);
					}
				}
				else{
					page.intervals[interval] = {name: page.intervals[interval].name, intervalId: val, func: page.intervals[interval].func, p: page.intervals[interval].p};
					if($i._get('debug')){
						//console.log("Interval: " + identifier + " updated on page: " + page.pageHash+', period: '+period);
					}
				}
			};
			
			this.clear = function (intervalName){
				var interval = intervalName || null;
				
				var page = $i.pages.find.visible();
				var intervals = this._get();
				if(intervals!==false){
					if(interval == null){ //Clear all intervals
						for(var i=0;i<intervals.length;i++){
							clearInterval(intervals[i].intervalId);
							$i.pages.find.visible().intervals[i].intervalId = null;
							if($i._get('debug')){
								//console.log("Interval: " + intervals[i].name + " cleared on page: " + page.pageHash);
							}
						}
						
					}
					else{ //Clear specific interval.
						for(var i=0;i<intervals.length;i++){
							if(intervals[i].name==interval){
								clearInterval(intervals[i].intervalId);
								$i.pages.find.visible().intervals[i].intervalId = null;
								if($i._get('debug')){
									//console.log("Interval: " + intervals[i].name + " cleared on page: " + page.pageHash);
								}
							}
						}
						
					}
					
				}
			};
			
			this._get = function(identifier){
				var name = identifier || null;
				var intervals = $i.pages.find.visible().intervals;
				if(name!=null){ //If we want to know if there is any interval name added to this page, return array position
					for(var i=0;i<intervals.length;i++){
						if(intervals[i].name == name){
							return i;
						}
					}
				}
				else{ //If we want a list of intervals of this page, return array of intervals
					if(intervals && intervals.length!=0){
						return intervals;
					}
				}
				//If any not found, return false.
				return false;
			};
			
			this.apply = function(){
				var intervals = $i.pages.find.visible().intervals;
				for(var i=0;i<intervals.length;i++){
					var val = setInterval(intervals[i].func,intervals[i].p);
					$i.pages.find.visible().intervals[i].intervalId = val;
					if($i._get('debug')){
						//console.log("Interval: " + intervals[i].name + " restored on page: " + $i.pages.find.visible().pageHash);
					}
				}
			};
		};
		
		var app = function(){
			var appIntervals = [];
			
			this._add = function (identifier,f,period){
				var val = setInterval(f,period);

				var interval = this._get(identifier);
				if(interval === false){
					appIntervals.push({name: identifier, intervalId: val,func: f,p: period,enabled: true});
					if($i._get('debug')){
						//console.log("Interval: " + identifier + " added on app, period: "+period);
					}
				}
				else{
					if(appIntervals[interval].enabled){
						this.clear(appIntervals[interval].identifier);
					}
					appIntervals[interval] = {name: appIntervals[interval].name, intervalId: val, func: appIntervals[interval].func, p: appIntervals[interval].p,enabled:true};
					if($i._get('debug')){
						//console.log("Interval: " + identifier + " updated on app, period: "+period);
					}
				}
				
			};
			
			this.clear = function (intervalName){
				var interval = intervalName || null;
				
				var intervals = this._get(interval);
				if(intervals!==false){
					if(interval == null){ //Clear all intervals
						if($i._get('debug')){
							//console.log("Clearing all app intervals.");
						}
						for(var i=0;i<intervals.length;i++){
							clearInterval(intervals[i].intervalId);
							intervals[i].intervalId = null;
							if($i._get('debug')){
								//console.log("Interval: " + intervals[i].name + " cleared on app");
							}
						}
						return true;
						
					}
					else{ //Clear specific interval.
						clearInterval(appIntervals[intervals].intervalId);
						appIntervals[intervals].enabled = false;
						if($i._get('debug')){
							//console.log("Interval: " + appIntervals[intervals].name + " cleared on app");
						}
						return true;
					}
				}
				return false;
			};
			
			this._get = function(identifier){
				var name = identifier || null;
				var intervals = appIntervals;
				if(name!=null){ //If we want to know if there is any interval name added to this page, return array position
					for(var i=0;i<intervals.length;i++){
						if(intervals[i].name == name){
							return i;
						}
					}
				}
				else{ //If we want a list of intervals of this page, return array of intervals
					if(intervals.length!=0){
						return intervals;
					}
				}
				//If any not found, return false.
				return false;
			};
			
			this.apply = function(intervalName){
				var interval = intervalName || null;
				var intervals = this._get(interval);
				if(intervals!==false){ 
					if(interval==null){ //Restore all App Intervals
						for(var i=0;i<intervals.length;i++){
							if(intervals[i].intervalId!=null){ //If the interval is already running, we restart it
								$i.intervals.app.clear(intervals[i].name);
							}
							var val = setInterval(intervals[i].func,intervals[i].p);
							intervals[i].intervalId = val;
							if($i._get('debug')){
								//console.log("Interval: " + intervals[i].name + " restored on app");
							}
						}
					}
					else{ //Restore specific Interval
						if(appIntervals[intervals].intervalId!=null){ //If the interval is already running, we restart it
							$i.intervals.app.clear(interval);
						}
						var val = setInterval(appIntervals[intervals].func,appIntervals[intervals].p);
						appIntervals[intervals].intervalId = val;
						if($i._get('debug')){
							//console.log("Interval: " + appIntervals[intervals].name + " restored on app");
						}
					}
					
				}
				
			};
			
			
		};
	
		this.pages = new pages();
		this.app = new app();
	}
	
	idcp.intervals = new idcp_intervals_management();
	//console.log("class idcp.intervals ready to be used.");
})();
