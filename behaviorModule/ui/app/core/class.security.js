/**
 * Given a userModuleName
 * 
 * menu -> each route of fullmenu.json
 */

(function(){
	function idcp_security(){
		
		/**
		 * Given an object id (usually a table_id), check the access for a needed actions.
		 * @param integer object_id
		 * @param object page 
		 */
		var accessTo = function(object_id){
			var logged_uid = idcp.user._get().id; //ID for Logged user
			var logged_gid = idcp.user._get().group.id; //Group ID for logged user.
			var oid = object_id; //Object ID.
			var permission;
			var permissions = idcp.user._get().group.permissions;
			for(var i=0,j=permissions.length;i<j;i++){
				if(permissions[i].table_id == oid || permissions[i].table_name == oid){
					permission = permissions[i];
				}
			}
			if(permission == null){
				console.log("ALERTA... de l'objecte especificat ("+oid+") no hem trobat cap grup de permissos.");
			}
				
			/**
			 * Check if current user can create a resource on object.
			 */
			this.create = function(){
				return ((permission.group_p.u.charAt(0)==1)? true:false);
			}
			
			this.read = function(){
				
			}
			
			/**
			 * Given the resource owner user and the resource owner group,
			 * check if current user can write the resource
			 * @param integer ruid
			 * @param integer rgid
			 * @return boolean
			 */
			this.write = function(ruid,rgid){
				if(ruid==logged_uid && permission.group_p.u.charAt(2)==1){
					return true;
				}
				if(rgid==logged_gid && permission.group_p.g.charAt(2)==1){
					return true;
				}
				if(permission.group_p.o.charAt(2)==1){
					return true;
				}
				
				return false;
			}
			
			/**
			 * Delete alias
			 * @param integer ruid
			 * @param integer rgid
			 * @return boolean
			 */
			this.del = function(ruid,rgid){
				if(ruid==logged_uid && permission.group_p.u.charAt(3)==1){
					return true;
				}
				if(rgid==logged_gid && permission.group_p.g.charAt(3)==1){
					return true;
				}
				if(permission.group_p.o.charAt(3)==1){
					return true;
				}
				return false;
			}
			
			this.administrate = function(ruid,rgid){
				//En el cas que ens enviin que el element que volem accedir pertany a lusuari null i password null... vol dir que és element nou per tant
				//posem els permisos com si fossim el propietari i pertanyes al nostre grup.
				ruid = ruid==null? logged_uid:ruid;
				rgid = rgid==null? logged_gid:rgid;
				//Si l'usuari propietari sóc jo i el meu grup té permissos d'administració per als elements els quals sóc el propietari.. ok
				if(ruid==logged_uid && permission.group_p.u.charAt(4)==1){
					return true;
				}
				//Si el registre és del meu grup  i el meu grup té permissos d'adminsitració per als elements del meu grup...
				if(rgid==logged_gid && permission.group_p.g.charAt(4)==1){
					return true;
				}
				if(permission.group_p.o.charAt(4)==1){
					return true;
				}
				return false;
			}
			
		};
		
		this.access = function(object_id){
			return new accessTo(object_id);
		}
		
		/**
		 * Methods related to render the main menu.
		 */
		this.menu = new (function(){
			/**
			 * Checks for a given menu if the user is allowed to access it.
			 * @param menu
			 * @returns {Boolean}
			 */
			this.checkAccessToModule = function(menu){
				if(!idcp._get('webservice').authentication_required) return true;
				for(var k=0,l=menu.modules.length;k<l;k++){
					var mid = menu.modules[k];
					if(!checkModuleAccess(mid)){
						return false;
					}
					
				}
				//De tots els moduls requerits per a renderitzar aquest menu, comprovem si 
				for(var i=0,j=menu.actions.length;i<j;i++){
					if(!checkActionFromModules(menu.modules, menu.actions[i])){
						return false;
					}
				}
				return true;
			}
			
			var checkModuleAccess = function(mid){
				var usermodules = $i.user._get().role.modules;
				for(var i=0,j=usermodules.length;i<j;i++){
					var amod = usermodules[i];
					if(amod.id == mid){
						return true;
					}
				}
				return false;
			}
			
			/**
			 * Given an array of modules, return if the action is in any of the modules.
			 * @param array modules
			 * @param integer action_id
			 * @return boolean
			 */
			var checkActionFromModules = function(modules,action){
				for(var i=0,j=modules.length;i<j;i++){ //Loop array modules
					var module = getModuleById(modules[i]); //Get current module.
					if(module!==false){ //If current module exists.
						var moduleactions = module.actions; //Get current module actions
						for(var k=0,l=moduleactions.length;k<l;k++){ //Loop actions.
							var maction = moduleactions[k]; //Get current action from current module
							if(maction.action_id == action){ //If current action equals action
								return true; //Return true.
							}
						}
					}
				}
				return false;
			}
			
			/**
			 * Get module by module id
			 * @param integer module_id
			 * @return module||false
			 */
			var getModuleById = function(mid){
				var usermodules = $i.user._get().role.modules;
				for(var i=0,j=usermodules.length;i<j;i++){
					if(usermodules[i].id == mid){
						return usermodules[i];
					}
				}
				return false;
			}
			
		})();
	}
	
	idcp.security = new idcp_security();
})();