/**
 * START PAGE OBJECTS AND METHODS
 * 
 * @author phidalgo

 * Object pages
 * A page object is like:  
 * {
	pageHash: pageHash, //Unique session page identifier.
	urlHash: window.location.hash, //URL has
	html: files[index], //HTML (template) file loaded.
	intervals: [], //Page intervals array
	cache: cache, //If this page can be cached.
	ui_setup : $i.getPageUISetup(window.location.hash), //UI setup object.
	ecmafile : null, //Javascript file of this page (template).
	script : null, //Javascript Code
	object : null, //Javascript object
	buttons : null, //Page buttons (if any).
	callback: ((cbf!=null)? 'callback_'+cbf:null), //Callback function (TODO:: Deprecated. Now javascript object has a method called callback).
	visits : 1, //Number of visits of this page (only works if the page is in caching system.
	visible: true //If the page is now visible.
}
 */

(function(){
	function idcp_pages_management(){
		var pagesArray = [];
		
		/**
		 * Class find
		 * 
		 * @author phidalgo
		 */
		var find = function(){
			/**
			 * Get visible page object.
			 */
			this.visible = function(){
				for(var i=0;i<pagesArray.length;i++){
					page = pagesArray[i];
					if(page.visible){
						return page;
					}
				}
				return false;
			};
			
			/**
			 * Get visible (page.visible) jQuery object
			 */
			this.me = function(){
				for(var i=0;i<pagesArray.length;i++){
					page = pagesArray[i];
					if(page.visible){
						return $('div[data-pageHash="'+page.pageHash+'"]');
					}
				}
				return false;
			};
			
			/**
			 * Get visible (by url) jQuery object
			 */
			this.url = function(){
				var url = window.location.hash;
				for(var i=0;i<pagesArray.length;i++){
					page = pagesArray[i];
					if (page.urlHash == url){
						return $('div[data-pageHash="'+page.pageHash+'"]');
					}
				}
				return false;
			};
			
			/**
			 * Get visible page array key
			 */
			this.key = function(pageHash){
				for(var i=0,j=pagesArray.length;i<j;i++){
					page = pagesArray[i];
					if (page.pageHash == pageHash){
						return i;
					}
				}
				return false;
			};
			
			this.page = function(pageHash){
				var key = this.key(pageHash)
				return (key===false? false:pagesArray[key]);
			}
		};
		
		/**
		 * Set page buttons on content header.
		 * 
		 * @param object config
		 * @author phidalgo
		 * 
		 * A current example config object is:
		 * 
		 *  var pobject = {
		 *		type: 'new',
		 *		back: true,
		 *		onClick: newDepartment
		 *	};
		 *
		 *	Type means what kind of button do you want. Actually, there is a limited number of options:
		 *		-LIST : Adds a "new" button and adds a event :NEW_ELEMENT.
		 *			-For this one, you must set an onClick option on the config object, specifying the function to be executed when the user clicks to new :)
		 *		-form : Adds two buttons, Submit and Undo, each with the specific events.
		 *			-The same for new, you must set an onClick option on the config object with the function to be executed.
		 *
		 *	Back means if you want to show a "back" button, this button will do a history.back(1);. 
		 *
		 * 	Validation method options.
		 * 		By default, when the type is form we don't do any validation. If you want to validate the form, you must add the validate option and set it's to true.
		 * 		Validate property can be also a function, to personalize validation checks (if needed). This function must return true (success) or false (fail), in order to know if we have to
		 * 		submit form or not.
		 * 
		 * 		When validate property sets to true, this object do the following.
		 * 			1- Get all input elements from the current visible page.
		 * 			2- For each input, get the type, value, data-name and required attribute.
		 * 			3- If required is set and data-name is not null or empty, check if the field is null, empty or something.
		 * 			4- If check validation for each input fails, add a has-error class in the form-group class div (form group container) and also appends in this div a error class.
		 * 			5- Adds onchange||onkey press event in order to trigger when user fills or select data into the input.
		 *
		 *
		 *		Personalize Error messages adding error message in the forms.json file for each language. The personalized error name has to be the "data-name" value. If no 
		 *		personalized error message found, will appear default error field message.
		 */
		var buttons = function(page){
			var pageHash = page.pageHash;
			this.create = create;
			this.box = $('#page-specific-buttons'); //Box where the buttons will be.
			
			this.config = ((typeof(config)!=='undefined')? config:{});
			
			this.buttons =  $('<div data-bthash="'+pageHash+'" class="visible"><div data-buttonstype="PRIMARY" style="display:inline;"></div><div data-buttonstype="EXTRA"  style="display:inline;"></div></div>');
			
			this.remove = remove;
			
			this.validate = typeof(this.config.validate)!=='undefined'? config.validate:false;
			
			this.validateAppendFunction = typeof(this.config.validateAppendFunction)==='function'? this.validateAppendFunction:null;
			
			this.hideDeleteButton = false;
			
			this.styles = {};
			this.styles.icon = {};
			this.styles.icon.saving = "fa fa-circle-o-notch fa-spin-2x";
			this.styles.icon.saved = "fa fa-floppy-o";
			
			//this.styles.submit = "btn btn-success";
			this.styles.submit = "btn btn-white btn-success";
			
			this.styles.del = 'btn btn-white';
			
			//this.styles.submitexit = "btn btn-success";
			this.styles.submitexit = "btn btn-white btn-success";
			this.styles.options = 'btn btn-white';
			
			this.styles.icon.del = 'fa fa-trash';
			
			this.styles.reload = 'btn btn-white';
			
			this.styles.extra = 'btn btn-white';
			
			//this.styles.undoform = "btn btn-danger no-visible";
			this.styles.undoform = "btn btn-white btn-danger no-visible";
			
			//this.styles.newelement = "btn btn-light";
			this.styles.newelement = "btn btn-white btn-info";
			
			//this.styles.back = "btn btn-grey";
			this.styles.back = "btn btn-white btn-primary";
			
			var _this = this;
			var bSubmit,bSExit;
			
			function remove(){
				this.box.html('');
			}
			
			this.extraButtonsCounter = 0;
			
			/**
			 * Adds an extra button.
			 * extra is an array with a object values like:
			 * extra {
			 * 	icon : button icon
			 * 	tooltip : button tooltip
			 * 	click : click event funcion.
			 * }
			 */
			this.addExtraButton = function(extra){
				var renderEB = "";
				var buttonsAdded = [];
				$.each(extra,function(){
					renderEB = ''+
					'<button class="'+_this.styles.extra+'" data-action=":OPTION_EXTRA_'+(_this.extraButtonsCounter++)+'" data-tooltip="true" data-placement="bottom" title="'+this.tooltip+'">'+
						'<i class="ace-icon '+this.icon+' align-top bigger-160 icon-only"></i>'+
					'</button>';
					_this.buttons.find('div[data-buttonstype="EXTRA"]').append(renderEB);
					$b = _this.buttons.find('button:last');
					
					if(typeof(this.uuid)!=='undefined'){
						$b.attr('data-uuid',this.uuid);
					}
					
					var onClickF = this.click;
					$b.on('click',function(){
						if(!$b.hasClass('disabled')){
							onClickF();
						}
					});
					
					$b.tooltip();
					buttonsAdded.push($b);
				});
				return buttonsAdded;
			}
			
			/**
			 * To be able to delete an extra button, you need to set a uuid (unique universal identifier) to the button.
			 * This can be done when adding the button.
			 */
			this.delExtraButton = function(uuid){
				var $b = _this.buttons.find('div[data-buttonstype="EXTRA"] button[data-uuid="'+uuid+'"]');
				if($b.length>0){
					$b.remove();
				}
			}
			
			/**
			 * Same as delExtrabutton but in this case only disables.
			 */
			this.disableExtraButton = function(uuid){
				var $b = _this.buttons.find('div[data-buttonstype="EXTRA"] button[data-uuid="'+uuid+'"]');
				if(!$b.hasClass('disabled')){
					$b.addClass('disabled')
				}
			}
			
			/**
			 * Same as delExtrabutton but in this case only enables.
			 */
			this.enableExtraButton = function(uuid){
				var $b = _this.buttons.find('div[data-buttonstype="EXTRA"] button[data-uuid="'+uuid+'"]');
				$b.removeClass('disabled')
				
			}
			
			/**
			 * Creates a drop down button.
			 */
			this.addDropButton = function(drop){
				var renderEB = "";
				var buttonsAdded = [];
				$.each(drop,function(){
					renderEB = ''+
					'<div class="btn-group">'+
					'<button data-toggle="dropdown" class="'+_this.styles.extra+' dropdown-toggle" aria-expanded="false" data-tooltip="true" data-placement="right" title="'+this.tooltip+'">'+
					'<span class="ace-icon '+this.icon+' align-top bigger-160 icon-only"></span>'+
					'</button>'+
					'<ul class="dropdown-menu dropdown-pink">'+this.list+'</ul>'+
					'</div>';
					_this.buttons.find('div[data-buttonstype="EXTRA"]').append(renderEB);
					$b = _this.buttons.find('button:last');
					//$b.on('click',this.click);
					$b.tooltip();
					buttonsAdded.push($b);
				});
				return buttonsAdded;
			}
			
			/**
			 * Adds other elements, not a buttons.
			 * others is an array with a object values like:
			 * others {
			 * 	tooltip : element tooltip.
			 * 	render : renderable element (function or string)
			 * 	event : event type to add a listener
			 * 	execute : execute function when event is triggered.
			 * }
			 */
			this.addOtherElements = function(others){
				var $othersDiv = this.buttons.find('div[data-content="others"]');
				if($othersDiv.length==0){
					this.buttons.append('<div data-content="others" style="margin-left:10px;display:inline;"></div>');
				}
				$othersDiv = this.buttons.find('div[data-content="others"]');
				$.each(others,function(){
					var render = "";
					if(typeof(this.render)==='function') render = $(this.render());
					else render = $(this.render);
					if(typeof(this.event)!=='undefined'){
						if(typeof(this.tooltip)!=='undefined'){
							render
							.attr('title',this.tooltip)
							.attr('data-placement','bottom');
						}
						$othersDiv.append(render);
						render.tooltip();
						var ex;
						if(typeof(this.execute)!=='undefined') ex = this.execute;
						else ex = function(){ console.log("other action event triggered!");}
						render.on(this.event,ex);
					}
				});
			}
	
			
			this.goBack = function(event){
				//In case of the type of button is form and the current page is not cached, and also there is any change in the form, when
				//we go back, we will notify  to the user that if she/he continues with the go back process, the modified content will be lost.
				var act = true;
				if(_this.config.type.toUpperCase()=='FORM'){
					var formChanged = page.$.isChanged();
					if(formChanged){
						//Even the page is in the caching system, when access again to the page it will reload the data.
						//This part has to be checked because the file uploading class will need special attention.
						$i.modal({
							show : true,
							autoClose : true,
							title: $i.i18n('md_title_confirm'),
							description : $i.i18n('md_desc_cofirm_back'),
							width: '500px',
							resizable : false,
							success : function(){
								$i.history.back();
							}
						});
					}
					else{
						$i.history.back();
					}
				}
				else{
					$i.history.back();
				}
			};
			
			this.box.i18n();
			
			this.init = function(){
				this.box.append(this.buttons);
			}
			this.init();
			
			this.hideDelete = function(){
				this.buttons.find('div[data-buttonstype="PRIMARY"]').find('button[data-action=":DELETE"]').addClass("hide");
				this.hideDeleteButton = true;
			};
			
			this.showDelete = function(){
				this.buttons.find('div[data-buttonstype="PRIMARY"]').find('button[data-action=":DELETE"]').removeClass("hide");
				this.hideDeleteButton = false;
			};
			
			/*this.disableSave = function(){
				this.buttons.find('div[data-buttonstype="PRIMARY"]').find('button[data-action=":SUBMIT_FORM"]').addClass("hide");
				this.buttons.find('div[data-buttonstype="PRIMARY"]').find('button[data-action=":SUBMIT_AND_EXIT"]').addClass("hide");
			}
			
			this.disableCreate = function(){
				this.buttons.find('div[data-buttonstype="PRIMARY"]').find('button[data-action=":NEW_ELEMENT"]').addClass("hide");
			}*/
			
			/**
			 * Return button type (new,form,back) or null if no button is set.
			 */
			this.getType = function(){
				return (typeof(this.config.type)!=='undefined'? this.config.type:null);
			}
			
			function create(config){
				this.config = config;
				var render = "";
				if(this.config!==null){
					if(this.config.back){ //Enabled when you want the back button on the buttons box.
						render += '\
						<button class="'+this.styles.back+'" data-action=":HISTORY_BACK" data-tooltip="true" data-placement="bottom" title="'+$i.i18n("layout:back_button")+'">\
						<i class="ace-icon fa fa-arrow-left align-top bigger-160 icon-only"></i>\
						</button>';
					}
					switch(this.config.type.toUpperCase()){
						case 'LIST': //Use this when you want a "New Element Button"
							render += '\
							<button class="'+this.styles.newelement+'" data-action=":NEW_ELEMENT" data-tooltip="true" data-placement="bottom" title="'+$i.i18n("layout:new_button")+'">\
								<i class="ace-icon fa fa-plus align-top bigger-160 icon-only"></i>\
							</button>';
							break;
						case 'FORM': //Use this when you want a form generic-buttons.
							render += '<button class="'+this.styles.submit+'" data-action=":SUBMIT_FORM" data-tooltip="true" data-placement="bottom" title="'+$i.i18n("layout:submit_button")+'">'+
								'<i class="ace-icon fa '+this.styles.icon.saved+' align-top bigger-160 icon-only" data-saving-class="'+this.styles.icon.saving+'" data-saved-class="'+this.styles.icon.saved+'"></i>'+
							'</button>'+
							'<button class="'+this.styles.submitexit+'" data-action=":SUBMIT_AND_EXIT" data-tooltip="true" data-placement="bottom" title="'+$i.i18n("layout:submit_exit_button")+'">'+
								'<i class="ace-icon fa '+this.styles.icon.saved+' align-top bigger-160 icon-only" data-saving-class="'+this.styles.icon.saving+'" data-saved-class="'+this.styles.icon.saved+'"></i>'+
							'</button>'+
							'<button class="'+this.styles.del+' '+(this.hideDeleteButton? 'hide':'')+'" data-action=":DELETE" data-tooltip="true" data-placement="bottom" title="'+$i.i18n("layout:delete_button")+'">'+
							'<i class="ace-icon fa '+this.styles.icon.del+' align-top bigger-160 icon-only" data-saving-class="'+this.styles.icon.saving+'" data-saved-class="'+this.styles.icon.del+'"></i>'+
							'</button>'+
							'<button class="'+this.styles.undoform+'" data-action=":UNDO_FORM" data-tooltip="true" data-placement="bottom" title="'+$i.i18n("layout:undo_button")+'">'+
								'<i class="ace-icon fa fa-undo bigger-160 icon-only" ></i>'+
							'</button>';
							break;
					}
					
					if(this.config.reload){
						render += '\
						<button class="'+this.styles.reload+'" data-action=":OPTION_RELOAD" data-tooltip="true" data-placement="bottom" title="'+$i.i18n('lbl_refresh')+'">\
							<i class="ace-icon fa fa-refresh align-top bigger-160 icon-only"></i>\
						</button>';
					}
					
					if(this.config.options){
						render += '\
						<button class="'+this.styles.options+'" data-action=":OPTION_CLICK" data-tooltip="true" data-placement="bottom" title="'+$i.i18n("layout:options_button")+'">\
							<i class="ace-icon fa fa-cog align-top bigger-160 icon-only"></i>\
						</button>';
					}
					
					if(typeof(this.config.extra)!=='undefined'){
						_this.extraButtonsCounter = 1;
						$.each(this.config.extra,function(){
							render += '\
							<button class="'+_this.styles.extra+'" data-action=":OPTION_EXTRA_'+(_this.extraButtonsCounter++)+'" data-tooltip="true" data-placement="bottom" title="'+this.tooltip+'">\
								<i class="ace-icon '+this.icon+' align-top bigger-160 icon-only"></i>\
							</button>';
						});
					}
					
					this.buttons.find('div[data-buttonstype="PRIMARY"]').html(render);
					//this.box.append(this.buttons);
					
					if(typeof(this.config.other)!=='undefined'){
						this.buttons.append('<div data-content="others" style="margin-left:10px;display:inline;"></div>');
						$.each(this.config.other,function(){
							var render = "";
							if(typeof(this.render)==='function') render = $(this.render());
							else render = $(this.render);
							if(typeof(this.event)!=='undefined'){
								if(typeof(this.tooltip)!=='undefined'){
									render
									.attr('data-tooltip','true')
									.attr('title',this.tooltip)
									.attr('data-placement','bottom');
								}
								_this.buttons.find('div[data-content="others"]').append(render);
								var ex;
								if(typeof(this.execute)!=='undefined') ex = this.execute;
								else ex = function(){ console.log("other action event triggered!");}
								render.on(this.event,ex);
							}
						});
						this.otherBox = this.buttons.find('div[data-content="others"]')
					}
	
					this.box.find('[data-tooltip="true"]').tooltip();
					
					if(this.config.type.toUpperCase()=='FORM'){
						page.$.trackChanges();
						this.box.find('div[data-bthash="'+pageHash+'"]').find('button[data-action=":UNDO_FORM"]').on('click',function(event){
							page.$.restoreChanges();
							$(this).addClass('no-visible'); //Make undo button no visible
						});
						bSubmit = this.box.find('div[data-bthash="'+pageHash+'"]').find('button[data-action=":SUBMIT_FORM"]');
						bSubmit.on('click',{saveAndExit : false},clickSave);
						bSExit = this.box.find('div[data-bthash="'+pageHash+'"]').find('button[data-action=":SUBMIT_AND_EXIT"]'); 
						bSExit.on('click',{saveAndExit : true},clickSave);
						
						bdelete = this.box.find('div[data-bthash="'+pageHash+'"]').find('button[data-action=":DELETE"]');
						bdelete.on('click',deleteFunction);
						
						$i.pages.find.me().form(); //Make it work as a form. TODO -> això s'ha de fer diferent. Aquesta classe té accés al objecte pàgina actual. Això no caldria fer-ho així.
					}
					
					if(this.config.type.toUpperCase()=='LIST'){
						this.box.find('div[data-bthash="'+pageHash+'"]').find('button[data-action=":NEW_ELEMENT"]').on('click',function(event){
							event.preventDefault();
							event.stopPropagation();
							_this.config.create();
						});
					}
					if(this.config.back){
						this.box.find('div[data-bthash="'+pageHash+'"]').find('button[data-action=":HISTORY_BACK"]').on('click',function(event){
							_this.goBack(event);
						});
					}
					if(this.config.reload){
						this.box.find('div[data-bthash="'+pageHash+'"]').find('button[data-action=":OPTION_RELOAD"]').on('click',function(event){
							event.preventDefault();
							event.stopPropagation();
							_this.config.doReload();
						});
					}
					
					if(this.config.options){
						this.box.find('div[data-bthash="'+pageHash+'"]').find('button[data-action=":OPTION_CLICK"]').on('click',function(event){
							event.preventDefault();
							event.stopPropagation();
							_this.config.doOption();
						});
					}
					
					if(typeof(this.config.extra)!=='undefined'){
						var ec = 1;
						$.each(this.config.extra,function(){
							var ef = this.click;
							_this.box.find('div[data-bthash="'+pageHash+'"]').find('button[data-action=":OPTION_EXTRA_'+(ec++)+'"]').on('click',function(event){
								event.preventDefault();
								event.stopPropagation();
								if(typeof(ef)==='function'){
									ef();
								}
								else{
									console.log(':OPTION_EXTRA clicked but no function attached');
								}
							});
						});
					}
				};
				
				if(typeof(this.config.done)==='function'){
					this.config.done(this);
				}
				
				/**
				 * Called by the developer when this.config.submit function success or fails and we want
				 * to notify to buttons to upload buttons status.
				 */
				function onAPIResponse(apiSuccess){
					var iSubmit = bSubmit.find('i');
					var iSExit = bSExit.find('i');
					iSubmit.removeClass(iSubmit.data('saving-class')).addClass(iSubmit.data('saved-class'));
					iSExit.removeClass(iSExit.data('saving-class')).addClass(iSExit.data('saved-class'));
					bSubmit.on('click',{saveAndExit : false},clickSave);
					bSExit.on('click',{saveAndExit : true},clickSave);
					if(apiSuccess){
						page.$.applyChanges(); //If webservice returns OK, apply changes.
						if(_this.saveAndExitClicked){
							$i.history.back();
						}
					}
				}
				this.saveAndExitClicked = false; 
				function clickSave(event){
					_this.saveAndExitClicked = event.data.saveAndExit;
					//console.log("Click Button : " + (_this.saveAndExitClicked? 'Save and Exit':'Save'));
					event.preventDefault();
					event.stopPropagation();
					if($i.pages.find.me().validate()){ //TODO:: L'objecte buttons (aquesta classe) ja té accés a l'objecte pàgina actual. Això no s'hauria de fer així.
						var values = page.$.getValues(true);
						if(values!=null){
							var ic = $(this).find('i');
							ic.removeClass(_this.styles.icon.saved);
							ic.addClass(_this.styles.icon.saving);
							bSubmit.off('click',clickSave);
							bSExit.off('click',clickSave);
							_this.config.submit(values,onAPIResponse);
						}
					}
				}
				
				function deleteFunction(){
					event.preventDefault();
					event.stopPropagation();
					if(typeof(_this.config.ondelete)==='function'){
						_this.config.ondelete(function(){
							onAPIResponse();
							$i.history.back();
						});
					}
				}
			};
	};

		
		this.hide = function(){
			for(var i=0;i<pagesArray.length;i++){
				page = pagesArray[i];
				if (page.visible){
					var content = $i.UI.layout_content.find('div[data-pageHash="'+page.pageHash+'"]');
					var buttons = $('#page-specific-buttons').find('div[data-bthash="'+page.pageHash+'"]');
					if(page.cache){
						content.removeClass("visible").addClass("no-visible");
						buttons.removeClass("visible").addClass("no-visible");
						$i.intervals.pages.clear();
						pagesArray[i].visible = false;
					}
					else{
						content.remove();
						buttons.remove();
						pagesArray.splice(i, 1);
					}
					return true;
				}
			}
			return false;
		};
		
		this.show = function(){
			var url = window.location.hash;
			for(var i=0;i<pagesArray.length;i++){
				page = pagesArray[i];
				// Compare current location hash with page object hash
				if (page.urlHash== url){
					var content = $i.UI.layout_content.find('div[data-pageHash="'+page.pageHash+'"]');
					var buttons = $('#page-specific-buttons').find('div[data-bthash="'+page.pageHash+'"]');
					buttons.removeClass("no-visible").addClass("visible");
					content.removeClass("no-visible");
					content.removeClass("no-visible").addClass("visible");
					pagesArray[i].visits++;
					pagesArray[i].visible = true;
					$i.intervals.pages.apply();
					$i.menu.openMenu();
					if(page.callback!=null){ //Execute the callback function (if exists);
						if(page.script!=null && typeof(page.script)==='function'){
							try{
								page.object.callback();
							}
							catch(e){
								console.log("error executing script callback");
								console.log(e);
							}
							
						}
					}
					return true;
				}
			}
			return false;
		};
		
		this.inCache = function(){
			var url = window.location.hash;
			for(var i=0,j=pagesArray.length;i<j;i++){
				page = pagesArray[i];
				if(page.urlHash==url){
					return true;
				}
			}
			return false;
		}
		
	
		this._add = function(page){
			page.buttons = new buttons(page); //Creem botons de la pàgina. Es mostraràn si calen, en funció del tipus de pàgina.
			
			/**
			 * Configuració de la pàgina.
			 */
			var pageConfig = {
				type 		: null, //Tipus de pàgina (LIST o FORM o READONLY) (obligatori)
				api			: null, //Adreça de la API amb la que aquesta pàgina ha d'interactuar (opcional)
				autoshow 	: page.autoshow, //Ho agafem de l'objecte creat inicialment.
				objectName 	: null, //Nom d'objecte (taula) que gestionarem des d'aquesta pàgina (per agafar permisos etc) (opcional)
				object 		: false, //Objecte en sí. (opcional, auto-assignable)
				trackChanges: false, //Control de canvis. (opcional, per defecte false)
				showBack 	: true, //Ensenyar botó enrere (opcional, per defecte true)
				submit 		: function(){ console.log("No submit function attached on this page");}, //Funció que s'executarà quan cliquem al botó submit o submit and exit o create. (opcional)
				remove 		: function(){ console.log("No delete function attached on this page");}, //Funció que s'executarà quan cliquem al botó remove. (opcional)
				beforeSubmit: function(){ console.log("No beforeSubmit function attachedo on this page");}, //Funció que s'executarà abans l'execució de submit. (opcional)
				afterSubmit : function(){ console.log("No afterSubmit function attached on this page");}, //Funció que s'executarà al acabar l'execució de submit. (opcional)
				resource 	: null, //Recurs associat a aquesta pàgina (opcional)
				configured 	: false, //Si la pàgina ha sigut configurada (auto-assignable)
				widgets 	: [] //Widgets que s'estàn executant en aquesta pàgina.
				
			}
			
			var buttonsObj = {};
			
			
			/**
			 * Saves plugin objects to access it if needed.
			 */
			page.plugins = function(pluginName,pluginObject){
				//We will check if plugin is loaded when the page is ready to be show to the user.
				pageConfig.widgets.push({name : pluginName, object: pluginObject,loaded : false,error : null});
			}
			
			/**
			 * Mètode per a configurar la pàgina. S'hauria de cridar a l'inici de cada script.
			 * Addicionalment comprova el recurs que es vol gestionar des de la pàgina (si existeix),
			 * actualitza el breadcrumbs, el titol de la pàgina i assigna propietats a l'objecte pàgina (newElement,resource,...etc).
			 * 
			 * @param config object
			 * 
			 */
			page.config = function(config){
				pageConfig.type 		= (typeof(config.type)!=='undefined'? config.type.toUpperCase():null);
				pageConfig.api 			= (typeof(config.api)!=='undefined'? config.api:null);
				pageConfig.autoshow 	= (typeof(config.autoshow)==='boolean'? config.autoshow:page.autoshow);
				pageConfig.objectName 	= (typeof(config.object)!=='undefined'? config.object:null);
				pageConfig.trackChanges = (typeof(config.trackChanges)==='boolean'? config.trackChanges:false);
				pageConfig.showBack 	= (typeof(config.showBack)==='boolean'? config.showBack:false);
				pageConfig.submit	 	= (typeof(config.submit)==='function'? config.submit:null);
				pageConfig.remove	 	= (typeof(config.remove)==='function'? config.remove:null);
				pageConfig.beforeSubmit	 = (typeof(config.beforeSubmit)==='function'? config.beforeSubmit:null);
				pageConfig.afterSubmit	 = (typeof(config.afterSubmit)==='function'? config.afterSubmit:null);
				pageConfig.resource 	= (typeof(config.resource)!=='undefined'? config.resource:null);
				
				
				pageConfig.configured 	= true;
				
				page.autoshow = pageConfig.autoshow;
				
				if(pageConfig.api!=null && !pageConfig.api.endsWith('/')){
					pageConfig.api += "/";
				}
				
				/**
				 * Inicializtació classe pàgina (és a dir el script de la pàgina).
				 */
				page.newElement = false;
				if(pageConfig.resource!=null){
					if(pageConfig.resource===false || pageConfig.resource.toUpperCase()=='NEW'){
						page.newElement = true;
						$i.documentTitle($i.i18n('tlt_new_resource',{resource : $i.i18n(pageConfig.objectName)}))
						page.buttons.hideDelete(); //Si és nou registre no cal mostrar el botó d'esborrar...
					}
					$i.breadcrumb._add(page.newElement? $i.i18n('lbl_new_resource'):config.resource);
				}
				
				
				if(pageConfig.objectName!=null){
					pageConfig.object = $i.objects._get(pageConfig.objectName);
				}
			}
			
			/**
			 * Obté una propietat de la pàgina (objecte pageConfig).
			 * @return string.
			 */
			page._get = function(property){
				return pageConfig[property];
			}
			
			page._setResource = function(resource){
				pageConfig.resource = resource;
				return true;
			}
			
			
			/**
			 * Send the current page information to the WebService, only if this page is a form and is not a new element. 
			 */
			page.save = function(){
				if(!page.newElement && typeof(buttonsObj.submit)==='function' && pageConfig.type=="FORM"){
					buttonsObj.submit();
				}
			}
			
			/**
			 * Called when a page is loaded.
			 * Aquesta funció és important en aquelles zones on el sistema de permisos granulars esta activat.
			 * Aquesta funció, a part de fer fade in/fade out, aplicar plugins a la UI i tal, també realitza canvis
			 * en la UI en funció dels permisos de l'usuari.
			 * No afecta a la seguretat en sí, ja que la seguretat resideix en el WS, però si deshabilita botons, amaga formularis etc.
			 * 
			 * Repeteixo, no gestiona la seguretat, això es fa desde el WS. Senzillament adapta la UI en funció dels permisos.
			 * Si aquesta funció no es cridés per error, podria passar que es veiguessin alguns formularis que no s'haurien de veure, o quedarien botons
			 * de "Desa" i "Desa i surt", però ni el formulari que es veu per error tindria DADES, ni al desar ens deixaria desar (el WS tornaria 401).
			 * 
			 */
			page.loaded = function(setup){
				$i('.wysiwyg-editor').rte(); //Apply rich Text Editor to all .wysiwyg-editor class.
				$i('.date-picker').dpicker();
				$i('.datetime-picker').dtpicker();
				$i('.date-picker-historic').dpicker({historicalDate:true});
				
				$i('select').each(function(){
					//if($(this).attr('data-id')!='owner_uid' && $(this).attr('data-id')!='owner_gid' && $(this).attr('data-id')!='department_id'){
						
						if($(this).attr('required') && page.newElement){
							//console.log($(this).attr('name')+" => " + $(this)[0].length);
							$(this).prepend('<option value=""></option>');
							$(this).val(0);	
							$(this).attr('data-placeholder');
						}
					//}
					
				});
				
				/**
				 * Si tenim objecte de configuració, vol dir que podem preguntar si aquest objecte
				 * té permisos granulars, té departaments, té tags, te atributs dinàmics, ...etc
				 * Si no en té, doncs... hem de mostrar la plana "as is". 
				 * Si en té, doncs... apliquem les coses.
				 */
				if(pageConfig.object!==false){
					if(pageConfig.type =='FORM' && pageConfig.object.hasTags){
						//console.log(pageConfig.object.name+" té tags");
						if(typeof(setup.tags)!=='undefined' && typeof(setup.$tags)!=='undefined'){
							var el = '<!-- B:TAGS -->'+
							'<div class="form-group">'+
								'<label class="control-label col-xs-12 col-sm-2 no-padding-right" for="form-field-1">'+$i.i18n('lbl_tags')+'</label>'+
								'<div class="col-xs-12 col-sm-7">'+
									'<select multiple class="col-xs-12 col-sm-12" name="tags" data-id="tags">';
										//Set tags to select multiple
										$.each(setup.options.tags,function(){
											var tagF = this;
											var selected = false;
											//Mark selected tags for this client.
											$.each(setup.tags,function(){
												if(tagF.id==this.id){
													selected = true;
													return true;
												}
											});
											el += '<option value="'+this.id+'" '+(selected? 'selected':'')+'>'+this.name+'</option>';
										});
									'</select>'+
								'</div>'+
							'</div>'+
							'<!-- E:TAGS -->';
							setup.$tags.html(el);
						}
					}
					
					
					if(pageConfig.type =='FORM' && pageConfig.object.hasDynAttr){
						//console.log(pageConfig.object.name+" té atributs dinàmics");
						if(typeof(setup.options.attributes)!=='undefined' && typeof(setup.$dynAttr)!=='undefined'){
							$.each(setup.options.attributes,function(){
								var attribute = this;
								var value = "";
								$.each(setup.dynattr,function(){
									if(this.attr_id==attribute.attr_id){
										value = (this.value!=null? this.value:"");
									}
								});
								var el = '<!-- B:ATTR_'+this.attr_id+' -->'+
								'<div class="form-group">'+
									'<label class="control-label col-xs-12 col-sm-3 no-padding-right blue" for="'+attribute.attr_id+'" >* '+attribute.label+'</label>'+
									'<div class="col-xs-12 col-sm-7">'+
										'<input class="col-xs-12 col-sm-12" type="text" name="dynattr_'+attribute.attr_id+'" data-id="dynattr_'+attribute.attr_id+'" class="col-xs-12 col-sm-12" value="'+value+'">'+
									'</div>'+
								'</div>'+
								'<!-- E:ATTR_'+this.attr_id+' -->';
								setup.$dynAttr.append(el);
							});
						}
						
					}
					
					if(pageConfig.object.hasGranularP){
						//console.log(object.name+" té permisos granulars");
						if(pageConfig.type =='LIST'){
							if(!$i.security.access(pageConfig.objectName).create()){
								/**
								 * Si No tenim accés de creació, el tipus de pàgina és de només lectura (no podem crear ni desar).
								 */
								pageConfig.type = 'READONLY';
							}
						}
						
						if(pageConfig.type =='FORM' && typeof(setup)!=='undefined' && typeof(setup.owner_uid)!=='undefined' && typeof(setup.owner_gid)!=='undefined'){
							if(!$i.security.access(pageConfig.objectName).create()){
								/*
								 * Si l'usuari en qüestió no té permisos de creació en un objecte, no veurà l'icone de '+' per crear-lo,
								 * però si escriu la ruta /aaa/bbb/new podrà accedir al formulari de creació. Encara que desi el WS li denegarà la creació,
								 * però configurant la pagina com a READONLY, no podrà ni enviar la petició des de el nostre Front End. 
								 */
								pageConfig.type = 'READONLY';
							}
							
							//console.warn("setup.owner_uid: "+setup.owner_uid+", setup.owner_gid: "+setup.owner_gid);
							if(!$i.security.access(pageConfig.objectName).administrate(setup.owner_uid,setup.owner_gid)){
								//console.warn("No té permisos d'administració");
								if(typeof(setup.$administration)!=='undefined'){
									setup.$administration.remove();
								}
							}
							else{
								//console.warn("Té permisos d'administració");
								var aside = false;
								var $administration_zone = null;
								if(typeof(setup.$administration)==='undefined'){
									//console.warn("This user can administrate this resource. I will render an aside");
									aside = true;
									var $renderAside = $('<div id="right-menu" class="modal aside" data-body-scroll="false" data-offset="true" data-placement="right" data-fixed="true" data-backdrop="false" tabindex="-1">'+
									'<div class="modal-dialog">'+
										'<div class="modal-content">'+
											'<div class="modal-header no-padding">'+
												'<div class="table-header idisc_background_color">'+
													'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">'+
														'<span class="white">&times;</span>'+
													'</button>'+
													idcp.i18n('lbl_permissions')+
												'</div>'+
											'</div>'+
											'<div class="modal-body">'+
											'<div class="row">'+
											'<form class="form-horizontal" role="form">'+
											'<div class="col-xs-12 col-sm-12" data-id="administration_zone">'+
											'</div>'+
											'</form>'+
											'</div>'+
											'</div>'+
										'</div><!-- /.modal-content -->'+
										'<button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" style="background-image: linear-gradient(to bottom, #bb0b24 0%, #ef0022 100%) !important;" data-target="#right-menu" data-toggle="modal" type="button">'+
											'<i data-icon1="fa-plus" data-icon2="fa-minus" class="ace-icon fa fa fa-eye fa-2x icon-only"></i>'+
										'</button>'+
									'</div><!-- /.modal-dialog -->');
									page.$.append($renderAside);
									$renderAside.ace_aside({container : page.$});
									$administration_zone = $i('#administration_zone',$renderAside);
								}
								else{
									$administration_zone = setup.$administration
								}
							
							
								var defaultOwner_uid = (setup.owner_uid==null)? idcp.user._get().id:setup.owner_uid;
								var defaultOwner_gid = (setup.owner_gid==null)? idcp.user._get().user_group:setup.owner_gid;
								var html = '<!-- B:USUARIPROPIETARI-->'+
								'<div class="form-group">'+
									'<label class="control-label col-xs-12 col-sm-'+(aside? 12:5)+' '+(aside? 'align-left':'no-padding-right')+'" for="name">'+idcp.i18n('lbl_owner_user')+'</label>'+
									'<div class="col-xs-12 col-sm-'+(aside? 12:7)+'">'+
										'<select class="col-xs-12 col-sm-12" data-select="no-search" name="owner_uid" data-id="owner_uid" required>';
											var users_glob = (typeof(setup.options)!=='undefined' && typeof(setup.options.users)!=='undefined')? setup.options.users.results:idcp._get('configuration').users.results;
											$.each(users_glob,function(){
												html += '<option value="'+this.id+'">'+this.username+'</option>';
											});
										html +='</select>'+
									'</div>'+
								'</div>'+
								'<!-- E:USUARIPROPIETARI-->'+
								'<!-- B:GRUP_PROPIETARI -->'+
								'<div class="form-group">'+
									'<label class="control-label col-xs-12 col-sm-'+(aside? 12:5)+' '+(aside? 'align-left':'no-padding-right')+'" for="name">'+idcp.i18n('lbl_owner_group')+'</label>'+
									'<div class="col-xs-12 col-sm-'+(aside? 12:7)+'">'+
										'<select class="col-xs-12 col-sm-12" data-select="no-search" name="owner_gid" data-id="owner_gid" required>';
											var groups_glob = (typeof(setup.options)!=='undefined' && typeof(setup.options.groups)!=='undefined')? setup.options.groups.results:idcp._get('configuration').groups.results;
											$.each(groups_glob,function(){
												html += '<option value="'+this.id+'">'+this.name+'</option>';
											});
											html += '</select>'+
									'</div>'+
								'</div>'+
								'<!-- E:GRUP_PROPIETARI -->';
								if(idcp.user._get().user_department==null){
									html +='<!-- B:DEPARTAMENT -->'+
									'<div class="form-group">'+
										'<label class="control-label col-xs-12 col-sm-'+(aside? 12:5)+' '+(aside? 'align-left':'no-padding-right')+'" for="name">'+idcp.i18n('lbl_visibility')+'</label>'+
										'<div class="col-xs-12 col-sm-'+(aside? 12:7)+'">'+
											'<select class="col-xs-12 col-sm-12" data-select="no-search" name="department_id" data-id="department_id" required>'+
											'<option value="ALL">'+$i.i18n('lbl_allDepartment')+'</option>';
											$.each($i._get('configuration').departments.results,function(){
												html += '<option value="'+this.id+'">'+this.name+'</option>';
											});
											html += '</select>'+
										'</div>'+
									'</div>'+
									'<!-- E:DEPARTAMENT -->';
								}

								$administration_zone.html(html);
								if(idcp.user._get().user_department==null){
									$administration_zone.find('select[data-id="department_id"]').val(((setup.dpt_id==null)? "ALL":setup.dpt_id));
								}
								
								$administration_zone.find('select[data-id="owner_uid"]').val(defaultOwner_uid);
								$administration_zone.find('select[data-id="owner_gid"]').val(defaultOwner_gid);

							}
							
							
							/**
							 * Aqui no cal mirar els permisos de lectura pq per començar ja no veurem els registres que no tenim lectura.
							 * Pero si hi accedissim manualment, el webservice ens retornaria 401 i el Front End ens desloguinaria.
							 * Per tant mirem permisos d'escriptura quan els page.buttons.getType és "form".
							 * Form indica formulari.
							 */
							if((!page.newElement && !$i.security.access(pageConfig.objectName).write(setup.owner_uid,setup.owner_gid))){
								//Si no tenim permisos d'escriptura, automàticament el mètode write canvia els inputs per span i posa el tipus de pàgina a "READONLY".
								/**
								 * Si No tenim accés de escriptura, el tipus de pàgina és de només lectura (no podem desar).
								 */
								pageConfig.type = 'READONLY';
								var $inputs = page.$.find(':input');
								$inputs.each(function(){
									var $input = $(this);
									var type = this.type || this.tagName.toLowerCase();
									var value;
									var $span = null;
									var hasIcon = false;
									switch(type){
										case 'text':
											if($input.next().length>0){
												hasIcon = true;
											}
											value = $input.val();
											value = (value == ""? '------------------':value);
											$span = $("<span />", { text: value, "class":(hasIcon? "readOnly_span_icon":"readOnly_span"),"data-original-name": $input.data('id') });
											$span.insertAfter(this);
											$input.attr("type","hidden");
											break;
										case 'hidden':
											break;
										case 'select-one':
											var optSel = $input.find('option:selected').text();
											value = (optSel==null? '':optSel);
											$span = $("<span />", { text: value, "class":"readOnly_span","data-original-name": $input.data('id') });
											$span.insertAfter(this);
											$input.remove();
											break;
										case 'select-multiple':
											var optSel = $input.find('option:selected')
											var value = ""
											for(var i=0,j=optSel.length;i<j;i++){
												value += $(optSel[i]).text()+((i+1<j)? ', ':'');
											}
											$span = $("<span />", { text: value, "class":"readOnly_span","data-original-name": $input.data('id')});
											$span.insertAfter(this);
											$input.remove();
											break;
										case 'button':
											break;
										case 'checkbox':
											$input.attr('disabled',true);
											//value = el.is(':checked');
											break;
									}
								});
								
								/**
								 * Addicionalment, posem tots els widgets a l'estat read only.
								 */
								for(var i=0,j=pageConfig.widgets.length;i<j;i++){
									if(typeof(pageConfig.widgets[i].object.readOnly)==='function'){
										pageConfig.widgets[i].object.readOnly(true);
									}
									else{
										console.warn("Widget: " + pageConfig.widgets[i].name + " does not have readOnly method");
									}
								}
							}
						}
					}
				}
				
				
				if(pageConfig.widgets.length>0){
					for(var i=0,j=pageConfig.widgets.length;i<j;i++){
						var widget = pageConfig.widgets[i];
						//console.log(widget)
						if(typeof(widget.object.pluginLoaded)==='function'){
							if(!widget.object.pluginLoaded()){
								page.$.on(widget.name+'_onload',widgetLoaded);
							}
							else{
								widgetLoaded(null,{name : widget.name});
							}
						}
						else{
							widgetLoaded(null,{name : widget.name});
						}
					}
				}
				else{
					loadPage();
				}
								

				
				/**
				 * When every widget is successfully loaded, we load the page.
				 * Els widgets autoacarreguen les llibreries necessaries el 1er cop que es criden. 
				 */
				function widgetLoaded(e,obj){
					//console.log(obj);
					for(var i=0,j=pageConfig.widgets.length;i<j;i++){
						var widget = pageConfig.widgets[i];
						if(widget.name==obj.name){
							//Els nous widgets tenen un métode pluginLoaded, si el té fem servir-lo per comprovar que realment estigui carregat.
							if(typeof(widget.object.pluginLoaded)==='function'){
								if(widget.object.pluginLoaded()){
									widget.loaded = true;
								}
								else{ 
									widget.loaded = false;
									widget.error = widget.object.error;
								}
							}
							else{ //Els widgets no antics i/o no actualtizats no ténen el metode per tant suposem que si estem aqui, es que esta carregat.
								widget.loaded = true;
							}
						}
					}
					
					//Ara mirem si tots els widgets estan carregats...
					var awl = true;
					for(var i=0,j=pageConfig.widgets.length;i<j;i++){
						var widget = pageConfig.widgets[i];
						if(!widget.loaded){
							awl = false;
							break;
						}
					}
					
					if(awl){
						loadPage();
					}
				}
				
				function loadPage(){
					//console.warn("Page Loaded :)");
					/**
					 * Si hem configurat la pàgina, creem els botons, sino no els creem pq la pàgina s'ha configurat
					 * manualment o no li cal botons.
					 */
					if(pageConfig.configured){
						buttonsObj = {
							type : pageConfig.type,
							back: pageConfig.showBack,
							validate : true
						}
						/**
						 * Si el tipus de pàgina és llistat, ens interessarà crear el botó "CREAR RECURS", en el cas que els permissos ens
						 * ho permetin (si aquesta pàgina té sistema de permisos granulars).
						 */
						if(pageConfig.type=="LIST"){
							//Si s'ha assignat una funció a la propietat de la configuració submit, executem aquesta funció.
							if(typeof(pageConfig.submit)==='function'){
								buttonsObj.create = pageConfig.submit;
							}
							else{ //Si no, apliquem la nostra funció per defecte, que crida a ./new
								buttonsObj.create = function(){$i.hash.follow('./new');} //Funció que s'executarà quan cliquem sobre l'acció.
							}
							
						}
						/**
						 * Si el tipus de pàgina és formulari, és que estem editant un recurs. Aquest recurs pot ser actualitzat o eliminat o creat.
						 * Actualitzat i creat fa el mateix, eliminat elimina el recurs.
						 * En qualsevol dels casos, si el recurs és d'un objecte el qual té el sistema de permissos granulars habilitat, ens podem
						 * trobar que alguns botons estiguin deshabilitats (en funció dels nostres permissos...).
						 */
						if(pageConfig.type=="FORM"){
							//Si s'ha assignat una funció a la propietat de la configuració remove, executem aquesta funció.
							if(typeof(pageConfig.remove)==='function'){
								buttonsObj.ondelete = pageConfig.remove;
							}
							else{
								if(pageConfig.api!=null){  
									/**
									 * Si hem assignat a la configuració de la pàgina la propietat api, podem intentar executar la nostra funció per a eliminar el recurs.
									 * El recurs serà eliminat fent una crida al WEBSERVICE en mètode DELETE a la URL especificada, concatenant el pageConfig.resource,
									 * que ha de ser el id del recurs.
									 * DELETE /users/phidalgo per exemple.
									 *
									 */
									buttonsObj.ondelete = function(response){
										$i.modal({
											show : true,
											autoClose : true,
											title: $i.i18n('md_title_confirm'),
											description : $i.i18n('md_desc_confirm_delete',{resource : pageConfig.resource}),
											width: '500px',
											resizable : false,
											success : function(){
												$i.promise._DELETE({
													restURL: pageConfig.api+pageConfig.resource,
													resource : pageConfig.resource,
													always : response
												}).done(function(){
													if(typeof(pageConfig.afterDelete)==='function'){
														pageConfig.afterDelete();
													}
												});
											}
										});
									}
								}
								else{
									//Si no hem asignat cap funció a remove i tampoc hem configurat la propietat api... no podem borrar el recurs... pq no sabem com ;)
									buttonsObj.ondelete = function(response){ console.log("No delete function attached on this page");response();}
								}
							}
							//El mateixos comentaris que per el tema de "REMOVE".
							if(typeof(pageConfig.submit)==='function'){
								buttonsObj.submit = pageConfig.submit;
							}
							else{
								if(pageConfig.api!=null){
									buttonsObj.submit = function(values,response){
										if(typeof(pageConfig.beforeSubmit)==='function'){
											var rsp = pageConfig.beforeSubmit(values);
											if(typeof(rsp)!=='undefined'){
												values = rsp;
											}
										}
										
										var method = page.newElement? 'POST':'PUT';
										var url = page.newElement? (pageConfig.api.slice(0, -1)):pageConfig.api+pageConfig.resource;
										$i.promise._request({
											method : method,
											restURL : url,
											resource : pageConfig.resource,
											data : values,
											always : response
										})
										.done(function(rsp){
											if(page.newElement){
												pageConfig.resource = rsp.data.id;
												$i.hash._updateLast(rsp.data.id,false);
												$i.breadcrumb._set(rsp.data.id); // Set tpid on aria bar.
												page.buttons.showDelete();
											}
											if(typeof(pageConfig.afterSubmit)==='function'){
												pageConfig.afterSubmit(rsp,values);
											}
											page.newElement = false;
										});
									}
								}
								else{
									buttonsObj.submit = function(values,response){ console.log("No submit function attached on this page");response();}
								}
							}
						}
						
						page.buttons.create(buttonsObj); //Create page buttons.
					}
					
					$i('select').select(); //Això va aqui perque hem creat o hem pogut crear els selects del tema de permisos, tags... etc
					page.$.fadeTo(300,1); //Make fade out
				}
			}
			pagesArray.push(page); //Add page to pageArray.
		} //end page._add();

		/**
		 * Update page cache management, if there is more pages than allowed pages, removes the oldest page (the first one on the pagesArray).
		 */
		this._update = function(){
			if($i._get('pageCache')!=0 && pagesArray.length>=$i._get('pageCache')){
				delete window[pagesArray[0].pageHash];
				var content = $i.UI.layout_content.find('div[data-pageHash="'+pagesArray[0].pageHash+'"]');
				var buttons = $('#page-specific-buttons').find('div[data-bthash="'+pagesArray[0].pageHash+'"]');
				content.remove();
				buttons.remove();
				pagesArray.splice(0,1);
			}
		}
		
		/**
		 * Return all pagesArray
		 */
		this._getAll = function(){
			return pagesArray;
		}
	
		this.find = new find();
		
		this.buttons = function(config){
			return this.find.visible().buttons(config);
		}
	}
	
	idcp.pages = new idcp_pages_management();
	//console.log("class idcp.pages ready to be used.");
})();
