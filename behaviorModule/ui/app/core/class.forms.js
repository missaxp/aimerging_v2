/**
 * This class add usefull methods to $jQuery objects to validate forms and adds nice UI tooltips for advice to the user that there is something in the form that needs their attention.
 * 
 * @author phidalgo
 * @created ??
 * @modified 20161801
 */

(function(){
	var addIconToInputText = function($obj,isSuccess){
		var $d = $obj.closest('span');
		$d.find('i').remove();
		if(isSuccess){
			var $i = '<i class="ace-icon fa fa-check-circle"></i>';
		}
		else{
			var $i = '<i class="ace-icon fa fa-times-circle"></i>';
		}
		
		$d.append($i);
	}
	
	/**
	 * When track Changes is enabled on a jquery element, it adds onkeyup,onchange,onclick,...events to track each
	 * input. Once the validation issue has been solved, this function show it to the user.
	 */
	var validateOnChange = function(event){
		var val = $(this).val();
		//console.log("->"+val+"<-");
		var fg = $(this).closest('.form-group');
		if(val!=""){
			fg.removeClass('has-error');
			fg.find('.help-block').remove();
			fg.addClass('has-success');
			addIconToInputText($(this), true);
		}
		else{
			fg.removeClass('has-success');
			if(!fg.hasClass('has-error')){
				fg.addClass('has-error');
				fg.append(event.data.helpBlock);
			}
			addIconToInputText($(this), false);
		}
	}
	
	/**
	 * Validation form function.
	 */
	var iformvalidate = function(){
		var isValid = true;
		var currentPage = $(this); //Current jQuery object
		var inputs = currentPage.find(':input');
		inputs.each(function(){
			var el = $(this);
			if(!(typeof(el.attr('data-track'))!=='undefined' && el.attr('data-track')==='false')){
				//Validar aquest contingut.
				var name = el.data('name') || el.attr('name');
				var type = this.type || this.tagName.toLowerCase();
				var value = "";
				var required = (typeof(el.attr('required'))!=='undefined'? true:false);
				var uniqueKeyValid = (typeof(el.attr('data-uniqueKeyValid'))!=='undefined')? (el.attr('data-uniqueKeyValid')=="true"? true:false):true;
				var varTypeSuccess = (typeof(el.attr('data-varTypeSuccess'))!=='undefined')? (el.attr('data-varTypeSuccess')=="true"? true:false):true
				type = (type=='select-one' || type=='select-multiple'? 'select':type);
				switch(type){
					case 'text':
						case 'hidden':
							value = el.val().trim();
							break;
					case 'select':
							value = (el.val()==null? '':el.val());
						break;
					case 'button':
						break;
					case 'checkbox':
						value = el.is(':checked');
						break;
				}
				
				if((typeof(name)!=='undefined' && name!="") || !uniqueKeyValid || !varTypeSuccess){
					if(required || !uniqueKeyValid){
						var text = $i.i18n('forms:fieldRequired');
						if(!varTypeSuccess){
							text = $i.i18n('forms:fieldVarTypeWrong');
						}
						var helpBlock = '<div data-type="form-error-field" data-e-name="'+name+'" class="help-block">'+$i.i18n('forms:'+name,{ defaultValue: text})+'</div>';
						var formgroup = $(this).closest('.form-group');
						if(value=="" || typeof(value)==='undefined' || !uniqueKeyValid || !varTypeSuccess){
							//console.log('value=="" => ' + (value=="") + ', typeof(value)==="undefined" => ' + (typeof(value)==='undefined') + ', !uniqueKeyValid => ' +!uniqueKeyValid + ',typeof(uniqueKeyValid) => ' + typeof(uniqueKeyValid) );
							isValid = false;
							if(!formgroup.hasClass('has-error')){
								formgroup.removeClass('has-success');
								formgroup.addClass('has-error');
								formgroup.append(helpBlock);
								if(type=='select' || type=='checkbox'){
									$(this).on('change',{helpBlock: helpBlock},validateOnChange);
								}
								else{
									addIconToInputText($(this), false);
									$(this).on('keyup',{helpBlock: helpBlock},validateOnChange);
								}
							}
						}
						else{ //Si el camp no és requerit i la clau és vàlida.
							if(formgroup.hasClass('has-error')){
								formgroup.removeClass('has-error');
								formgroup.find('.help-block').remove();
							}
						}
					}
					//console.log("type: " + type + ", name: " + name + ", value: " + value, ", required: " + required + ",uniqueKeyValid: " +uniqueKeyValid);
				}
			}
			else{
				//console.log("No validar aquest contingut:" + el.val());
			}
		});
		if(!isValid){
			var radvice = '<div class="alert alert-block alert-danger" data-id="warn-form-not-valid">'+
			'<button type="button" class="close" data-dismiss="alert">'+
				'<i class="ace-icon fa fa-times"></i>'+
			'</button>'+
			'<i class="ace-icon fa fa-check green"></i>'+
			$i.i18n('forms:form_not_valid')+
			'</div>';
			if(currentPage.find('div[data-id="warn-form-not-valid"]').length==0){
				currentPage.prepend(radvice);
			}
		}
		return isValid;
	};
	
	function beforeUnloadFunction(){
		return $i.i18n('forms:txt_confirm_reload_app');
	}
	
	
	var beforeUnloadEnabled = false; //To track if beforeUnloadEvent is already added to $(document).
	var showFormChangeNotifications = function(setDefaultValueNull){
		var page = $i.pages.find.visible();
		var buttons = $('#page-specific-buttons').find('div[data-bthash="'+page.pageHash+'"]');
		buttons.find('button[data-action=":UNDO_FORM"]').removeClass("no-visible");
		if(!beforeUnloadEnabled && !setDefaultValueNull){
			$(window).on('beforeunload',beforeUnloadFunction);
			beforeUnloadEnabled = true;
		}
	}
	
	var unBindBeforeUnload = function(){
		$(window).off('beforeunload',beforeUnloadFunction);
		beforeUnloadEnabled = false;
	}
	
	/**
	 * Enables track changes for a inputs inside a jquery object.
	 * When a user changes any input data, it shows a "Undo" button on the buttonsform zone.
	 * 
	 * setDefaultValueNull is helpfull to use CLEAN forms. Is an optional param that can accept a boolean. Default false.
	 */
	var iformtrackchanges = function(setDefaultValueNull) {
		setDefaultValueNull = (typeof(setDefaultValueNull)==='boolean'? setDefaultValueNull:false);
		//Track form changes for every type of input.
		$(":input",$(this)).on('change',function(){
			var sendvalue = (typeof($(this).attr('nosend'))!=='undefined'? false:true); //if contains attribute nosend, this file will never be sent. But validation will work
			if(sendvalue){
				showFormChangeNotifications(setDefaultValueNull);
				$(this).attr('data-ischanged',true);
			}
		 });
		 $(":input",$(this)).on('keyup',function() {
				var sendvalue = (typeof($(this).attr('nosend'))!=='undefined'? false:true); //if contains attribute nosend, this file will never be sent. But validation will work
				if(sendvalue){
					showFormChangeNotifications(setDefaultValueNull);
					$(this).attr('data-ischanged',true);
				}
		 });
		 
		//Track form changes also on wysiwyg-editors
		$('.wysiwyg-editor',$(this)).on('change',function(){
			showFormChangeNotifications(setDefaultValueNull);
			$(this).attr('data-ischanged',true);
		});
		
		/**
		 * Track form changes also on a specific widgets.
		 * Eeach plugin has their construction and when it fires this event,
		 * sends the right $ element to set changed.  
		 */
		$(this).on('idcp.widgets.trigger.change',function(e,data){
			//console.log("The widget "+data.name+" has change it's value!");
			showFormChangeNotifications(setDefaultValueNull);
			data.$element.attr('data-ischanged',true);
		});
		
		/**
		 * This listen when a widget is loaded to add it to the
		 * track changes system.
		 */
		$(this).on('idcp.widgets.trackchanges',function(e,data){
			//console.log("Add widget: "+data.name + " to the form tracking system");
			data.$element.attr('data-ischanged',false);
			data.$element.attr("data-default-value",data.$element.val());
		})
		
		 //Adds data-default-value to the input;
		 $(":input",$(this)).each(function(){
			 var type = this.type || this.tagName.toLowerCase();
			 type = (type=='select-one' || type=='select-multiple'? 'select':type);
			 var sendvalue = (typeof($(this).attr('nosend'))!=='undefined'? false:true); //if contains attribute nosend, this file will never be sent. But validation will work
			 var value = "";
			 if(sendvalue){
				 if(!setDefaultValueNull){
					 switch(type){
					 	case "text":
					 	case "hidden":
					 	case "select":
					 		value = $(this).val();
					 		break;
					 	case "checkbox":
					 		value = $(this).is(':checked');
					 		break;
					 	case "radio":
					 		break;
					 	case "textarea":
					 		value = $(this).html();
					 }
					 $(this).attr('data-ischanged',false);
				 }
				 else{
					 switch(type){
						 case "text":
						 case "hidden":
						 case "select":
							if($(this).val()!=null && $(this).val()!=""){
								$(this).attr('data-ischanged',true);
				 			}
							break;
						 case "checkbox":
							 if($(this).is(':checked')){
								 $(this).attr('data-ischanged',true);
							 }
							 break;
					 }
				 }
				 $(this).attr("data-default-value",value);
			 }
		 });
		 
		 //Adds data-default-value to wysiwyg-editors
		 $('.wysiwyg-editor',$(this)).each(function(){
			 value = $(this).html();
			 $(this).attr('data-ischanged',false);
			 $(this).attr("data-default-value",value); 
		 });
	 };
	 
	 /**
	  * Restore the form with the default values.
	  */
	 var iformrestorechanges = function(){
		 $(":input",$(this)).each(function(){
			 if($(this).attr('data-ischanged')=='true'){ //As data-ischanged is a string, it needs to be evaluated as a string not as a boolean.
				 //console.log($(this).attr('name') + ', data is changed? ' + $(this).attr('data-ischanged'));
				 var type = this.type || this.tagName.toLowerCase();
				 //type = (type=='select-one' || type=='select-multiple'? 'select':type);
				 switch(type){
				 	case "text":
				 	case "hidden":
				 	case "select-one":
				 		$(this).val($(this).attr('data-default-value'));
				 		break;
				 	case "select-multiple":
				 		var dv = $(this).attr('data-default-value'); //When a select multiple default value is empty or null or "", data-default-value is not set so we have to check it before.
				 		var mval = [];
				 		if(typeof(dv)!=='undefined'){
				 			mval = $(this).attr('data-default-value').split(',');
				 		}
				 		var options = $(this).find('option');
				 		for(var i=0,j=options.length;i<j;i++){
				 			var option = $(options[i]);
				 			if(option.is(':selected')){
				 				option.prop('selected',false);
				 			}
				 			for(var k=0,l=mval.length;k<l;k++){
				 				if(mval[k]==option.val()){
				 					option.prop('selected',true);
				 				}
				 			}
				 		}
				 		break;
				 	case 'textarea':
				 		$(this).val($(this).attr('data-default-value'));
				 		break;
				 	case "checkbox":
				 		$(this).prop('checked',($(this).attr('data-default-value')=="false"? false:true));
				 		break;
				 	case "radio":
				 		break;
				 }
				 
				//Removes UI alerts on each invalid input
				var formgroup = $(this).closest('.form-group');
				formgroup.removeClass('has-error');
				formgroup.removeClass('has-success');
				formgroup.find('.help-block').remove();
				$(this).off('change',validateOnChange);
				$(this).off('keyup',validateOnChange);
				 
				$(this).trigger('change'); //Trigger change event
				$(this).trigger('chosen:updated'); //Trigger chosen updated event.
				$(this).attr('data-ischanged',false); //Set false to data-ischanged attribute.
			 }
		 });
		 
		//Adds data-default-value to wysiwyg-editors
		 $('.wysiwyg-editor',$(this)).each(function(){
			 if($(this).attr('data-ischanged')=='true'){
				 $(this).html($(this).attr("data-default-value"));
				 $(this).attr('data-ischanged',false);
				 $(this).trigger('trackchanges:updated');
			 }
		 });
		 
		 unBindBeforeUnload();
		 $(this).trigger('idcp.widgets.event.rollback');
	 };
	 
	 /**
	  * Check if form is changed.
	  * WARNING : This function may change in the future.I do not like the way to check if the inputs inside a $jQo has changed.
	  */
	 var iformischanged = function() {
		 var page = $i.pages.find.visible();
		 return !$('#page-specific-buttons').find('div[data-bthash="'+page.pageHash+'"]').find('button[data-action=":UNDO_FORM"]').hasClass('no-visible'); 
	 };
	 
	 /**
	  * When the form is submitted, apply changes to let trackchanges know that the default values are the submitted (and correct) values.
	  */
	 var iformapplychanges = function(){
		 $(":input",$(this)).each(function(){
			 var type = this.type || this.tagName.toLowerCase();
			 type = (type=='select-one' || type=='select-multiple'? 'select':type);
			 switch(type){
			 	case "text":
			 	case "select":
			 	case "hidden":
			 		$(this).attr('data-default-value',$(this).val());
			 		break;
			 	case "checkbox":
			 		$(this).attr('data-default-value',$(this).is(':checked'));
			 		break;
			 	case 'textarea':
			 		$(this).attr('data-default-value',$(this).val());
			 		break;
			 	case "radio":
			 		break;
			 }
			 $(this).attr('data-ischanged',false);
		 });
		 
		//Apply data-default-value to wysiwyg-editors
		 $('.wysiwyg-editor',$(this)).each(function(){
			 $(this).attr("data-default-value",$(this).html());
			 $(this).attr('data-ischanged',false);
		 });
		 
		 var buttons = $('#page-specific-buttons').find('div[data-bthash="'+page.pageHash+'"]');
		 var undoB = buttons.find('button[data-action=":UNDO_FORM"]');
		 if(!undoB.hasClass('no-visible')){
			 undoB.addClass("no-visible");
		 }
		 var warn_form = $(this).find('div[data-id="warn-form-not-valid"]');
		 if(warn_form.length!=0){
			 warn_form.remove();
		 }
		 
		 unBindBeforeUnload();
		 
		 $(this).trigger('idcp.widgets.event.commit');
	 };
	 
	 /**
	  * Set to a input element, it allows to assure that the input value are unique in relation a set of values passed (or checked on a WS).
	  * The function argument could be an array of values or a Webservice request.
	  * A webservice request will be like:
	  * 
	  * Hem afegit a 20160428 un delay de 500 ms en les peticions ajax.
	  * 
	  *  arg.rest/@input_value,
	  *  
	  */
	 var iformuniquekey = function(usedKeys){
		 var delayOnAJAX = 500;
		 var iidA;
		 var _this = $(this);
		 $(this).attr('data-uniqueKeyValid',false);
		 $(this).on('keyup',function(){
			var keyValue = $(this).val();
			if(typeof(usedKeys)==='undefined'){
				//OK -> retornar true pq la clau actual és vàlida al no tenir claus no vàlides per referenciar.
				returnResponse(true,keyValue,$(this));
			}
			if($.type(usedKeys)==='array'){
				console.log(usedKeys);
				if(usedKeys.indexOf(keyValue)!=-1){ //MALAMENT -> Clau actual trobada i no vàlida
					returnResponse(false,keyValue,$(this));
				}
				else{ //OK -> Clau actual no trobada en l'array i per tant vàlida
					returnResponse(true,keyValue,$(this));
				}
			}
			if($.type(usedKeys)==='object'){
				if(keyValue!=""){
					if(iidA!=null){
						clearTimeout(iidA);
					}
					iidA = setTimeout(function(){
						$i.promise._GET(usedKeys.rest+'/'+keyValue)
						.done(function(rsp){
							returnResponse(rsp.data,keyValue,_this);
						});
					},delayOnAJAX);
					
				}
				else{
					//returnResponse(true,"",$(this));
				}
			}
		 });
		 
		 function returnResponse(valid,keyValue,$el){
			 if(!valid){
				var helpBlock = '<div data-type="form-error-field" data-e-name="" class="help-block">'+$i.i18n('forms:duplicated_key')+'</div>';
				var formgroup = $el.closest('.form-group');
				if(!formgroup.hasClass('has-error')){
					formgroup.removeClass('has-success');
					formgroup.addClass('has-error');
					formgroup.append(helpBlock);
				}
				$el.attr('data-uniqueKeyValid',false);
			 }
			 else{
				var fg = $el.closest('.form-group');
				fg.removeClass('has-error');
				fg.find('.help-block').remove();
				fg.addClass('has-success');
				$el.attr('data-uniqueKeyValid',true);
			 }
		 }
	 };

	 
	 /**
	  * Get :input values (and also wysiwyg values) from a inputs inside $jQuery object. 
	  * 
	  * @param $container
	  * @returns object
	  * @author phidalgo
	  * @created 20151126
	  * @modified 20160210
	  */
	 var iformgetvalues = function(onlyChanged){
	 	var formdata = [];
	 	var formdataObj;
	 	var $container = $(this);
	 	var inputs = $container.find(':input');
	 	var radioGroups = []; //Radio buttons are more like select one than checkbox so we have to check if they are grouped by names. 
	 	//And finally return one result with the selected value from the group.
	 	inputs.each(function(){
	 		var el = $(this);
	 		var sendvalue = (typeof(el.attr('nosend'))!=='undefined'? false:true); //if contains attribute nosend, this file will never be sent. But validation will work
	 		if(sendvalue){
	 			formdataObj = {};
		 		var name = el.data('name') || el.attr('name');
		 		if(typeof(name)!=='undefined'){
		 			var type = this.type || this.tagName.toLowerCase();
		 			var value = "";
		 			var required = (typeof(el.attr('required'))!=='undefined'? true:false);
		 			formdataObj.name = name;
		 			if($i._get('debug')){
		 				formdataObj.type = type;
		 				formdataObj.required = required;
		 			}
		 			type = (type=='select-one' || type=='select-multiple'? 'select':type);
		 			switch(type){
		 				case 'hidden':
		 					//console.log(el);
		 				case 'text':
		 				case 'select':
		 					value = el.val();
		 					break;
		 				case 'button':
		 					break;
		 				case 'checkbox':
		 					value = (el.is(':checked')? 'Y':'N');
		 					break;
		 				case 'textarea':
		 					value = el.val();
		 					break;
		 				case 'radio':
		 					var foundR = false;
		 					value = (el.is(':checked')? el.val():null);
		 					for(var i=0,j=radioGroups.length;i<j;i++){
		 						var radio = radioGroups[i];
		 						if(radio.name==name){
		 							if(value!=null){
		 								radio.value = value;
		 							}
		 							foundR = true;
		 							break;
		 						}
		 					}
		 					if(!foundR){
		 						radioGroups.push({
		 							name : name,
		 							value : value,
		 							isChanged : (typeof(el.attr('data-ischanged'))!=='undefined'? (el.attr('data-ischanged')=="false"? false:true):false),
		 							required : required
		 						});
		 					}
		 					break;
		 			}
		 			formdataObj.value = value;
		 			
		 			var valueChanged =  (typeof(el.attr('data-ischanged'))!=='undefined'? (el.attr('data-ischanged')=="false"? false:true):false);
		 			if(typeof(el.attr('send'))!=='undefined'){
		 				valueChanged = true;
		 			}
		 			if(type!='radio' && !onlyChanged || (onlyChanged && valueChanged)){
		 				formdata.push(formdataObj);
		 			}
		 		}
	 		}
	 	});
	 	
	 	for(var i=0,j=radioGroups.length;i<j;i++){
			var radio = radioGroups[i];
	 		if(!onlyChanged || radio.isChanged){
	 			formdataObj = {};
	 			if($i._get('debug')){
	 				formdataObj.type = 'radio';
	 				formdataObj.required = radio.required;
	 			}
	 			formdataObj.name = radio.name;
	 			formdataObj.value = radio.value;
	 			formdata.push(formdataObj);
	 		}
	 	}
	 	
	 	$container.find('.wysiwyg-editor').each(function(){
	 		formdataObj = {};
	 		var el = $(this);
	 		var name = el.data('name') || el.attr('name');
	 		var required = (typeof(el.attr('required'))!=='undefined'? true:false);
	 		formdataObj.name = name;
	 		type = 'wysiwyg';
	 		if($i._get('debug')){
	 			formdataObj.type = type;
	 			formdataObj.required = required;
	 		}
	 		formdataObj.value = el.html();
	 		var valueChanged =  (typeof(el.attr('data-ischanged'))!=='undefined'? (el.attr('data-ischanged')=="false"? false:true):false);
	 		if(!onlyChanged || (onlyChanged && valueChanged)){
	 			formdata.push(formdataObj);
	 		}
	 	});
	 	
	 	var page = $i.pages.find.page($container.closest('div[data-pagehash]').data('pagehash'));
	 	//console.log(page._get('widgets'));
	 	if(page!==false){
	 		$.each(page._get('widgets'),function(){
		 		if(typeof(this.object.getValues)==='function'){
		 			var wval = this.object.getValues();
		 			console.log(wval);
		 			if(wval.changed){
		 				formdata.push({ name : wval.form, value : wval.data});
		 			}
		 		}
		 	});
	 	}
	 	
	 	
	 	/**
	 	 * Transform array of objects to object of name : value
	 	 */
	 	var serialized = {};
	 	for(var i=0,j=formdata.length;i<j;i++){
	 		serialized[formdata[i].name] = formdata[i].value;
	 	}
	 	
	 	return ((formdata.length>0)? serialized:null);
	 };
	 
	 
	 var iformAddIns = function(){
		 //First we may add usefull HTML attributes before or after input elements in order to make a friendly UI and use special input options.
		 
		 //We check if the HTML structure near input element is right to construct alerts and show some information to the user.
		 //Only in input elements, not in all :input elements (select,input,...etc). ONLY INPUT
		 $(this).find('input[type="text"]').each(function(){
			 var $el = $(this);
			 var $parent = $el.parent();
			 if($parent.is('div')){
				 var $span = $('<span class="block input-icon input-icon-right"></span>');
				 $span.html($el);
				 $parent.html($span);
			 }
			 //console.log(el.prop('tagName') + ':::' + el.attr('type') + ':::' + el.attr('name')+':::'+parent.prop('tagName'));
		 });
		 
		
		//Add specific tooltips to input max length
		var inputs = $(this).find(':input[maxlength]');
		inputs.each(function(){
			var c = $(this);
			var mx = c.attr('maxlength');
			c.attr('title',$i.i18n('forms:max_characters_string_advice',{ml: mx}));
			$(this).tooltip().off("mouseover mouseout");
		});
		
		
		//Add variable type limitations to inputs with data-vartype attribute.
		$(this).find(':input[data-vartype]').each(function(){
			var el = $(this);
			var valueType = (typeof(el.attr('data-vartype')!=='undefined'))? (el.attr('data-vartype')):null;
			if(valueType!=null){
				switch(valueType){
					case 'string':
						break;
					case 'integer':
						el.attr('title',$i.i18n('forms:lbl_integer'));
						el.on('keyup',function(e){
							var v = this.value;
							if($.isNumeric(v) === false) {
						        //this.value = this.value.slice(0,-1); //chop off the last char entered
								el.attr('data-varTypeSuccess',false);
								addIconToInputText(el,false);
							}
							else{
								el.attr('data-varTypeSuccess',true);
								addIconToInputText(el,true);
							}
						});
						el.tooltip();
						break;
					case 'alphabets':
						var regx = /^[a-zA-Z(),. ]+$/;
						el.attr('title',$i.i18n('forms:lbl_alphabets'));
						el.on('keyup',function(e){
							var v = this.value;
							if (!regx.test(v)) {
						        //this.value = this.value.slice(0,-1); //chop off the last char entered
								el.attr('data-varTypeSuccess',false);
								addIconToInputText(el,false);
							}
							else{
								el.attr('data-varTypeSuccess',true);
								addIconToInputText(el,true);
							}
						});
						el.tooltip();
						break;
					case 'email':
						el.attr('title',$i.i18n('forms:lbl_email'));
						var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
						el.on('keyup',function(e){
							var v = this.value;
							if (!emailReg.test(v)) {
								el.attr('data-varTypeSuccess',false);
								addIconToInputText(el,false);
							}
							else{
								el.attr('data-varTypeSuccess',true);
								addIconToInputText(el,true);
							}
						});
						el.tooltip();
						break;
				}
			}
		});
	 }
	 
	 var iformistranslatable = function(config){
		 var config = config || {}; 
		 var lvalues = config.labels || null;
		 var $ci = $(this);
		 var inputType = config.type || $ci.prop("tagName").toLowerCase();
		 var required = (typeof($ci.attr('required'))!=='undefined'? true:false);
		 var $parent = $(this).parent();
		 var name = $ci.attr('name');
		 var classS = $ci.attr('class');
		 var mlength = $ci.attr('maxlength');
		 
		 var UNIQUE_ID = $i.generator();
		 
		 var dvalue = $ci.val();
		 if(dvalue==""){
			 dvalue = ((lvalues!=null && typeof(lvalues[$i.i18n.currentLang])!=='undefined')? lvalues[$i.i18n.currentLang]:'');
		 }
		 
		 function renderType(name,length,value,visible){
			 value = (value==null? '':value);
			 switch(inputType){
				 case 'input':
					 return '<input type="text" name="'+name+'" data-id="'+name+'" class="col-xs-12 col-sm-'+(visible? '10':'12')+'" autocomplete="off" maxlength="'+length+'" value="'+value+'" '+(required? 'required':'')+'>';
					 break;
				 case 'textarea':
					 return '<textarea name="'+name+'" data-id="'+name+'" class="col-xs-12 col-sm-'+(visible? '10':'12')+'" autocomplete="off" maxlength="'+length+'">'+value+'</textarea>';
					 break;
			 }
			 
		 }
		 
		 var tcid = name+'_translatable_content_'+UNIQUE_ID;
		 var tbid = 'button_'+UNIQUE_ID;
		 var cbid = 'close_'+UNIQUE_ID;
		 var render = '<span><i class="fa fa-globe fa-2x press col-xs-12 col-sm-2" aria-hidden="true" data-id="'+tbid+'"></i>';
		 render += renderType(name+'_'+$i.i18n.currentLang,mlength,dvalue,true);
		 render += '</span>'+
		 '<div>'+
		 '<div data-id="'+tcid+'" class="translatable_div" data-hidden="true">'+
		 '<div class="translatable_title">'+
		 	'<span class="col-xs-11 col-sm-11 no-padding-right">'+$i.i18n('tlt_translatable_content')+'</span><i class="fa fa-times-circle bigger-160 press col-xs-1 col-sm-1 no-padding-right" aria-hidden="true" data-id="'+cbid+'"></i>'+
		 '</div>'+
		 '<div class="translatable_content">';
		 	$.each($i._get('langs'),function(){
		 		if(this!=$i.i18n.currentLang){
		 			var lv = ((lvalues!=null && typeof(lvalues[this])!=='undefined')? lvalues[this]:'');
		 			render += '<!-- B:TRANSLATABLE_CONTENT_'+this+' -->'+
					'<div class="form-group">'+
						'<label class="control-label col-xs-12 col-sm-2 no-padding-right" for="name">'+$i.i18n('lang_'+this.toUpperCase())+'</label>'+
						'<div class="col-xs-12 col-sm-9">';
		 					render += renderType(name+'_'+this,mlength,lv,false);
		 					render += '</div>'+
					'</div>'+
					'<!-- E:TRANSLATABLE_CONTENT_'+this+' -->';
		 		}
		 	});
		 render +='</div></div></div>';
		 
		 var $render = $(render); 
		 
		 $parent.attr('data-field','translatable');
		 $parent.html($render);
		 
		 $i('#'+cbid,$render).on('click',function(){
			 var $content = $i('#'+tcid,$render);
			 $content.hide('fast');
			 $content.attr('data-hidden',true);
		 });
		 
		 var currentMainVal = null;
		 $i('#'+name+'_'+$i.i18n.currentLang,$render).on('change',function(){
			 console.log("HOLA");
			var val = $(this).val();
			$.each($i._get('langs'),function(){
				$obj = $i('#'+name+'_'+this,$render);
				if($obj.val()=="" || $obj.val()==currentMainVal){
					$obj.val(val);
				}
			});
			currentMainVal = val;
		 });
		 
		 $i('#'+tbid,$render).on('click',function(){
			 
			var $content = $('div[data-id="'+tcid+'"]');
			//console.log($render.find('div[data-id="'+tcid+'"]'));
			if($content.attr('data-hidden')=="true"){
				$content.show('fast');
				$content.attr('data-hidden',false);
			}
			else{
				$content.hide('fast');
				$content.attr('data-hidden',true);
			}
		 });
	 }
	 
	/**
	 * jQuery plugin integration for this new objects:
	 * 
	 * $jQo.validate();
	 * $jQo.trackChanges();
	 * $jQo.restoreChanges();
	 * $jQo.isChanged();
	 * $jQo.uniqueKey();
	 * $jQo.getValues();
	 */
	$.fn.extend({
		validate : iformvalidate,
		trackChanges: iformtrackchanges,
		restoreChanges: iformrestorechanges,
		isChanged: iformischanged,
		applyChanges : iformapplychanges,
		uniqueKey : iformuniquekey,
		getValues : iformgetvalues,
		translatable : iformistranslatable,
		form : iformAddIns
	});
	//console.log("Class forms ready to be called. This class adds $.fn extensions. Not accessible via idcp namespace");
})();