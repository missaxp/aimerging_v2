/**
 * @author phidalgo
 * 
 * Application Exception Handler.
 * 
 * Proxy Error handler, every javascript error goes through this error handling functions.
 */


idcp.showingError = false;
var ExceptionObject = {
	code : null,
	status : null,
	responseText : null,
	restURL : null,
	message : null,
	customMessage : null,
	critical : false,
	set : false,
	source : null
};
function AppException(code,cm,critical){
	ExceptionObject.code = code || null;
	ExceptionObject.message = getIDCPErrorMessage(code)
	ExceptionObject.customMessage = cm || null;
	ExceptionObject.critical = critical || false;
	ExceptionObject.set = true;
	ExceptionObject.source = "AppException";

};
AppException.prototype = new Error();
AppException.prototype.constructor = AppException;


function APIException(jqXHR,critical,url){
	ExceptionObject.status = jqXHR.status;
	ExceptionObject.message = null; 
	ExceptionObject.responseText = jqXHR.responseText;
	ExceptionObject.restURL = url;
	ExceptionObject.critical = critical || false;
	ExceptionObject.set = true;
	ExceptionObject.source = "APIException";

}
APIException.prototype = new Error();
APIException.prototype.constructor = APIException;


window.onerror = function(msg,url,line,column,errorObject){
	if(ExceptionObject.set){
		ExceptionObject.set = false;
		errorHandler({
			code : ExceptionObject.code,
			status : ExceptionObject.status,
			customMessage : ExceptionObject.customMessage,
			message : ExceptionObject.message,
			responseText : ExceptionObject.responseText,
			restURL : ExceptionObject.restURL,
			line : line,
			column : column,
			file : url,
			source : ExceptionObject.source
		});
	}
	else{
		ExceptionObject.set = false;
		var page = $i.pages.find.visible();
		errorHandler({
			code : -1,
			status : null,
			customMessage : null,
			message : msg,
			responseText : null,
			restURL : null,
			url : page.urlHash,
			line : line,
			column : column,
			file : page.ecmafile,
			source : 'javascript'
		});
	}
	ExceptionObject = { //Clean Exception Object
		code : null,
		status : null,
		responseText : null,
		message : null,
		customMessage : null,
		critical : false,
		set : false,
		source : null
	};
	
	if(!$i._get('debug')){
		return true;
	}
};

var errorHandler = function(e){
	var value = '';
	var title = '';
	var type = '';
	var error_description = '';
	switch(e.source){
		case 'javascript': //Errors on javascript code, syntax errors, parse errors,...
			type = "Javascript Error";
			title = "Javascript Error";
			error_description = e.message;
			value = '<strong>'+e.message+'</strong><br />';
			value += '<br /><strong>File: </strong>'+e.file;
			value += '<br /><strong>Line: </strong>'+e.line+((typeof(e.column)!=='undefined')? ':'+e.column:'');
			break;
		case 'AppException': //Errors on IDCP. throws new AppException.
			type = "IDCP Error";
			title = "IDCP Error";
			error_description = e.message;
			value += '<strong>Code: </strong>'+e.code+'</strong><br /><strong>'+e.message+'</strong><br />';
			if(e.customMessage!=''){
				value += '<strong>Custom Message: </strong>'+e.customMessage;
			}
			value += '<br /><strong>File: </strong>'+e.file;
			value += '<br /><strong>Line: </strong>'+e.line+((typeof(e.column)!=='undefined')? ':'+e.column:'');
			break;
		case 'APIException': //Errors on webservice response.
			type = "API";
			title = "Service Error";
			error_description = e.message;
			value += '<strong>HTTP Status: </strong>'+e.status+'</strong><br />';
			var jsonO = null;
			try{
				if(e.status == 0 && e.statusText=='timeout'){
					value += '<strong>API timeout</strong><br />';
				}
				else{
					jsonO = JSON.parse(e.responseText);
					if(jsonO.error_code){
						value += '<strong>Error n: </strong>'+jsonO.error_code+'<br />';
					}
					if(jsonO.message){
						value += '<strong>Message: </strong>'+jsonO.message+'<br />';
					}
					if(jsonO.data.custom_message){
						value += '<strong>Custom Message: </strong>'+jsonO.data.custom_message+'<br />';
					}
				}
				
			}
			catch(c){
				value += '<strong>Response is not a JSON object</strong><br />';
				value += '<strong>responseText: </strong>'+ e.responseText+'<br />';
			}
			

			
			var restURL = null;
			var qs = null;
			if(e.restURL.indexOf('?')!==false){
				var a  = e.restURL.split('?');
				restURL = a[0];
				qs = a[1];
			}
			else{
				restURL = e.restURL;
			}
			value += '<strong>Rest URI: </strong>'+restURL;
			if(qs!=null){
				value += '<br /><strong>Query String: </strong>'+qs;
			}
			break;
	}
	
	if($i._get('debug')){
		$i.gritter({
			type: "fail",
			title: title,
			description: value
		});
	}
	else{
		if($i.showingError) return;
		if ($i.UI.layout_root.find('#login-box').length!=0){ //If login box.
			$("#login-response-msg").removeClass("hidden alert-info").addClass("alert-danger").html("<i class='fa fa-exclamation-triangle bigger-125'></i>" + error_description);
			$('#login').addClass('disabled');
		}
		else if($i.UI.layout_content!==null){ //If app loaded.
			$i.showingError = true;
			$i.hash.follow('error',title,e);
		}
		else{ //if... i don't know.
			$i.cookies._delete("session_token");
			if(!$i._get('debug')){
				location.reload();
			}
			else{
				console.log("I don't know where you are... Deleting token cookie.");
			}
		}
	}
	
	if(e.source!='APIException'){ //We don't want to catch API errors due the API server should be catched by itself
		e.userid = $i.user._get().id;
		e.type = type;
		e.navigator = navigator.sayswho;
		
		//console.log(e);
		//try{
			//Use of $.ajax directly instead of $i.promise._request because we want to be sure that the error is not inside the $i.promise._request method.
			var ajax_options = { 
				async: true,
				timeout: $i._get('apiTimeout'),
				type: _POST_,
				url: $i._get('api') + 'management/errorHandler/register',
				headers: JSON.parse('{"Authorization":"' + $i.cookies._get("session_token") + '"}'),
				data: e
			};
			$.ajax(ajax_options)
			.fail(function(){
				var message = 	"We've tried to report this error through our error handler system but it is something wrong with it.<br />We kindly ask you to" +
				" send an email to <strong>informatica@idisc.es</strong> with this message. It would be helpful to fix this issue<br />"+
				value;
				$i.gritter({
					type: "fail",
					title: "Please report this error",
					description: message,
					sticky: true
				});
			});
		//}
		//catch(err){}
		
	}
};

/**
 * Ha de quedar així:
 * 
 * 2000-4000 Errors d'aplicació
 * 4000-x Errors de widgets
 */
function getIDCPErrorMessage(code){
	switch(code){
		case 0:
			return $i.i18n("throw:throw_0"); 
		case 1:
			return $i.i18n("throw:throw_1");
		case 2:
			return $i.i18n("throw:throw_2");
		case 3:
			return $i.i18n("throw:throw_3");
		case 4:
			return "@LITERAL Error: I've successfully download new data but no layout found to print on it.";
		case 5:
			return "@LITERAL Error reading map.json.";
		case 6:
			return "@LITERAL Error, no route found on app map.json";
		case 7:
			return "@LITERAL Error, Javascript parserError.";
		case 8:
			return "@LITERAL Error, Can not load menu";
		case 9:
			return "@LITERAL Error, $i config object not found";
		case 404:
			return "@LITERAL Error, Page not found";
		case 1000: //code > 1000 -> GRIDS ERROR
			return "@LITERAL GRID: idTable must be set. Example: idTable: 'MY_TABLE_ID";
		case 1001:
			return "@LITERAL GRID: rowId must be set. Example: rowId: {fieldId: 'MY_FIELD_ID',startId: 'START_ID_STRING'}";
		case 1002:
			return "@LITERAL GRID: appendTo var must be set. appendTo has to be a jquery OBJECT";
		case 1003:
			return "@LITERAL GRID: returnFields var must be set. returnFields has to be an ARRAY";
		case 1005:
			return "@LITERAL GRID: config file not found";
		case 1006:
			return "@LITERAL GRID: Row onclick has to be a function!";
		case 1007:
			return "@LITERAL GRID: rowclick has to be an object. Example: rowClick: {enable: BOOLEAN, onClick: function}";
		case 1008:
			return "@LITERAL GRID: appendTo has to be a jquery OBJECT";
		case 1009:
			return "@LITERAL GRID: appendTo div does not exist";
		case 2000: //code > 2000 -> MODAL ERROR
			return "@LITERAL MODAL: bind is not a jQuery object";
		case 2001:
			return "@LITERAL MODAL: You must instanciate success var as a function";
		case 2002:
			return "@LITERAL MODAL: You must instanciate cancel var as a function";
		case 2003:
			return "@LITERAL MODAL: Execution fail when call to your success function.";
		case 2004:
			return "@LITERAL MODAL: Execution fail when call to your cancel function.";
		case 3000: //code > 3000 -> GRITTER ERROR
			return "@LITERAL GRITTER: Type must be defined (success,warning,fail)";
		
		default:
			return "Uncaught Error";
	}
};