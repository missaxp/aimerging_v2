/**
 * Methods to make requests to API.
 * 
 * @author phidalgo
 * @creation undefined
 * @modified 20151124 
 */
(function(){
	var auth_required_gritter_shown = false;
	function idcp_promises(){
		
		/**
		 * Same as getPromise but needs an object with only one required property -> restURL
		 * 
		 * Object argument method declaration.
		 * type -> request method type | GET.
		 * restURL -> request RESTURL.
		 * data -> data (json) | null
		 * auth -> if API authorization is requirede to make this request. | true
		 * async -> if request is asyncron | true
		 * bsend -> function or jQuery object to attach/make some effects before send | predefined function.
		 * isFile -> if the request has to upload a file | null
		 * inProgress -> function to get in progress percent status | null
		 * 
		 * @returns promise
		 * 
		 */
		var getPromiseObj = function(config){
			if(typeof(config.restURL)==='undefined') throw new AppException(null,'restURL must be defined when calling getPromise',true);
			config.method = typeof(config.method)!=='undefined'? config.method:'GET';
			config.data = typeof(config.data)!=='undefined'? config.data:null;
			config.auth = typeof(config.auth)!=='undefined'? config.auth:true;
			config.async = typeof(config.async)!=='undefined'? config.async:true;
			config.bsend = typeof(config.bsend)!=='undefined'? config.bsend:null;
			config.isFile = typeof(config.isFile)!=='undefined'? config.isFile:false;
			config.inProgress = typeof(config.inProgress)!=='undefined'? config.inProgress:null;
			config.always = typeof(config.always)==='function'? config.always:null;
			config.successMessage = typeof(config.successMessage)!=='undefined'? config.successMessage:false;
			config.resource = (typeof(config.resource)!=='undefined'? config.resource:null);
			config.clearTimeout = (typeof(config.clearTimeout)!=='boolean'? true:config.clearTimeout); //Si reinicia el comptador de sessió o no. 
			return promise(config.method,config.restURL,config.data,config.auth,config.async,config.bsend,config.isFile,config.inProgress,config.resource,config.always,config.clearTimeout);
		}
		
		var loadContent = function(load,context){
			load = (typeof(load)==='undefined'? true:load);
			context = (typeof(context)==='undefined'? $i.pages.find.me():context);
			if(load){
				if (context.hasClass('position-relative')) context.addClass('position-relative-aux');
				else context.addClass('position-relative');
				
				// Remove previous loading
				context.find('>.idcp-loading').remove();
				
				// Add new loading
				context.append('<div class="position-absolute idcp-loading"><div class="loading-content"><i class="fa fa-circle-o-notch fa-spin red bigger-300"></i></div></div>');
			}
			else{
				// Remove loading overlay and restore original position
				context.find('>.idcp-loading').remove();
				if (context.hasClass('position-relative-aux')) context.removeClass('position-relative-aux');
				else context.removeClass('position-relative');
			}
		}
		
		function promise(method,url,JSondata,authorization,asyncron,bSend,isFile,progresF,resource,alwaysF,clearTimeout){
			JSondata = (typeof JSondata === "undefined") ? null : JSondata; //Per defecte, null (sense dades).
			authorization = (typeof authorization === "undefined") ? true : authorization; //Per defecte, amb autenticacio.
			asyncron = (typeof asyncron === "undefined") ? true : asyncron; //Per defecte, asincron.
			bSend = (typeof bSend === "undefined") ? null : bSend; //Funcio que s'executarà abans d'enviar la petició (si es vol fer algun fade o efecte o el q sigui)
			isFile = (typeof isFile === "undefined") ? false : isFile; //Determina si es puja un fitxer amb FormData o no
			progresF = (typeof progresF === "function") ? progresF: function(){};
			successMessage = typeof(successMessage)==='undefined'? false:successMessage;
			resource = typeof(resource)==='undefined'? null:resource; //If a resource id is given, shows a gritter when PUT, POST or DELETE method.
			clearTimeout = (typeof(clearTimeout)!=='boolean'? true:clearTimeout); //Si reinicia el comptador de sessió o no. 
			
			$('#api-receive-data').css("opacity","0");
			$('#api-send-data').css("opacity","1");
			fixedDiv(true); //Remove fixed div.
			function funcbeforeSend(xhr) {
				if ((typeof bSend === "function")) {
					bSend(); //Execute the function
				} else if (bSend){
					var container = $(bSend);
					loadContent(true,container);
				}
			}
			
			var headers = {};
			//headers["Content-Type"] = "application/json";
			if(authorization){
				headers["Authorization"] = $i.cookies._get("session_token");
			} 

			var promise;
			var ajax_options = {
				async: asyncron,
				beforeSend: funcbeforeSend,
				dataType : 'json',
				contentType : 'application/json',
				timeout: $i._get('apiTimeout'),
				type: method,
				url: $i._get('api') + url,
				headers: headers,
			};

			if(JSondata!=null){
				//console.log(JSON.stringify(JSondata));
				ajax_options.data = ((method==_GET_)? JSondata:JSON.stringify(JSondata));
			}
			
			promise = $.ajax(ajax_options);
			promise.always(promiseAlways); // Always fire this method after done or fail
			promise.fail(promiseFail); // If response fail fires fail functions in declaration order. Add more fail methods to catch specific errors (httpstatus >=400)
			promise.done(promiseDone); //Fired when success (http status >=200 && <399)
			
			function promiseDone(data,textStatus,jqXHR){
				//$i.intervals.app.apply('token_expires');
				if(clearTimeout){
					$i.intervals.app.apply('session_timeout');
				}
				//Check if response data is not an object (JSON object). If not, we launch an exception because API server ALWAYS has to response a JSON object.
				//Except when a file is sent, in that case, it will response the file on a base64 encoding.
				if($.type(data)!=="object" && $.type(data)!=='undefined'){
					throw new APIException(jqXHR,true,$i._get('api')+url);
				}
				
				if(resource!==null){
					var title = "";
					var message = "";
					var icon = "";
					switch(method){
						case _PUT_:
							title =  $i.i18n('title_resource_updated');
							message = $i.i18n('desc_resource_updated',{resource: resource});
							icon = 'fa-floppy-o';
							break;
						case _POST_:
							title = $i.i18n('title_resource_created');
							message = $i.i18n('desc_resource_created',{resource: resource});
							icon = 'fa fa-plus';
							break;
						case _DELETE_:
							title = $i.i18n('title_resource_deleted');
							message = $i.i18n('desc_resource_deleted',{resource: resource});
							icon = 'fa-trash';
							break;
					}

					if($i._get('notificationType')=='notification'){
						$i.notification.add({title : title,message : message, icon : icon,badge : '', link : $i.hash._get(),flash : '#81a87b',type : 'success'});
					}
					else{
						$i.gritter({type : 'success', title : title, description : message});
					}
				}
			}
			
			
			function promiseFail(jqXHR, textStatus) {
				console.warn("Promise Fail!");
				/**
				 * El fail hauria de ser algo com:
				 * 
				 *  Si l'error és 500 (ISE) -> throw new APIException(500, missatge_ajax);
				 *  
				 *  Si no és 500 s'hauria de poder tractar amb l'acció que creiem més pertinent.
				 *  
				 *  Per recordar:
				 *  
				 *  Status 200-299 -> STATUS OK
				 *  
				 *  Status 300-399 -> Situacions poc especifiques amb URLS (URLS que han canviat, URLS que han de ser més especifiques...etc)
				 *  
				 *  Status 400-499 -> Errors forçats, com recurs no existeix, permisos incorrectes, etc.
				 *  
				 *  Status 500-599 -> Internal Server Errors, errors de servidor (timeout del script php, funció no definida, problemes de sintaxi,...etc).
				 *   
				 *  Status 600-699 -> Ara no ho recordo, però crec que alguns errors 600+ els hem creat.
				 * 
				 */
				
				if(!$i._get('logged')){ //If not logged, we are on login screen and login screen has their own alert types.
					return;
				}
				
				var status = jqXHR.status;

				//TODO -> aquesta condició és de dubtosa funcionalitat.
				if(status==200 && textStatus=='parsererror' && jqXHR.responseText!=""){
					throw new APIException(jqXHR,true,$i._get('api')+url);
				}
				if(textStatus=="timeout"){
					throw new APIException(jqXHR,true,$i._get('api')+url);
				}
				
				//S'ha de tractar millor,... però suposem que és un warning (no es pot borrar el fitxer pq no compleix requisits...etc)
				//Això s'ha de tractar millor PRIMER desde el WEB SERVICE i després, gestionar els errors aqui.
				if(status>=400 && status<500){
					//BadRequest (token not found)
					// Unauthorized access (token found, but not valid or unauthorized access, force reload)
					if(status==401){
						if($i._get('debug')){
							console.warn("PROMISE FAIL: STATUS 400!");
						}
						if($("#content").length!=0){
							$i.intervals.app.clear('token_expires'); //Clear expire timeout.
							$i.intervals.app.clear('session_timeout'); //Clear expire timeout.
							$i.cookies._delete("session_token");
							// Hide modal-login if exists
							// Shows login modal
							$i.require(["app/base/loginmodal.html","app/base/js/loginmodal.js"], $i.UI.layout_modal)
							.done(function(){
								$('#modal-login').modal();
							});
						}
						return;
					}
					if(status==422) return; //Login credentials FAIL.
					
					var title = "Something wrong happens";
					var desc = jqXHR.responseJSON.message;
					if(status==404 && jqXHR.responseJSON.error_code==2292){
						//console.warn("FK violated. Children found");
						var track = jqXHR.responseJSON.data.track;
						//console.warn(track);
						var fields = "";
						for(var i=0,j=track.fk_object.fields.length;i<j;i++){
							fields+= track.fk_object.fields[i]+(i+1<j? ', ':'');
						}
						
						title = $i.i18n('title_error_fk_child');
						desc = $i.i18n('error_fk_child',{object : track.fk_object.name, field: fields});
						
						var elements = "<br />";
						for(var i=0,j=track.elements.length;i<j;i++){
							var el = track.elements[i];
							elements+= "id: "+el.id0+(el.name? ", name: "+el.name:"")+(i+1<j? '<br />':'');
						}
						desc += elements;
					}
					
					switch(method){
						case _PUT_:
							icon = 'fa-floppy-o';
							break;
						case _POST_:
							icon = 'fa fa-plus';
							break;
						case _DELETE_:
							icon = 'fa-trash';
							break;
					}
					
					if($i._get('notificationType')=='notification'){
						$i.notification.add({title : title,message : desc, icon : icon,badge : '', link :null ,flash : '#81a87b',type : 'warning'});
					}
					else{
						$i.gritter({ type:"warning", title: title,description: desc});
					}
				}
				
				if(status==0 || status>=500){
					if(status==503){ //WebService en manteniment. No es pot accedir.
						$i.cookies._delete("session_token");
						if($("#content").length!=0){
							location.reload();
						}
						return;
					}
					throw new APIException(jqXHR,true,url);
				}
			}
			
			function promiseAlways( data, textStatus, jqXHR ){
				$i.intervals.app.clear('API_SUCCESS_CONNECTION');
				$('#api-send-data').css("opacity","1");
				$('#api-receive-data').css("opacity","1");
				$i.intervals.app._add('API_SUCCESS_CONNECTION',function(){
					$('#api-send-data').css("opacity","0");
					$('#api-receive-data').css("opacity","0");
					$i.intervals.app.clear('API_SUCCESS_CONNECTION');
				},1000);
				
				fixedDiv(false); //Remove fixed div.
				$('#api-receive-data').css("opacity","1");
				if ((typeof bSend !== "function") && bSend) {
					var container = $(bSend);
					loadContent(false,container);
				}
				if(typeof(alwaysF)==='function'){
					setTimeout(function(){
						alwaysF(((jqXHR.status>=200 && jqXHR.status<300)? true:false));
					},50);
				}
			}
			
			/**
			 * Creates a full screen transparent DIV to disable all buttons.
			 */
			function fixedDiv(show){
				if ($i.UI.layout_content==null) return;
				if(show){
					var $p = $i.pages.find.me().find('div[data-id="disable_events"]');
					if($p.length==0){
						$p.append('<div data-id="disable_events" style="position:absolute;top:0;left:0;right:0;bottom:0;z-index:5000;background-color:white;opacity:0;"></div>');
					}
				}
				else{
					var $p = $i.pages.find.me();
					if($p.length==0){
						$p = $('div[data-id="disable_events"]').remove();
					}
					else{
						$p.find('div[data-id="disable_events"]').remove();
					}

				}
			}
			
			return promise; //Return promise object ($.ajax)
		}
		
		
		function sequential(promises){
			var _this = this;
			
			var context = $i.pages.find.me();
			loadContent();
			
			this.doneF = function(){
				console.log("ALL REQUEST TO WEBSERVICE FINISHED, but no done function is attached");
			}
			
			this.done = function(f){
				if(typeof(f)==='function'){
					this.doneF = f;
				}
			}
			
			if(typeof(promises)==='object'){
				var index = 0;
				var length = promises.length-1;
				var response = [];
				if(length>0){
					dload = promises[index];
					promise(dload.method,dload.rest,(typeof(dload.data)!=='undefined'? dload.data:null)).done(loaded).fail(onfail).always(onalways);
				}
				
				function onalways(){
					
				}
				
				function onfail(){
					console.log("Failed request to a webservice.:" + dload.rest + ", stop loading anything else.");
				}
				
				function loaded(data){
					data.rest = dload.rest;
					response[index] = data;
					if(length==index){
						loadContent(false);
						_this.doneF(response);
					}
					else{
						index++;
						dload = promises[index];
						promise(dload.method,dload.rest,(typeof(dload.data)!=='undefined'? dload.data:null)).done(loaded).fail(onfail).always(onalways);
					}
				}
			}
			else{
				throw new AppError();
			}
		}
		
		/**
		 * Get a ajax promise from the required type and url
		 * 
		 * USE ONLY WITH CONNECTION TO THE API SERVER.
		 * 
		 * 
		 * @param type
		 * @param url
		 * @param JSondata
		 * @param authorization
		 * @param asyncron
		 * @param bSend if function execute it before send, if container
		 * @returns promise
		 * @throws Exception
		 */
		this._request = function(type, url, JSondata, authorization, asyncron, bSend,isFile,progresF,successMessage,always){
			if(typeof(type)==='object'){
				return getPromiseObj(type);
			}
			return promise(type,url,JSondata,authorization,asyncron,bSend,isFile,progresF,successMessage,always);
		};
		
		/*Alias _request GET*/
		this._GET = function(url, JSondata, authorization, asyncron, bSend,isFile,progresF,successMessage,always){
			if(typeof(url)==='object'){
				url.method = _GET_;
				return getPromiseObj(url);
			}
			else{
				return this._request(_GET_, url, JSondata, authorization, asyncron, bSend,isFile,progresF,successMessage,always)
			}
		}; 
		
		/*Alias _request POST*/
		this._POST = function(url, JSondata, authorization, asyncron, bSend,isFile,progresF,successMessage,always){
			if(typeof(url)==='object'){
				url.method = _POST_;
				return getPromiseObj(url);
			}
			else{
				return this._request(_POST_, url, JSondata, authorization, asyncron, bSend,isFile,progresF,successMessage,always)
			}
		};
		
		/*Alias _request PUT*/
		this._PUT = function(url, JSondata, authorization, asyncron, bSend,isFile,progresF,successMessage,always){
			if(typeof(url)==='object'){
				url.method = _PUT_;
				return getPromiseObj(url);
			}
			else{
				return this._request(_PUT_, url, JSondata, authorization, asyncron, bSend,isFile,progresF,successMessage,always)
			}
		};
		
		/*Alias _request DELETE*/
		this._DELETE = function(url, JSondata, authorization, asyncron, bSend,isFile,progresF,successMessage,always){
			if(typeof(url)==='object'){
				url.method = _DELETE_;
				return getPromiseObj(url);
			}
			else{
				return this._request(_DELETE_, url, JSondata, authorization, asyncron, bSend,isFile,progresF,successMessage,always)
			}
		};
		
		/**
		 * Get a multiple ajax promise and return all when all promises are loaded.
		 */
		this.sequential = function(promises){
			return new sequential(promises);
		}
	}
	
	idcp.promise = new idcp_promises();
	//console.log("class idcp.promise ready to be used.");
})();


var _GET_ = 'GET';
var _POST_ = 'POST';
var _PUT_ = 'PUT';
var _DELETE_ = 'DELETE';