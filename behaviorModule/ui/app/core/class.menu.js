(function(){
	var full_menu_path = "app/json/fullmenu.json"
	var user_menu = null; //Visible menu points for this user.
	
	
	function idcp_main_menu(){
		var _this = this;
		var $menu = $i.UI.mainMenu;
		
		this.load = function(callback){
			$.getJSON(full_menu_path,function(allmenu) {
				user_menu = getUserMenu(allmenu); //Get current user menu using user modules for permissions and allmenu structure
				render(user_menu.menu); // Mount and append menu nav-list to html element
				idcp.hash.process(); //Forçem l'execució de la funció hashchange (/app/core/class.hashManagement.js) per carregar els fitxers que falten segons la ruta donada.
				if(typeof(callback)==='function'){
					callback(); //If fails, no callback is called.
				}
			})
			.fail(function( jqXHR, textStatus ) {
				throw new AppException(8,textStatus,true);
			});
		}
		
		this._get = function(){
			return user_menu;
		}
		
		/**
		 * Bàsicament el que fa aquest mètode és marcar el punt de menú obert.
		 * Hi ha una mica de lògica referent a posicions de hash que no ténen punts de menu però si relatius a ell.
		 * Com és el cas del següent exemple:
		 * 	Entrem a la app amb l'adreça: idcpfe.linux/#crm/contacts
		 * 
		 * S'executa aquesta funció i en la linia on if(ar==hash) - on hash és el hash del navegador i ar és el hfref del anchor en qüestió - s'atura pq ha trobat un punt de menu q coincideix amb
		 * el hash.
		 * 
		 * Ara entrem directament a un registre:
		 * idcpfe.linux/#crm/contacts/7400
		 * 
		 * Aquest hash no té cap punt de menu associat. Per a trobar-lo es busca el punt més proper i és el que es marca.
		 * 
		 * @return boolean
		 */
		this.openMenu = function(){
			var menuNav = $menu.find('li a');
			var hash = window.location.hash.substring(1); //Remove '#'
			$menu.find('li.active').removeClass('active');
			if(hash==""){
				return true;
			}
			var hs = hash.split('/');
			var $anchor = null;
			var anchorRoutes = []
			$.each(menuNav,function(){
				$anchor = $(this);
				var ar = $anchor.attr('href').substring(1); //Anchor href url. Remove '#'
				//console.log(ar);
				var ars = ar.split('/');
				if(ar==hash){
					return false;
				}
				if(ar!=""){
					anchorRoutes.push({ hash: ar,length: ars.length});
				}
				
				$anchor = null;
			});
			var bestAnchor = null; 
			if($anchor == null){
				for(var g=hs.length,h=0;g>h;g--){
					var routeCounter = 0;
					$.each(anchorRoutes,function(){
						if(this.length == g){
							routeCounter++;
							var currentAnchorSplit = this.hash.split('/');
							//console.log("Mirem el següent anchor: " + this.hash);
							var validAnchorLink = "#";
							for(var i=0,j=currentAnchorSplit.length;i<j;i++){
								var a = currentAnchorSplit[i];
								for(var k=0,l=hs.length;k<l;k++){
									var hsp = hs[k];
									if(a==hsp){
										validAnchorLink += hsp+'/';
									}
									else{
										if(validAnchorLink=='#'){
											break;
										}
									}
								}
							}
							if(validAnchorLink!='#'){
								validAnchorLink = validAnchorLink.slice(0, -1);
								//console.log("Found validAnchorLink: "+validAnchorLink+", validAnchorLink.split('/').length: "+validAnchorLink.split('/').length+", this.length: "+this.length);
								if(this.length <= validAnchorLink.split('/').length){
									bestAnchor = validAnchorLink;
									//console.log("Found best anchor: " + bestAnchor);
								}
							}
						}
					});
					//console.log("Hi ha "+routeCounter+" links amb "+g+" subrutes");
				}
			}
			//We have found the nearest anchor.
			if(bestAnchor!=null){
				$anchor = $menu.find('li a[href="'+bestAnchor+'"]');
				//console.log("Best Anchor found: " + bestAnchor);
			}
			
			
			if($anchor!=null){
				//TODO -> no obre el menu (sempre parlant visualment) si té més de 2 nivells (com el cas de gestio/changelog/frontend)
				var $li = $anchor.parent(); 
				var $ul = $li.parent();
				$li.addClass('active');
				$li.parents('.nav-list li').addClass('active open');
				$ul.removeClass('nav-hide').addClass('nav-show').css('display','block');
			}
			return ($anchor!=null)? true:false;
		}
		
		/**
		 * Gets the menu structure of the current user based on user permissions
		 * 
		 * @param fullmenu
		 */
		var getUserMenu = function (fullmenu) {
			var newmenu = new Object;
			if (!fullmenu.menu)
				return newmenu;
			
			//console.log(fullmenu);
			newmenu.menu = new Object;
			// Recorre els elements de menú
			$.each( fullmenu.menu, function(menu_key, menu_val ) {
				//In order to get access to this section, check if the user has access to each required module.
				
				if(!$i.security.menu.checkAccessToModule(menu_val)){
					return true; //Same effect as "continue" in "for" loop.
				}
				// Crea l'element de menú en el menú d'usuari 
				newmenu.menu[menu_key] = new Object;
				
				// Assigna els atributs del menu en el menú de l'usuari
				$.each( menu_val, function( menuelement_key, menuelement_val ) {
					// Excepte els atributs menu, actions i modules
					if (menuelement_key!="menu" && menuelement_key!="actions" && menuelement_key!="modules") {
						newmenu.menu[menu_key][menuelement_key] = menuelement_val;
					}
					newmenu.menu[menu_key]["name"] = menu_key;
					newmenu.menu[menu_key]["title"] = $i.i18n("menu:" + menu_key);
				});
				
				// Assigna el submenú de fullmenu si en té al nou menú d'usuari
				if (menu_val['menu']) {
					// Recursive function to get child menus
					var usersubmenu = getUserMenu(menu_val);
					if (usersubmenu['menu']) {
						newmenu.menu[menu_key]['menu'] = usersubmenu['menu'];
					}
				}
				
			});
			
			return newmenu;
		};
		
		 /**
		 * Return the HTML code for the nav-list menu
		 * 
		 * @param navlist
		 * @return string
		 */
		var mountNavList = function(user_menu, route) {
			var html = "";
			route = (typeof route === "undefined") ? "" : route + "/"; // Default route value
			$.each(user_menu, function() { // Creates nav-bar menu
				if(route==""){
					html += '<li><a href="#" ';
				}
				else{
					html += "<li><a href=\"#" + route + this.route + "\" data-menuname=\"" + this.name + "\" data-menuroute=\"" + route + this.route + "\"";  //S'ha eliminat això 30/04/2014 data-menufile=\"" + this.file + "\""
				}
				if (this.menu) {
					html += " class=\"dropdown-toggle\"";
				}
				html += ">";
				
				if (this.icon && this.icon!="") {
					html += "<i class=\"menu-icon " + this.icon + "\"></i>";
				} else {
					html += "<i class=\"menu-icon fa fa-caret-right\"></i>";
				}
				html += "<span class=\"menu-text\">" + $i.i18n('menu:'+route+this.route) + "</span>";
				if (this.menu) {
					html += "<b class=\"arrow fa fa-angle-down\"></b>";
				}
				html += "</a>";
				
				if (this.menu) {
					html += "<ul class=\"submenu nav-hide\" style=\"display: none;\">";
					html += mountNavList(this.menu, route + this.route);
					html += "</ul>";
				}
				html += "</li>";
			});
			return html;
		};
		
		var render = function(user_menu){
			var render = mountNavList(user_menu);
			$menu.html(render); //Append it to menu html location.
			menuBehaviour();
		}
		
		
		var menuBehaviour = function(){
			var sidebar_count = 0;
			
			var defaults = { //Abans era : $.fn.ace_sidebar.defaults
				'duration': 300
		    };
			
			
			var self = this;
			this.$sidebar = $('#sidebar',$i.UI.rootObject);
			this.$sidebar.attr('data-sidebar', 'true');
			var sidebar = this.$sidebar[0];
			var settings = {};
			//if( !this.$sidebar.attr('id') ) this.$sidebar.attr( 'id' , 'id-sidebar-'+(++sidebar_count) )

			//get a list of 'data-*' attributes that override 'defaults' and 'settings'
			var attrib_values = ace.helper.getAttrSettings(sidebar, defaults, 'sidebar-');
			this.settings = $.extend({}, defaults, settings, attrib_values);


			//some vars
			this.minimized = false;//will be initiated later
			this.collapsible = false;//...
			this.horizontal = false;//...
			this.mobile_view = false;//


			this.vars = function() {
				return {'minimized': this.minimized, 'collapsible': this.collapsible, 'horizontal': this.horizontal, 'mobile_view': this.mobile_view}
			}
			this.get = function(name) {
				if(this.hasOwnProperty(name)) return this[name];
			}
			this.set = function(name, value) {
				if(this.hasOwnProperty(name)) this[name] = value;
			}
			

			this.ref = function() {
				//return a reference to self
				return this;
			}

			var toggleIcon = function(minimized) {
				var icon = $(this).find(ace.vars['.icon']), icon1, icon2;
				if(icon.length > 0) {
					icon1 = icon.attr('data-icon1');//the icon for expanded state
					icon2 = icon.attr('data-icon2');//the icon for collapsed state

					if(minimized !== undefined) {
						if(minimized) icon.removeClass(icon1).addClass(icon2);
						else icon.removeClass(icon2).addClass(icon1);
					}
					else {
						icon.toggleClass(icon1).toggleClass(icon2);
					}
				}
			}		
			
			var findToggleBtn = function() {
				var toggle_btn = self.$sidebar.find('.sidebar-collapse');
				if(toggle_btn.length == 0) toggle_btn = $('.sidebar-collapse[data-target="#'+(self.$sidebar.attr('id')||'')+'"]');
				if(toggle_btn.length != 0) toggle_btn = toggle_btn[0];
				else toggle_btn = null;
				
				return toggle_btn;
			}
			
			//collapse/expand button
			this.toggleMenu = function(toggle_btn, save) {
				if(this.collapsible) return;

				//var minimized = this.$sidebar.hasClass('menu-min');
				this.minimized = !this.minimized;
				
				try {
					//toggle_btn can also be a param to indicate saving to cookie or not?! if toggle_btn === false, it won't be saved
					ace.settings.sidebar_collapsed(sidebar, this.minimized, !(toggle_btn === false || save === false));//@ ace-extra.js
				} catch(e) {
					if(this.minimized)
						this.$sidebar.addClass('menu-min');
					else this.$sidebar.removeClass('menu-min');
				}
		
				if( !toggle_btn ) {
					toggle_btn = findToggleBtn();
				}
				if(toggle_btn) {
					toggleIcon.call(toggle_btn, this.minimized);
				}

				//force redraw for ie8
				if(ace.vars['old_ie']) ace.helper.redraw(sidebar);
			}
			this.collapse = function(toggle_btn, save) {
				if(this.collapsible) return;
				this.minimized = false;
				
				this.toggleMenu(toggle_btn, save);
			}
			this.expand = function(toggle_btn, save) {
				if(this.collapsible) return;
				this.minimized = true;
				
				this.toggleMenu(toggle_btn, save);
			}
			

			
			//collapse/expand in 2nd mobile style
			this.toggleResponsive = function(toggle_btn) {
				if(!this.mobile_view || this.mobile_style != 3) return;
			
				if( this.$sidebar.hasClass('menu-min') ) {
					//remove menu-min because it interferes with responsive-max
					this.$sidebar.removeClass('menu-min');
					var btn = findToggleBtn();
					if(btn) toggleIcon.call(btn);
				}


				this.minimized = !this.$sidebar.hasClass('responsive-min');
				this.$sidebar.toggleClass('responsive-min responsive-max');


				if( !toggle_btn ) {
					toggle_btn = this.$sidebar.find('.sidebar-expand');
					if(toggle_btn.length == 0) toggle_btn = $('.sidebar-expand[data-target="#'+(this.$sidebar.attr('id')||'')+'"]');
					if(toggle_btn.length != 0) toggle_btn = toggle_btn[0];
					else toggle_btn = null;
				}
				
				if(toggle_btn) {
					var icon = $(toggle_btn).find(ace.vars['.icon']), icon1, icon2;
					if(icon.length > 0) {
						icon1 = icon.attr('data-icon1');//the icon for expanded state
						icon2 = icon.attr('data-icon2');//the icon for collapsed state

						icon.toggleClass(icon1).toggleClass(icon2);
					}
				}

				$(document).triggerHandler('settings.ace', ['sidebar_collapsed' , this.minimized]);
			}
			
			//some helper functions
			this.is_collapsible = function() {
				var toggle
				return (this.$sidebar.hasClass('navbar-collapse'))
				&& ((toggle = $('.navbar-toggle[data-target="#'+(this.$sidebar.attr('id')||'')+'"]').get(0)) != null)
				&&  toggle.scrollHeight > 0
				//sidebar is collapsible and collapse button is visible?
			}
			this.is_mobile_view = function() {
				var toggle
				return ((toggle = $('.menu-toggler[data-target="#'+(this.$sidebar.attr('id')||'')+'"]').get(0)) != null)
				&&  toggle.scrollHeight > 0
			}


			//toggling submenu
			this.$sidebar.on(ace.click_event+'.ace.submenu', '.nav-list', function (ev) {
				var nav_list = this;

				//check to see if we have clicked on an element which is inside a .dropdown-toggle element?!
				//if so, it means we should toggle a submenu
				var link_element = $(ev.target).closest('a');
				if(!link_element || link_element.length == 0) return;//return if not clicked inside a link element

				var minimized  = self.minimized && !self.collapsible;
				//if .sidebar is .navbar-collapse and in small device mode, then let minimized be uneffective
		
				if( !link_element.hasClass('dropdown-toggle') ) {//it doesn't have a submenu return
					//just one thing before we return
					//if sidebar is collapsed(minimized) and we click on a first level menu item
					//and the click is on the icon, not on the menu text then let's cancel event and cancel navigation
					//Good for touch devices, that when the icon is tapped to see the menu text, navigation is cancelled
					//navigation is only done when menu text is tapped

					if( ace.click_event == 'tap'
						&&
						minimized
						&&
						link_element.get(0).parentNode.parentNode == nav_list )//only level-1 links
					{
						var text = link_element.find('.menu-text').get(0);
						if( text != null && ev.target != text && !$.contains(text , ev.target) ) {//not clicking on the text or its children
							ev.preventDefault();
							return false;
						}
					}


					//ios safari only has a bit of a problem not navigating to link address when scrolling down
					//specify data-link attribute to ignore this
					if(ace.vars['ios_safari'] && link_element.attr('data-link') !== 'false') {
						//only ios safari has a bit of a problem not navigating to link address when scrolling down
						//please see issues section in documentation
						document.location = link_element.attr('href');
						ev.preventDefault();
						return false;
					}

					return;
				}
				
				ev.preventDefault();
				
				


				var sub = link_element.siblings('.submenu').get(0);
				if(!sub) return false;
				var $sub = $(sub);

				var height_change = 0;//the amount of height change in .nav-list

				var parent_ul = sub.parentNode.parentNode;
				if
				(
					( minimized && parent_ul == nav_list )
					 || 
					( ( $sub.parent().hasClass('hover') && $sub.css('position') == 'absolute' ) && !self.collapsible )
				)
				{
					return false;
				}

				
				var sub_hidden = (sub.scrollHeight == 0)

				//if not open and visible, let's open it and make it visible
				if( sub_hidden ) {//being shown now
				  $(parent_ul).find('> .open > .submenu').each(function() {
					//close all other open submenus except for the active one
					if(this != sub && !$(this.parentNode).hasClass('active')) {
						height_change -= this.scrollHeight;
						self.hide(this, self.settings.duration, false);
					}
				  })
				}

				if( sub_hidden ) {//being shown now
					self.show(sub, self.settings.duration);
					//if a submenu is being shown and another one previously started to hide, then we may need to update/hide scrollbars
					//but if no previous submenu is being hidden, then no need to check if we need to hide the scrollbars in advance
					if(height_change != 0) height_change += sub.scrollHeight;//we need new updated 'scrollHeight' here
				} else {
					self.hide(sub, self.settings.duration);
					height_change -= sub.scrollHeight;
					//== -1 means submenu is being hidden
				}

				//hide scrollbars if content is going to be small enough that scrollbars is not needed anymore
				//do this almost before submenu hiding begins
				//but when minimized submenu's toggle should have no effect
				if (height_change != 0) {
					if(self.$sidebar.attr('data-sidebar-scroll') == 'true' && !self.minimized) 
						self.$sidebar.ace_sidebar_scroll('prehide', height_change)
				}

				return false;
			})

			var submenu_working = false;
			
			this.show = function(sub, $duration, shouldWait) {
				//'shouldWait' indicates whether to wait for previous transition (submenu toggle) to be complete or not?
				shouldWait = (shouldWait !== false);
				if(shouldWait && submenu_working) return false;
						
				var $sub = $(sub);
				var event;
				$sub.trigger(event = $.Event('show.ace.submenu'))
				if (event.isDefaultPrevented()) {
					return false;
				}
				
				if(shouldWait) submenu_working = true;


				$duration = $duration || this.settings.duration;
				
				$sub.css({
					height: 0,
					overflow: 'hidden',
					display: 'block'
				})
				.removeClass('nav-hide').addClass('nav-show')//only for window < @grid-float-breakpoint and .navbar-collapse.menu-min
				.parent().addClass('open');
				
				sub.scrollTop = 0;//this is for submenu_hover when sidebar is minimized and a submenu is scrollTop'ed using scrollbars ...

				if( $duration > 0 ) {
				  $sub.css({height: sub.scrollHeight,
					'transition-property': 'height',
					'transition-duration': ($duration/1000)+'s'})
				}

				var complete = function(ev, trigger) {
					ev && ev.stopPropagation();
					$sub
					.css({'transition-property': '', 'transition-duration': '', overflow:'', height: ''})
					//if(ace.vars['webkit']) ace.helper.redraw(sub);//little Chrome issue, force redraw ;)

					if(trigger !== false) $sub.trigger($.Event('shown.ace.submenu'))
					
					if(shouldWait) submenu_working = false;
				}
				
				if( $duration > 0 && !!$.support.transition.end ) {
				  $sub.one($.support.transition.end, complete);
				}
				else complete();
				
				//there is sometimes a glitch, so maybe retry
				if(ace.vars['android']) {
					setTimeout(function() {
						complete(null, false);
						ace.helper.redraw(sub);
					}, $duration + 20);
				}

				return true;
			 }
			 
			 
			 this.hide = function(sub, $duration, shouldWait) {
				//'shouldWait' indicates whether to wait for previous transition (submenu toggle) to be complete or not?
				shouldWait = (shouldWait !== false);
				if(shouldWait && submenu_working) return false;
			 
				
				var $sub = $(sub);
				var event;
				$sub.trigger(event = $.Event('hide.ace.submenu'))
				if (event.isDefaultPrevented()) {
					return false;
				}
				
				if(shouldWait) submenu_working = true;
				

				$duration = $duration || this.settings.duration;
				
				$sub.css({
					height: sub.scrollHeight,
					overflow: 'hidden',
					display: 'block'
				})
				.parent().removeClass('open');

				sub.offsetHeight;
				//forces the "sub" to re-consider the new 'height' before transition

				if( $duration > 0 ) {
				  $sub.css({'height': 0,
					'transition-property': 'height',
					'transition-duration': ($duration/1000)+'s'});
				}


				var complete = function(ev, trigger) {
					ev && ev.stopPropagation();
					$sub
					.css({display: 'none', overflow:'', height: '', 'transition-property': '', 'transition-duration': ''})
					.removeClass('nav-show').addClass('nav-hide')//only for window < @grid-float-breakpoint and .navbar-collapse.menu-min

					if(trigger !== false) $sub.trigger($.Event('hidden.ace.submenu'))
					
					if(shouldWait) submenu_working = false;
				}

				if( $duration > 0 && !!$.support.transition.end ) {
				   $sub.one($.support.transition.end, complete);
				}
				else complete();


				//there is sometimes a glitch, so maybe retry
				if(ace.vars['android']) {
					setTimeout(function() {
						complete(null, false);
						ace.helper.redraw(sub);
					}, $duration + 20);
				}

				return true;
			 }

			 this.toggle = function(sub, $duration) {
				$duration = $duration || self.settings.duration;
			 
				if( sub.scrollHeight == 0 ) {//if an element is hidden scrollHeight becomes 0
					if( this.show(sub, $duration) ) return 1;
				} else {
					if( this.hide(sub, $duration) ) return -1;
				}
				return 0;
			 }


			//sidebar vars
			var minimized_menu_class  = 'menu-min';
			var responsive_min_class  = 'responsive-min';
			var horizontal_menu_class = 'h-sidebar';

			var sidebar_mobile_style = function() {
				//differnet mobile menu styles
				this.mobile_style = 1;//default responsive mode with toggle button inside navbar
				if(this.$sidebar.hasClass('responsive') && !$('.menu-toggler[data-target="#'+this.$sidebar.attr('id')+'"]').hasClass('navbar-toggle')) this.mobile_style = 2;//toggle button behind sidebar
				 else if(this.$sidebar.hasClass(responsive_min_class)) this.mobile_style = 3;//minimized menu
				  else if(this.$sidebar.hasClass('navbar-collapse')) this.mobile_style = 4;//collapsible (bootstrap style)
			}
			sidebar_mobile_style.call(self);
			  
			function update_vars() {
				this.mobile_view = this.mobile_style < 4 && this.is_mobile_view();
				this.collapsible = !this.mobile_view && this.is_collapsible();

				this.minimized = 
				(!this.collapsible && this.$sidebar.hasClass(minimized_menu_class))
				 ||
				(this.mobile_style == 3 && this.mobile_view && this.$sidebar.hasClass(responsive_min_class))

				this.horizontal = !(this.mobile_view || this.collapsible) && this.$sidebar.hasClass(horizontal_menu_class)
			}

			//update some basic variables
			$(window).on('resize.sidebar.vars' , function(){
				update_vars.call(self);
			}).triggerHandler('resize.sidebar.vars')
		

			//sidebar events
			//menu-toggler
			$(document)
			.on(ace.click_event+'.ace.menu', '.menu-toggler', function(e){
				var btn = $(this);
				var sidebar = $(btn.attr('data-target'));
				if(sidebar.length == 0) return;
				
				e.preventDefault();
						
				sidebar.toggleClass('display');
				btn.toggleClass('display');
				
				var click_event = ace.click_event+'.ace.autohide';
				var auto_hide = sidebar.attr('data-auto-hide') === 'true';

				if( btn.hasClass('display') ) {
					//hide menu if clicked outside of it!
					if(auto_hide) {
						$(document).on(click_event, function(ev) {
							if( sidebar.get(0) == ev.target || $.contains(sidebar.get(0), ev.target) ) {
								ev.stopPropagation();
								return;
							}

							sidebar.removeClass('display');
							btn.removeClass('display');
							$(document).off(click_event);
						})
					}

					if(sidebar.attr('data-sidebar-scroll') == 'true') sidebar.ace_sidebar_scroll('reset');
				}
				else {
					if(auto_hide) $(document).off(click_event);
				}

				return false;
			})
			//sidebar collapse/expand button
			.on(ace.click_event+'.ace.menu', '.sidebar-collapse', function(e){
				var target = $(this).attr('data-target'), $sidebar = null;
				if(target) $sidebar = $(target);
				if($sidebar == null || $sidebar.length == 0) $sidebar = $(this).closest('.sidebar');
				if($sidebar.length == 0) return;

				e.preventDefault();
				$sidebar.ace_sidebar('toggleMenu', this);
			})
			//this button is used in `mobile_style = 3` responsive menu style to expand minimized sidebar
			.on(ace.click_event+'.ace.menu', '.sidebar-expand', function(e){
				var target = $(this).attr('data-target'), $sidebar = null;
				if(target) $sidebar = $(target);
				if($sidebar == null || $sidebar.length == 0) $sidebar = $(this).closest('.sidebar');
				if($sidebar.length == 0) return;	
			
				var btn = this;
				e.preventDefault();
				$sidebar.ace_sidebar('toggleResponsive', this);
				
				var click_event = ace.click_event+'.ace.autohide';
				if($sidebar.attr('data-auto-hide') === 'true') {
					if( $sidebar.hasClass('responsive-max') ) {
						$(document).on(click_event, function(ev) {
							if( $sidebar.get(0) == ev.target || $.contains($sidebar.get(0), ev.target) ) {
								ev.stopPropagation();
								return;
							}

							$sidebar.ace_sidebar('toggleResponsive', btn);
							$(document).off(click_event);
						})
					}
					else {
						$(document).off(click_event);
					}
				}
			})

			
			$.fn.ace_sidebar = function (option, value) {
				var method_call;

				var $set = this.each(function () {
					var $this = $(this);
					var data = $this.data('ace_sidebar');
					var options = typeof option === 'object' && option;

					if (!data) $this.data('ace_sidebar', (data = new Sidebar(this, options)));
					if (typeof option === 'string' && typeof data[option] === 'function') {
						if(value instanceof Array) method_call = data[option].apply(data, value);
						else method_call = data[option](value);
					}
				});

				return (method_call === undefined) ? $set : method_call;
			};
			
			
			$.fn.ace_sidebar.defaults = {
				'duration': 300
		    }

		}
	}
	
	idcp.menu = new idcp_main_menu();
	//console.log("class idcp.menu ready to be used.");
})();


$i.isFromMenu = function(hash){
	var menuNav = $("#nav-list li a",$i.UI.rootObject);
	//var hash = window.location.hash;
	if(hash.substr(hash.length-1,1)=='/'){
		hash = hash.substr(0,hash.length-1);
	}
	var hashm = "";
	var ret = false;
	menuNav.each(function(k,v){
		hashm = $(v).attr('href');
		if(hashm == hash){
			ret = true;
			return true;
		}
	});
	
	return ret;
}



/**
 * Shows login modal
 */
function showLoginModal(){
	$i.intervals.app.clear('token_expires'); //Clear expire timeout.
	$i.intervals.app.clear('session_timeout'); //Clear expire timeout.
	event.preventDefault();
	event.stopPropagation();
	// Hide modal-login if exists
	$('#modal-login').modal('hide');
	
	$i.require(["app/base/loginmodal.html","app/base/js/loginmodal.js"],$i.UI.layout_modal)
	.done(function(){ // Shows login modal
		$('#modal-login').modal();
	});
}