(function(){
	var idcp_history_management = function(){
		var historyHash = [];
		var position = 0;
		
		this._add = function(){
			if(window.location.hash!='#error'){
				if(historyHash.length==0 || historyHash[this.position].url!=window.location.hash){
					historyHash.push({
						url :window.location.hash, //Saves the user's push history
						time: new Date().toString()
					});
					this.position = historyHash.length -1;
					if($i._get('debug')){
						//console.log("Add to history position: " + this.position + " : " + window.location.hash);
					}
				}
				
			}
		};
		
		this.back = function(back){
			var b = back || 1;
			var backUrl = '';
			this.position = this.position - b;
			//this.position = (this.position<0? 0:this.position);
			if(typeof(historyHash[this.position])!=='undefined'){
				backUrl = historyHash[this.position].url;
			}
			else{
				var u = window.location.hash.split('/');
				var url = '';
				for(var i=0;i<u.length-1;i++){
					url += u[i]+'/';
				}
				backUrl = url.substr(0,url.length-1);
			}
			historyHash.splice(this.position+1,1); //Delete this position.
			this.position = (this.position<0? 0:this.position);
			if($i._get('debug')){
				//console.log("Go back history " + b + " times. Position of history: " + this.position + " => " + backUrl);
			}
			$i.hash.follow(backUrl);
		}
		
		this.forward = function(){
			var fw = forward || 1;
			$i.hash.follow(historyHash[historyHash.length - fw]);
		}
	}
	
	idcp.history = new idcp_history_management();
	//console.log("class idcp.history ready to be used.");
})();
