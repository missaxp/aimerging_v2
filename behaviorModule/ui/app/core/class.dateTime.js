/**
 * IDCP dates and times manipulation.
 * Allows to transform dates to a specific timeZone, to a specific output format and render date as string.
 * 
 * G:\web\idcp\IDCP 2014\pau\IDCP  Multi Zona Horaria.docx
 * 
 * 
 * 
 * This class it also meets with DST offset adjustments.
 * @param date string date
 * @param inputFormat string with the date format.
 * @return idcp_date_transform object.
 */
(function(){
	var formats = [
	               {locale : 'ca', dateFormat: 'DD/MM/YYYY',timeFormat : 'HH:mm:ss [UTC]Z',startDay : 1},
	               {locale : 'es', dateFormat: 'DD/MM/YYYY',timeFormat : 'HH:mm:ss [UTC]Z',startDay : 1},
	               {locale : 'en', dateFormat: 'YYYY-MM-DD',timeFormat : 'HH:mm:ss [UTC]Z',startDay : 0}
	];
	
	
	/**
	 * Set timeZone names on a local storage.
	 */
	$i.timeZones = moment.tz.names();
	
	function idcp_date_transform(date){
		var simpleDate = false; //A simple date is a date which NEVER CHANGES. Until the TZ changes this date remains the same. (ex. birth date). It always be expressed in UTC TZ.
		var serverTZ = ($i._get('webservice').timezone || 'UTC'); //Server timezone, given from Webservice.
		var userTZ = ($i.user._get().timezone==null? serverTZ:$i.user._get().timezone); //User config timezone, given by user config in their profile.
		var fallbackLang = 'en'; //Default fallback Language.
		var format = null; 
		var fallbackFormat = null;
		var momentDate = null;
		var _this = this;
		
		/**
		 * Constructor (always called).
		 * 
		 */
		(function(){
			switch(typeof date){
				case 'string':
					/**
					 * Ens arriba una data, en format 20160316T101112Z...
					 * 
					 * 2016 ANY
					 * 03 MES
					 * 16 DIA
					 * T separador temporal
					 * 10 HORA
					 * 11 MINUT
					 * 12 SEGON
					 * Z Zona horaria (Zulu == UTC == GMT).
					 * 
					 * 
					 * Creem un moment amb la data, com que aquesta data és en format correcte (ISO8601), la processem.
					 * El moment el creem especificant la zona horaria d'aquesta data. Li diem que la data creada té la zona horaria que el usuari té configurada...
					 * moment.tz(20160316T101112Z,'Pacific/Apia');
					 * 
					 * Moment detecta que la data entrada ja té especificada la zona horaria, per tant el que fa es calcular la hora entrada en la zona horaria que s'especifica.
					 * 
					 * Si la data entrada NO té especificada la zona horaria, llavors crea la data entrada en la zona horaria que s'ha especificat.
					 * 
					 * Cas 1: moment.tz(20160316T101112,'Pacific/Apia').format() //2016-03-16T10:11:12+14:00
					 * 
					 * Cas 2: moment.tz(20160316T101112Z,'Pacific/Apia').format() //2016-03-17T00:11:12+14:00
					 * 
					 * Així veiem que en el cas 1 sencillament ha creat la data en la zona horaria especifcada, en el segon cas ha transformat la data a la zona horaria,
					 * pq ha detectat que la data entrada ja tenia una zona horaria.
					 * 
					 * 
					 */
					momentDate = moment.tz(date,userTZ);
					if(!momentDate.isValid()){
						console.warn(date+" does not have a known date format.");
						momentDate = null;
					}
					break;
				case 'object':
					if(date._isAMomentObject){
						/*
						 * 	Si és un objecte moment, ens hem d'assegurar del següent:
								- Que la data entrada per l'usuari la volem en la ZONA HORARIA configurada per l'usuari, no la zona horaria LOCAL de l'usuari.
							Per tant el que fem és el següent:
								- Formatem l'objecte moment amb un format OFFSET DE ZONA HORARIA (l'offset es posa en el format amb la lletra Z).
								- Creem un objecte moment amb la data entrada ESPECIFICANT que el format de la data entrada és la zona horaria configurada per l'usuari. 
						*/
						var md = date.format('YYYYMMDD HH:mm:ss');  //REMOVE TIME ZONE FROM DATE.
						momentDate = moment.tz(md,'YYYYMMDD HH:mm:ss',userTZ); //SPECIFY THAT THIS CURRENT DATE INSERTED THIS TIMEZONE
					}
					else{
						console.warn("Not a moment object");
					}
					break;
				case 'undefined':
					//console.warn("Date is undefined");
					break;
			};
			
			//Set FallBackFormat.
			$.each(formats,function(){
				if(this.locale==fallbackLang){
					fallbackFormat = this;
				}
			});
		})();
		
		this.setSimple = function(sm){
			simpleDate = sm;
		}
		
		/**
		 * Set user's specific timeZone, format date to a local format language and return it as string.
		 * This method overwrites olders timezone settings and apply user config timezone.
		 * @return an object with 3 properties:
		 * 	date -> return only date.
		 * 	time -> return only time.
		 * 	datetime -> return date and time.
		 */
		this.local = function(){
			this.toTZ((simpleDate? serverTZ:userTZ));
			return {
				date 	: _this.toLocalFormat('D').render(),
				time 	: _this.toLocalFormat('T').render(),
				datetime: _this.toLocalFormat('DT').render()
			};
		}
		
		/**
		 * Set date to UTC (offset 00:00), format date to a server format and return it as string.
		 * This method overwrites olders timezone settings and apply servers timezone.
		 * @return string
		 */
		this.remote = function(){
			this.toTZ(serverTZ);
			this.toServerFormat();
			return this.render();
		}
		
		/**
		 * Set date to specific timeZone.
		 * @param tz string
		 * @return this
		 */
		this.toTZ = function(tz){
			if(momentDate==null){
				console.warn("I can not convert date to TZ "+tz);
			}
			else{
				momentDate = momentDate.tz(tz);
			}
			return this;
		}
		
		/**
		 * Alias toTZ method to set user config timezone.
		 */
		this.toUserTZ = function(){
			return this.toTZ(userTZ);
		}
		
		/**
		 * Alias toTZ method to set server timezone.
		 */
		this.toServerTZ = function(){
			return this.toTZ(serverTZ);
		}
		
		
		/**
		 * Set date format to local format (array formats), on specific language
		 * @return this
		 */
		this.toLocalFormat = function(f){
			var locale = i18n.lng();
			var formatFound = null;
			$.each(formats,function(){
				if(this.locale==locale){
					formatFound = this;
				}
			});
			if(formatFound!=null){
				switch(f){
					case 'D':
						format = formatFound.dateFormat;
						break;
					case 'T':
						format = formatFound.timeFormat;
						break;
					case 'DT':
					default:
						format = formatFound.dateFormat+" "+formatFound.timeFormat;
						break;
				}
			}
			return this;
		}
		
		
		/**
		 * Set date format to server format, specified by $i._get('serverTimeFormat').
		 * @return this
		 */
		this.toServerFormat = function(){
			format = $i._get('serverTimeFormat');
			if(format.endsWith('Z')){
				format = format.slice(0, -1)+'[Z]'; //[] is the escape char. I need Z as a char, not as a TimeZone. 
			}
			return this;
		}
		
		
		
		/**
		 * Render date to string.
		 * The render takes specific format. If no format is set, return the date in the fallback format.
		 * @return string
		 */
		this.render = function(){
			if(format==null){
				format = fallbackFormat.dateFormat+" "+fallbackFormat.timeFormat;
				console.warn("No format is set on this date, date is returned in: " +format);
			}
			if(momentDate==null){
				console.warn("Format is not possible. Return original date string: " +date);
				return date;
			}
			else{
				return momentDate.format(format);
			}
		}
		
		/**
		 * Retun date server formatted with offset relative to UTC.
		 * @return string
		 */
		this.relativeUTC = function(){
			format = $i._get('serverTimeFormat');
			return this.render();
		}
		
		
		/**
		 * Returns true if the current date is DayLight Saving Time. Otherwise returns false.
		 * @return boolean
		 */
		this.DST = function(){
			return momentDate.isDST();
		}
		
		/**
		 * Set moment object.
		 */
		this.setMoment = function(moment){
			momentDate = moment;
			return this;
		}
		
		/**
		 * Return current moment object.
		 * @return moment object
		 */
		this.getMoment = function(){
			return momentDate;
		}
		
		/**
		 * Get the local format.
		 * @return format object
		 */
		this.getLocalFormat = function(){
			var locale = i18n.lng();
			var formatFound = null;
			$.each(formats,function(){
				if(this.locale==locale){
					formatFound = this;
				}
			});
			if(formatFound==null){
				formatFound = fallbackFormat;
			}
			return formatFound;
		}
	}

	idcp.times = function(date,inputFormat,simple){
		return new idcp_date_transform(date,inputFormat,simple);
	};
	//console.log("Class idcp.times ready to be called. Every time you call idcp.times I will create a new idcp.times instance");
})();