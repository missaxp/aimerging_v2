/**
 * Read the map.json and get the require files for each URL.
 * 
 * @author phidalgo
 * @throws Exception
 * @usage:
 * 		$i.getAppMap()
 * 		.done(function(map,var){
 * 			//your code goes here...
 * 		});
 * 
 * If it does not found a require for this URL, it will throw an Exception.
 * If it could not read properly the map.json, it will throw an Exception
 * 
 *  map are structured by the URL "stages". For example:
 *  	www.example.com/stage1/stage2/stage3
 *  	
 *  	For each stage, map.json has the required files. If I enter to:
 *  	www.example.com/stage1, getAppMap will return the required files for stage1.
 *  
 *  	Lets going to suppose that map.json only has info up to stage3. What would be happen if the user enters to www.example.com/stage1/stage2/stage3/stage4 ?
 *  	The getAppMap will return required files for stage3 and will be pass "stage4" as a var.
 *  
 * 
 * Another example, if we want to have a dynamic URL with id of anything like:
 * 			www.example.com/stage1/MY_SPECIFIC_ID
 * 			getAppMap will return the required files for MY_SPECIFIC_ID, because on map.json we will set that in this case, maybe you will have a dynamic var ...
 * 			getAppMap also will add my "MY_SPECIFIC_ID" into a var and send it you too.
 * 
 * You can, obviously do a full stage request with any var on it like:
 * 			www.example.com/stage1/_ID1/stage3/_ID2/stage5/_ID3/stage7
 * 	And getAppMap will return the required files for the last right stage, let's suppose that stage7 is not on the map.json, the getAppMap will return required files for stage6 (= _ID3)
 *  And on the vars you will find:
 *  		MAP_0 = _ID1
 *  		MAP_1 = _ID2
 *  		MAP_2 = _ID3
 *  		MAP_3 = stage7
 *  You also have the possibility yo add a option on map json called "is" and specify the varname. Example of map.json:
 *  		
 *  		"/roles":{
 *				"require" : ["content/security/roles.html","content/security/js/roles.js"],
 *				"is": "role",
 *				"/:ALPHANUMERIC" :{
 *					"require" : ["content/security/role-module.html","content/security/js/role-module.js"]
 *				}
 *			},
 *
 *		lets suppose this URL:
 *
 *		www.example.com/roles/ID_ROLE_5/NOT_A_VALID_URL_MAP
 *
 *		What would getAppMap return, two args:
 *
 *		map,vars
 *
 *		map will contain an array like:
 *
 *		["content/security/role-module.html","content/security/js/role-module.js"]. 
 *		Thats the "/:ALPHANUMERIC" require array
 *
 *		vars object will contain:
 *
 *			{
 *				role: "ID_ROLE_5",
 *				MAP_1: "NOT_A_VALID_URL_MAP"
 *			}
 *		
 *	Its important to know that getAppMap does not check if the files of the require array exists. It does not do this kind of checks.
 */
(function(){
	function idcp_routing_management(){
		var mapFileJSON = null;
		
		this._get = function(){
			this.done = done;
			this.doneF;
			this.checkMap = checkMap;
			this.addVar = addVar;
			this.vars = {};
			this.varsCount = 0;
			
			var _this = this;
			
			getMap(function(map){
				checkMap(map);
			});
			
			function checkMap(m){
				var map = m;
				var hash = "";
				//This if is to avoid to fail when last character is /
				if(window.location.hash.substr(window.location.hash.length-1,1)=='/'){
					hash = window.location.hash.substr(0,window.location.hash.length-1);
				}
				else{
					hash = window.location.hash;
				}
				
				var url = hash.replace('#','').split('/');
				
				var currentRequire = [];
				
				var i = 0;
				var uriSpace = '';
				for(i=0;i<url.length;i++){
					uriSpace = url[i];
					var varName = '';
					if(typeof(map.is)!=='undefined'){
						varName = map.is;
					}
					rw = is_regExp(map,uriSpace);
					if(rw!==false || typeof(map['/'+uriSpace])!=='undefined'){
						if(rw===false){
							map = map['/' + uriSpace];
							currentRequire = map.require;
						}
						else{
							if(varName==''){
								varName = 'VAR_'+_this.varsCount;
							}
							_this.addVar(varName,uriSpace);
							uriSpace = rw;
							//console.log(uriSpace);
							map = map['/' + uriSpace];
							currentRequire = map.require;
						}
					}
					else{
						_this.addVar('MAP_'+_this.varsCount,uriSpace);
					}
				}
				var resp;
				if(typeof(map)==='undefined'){
					resp = currentRequire;
				}
				else{
					resp = map.require;
				}
				if(typeof(resp)!=='undefined'){
					_this.doneF(resp,((_this.varsCount==0)? null:_this.vars));
				}
				else{
					throw new AppException(6,'Current hash: ' + window.location.hash);
				}
			}
			

			function done(f){
				this.doneF = f; 
			}
			
			function addVar(n,u){
				//console.log("addVar => " + n + "=>" + u);
				this.vars[n] = u;
				this.varsCount++;
			}
			
			return this;
		};
		
		var is_regExp = function is_regExp(m,value){
			if (typeof(m)!=='undefined'){
				if(typeof(m['/'+value])!=='undefined') return false;
				var reservedWords = ["/:ALPHANUMERIC","/:ALPHABET","/:NUMERIC"];
				for(var i=0;i<reservedWords.length;i++){
					word = reservedWords[i];
					if(typeof(m[word])!=='undefined'){
						switch(word){
							case "/:ALPHANUMERIC":
								break;
							case "/:ALPHABET":
								if(!/^[a-zA-Z]+$/.test(value)){
									return false;
								}
								break;
							case "/:NUMERIC":
								if(!/^\d+$/.test(value)){
									return false;
								}
								break;
						}
						return word.replace('/','');
					}
				};
			}
			return false;
		};
		
		var getMap = function(cbk){
			if(mapFileJSON!=null){
				if(typeof(cbk)==='function'){
					cbk(mapFileJSON)
				}
				else{
					throw new Exception(50000,'No function attached to $i.getMap: ' + window.location.hash);
				}
				//this.checkMap(m);
			}
			else{
				$.getJSON("app/json/map.json", function(map) {
					mapFileJSON = {};
					mapFileJSON = map;
					var m = map;
					if(typeof(cbk)==='function'){
						cbk(mapFileJSON);
					}
					else{
						throw new Exception(50000,'No function attached to $i.getMap: ' + window.location.hash);
					}
				})
				.fail(function( jqXHR, textStatus ) {
					throw new Exception(5,textStatus,true);
				});
			}
		}
		
		this.exists = function(hash){
			var map = mapFileJSON;
			//This if is to avoid to fail when last character is /
			if(hash.substr(hash.length-1,1)=='/'){
				hash = hash.substr(0,hash.length-1);
			}
			else{
				hash = hash;
			}
			
			var url = hash.replace('#','').split('/');
			var currentRequire = [];
			var i = 0;
			var uriSpace = '';
			for(i=0;i<url.length;i++){
				uriSpace = url[i];
				rw = is_regExp(map);
				var varName = '';
				currentRequire = map.require;
				map = map['/' + uriSpace];
				if(typeof(map)==='undefined'){
					break;
				}
			}
			var resp;
			if(typeof(map)==='undefined'){
				resp = currentRequire;
			}
			else{
				resp = map.require;
			}
			if(typeof(resp)!=='undefined'){
				return true;
			}
			else{
				return false;
			}
		}
	}
	
	idcp.routing = new idcp_routing_management();
	//console.log("class idcp.routing ready to be used.");
})();
