var countDownInitialVal = 20;
var countDownToZero = false;

$('#modal-login p[data-id="profile-name"]').html($i.user._get().name+' '+($i.user._get().surname!=null? $i.user._get().surname:""));
$('#modal-login input[data-id="username"]').val($i.user._get().username);
$('#modal-login h3[data-id="countdown"]').html(countDownInitialVal);

//Clear all app intervals
$i.intervals.app.clear();

if($i.user._get().avatar!=null){
	$('#modal-login img[data-id="profile-img"]').attr('src',"data:image/gif;base64," + $i.user._get().avatar);
}

if($i.cookies._get("session_token")){ //If, for any reason, we do not have session token on cookie, we ask for password.
	$i.intervals.app._add('countdown_login_modal',makeLogin,1000);
}
else{
	$('#modal-login h3[data-id="countdown"]').html('0');
	makeLogin();
}

function makeLogin(){
	var val = parseInt($('#modal-login h3[data-id="countdown"]').html());
	if(val!==0){
		$('#modal-login h3[data-id="countdown"]').html(val-1);
	}
	else{
		$i.intervals.app.clear('countdown_login_modal');
		$('#modal-login-form input[name="password"]').removeClass("hidden");
		$('#modal-login-form input[name="signIn"]').val("Continua");
		$('#modal-login h4[data-id="modaltitle"]').html($i.i18n('login:session_expired'));
		$('#modal-login div[data-id="countdownContainer"]').html('');
		$i.cookies._delete("session_token");
		countDownToZero = true;
	}
}

$i.intervals.pages.clear(); //Clear the current page intervals, if any.

var message = ""; //Aqui situem el missatge d'error en cas que l'usuari hagi cancelat la finestra modal.
var isValid = false; //Determina si al amagar la finestra modal, cal anar a la pàgina de login o no.
$('#modal-login-form').on('submit', function(){
	 event.preventDefault(); // Stop form from submitting normally
	 if(countDownToZero){ //Si cal reloguejar-se
		 $("#login-response-msg").removeClass("hidden alert-danger").addClass("alert-info").html("<i class='fa fa-spinner fa-spin orange bigger-125'></i>" +  $i.i18n("login:user_validating"));
		 var data = {
				 user : $('input[name="user"]',this).val(),
				 password : $.md5($('input[name="password"]',this).val())
			 };
		 
		 
		 /*var pwd = $('input[name="password"]',this).val();
		 var serializedReturn = $('input[name!="password"]', this).serialize();
		 serializedReturn += '&password='+$.md5(pwd);*/ 
		 $i.promise._request("POST", "login", data)
		.done(function(data) {
			isValid = true;
			idcp.sessionManagement(data);
			
			// Set session_token cookie value
			$i.cookies._set("session_token", data.data.token, data.data.expires);
			$("#login-response-msg").removeClass("hidden alert-info").addClass("alert alert-block alert-success").html("<i class='fa fa-check bigger-125'></i>" + $i.i18n("login:success"));
			$("#modal-login").modal('hide');
			// Bug: modal-backdrop remains after modal hide
			$('body').removeClass('modal-open');
			$('.modal-backdrop').remove();
			$i.intervals.pages.apply();
		})
		.fail(function( jqXHR, textStatus ) {
			$("#login-response-msg").removeClass("hidden alert-info").addClass("alert-danger").html("<i class='fa fa-exclamation-triangle bigger-125'></i>" + $i.i18n("login:error_" + jqXHR.responseJSON.error_code));
		});
	 }
	 else{ //Si no cal.
		 $i.intervals.app.clear('countdown_login_modal');
		 $i.checkToken()
		.done(function(data){
			isValid = true;
			$i.sessionManagement(data);
			$i.intervals.pages.apply();
			$('#modal-login').modal('hide');
		})
	 	.fail(function(jqXHR){
	 		$i.cookies._delete("session_token");
	 		isValid = false;
			//$("#login-response-msg").removeClass("hidden alert-info").addClass("alert-danger").html("<i class='fa fa-exclamation-triangle bigger-125'></i>" + $i.i18n("login:error_" + jqXHR.responseJSON.error_code));
			makeLogin();
	 	});
	 }
	
	return false; // Stop the normal form submit.
});
$("#modal-login").on('hidden.bs.modal', function(e){
	$i.UI.layout_modal.html('');
	if (!isValid){
		$i.cookies._delete('session_token');
		location.reload();
	}
	else{
		$i.intervals.app.apply('session_timeout');
		$i.intervals.app.apply('token_expires');
	}
});