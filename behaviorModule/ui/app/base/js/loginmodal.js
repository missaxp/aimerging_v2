$i.cookies._delete("session_token");


$('#modal-login p[data-id="profile-name"]').html($i.user._get().name+' '+($i.user._get().surname!=null? $i.user._get().surname:""));
$('#modal-login span[data-id="username-title"]').html($i.user._get().username);
$('#modal-login input[data-id="username"]').val($i.user._get().username);

$i.intervals.pages.clear(); //Clear the current page intervals, if any.

var isValid = false; //Determina si al amagar la finestra modal, cal anar a la pàgina de login o no.
$('#modal-login-form').on('submit', function(){
	 event.preventDefault(); // Stop form from submitting normally
	$("#login-response-msg").removeClass("hidden alert-danger").addClass("alert-info").html("<i class='fa fa-spinner fa-spin orange bigger-125'></i>" +  $i.i18n("login:user_validating"));
	 // Get ajax promise for form serialized data
	 var data = {
		 user : $('input[name="user"]',this).val(),
		 password : $.md5($('input[name="password"]',this).val())
	 };
	
	/* var pwd = $('input[name="password"]',this).val();
	 var serializedReturn = $('input[name!="password"]', this).serialize();
	 serializedReturn += '&password='+$.md5(pwd);*/
	 $i.promise._POST("login", data)
	.done(function( data ) {
		isValid = true;
		$i.sessionManagement(data);
		
		// Set session_token cookie value
		$i.cookies._set("session_token", data.data.token, data.data.expires);
		$("#login-response-msg").removeClass("hidden alert-info").addClass("alert alert-block alert-success").html("<i class='fa fa-check bigger-125'></i>" + $i.i18n("login:success"));
		$("#modal-login").modal('hide');
		// Bug: modal-backdrop remains after modal hide
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
	})
	.fail(function( jqXHR, textStatus ) {
		isValid = false;
		$("#login-response-msg").removeClass("hidden alert-info").addClass("alert-danger").html("<i class='fa fa-exclamation-triangle bigger-125'></i>" + $i.i18n("login:error_" + jqXHR.responseJSON.error_code));
	});
	return false; // Stop the normal form submission
});
$("#modal-login").on('hidden.bs.modal', function(e){
	$i.UI.layout_modal.html('');
	if (!isValid){
		$i.cookies._delete('session_token');
		location.reload();
	}
});