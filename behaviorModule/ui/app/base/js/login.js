function submitActions(){
	 $("#login-response-msg").removeClass("hidden alert-danger").addClass("alert-info").html("<i class='fa fa-spinner fa-spin orange bigger-125'></i>" + $i.i18n("login:user_validating"));
	 var data = {
		 user : $('input[name="user"]').val(),
		 password : $.md5($('input[name="password"]').val())
	 };
	 $i.promise._POST("login", data)
	.done(function(data) {
		$("#login-response-msg").removeClass("hidden alert-danger").addClass("alert-info").html("<i class='fa fa-spinner fa-spin orange bigger-125'></i>" + $i.i18n("login:loading"));
		$i.cookies._set("session_token", data.data.token, data.data.expires); // Set session_token cookie value
		$i.UI.application.load(data,true);
	})
	.fail(function( jqXHR, textStatus ) {
		// Show app error code
		if (jqXHR.responseJSON && jqXHR.responseJSON.error_code) {
			$("#login-response-msg").removeClass("hidden alert-info").addClass("alert-danger").html("<i class='fa fa-exclamation-triangle bigger-125'></i>" + $i.i18n("login:error_" + jqXHR.responseJSON.error_code, { defaultValue: $i.i18n('login:default_error')}));
			
		// Show status code, warning: if OPTIONS web service is not configured for the request the status code is 0
		} else if (jqXHR.status>0) {
			$("#login-response-msg").removeClass("hidden alert-info").addClass("alert-danger").html("<i class='fa fa-exclamation-triangle bigger-125'></i>"  + $i.i18n("idcp:error_" + jqXHR.status));
		} else {
			$("#login-response-msg").removeClass("hidden alert-info").addClass("alert-danger").html("<i class='fa fa-exclamation-triangle bigger-125'></i>"  + $i.i18n("idcp:error_options"));
		}
	});
	 return false;
}
$("#loginform").on("submit", submitActions); //Attach a submit handler to the form

$('input[name="user"]').focus();