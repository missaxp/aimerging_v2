$i.user._get.done(function(){
	//If the login screen is visible, in this point show to the user that now is rendering the UI.
	$("#login-response-msg").html("<i class='fa fa-spinner fa-spin orange bigger-125'></i>" + $i.i18n("login:loading_layout"));
	$i.require([
	    "lib/external-lib/select2.full.min.js","lib/css/select2.min.css",    
	    //"assets/js/chosen.jquery.js","assets/css/chosen.css",
	    //"assets/js/jquery.ui.touch-punch.js",
	    //"assets/js/bootbox.js",
	    //"assets/js/jquery.hotkeys.js",
	    //"assets/js/fuelux/fuelux.spinner.js",
	    //"assets/js/jquery.maskedinput.js",
	    //"assets/js/ace/elements.fileinput.js",
	    //"assets/js/ace/elements.typeahead.js",
	    //"assets/js/ace/elements.spinner.js",
	    //"assets/js/ace/elements.treeview.js",
	    //"assets/js/ace/elements.wizard.js",
	    "assets/js/ace/elements.aside.js",
	    //"lib/external-lib/jquery.ba-hashchange.min.js",
	    //"lib/external-lib/jquery.collapse.js",
	    "lib/external-lib/masonry.pkgd.min.js",
	    //"lib/external-lib/colResizable-1.5.min.js",
	    "lib/external-lib/daterangepicker.js","lib/css/daterangepicker.css",
		"app/core/class.forms.js",
		"app/core/class.history.js",
		"app/core/class.maprouting.js",
		"app/core/class.page.js",
		"app/core/class.security.js",
		"app/core/class.dateTime.js",
		"app/core/class.menu.js",
		"app/core/class.notifications.js",
	    "app/widgets/views.js",
	    "app/widgets/graphs.js",
	    "app/widgets/list.js",
	    "app/widgets/modals.js",
	    "app/widgets/gritters.js",
	    "app/widgets/wysiwyg.js",
	    "app/widgets/relation.js",
	    "app/widgets/suggestion.js",
	    "app/widgets/starrating.js",
	    "app/widgets/nestable.js",
	    "app/widgets/fupload.js",
	    "app/widgets/flist.js",
	    "app/widgets/avatar.js",
	    "app/widgets/steps.js",
	    "app/widgets/datepicker.js",
	    "app/widgets/daterange.js",
	    "app/widgets/select2.js",
   ],null,null,false).done(function(){
	   /**
	    * Adds an interval to check webservice status.
	    */
	   var times = $i._get('time');
	   times.hq = $i.times($i._get('webservice').time).toTZ($i._get('webservice').hqtz).toLocalFormat();
	   times.local = $i.times($i._get('webservice').time).toUserTZ().toLocalFormat();
	   times.server = $i.times($i._get('webservice').time).toServerTZ().toLocalFormat();
	   times.os = moment();
	   times.localFormat = times.server.getLocalFormat().dateFormat+' '+times.server.getLocalFormat().timeFormat;
	   //console.warn("User OS TZ: "+moment.tz.guess()+"', User OS time: " +times.os.format(times.localFormat)+", User Config TZ: " + $i.user._get().timezone+", User Config time: "+times.local.render()+", Server Time Zone: " + $i._get('webservice').timezone+", Server Time: "+times.server.render());
    	// Assign content layout 
		$i.i18n.loadNamespaces(['login','widgets','layout','forms','throw','menu','wordcount'],function(){
			$i.UI.rootObject.i18n();
			$i.require([
                "assets/js/ace/ace.js",
                "assets/js/ace/elements.scroller.js",
                //"assets/js/ace/elements.fileinput.js", //Ho necessita per la càrrega d'avatar (prototype x-editable type image).
        		"assets/js/ace/ace.touch-drag.js",
        		//"assets/js/ace/ace.sidebar.js", //Per menu.
        		//"assets/js/ace/ace.sidebar-scroll-1.js", //Per menu.
        		"assets/js/ace/ace.submenu-hover.js",
		    ],null,null,false).done(function(){
		    	$i.menu.load(function(){ //Load menu function from class.menu.js
		    		var appserinfo = ($i._get('webservice').development? 'Development':'Production') + ' server';
			    	appserinfo += ' | Front end v0.' + $i._get('version') + ' | Back end v0.'+ $i._get('webservice').revision;
			    	$i.UI.footer.html(appserinfo); //Set prod or dev environment string
					$i.UI.usersName.html(($i._get('webservice').authentication_required? $i.user._get().name:'root')); //Set the username into the User Menu (top right)
					if($i.user._get().avatar!=null){
						$i.UI.usersPhoto.attr('src',"data:image/gif;base64," + $i.user._get().avatar);
					}
					// Logout button
					$i("#logout",$i.UI.rootObject).on('click',function(){
						$i.cookies._delete("session_token");
						location.reload();
					});
					
					$("body").removeClass("login-layout");
					$("body").addClass("no-skin");
					$i.UI.layout_content = $i.UI.rootObject.find("#content");
					$i.UI.layout_root.html($i.UI.rootObject);
					
					if(!$i._get('debug')){
					   $('#server_time_li').hide();
					   $('#os_time_li').hide();
				   }
					
					if(($i.hash._get()=='#' || $i.hash._get()=="") && $i.user._get().ui_home_page!=null && $i.user._get().ui_home_page!="/"){
						console.warn('$i.hash._get(): '+$i.hash._get()+', $i.user._get().ui_home_page: '+$i.user._get().ui_home_page);
						setTimeout(() => {
							$i.hash._set($i.user._get().ui_home_page);
						}, 50);
						
					}
					
					$('.initial_load').fadeOut(function(){$(this).remove();}); //Removes initial load div.
		    	}); 
		    });
		});
   });
});
