var idcp_config = {
	debug 				: false, // Debug mode on/off console.log and gritter messages
	api					: null,
	apiTimeout 			: 100000, //10 seconds.
	langs 				: ["en","es","ca"], // Available languages
	defaultLang			: 'en',
	titlePrefix 		: "AI - ", //Prefix title
	timeoutWarning		: 30000, //30 seconds
	pageCache			: 20, //Max number of cached pages. 0 no limit.
	gritterTime 		: 5000, //Time in ms to hide a gritter.
	fileSize 			: 107374182400, //bytes (1024 MB),
	chunkSize 			: 10000000, // 10 MB
	checkAPIStatusEvery : 60000, //Check API Status every 60 seconds. (checkAPI status UPDATE frontend clock to be sync with server)!important thing update clock..
	notificationType	: 'notification', //values : notification/gritter
	serverTimeFormat 	: 'YYYYMMDDTHHmmssZ', //Server Date time format in ISO8601 simple mode.
	timePattern 		: /(19|20)\d{2}(0|1)[0-9][0-3][0-9]T[0-5][0-9][0-5][0-9][0-5][0-9]Z/, //Pattern to search a datetime in format: YYYYMMDDTHHMISS±TZ
	version				: '696', //Front end version.
	appName				: 'AIMBM:UI'
	
}

if(idcp_config.debug){
	//idcp_config.api = '//idcpws.linux/v1/';
	idcp_config.api = '//ai.linux/api/v1/'
}
else{
	idcp_config.api = '//ai.idisc.es/api/v1/';
}
