function graph(config){
	var flot_library_path = ['assets/js/jquery.easypiechart.js','assets/js/flot/jquery.flot.js','assets/js/flot/jquery.flot.pie.js'];
	this.printChart = printChart;
	this.getInfo = getInfo;
	
	this.api = null;
	this.minGroup = 0;
	this.fields = null;
	this.sort = null;
	this.box = null; //Jquery Element
	
	this.graphData = null;
	
	if(typeof(config)!== 'object') throw ("GRAPH: config object is missing or is not an object");
	else{
		this.api = (typeof(config.data)!=='undefined')? config.data:null;
		this.minGroup = (typeof(config.minGroup)!=='undefined')? config.minGroup:null;
		this.box = (typeof(config.box)!=='undefined')? config.box : null;
		this.graph = (typeof(config.graph)!=='undefined')? config.graph : null;
	}
	
	//setup array of scripts and an index to keep track of where we are in the process
	var _this = this;
	idcp.require(flot_library_path).done(function(){
		_this.getInfo();
	});
	
	function getInfo(){
		var _this = this;
		$i.promise._GET(this.api.url,{returnFields: this.api.fields,sortfield: '-'+this.graph.data})
		.done(function(data){
			var info = [];
			$.each(data.data.results,function(k,v){
				info.push({label: v[_this.graph.label.toLowerCase()],data: v[_this.graph.data.toLowerCase()]});
			});
			if(info.length>_this.minGroup){ //If there is more than this.minGroup roles, we group less relevant of them (by users indise each role).
				var others = {};
				others.label = '';
				others.data = 0;
				for(var i=_this.minGroup; i<info.length;i++){
					others.label += info[i].label+", ";
					others.data = parseInt(others.data) + parseInt(info[i].data);
				}
				others.label = others.label.substr(0,others.label.length-2);
				info = info.slice(0,_this.minGroup);
				info.push(others);
			}
			_this.graphData = info;
			_this.printChart();
		});
	}
	
	function printChart(){
		var placeholder = this.box.css({'width':'90%' , 'min-height':'150px'});
		
		drawPieChart(placeholder, this.graphData);
		
		function drawPieChart(placeholder, info, position) {
			$.plot(placeholder, info, {
				series: {
					pie: {
						show: true,
						tilt:0.8,
						highlight: {
							opacity: 0.25
						},
						stroke: {
							color: '#fff',
							width: 2
						},
						startAngle: 2
						}
				},
				legend: {
					show: true,
					position: position || "ne", 
					labelBoxBorderColor: null,
					margin:[-30,15]
					},
				grid: {
					hoverable: true,
					clickable: true
				}
			 });
		}
		
		
		 
		 /**
		 we saved the drawing function and the data to redraw with different position later when switching to RTL mode dynamically
		 so that's not needed actually.
		 */
		 placeholder.data('chart', this.graphData);
		 placeholder.data('draw', drawPieChart);
	
	
	
		  var $tooltip = $("<div class='tooltip top in'><div class='tooltip-inner'></div></div>").hide().appendTo('body');
		  var previousPoint = null;
		
		  placeholder.on('plothover', function (event, pos, item) {
			if(item) {
				if (previousPoint != item.seriesIndex) {
					previousPoint = item.seriesIndex;
					var tip = item.series['label'] + " : " + item.series['percent'].toFixed(2)+'%';
					$tooltip.show().children(0).text(tip);
				}
				$tooltip.css({top:pos.pageY + 10, left:pos.pageX + 10});
			} else {
				$tooltip.hide();
				previousPoint = null;
			}
		 });
	}
}

idcp.graphs = function(cfg){
	return new graph(cfg);	
};