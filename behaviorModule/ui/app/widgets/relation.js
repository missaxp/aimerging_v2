/**
 * Class relation.js.
 * 
 * This class provides a relation functionality on a form.
 * When we want to select a option having a large amount of options perhaps is a better solution use a paginated table with filter options (search,...)
 * instead of a simple select load with all the large amount of options.
 * 
 * This class provides this functionality. 
 * 
 * This class uses another own class called $i.views specifically configured to set a button used to select the option of a table. We can configure 
 * how many columns we want and also which column is the VALUE and which column is the NAME of the value. This is the options we have on a typical select.
 * 
 * The use of this class is on a <input type="">, this class changes the type of the input to "hidden" and creates a "search" button. Once the user click
 * on this button, a modal tab is opened with the $i.view. Once the user has found the option and click to the select button, the class update the input
 * value and triggers change event on the input to notify a $i.form trackchanges that this input is changed (and follow, if the page is configured to follow)
 * the form trackchanges and validation processes.
 * 
 * 
 * 
 * As usual, we have to configure it having an argument that is an object. This object has to have the following settings.
 * 
 * {
 * 	rest : 'crm/thirdparty', <--- REST URL TO GET THE DATA.
 *  columns : ["CODE","NAME"], <--- COLUMNS TO RENDER
 *  render : "NAME", <--- COLUMN USED TO RENDER THE OPTION ON THE FORM (typically ID: 0001 NAME : 'OPTION NAME 1'. We want to render NAME not ID)
 *  set: "CODE", <--- OPTION TO SET THE VALUE TO THE INPUT (we want to save the ID not the NAME...)
 *  value: ct.tp_id <--- default value if any.
 * }
 * 
 * Can also accept .done method to call a done function when the user click on "DONE" modal button.
 * 
 * @author phidalgo
 * @created 20151130
 * @modified 20151201
 */

function relation(config,$primaryObject){
	var _PLUGIN_NAME = 'idcp.widgets.relation';
	var _PLUGIN_ID = $i.generator();
	var readOnly = false;
	this.rest = config.rest;
	this.columns = config.columns;
	this.getFunction = null;
	this.modal = null;
	this.value = config.value || null; //Default value.
	this.name = config.name || null;//Default name.
	this.view = null;
	this.get = config.get || function(){};
	
	this.selectedItems = []; //Array which contains the selected items from the instantiation.
	
	//this.iconRel = 'fa fa-search';
	this.iconRel = 'fa fa-link';
	this.iconClearRel = 'fa fa-chain-broken';
	this.iconRel404 = 'fa fa-exclamation-triangle orange';
	
	this.tooltipRel = $i.i18n('widgets:tooltip_addRelation');
	this.tooltipRemoveRel = $i.i18n('widgets:tooltip_removeRelation');
	this.tooltipRel404 = $i.i18n('widgets:tooltip_rel404');
	
	this.done = function(cbk){
		if(typeof(cbk)==='function'){
			_this.get = cbk;
		}
		else{
			_this.get = function(rsp){
				console.log("NO DONE ATTACHED FUNCTION TO RELATION.");
				console.log(rsp);
			}
		}
	};
	
	var _this = this;
	
	var $parent = $primaryObject.parent();

	var nameContainer = "renderColumn_"+_PLUGIN_ID;
	$primaryObject.attr('disabled',true);
	$primaryObject.removeAttr('class');
	var inputId = $primaryObject.data('id');
	$primaryObject.attr('type','hidden');
	$primaryObject.attr('data-relation','true'); //Set that is a input which contains a relation object.
	$primaryObject.attr('data-plugin-name',_PLUGIN_NAME);
	$primaryObject.attr('data-plugin-id',_PLUGIN_ID);
	$parent.append('<span data-id="'+nameContainer+'" class="relation_span_view"></span>'+
		'&nbsp;<i class="'+this.iconRel+' bigger-120" data-id="relation_addRel" style="cursor:pointer;" data-placement="bottom" title="'+this.tooltipRel+'"></i>'+
		'&nbsp;<i class="'+this.iconClearRel+' bigger-120" data-id="relation_clearRel" style="cursor:pointer;display:none;" data-placement="bottom" title="'+this.tooltipRemoveRel+'"></i>'+
		'&nbsp;<i class="'+this.iconRel404+' bigger-120" data-id="relation_rel404" style="cursor:pointer;display:none;" data-placement="bottom" title="'+this.tooltipRel404+'"></i>'
	);
	
	nameContainer = $parent.find('span[data-id="'+nameContainer+'"]');
	$parent.find('i').tooltip();
	
	$parent.find('i[data-id="relation_clearRel"]').on('click',function(){
		$primaryObject.val('').trigger('change');
		nameContainer.html('');
		$parent.find('i[data-id="relation_clearRel"]').css('display','none');
	});
	
	
	/**
	 * Triggers that when on change the value in the following actions:
	 * - When a default value is instantiated (check the name on the API).
	 * - When a value is set by selecting it (obj argument is not null and it has the name, so not check it on the api).
	 * - When an external event triggers change (for example, form.trackChanges).
	 */
	$primaryObject.on('change',function(e,obj){
		//console.log("VALUE CHANGED:" + $primaryObject.val());
		if($primaryObject.val()!=""){
			if(typeof(obj)!=='undefined' && typeof(obj.name)!=='undefined'){
				nameContainer.html(obj.name);
				$i('#relation_clearRel').css('display','auto');
			}
			else{
				//$i.promise._GET(config.rest,{searchString: $primaryObject.val(),fields : _this.columns.toString()})
				$i.promise._GET(config.rest,{filterfield: config.set.toLowerCase(),filtervalue: $primaryObject.val(),returnFields : _this.columns})
				.done(function(rsp){
					if(rsp.data.results.length!=0){
						//console.log(rsp.data.results[0][config.render.toLowerCase()]);
						if(typeof(rsp.data.results[0][config.render.toLowerCase()])!=='undefined'){
							_this.name = rsp.data.results[0][config.render.toLowerCase()]
							nameContainer.html(_this.name);
							$i('#relation_clearRel').css('display','auto');
						}
						else{
							
						}
					}
					else{
						$i('#relation_rel404').css('display','auto');
					}
				});
			}
		}
	});
	
	if(this.value!=null){
		//$primaryObject.val(this.value).trigger('change');
		nameContainer.html(this.name);
		$i('#relation_clearRel').css('display','auto');
	}
	
	$parent.find('i[data-id="relation_addRel"]').on('click',function(){
		_this.modal = $i.modal({
			title : $i.i18n("widgets:modal_title_relation"),
			description : '<div data-id="put_view"></div>',
			show: true,
			position : ['center',20],
			width: '70%',
			resizable : true,
			success: function(id,modal,$modal){
				_this.get();
			},
			execute : renderView //Return $modal
		});
	});
	
	function renderView(id,$modal){
		var columns = [];
		for(var i=0,j=_this.columns.length;i<j;i++){
			columns.push({name: _this.columns[i]});
		}

		$modal.superview({
			type : 'TABLE',
			rest: _this.rest,
			rowId: columns[0].name,
			searchable: _this.columns.toString(),
			tipscol: {success: true,action:buttonsFunction},
			dataType: _this.columns,
			i18n : config.i18n,
			cols: columns,
			addReloadButton : false
		});
		
		function buttonsFunction(obj,action){
			var code = $(obj).data(_this.columns[0].toLowerCase());
			//console.log(action+": "+_this.columns[0]+": " +code);
			var rsp = [];
			$.each(_this.columns,function(){
				rsp.push($(obj).data(this.toLowerCase()));
			});
			nameContainer.html($(obj).data(config.render.toLowerCase()));
			$primaryObject.val($(obj).data(config.set.toLowerCase())).trigger('change',{name : $(obj).data(config.render.toLowerCase())}); //Send name to avoid to check it again on WS
			$parent.find('i[data-id="relation_clearRel"]').css('display','auto');
			//$modal.dialog('close');
			_this.modal.close();
			_this.get(rsp);
		}
	}
	
	var iid = null;
	var readOnlyF = this.readOnly = function(ro){
		//console.log("POSEM A READ ONLY el plugin "+$primaryObject.data('plugin-name')+" amb ID: "+$primaryObject.data('plugin-id'));
		readOnly = ro;
		$parent.find('i').remove(); //Deshabilitar les funcionalitats del plugin.
	}
	
	var page = $i.pages.find.page($primaryObject.closest('div[data-pagehash]').data('pagehash'))
	if(page!==false){
		page.plugins(_PLUGIN_NAME,this);
	}
	else{
		console.log("Page object not found");
	}
	
}

$.fn.extend({
	relation : function(config){
		return new relation(config,$(this));
	}
});


$i.relation = function(config){
	return new relation(config)
}