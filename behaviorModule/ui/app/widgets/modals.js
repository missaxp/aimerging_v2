idcp.modal = function(config){
	var _PLUGIN_NAME = 'idcp.widgets.modal';
	var uniqueID = $i.generator();
	
	this.autoShow = config.show || false; //Automatically shows the modal if true.
	this.title = config.title || '';
	this.description = config.description || '';
	this.width = (typeof(config.width)!=='undefined'? config.width:'500px');
	this.height = config.height || 'auto';
	this.resizable = (typeof(config.resizable)!=='undefined'? config.resizable:true);
	this.autoCloseOnSuccess = (typeof(config.autoClose)!=='undefined'? config.autoClose:true);
	this.doneButton = config.doneButton || $i.i18n('widgets:bt_success');
	this.cancelButton = config.cancelButton || $i.i18n('widgets:bt_cancel');
	this.dialogPosition = config.position || null;
	this.defaultButtons = (typeof(config.defaultButtons)!=='undefined'? config.noButtons:true);
	this.open = function(){
		if(typeof(config.open)==='function'){
			config.open($('#'+_this.id));
		}
	}
	
	
	
	this.bind = config.bind || null;
	this.event = config.event || 'click';
	this.success = ((typeof(config.success)==='function')? config.success : null); //Mandatory to instance
	this.cancel = ((typeof(config.cancel)==='function')? config.cancel : null); //Optional
	
	this.destroy = destroy;
	this.generate = generate;
	this.html = html;
	//this.show = show;
	this.id = null;
	
	this.$dialog = null;

	var buttons = [];
	
	
	//Cancel (close) button.
	if(this.defaultButtons){
		buttons.push({
			text: this.cancelButton,
			"class" : "btn btn-sm",
			icon : 'fa fa-times',
			click: function() {
				var closeDialog = true;
				if(typeof(_this.cancel)==='function'){
					try{
						var rsp = _this.cancel(id,_this,$i('#modal_content',$modal),$modal);
						if(typeof(rsp)==='boolean'){
							closeDialog = rsp;
						}
					}
					catch(err){
						throw new AppException(2004,err);
					}
				}
				if(closeDialog){
					_this.close();
				}
			} 
		});
	}
	
	
	if(typeof(config.buttons)==='object'){
		$.each(config.buttons,function(){
			buttons.push(this);
		});
	}
	
	if(this.defaultButtons){
		//At the last, add the done button.
		buttons.push({
				text: this.doneButton,
				"class" : "btn btn-sm btn-primary",
				icon : "fa fa-check",
				click: function() {
					var closeDialog = true;
					try{
						var rsp = _this.success(id,_this,$i('#modal_content',$modal),$modal);
						if(typeof(rsp)==='boolean'){
							closeDialog = rsp;
						}
					}
					catch(err){
						throw new AppException(2003,err);
					}
					if(_this.autoCloseOnSuccess && closeDialog){
						_this.close();
					}
				} 
			}
		);
	}
	
	var _this = this;
	
	/**
	 * Local vars.
	 */
	var icon = (config.icon? config.icon:"ace-icon fa fa-check"); //Default title icon.
	
	if(this.success==null){
		throw new AppException(2001);
	}
	
	this.getId = function(){
		return this.id;
	}
	
	this.generate();



	var $modal;
	function generate(){
		$modal = this.html();
		
		this.getModelContent = function () {
			return $i('#modal-dialog',$modal).find('div[data-id="modal_content"]');
		}

		$i('#modal-dialog',$modal).css('width',this.width);
		$i('#modal-dialog',$modal).css('height',this.height);
		$modal.on('hide.bs.modal', function () {
            _this.close();
    	});
    	
		$buttons = $i('#buttons_content',$modal);
		
		$.each(buttons,function(){
			var bt = this;
			var $bt = $('<button class="'+this['class']+'">'+
				'<i class="'+this.icon+'"></i>&nbsp;<span">'+this.text+'</span>'+
			'</button>');
			$bt.on('click',function(){
				bt.click();
			});
			$bt.appendTo($buttons);
		});
		
		$i('#modal_title',$modal).html('<i class="'+icon+'"></i>'+this.title);
		$i('#modal_content',$modal).html((typeof(this.description)==='function'? this.description():this.description));
		
		$i('#modal-close',$modal).on('click',function(){
			_this.close();
		});

		

		if(config.i18n){
			//Load i18n localizated strings.
			i18n.loadNamespace(config.i18n,function(){
				$modal.i18n();
			}); 
		}
		else{
			$modal.i18n();
		}
		if(typeof(config.execute)==='function'){
			//config.execute(this.id,$i('#'+this.id,$i.UI.layout_modal),$i('#'+this.id,$i.UI.layout_modal).next().find('div.ui-dialog-buttonset'),$i('div[aria-describedby="'+this.id+'"]',$i.UI.layout_modal));
			config.execute(this.id,$i('#modal_content',$modal),$i('#buttons_content',$modal),$modal,this);
		}
		
		
		if(this.autoShow){
			//$('#'+this.id).dialog("open");
			$modal.modal();
		}
	}
	
	this.close = function(){
		$modal.remove();
		//console.log($('div[data-id="modal-'+uniqueID+'"]'));
		$('div[data-id="modal-'+uniqueID+'"]').remove();
		// Bug: modal-backdrop remains after modal hide
		$('body').removeClass('modal-open');
		$('body').removeAttr('style');
		$('.modal-backdrop.in').remove();
	}
	
	function destroy(){
		if(typeof(_this.cancel)==='function'){
			try{
				_this.cancel();
			}
			catch(err){
				throw new AppException(2004,err);
			}
		}
		$i.UI.layout_modal.empty();
	}
	
	function html(){
		return $('<div data-id="modal-'+uniqueID+'" id="headModal" class="modal" tabindex="-1"	aria-hidden="true" style="display: none;" role="dialog"'+
		'data-plugin-name="'+_PLUGIN_NAME+'" data-plugin-id="'+uniqueID+'">'+
			'<div class="modal-dialog" data-id="modal-dialog" >'+
				'<div class="modal-content">'+
					'<div class="modal-header">'+
						'<button type="button" class="close" data-dismiss="modal" data-id="modal-close">×</button>'+
						'<h4 class="blue bigger" data-id="modal_title"></h4>'+
					'</div>'+
					'<div class="modal-body overflow-visible">'+
						'<div class="widget-body">'+
							'<div class="widget-main" data-id="modal_content" id="modal">'+
							'</div>'+
							'<!-- /widget-main -->'+
						'</div>'+
						'<!-- /widget-body -->'+
					'</div>'+
					'<div class="modal-footer" data-id="buttons_content">'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>');
	}
	
	return this;
};

/**
 * Overrides the function used when setting jQuery UI dialog titles, allowing it to contain HTML.
 */
$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
    _title: function(title) {
        if (!this.options.title ) {
            title.html("&#160;");
        } else {
            title.html(this.options.title);
        }
    }
}));