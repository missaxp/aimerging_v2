/**
 * @author phidalgo
 * @created 20151612
 * 
 * Class used to upload a single or multiple files to a webservice.
 * 
 * Can send notification to the current user page or if the user closes the page, send it  through notification system.
 * 
 * Exists two types of render.
 * The default render (1) is with a button.
 * The FULLSCREEN render (2) is a full screen (100% width and height of their parent container) with a dropdown zone.
 * 
 * This code autoloads needed $ library.
 * 
 * @param object config
 * @param jQuery $primaryObject
 */

function idcp_fupload(config,$primaryObject){
	var _PLUGIN_NAME = "idcp.widgets.fupload";
	var fupload_library_path = "lib/external-lib/jquery.fileupload/jquery.fileupload.js";
	/**
	 * Default render type.
	 */
	var _DEFAULT = 1;
	var _FULLSCREEN = 2;
	
	//you can have multiple files, or a file input with "multiple" attribute
	var method = config.method || 'POST';
	var renderType = config.type || _DEFAULT;
	var restURL = config.restURL;
	var autoSubmit = typeof(config.autoSubmit)!=='undefined'? config.autoSubmit:true;
	var cancelButton = typeof(config.cancelButton)!=='undefined'? config.cancelButton:false;
	var fileSize = config.filesize || $i._get('fileSize');
	var extensions = (typeof(config.extensions)!=='undefined'?  config.extensions.toUpperCase():'ALL'); //String. Can be ALL, IMAGE
	var $submit = config.submit; //jQuery object to attach submit functions.
	var $reset = config.reset; //jQuery object to reset input files actions.
	var progressbar = (typeof(config.progressbar)!=='undefined'? config.progressbar:true);
	var flist = (typeof(config.flist)!=='undefined')? config.flist:false; //if you also want to see your files.
	var flistObject = null; //In case of flist enabled, flist object is here.
	var instanceToPage = $i.pages.find.visible(); //Current Page object, used to track if the user closes current page.
	var newElement = (typeof(config.newElement)!=='undefined'? config.newElement:false);
	
	var upload_start = false;
	
	var jqXHR = null;
	
	var lastStatusKnown = null; //Object which contains the last transaction status.
	var SecondsCounter = null; //The time (in seconds) taken from the transaction begin to now.
	var stostatus = null; //The setTimeOut identifier to cancel it later
	var notification_id = null //Set notification id in case of it needed
	
	var FileUploadInstance = null; //FileUpload jQuery plugin instance.
	var pluginLoaded = false;
	
	
	var uniqueID =  $i.generator();
	//If filesize is greater thant max filesize, put max filesize.
	if(fileSize > $i._get('fileSize')){
		fileSize = $i._get('fileSize');
	}
	
	var queue_file_list = []; //Array with file data for upload.
	var processed_file_count = 0;//Count files which are already uploaded.
	var current_file_uploading = null;
	
	var readOnly = false;
	
	//Sets primary object values
	var render = "";
	if(renderType==_DEFAULT){
		render = 
		'<div class="row" data-id="fupload-container-all-'+uniqueID+'" data-plugin-name="'+_PLUGIN_NAME+'" data-plugin-id="'+uniqueID+'">'+
			'<div class="col-xs-1 col-sm-5" data-id="button-container"></div>'+
			'<div class="col-xs-1 col-sm-7" data-id="status-container"></div>'+
		'</div>'+
		'<div class="row">'+
			'<div class="col-xs-1 col-sm-12" data-id="pbar-container"></div>'+
		'</div>'+
		'<div class="row">'+
			'<div class="col-xs-1 col-sm-12" data-id="fupload-files-container"></div>'+
		'</div>';
	}
	else{
		render = 
		'<div data-id="fupload-container-all-'+uniqueID+'" data-plugin="'+_PLUGIN_NAME+'">'+
			'<div class="row" >'+
				'<div class="col-xs-1 col-sm-12" data-id="pbar-container"></div>'+
			'</div>'+
			'<div class="row">'+
				'<div class="col-xs-1 col-sm-7" data-id="status-container"></div>'+
			'</div>'+
			'<div class="row">'+
				'<div class="col-xs-12 col-sm-12" data-id="dropzone-click"></div>'+
				'<!-- Put this as attribute on input file and will allow to select directories instead of files "directory webkitdirectory mozdirectory"-->'+
				'<input data-id="fuploadInput-'+uniqueID+'" type="file" name="files[]" multiple class="no-visible">'+
			'</div>'+
			'<div class="row">'+
				'<div class="col-xs-1 col-sm-5" data-id="button-container"></div>'+
			'</div>'+
			'<div class="row">'+
				'<div class="col-xs-1 col-sm-12" data-id="fupload-files-container"></div>'+
				'<span class="dz-default dz-message">'+$i.i18n('widgets:txt_click_or_merge_information')+'</span>'+
			'</div>'+
		'</div>';
		
	}
	$primaryObject.html(render);
	
	var $bt_cont = $primaryObject.find('div[data-id="button-container"]');
	var $pb_cont = $primaryObject.find('div[data-id="pbar-container"]');
	var $st_cont = $primaryObject.find('div[data-id="status-container"]');
	var $fl_cont = $primaryObject.find('div[data-id="fupload-files-container"]');
	if(renderType==_FULLSCREEN){
		var $dz_cont = $primaryObject.find('div[data-id="dropzone-click"]'); //Drop zone container.
	}
	
	//Create html objects.
	if(renderType==_DEFAULT){
		render = 
			'<span class="btn btn-success fileinput-button">'+
		    '<i class="glyphicon glyphicon-plus"></i>'+
		    '<span> '+$i.i18n('widgets:bt_select_files')+'</span>'+
		    '<!-- The file input field used as target for the file upload widget -->'+
		    '<input data-id="fuploadInput-'+uniqueID+'" type="file" name="files[]" multiple>'+
		    '</span>';
	}
	else{
		render = 
		'<div style="box-sizing:border-box;background-color:#F8F8F8;cursor:pointer;">\
			<form class="dropzone dz-clickable" style="min-height:200px;" data-id="form-container"> \
				<div class="dz-default dz-message" data-id="dz-default-message"> \
					<span>\
						<span class="bigger-150 bolder"><i class="ace-icon fa fa-caret-right red"></i> '+$i.i18n('widgets:drop_files')+'</span> '+$i.i18n('widgets:to_upload')+' \
						<span class="smaller-80 grey">'+$i.i18n('widgets:or_click')+'</span> <br /> \
						<i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i> \
					</span>\
				</div> \
			</form> \
		<div>';
		$primaryObject.css('height','80%');
	}
	var $fuploadHTML = $(render);
	
	var $fuploadPBAR = $(
	'<!-- The global progress bar -->'+
	'<div data-id="progress-'+(uniqueID)+'" class="progress no-visible" style="text-align:center;">'+
	'<div class="progress-bar progress-bar-success" style="text-align:right;">'+
	'<span class="pbar-percent"></span>'+
	'</div>'+
	'</div>');
	
	if(renderType==_DEFAULT){
		$bt_cont.html($fuploadHTML); //Add buttons and inputs
	}
	else{
		$dz_cont.html($fuploadHTML);
		var $dz_form =  $i('#form-container',$dz_cont); //DropZone form.
		var $dz_default_message = $i('#dz-default-message',$dz_form);
	}
	
	
	var listUIElements = 0;
	if(flist){
		flistObject = $i.flist({
			rest: flist.rest,
			newElement : newElement,
			onDone : function(rsp){
				$.each(rsp,function(){
					$dz_form.append(this);
				});
				if(rsp.length>0){
					$dz_default_message.hide('fast');
				}
			},
			onRemove : function(uielements,file_id){
				listUIElements = uielements;
				//console.log("Callback fupload flist.onremove:" + uielements);
				if((listUIElements +processed_file_count) == 0){
					$dz_default_message.show('fast');
				}
				if(typeof(flist.onRemove)==='function'){
					flist.onRemove(uielements,file_id);
				}
			}
		});
	}
	
	
	//Create html objects.
	var $fuploadSubmit = $('<button class="btn btn-success"><i class="fa fa-cloud-upload"></i> '+$i.i18n('widgets:bt_submit')+'</button>');
	if(!autoSubmit){$bt_cont.append($fuploadSubmit);} //Adds submit button.
	
	if(progressbar){$pb_cont.append($fuploadPBAR);} //Add progressbar.
	
	var $inputFile = $i('#fuploadInput-'+uniqueID,$primaryObject); //input type="file" jquery Object.
	
	if(renderType==_FULLSCREEN){ //if render type is FULLSCREEN adds click event on $dz_cont (drop zone container). 
		$dz_cont.on('click',function(e){
			$i('#fuploadInput-'+uniqueID,$primaryObject).click();
		});
	}
	
	
		$inputFile.on('click',function(){
			if(!upload_start){
				$fl_cont.html('');
				lastStatusKnown = null; //Object which contains the last transaction status.
				SecondsCounter = null; //The time (in seconds) taken from the transaction begin to now.
				stostatus = null; //The setTimeOut identifier to cancel it later
			}
			//queue_file_list = [];
			//processed_file_count = 0;
		});
	
	
	if(typeof($.fn.fileupload)==='undefined'){
		$i.require([fupload_library_path]).done(loadUploadHandler);
	}
	else{
		loadUploadHandler();
	}
	
	
	var fuploadinformation = '<div class="alert alert-info" data-id="fupload-information">'+
	'<button type="button" class="close" data-dismiss="alert">'+
		'<i class="ace-icon fa fa-times"></i>'+
	'</button>'+
	'<i class="ace-icon fa fa-check blue"></i>'+
	$i.i18n('widgets:information')+
	'</div>';
	
	var fuploadinformationAllUploadedSuccess = '<div class="alert alert-success" data-id="fupload-information-allsuccess">'+
	'<button type="button" class="close" data-dismiss="alert">'+
		'<i class="ace-icon fa fa-times"></i>'+
	'</button>'+
	'<i class="ace-icon fa fa-check green"></i>'+
	$i.i18n('widgets:information_all_uploaded_success')+
	'</div>';
	
	var information_all_uploaded_warning = '<div class="alert alert-warning" data-id="information_all_uploaded_warning">'+
	'<button type="button" class="close" data-dismiss="alert">'+
		'<i class="ace-icon fa fa-times"></i>'+
	'</button>'+
	'<i class="ace-icon fa fa-check yellow"></i>'+
	$i.i18n('widgets:information_all_uploaded_warning')+
	'</div>';
	
	var information_all_uploaded_error = '<div class="alert alert-danger" data-id="fupload-information_all_uploaded_error">'+
	'<button type="button" class="close" data-dismiss="alert">'+
		'<i class="ace-icon fa fa-times"></i>'+
	'</button>'+
	'<i class="ace-icon fa fa-exclamation-triangle red"></i>'+
	$i.i18n('widgets:information_all_uploaded_error')+
	'</div>';

	/**
	 * Check if user closes upload window/modal/page.
	 */
	var fileUploadRendered = function(){
		//console.log($i('#fupload-container-all-'+uniqueID,$primaryObject));
		return ($('div[data-id="fupload-container-all-'+uniqueID+'"]').length==0? false:true);
	};
	
	function beforeUnload(){
		return $i.i18n('widgets:txt_confirm_reload_app');
	}
	
	function loadUploadHandler(){
		pluginLoaded = true; //The plugin libraries are loaded.
		
		if(!autoSubmit){
			$fuploadSubmit.on('click',function(e){
				e.stopPropagation();
				e.preventDefault();
			});
		}
		/**
		 * BEGIN DECLARATION AND CODE FOR
		 * Fupload CALLBACK FUNCTIONS.
		 */
		var fupload_addF = function (e, data) {
			var acceptFileTypes = config.fileTypes;
	        if(typeof(acceptFileTypes)!=='undefined' && data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['name'])) {
	        	//MostraError(translations.err_fileType, $primaryObject);
	        	alert(translations.err_fileType+": "+data.originalFiles[0]['name']);
	        }
	        else{
        	fileAddToQueue(e,data);
			if(!autoSubmit){ //If autoSubmit is disabled.
				$fuploadSubmit.off('click');
				$fuploadSubmit.on('click',function (e) {
					e.stopPropagation();
					e.preventDefault();
					if (data.files.length > 0) {     //only submit if we have something to upload
                        data.submit();
					}
                });
			}
			else{
				if (data.files.length > 0) {     //only submit if we have something to upload
                    data.submit();
				}
			}
	        }
		};
		
		var startF = function(e){ //Called one time at the beggining of the upload process (just when the files submit().) ONE TIME CALLED.
        	$st_cont.show();
        	upload_start = true;
        	$i('.alert',$primaryObject).remove(); //Removes all previous alert dialogs
        	$primaryObject.prepend(fuploadinformation); //Adds information about system upload.
			$('#dropzone-click').append('<div class="processing_queue"><div>'+$i.i18n('widgets:txt_processing_queue')+'</div></div>');
        	 
        	/**
        	 * As the upload process start, user can go to any other app page, but user may not RELOAD or close the app page, so
        	 * so we add a beforeunload event to check if user are leaving the app.
        	 */
        	 $(window).on('beforeunload',beforeUnload);

        	 $fuploadPBAR.removeClass('no-visible'); //Shows progress bar.
        	
        	var last_eta = null;
        	function showLastKnownStatus(){
        		SecondsCounter++;
        		if(lastStatusKnown==null) return;
        		var bitrate = (lastStatusKnown.loaded/SecondsCounter/1024).toFixed(2); //Set to KB/s by default and limit to two decimals
        		var rateUnit = 'KB/s';
    			if(bitrate>1000){
    				bitrate = (bitrate/1024).toFixed(2);
    				rateUnit = 'MB/s';
    			}
    			var eta = ((lastStatusKnown.total - lastStatusKnown.loaded)/ (lastStatusKnown.loaded/SecondsCounter).toFixed(0)).toFixed(0);
    			var etaUnit = 'seconds';
    			if(eta>66){
    				eta = (eta/60).toFixed(0);
    				etaUnit = 'minutes';
    			}
    			if(last_eta == eta){
    				eta = eta++;
    			}
    			var bremaining = ((lastStatusKnown.total - lastStatusKnown.loaded)/1048576).toFixed(0);
    			last_eta = eta;
    			var dinfo = $i.i18n('widgets:txt_speed')+': '+bitrate + ' ' + rateUnit + '| '+$i.i18n('widgets:txt_bytes_remaining')+': ' + bremaining + ' MB | ETA: ' + eta + ' ' + etaUnit;
    			$st_cont.html(dinfo);
        	} 
        	stostatus = setInterval(showLastKnownStatus, 1000);
        };
		
        var beforeSendF = function(xhr, data) {
            xhr.setRequestHeader('Authorization', $i.cookies._get("session_token"));
        };
        
        var fupload_sendF = function(e,data){ //Called every time a file starts to upload. MULTI TIME CALLED.
			if(data.files.length==0) return false; //If this file is empty it means that the file has been deleted from queue list.
			console.log("START PROCESSING: " + data.files[0].id);
			current_file_uploading = queue_file_list[getIndexOfFile(data.files[0].id)];
			//console.log(queue_file_list);
			var id = current_file_uploading.id;
        	if(renderType==_FULLSCREEN){ //We also change the REMOVE event to a ABORT event.
        		$i('#preview_'+id + ' .dz-remove',$dz_form).html($i.i18n('widgets:bt_abort'))
        		$i('#preview_'+id + ' .dz-remove',$dz_form).attr('data-dz-remove','ABORT');
        	}
		};
		
		var fupload_onprogressF = function (e, data) { //A file is uploading.
			if(typeof(config.onprogress)==='function'){
				config.onprogress(progressF(data));
			}
		};
		
		var fupload_doneF = function (e, data) { //A file us successfull uploaded.
        	var id = current_file_uploading.id;
        	if(renderType==_FULLSCREEN){
        		if(flist){
        			//console.log("DELETING UPLOAD FILE ID: " + id);
        			$i('#preview_'+id).after(flistObject.renderFile(data.result.data.files[0]));
        			$i('#preview_'+id).remove();
        			//$dz_form.append(flistObject.renderFile(data.result.data.files[0]));
	        	}
        		else{
        			$i('#preview_'+id+' .dz-upload-mark',$dz_cont).addClass('no-visible');
		        	$i('#preview_'+id+' .dz-success-mark',$dz_cont).removeClass('no-visible');
		        	$i('#preview_'+id + ' .dz-remove',$dz_form).remove();
        		}
        	}
        	current_file_uploading.id_api = data.result.data.files[0].id;
        	current_file_uploading.data = data.result.data.files[0];
        	current_file_uploading.upload = true;
        	data.files.length = 0;
        	//console.log("A file was successfully upload. Download URL : " + $i.ws_url+'file/'+$i.cookies._get("session_token")+'/'+current_file_uploading.id_api);
        };
        
        var fupload_failF = function(e,data){ //A file has an error when uploading.
        	if(typeof(data.textStatus)==='undefined') return false;
        	if(data.textStatus!='abort'){
        		data.jqXHR.abort();
        		current_file_uploading.error = true;
        	}
        	else{
        		current_file_uploading.upload = false;
        	}
        	var id = current_file_uploading.id;
        	//We also change the ABORT event to a REMOVE event.
        	$i('#preview_'+id+' .dz-upload-mark',$dz_form).addClass('no-visible');
    		$i('#preview_'+id +' .dz-error-mark',$dz_form).removeClass('no-visible').css('display','block');
    		$i('#preview_'+id + ' .dz-remove',$dz_form).html($i.i18n('widgets:remove_file_from_queue'))
    		$i('#preview_'+id + ' .dz-remove',$dz_form).attr('data-dz-remove','REMOVE');
        	
        };
        
        var queueProcessFinishF = function (e) {
        	if(!fileUploadRendered()){
				$i.notification.update(notification_id,{title : 'File Upload successfull',icon : 'fa-check',link : instanceToPage.urlHash,badge : processed_file_count,flag: true})
			}
        	/**
        	 * As the upload process ends we remove the beforeunload event to check if user are leaving the app.
        	*/
        	$(window).off('beforeunload',beforeUnload);
        	$i('.alert',$primaryObject).remove(); //Removes all previous alert dialogs
        	
        	var error_n = 0;
        	var success_n = 0;
        	$.each(queue_file_list,function(){
        		if(this.error){
        			error_n++;
        		}
        		if(this.upload && !this.error){
        			success_n++;
        		}
        	});
        	
        	if(error_n!=0){
        		//console.log("error_n: " + error_n + ", processed_file_count: " + processed_file_count);
        		if(error_n!=processed_file_count){
            		$primaryObject.prepend(information_all_uploaded_warning);
        		}
        		else{
            		$primaryObject.prepend(information_all_uploaded_error);
        		}
        	}
        	else{
        		$primaryObject.prepend(fuploadinformationAllUploadedSuccess);
        	}
        	
        	if(progressbar){
				$i('#progress-'+uniqueID+' .progress-bar',$primaryObject).css('width','0%');
				$i('#progress-'+uniqueID+' .pbar-percent',$primaryObject).html('');
			}
        	$fuploadPBAR.addClass('no-visible'); //Shows progress bar.
        	upload_start = false;
        	
        	clearInterval(stostatus); //Clear status interval.
			$st_cont.hide("fast",function(){ //Clear and hide status information.
				$st_cont.hide('slow',function(){
					$st_cont.html('');
				});
			});
        	$(window).off('beforeunload',beforeUnload); //Unbind beforeunload event.
        	
        	
            
            if(newElement){
            	
            	var ids = "";
            	$.each(queue_file_list,function(){
            		if(this.upload){
            			ids +=this.id_api+',';
            		}
            	});
            	if(ids.endsWith(',')){
            		ids = ids.substring(0, ids.length - 1);
            	}
            	var $ih = $('<input type="hidden" name="attachments" value="'+ids+'" data-name="attachments" data-ischanged="true" />');
    			$primaryObject.append($ih);
    			$ih.trigger('change');
            }
            
           
            $('#dropzone-click .processing_queue').remove(); //Eliminem Div processant...
            
           	if(typeof(config.ondone)==='function') config.ondone(queue_file_list); //Execute ALLDONE front end callback. 
           	//console.log("All files are processed"); 

            processed_file_count = 0;
            queue_file_list = [];
            queue_file_list.length = 0;
            
           
            
            
        };
        
        var alwaysF = function(e,data){ //Always fired done/fail.
        	data.files.length = 0; //Delete that file from queue.
        	processed_file_count++;
        	queue_file_list[getIndexOfFile(current_file_uploading.id)] = current_file_uploading;
            if(typeof(config.onfiledone)==='function'){ //Execute onfiledone front end callback
        		config.onfiledone(data.result.data);
        	}
        };
        
		var progressAllF = function (e, data) { //Progress of ALL files.
        	var progress = ((data.loaded / data.total)*100).toFixed(2);
        	if(fileUploadRendered()){
        		$i('#progress-'+uniqueID+' .progress-bar',$primaryObject).css('width',progress + '%');
	        	$i('#progress-'+uniqueID+' .pbar-percent',$primaryObject).html(progress+'%');
        	}
        	else{ //If not, the window has been closed so we will add a notification.
        		if(notification_id!=null){
        			$i.notification.update(notification_id,{title : 'Uploading files... <b>' + progress + '%</b>',badge : processed_file_count})
        		}
        		else{
        			notification_id = $i.notification.add({title : 'Uploading files... <b>' + progress + '%</b>',icon : 'fa-cloud-upload',badge : '1', link : instanceToPage.urlHash});
        		}
        	}
        	 
        	 var status = progressF(data);
        	 lastStatusKnown = status;
        	 if(typeof(config.onprogressall)==='function'){
 				config.onprogressall(progressF(data));
 			}
        };
        
        var chunksendF = function (e,data){
        	//http://stackoverflow.com/questions/20079264/jquery-file-upload-formdata-not-updating
        	if(current_file_uploading.id_api != null){
        		// and append UploadId
        	    // Yes, append, not override
        		data.data.append('fid', current_file_uploading.id_api);
        	}
        }
		
        var chunkdoneF = function(e,data){ //Called when receive a chunk response.
        	if(data.result.data.files[0].id){
        		//console.log('FILE ID ON CHUNKED: ' + data.result.data.files[0].id);
        		current_file_uploading.id_api = data.result.data.files[0].id;
        	}
        };
        /**
		 * END DECLARATION AND CODE FOR
		 * Fupload CALLBACK FUNCTIONS.
		 */
        
        
        /**
         * BEGIN HELP FUNCTIONS.
         * This functions are called by Fupload CALLBACK FUNCTIONS.
         */
        
        /**
         * Adds UI file render.
         */
        var fileAddToQueue = function (e, data) { //Called when a file is added to the upload queue list. Triggers when click or drag.
			$.each(data.files, function (index, file){
				file.error = false;
				file.upload = false;
				file.abort = false;
				file.id = $i.generator();
				file.id_api = null; //This is the file api token sent on the first return (chunked or not).
				queue_file_list.push(file);
				console.log("Add to queue: " + file.id);
            	if(renderType==_DEFAULT){
            		$fl_cont.append('<p>'+file.name+'</p>')
            	}
            	else{
            		var $render = $('\
            		<div data-id="preview_'+file.id+'" class="dz-preview dz-file-preview dz-processing dz-success"> \
            			<div class="dz-details">\
            				<div class="dz-filename"><span data-dz-name="'+file.name+'">'+file.name+'</span></div>\
            				<div class="dz-size" data-dz-size=""><strong>'+(file.size/1024).toFixed(0)+'</strong> KiB</div>\
            			</div>\
            			<div class="progress progress-small progress-striped active">\
            				<div class="progress-bar progress-bar-success" data-dz-uploadprogress="" style="width: 100%;"></div>\
            			</div>\
            			<div class="dz-success-mark no-visible"><span></span></div>\
            			<div class="dz-error-mark  no-visible"><span></span></div>\
            			<div class="dz-upload-mark"><span><i class="fa fa-cloud-upload blue"></i></span></div>\
            			<div class="dz-error-message"><span data-dz-errormessage=""></span></div>\
            			<a class="dz-remove" href="javascript:undefined;" data-dz-remove="REMOVE">'+$i.i18n('widgets:remove_file_from_queue')+'</a>\
            		</div>');
            		$dz_form.append($render);
            		$render.on('click',function(){ return false;}); //Prevent click on input object.
            		$i('.dz-remove',$render).on('click',function(){ //Function to remove a file from queue list
            			$parent = $(this).parent();
            			if($(this).attr('data-dz-remove')=='REMOVE'){ //If REMOVE from upload.
            				fileRemoveFromQueue($parent,data);
            			}
            			else{ //If abort uploading...
            				fileAbortUpload($parent,data);
            			}
            		});
            		if(!$dz_form.hasClass('dz-started')){
            			$dz_form.addClass('dz-started');
            		}
            	}
			});
			if(renderType==_FULLSCREEN){
				$i('#dz-default-message',$dz_form).hide('fast');
			}
			//console.log("filequeueadd queue_file_list.length: " + queue_file_list.length);
		}
		
		/**
		 * Can accept both id and name
		 */
		var getIndexOfFile = function (id){
			for(var i=0,j=queue_file_list.length;i<j;i++){
				var f = queue_file_list[i];
				if(f.id==id || f.name == id){
					return i;
				}
			}
			return null;
		};
		
		/**
		 * When a file is uploading, this function is called to abort it.
		 * Once the file is canceled, if we have the file ID; we make a DELETE request to WS to delete it.
		 */
		function fileAbortUpload($jqo,data){ //Called when a file is aborted. Only available when the file is uploading.
			data.abort();
			if(current_file_uploading.id_api!=null){
				$i.promise._request({method :'DELETE',restURL : 'file/'+current_file_uploading.id_api});
			}
			fileRemoveFromQueue($jqo, data);
	        //data.submit = function () { return false; };
		}
		
		/**
		 * Remove a file from UI and for FUPLOAD QUEUE.
		 */
		function fileRemoveFromQueue($jqo,data){ //Called when a file is removed from queue. Only available when the file is in queue, not uploading.
			data.files.length = 0;//zero out the files array
			var id = $jqo.data('id').replace('preview_','');
			var fileObj = queue_file_list[getIndexOfFile(id)];
			if(fileObj!=null){
				fileObj.abort = true;
				fileObj.error = false;
				fileObj.upload = false;
			}
			//queue_file_list.splice(getIndexOfFile(id), 1);
			//processed_file_count--;
			console.log("Delete from queue: " + id);
			$jqo.hide('fast',function(){ //Callback function when hides.
				$jqo.remove();
				if(!checkIfFilePreview() && renderType==_FULLSCREEN){
					$dz_form.removeClass('dz-started');
					$i('#dz-default-message',$dz_form).show('fast');
				}
			});
			     
		}
		/**
         * BEGIN HELP FUNCTIONS.
         * This functions are called by Fupload CALLBACK FUNCTIONS.
         */
        
        /**
         * Upload file object.
         */
        var uploadFile_obj = {
			sequentialUploads: true,
			url: $i._get('api')+restURL,
	        maxChunkSize: $i._get('chunkSize'),
	        dataType: 'json',
	        add : fupload_addF, //When a file is added to queue.
	        start : startF, //When upload progress start.
	        beforeSend: beforeSendF, //When each file is ready to send (before)
	        send : fupload_sendF, //When each file is start to send
	        progress: fupload_onprogressF, //When each file is uploading.
	        done: fupload_doneF, //When each file is successfull sent.
	        fail : fupload_failF, //When each file fails sent
	        always : alwaysF, //When each file ALWAYS on done or fail.
	        progressall: progressAllF, //Overal upload progress.
	        chunksend : chunksendF,
	        chunkdone : chunkdoneF, //When each part (chunk) of each file is done.
	        stop : queueProcessFinishF
		};
        
        FileUploadInstance = $inputFile.fileupload(uploadFile_obj); //Creates FILE UPLOAD OBJECT with uploadFile_obj properties.

		/**
		 * Check if there is any File Preview on Rendered UI.
		 * @returns true:false.
		 */
		function checkIfFilePreview(){
			return ($i('.dz-preview',$dz_form).length>0? true:false);
		}
			
		/**
		 * Returns upload status, bitrate, eta, percent uploaded...
		 * returns {
				rate : rate,
				rateUnit : rateUnit,
				eta : eta,
				etaUnit : etaUnit,
				progress : progress,
				loaded : data.loaded,
				total : data.total,
			}
		 */
		function progressF(data){
			var progress = ((data.loaded / data.total)*100).toFixed(2);
        	var rate = (data.bitrate/1024);
			var rateUnit = 'KB/s';
			if(rate>1000){
				rate = rate/1024;
				rateUnit = 'MB/s';
			}
			rate = rate.toFixed(2);
			
			var eta = ((data.total - data.loaded) * 8 / data.bitrate).toFixed(0);
			var etaUnit = 'seconds';
			if(eta>66){
				eta = (eta/60).toFixed(2);
				etaUnit = 'minutes';
			}

			var rsp = {
				rate : rate,
				rateUnit : rateUnit,
				eta : eta,
				etaUnit : etaUnit,
				progress : progress,
				loaded : data.loaded,
				total : data.total,
			};
			
			if((typeof(data.files)!=='undefined') && typeof(data.files[0])!=='undefined' && typeof(data.files[0].name)!=='undefined'){
				rsp.file =  data.files[0].name;
			}
			return rsp;
		}
	}

	var iid = null;
	var readOnlyF = this.readOnly = function(ro){
		if(!pluginLoaded){
			iid = setInterval(function(){
				readOnlyF(ro);
			},50);
		}
		else{
			if(iid!=null){
				clearInterval(iid);
			}
			//console.log("POSEM A READ ONLY el plugin "+_PLUGIN_NAME+" amb ID: "+uniqueID);
			readOnly = ro;
			if(flistObject!=null){
				flistObject.readOnly(ro);
			}
			//$inputFile.fileupload('destroy');
		}
	}
	
	var page = $i.pages.find.page($primaryObject.closest('div[data-pagehash]').data('pagehash'))
	if(page!==false){
		page.plugins(_PLUGIN_NAME,this);
	}
	else{
		console.log("Page object not found");
	}
}

$i.fupload = function(config){
	return new idcp_fupload(config,config.$primaryObject);
};

$.fn.extend({
	fupload : function(config){
		return new idcp_fupload(config,$(this));
	}
});