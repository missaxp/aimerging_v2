(function(){
	function idcp_avatar(avatar,$primaryObject){
		var _PLUGIN_NAME = 'idcp.widgets.avatar';
		var pluginLoaded = false;
		
		var page = $i.pages.find.page($primaryObject.closest('div[data-pagehash]').data('pagehash'));
		
		var editable_library_path = ['assets/js/ace/elements.fileinput.js','lib/css/bootstrap-editable.css','lib/external-lib/bootstrap-editable.min.js'];
		if(typeof($.fn.editable)==='undefined'){
			$i.require(editable_library_path).done(function(){
				/**
				Image editable input.
				**/
				(function ($) {
				    "use strict";
				    var Image = function (options) {
				        this.init('image', options, Image.defaults);

						if('on_error' in options.image) {
							this.on_error = options.image['on_error'];
							delete options.image['on_error']
						}
						if('on_success' in options.image) {
							this.on_success = options.image['on_success'];
							delete options.image['on_success']
						}
						if('max_size' in options.image) {
							this.max_size = options.image['max_size'];
							delete options.image['max_size']
						}

						this.initImage(options, Image.defaults);
				    };

				    //inherit from Abstract input
				    $.fn.editableutils.inherit(Image, $.fn.editabletypes.abstractinput);

				    $.extend(Image.prototype, {
						initImage: function(options, defaults) {
				          this.options.image = $.extend({}, defaults.image, options.image);
						  this.name = this.options.image.name || 'editable-image-input';
				        },

				        /**
				        	Renders input from tpl
				        	@method render() 
				        **/        
				        render: function() {
							var self = this;
							this.$input = this.$tpl.find('input[type=hidden]:eq(0)');
							this.$file = this.$tpl.find('input[type=file]:eq(0)');

							this.$file.attr({'name':this.name});
							this.$input.attr({'name':this.name+'-hidden'});
							
							
							this.options.image.allowExt = this.options.image.allowExt || ['jpg', 'jpeg', 'png', 'gif'];
							this.options.image.allowMime = this.options.image.allowMime || ['image/jpg', 'image/jpeg', 'image/png', 'image/gif'];
							this.options.image.maxSize = self.max_size || this.options.image.maxSize || false;
							
							this.options.image.before_remove = this.options.image.before_remove || function() {
								self.$input.val(null);
								return true;
							}

							this.$file.ace_file_input(this.options.image).on('change', function(){
								var $rand = (self.$file.val() || self.$file.data('ace_input_files')) ? Math.random() + "" + (new Date()).getTime() : null;
								self.$input.val($rand)//set a random value, so that selected file is uploaded each time, even if it's the same file, because inline editable plugin does not update if the value is not changed!
							}).closest('.ace-file-input').css({'width':'150px'}).closest('.editable-input').addClass('editable-image');
							
							this.$file
							.off('file.error.ace')
							.on('file.error.ace', function(e, info) {
								if( !self.on_error ) return;
								if( info.error_count['ext'] > 0 || info.error_count['mime'] > 0 ) {
									//wrong ext or mime?
									self.on_error(1);
								}
								else if( info.error_count['size'] > 0 ) {
									//wrong size
									self.on_error(2);
								}
							});
						}

				    });

					
				    Image.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
				        tpl: '<span><input type="hidden" /></span><span><input type="file" /></span>',
				        inputclass: '',
						image:
						{
							style: 'well',
							btn_choose: 'Change Image',
							btn_change: null,
							no_icon: 'fa fa-picture-o',
							thumbnail: 'large'
						}
				    });

				    $.fn.editabletypes.image = Image;
				    
				    $.fn.editableform.buttons  = '<button type="submit" class="btn btn-primary btn-sm editable-submit"><i class="fa fa-check"></i></button>' +
				    '<button type="button" class="btn btn-default btn-sm editable-cancel"><i class="fa fa-times"></i></button>';


				}(window.jQuery));
				
				loadWidget();
			});
		}
		else{
			loadWidget();
		}
		
		
		
		//var $imageObject;
		function loadWidget(){
			pluginLoaded = true;
			var uniqueID = $i.generator();
			var aid = 'avatar_'+uniqueID;
			var avatar_max_size = 11000000; //bytes
			var defaultAvatarImage = '../assets/avatars/defaultImage.png';
			
			var originalID = $primaryObject.data('id');
			$primaryObject.attr('data-id',aid);
			
			var $htmlObj = $('<span class="profile-picture">\
			<img data-id="'+aid+'" class="editable img-responsive editable-click editable-empty" alt="" src="'+defaultAvatarImage+'" \
			data-plugin-name="'+_PLUGIN_NAME+'" data-plugin-id="'+uniqueID+'" style="display: block;"></img>\
			</span><input type="hidden" name="'+originalID+'" data-id="'+originalID+'" value="" />');
			
			$primaryObject.html($htmlObj);
			$imageObject = $htmlObj.find('img[data-id="'+aid+'"]');
			$inputObject = $primaryObject.find('input[data-id="'+originalID+'"]');
			
			if(avatar!=null){
				$imageObject.attr("src","data:image/gif;base64,"+avatar);
				$inputObject.attr("value",avatar);
			}
			
			$inputObject.on("change",function(e,data){
				if(typeof(data)!=='undefined' && typeof(data.thumb)!=='undefined'){ //User adds a diferent avatar.
					$imageObject.get(0).src = data.thumb;
					var a  = data.thumb.substring(data.thumb.indexOf(",") + 1);
					$inputObject.attr("value",a);
					page.$.trigger('idcp.widgets.trigger.change',{name : _PLUGIN_NAME, id: uniqueID, $element: $inputObject});
				}
				else{ //Form trackchanges is restoring to the default value.
					if($inputObject.attr("data-default-value")==""){
						$imageObject.get(0).src = defaultAvatarImage;
						$inputObject.attr("value","");
					}
					else{
						$imageObject.get(0).src = "data:image/gif;base64,"+$inputObject.attr("data-default-value");
						$inputObject.attr("value",$inputObject.attr("data-default-value"));
					}
				}
			});
			
			page.$.trigger('idcp.widgets.trackchanges',{name : _PLUGIN_NAME, id: uniqueID, $element: $inputObject});
			
			// *** editable logotype *** //
			try {//ie8 throws some harmless exception, so let's catch it

				//it seems that editable plugin calls appendChild, and as Image doesn't have it, it causes errors on IE at unpredicted points
				//so let's have a fake appendChild for it!
				if( /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ) Image.prototype.appendChild = function(el){}

				var last_gritter
				$imageObject.editable({
					type: 'image',
					name: aid,
					placement: 'bottom',
					value: null,
					image: {
						//specify ace file input plugin's options here
						btn_choose: $i.i18n('widgets:bt_choose_avatar'),
						droppable: true,
						/**
						//this will override the default before_change that only accepts image files
						before_change: function(files, dropped) {
							return true;
						},
						*/

						//and a few extra ones here
						name: aid,//put the field name here as well, will be used inside the custom plugin
						max_size: avatar_max_size,//~100Kb
						on_error : function(code) {//on_error function will be called when the selected file has a problem
							if(last_gritter) $.gritter.remove(last_gritter);
							if(code == 1) {//file format error
								last_gritter = $i.gritter({
									type: "warning",
									title: $i.i18n('widgets:title_avatar_is_not_an_image'), //'File is not an image!',
									description: $i.i18n('widgets:text_select_an_image') //'Please choose a jpg|gif|png image!',
								});
							} else if(code == 2) {//file size rror
								last_gritter = $i.gritter({
									type: "warning",
									title: $i.i18n('widgets:title_avatar_too_big'), //'File too big!',
									description: $i.i18n('widgets:text_image_size_info ',{size : (avatar_max_size/1024).toFixed(0)}) //'Image size should not exceed 100Kb!',
								});
							}
							else {//other error
							}
						},
						on_success : function() {
							$.gritter.removeAll();
						}
					},
				    url: function(params) {
				    	var deferred = null;

				    	//if value is empty (""), it means no valid files were selected
				    	//but it may still be submitted by x-editable plugin
				    	//because "" (empty string) is different from previous non-empty value whatever it was
				    	//so we return just here to prevent problems
				    	var value = $imageObject.next().find('input[type=hidden]:eq(0)').val();
				    	if(!value || value.length == 0) {
				    		deferred = new $.Deferred
				    		deferred.resolve();
				    		return true;
				    	}

				    	var $form = $imageObject.next().find('.editableform:eq(0)')
				    	var file_input = $form.find('input[type=file]:eq(0)');
				    	var ie_timeout = null
				    	
						// ***UPDATE AVATAR HERE*** //
						//You can replace the contents of this function with examples/profile-avatar-update.js for actual upload
				    	if( "FormData" in window ) {
				    		var formData_object = new FormData();//create empty FormData object
				    		//and then add files
				    		$form.find('input[type=file]').each(function(){
				    			var field_name = 'avatar';
				    			var files = $(this).data('ace_input_files');
				    			if(files && files.length > 0) {
				    				formData_object.append('avatar', files[0]);
				    			}
				    		});
				    	}
				    	
				    	if("FileReader" in window) {
							//for browsers that have a thumbnail of selected image
							var thumb = $imageObject.next().find('img').data('thumb');
							if(thumb){
								$inputObject.trigger("change",{ thumb : thumb});
							}
						}
				    	
				    	return true;
					},
					
					success: function(response, newValue) {
					}
				});
			}catch(e) {
				console.log(e);
			}
		}
		
		var iid = null;
		var readOnlyF = this.readOnly = function(ro){
			readOnly = ro;
			if(!pluginLoaded){
				if(iid==null){
					iid = setInterval(function(){
						readOnlyF(ro);
					},50);
				}
			}
			else{
				if(iid!=null){
					clearInterval(iid);
				}
				//console.log("POSEM A READ ONLY el plugin "+$imageObject.data('plugin-name')+" amb ID: "+$imageObject.data('plugin-id'));
				$imageObject.unbind(); //Deshabilitem el plugin ($.raty('readOnly',true); no funciona).
				
			}
		}
		
		if(page!==false){
			page.plugins(_PLUGIN_NAME,this);
		}
		else{
			console.log("Page object not found");
		}
	}
	
	/**
	 * Add to $ plugin.
	 */
	$.fn.extend({
		avatar : function(config){
			return new idcp_avatar(config,$(this));
		}
	});
})();

