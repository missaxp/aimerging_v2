/**
 * Implementació TinyMCE per a treballar amb jQuery sense llibreries addicionals.
 * Com tots els widgets a on es necessita, aquest widget carrega dinàmicament les llibreries el 1er cop que es crida.
 */

function idcp_richTextEditor(config,$element){
	if($element.length==0) return;
	
	var page = $i.pages.find.page($element.closest('div[data-pagehash]').data('pagehash'));
	var wysiwyg_library_path = ["lib/external-lib/tinymce/tinymce.min.js"];
	var _PLUGIN_NAME = "idcp.widgets.wysiwyg";
	var pluginLoaded = false;
	var readOnly = false;
	
	if(typeof(tinymce)==='undefined'){
		$i.require(wysiwyg_library_path).done(function(){
			tinyMCE.baseURL = "lib/external-lib/tinymce/"; // trailing slash important
			loadWidget();
		});
	}
	else{
		loadWidget();
	}

	function loadWidget(){
		$element.each(function(){
			var uniqueID = $i.generator();
			$(this).attr('data-plugin-name',_PLUGIN_NAME);
			$(this).attr('data-plugin-id',uniqueID);
			// console.log($(this).data('id'));
			tinymce.init({
				selector : '[data-id="'+$(this).data('id')+'"]', //CSS selector.
				// selector: '.wysiwyg-editor',	
				language : $i.i18n.currentLang,
				readonly : readOnly,
				convert_urls : false, // Evita que canvii les urls relatives 
				allow_script_urls: true,
				plugins: [ //compat3x 
			         "advlist autolink lists charmap print preview hr anchor pagebreak",
			         "searchreplace wordcount visualblocks visualchars code insertdatetime nonbreaking",
			         "save table contextmenu directionality emoticons paste textcolor colorpicker"
			   ],
				menubar: false,  // removes the menubar
				toolbar: 'newdocument | undo redo | styleselect formatselect removeformat | forecolor backcolor | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | blockquote charmap hr | table | unlink | copy paste pastetext | print preview code',
				setup : function(ed) {
					ed.on('init', function() {
					      //all your after init logics here.
						 $(this.targetElm).on('trackchanges:updated',function(){
							 ed.setContent($(this).html());
						 });
					  });
					ed.on('change keyup',function(){
						$(this.targetElm).html(this.getContent());
						$(this.targetElm).trigger('change');
					})
			   }
			});
		});
		pluginLoaded = true;
	}
	
	var iid = null;
	var readOnlyF = this.readOnly = function(ro){
		readOnly = ro;
		if(!pluginLoaded){
			if(iid==null){
				iid = setInterval(function(){
					readOnlyF(ro);
				},50);
			}
		}
		else{
			if(iid!=null){
				clearInterval(iid);
			}
			$element.each(function(){
				//console.log("POSEM A READ ONLY el plugin "+$(this).data('plugin-name')+" amb id: "+$(this).data('plugin-id')+", amb tag#id: "+$(this).attr('id'));
				tinymce.EditorManager.execCommand("mceRemoveEditor", true, $(this).attr('id'));
			});
		}
	}
	
	if(page!==false){
		page.plugins(_PLUGIN_NAME,this);
	}
	else{
		console.log("Page object not found");
	}
};

$i.rte = function(config){

	return new idcp_richTextEditor(config,config.content);
}

$.fn.extend({
	rte : function(config){
		// console.log(config);
		return new idcp_richTextEditor(config,$(this));
	}
});