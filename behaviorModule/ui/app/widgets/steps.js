function idcp_wizard(config,$obj){
	if(typeof($.fn.wizard)==='undefined'){
		$i.require(["assets/js/fuelux/fuelux.wizard.js"]).done(load);
	}
	else{
		load();
	}

	function load(){
		if(config.buttons){
			config.buttons.addClass('wizard-actions');
		}
		$obj.wizard();
		
		if(config.option){
			$obj.wizard(config.option.item,config.option.value);
		}
		
		if(config.remove){
			$obj.wizard('removeSteps',config.remove,1);
		}
		
		
		if(typeof(config.onFinish)==='function'){
			$obj.on('finished.fu.wizard',config.onFinish);
		}
		
		if($.isArray(config.steps)){
			$obj.on('actionclicked.fu.wizard',function(evt,data){
				$.each(config.steps,function(){
					if(this.step==data.step && this.direction==data.direction){
						this.action(evt);
					}
				});
			});
		}
	}
}

$i.steps = function(config){
	return new idcp_wizard(config,config.$obj);
}

$.fn.extend({
	steps : function(config){
		return new idcp_wizard(config,$(this));
	}
});