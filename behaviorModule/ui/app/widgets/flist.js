function idcp_flist(config,$primaryObject){
	var restURL = config.rest;
	var callbackOnDone = typeof(config.onDone)==='function'? config.onDone:null;
	var callbackonRemove = typeof(config.onRemove)==='function'? config.onRemove:null;
	var returnElements = [];
	var totalUIElements = 0;
	var readOnly = false;
	var newElement = (typeof(config.newElement)==='boolean'? config.newElement:false); 
	
	this.readOnly = function(ro){
		readOnly = ro;
	}
	

	if(callbackOnDone==null){
		var ui = '\
		<div class="col-xs-12 col-sm-12" data-id="dropzone-click">\
			<div style="box-sizing:border-box;background-color:#F8F8F8;cursor:pointer;">\
				<form class="dropzone dz-clickable dz-started" style="min-height:200px;" data-id="flist-container">\
				</form>\
			<div>\
		</div>';
		$primaryObject.html(ui);
		var $flcont = $i('#flist-container',$primaryObject);
	}
	
	if(!newElement){
		$i.promise._request('GET',restURL).done(function(rsp){
			loadUI(rsp.data.results);
		});
	}
	
	
	function loadUI(files){
		$.each(files,function(){
			if(callbackOnDone==null){
				$flcont.append(renderFile(this));
			}
			else{
				returnElements.push(renderFile(this));
			}
			totalUIElements++;
		});
		if(callbackOnDone!=null){
			callbackOnDone(returnElements);
		}
	}
	
	var renderFile = this.renderFile = function(file){
		var fileIcon = getFileIcon(file.type);
		var render = '\
			<div data-id="preview_'+file.id+'" data-file-id="'+file.id+'" class="dz-preview dz-file-preview dz-processing dz-success"> \
			<div data-id="file_details">\
				<div class="dz-details">\
					<a href="'+($i._get('api')+'file/'+$i.cookies._get("session_token")+'/'+file.id)+'">\
						<div class="dz-filename" style="cursor:pointer;">\
							<span style="z-index:2;" data-dz-name="'+file.name+'">'+file.name+'</span>\
							<div class="dz-file-mark">\
								<i  class="fa fa-cloud-download fa-4x"></i>\
							</div>\
						</div>\
					</a>\
					<div class="dz-size" data-dz-size=""><strong>'+(file.file_size/1024).toFixed(0)+'</strong> KiB</div>\
				</div>\
				<div class="dz-icon-mark"><span><i class="fa '+fileIcon+' red"></i></span></div>\
				<div class="dz-error-message"><span data-dz-errormessage=""></span></div>\
				<div class="pull-right">';
					if(!readOnly){
						render+= '<span data-action="EDIT"><i style="cursor:pointer;" class="fa fa-pencil fa-lg"></i></span>\
						<span data-action="DELETE"><i style="cursor:pointer;" class="fa fa-trash fa-lg"></i></span>';
					}
				render +='\
				</div>\
			</div>\
			<div class="dz-details" data-id="file_comments" style="display:none;">\
				<textarea class="dz-details" data-id="textarea_comments" data-file-id="'+file.id+'" style="width: 100%; height: 100%;font-size:12px;margin-bottom:0px;">'+(file.comments!=null? file.comments:"")+'</textarea>\
				<br />\
				<br />\
				<div class="pull-left">\
					<span data-action="SAVE"><i style="cursor:pointer;" class="fa fa-check fa-2x green"></i></span>\
				</div>\
				<div class="pull-right">\
					<span data-action="CANCEL"><i style="cursor:pointer;" class="fa fa-times fa-2x red"></i></span>\
				</div>\
			</div>\
			<div class="dz-details" data-id="file_delete_confirm" style="display:none;">\
				<div class="dz-filename" style="width: 100%; height: 100%;"><span>'+$i.i18n('sureDeleteResource')+'</span></div>\
				<br />\
				<div class="pull-left">\
					<span data-action="YES_DELETE"><i style="cursor:pointer;" class="fa fa-check fa-2x green"></i></span>\
				</div>\
				<div class="pull-right">\
					<span data-action="CANCEL"><i style="cursor:pointer;" class="fa fa-times fa-2x red"></i></span>\
				</div>\
			</div>\
		</div>';
		var $render = $(render);
		
			if(file.comments!=null && file.comments!=""){
				$render.attr('title',file.comments);
				$render.tooltip();
			}
			$render.on('click',function(event){ //To prevent click to container.
				event.stopPropagation();
			});
			
			$render.find('span[data-action!=""]').on('click',function(){
				var action = $(this).data('action');
				var $parent = $(this).parent().parent().parent();
				var fid = $parent.data('file-id');
				//console.log("Action: " + action + ", on file id: " + fid);
				if (action == "EDIT"){
					var $filed =$parent.find('div[data-id="file_details"]');
					var $filec = $parent.find('div[data-id="file_comments"]');
					$parent.addClass('canrotate');
					$parent.addClass('flip');
					var st = setTimeout(function(){
						var ht = $parent.css('height');
						var wt = $parent.css('width');
						$parent.css('width',wt);
						$parent.css('height',ht);
						$filed.hide();
						$filec.show();
						$parent.removeClass('canrotate');
						$parent.removeClass('flip');
						clearTimeout(st);
					}, 200);
				}
				if(action == "SAVE"){
					var $textc = $parent.find('textarea[data-id="textarea_comments"]');
					var $filed =$parent.find('div[data-id="file_details"]');
					var $filec = $parent.find('div[data-id="file_comments"]');
					//console.log('$filec.val(): ' + $filec.val());
					var value = $textc.val().trim();
					$i.promise._PUT({restURL : 'file/'+fid,data : {comment : value}, bsend : $parent}).done(doInverseRotate);
					
					function doInverseRotate(){
						$parent.addClass('canrotate');
						$parent.addClass('flip');
						var st = setTimeout(function(){
							$filed.show();
							$filec.hide();
							$parent.removeClass('canrotate');
							$parent.removeClass('flip');
							$parent.attr('title',value);
							$parent.removeAttr('data-original-title');
							$parent.attr('data-original-title',value);
							$parent.tooltip();
							clearTimeout(st);
						}, 200);
					}
				}
				if(action == "CANCEL"){
					var $filed =$parent.find('div[data-id="file_details"]');
					var $filec = $parent.find('div[data-id="file_comments"]');
					var $filer = $parent.find('div[data-id="file_delete_confirm"]');
					$parent.addClass('canrotate');
					$parent.addClass('flip');
					var st = setTimeout(function(){
						$filed.show();
						$filec.hide();
						$filer.hide();
						$parent.removeClass('canrotate');
						$parent.removeClass('flip');
						clearTimeout(st);
					}, 200);
				}
				if(action == "DELETE"){
					var $filed =$parent.find('div[data-id="file_details"]');
					var $filer = $parent.find('div[data-id="file_delete_confirm"]');
					$parent.addClass('canrotate');
					$parent.addClass('flip');
					var st = setTimeout(function(){
						$filer.show();
						$filed.hide();
						$parent.removeClass('canrotate');
						$parent.removeClass('flip');
						clearTimeout(st);
					}, 200);
				}
				if(action == "YES_DELETE"){
					$i.promise._DELETE({restURL : 'file/'+fid, bsend : $parent})
					.done(function(){
						$parent.hide('fast',function(){
							totalUIElements--;
							if(typeof(callbackonRemove)==='function'){
								callbackonRemove(totalUIElements,$parent.data('file-id'));
							}
							$parent.remove();
						});
					});
				}
			});
			//$dz_form.append($render);
		return $render;
	}
	
	var getFileIcon = function(type){
		switch(type){
			case 'application/x-zip-compressed':
				return 'fa-file-archive-o';
			case "application/pdf":
				return 'fa-file-pdf-o';
			case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
				return 'fa-file-excel-o';
			case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
				return 'fa-file-word-o';
			case "video/x-flv":
				return 'fa-film';
			case "image/png":
			case "image/gif":
				return 'fa-file-image-o';
			case "application/vnd.ms-visio.viewer":
				return 'fa-file-powerpoint-o';
			case "text/xml":
			case 'application/x-msdownload':
			default:
				return 'fa-file-o';
		}
		
	}
	
}

$i.flist = function(config){
	return new idcp_flist(config,config.$primaryObject);
};

$.fn.extend({
	flist : function(config){
		return new idcp_flist(config,$(this));
	}
});