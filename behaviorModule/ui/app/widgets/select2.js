/**
 * jQuery chosen property implementation for IDCP.
 */
(function(){
	function idcp_select(config,$objects){
		$objects.each(function(){
			config = {};
			config.width = '100%';
			var type = $(this).data('select');
			switch(type){
				case 'no-search':
					config.minimumResultsForSearch =  Infinity;
					break;
				case 'create' :
					config.tags = true;
					break;
				case 'ajax' : 
					var url = $(this).data('url');
					 function formatRepo (repo) {
				     	if (repo.loading) return repo.text;
				     	
				     	console.log(repo);
				     	var markup = "<div class='select2-result-repository clearfix'>";
				     	if(repo.avatar==null){
				     		markup+='<div class="select2-result-repository__avatar"><i class="fa fa-user fa-4x"></i></div>';
				     	}
				     	else{
				     		markup+="<div class='select2-result-repository__avatar'><img src='data:image/gif;base64," + repo.avatar + "' /></div>";
				     	}
				        
				     	 markup +="<div class='select2-result-repository__meta'>";
				     	 markup +="<div class='select2-result-repository__title'>"+repo.name + "</div>";

						 if (repo.description) {
							 markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
						 }
	
					     markup += "<div class='select2-result-repository__statistics'>";
					     if(repo.email){
					    	 markup +="<div class='select2-result-repository__stargazers'><i class='fa fa-envelope-o'></i> " + repo.email + "</div>";
					     }
					     
					     if(repo.phone){
					    	 markup +="<div class='select2-result-repository__watchers'><i class='fa fa-phone'></i> " + repo.phone + "</div>";
					     }
					     
					     "</div>" +
					     "</div></div>";
					     
	    		         return markup;
				     }

    				var headers = {};
					headers["Authorization"] = $i.cookies._get("session_token");

					 
					config = {
							  //theme: "bootstrap",
							  placeholder: $(this).data('placeholder'),
							  //allowClear: true,
							  width : '100%',
							  ajax: {
								  url: $i._get('api') + url,
								  dataType: 'json',
								  delay: 250,
								  headers: headers,
								  contentType : 'application/json',
								  data: function (params) {
									  return {
										  q: params.term, // search term
										  page: params.page
									  };
								  },
								  processResults: function (data, params) {
								      // parse the results into the format expected by Select2
								      // since we are using custom formatting functions we do not need to
								      // alter the remote JSON data, except to indicate that infinite
								      // scrolling can be used
									  params.page = params.page || 1;
									  return {
										  results: data.data,
										  pagination: {
											  more: (params.page * 30) < data.total_count
										  }
									  };
								  },
								  cache: true
							  },
						  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
						  minimumInputLength: 1,
						  templateResult: formatRepo,
						  templateSelection: function(repo){
							  return (repo.name || repo.text);
						  }
					};
					break;
			}
			
			if($(this).attr('multiple')!='undefined'){
				
			}
			
			if($(this).attr('data-placeholder')){
				if(typeof(config.placeholder)==='undefined'){
					config.placeholder = $i.i18n('widgets:plh_select_an_option');
				}
				$(this).removeAttr('data-placeholder');
			}
			
			
			if($(this).find('option').length<15){
				config.minimumResultsForSearch =  Infinity;
			}
			
			$(this).select2(config);
		});
	}
	
	
	$.fn.extend({
		select : function(config){
			return new idcp_select(config,$(this));
		}
	});
})();