(function(){
	function starrating(config,$primaryObject){
		var page = $i.pages.find.page($primaryObject.closest('div[data-pagehash]').data('pagehash'));
		var _PLUGIN_NAME = "idcp.widgets.rating";
		var pluginLoaded = false;
		var $input = null;
		var raty_library_path = ["assets/js/jquery.raty.js"];
		if(typeof($.fn.raty)==='undefined'){
			$i.require(raty_library_path).done(loadWidget);
		}
		else{
			loadWidget();
		}
		function loadWidget(){
			var uniqueID = $i.generator();
			pluginLoaded = true;
			$primaryObject.raty({
				cancel : config.cancel,
				half : config.half,
				readOnly : ((typeof(config.readOnly)==='boolean')? config.readOnly:false), 
				starType : 'i',
				score : config.value,
				click : function(score,evt){
					$input.val(score);
					page.$.trigger('idcp.widgets.trigger.change',{name : _PLUGIN_NAME, id: uniqueID, $element: $input});
				}
			});
			$primaryObject.parent().append('<input type="hidden" value="'+config.value+'" name="'+config.name+'" data-id="'+config.name+'" data-plugin-name="'+_PLUGIN_NAME+'" data-plugin-id="'+uniqueID+'" />');
			$input = $primaryObject.parent().find('input[data-id="'+config.name+'"]');
			
			
			$input.on('change',function(e){
				$primaryObject.raty('set', { score: $(this).val()});
			});
			
			page.$.trigger('idcp.widgets.trackchanges',{name : _PLUGIN_NAME, id: uniqueID, $element: $input});
		}
		
		var iid = null;
		var readOnlyF = this.readOnly = function(ro){
			if(!pluginLoaded){
				iid = setInterval(function(){
					readOnlyF(ro);
				},50);
			}
			else{
				if(iid!=null){
					clearInterval(iid);
				}
				//console.log("POSEM A READ ONLY el plugin "+$input.data('plugin-name')+" amb ID: "+$input.data('plugin-id'));
				readOnly = ro;
				$primaryObject.find('i').unbind(); //Deshabilitem els events del plugin ($.raty('readOnly',true); no funciona).
				
			}
		}

		if(page!==false){
			page.plugins(_PLUGIN_NAME,this);
		}
		else{
			console.log("Page object not found");
		}
	}

	$.fn.extend({
		rating : function(config){
			return new starrating(config,$(this));
		}
	});
})();