var idcp_nest = function(config,$primaryObject){
	var _this = this;
	this.div = $primaryObject;
	
	var page = $i.pages.find.page($primaryObject.closest('div[data-pagehash]').data('pagehash'))

	this.reload = this.init = function(){
		if(!this.div.hasClass('dd')){
			this.div.addClass('dd');
		}
		
		$i.promise._GET(config.restURL)
		.done(function(data){
			_this.render(data.data);
		});
	};
	
	if(typeof($.fn.nestable)==='undefined'){
		//console.log("NESTABLE UNDEFINED");
		$i.require(["assets/js/jquery.nestable.js"])
		.done(function(){
			_this.init();
		});
	}
	else{
		//console.log("NESTABLE DEFINED");
		this.init();
		
	}
	
	page.buttons.addExtraButton([
         { icon: 'fa fa-compress', tooltip : $i.i18n('widgets:collapse'),click : function(){_this.collapse();}},
         { icon: 'fa fa-expand', tooltip : $i.i18n('widgets:expand'),click : function(){_this.expand();}} 
	]);
	
	
	this.render = function(data){
		this.div.html();
		//console.log(data);
		var render = '<ol class="dd-list" data-id="nest-root">';
		$.each(data,function(){
			render += createTag(this);
		});
		render += '</ol>';
		this.div.html(render);
		
		
		$('.dd i[data-type="NEW_CHILD"]').on('click',function(){
			nestableTagF('NEW_CHILD',$(this).parent().parent());
		});
		$('.dd i[data-type="DELETE"]').on('click',function(){
			nestableTagF('DELETE',$(this).parent().parent());
		});
		
		/*$('.dd i[data-type="EDIT"]').on('click',function(){
			_this.event('EDIT',$(this).parent().parent());
		});*/
		
		
		
		
		this.div.nestable({ /* config options */ });
		this.div.nestable('collapseAll')
		.on('change',function(e){
			$i.promise._POST({
				restURL : 'mant/tags/reorder',
				data : _this.div.nestable('serialize'),
			}).done(function(){
				$i.gritter({
					type : 'success',
					title : $i.i18n('title_resource_updated'),
					description : $i.i18n('mant:reorder_success_text')
				});
			});
			
			if(typeof(_this.onchangeF)==='function'){
				_this.onchangeF(e,_this.div.nestable('serialize'));
			}

			
		});
	};
	
	var nestableTagF = this.createTag = function(action,evt){
		if(action=="DELETE"){
			var idc = evt.data('id_tag');
			$i.modal({
				show : true,
				autoClose : true,
				title: $i.i18n('md_title_confirm'),
				description : $i.i18n('sureDeleteResource'),
				width: '500px',
				resizable : false,
				success : function(){
					$i.promise._DELETE({
						restURL : 'mant/tags/'+idc,
						resource : idc
					})
					.done(function(){
						_this.removeChildren(idc);
					});
				}
			});
			
		}
		else{
			var pid = null
			if(typeof(evt)!=='undefined'){
				pid = evt.data('id_tag');
			}
			var rtagf = renderTagForm(pid);
			$i.modal({
				show : true,
				autoClose : false,
				title: $i.i18n('mant:md_newTag'),
				description : rtagf,
				width: '500px',
				resizable : false,
				success : (typeof(config.oncreate)==='function'? config.oncreate:successf)
			});
		}
	};
	
	function successf(id,modal,$modal){
		var validate = $modal.validate();
		if(validate){
			var values = $modal.getValues(); 
			$i.promise._POST({
				restURL : config.restURL,
				resource : values.name,
				data : values
			})
			.done(function(data){
				var idp = $('#'+id + ' > form[name="tag-form"] > input[name="id_parent"]').val();
				idp = (idp==""? null:idp);
				_this.addChildren(idp,data.data);
				modal.close();
				//nestag.removeChildren(idc); //TODO: ?????
			});
		}
		else{
			console.log("form validation fail!");
		}
	}
	
	function renderTagForm(id){
		var el = '<form class="form-horizontal" role="form" name="tag-form">'+
		'		<input type="hidden" name="id_parent" data-id="id_parent" value="'+(id==null? '':id)+'" />'+
		'		<div class="row">'+
		'		<div class="col-xs-12">'+
		'		<!-- PAGE CONTENT BEGINS -->'+
		'			<!-- B:TNAME -->'+
		'			<div class="form-group">'+
		'				<label class="control-label col-xs-2 col-sm-2 no-padding-right" for="name" data-i18n="mant:lbl_name"></label>'+
		'				<div class="col-xs-10 col-sm-10">'+
		'					<input type="text" name="name" data-id="name" class="col-xs-12 col-sm-12" required>'+
		'				</div>'+
		'			</div>'+
		'			<!-- E:ANAME -->'+
		'		</div>'+
		'	</div>'+
		'	</form>';
		return el;
	}
	
	this.collapse = function(){
		this.div.nestable('collapseAll');
	};
	
	this.expand = function(){
		this.div.nestable('expandAll');
	};
	
	this.onchangeF = null;
	this.onchange = function(f){
		this.onchangeF = f;
	};
	
	
	var count = 1;
	var createTag = function(tag){
		var r = ''+
		'<li class="dd-item dd2-item" data-id_tag="'+tag.id+'" data-id_parent="'+(tag.id_parent!=null? tag.id_parent:"")+'" data-position="'+tag.position+'">\r\n'+
		'<div class="dd-handle dd2-handle"><i class="normal-icon ace-icon fa fa-tags blue bigger-130"></i><i class="drag-icon ace-icon fa fa-arrows bigger-125"></i></div>\r\n'+
		'<div class="dd2-content">'+tag.name+
		'<i data-event="true" data-type="DELETE" class="pull-right bigger-130 ace-icon fa fa-trash-o black" style="cursor:pointer;"></i>'+
		//'<i data-event="true" data-type="EDIT" class="pull-right bigger-130 ace-icon fa fa-pencil black" style="cursor:pointer;"></i>'+
		'<i data-event="true" data-type="NEW_CHILD" class="pull-right bigger-130 ace-icon fa fa-plus blue" style="cursor:pointer;"></i>'+
		'</div>\r\n';
		if(tag.childrens.length>0){
			r +='<ol class="dd-list">\r\n';
			for(var i=0,j=tag.childrens.length;i<j;i++){
				r += createTag(tag.childrens[i]);
			}
			r += '</ol>\r\n';
		}
		r += '</li>\r\n';
		
		return r;
	};
	
	this.addChildren = function(idp,data){
		var parent = null;
		if(idp==null){
			parent = $i('#nest-root');
		}
		else{
			parent = $i('#nest-root li[data-id_tag="'+idp+'"]');
		}
		
		//console.log(idtag);
		var tag = {
			id_parent : null,
			childrens : [],
			id : data.id,
			name : data.name,
			position : data.position
		};
		var r = "";
		if(idp!=null){ //Is children of other tag.
			tag.id_parent = idp;
			if(parent.has('ol').length){ //has childrens.
				parent = parent.find('> ol');
				r += createTag(tag);
			}
			else{ //Is the first child of this parent.
				parent.prepend('<button data-action="collapse" type="button" style="display: block;">Collapse</button><button data-action="expand" type="button" style="display: none;">Expand</button>');
				r +='<ol class="dd-list">\r\n';
				r += createTag(tag);
				r += '</ol>';
			}
		}
		else{ //New element on root.
			r += createTag(tag);
		}
		var $r = $(r);
		$r.find('i[data-type="NEW_CHILD"]').on('click',function(){
			nestableTagF('NEW_CHILD',$(this).parent().parent());
		});
		
		/*$r.find('i[data-type="EDIT"]').on('click',function(){
			_this.event('EDIT',$(this).parent().parent());
		});*/
		
		$r.find('i[data-type="DELETE"]').on('click',function(){
			nestableTagF('DELETE',$(this).parent().parent());
		});
		parent.append($r);
	}
	
	this.removeChildren = function(idc){
		children = $i('#nest-root li[data-id_tag="'+idc+'"]');
		children.remove();
	}
	
	
	
}

$i.nestable = function(config){
	return new idcp_nest(config,config.$primaryObject);
}

$.fn.extend({
	nest : function(config){
		return new idcp_nest(config,$(this));
	}
});