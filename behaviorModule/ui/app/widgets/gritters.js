/**
 * Gritters widget.
 * 
 * @author phidalgo
 * @param object config
 * @returns gritters
 */

gritters = function(config){
	
	var title = config.title || '';
	var type = config.type || null;
	var description = config.description || '';
	var sticky = config.sticky || false;
	
	if(type==null){
		throw new Exception(3000);
	}
	
	var cName = '';
	switch(type){
		case "success":
			cName = 'gritter-success gritter-ligth';
			if(title==''){
				title = "Success";
			}
			break;
		case "warning": 
			cName = 'gritter-warning gritter-ligth';
			if(title==''){
				title = "Warning";
			}
			break;
		case "fail":
			cName = 'gritter-error gritter-ligth';
			if(title==''){
				title = "Fail";
			}
			break;
		default:
			cName = '';
	}
	
	
	if(typeof($.gritter)==='undefined'){
		$i.require(["assets/js/jquery.gritter.js","assets/css/jquery.gritter.css"])
		.done(function(){
			$.gritter.add({
				title: '<h3>'+title+'</h3>',
				text:  description,
				class_name: cName,
				sticky: sticky, 
				time: $i._get('gritterTime') // (int | optional) the time you want it to be alive for before fading out (milliseconds)
			});
		});
	}
	else{
		$.gritter.add({
			title: '<h3>'+title+'</h3>',
			text:  description,
			class_name: cName,
			sticky: sticky, 
			time: $i._get('gritterTime') // (int | optional) the time you want it to be alive for before fading out (milliseconds)
		});
	}
	
};

idcp.gritter = function(config){
	return new gritters(config);
};