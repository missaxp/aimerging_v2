(function(){
	/**
	 * A veure, aqui no podem treballar amb trunc, i la explicació és la següent.
	 * 
	 * Si jo vull els registres d'un dia en concret i faig trunc(16/03/2016 10:11:12)=trunc([camp_db]) em tornarà les tasques del dia 16/03/2016.
	 * Però el dia 16 per aqui?
	 * Per a mi, el dia 16 comença el dia 15/03/2016 a les 23:00:00+0200
	 * Per a un que viu a Londres, el dia 16 comença el dia 16/03/2016 a les 00:00:00+0000.
	 * Per a un que viu a Pacific/Apia, el dia 16 comença el dia 15/03/2016 a les 10:00:00 UTC.
	 * 
	 *  Així que no podem fer trunc perque cada dia comença en una data diferent. Així de senzill.
	 *  El datepicker sense rang internament haurà de funcionar com un rang. Amb un interval definit per a cada dia.
	 *  
	 *  Si jo, que visc en TZ Europe/Madrid vull saber les tasques del dia 16/03/2016, li hauré de demanar al servidor el següent interval:
	 *  Des de : 15/03/2016 23:00:00 UTC
	 *  Fins : 16/03/2016 22:59:59 
	 *  
	 *  ----
	 *  També hi ha el cas de les dates HISTORIQUES. Són aquelles dates que sempre s'expressen en UTC. Jo vaig néixer el 24 de Juliol del 1985.
	 *  Vaig neixer aquest dia en UTC, i quan afegeixo aquesta data sempre és en UTC. No vaig neixer el dia 23 a Pacific/Apia per exemple.
	 *
	 */
	function idcp_datepicker(config,$primaryObject){
		if($primaryObject.length==0) return;
		var pluginLoaded = false;
		var readOnly = false;
		var page = $i.pages.find.page($primaryObject.closest('div[data-pagehash]').data('pagehash'));
		var datepicker_library_path = ["lib/external-lib/bootstrap-datepicker.min.js","assets/css/bootstrap-datetimepicker.css","assets/css/datepicker.css"];
		var datePickerSeparator = '-';
		var plugin_loaded = false;
		var _PLUGIN_NAME = "idcp.widgets.datepicker";
		/**
			This datepicker plugin is: http://eternicode.github.io/bootstrap-datepicker/
		 */
		if(typeof($.fn.datepicker)==='undefined'){
			$i.require(datepicker_library_path).done(function(){
				$.fn.datepicker.dates['en'] = {
				    days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
				    daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
				    daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
				    months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
				    monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
				    today: "Today",
				    clear: "Clear",
				    format: "mm/dd/yyyy",
				    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
				    weekStart: 0
				};
				
				
				$.fn.datepicker.dates.ca = {
					days : ["Diumenge", "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte"],
					daysShort : ["Diu", "Dil", "Dmt", "Dmc", "Dij", "Div", "Dis"],
					daysMin : ["dg", "dl", "dt", "dc", "dj", "dv", "ds"],
					months : ["Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre"],
					monthsShort : ["Gen", "Feb", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Des"],
					today : "Avui",
					monthsTitle : "Mesos",
					clear : "Esborrar",
					weekStart : 1,
					format : "dd/mm/yyyy"
				}
				
				$.fn.datepicker.dates.es = {
					days : ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
					daysShort : ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
					daysMin : ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
					months : ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
					monthsShort : ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
					today : "Hoy",
					monthsTitle : "Meses",
					clear : "Borrar",
					weekStart : 1,
					format : "dd/mm/yyyy"
				};
				loadWidget();
			});
		}
		else{
			loadWidget();
		}
		
		function loadWidget(){
			/**
			 * Per a cada objecte jQuery...
			 */
			$primaryObject.each(function(){
				var $currentObject = $(this);
				config = config || {}; //Initialize config variable if config is undefined
				var uniqueID = $i.generator();
				var data_id = $currentObject.data("id");
				var name = $currentObject.attr("name");
				$currentObject.attr('data-plugin-name',_PLUGIN_NAME);
				$currentObject.attr('data-plugin-id',uniqueID);
				$currentObject.attr('nosend',true); //This input field will not send to API.
				$currentObject.attr("name",name+"_dpicker");
				$currentObject.attr('data-id',data_id+"_dpicker");
				$currentObject.attr('readonly',true);
				$currentObject.attr('style','cursor:pointer!important'); //Remove "not-allowed" cursor from readonly attribute on input.
				$currentObject.parent().append('<input type="hidden" value="" data-id="'+data_id+'" name="'+name+'">');
				
				var minDate = false;
				var attrminDate = $currentObject.attr('data-minDate');
				if(typeof(attrminDate)==='string' && attrminDate=="true"){
					minDate = true;
				}
				
				var historicalDate = (typeof(config.historicalDate)==='boolean'? config.historicalDate:false);
				$currentObject.attr("data-historical-date",historicalDate);
				var value = $currentObject.val();
				var format = $i.times().getLocalFormat().dateFormat; //Get local format to notify the format to datepicker plugin
				var $hiddenInput = $i('#'+data_id);
				
				if(value!=""){
					if(historicalDate){
						//En aquest cas, la data ve en format de servidor i el que fem
						//es transformar-la al format local SENSE transformar la zona horaria.
						$currentObject.val($i.times(value).toServerTZ().toLocalFormat('D').render());
					}
					else{
						var ranges = value.split(datePickerSeparator);
						$currentObject.val($i.times(ranges[0]).local().date);//Get and setdate in local format.
						$hiddenInput.val(value);
					}
					
				}
				
				
				var dpickerobject= {
					language : $i.i18n.currentLang,
					autoclose:true,
					clearBtn: true,
					format: format.toLowerCase(), //This format needs to be in lowercase...
				};
				
				if(minDate){
					dpickerobject.startDate = $i._get('time').local.local().date;
				}
				
				/**
				 * Set datepicker plugin...
				 */
				$currentObject.datepicker(dpickerobject).on('changeDate', function(e) {
					if($currentObject.val()!=""){ //New date.
						if(historicalDate){
							//En aquest cas, agafem la data i NO la transformem a la zona horaria del servidor, sinó que la enviem
							//en aquesta zona horaria, ja que així la desarem en UTC.
							var m = moment($currentObject.val(),format).startOf('day');
							$hiddenInput.val($i.times(m).toServerFormat().render());
							console.log($hiddenInput.val());
						}
						else{
							var start = moment($currentObject.val(),format).startOf('day');
							var end = moment($currentObject.val(),format).endOf('day');
							var $from = $i.times(start);
							var $to = $i.times(end);
							$currentObject.val($from.local().date);
							$hiddenInput.val($from.remote()+datePickerSeparator+$to.remote());
							console.log("LOCAL -> from: "+$from.local().date+", to: "+$to.local().date +", REMOTE -> from: "+$from.remote()+", to: "+$to.remote());
						}
						$hiddenInput.trigger('change');
					}
					else{
						$hiddenInput.val('')
					}
			    }).on('clearDate',function(e){
			    	$hiddenInput.val(''); //Clear hidden input
			    })
			    .next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
			});
			pluginLoaded = true;
			
			if(page!==false){
				page.$.trigger(_PLUGIN_NAME+"_onload",{name : _PLUGIN_NAME});
			}

		}
		
		var iid = null;
		var readOnlyF = this.readOnly = function(ro){
			readOnly = ro;
			if(!pluginLoaded){
				if(iid==null){
					iid = setInterval(function(){
						readOnlyF(ro);
					},50);
				}
			}
			else{
				if(iid!=null){
					clearInterval(iid);
				}
				//TODO: Si carreguem una plana i encara no haviem carregat el plugin de datepicker i la plana és readOnly, mostrarà la data en format servidor.
				//Aquest widget no cal posar-lo a read only pq està instanciat sobre el input mateix i aquest és eliminat quan
				//la pàgina és de només lectura, però ens hem d'assegurar que la data que es mostra esta en format correcte i no 
				//en format de servidor. Això passa quan posem la pàgina a readOnly abans que el plugin estigui carregat...
				
				$primaryObject.each(function(){
					console.log("POSEM A READ ONLY el plugin "+$(this).data('plugin-name')+" amb ID: "+$(this).data('plugin-id'));
				});
			}
		}
		
		if(page!==false){
			page.plugins(_PLUGIN_NAME,this);
		}
		else{
			console.log("Page object not found");
		}
		
		this.pluginLoaded = function(){
			return pluginLoaded;
		}
	}

	$i.dpicker = dpicker = function(config){
		config.timepicker = false;
		return new idcp_datepicker(config,config.$primaryObject);
	}
	
	$i.dtpicker = dtpicker = function(config){
		config.timepicker = true;
		return new idcp_datepicker(config,config.$primaryObject);
	}

	$.fn.extend({
		dpicker : function(config){
			config = config || {};
			config.$primaryObject = $(this);
			return dpicker(config);
		},
		dtpicker : function(config){
			config = config || {};
			config.$primaryObject = $(this);
			return dtpicker(config);
		}
	});
})();

