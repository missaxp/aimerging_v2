(function(){
	function suggestion(config,$primaryObject){
		var restURL = config.rest;
		
		var format = config.format || 'default';
		
		var options = {
			appendTo : $primaryObject.parent().parent(),
			minLength: 2,
			delay : 250,
			source: function( request, response ) {
		    	if(!$primaryObject.hasClass('inputIcon')){
		    		$primaryObject.addClass('inputIcon');
		    	}
		    	$i.promise._request('GET',restURL,{q : $primaryObject.val().toUpperCase()})
		    	.done(function(data){
		    		response(data.data);
		    		return;
		    	});
			},
			create :  function () {
	            $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
	                return $('<li>').append((format=='default'? defaultRepo(item):formatRepo(item))).appendTo(ul);
	            };
	        },
	        focus: function( ev, data ) {
	        	ev.preventDefault();
	        	$primaryObject.val(data.item.name);
	        	
	        },
	        select : function(ev,data){
	        	ev.preventDefault();
	        	$primaryObject.val(data.item.name);
	        	if(typeof(config.onSelect)==='function'){
	        		config.onSelect(data.item);
	        	}
	        }
		}
		
		$primaryObject.autocomplete(options);
	}
	
	function defaultRepo(repo){
		return repo.name;
	}
	
	function formatRepo (repo) {
		var markup = "<div class='select2-result-repository clearfix'>";
	    if(repo.avatar==null){
	    	markup+='<div class="select2-result-repository__avatar"><i class="fa fa-user fa-4x"></i></div>';
	    }
	    else{
	    	markup+="<div class='select2-result-repository__avatar'><img src='data:image/gif;base64," + repo.avatar + "' /></div>";
	    }
		markup +="<div class='select2-result-repository__meta'>";
		markup +="<div class='select2-result-repository__title'>" + repo.name+"</div>";
		if (repo.description) {
			markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
		}
		markup += "<div class='select2-result-repository__statistics'>" +
		"<div class='select2-result-repository__stargazers'><i class='fa fa-envelope-o'></i> " + repo.email + "</div>" +
		"<div class='select2-result-repository__watchers'><i class='fa fa-phone'></i> " + repo.wphone + "</div>" +
		"</div>" +
		"</div></div>";
		return markup;
	}

	$i.autocomplete = function(config){
		return new suggestion(config,config.$primaryObject);
	}

	$.fn.extend({
		suggestion : function(config){
			return new suggestion(config,$(this));
		}
	});
})();