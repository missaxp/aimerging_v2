/**
 * 
 */

/**
 * This function is outside the local space because it would be used by another classes, so it has to be public
 * IMPORTANT! Sempre que creem una nova data NO agafem la hora de l'usuari, sinó la hora del servidor. 
 *  
 */
var getDefaultDateRanges = function(){
	var serverTime = $i._get('webservice').time;
	var obj = {};
	obj[$i.i18n('widgets:lbl_today')] 		= [$i.times(serverTime).getMoment().startOf('day'), $i.times(serverTime).getMoment().endOf('day'),':TODAY'];
	obj[$i.i18n('widgets:lbl_yesterday')] 	= [$i.times(serverTime).getMoment().subtract(1, 'days').startOf('day'), $i.times(serverTime).getMoment().subtract(1, 'days').endOf('day'),':YESTERDAY'];
	obj[$i.i18n('widgets:lbl_last7days')] 	= [$i.times(serverTime).getMoment().subtract(6, 'days').startOf('day'), $i.times(serverTime).getMoment().endOf('day'),':LAST7DAYS'];
	obj[$i.i18n('widgets:lbl_last30days')] 	= [$i.times(serverTime).getMoment().subtract(29, 'days').startOf('day'), $i.times(serverTime).getMoment().endOf('day'),':LAST30DAYS'];
	obj[$i.i18n('widgets:lbl_thisMonth')] 	= [$i.times(serverTime).getMoment().startOf('month'), $i.times(serverTime).getMoment().endOf('month'),':THISMONTH'];
	obj[$i.i18n('widgets:lbl_lastMonth')] 	= [$i.times(serverTime).getMoment().subtract(1, 'month').startOf('month'), $i.times(serverTime).getMoment().subtract(1, 'month').endOf('month'),':LASTMONTH'];
	
	
	$.each(obj,function(range,value){
		//console.log("RANGE: " + range + ', start date: '+value[0].format()+', end date: '+value[1].format()+', timezone: '+value[0].tz());
	});
	return obj;
};

/**
 * Aquest widget el que fa és inicialitzar correctament el plugin de bootstrap daterangepicker
 * @web: http://www.daterangepicker.com/
 * 
 * La idea és la següent.
 * 
 * El plugin ha de generar els rangs localitzats per mostrar-los a l'usuari.
 * El plugin ha de generar les equivalents dates en el format que el servidor requereixi.
 * 
 * Si un codi extern demana el valor d'aquest plugin, aquest sempre retornarà la data per al servidor.
 * Quan hagi de renderitzar les dates, ho farà en format usuari.
 * 
 * El que faig és quan instancio aquest objecte sobre un input, aquest input es queda readonly, i agafo els camps name i data-id d'aquest input i els canvio
 * pels valors de name i data_id seguit de "_drange". Si el input té de name="input_name" aquest input desrpés d'inicialitzar-se la classe tindrà un name de "input_name_drange".
 * 
 * Creem un input type="hidden" amb els atributs data-id i name del input original.
 * 
 * En el input original hi emplaçarem les dates localitzades.
 * En el input hidden creat, hi emplaçarem les dates en format de servidor.
 * 
 * Quan algu demani la data a aquest plugin, obtindrà les dates del input type="hidden" ja que tindrà el data-id i name que originals.
 * Quan algu inicialitzi el input original amb algun valor, aquest valor serà el rang o data en format servidor. Aquest plugin agafarà les dates operarà amb elles
 * per a mostrar les coses de forma corrrecte.
 * 
 * !!!!!Aquest plugin és la ultima capa entre l'usuari i tot lo de sota. Tot codi o objecte que parli amb aquest plugin ha de fer-ho en data format servidor.
 * Tot lo que mostri al client, ha de ser en format client.!!!!!!
 * 
 * La inicialització la fem en vàris sentits.
 * 	Localització: Traduïm els noms dels diferents elements que el plugin necessitarà (Abreviacions, dies, mesos, anys... etc).
 * 	
 */


(function(){
	function idcp_daterange(config,$primaryObject){
		var dayNameAbv 		= [$i.i18n('widgets:dname_min_monday'),$i.i18n('widgets:dname_min_tuesday'),$i.i18n('widgets:dname_min_wednesday'),$i.i18n('widgets:dname_min_thursday'),
		               		   $i.i18n('widgets:dname_min_friday'),$i.i18n('widgets:dname_min_saturday'),$i.i18n('widgets:dname_min_sunday')];

		var dayNames 		= [$i.i18n('datepicker:dname_monday'),$i.i18n('widgets:dname_tuesday'),$i.i18n('widgets:dname_wednesday'),$i.i18n('widgets:dname_thursday'),
		             		   $i.i18n('widgets:dname_friday'),$i.i18n('widgets:dname_saturday'),$i.i18n('widgets:dname_sunday')];

		var monthNames 		= [$i.i18n('widgets:dname_january'),$i.i18n('widgets:dname_february'),$i.i18n('widgets:dname_march'),$i.i18n('widgets:dname_april'),$i.i18n('widgets:dname_may'),
		               		   $i.i18n('widgets:dname_june'),$i.i18n('widgets:dname_july'),$i.i18n('widgets:dname_august'),$i.i18n('widgets:dname_september'),$i.i18n('widgets:dname_october'),
		               		   $i.i18n('widgets:dname_november'),$i.i18n('widgets:dname_december')];
		
		var monthNamesShort = [$i.i18n('widgets:dname_min_january'),$i.i18n('widgets:dname_min_february'),$i.i18n('widgets:dname_min_march'),$i.i18n('widgets:dname_min_april'),
		                       $i.i18n('widgets:dname_min_may'),$i.i18n('widgets:dname_min_june'),$i.i18n('widgets:dname_min_july'),$i.i18n('widgets:dname_min_august'),
		                       $i.i18n('widgets:dname_min_september'),$i.i18n('widgets:dname_min_october'),$i.i18n('widgets:dname_min_november'),$i.i18n('widgets:dname_min_december')];
		
		var data_id = $primaryObject.data("id");
		var name = $primaryObject.attr("name");
		
		$primaryObject.attr('nosend',true); //This input field will not send to API.
		$primaryObject.attr("name",name+"_daterange");
		$primaryObject.attr('data-id',data_id+"_daterange");
		$primaryObject.attr('readonly',true); //Set as readonly input.
		$primaryObject.attr('style','cursor:pointer!important'); //Remove "not-allowed" cursor from readonly attribute on input.
		$primaryObject.parent().append('<input type="hidden" value="" data-id="'+data_id+'" name="'+name+'">');
		
		var $hiddenInput = $i('#'+data_id);
		var dateRangeSeparator = '-'; 
		var value = $primaryObject.val();
		
		var format = $i.times().getLocalFormat().dateFormat; //Get local format to notify the format to datepicker plugin
		
		var drange_obj = {
			 ranges: getDefaultDateRanges(),
			 format: format,
	         separator: dateRangeSeparator,
			 "locale": {
				 	"format": format,
			        "separator": dateRangeSeparator,
			        "applyLabel": $i.i18n('widgets:bt_submit'),
			        "cancelLabel": $i.i18n('widgets:bt_clear'),
			        "fromLabel": $i.i18n('widgets:lbl_from'),
			        "toLabel": $i.i18n('widgets:lbl_to'),
			        "customRangeLabel": $i.i18n('widgets:lbl_custom'),
			        "daysOfWeek": dayNameAbv,
			        "monthNames": monthNames,
			        "firstDay": 0
			 }
		};
		
		if(value!=""){
			/**
			 * Quan tenim un rang inicial, el valor tindrà el format del servidor.
			 * 
			 * El que farem serà convertir aquest valor a una data local (que serà el format necessari q entendrà aquest plugin).
			 * I omplim, l'objecte drange_obj que és el de configuració del plugin, afegint startDate i endDate els valors corresponents.
			 * Al final, en el camp hidden (que és el que s'enviarà el servidor i el que tots els objectes que criden a aquest mètode veuen),
			 * posem el rang en format i zona horaria de servidor.
			 */
			
			var range1,range2;
			var ranges = value.split(dateRangeSeparator);
			var $range1 = $i.times(ranges[0]);
			var $range2 = $i.times(ranges[1]);
			drange_obj.startDate = $range1.local().date; //Set start date in local format.
			drange_obj.endDate = $range2.local().date; //Set start date in local format.
			$hiddenInput.val($range1.remote()+dateRangeSeparator+$range2.remote());
		}
		
		
		$primaryObject.daterangepicker(drange_obj); //Create daterangepicker object http://www.daterangepicker.com/
		
		//Event when date range is entered.
		$primaryObject.on('apply.daterangepicker',function(ev,picker){
			/**
			 * La data inicial i la final poden venir desde un rang predefeinit (funció getDefaultDateRanges()) o des de l'usuari
			 * insertant un rang en concret. En qualsevol cas, bé un objecte moment.
			 * Start date la hora és 00:00:00 Hora local usuari.
			 * End date la hora és 23:59:59 hora local usuari.
			 * 
			 * Aqui agafaem la data i la convertim a la zona horària del servidor. 
			 * $i.times(start).remote().
			 * Obtenim la data entrada de l'usuari en el format del servidor i amb la zona horaria del servidor.
			 */
			start = picker.startDate; //Agafem la data inicial (objecte moment) insertada per l'usuari.
			end = picker.endDate; //Agafem la data final (objecte moment) insertada per l'usuari.
			var $start = $i.times(start);
			var $end = $i.times(end);
			
			var ls = $start.toUserTZ().relativeUTC();
			var le = $end.toUserTZ().relativeUTC();
			var ss = $start.toServerTZ().relativeUTC();
			var se = $end.toServerTZ().relativeUTC();
			console.log("User time: From: "+ls+", to:"+le+", server time: From: "+ss+", to: "+se);
			$hiddenInput.val($start.remote()+dateRangeSeparator+$end.remote());
		});
		
		//Event when users click to clear button.
		$primaryObject.on('cancel.daterangepicker', function(ev, picker) {
			$primaryObject.val(''); //do something, like clearing an input
			$hiddenInput.val(''); //clear hidden input
		});
		
		//Si al iniciar no posem això, el plugin s'inicia amb la data d'avui.
		if(value==""){
			$primaryObject.val(''); //do something, like clearing an input
			$hiddenInput.val(''); //clear hidden input
		}
	}
	
	$i.daterange = function(config){
		return new idcp_daterange(config,config.$primaryObject);
	}

	$.fn.extend({
		daterange : function(config){
			return new idcp_daterange(config,$(this));
		}
	});
})();