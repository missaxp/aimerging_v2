(function(){
	function idcp_list(config,$object){
		var _PLUGIN_NAME = 'idcp.widgets.list';
		var _PLUGIN_ID = $i.generator();
		var columns = config.columns;
		var defaultValues = ((config.values==null)? []:config.values);
		var onElementRender = (typeof(config.onElementRender)==='function'? config.onElementRender:null);
		// var onActionNewElement = (typeof(config.onActionNewElement)==='function'? config.onElementRender:null);
		var autocomplete = config.autocomplete || null;
		var uniqueElement = config.uniqueKey || null;
		var sortable = (typeof(config.sort)==='boolean'? config.sort:false);
		var showNewElement = (typeof(config.showNewElement)==='boolean'? config.showNewElement:true);
		var onElementClick = (typeof(config.onclick)==='function'? config.onclick:null);
		var rowId = (typeof(config.rowid)!=='undefined'? config.rowid:null);
		
		var commitedData = []; 
		
		
		var page = $i.pages.find.page($object.closest('div[data-pagehash]').data('pagehash'));
		
		var tbody_id = 'list_body_'+_PLUGIN_ID;
		var fe_id = 1; //Id assigned by the front end.
		var html = '<table class="table table-striped table-hover" data-list-name="'+config.name+'" data-plugin-name="'+_PLUGIN_NAME+'" data-plugin-id="'+_PLUGIN_ID+'">'+
					'<thead class="thin-border-bottom">'+
						'<tr>';
							$.each(columns,function(){
								if(this.icon && this.icon!=""){
									html +='<th '+(typeof(this.width)!=='undefined'? 'style="width:'+this.width+'px!important;"':'')+'><i class="ace-icon fa '+this.icon+'"></i>'+this.text+'</th>';
								}
								else{
									html +='<th>'+this.text+'</th>';
								}
							});
							html +='<th data-removeonro="true" style="width:50px!important;"><i class="fa fa-cog bigger-110"></i></th>';
							if(sortable){
								html+='<th data-removeonro="true" style="width:50px!important;">'+
									'<i class="fa fa-sort-asc list-sort-asc press fa-lg" style="position:absolute;top:5px;" aria-hidden="true" data-move="up"></i>'+
									'<i class="fa fa-sort-desc list-sort-desc press fa-lg" aria-hidden="true" data-move="down"></i>'+
								'</th>';
							}
						html +='</tr>'+
					'</thead>'+
					'<tbody data-id="'+tbody_id+'">'+
					'</tbody>'+
				'</table>';
		$object.html(html);
		
		var $table = $i('table[data-plugin-id="'+_PLUGIN_ID+'"]',$object);
		$table.attr('data-ischanged',false);
		
		var $tbody = $i('#'+tbody_id,$object);
		
		$.each(defaultValues,function(){
			var $tr = addElement(this,true);
			if($tr!==false){
				$tbody.append($tr);
				fe_id++;
			}
			
		});
		
		if(sortable){
			$i('i[data-move="up"]',$table).on('click',function(){
				var clid = parseInt($i('input[name="'+config.name+'_sort"]:checked',$tbody).val());
				var plid = (parseInt(clid)-1);
				if(clid>1){
					var $ctr = $i('tr[data-fe-id="'+clid+'"]',$tbody);
					var $ptr = $i('tr[data-fe-id="'+plid+'"]',$tbody);
					var $cctr = $ctr.clone();
					$ctr.remove();
					$ptr.before($cctr);
					$cctr.attr('data-fe-id',plid);
					$ptr.attr('data-fe-id',clid);
					$cctr.find('input[type="radio"]').val(plid);
					$ptr.find('input[type="radio"]').val(clid);
					$table.attr('data-ischanged',true);
					if(page!==false){
						page.$.trigger('idcp.widgets.trigger.change',{name : _PLUGIN_NAME, id: _PLUGIN_ID, $element: $object});
					}
				}
			});
			
			$i('i[data-move="down"]',$table).on('click',function(){
				var clid = $i('input[name="'+config.name+'_sort"]:checked',$tbody).val()
				var plid = (parseInt(clid)+1);
				var elements = $tbody.find('tr');
				if(plid<=(elements.length)){
					var $ctr = $i('tr[data-fe-id="'+clid+'"]',$tbody);
					var $ptr = $i('tr[data-fe-id="'+plid+'"]',$tbody);
					var $cctr = $ctr.clone();
					$ctr.remove();
					$ptr.after($cctr);
					$cctr.attr('data-fe-id',plid);
					$ptr.attr('data-fe-id',clid);
					$cctr.find('input[type="radio"]').val(plid);
					$ptr.find('input[type="radio"]').val(clid);
					$table.attr('data-ischanged',true);
					if(page!==false){
						page.$.trigger('idcp.widgets.trigger.change',{name : _PLUGIN_NAME, id: _PLUGIN_ID, $element: $object});
					}
				}
			});
			//'+config.name+'_sort
		}
		
		if(showNewElement){
			$tbody.append(addNewElement());
		}
		
		
		
		this.add = function(elements,add = false){
			$tbody.find('tr[data-form-new-element="true"]').remove();
			//
			if(typeof(elements)==='array'){
				$.each(elements,function(){
					programmaticallyAddElement(this);
				});
			}
			else if(typeof(elements)==='object'){
				programmaticallyAddElement(elements,false);
			}
			
			$tbody.append(addNewElement());
			if(page!==false){
				page.$.trigger('idcp.widgets.trigger.change',{name : _PLUGIN_NAME, id: _PLUGIN_ID, $element: $object});
			}
			if (add) {
				$table.find('tr[data-form-new-element="true"]').remove();
			}
			$table.attr('data-ischanged',true);
		}
		
		function programmaticallyAddElement(el,c){
			var add = true;
			if(typeof(config.onElementCreate)==='function'){
				var rsp = config.onElementCreate(el,$tbody);
				if(typeof(rsp)==='boolean'){
					add = rsp;
				}
			}
			if(add){
				var $tr = addElement(el,c);
				if($tr!==false){
					$tbody.append($tr);
					fe_id++;
				}
			}
		}
		
		function addElement(element,c){
			var committed = ((typeof(c)==='boolean')? c:false);
			var html = "";
			if(onElementRender!=null){
				html += '<tr data-fe-id="'+fe_id+'" data-committed="'+committed+'" data-removed="false"></tr>';
			}
			else{
				var elementId = ((rowId!==null && typeof(element[rowId])!=='undefined')? element[rowId]:null);
				
				html += '<tr data-fe-id="'+fe_id+'" data-committed="'+committed+'" data-removed="false" '+(elementId!=null? 'data-rowid="'+elementId+'" id="'+elementId+'"':'')+'>';
				$.each(columns,function(){
					if(typeof(this.source)==='undefined'){
						html += '<td style="cursor: pointer" id="'+elementId+'" data-column="'+this.name+'">'+(element[this.name]==null? '':element[this.name])+
						'<input type="hidden" nosend name="'+this.name+'" value="'+(element[this.name]==null? '':element[this.name])+'" /></td>';
					}
					else{
						if(typeof(element[this.name])!=='undefined' && element[this.name]!=null && typeof(element[this.name].text)!=='undefined'){
							html += '<td data-column="'+this.name+'">'+element[this.name].text+
							'<input type="hidden" nosend name="'+this.name+'" value="'+element[this.name].value+'" /></td>';
						}
						else{
							var foundEl = false;
							for(var i=0,j=this.source.length;i<j;i++){
								var el = this.source[i];
								//console.log(el[selectText]+" == "+element[this.name]);
								if(el.id == element[this.name]){
									html += '<td data-column="'+this.name+'">'+(el.name==null? '':el.name)+
									'<input type="hidden" nosend name="'+this.name+'" value="'+(el.id==null? '':el.id)+'" /></td>';
									foundEl = true;
									break;
								}
							}
							if(!foundEl){
								html += '<td data-column="'+this.name+'">'+(element[this.name]==null? '':element[this.name])+
								'<input type="hidden" nosend name="'+this.name+'" value="'+(element[this.name]==null? '':element[this.name])+'" /></td>';
							}
						}
					}
				});
				html +='</tr>';
			}
			var $html = $(html);
			
			if(onElementClick!==null){
				$html.find('td').on('click',function(){
					onElementClick($(this).closest('tr'),element);
				});
			}
			
			
			var renderable = true;
			if(onElementRender!=null){
				var r = config.onElementRender(element,$html);
				if(r!==false){
					$html.append(r);
				}
				else{
					renderable = false;
				}
				
			}
			if(!renderable) return false;
			
			
			var addDeleteButton = true;
			if(typeof(config.appendDelete)==='function'){
				var rsp = config.appendDelete(element);
				if(typeof(rsp)==='boolean'){
					addDeleteButton = rsp;
				}
			}
			if(!addDeleteButton){
				$html.append('<td data-removeonro="true" style="width:50px!important;"></td>');
			}
			else{
				$html.append('<td data-removeonro="true" style="width:50px!important;">'+
						'<i class="fa fa-trash press bigger-110" data-id="delete_element"></i>'+
				'</td>');
				
				
				$html.find('i[data-id="delete_element"]').on('click',function(e){
					e.stopPropagation();
					e.preventDefault();
					if (typeof(config.onElementDelete)==='function') {
						rsp = config.onElementDelete(element);
						if (typeof(rsp)==='boolean') {
							if (!rsp) {
								return false;
							}
						}
					}
					$html.hide('fast',function(){
						if($(this).attr('data-committed')=="true"){
							// console.log("ELIMINEM UN ELEMENT que existeix, per tant només l'amaguem.")
							$(this).attr('data-committed',"false");
							$(this).attr('data-removed','true');
						}
						else{
							//console.log("ELIMINEM UN ELEMENT que no existeix, per tant borrem el TR");
							$(this).remove();
						}
					});
					if(page!==false){
						page.$.trigger('idcp.widgets.trigger.change',{name : _PLUGIN_NAME, id: _PLUGIN_ID, $element: $object});
					}
					
					$table.attr('data-ischanged',true);
				});
			}
			
			if(sortable){
				$html.append('<td data-removeonro="true" style="width:50px!important;"><input type="radio" name="'+config.name+'_sort" value="'+fe_id+'" nosend/></td>');
			}
			
			if(typeof(config.afterRender)==='function'){
				config.afterRender(element,$html);
			}
			commitedData.push($html);
			return $html;
		}
		
		function addNewElement(){
			var html = "";
			html += '<tr data-form-new-element="true" data-removeonro="true">';
			$.each(columns,function(){
				var maxLength = (typeof(this.length)!=='undefined')? this.length:false;
				if(typeof(this.source)==='undefined'){
					html +='<td data-td-column="'+this.name+'">'+
					'<input '+(maxLength!==false? 'maxlength="'+maxLength+'"':'')+' type="text" data-track="false" nosend class="col-xs-12 col-sm-12" data-column="'+this.name+'" data-list-input="true" autocomplete="off">'+
					'</td>';
				}
				else{
					html +='<td data-td-column="'+this.name+'"><select data-track="false" nosend data-column="'+this.name+'" data-list-input="true">';
					$.each(this.source,function(){
						html+='<option value="'+this.id+'" '+((typeof(this.selected)!=='undefined' && this.selected)? 'selected':'')+'>'+this.name+'</option>';
					});
					html+='</td>';
				}
			});
			html +='<td data-removeonro="true" style="width:50px!important;">'+
				'<i class="fa fa-floppy-o bigger-110 press" data-id="save_element"></i>'+
			'</td>'+
			'</tr>';
			var $html = $(html);
			
			if(autocomplete!=null){
				$html.find('input[data-column="'+autocomplete.on+'"]').suggestion({
					rest : autocomplete.url,
					format : autocomplete.format,
					onSelect : function(data){
						if(typeof(autocomplete.onSelect)==='function'){
								autocomplete.onSelect(data,$html);
						}
					}
				}); //list autocomplete helper.
			}
			
			if(uniqueElement!=null){
				$html.find('input[data-column="'+uniqueElement.on+'"]').uniqueKey({rest : uniqueElement.url});
			}
			
			
			$html.find('i[data-id="save_element"]').on('click',function(e){
				e.stopPropagation();
				e.preventDefault();
				
				var uniqueCol = null;
				var send = {};
				$.each(columns,function(){
					if(typeof(this.source)==='undefined'){
						send[this.name] = $html.find('input[data-column="'+this.name+'"]').val();
					}
					else{
						send[this.name] = {
								text : $html.find('select[data-column="'+this.name+'"] option:selected').text(),
								value : $html.find('select[data-column="'+this.name+'"] option:selected').val()
						};
					}
					
					if(typeof(this.unique)!=='undefined' && this.unique===true){
						uniqueCol = this.name;
					}
				});
				
				if(typeof(config.onElementSave)==='function'){
					var rsp = config.onElementSave(send,$html);
					if(typeof(rsp)!=='undefined'){
						send = rsp;
					}
				}
				
				var add = true;
				if(typeof(config.onElementCreate)==='function'){
					var rsp = config.onElementCreate(send,$tbody);
					if(typeof(rsp)==='boolean'){
						add = rsp;
					}
				}
				
				if(uniqueCol!=null){
					var value = (typeof(send[uniqueCol].value)!=='undefined'? send[uniqueCol].value:send[uniqueCol]);
					var found = false;
					$tbody.find('tr:visible td[data-column="'+uniqueCol+'"] :input').each(function(){
						if(value == $(this).val()){
							var $tr = $(this).parent().parent();
							$tr.addClass('blinking');
							function onEndT(){
								$tr.removeClass('blinking');
							}
							$tr.onCSSAnimationEnd(onEndT);
							found = true;
							return false;
						}
						
					});
					add = !found;
				}

				if(add){
					$html.remove();
					$tbody.append(addElement(send,false));
					$tbody.append(addNewElement());
					if (typeof(config.afterCreateElement)==='function') {
						config.afterCreateElement(send,$tbody); 
					}
					if(page!==false){
						page.$.trigger('idcp.widgets.trigger.change',{name : _PLUGIN_NAME, id: _PLUGIN_ID, $element: $object});
					}
					$table.attr('data-ischanged',true);
				}
				else{
					console.warn("NO AFEGIM PQ NO COMPLEIX CONDICIÓ");
				}
				
			});
			return $html;
		}
		
		var getValues = this.getValues = function(){
			var formName = config.name;
			var values = [];
			if($table.attr('data-ischanged')=="true"){
				$tbody.find('tr[data-removed="false"]').each(function(){
					var a = (($(this).attr('data-removeonro')=== undefined)? true:false);
					if(a){
						var $input = $(this).find(':input[type="hidden"]');
						var elementData = {};
						$input.each(function(){
							elementData[$(this).attr('name')] = $(this).val(); 
							elementData["id_accion"] = $(this).parent().attr("id"); 

						});
						values.push(elementData);
					}
				});
			}
			
			var a = { form : formName, data : values, changed : ($table.attr('data-ischanged')=="true"? true:false)};
			return a;
		}
		
		var readOnlyF = this.readOnly = function(ro){
			readOnly = ro;
			$object.find('*[data-removeonro="true"]').remove();
		}
		
		if(page!==false){
			page.plugins(_PLUGIN_NAME,this);
			
			/**
			 * Event to rollback widget. Fired by page when restore_changes is clicked
			 * In this widget, on rollback we remove all tr which has data-committed=false
			 */
			page.$.on('idcp.widgets.event.rollback',function(){
				/*console.log(commitedData);
				console.log("ROLLBACK!");
				if($table.attr('data-ischanged')!=='false'){
					$tbody.html('');
					$.each(commitedData,function(){
						this.appendTo($tbody);
					});
					$tbody.append(addNewElement());
				}*/
				$tbody.find('tr[data-committed="false"]').each(function(){
					if(!$(this).attr("data-removed","true")){
						//console.log("NO ÉS VISIBLE, per tant és un element que existia que hem eliminat. El fem visible.");
						$(this).attr('data-committed',"true");
						$(this).show('fast');
					}
					else{
						//console.log("ÉS VISIBLE, per tant és un element que no existia i l'hem creat. L'eliminem");
						$(this).hide('fast',function(){
							$(this).remove();
						});
					}
				});
				$object.attr('data-ischanged',false);
				$table.attr('data-ischanged',false);
			});
		
			/**
			 * Event to apply changes on widget. Fired by page when the data is correctly saved on WebService
			 */
			page.$.on('idcp.widgets.event.commit',function(){
				console.log("COMMIT");
				page.$.trigger('idcp.widgets.event.getValues',{name : _PLUGIN_NAME, id: _PLUGIN_ID, $element: $object,values : []});
				$tbody.find('tr[data-committed="false"]').attr('data-committed',"true");
				
				var $tr = $tbody.find('tr');
				
				commitedData = [];
				$tr.each(function(){
					if($(this).attr('data-form-new-element')!=='true'){
						commitedData.push($(this));
					}
				});
			});
		}
		else{
			console.log("Page object not found");
		}
	}
	
	
	
	$.fn.extend({
		list : function(config){
			return new idcp_list(config,$(this));
		}
	});
})();