/**
 * Clase superView.
 * @author: phidalgo
 * @data: 30/03/2014
 * @projecte: IDCP 2014 Front End
 * 
 * TODO: Aquesta classe ha soferts MOLTS canvis entre la 1era i la 3era setmana d'Octubre 2015. Cal reestructurar la documentació. La veritat és que
 * ni jo mateix sé quantes possibles configuracions admet. Un dia m'ho hauré de mirar amb tranquil·litat per elaborar bé la documentació.
 * 
 * TODO: Aquesta classe ha sofert canvis considerables. La majoria de funcionalitats llistades segueixen funcionant, però s'han afegit d'altres que també són rellevants.
 *
 * Aquest objecte crea una taula configurable amb els paràmetres que se li passen.
 * Aquest objecte té associat una clase anomenada card en el backend que processa la petició automatizada i crea la consulta.
 * Par�metres de configuració. S'ha de passar en el constructor un objecte config amb la següent configuració. Alguns paràmetres són opcionals.
 *
 *	rest: String | REST URL a on anirà la petició.
 *	(opcional) title: String | Títol de la taula.
 *	(opcional) sortable: bool | Indica si les columnes ser�n ordenables.
 *	(opcional) defaultsort: Object {col:String _COLUMNA_DB_,sort: String _ORDRE_ (valors fixats: asc o desc)}
 *	(opcional) searchable: String | Camps per a on la cerca mirarà el string de la cerca. Valors separats amb coma.
 *	idTable: string | Identificador de la taula, el camp ID de l'element <table>
 *	(opcional) rowid: Object {fieldId: String _COLUMNA_DB_,startId: String _START_ID} | Si volem que cada <tr> tingui un identificador únic, li posem.
 *	(opcional) tipscol: bool | Indica si vols una columna addicional amb botons d'editar, eliminar, etc.
 *	(opcional) maxWidthtd: integer | Per si volem fixar un ample màxim per a cada columna. (No funciona massa b�, suposo que per interferencies amb classes del bootstrap).
 *	(opcional) dataType: array(String)[_COLUMNA_DB_] | Si hi és, afegeix atributs en el <td> del tipus data-type amb el nom de la columna de la db.
 *	(opcional) appendCode: function | Si hi és, cada cop que realitzem una petici� (per ordre, cerca, filtratge) s'executa aquesta funció, que pot servir per afegir configuracions personalitzades
 *	(opcional) paging: Object {defaultRowsPage: Integer,(opcional) rowsList: array(Integer)} | Si hi és, s'habilita la paginació, el primer camp determina el nombre de regisres per defecte.
 *		El segon camp, si hi és, afegeix un select amb els valors que s'hi posin, per que l'usuari pugui decidir el nombre de registres per pàgina.
 *			
 *	(opcional) cols: array(Object {colName: _COLUMNA_DB_, colTitle: String,sort: bool}). Si hi és, especifica quines columnes es veuràn, sino, es veuràn totes les columnes de la resposta 
 *		del WebService.
 *		Dels paràmetres dels objectes de l'array, especifiquem la columna de la base de dades i el titol. Addicionalment es pot afegir el camp sort, que si val false, especifiquem
 *		que aquella columna NO serà objecte d'ordenació - en cas que les columnes siguin ordenables.
 *			
 *	(opcional)filter: array(Object {field: _COLUMNA_DB_,type: String,title: String,dataSource: mixed array|function})
 *		Si hi és, afegeix filtres personalitzables a la taula.
 *		Dels paràmetres dels objectes de l'array:
 *			field: Columna de la DB a on es filtrarà
 *			type: Tipus de filtre HTML:
 *				-date: Si volem filtrar per un camp data (input type="text" class="date-picker").
 *				-date+ : Si volem filtrar per un camp data (indicant data major que).
 *				-date- : Si volem filtrar per un camp data (indicant data menor que).
 *				-text: Si volem filtrar per un camp de text (input type="text ...)
 *				-select: Si volem filtrar per un camp desplegable.
 *			title: Títol del camp.
 *			dataSource: En camps del tipus "text i select", seleccionem el m�tode de carrega d'informació. En el cas del text, està programat que pugui ser un autocomplete (JqueryUI)
 *				Es pot donar un array amb dades, que en el cas de select, value==text, o una funció on podrem personalitzar la informació.
 *			dbdata: Especifiquem el tipus de camp de filtre de la base de dades, si per exemple és un camp data farem un trunc(data) = valor, si és un text farem  upper(data)=valor, etc.
 *			
 *		S'ha de fer notar que en els camps de filtratge, si el valor de filtratge comen�a amb NOT_, el WS esta preparat per negar aquella condició.
 *		és a dir si jo tinc un filtre com:
 *			{field: "COLUMNA_TIPUS",type: "select", title: "Titol Columna Tipus", dataSource: ["Valor 1","NOT_Valor 1"],dbdata: 'text'}
 *			El valor "Valor 1" serà COLUMNA_TIPUS = "Valor 1".
 *			El valor "NOT_Valor 1" serà COLUMNA_TIPUS != "Valor 1"
 *
 *	Totes les funcions que s'espeficiquen aqui en la configuració, retornen un objecte JQUERY de l'element en questio. Per exemple, si fem una funció per omplir un select
 *	d'un filtre, podem crear la funció això function xxxx(input){} on input serà l'objecte JQUERY de l'element on hem d'afegir la informació.
 *	AppendCode però, retorna l'id de la taula, perque ha de ser lo suficientment gen�ric com per poder personalitzar algunes accions.
 *
 *
 *
 */
function idcp_superView(config,$primaryObject){
	var _PLUGIN_NAME = 'idcp.widgets.superview';
	var _PLUGIN_ID = $i.generator();
	/**
	 * Constants
	 */
	var _CARDS = 'CARDS';
	var _TABLE = 'TABLE';
	
	/**
	 * Methods
	 */
	this.HTMLcard = HTMLcard;
	this.fillConfig = fillConfig;
	this.mount = mount;
	this.newid = newid;
	this.create = create;
	this.reload = reload;
	this.request = request;
	this.dropButton = dropButton;
	this.rowClickEvent = rowClickEvent;
	this.fillCard = fillCard;
	this.addPaging = addPaging;
	this.addSearch = addSearch;
	this.addSorting = addSorting;
	this.addFilter = addFilter;
	this.fillFilter = fillFilter;
	this.appendFilterEventButton = appendFilterEventButton;
	this.tipsClickEvent = tipsClickEvent;
	this.done = done;
	this.fillDefaultView = fillDefaultView;
	this.setColumns = setColumns;
	this.isColumnSortable = isColumnSortable;
	this.setFilters = setFilters;
	this.updateFilterValues = updateFilterValues;
	this.export_to_excel =  export_to_excel;
	this.advancedSearch = advancedSearch; //Advanced search, advanced option on search filters.
	this.casearch = casearch; //Advanced search class.
	
	this.doneF = null;
	
	/**
	 * Properties
	 */
	this.$ = {}; //jQuery table/cards objects
	this.appendTo = $primaryObject; //Jquery object where the card will be appened 
	this.id; //The id of the <table> tag
	this.fieldid = ''; //If we want to set an id for each row (tr)
	this.restURL=''; //The URL of the RESTFULL API
	this.restMethod=''; //The method of the RESTFULL API (POST,GET...);
	this.data=null; //The result rows for the request
	this.dataType = []; //For each row (tr) this array saves the specified data-="" attribute if we want to add some extra information.
	this.filterable = false; //Sets if the current card is filterable.
	this.filterInfo = null; //Saves the specified filter information provided on the card config.
	this.filters = null; //Has the filters info provided by the user (the data filled on the input filters)
	this.sortInfo = null; //Saves the specified Sort information.
	this.searchFields = null; //Save the specified search fields on DB.
	this.searchString = ''; //Save the search string.
	this.searchable = false; //Sets if there will be a searchable list. default false;
	this.paginable = true; //Enables or disables paging system. default true
	this.availableRowsPage = ["10","25","50","100"]; //Array with the available rows per page selection
	this.rowsPage = 10; //Has the current rows per page
	this.title=''; //Title Card
	this.cols = null; //Array of specified columns.
	this.currentPage = 0; //Has the current page of the list.
	this.appendCode = null; //Every time we reload the list, we will execute this user function.
	this.pagingInfo = null; //If the recordset is paginated, we get the paging info.
	this.maxWidthtd= '200px'; //Sets the max td width. TODO:: This value are applied but does not work.
	this.sortable = true; //If enabled, we will can sort for any column. Default true
	this.enableTips = null; //If this is true, we will add a new col with helpfull buttons
 	this.rowClick = null; //If not null, we will add a click event for each row
	this.actionCol = null; //If there is a ':DROP_BUTTON' Column, we save it here.
	this.returnFields = null; //The returned fields on the recordset
	this.queryFields = null; //Available/Returned fields from API.
	this.queryFilters = null; //Available filter fields from API.
	this.querySortables = null; //Available sortable cols from API.
	this.config = (typeof config === 'object') ? config : null; // Optional Args.
	this.timeoutVar = null;
	this.settime = null;
	this.colResizable = true; //Default true.
	this.addRating = false;
	this.addRatingObject = null;
	this.addActions = false;
	this.pagingBusy = false;
	this.backgroundColor = false;
	this.backgroundColorObject = null;
	this.card = null;
	this.asearch = null;
	this.queryString = "";
	this.listType = _TABLE; //Type of view. Two options: _TABLE or _CARDS
	this.viewTemplates = []; //Això ho omplirà del WebService, de la configuració d'usuari.
	this.currentTemplate = 0; //Això ho omplirà del WebService, de la configuració d'usuari.
	this.pageButtons = null; //Page Buttons object.
	this.allowTemplates = false; //To enable/disable editing views (templates) for this superview. Default false.
	this.exportable = false; //To allow exporting to EXCEL (xlsx) format. Default false.
	this.addReloadButton = true; //To add a reload button on the page button's zone. (default true). 
	
	
	this.pageObject = null; //Page object where the widget will be instanciated.
	var mouseWheelEventCreated = false; //Pagination CARDS checking variables
	var lastScrollTop = 0;//Pagination CARDS checking variables
	var readOnly = false; //Set this plugin to be readonly.
	
		
	if(this.config!=null){
		var foundrowId = false;
		$.each(this.config,function(key,value){
			if(key=='rowId') foundrowId = true;
		});
		if(!foundrowId) throw new AppException(1001);
		this.fillConfig();
	}
	else{
		throw new AppException(1005);
	}

	function done(f){
		if(typeof(f)==='function'){
			this.doneF = f;
		}
	};
	var _this = this;
	
	this.id = _PLUGIN_ID; //Adds new unique ID. In fact is not really necessary, but i prefer to use unique id's for each plugin.
	this.pageObject = $i.pages.find.page($primaryObject.closest('div[data-pagehash]').data('pagehash'));
	
	this.create(); //Fill the card and instanciate the filters, search, sort if required.

	function fillConfig(){
		var _this = this;
		$.each(this.config, function(key,value){
			if(key == 'addReloadButton'){
				_this.addReloadButton = ((typeof(value)==='boolean')? value:true);
			}
			if(key == 'exportable'){
				_this.exportable = value;
			}
			if(key == 'allowTemplates'){
				_this.allowTemplates = value;
			}
			if(key == 'asearch'){
				_this.asearch = value;
			}
			if(key == 'type'){
				_this.listType = value.toUpperCase();
			}
			if(key=='card'){
				_this.card = value;
			}
			if(key=='addRating'){
				_this.addRating = true;
				_this.addRatingObject = value;
			}
			if(key=='addActions'){
				_this.addActions = true;
				_this.actions = value;
			}
			if(key=='idTable'){
				_this.id = value;
			}
			if(key=="rest"){
				if(typeof(value)==='object'){
					_this.restMethod = value.method || _GET_;
					_this.restURL = value.url;
				}
				else{
					_this.restURL = value;
					_this.restMethod = _GET_;
				}
			}
			if(key=="tdata"){
				_this.data = value;
			}
			if(key=="rowId"){
				_this.fieldid = value;
			}
			if(key=="dataType"){
				_this.dataType = value;
			}
			if(key=="actionCol"){
				_this.actionCol = value;
			}
			if(key=="tipscol"){
				_this.enableTips = value;
			}
			if(key=="sortable"){
				_this.sortable=value;
			}
			if(key=="defaultsort"){
				_this.sortInfo= value;
			}
			if(key=="maxWidthtd"){
				_this.maxWidthtd = value +'px';
			}
			if(key=="paginable"){
				_this.paginable = value;
			}
			if(key=="paging"){
				_this.paginable = true;
				_this.availableRowsPage = value.rowsList;
				_this.rowsPage = value.defaultRowsPage;
			}
			if(key=="appendCode"){
				_this.appendCode = value;
			}
			if(key=="cols"){
				_this.cols = value;
			}
			if(key=="title"){
				_this.title = value;
			}
			if(key=="searchable" && value!=null){
				_this.searchable = true;
				_this.searchFields = value;
			}
			if(key=="filter" && value!=null){
				_this.filterable = true;
				_this.filterInfo = value;
			}
			if(key=="rowClick"){
				if(typeof(value.onClick)!=='function'){
					throw new AppException(1006);
				}
				if(typeof(value.enable)==='undefined'){
					throw new AppException(1007);
				}
				_this.rowClick = {};
				_this.rowClick.enable = value.enable;
				_this.rowClick.onClick = value.onClick;
			}
			if(key=="refresh"){
				_this.settime = ((value*60000)<300000)? 300000:value*60000;
			}
			if(key=="colResizable"){
				_this.colResizable = value;
			}
			if(key=="returnFields"){
				_this.returnFields = value;
			}
		});
	}
	
	var loadingCards = $('<div class="grid-item icard cards-loading-content" style="position:relative;width:100%;opacity:0.8;"><div style="position:relative;margin-top:75px;text-align: center;font-weight:bold;"><i class="fa fa-circle-o-notch fa-spin red bigger-260" style="margin-top:0px!important;"></i><p>Loading new cards...</p></div></div>');

	
	/**
	 * This method changes the id for the generic-list.
	 */
	function newid(){
		this.$.main = $i('#'+this.id+'_superview_main',this.appendTo);
		this.$.header = $i('#'+this.id+'_superview_header',this.appendTo);
		this.$.content = $i('#'+this.id+'_superview_content',this.appendTo);
		this.$.wrapper = $i('#'+this.id+'_superview_wrapper',this.appendTo);
		this.$.length = $i('#'+this.id+'_superview_length',this.appendTo);
		this.$.search = $i('#'+this.id+'_superview_search',this.appendTo);
		this.$.filter = $i('#'+this.id+'_superview_filter',this.appendTo);
		this.$.table = $i('#'+this.id+'_superview_table',this.appendTo);
		this.$.paging = $i('#'+this.id+'_superview_paging',this.appendTo);
		this.$.options = $i('#'+this.id+'_superview_options',this.appendTo);
		this.$.cards = $i('#'+this.id+'_superview_masonry',this.appendTo);
	}
	
	function create(){
		this.pageButtons = (this.pageObject.buttons!==false? this.pageObject.buttons:null); //Get buttons if exist.
		this.HTMLcard();
		this.newid();
		
		/**
		 * Mirem si l'objecte de la pàgina actual té departaments. 
		 * Si té departaments, mirem si l'usuari pertany a algun o a tots els departaments.
		 * Si pertany a algun departament, la informació vindrà filtrada OBLIGATORIAMENT per aquest departament.
		 * Si pertany a tots, no vindrà filtrada per departament.
		 * Per tant, afegirem el filtre de departament en aquells casos on l'objecte de la pàgina tingui departament i l'usuari pertanyi a tots.
		 */
		if(this.pageObject!==false && this.pageObject._get('object').hasDepartment){
			if($i.user._get().user_department==null){
				if(this.filterInfo==null){
					this.filterInfo = [];
				}
				this.filterInfo.push({field: 'OWNER_DEPARTMENT',type: 'select',title:$i.i18n("widgets:col_owner_department")});
			}
		}
		
		/**
		 * Disable submit on filter/search when user clicks enter inside any input
		 */
		this.$.options.find('form').on('submit',function(){
			return false;
		});
		
		//In case of the view type is CARDS, set min rowsPerPage to 50.
		if(this.listType==_CARDS && this.paginable && this.rowsPage<=10){
			this.rowsPage = 50;
			this.$.table.hide();
			this.$.cards.show();
		}
		else{
			this.$.table.show();
			this.$.cards.hide();
		}

		this.fillDefaultView();
		this.mount(1);
		
		this.$.header.html(this.title); //Adds table title.
		this.addSearch(); //This method adds the search, if search config has been enabled
		
		if(this.listType==_CARDS){
			this.$.cards.masonry({
				itemSelector: '.icard', // options
				columnWidth: 5
			});
		}
		
		//Add refresh button.
		if(this.pageButtons!=null){
			if(this.addReloadButton){
				this.pageButtons.addExtraButton([{
					icon : 'fa fa-refresh',
					tooltip : $i.i18n('widgets:tlt_option_reload'),
					click : function(){_this.reload();}
				}]);
			}
		}
		else{
			console.log("Can not add refresh button because pageButtons is null. This page does not contain a pagebuttons object");
		}
		
		//Set advanced search.
		if(this.asearch!=null){
			this.casearch = new casearch(this.asearch);
			this.casearch.addButtons();
			this.asearch.show = this.casearch.render;
		}
		else{
			this.asearch = {};
			this.asearch.show = function(){console.log("Asearch is not configured, so render is not possible.");}
		}
		
		//Set view as exportable.
		if(this.exportable){
			if(this.pageButtons!=null){
				this.pageButtons.addExtraButton([{
					icon : 'fa fa-file-excel-o',
					tooltip : $i.i18n('widgets:tlt_option_export_excel'),
					click : this.export_to_excel
				}]);
			}
			else{
				console.log("Can not make this view exportable because pageButtons is null.  This page does not contain a pagebuttons object");
			}
		}
		
		//Set view as editable templates.
		if(this.allowTemplates){
			if(this.pageButtons!=null){
				this.pageButtons.addExtraButton([{
					icon : 'fa fa-eye',
					tooltip : $i.i18n('widgets:tlt_option_view'),
					click : function(){_this.renderConfig();}
				}]);
				
				this.pageButtons.addOtherElements([{
					tooltip : $i.i18n('widgets:tlt_change_view'),
					render : '<select name="view_config_templates"></select>', 
					event : 'change',
					execute : function(){
						_this.loadTemplate($(this).val())
					}
				}]);
			}
			else{
				console.log("Can not add template edition because pageButtons is null.  This page does not contain a pagebuttons object");
			}
		}
	}
	
	function reload(type){
		if(typeof(type)!=='undefined' && type!="" && type!=null && type!=this.listType){
			//console.log("Change view of type, from: " + this.listType+ " to " + type);
			this.listType=type;
			//Set some init values...
			this.currentPage = 0;
			if(this.listType==_CARDS){
				mouseWheelEventCreated = false;
				lastScrollTop = 0;
				this.rowsPage = 30;
				this.$.length.hide();
				this.$.table.find('tbody').html('');
				this.$.cards.html('');
				this.$.paging.html('');
				this.$.table.hide();
				this.$.cards.show();
				this.$.cards.masonry({
					itemSelector: '.icard', // options
					columnWidth: 5
				});
			}
			else{
				this.rowsPage = (typeof(this.$.length.find('select[name="rowsPerPage-length"]').val())!=="undefined")? this.$.length.find('select[name="rowsPerPage-length"]').val():this.rowsPage;
				this.$.table.find('tbody').html('');
				this.$.length.show();
				this.$.cards.html('');
				this.$.table.show();
				this.$.cards.hide();
			}
		}
		else{
			if(this.casearch!=null){
				if(this.casearch.currentSearch!=null){
					this.advancedSearch(this.casearch.currentSearch);
				}
			}
		}
		if(this.listType==_CARDS){
			this.currentPage = 1;
		}
		this.request(false,true); //Make Request
	}

	/**
	 * Makes the request to send the data in EXCEL file.
	 */
	function export_to_excel(){
		var $btn_export = null;
		var $jQModal = null;
		var $widget = null;
		var exporting = false;
		var notification_id = null;
		var modalClosed = false;
		function clickExportButtonF(){
			exporting = true;
			var excelFields = [];
			var $sel = $jQModal.find('form[name="form_options"]').find(':input');
			$sel.each(function(){
				var val = $(this).val();
				if(val!=""){
					excelFields.push($(this).val());
				}
			});
			
			$btn_export.off('click',clickExportButtonF);
 		   	$btn_export.html('<i class="fa fa-circle-o-notch fa-spin-2x white"></i> '+$i.i18n('widgets:bt_exporting'));
 		   	_this.request(true,true,function(rsp){
 		   		var export_URL = $i._get('api') + "file/temporary/"+$i.cookies._get("session_token")+"/"+rsp;
 		   		exporting = false;
 		   		if(modalClosed){
 		   			$i.notification.update(notification_id,{title : 'Export finished',link: export_URL });
 		   		}
 		   		else{
	 		   		$btn_export.html('<i class="ace-icon fa fa-cloud-download align-top bigger-125"></i> '+$i.i18n('widgets:bt_download'));
	 		   		$btn_export.on('click',function(){
	 		   			window.location = export_URL;
	 		   			$widget.close();
	 		   		});
 		   		}
 		   	},excelFields);
 		   //window.location = $i.ws_url + "file/temporary/"+$i.cookies._get("session_token")+"/"+data.data;
 	   }
		
		$i.modal({
			show: true,
			title : $i.i18n('widgets:title_option_export_excel'),
			width: '30%',
			description : renderExcelExp,
			icon : 'fa fa-file-excel-o',
			defaultButtons : false,
			buttons : [
	           {
	        	   text :$i.i18n('widgets:bt_export'),
	        	   'class' :"btn btn-sm btn-success btn-export", 
	        	   click : function(){}
		        }
			],
			execute : function(id,$modal,$buttons,$dialog,widget){
				$jQModal = $modal;
				$widget = widget;
				$btn_export = $buttons.find('.btn-export');
				$btn_export.on('click',clickExportButtonF);
			},
			success : function(){
				
			},
			cancel : function(){ //On close modal.
				if(exporting){ //If we start to export the excel but before it ends we close the modal window, add a notification event.
					var page = $i.pages.find.visible();
					notification_id = $i.notification.add({title : 'Exporting excel file...',icon : 'fa-file-excel-o',badge : '1'});
				}
				modalClosed = true;
			}
		});
		
		function renderExcelExp(){
			var render = '\
				<div class="row">\
					<div class="col-xs-12">\
						<form class="form-horizontal" role="form" name="form_options">';
						for(var i=0;i<_this.queryFields.available.length;i++){
							render += '<!-- B:LABEL_'+i+' -->\
							<div class="form-group">\
								<label class="control-label col-xs-12 col-sm-3 no-padding-right blue" for="label_'+i+'">'+$i.i18n('widgets:column_name',{key:(i+1)})+'</label>\
								<div class="col-xs-12 col-sm-9">\
									<select class="col-xs-12 col-sm-12 tag-input-style chosen" name="show_'+i+'">\
									<option value=""></option>';
									for(var k=0,l=_this.queryFields.available.length;k<l;k++){
										var av = _this.queryFields.available[k];
										render +='<option value="'+av.toLowerCase()+'" '+(i==k? 'selected':'')+'>'+$i.i18n(config.i18n+':col_'+av.toLowerCase())+'</option>';
									}
									render += '</select>\
								</div>\
							</div>\
							<!-- E:LABEL_'+i+' -->';
						}
					render += '</form></div>\
				</div>';
				return render;
		}
	}
	
	function advancedSearch(filters){
		//First, we clear all the previous filtered data.
		this.searchString = "";
		this.filters = filters.length==0? null:filters;
		this.addSearch(); //Adds search.
		this.addFilter(); //Adds Filter
		this.request(false,true);
	}
	
	/**
	 * This method creates a default view construction.
	 * In case of the user does not specify a custom view or this view has the customizable views disabled, this will be
	 * the view setup of the superview class.
	 */
	function fillDefaultView(){
		this.currentTemplate = 0; //Set default view to 0. In case of any of other views sets as default view, set it to that id.
		//Fill the default view.
		var cols = [];
		if(this.cols!=null){
			$.each(this.cols,function(){
				cols.push(constructColumns(this.name));
			});
		}
		var defaultV =  { 
			id: 0, 
			name: $i.i18n('widgets:default_view'),
			values : cols,
			type: this.listType,
			filters: [],
			rowsPerPage : this.rowsPage,
			defaultView : false,
			main: true
		}; //Default view.
		
		if(this.sortInfo!=null){
			defaultV.defaultOrder = this.sortInfo.substring(1).toLowerCase();
			defaultV.orderDirection = this.sortInfo.charAt(0);
		}

		this.viewTemplates[0] = defaultV;
		
		//Get USER UI SETUP AND UPDATE templates with users data.
		var pagesetup = $i.getPageUISetup($i.pages.find.visible().urlHash);
		if(pagesetup!=null && typeof(pagesetup.superview)!=='undefined' && pagesetup.superview!=null && typeof(pagesetup.superview.templates)!=='undefined'){
			this.currentTemplate = pagesetup.superview.main; //Set default view.
			$.each(pagesetup.superview.templates,function(){
				_this.viewTemplates.push(this);
				if(this.defaultView){
					_this.currentTemplate = this.id;
					_this.sortInfo = this.orderDirection+this.defaultOrder; //Set order and direction.
					_this.rowsPage = this.rowsPerPage; //Set rowsPerPage
					_this.setColumns(this.values); //Set columns
					_this.setFilters(this.filters); //Set filters from this template.
					_this.currentPage = 1; //On load the template, set currentPage to 1.
					if(this.type!=_this.listType){
						_this.reload(this.type);
					}
				}
			});
		}
	}
	
	
	/**
	 * Mount request
	 */
	function mount(page){
		if(this.settime!=null){
			$i.intervals.pages.clear('GRID:'+this.id);
		}
		var paged = (typeof page === 'undefined') ? null : page;
		
		var goToPage;
		if(paged=="prev"){
			goToPage = this.currentPage - 1;
		}
		else if(paged=="next"){
			goToPage = this.currentPage + 1;
		}
		else if(paged==null){
			goToPage = this.currentPage;
		}
		else{
			goToPage = paged;
		}
		//Get the max rows.
		var rowsPage = (typeof(this.$.length.find('select[name="rowsPerPage-length"]').val())!=="undefined")? this.$.length.find('select[name="rowsPerPage-length"]').val():this.rowsPage;
		
		//CARDS loads a new page after the current page so when apply some filters or search, we need to go to always to the first page.
		if(this.listType==_CARDS){
			//goToPage = 1;
		}

		this.rowsPage = rowsPage;
		this.currentPage = goToPage;
		this.request();
	}
	
	
	/**
	 * Make a Request to the webservice.
	 * if toExcel == true, make a request to create an XLSX.
	 * if reload == true, make a request even the request is the same that is saved.
	 * callbackF can be used to return data to the caller. (now only when make an excel request.
	 * excelFields option can be used when toExcel is true to send the collected columns to create the excel request.
	 * @param toExcel bool (optional, default false)
	 * @param reload bool (optional, default false)
	 * @param callbackF function (optional, default undefined).
	 * @param excelFields array (optional, default undefined).
	 */
	function request(toExcel,reload,callbackF,excelFields) {
		toExcel = (typeof(toExcel)!=='undefined')? toExcel:false;
		reload = (typeof(reload)!=='undefined')? reload:false;
		
		//Generate the queryString
		var qs = {};
		if(!toExcel){
			if(this.paginable){
				qs.page = this.currentPage;
				qs.rowspage = this.rowsPage;
			}
			if(this.searchable && this.searchString!=''){
				qs.fields = this.searchFields;
				qs.searchString = this.searchString;
			}
			if(this.sortable && this.sortInfo!=null){
				qs.sortfield = this.sortInfo;
			}
			if((this.filterable && this.filters!=null && this.filters.length>0)){
				for(var i=0;i<this.filters.length;i++){
					if(i==0){ //If there is at least 1 filter enabled, we instanciate the needed object vars.
						qs.filterfield = '';
						qs.filtervalue ='';
						qs.filtertype = '';
						if(typeof(this.filters[0].choice)!=='undefined') qs.filterchoice = '';
					} 
					qs.filterfield += this.filters[i].field+((i+1==this.filters.length)? '':',');
					qs.filtervalue += this.filters[i].value+((i+1==this.filters.length)? '':'|');
					qs.filtertype += this.filters[i].type+((i+1==this.filters.length)? '':',');
					if(typeof(this.filters[0].choice)!=='undefined') qs.filterchoice += this.filters[i].choice+((i+1==this.filters.length)? '':',');
				}
			}
		}
		
		//Add to the queryString the needed return fields.
		var rf = [];
		if(toExcel){
			rf = excelFields;
		}
		else if(this.returnFields==null){
			//console.log(this.cols);
			var lc;
			$.each(this.cols,function(){
				var lc = this.name.toLowerCase();
				if(!$.inArray2(lc,rf)){
					rf.push(lc);
				}
				if(typeof(this.alternativeCols)!=='undefined' && this.alternativeCols!=null){
					$.each(this.alternativeCols,function(){
						lc = this.name.toLowerCase();
						if(!$.inArray2(lc,rf)){
							rf.push(lc);
						}
					});
				}
				
			});
			
			//Check if DEFAULT SORT or fieldID or DATA-TYPE is in returnFields array.
			$.each(this.dataType,function(){
				lc = this.toLowerCase();
				if(!$.inArray2(lc,rf)){
					rf.push(lc);
				}
			});
			
			if(this.listType==_CARDS && this.addRating){
				lc = this.addRatingObject.field.toLowerCase();
				if(!$.inArray2(lc,rf)){
					rf.push(lc);
				}
			}
			
			lc = this.fieldid.toLowerCase();
			if(!$.inArray2(lc,rf)){
				rf.push(lc);
			}
			
			//If exits config.additionalFields array, we add it extra mandatory fields to be returned by the webservice.
			if(config.additionalFields){
				$.each(config.additionalFields,function(){
					lc = this.toLowerCase();
					if(!$.inArray2(lc,rf)){
						rf.push(lc);
					}
				});
			}
		}
		else{
			rf = this.returnFields;
		}
		
		qs.returnFields = rf; //Every time we make a request, we have to send the returned fields.
		
		qs.toxls = toExcel? "yes":"no";
		qs = $.param(qs);
		
		if(qs==this.queryString && !reload){
			console.log("No action because you want reload the page with the same settings.");
			this.pagingBusy = false;
			//console.log(qs);
			return;
		}
		this.queryString = qs;

		var queryString = (qs.length==0? '':'?'+qs);
		$i.promise._request(this.restMethod, this.restURL+queryString, null, true, true, this.$.table)
		.always(function(){
			if (_this.settime!=null){
				$i.intervals.pages.clear('GRID:'+_this.id);
				$i.intervals.pages._add(
					'GRID:'+_this.id,
					function(){_this.request();},
					_this.settime
				);
			}
		})
		.done(function(data) {
			if(toExcel){
				if(typeof(callbackF)==='function'){
					callbackF(data.data); //Return data to the calling resource.
				}
				else{
					window.location = $i._get('api') + "file/temporary/"+$i.cookies._get("session_token")+"/"+data.data;
				}
				 
			}
			else{
				if (_this.paginable) {
					if (typeof(data.data.query.page)!='undefined' && typeof(data.data.query.limit)!='undefined' && typeof(data.data.query.total)!='undefined' && typeof(data.data.query.count)!='undefined'){
						_this.pagingInfo = ({
							page: data.data.query.page,
							limit: data.data.query.limit,
							total: data.data.query.total,
							count: data.data.query.count
						});
					}
				}
				else { //As there is a limit for each row request on the webservice, we have to check if we have all the return rows for admin information.
					if(typeof(data.data.total)!=="undefined" && typeof(data.data.count)){
						if(data.data.total>data.data.count){
							if ($i._get('debug')){
								console.log("ALERT!!! The list is configured as paging:false, but there are "+data.data.total+" rows on DB and I have "+data.data.count+
										" due the limit paging restriction"+
										" You may have to change the list configuration and set paging:true");
							}
						}
					}
				}
				_this.queryFields = data.data.query.fields;
				_this.queryFilters = data.data.query.filters;
				_this.querySortables = data.data.query.sortables;
				_this.updateFilterValues();
				_this.addFilter();
				
				//In case of there is no set col property on superview and we have to apply the default view, default view will be set with no columns, so here we add all available cols
				if(_this.viewTemplates[0].values.length==0){
					var cols = [];
					$.each(_this.queryFields.available,function(){
						cols.push(constructColumns(this));
					});
					_this.viewTemplates[0].values = cols;
				}
				_this.fillCard(data); //Render superview.
			}
		});
	}
	
	/**
	 * From the filters returned by de API, get the available values and set it on the filter.
	 */
	function updateFilterValues(){
		if(!this.filterable){
			return;
		}
		$.each(this.filterInfo,function(){
			var renderedFilter = this;
			var type = renderedFilter.type.toLowerCase(); 
			if(type=="select" || type=="select2"){
				$.each(_this.queryFilters.available,function(){
					if(renderedFilter.field.toLowerCase()==this.name){
						if(typeof(renderedFilter.dataSource)==='undefined'){
							var dataSource = [];
							$.each(this.values.results,function(){
								var obj = {}
								var optionF = false;
								for(var prop in this){
									if(!optionF && (prop=="id" || prop.endsWith('_id') || prop=='option')){
										obj.option = this[prop];
										optionF = true;
									}
									else{
										obj.value = this[prop];
									}
								}
								dataSource.push(obj);
								
							});
							renderedFilter.dataSource = dataSource;
						}
					}
				});
			}
		});
	}
	
	/**
	 * Set new columns to render.
	 * @return array
	 */
	function setColumns(cols){
		this.cols = cols;
	}
	
	/**
	 * Get rendered columns
	 * @return array
	 */
	this.getColumns = function(){
		return this.cols;
	}
	
	/**
	 * Check if this column is sortable or not.
	 * @return boolean.
	 */
	function isColumnSortable(colName){
		for(var i=0,j=this.querySortables.available.length;i<j;i++){
			if(this.querySortables.available[i] == colName){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Get the visible value for each row and column.
	 * @return value || null
	 */
	this.getVisibleValue = function(row,colName){
		var ret = false;
		if(typeof(row[colName])!=='undefined' && row[colName]!=null){
			ret = row[colName];
		}
		if(colName==':empty_col'){
			ret =  '&nbsp;';
		}
		
		/*
		 * Això i el if comentat d'aqui just més a sota és per detectar del WebService el tipus de variable d'aquesta columna.
		 * Però el webservice sempre retorna text. Quan trobi la manera que desde el WebService es retorni correctament el tipus de variable, 
		 * això podrà tornar a funcionar.
		var colVarType = 'text';
		$.each(this.queryFields.returned,function(){
			if(this.field==colName){
				colVarType = this.type;
				return;
			}
		});
		*/

		//Check if this value is a date and create a date with the default format.
		//if(colVarType=='datetime' && $i.dateTimePattern.test(ret)){
		if($i._get('timePattern').test(ret)){
			ret = $i.times(ret).toLocalFormat().render(); //Server date to specific user set timeZone formatted to local language
		}
		return ret;
	}
	
	/**
	 * Adds the click event for each clicable row if click event is enabled. 
	 */
	function rowClickEvent(){
		var $trs = this.$.table.find('tbody > tr'); 
		$trs.css('cursor','pointer'); // Add a pointer to the click row tr.
		$trs.find('td[data-click!="no-click"]').on("click", function (event) {
			 event.preventDefault();
			 event.stopPropagation();
			 _this.rowClick.onClick($(this).parent());
		 });
	}
	
	/**
	 * Fill the view with the returned data from WEBSERVICE.
	 */
	function fillCard(data){
		var columns = [];
		var row;
		var $tbody = null;
		if(this.listType==_TABLE){
			$tbody = this.$.table.find('tbody');
		}
		else{
			$tbody = this.$.cards;
		}
		
		$.each(this.cols,function(){
			if(typeof(this.name)!=='undefined'){
				this.name = this.name.toLowerCase();
				columns.push(this);
			}
		});
		
		
		if(this.listType==_TABLE){
			var $thead = this.$.table.find('thead tr');
			$thead.html(''); //Empty HTML
			
			var button = "";
			//Render :DROP_BUTTON.
			if(!readOnly && this.actionCol!=null){
				var th = '<th ';
				th += 'data-column-sortable="no" style="width:100px!important;overflow:visible;"';
				button = ''+
				'<div class="span6">'+
				'<div data-id="action_column" class="btn-group">'+
				'<button data-toggle="dropdown" class="btn btn-info btn-sm dropdown-toggle" style="white-space: nowrap;">'+
					this.actionCol.colTitle+
					'<span class="ace-icon fa fa-caret-down icon-on-right"></span>'+
				'</button>'+
				'<ul class="dropdown-menu dropdown-info" style="text-align:left;margin-left:3px;">';
				
				$.each(this.actionCol.actions,function(){
					button += ''+
						'<li data-action="'+this.action+'">'+
							'<a href="#">'+
								'<div class="row-fluid">'+
									'<span class="'+((typeof(this.icon)!=='undefined')? this.icon:'')+' bigger-150 pull-right"></span>'+
									'<span class="pull-left">'+this.title+'</span>'+
									'<br />'+ //TODO: la classe row posa un background color blue (difuminat) i posa paddings, suposo que deu ser el ACE, i no la puc fer servir. Per això afegeixo <br />, però queda pendent de REVISIÓ!
								'</div>'+
							'</a>'+								
						'</li>';
				});
				if(this.actionCol.help){
					button += '<li class="divider"></li>'+
					'<li data-action="ALL"><a href="#"><div class="row-fluid"><span class="ace-icon fa fa-check-square-o bigger-150 blue pull-right"></span><span class="pull-left">'+$i.i18n("widgets:drop_button_all")+'</span><br /></div></a></li>'+
					'<li data-action="NONE"><a href="#"><div class="row-fluid"><span class="ace-icon fa fa-square-o bigger-150 blue pull-right"></span><span class="pull-left">'+$i.i18n("widgets:drop_button_none")+'</span><br /></div></a></li>';
				}
				button += ''+
					'</ul>'+
				'</div><!-- /btn-group -->'+
				'</div>';
				th += ' class="center sorting_disabled" aria-label=" " style="min-width: 100px; max-width: 100px!important;width:100px;" ';
				th += ' data-resizable="false" ';
				if(this.maxWidthtd!=''){
					th += 'style="max-width:'+this.maxWidthtd+'" ';
				}
				
				th += ' role="columnheader" rowspan="1" colspan="1">';
				th += button;
				th += '</th>';
				$thead.append(th);
				this.dropButton();
			}
			
			//Render columns.
			$.each(columns,function(){
				var colTitle = (typeof(this.text)!=='undefined'? this.text:$i.i18n(config.i18n+':col_'+this.name));
				th = '<th ';
				
				colSortable = _this.isColumnSortable(this.name)? true:false;
				if(_this.sortable && colSortable){
					th += 'aria-controls="'+_this.id+'" data-column-sortable="yes" ';
					if(_this.sortInfo!=null && _this.sortInfo.substring(1,_this.sortInfo.length).toLowerCase()==this.name){
						order = _this.sortInfo.charAt(0);
						column = _this.sortInfo.substring(1,_this.sortInfo.length);
						th += 'class="'+(order=="+"? "sorting_asc":"sorting_desc")+'" data-sortable="'+this.name+'" tabindex="0" aria-sort="'+((order=='+')?+ 'ascending':'descending')+'" aria-label="'+colTitle+': activate to sort column"';
					}
					else{
						th += 'class="sorting" data-sortable="'+this.name+'" aria-label="'+colTitle+': activate to sort column"';
					}
				}
				else{
					th += 'data-column-sortable="no"';
				}
				if(_this.maxWidthtd!=''){
					th += 'style="max-width:'+_this.maxWidthtd+'" ';
				}
				
				th += ' role="columnheader" rowspan="1" colspan="1">';
				th += colTitle;
				th += '</th>';
				$thead.append(th);
			});
			
			if(!readOnly && this.enableTips){
				$thead.append('<th style="min-width: 100px;max-width:100px!important;width:100px;" data-column=":TIPS" role="columnheader" tabindex="0" aria-controls="'+this.id+'" rowspan="1" colspan="1" data-resizable="false">'+$i.i18n("widgets:actionColName")+'</th>');
			}
		}
		
		if(this.listType==_CARDS){
			if($tbody.find(loadingCards).length>0){
				loadingCards.remove();
			}
			if(this.currentPage==1){
				//CARDS view does not clean container when paginating, so when apply filters, does not clean too. So in that case, if we filter,
				//we need to clean here the container.
				$tbody.html('').masonry('layout'); //Force a tbody clean.
			}
			$tbody.data('masonry').layout();
		}
		else{
			$tbody.html(''); //Force a tbody clean.
		}
		 
		//If there is no results, create an advice to the user.
		if(data.data.results.length==0){
			if(this.listType==_CARDS){
				$i.gritter({
					type: "warning",
					title: $i.i18n('widgets:no_results'),
					description: ""
				});
			}
			else{
				$tbody.append('<tr><td colspan="'+columns.length+((this.enableTips)? 1:0)+'"><span style="text-align:center;"><h3>'+$i.i18n("widgets:no_results")+'</h3></span></td></tr>');
			}
		}
		else{
			var rowCount = 0;
			var par = false;
			$.each(data.data.results, function(){
				row = this;
				var content = '';
				if(_this.listType==_TABLE){
					var td = '';
					var tr = '<tr class="'+(par? "odd":"even")+'" ';
					par = !par;
					
					if(_this.fieldid!=''){
						fid = _this.fieldid.toLowerCase();
						tr+= 'data-id="'+row[fid]+'" ';
					}

					$.each(_this.dataType,function(key,value){
						value = value.toLowerCase();
						tr+= 'data-'+value+'="'+row[value]+'" ';
					});
					tr += 'data-tr-number="'+rowCount+'">';
				}
				
				if(!readOnly && _this.listType==_TABLE && _this.actionCol!=null){
					td += '<td ';
					if(_this.maxWidthtd!=''){
						td+= 'style="max-width:'+_this.maxWidthtd+';" ';
					}
					td += ' data-click="no-click" data-td-column=":DROP_BUTTON" class="center" style="text-align:center;'+(_this.maxWidthtd!=''? 'max-width:'+_this.maxWidthtd+';':'')+'" >'+
					'<label class="position-relative">'+
							'<input name="action_checkbox_tr" type="checkbox" class="ace" />'+
							'<span class="lbl"></span>'+
						'</label>'+
					'</td>';
				}
				
				var totalRenderedCols = 0;
				for (var i=0;i<columns.length;i++) {
					if (typeof(columns[i].dataType)==='undefined'){
						columns[i].dataType = 'TEXT';
					}

					if(_this.listType==_TABLE){
						td += '<td ';
						if(typeof(columns[i].align)!=='undefined'){
							td += ' class="'+columns[i].align+'"';
						}
						else{
							td += 'style="padding-left:5px!important;"';
						}
					}
					
					//If the column row exists, we show it, if not, we create a blanc row.
					var value = _this.getVisibleValue(row,columns[i].name);
					if(value!==false){
						if(_this.listType==_TABLE){
							td +=' data-column="'+columns[i].name+'" data-value="'+row[columns[i].name]+'"><span>'+value+'</span></td>';
						}
						else{
							content += '<p style="overflow:hidden!important;white-space: nowrap!important;"';
							content +=' data-td-column="'+columns[i].name+'" data-td-'+columns[i].name+'="'+value+'"><span>'+value+'</span></p>';
						}
						totalRenderedCols++;
					}
					
					if(_this.listType==_TABLE){
						td +=' data-column="'+columns[i].colName+'" data-value=""><span></span></td>';
					}
				}
				
				if(_this.listType==_TABLE){
					if(!readOnly && _this.enableTips!=null){
						td+='<td data-td-column="ACTIONS" data-click="no-click">'+
							'<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">';
						if(_this.enableTips.success){
							td +='<button data-button-action="button-success" class="btn btn-xs btn-success" data-istooltip="true">'+
							'<i class="ace-icon fa fa-check bigger-120"></i>'+
						'</button>';
						}
						if(_this.enableTips.edit){
							td += '<button data-button-action="button-edit" class="btn btn-xs btn-info" data-istooltip="true">'+
							'<i class="ace-icon fa fa-pencil bigger-120"></i>'+
							'</button>';
						}
						
						if(_this.enableTips.flag){
							icon = _this.enableTips.flagIcon ? _this.enableTips.flagIcon:"fa-user";
							td +='<button data-button-action="button-flag" class="btn btn-xs btn-warning" data-istooltip="true">'+
							'<i class="ace-icon fa '+icon+' bigger-120"></i>'+
							'</button>';
						}
						
						if(_this.enableTips.del){
							td += 	'<button data-button-action="button-delete" class="btn btn-xs btn-danger" data-istooltip="true">'+
							'<i class="ace-icon fa fa-trash-o bigger-120"></i>'+
							'</button>';
						}
			
							td += '</div>'+
			
							'<div class="visible-xs visible-sm hidden-md hidden-lg">'+
								'<div class="inline position-relative">'+
									'<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-istooltip="false">'+
										'<i class="ace-icon fa fa-cog icon-only bigger-110"></i>'+
									'</button>'+
			
									'<ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">';
									if(_this.enableTips.success){
										td += '<li>'+
											'<a href="#" class="tooltip-info" data-rel="tooltip" title="" data-original-title="Success" data-istooltip="true" data-button-action="button-success">'+
												'<span class="blue">'+
													'<i class="ace-icon fa fa-check bigger-120"></i>'+
												'</span>'+
											'</a>'+
										'</li>';
									}
									if(_this.enableTips.edit){
										td += '<li>'+
											'<a href="#" class="tooltip-success" data-rel="tooltip" title="" data-original-title="Edit" data-istooltip="true" data-button-action="button-edit">'+
												'<span class="green">'+
													'<i class="ace-icon fa fa-pencil bigger-120"></i>'+
												'</span>'+
											'</a>'+
										'</li>';
									}
									if(_this.enableTips.del){
										td += '<li>'+
											'<a href="#" class="tooltip-error" data-rel="tooltip" title="" data-original-title="Delete" data-istooltip="true" data-button-action="button-delete">'+
											'<span class="red">'+
													'<i class="ace-icon fa fa-trash-o bigger-120"></i>'+
												'</span>'+
											'</a>'+
										'</li>';
									}
									if(_this.enableTips.flag){
										td += '<li>'+
										'<a href="#" class="tooltip-flag" data-rel="tooltip" title="" data-original-title="Flag" data-istooltip="true" data-button-action="button-flag">'+
											'<span class="orange">'+
													'<i class="fa fa-flag bigger-120"></i>'+
												'</span>'+
											'</a>'+
										'</li>';
									}
									'</ul>'+
								'</div>'+
							'</div>'+
						'</td>';
					}
					$tbody.append(tr+td+'</tr>');
				}
				else{
					if(_this.addActions || _this.addRating){
						content += '<div style="position:absolute;padding-top:5px;bottom:0px;height:30px;border-top:2px solid #989898;right:5px;left:5px;">';
					}
					if(_this.addRating){
						content += '<div style="float:left;" class="rating" data-score="'+row[_this.addRatingObject.field.toLowerCase()]+'"></div>';
					}
					else{
						content += '<div style="float:left;">&nbsp;</div>';
					}
					if(!readOnly && _this.addActions){
						content += '<div data-actions="true" style="position:absolute;right:5px;">';
						for(var name in _this.actions){
							if(name.toUpperCase()=='DELETE' && _this.pageObject!=false && _this.pageObject._get('object')!=false && _this.pageObject._get('object').hasGranularP){
								if($i.security.access(_this.pageObject._get('object').name).del(row["owner_uid"],row["owner_gid"])){
									content += '<i class="fa '+_this.actions[name].icon+' bigger-160" style="cursor:pointer;margin-left:15px;" data-action="'+name+'"></i>';
								}
							}
							else{
								content += '<i class="fa '+_this.actions[name].icon+' bigger-160" style="cursor:pointer;margin-left:15px;" data-action="'+name+'"></i>';
							}
							
						}
						content += '</div>';
					}

					
					var doubleSize = "";
					if(totalRenderedCols>5){
						doubleSize = 'icard-size-2';
					}
					
					var card = '<div class="grid-item icard '+doubleSize+'"';
					
					if(_this.fieldid!=''){
						fid = _this.fieldid.toLowerCase();
						card+= 'data-id="'+row[fid]+'" ';
					}

					$.each(_this.dataType,function(key,value){
						card+= 'data-'+value+'="'+row[value]+'" ';
					});
					
					card += 'data-card-number="'+rowCount+'">';
					var $card = $(card+content+'</div>');
					$tbody.append($card).masonry( 'appended', $card ); // add and lay out newly appended items
					
					if(typeof(_this.card)==='function'){
						_this.card($card,row);
					}
				}
				rowCount++;
			});
			
			if(_this.listType==_TABLE){
				if(!readOnly && _this.enableTips!=null){
					_this.tipsClickEvent();
				}
				
				if(!readOnly && _this.rowClick!=null && _this.rowClick.enable){
					_this.rowClickEvent();
				}
			}
		}
		
		if(_this.listType==_CARDS){
			if(_this.addRating && data.data.results.lenght>0){
				try{
					$i('.rating').rating({
						cancel : false,
						half : false,
						readOnly : true,
						value : function(){
							return $(this).attr('data-score');
						}
					});
				}
				catch(e){
					console.log(e);
				}
			}
			
			if(!readOnly && this.addActions){
				for(var name in _this.actions){
					this.$.cards.find('i[data-action="'+name+'"]').off('click');
					this.$.cards.find('i[data-action="'+name+'"]').on('click',function(){
						var ac = $(this).data('action');
						_this.actions[ac].action($(this).parent().parent().parent());
					});
				}
			}
		}
		

		//This method adds the paging system, if paging config has been configured.
		//As the page change every time, we update the paging state when the data is already recovered and filled.
		this.addPaging();
		
		//This method adds the sortable system, if sortable config has been enabled.
		//As the page changes as the users performs any action, we update the paging state when the data is already recover and filled.
		//The sort is only available when we have columns... and it is only on TABLE view.
		if(this.listType==_TABLE){
			this.addSorting();
		}
		
		
		//Execute the user's code if it has been provided.
		if(this.appendCode!=null){
			if(typeof(this.appendCode)==='function'){
				var results = data.data.results.length==0? false:true;
				this.appendCode(this.$.table,results);
			}	
			else{
				window[this.appendCode](this.id);
			}
		}
		
		//Here we have to call to done function
		if(typeof(this.doneF)==='function'){
			this.doneF();
		}
		
		if(_this.listType==_CARDS){
			_this.pagingBusy = false;
		}
		else if(_this.colResizable){
			/**
			 * Enable column resizable method.
			 * @livedrag : update in hot.
			 * @partialRefresh : if the content will be dynamically updated.
			 * @postbackSafe : on content updated, mantain col size.
			 * TODO: Disabling on 20160308
			 */
			//this.$.table.colResizable({liveDrag:false,partialRefresh:true,postbackSafe:true,minWidth:100});
		}
	}
	
	/**
	 * Adds the paging functionality if the paging system is enabled.
	 */
	function addPaging(){
		if(!this.paginable){ return;} //If not paginable, return.
		
		var doPaging = true;
		//If there is no paginginfo provided in the recordset, we can not paging. abort.
		if(this.pagingInfo==null){
			if($i._get('debug')){
				console.log("Paging Information not found. I can not do the pagination");
			}
			doPaging = false;
			//Clear paging-related content
			this.$.paging.html('');
		}
		else if(this.rowsPage>=this.pagingInfo.total){ //If there are less rows than the specified rowsperpage, no paging.
			if($i._get('debug')){
				//console.log("Hide the paging because there are more rowsperpage than total rows");
			}
			doPaging = false;
			//Clear paging-related content
			this.$.paging.html('');
		}
		
		if(this.listType==_CARDS && !mouseWheelEventCreated){
			mouseWheelEventCreated = true;
			//console.log("mousewheel event attached");
			this.$.cards.on('mousewheel', function(e){
				var scrollDown = false;
				var st = $(window).scrollTop();
				if (st > lastScrollTop){
					scrollDown = true;
				}
				lastScrollTop = st;
				if(doPaging && scrollDown){
					//console.log("Dopaging true mousewheel event triggered");
					try{
						var currHeight = parseInt($(window).scrollTop())+parseInt($(window).height());
						//console.log(currHeight +">"+ _this.$.cards.height() +" && "+ !_this.pagingBusy);
						if(currHeight > _this.$.cards.height() && !_this.pagingBusy) {
							_this.pagingBusy = true;
							if(typeof(_this.currentPage)==='undefined'){
								npage = 1;
							}
							else{
								npage = _this.currentPage + 1;
							}
							//console.log("Current Page : " + npage);
							var $tb = $('#'+this.id);
							$tb.append(loadingCards)
						    // add and lay out newly appended items
						    .masonry( 'appended', loadingCards);
							_this.mount(npage);
						}
					}
					catch(e){
						console.log(e);
					}
				}
				else{
					//console.log("Dopaging false mousewheel event triggered");
				}
			});
		}
		
		if(doPaging){
			if(this.listType!=_CARDS){
				this.$.options.removeClass("hide");
				if(this.availableRowsPage!=null){
					//If paging, we add a selectable max row per page select.
					var selectMaxRows = '<label>'+$i.i18n('widgets:paging_select_display_text')+ ' &nbsp;'+
					'<select size="1" name="rowsPerPage-length" aria-controls="'+this.id+'">';
					for(var i=0;i<this.availableRowsPage.length;i++){
						selectMaxRows +='<option value="'+this.availableRowsPage[i]+'" '+((this.rowsPage==this.availableRowsPage[i])? 'selected="selected"':'')+'>'+
							this.availableRowsPage[i]+
						'</option>';
					}
					selectMaxRows +='</select> '+$i.i18n('widgets:paging_select_records_text')+'</label>';
					this.$.length.html(selectMaxRows);
					//Event onchange to perform the change of the max rows per page action.
					this.$.length.find('select[name="rowsPerPage-length"]').change(function(event){
						event.preventDefault();
						event.stopPropagation();
						_this.mount();
					});
				}
				
				var from = (this.pagingInfo.page-1)*this.pagingInfo.limit +1;
				var to = (this.pagingInfo.page*this.pagingInfo.limit) - this.pagingInfo.count*(this.pagingInfo.limit - this.pagingInfo.count);
				var max_pages = (Math.ceil(this.pagingInfo.total/this.pagingInfo.limit)); //var n = 4.3; alert(Math.ceil(n)); <-- alerts 5
				var paginationHTML = '<div class="row">'+
							'<div class="col-sm-6">'+
								'<div class="dataTables_info" id="'+this.id+'_info">'+
								$i.i18n('widgets:paging_select_showing_text')+' <strong>'+from+'</strong> '+$i.i18n('widgets:paging_select_to_text')+' <strong>'+to+'</strong> '+$i.i18n('widgets:paging_select_of_text')+' <strong>'+this.pagingInfo.total+'</strong> '+$i.i18n('widgets:paging_select_entries_text')+
								'</div>'+
							'</div>'+
							'<div class="col-sm-6">'+
							'<div class="dataTables_paginate paging_bootstrap">'+
								'<ul class="pagination">'+
								'<li class="prev '+((this.pagingInfo.page==1)? 'disabled':'')+'">'+
									'<a data-pagination="prev" href="#prev">'+
									'<i class="ace-icon fa fa-angle-double-left"></i>'+
									'</a>'+
								'</li>';
				if(max_pages<=10){
					for(var i=1;i<max_pages+1;i++){
						paginationHTML += '<li class="'+((i==this.pagingInfo.page)? 'active':'')+'"><a data-pagination="'+i+'" href="#'+i+'">'+i+'</a></li>';
					}
				}
				else{
					var from = 1;
					if(this.pagingInfo.page>2){
						from = this.pagingInfo.page-2;
					}
					var to = from+4;
					to = to>max_pages? max_pages:to;
					//console.log("From: " + from + ", to: " + to + ", current page: " + this.pagingInfo.page);
					if(from+2>4){
						paginationHTML += '<li class=""><a data-pagination="1" href="#1">1</a></li>';
						paginationHTML += '<li class=""><a data-pagination="middle" href="#middle">...</a></li>';
					}
					for(var i=from;i<=to;i++){
						if(i!=max_pages){
							paginationHTML += '<li class="'+((i==this.pagingInfo.page)? 'active':'')+'"><a data-pagination="'+i+'" href="#'+i+'">'+i+'</a></li>';
						}
					}
					if(to+2<=max_pages){
						paginationHTML += '<li class=""><a data-pagination="middle" href="#middle">...</a></li>';
					}
					paginationHTML += '<li class="'+((max_pages==this.pagingInfo.page)? 'active':'')+'"><a data-pagination="'+max_pages+'" href="#'+max_pages+'">'+max_pages+'</a></li>';
					
				}
				
				paginationHTML +='<li class="next '+((this.pagingInfo.page==max_pages)? 'disabled':'')+'">'+
				'<a data-pagination="next" href="#next"><i class="ace-icon fa fa-angle-double-right"></i></a></li></ul>'+
				'</div></div></div>';
				
					
				//Append paging info to the content.
				this.$.paging.html(paginationHTML);
				//Attach events to the pagination button elements.
				this.$.paging.find("ul.pagination a").click(function(event){
					event.preventDefault();
					event.stopPropagation();
					//Check if the button is disabled, and if it, then do nothing.
					if(!$(this).parent().hasClass("disabled")){
						if($(this).data("pagination")!=='middle'){
							_this.mount($(this).data("pagination"));
						}
					}
					else{
						if($i._get('debug')){
							console.log("Disabled button. No action is allowed!");
						}
					}
				});
			}
		}
	}
	
	/**
	 * Adds the search functionality if the search is enabled.
	 */
	function addSearch(){
		var _this = this;
		if (this.searchable){
			this.$.options.removeClass("hide");
			var fields = this.searchFields.split(',');
			
			var popupInfo = $i.i18n('widgets:search_popup')+' ';
			for(var i=0;i<fields.length;i++){
				if(i+2==fields.length){
					popupInfo +=getColTitleByColName(this.cols,fields[i])+' '+$i.i18n('lbl_and')+' ';
				}
				else{
					popupInfo += getColTitleByColName(this.cols,fields[i])+", ";
				}
			}
			popupInfo = popupInfo.substring(0, popupInfo.length - 2);
		
			var inputsearch= ''+
			'<input class="col-sm-12 col-xs-12" name="superview-search" autocomplete="off" type="text" value="'+((this.searchString!=null)? this.searchString:'')+'" placeholder="'+$i.i18n('widgets:search_placeholder')+'" title="'+popupInfo+'"/>';
						
			this.$.search.html(inputsearch);
			
			this.$.search.find('input[name="superview-search"]').tooltip({
				show: {
					effect: "slideDown",
					delay: 250
				}
			});
			
			$('[data-rel=popover]').popover({container:'body'});

			
			// Event onchange in input element to perform the search action
			this.$.search.find('input[name="superview-search"]').keyup(function(event){
				event.preventDefault();
				event.stopPropagation();
				// Prevent search length values between 1 and 3.
				// Length valueo of 0 is allowed for clear results
				if ($(this).val().length>3 || $(this).val().length==0) {
					_this.searchString = $(this).val();
					_this.request();
					//_this.mount(_this.currentPage, $(this).val());
				}
				return false;
			});
		}
	}
	
	/**
	 * Adds the column sort functionality if the sort is enabled.
	 */
	function addSorting(){
		if(!this.sortable) return false;		
		//Event click on each thead td column name to perform the sort action
		this.$.table.find('thead > tr > th[data-column-sortable="yes"]').click(function(event){
			event.preventDefault();
			event.stopPropagation();
			if(typeof($(this).data("sortable"))!=="undefined"){
				var $col = $(this);
				if($col.hasClass("sorting_asc")){
					_this.sortInfo = '-'+$col.data("sortable");
				}
				else if($col.hasClass("sorting_desc")){
					_this.sortInfo = null;
				}
				else{
					_this.sortInfo = '+'+$col.data("sortable");
				}
				_this.request();
			}
		});
	}
	
	/**
	 * Add's the filter functionality, if any filter is configured.
	 */
	function addFilter(){
		//console.log("addFilter");
		this.$.filter.html(''); //Clean filters zone.
		if(!this.filterable){
			return;
		}
		this.$.options.removeClass("hide");
		var value;

		$.each(this.filterInfo,function(k,filter){
			value = _this.fillFilter(filter.field);
			var output = '';
			output += '<div class="form-group">';
			switch(filter.type){
				case 'date+':
				case 'date-':
				case 'date':
					output +=''+
					'<label for="filter_date">'+filter.title+'</label>'+					
					'<div><span class="input-icon">'+
					'<input type="text" data-id="'+filter.field.toLowerCase()+'_filter" name="'+filter.field.toLowerCase()+'_filter" data-type="'+filter.type+'" '+((value!=null)? 'value="'+value+'"':'')+' class="form-control date-picker input-mask-date" data-date-format="dd/mm/yyyy" title="'+$i.i18n('widgets:filter_tooltip_date')+'">'+
						'<i class="ace-icon fa fa-calendar blue"></i>'+
					'</span></div></div>';
					_this.$.filter.append(output);
					var $input = _this.$.filter.find('input[name="'+filter.field.toLowerCase()+'_filter"]');
					$input.tooltip({
						show: {
							effect: "slideDown",
							delay: 250
						}
					});
					$input.dpicker();
					break;
				case 'daterange':
					output +=''+
					'<label for="filter_date">'+filter.title+'</label>'+					
					'<div><span class="input-icon">'+
					'<input type="text" data-id="'+filter.field.toLowerCase()+'_filter" name="'+filter.field.toLowerCase()+'_filter" data-type="'+filter.type+'" '+((value!=null)? 'value="'+value+'"':'')+' class="form-control date-picker input-mask-date">'+
						'<i class="ace-icon fa fa-calendar blue"></i>'+
					'</span></div></div>';
					_this.$.filter.append(output);
					var $input = _this.$.filter.find('input[name="'+filter.field.toLowerCase()+'_filter"]');
					$input.daterange();
					break;
				case 'text':
					output +=''+
					'<label for="filter_text">'+filter.title+'</label>'+
					'<input type="text" name="'+filter.field.toLowerCase()+'_filter" data-type="'+filter.type+'" '+((value!=null)? 'value="'+value+'"':'')+' class="form-control ui-autocomplete-input" autocomplete="off" title="'+$i.i18n('widgets:filter_tooltip_text')+'">'+
					'</div>';
					_this.$.filter.append(output);
					var $input = _this.$.filter.find('input[name="'+filter.field.toLowerCase()+'_filter"]');
					if(typeof(filter.dataSource)!=="undefined"){
						if($.isArray(filter.dataSource)){
							$input.autocomplete({source: filter.dataSource});
						}
						else if(typeof(filter.dataSource) === "function"){
							filter.dataSource($input);
						}
					}
					$input.tooltip({show:{effect: "slideDown",delay:250}});
					break;
				case 'select':
					defaultOption = (typeof(filter.defaultOption)!=="undefined")? filter.defaultOption:null;
					output +=''+
					'<label for="filter_select">'+filter.title+'</label><br />'+
					'<select name="'+filter.field.toLowerCase()+'_filter" data-type="'+filter.type+'" class="form-control" title="'+$i.i18n('widgets:filter_tooltip_select')+'">'+
					'<option value="">'+$i.i18n('widgets:filter_tooltip_select_option_any')+'</option>'+
					'</select>'+
					'</div>';
					_this.$.filter.append(output);
					
					var $select = _this.$.filter.find('select[name="'+filter.field.toLowerCase()+'_filter"]');
					if(typeof(filter.dataSource)!=="undefined"){
						if($.isArray(filter.dataSource)){
							defaultOption = ((value!=null)? value:defaultOption);
							$.each(filter.dataSource,function(){
								//console.log(this);
								$select
								.append($("<option "+((defaultOption==this.option)? 'selected':'')+"></option>")
								.attr("value",this.option)
								.text(this.value)); 
							}); 
						}
						else if(typeof(filter.dataSource) === "function"){
							filter.dataSource($select);
							$select.find('option').each(function(){
								if($(this).val() == value){
									$(this).attr("selected","selected");
								}
							});
						}
					}
					$select.tooltip({show:{effect: "slideDown",delay:250}});
					break;
				case 'select2':	//TODO: Amb width: 'resolve', de vegades no es veu bé el text en el select
					defaultOption = (typeof(filter.defaultOption)!=="undefined")? filter.defaultOption:null;
					output +=''+
					'<label for="filter_select2">'+filter.title+'</label><br />'+
					'<select name="'+filter.field.toLowerCase()+'_filter" data-type="'+filter.type+'" class="chosen">'+
					'<option value="">'+$i.i18n('widgets:filter_tooltip_select_option_any')+'</option>'+
					'</select>'+
					'</div>';
					
					_this.$.filter.append(output);
					var $select = _this.$.filter.find('select[name="'+filter.field.toLowerCase()+'_filter"]');
					if(typeof(filter.dataSource)!=="undefined"){
						if($.isArray(filter.dataSource)){
							defaultOption = ((value!=null)? value:defaultOption);
							$.each(filter.dataSource,function(){
								if(typeof(this)==='object'){
									$select
									.append($("<option "+((defaultOption==this.option)? 'selected':'')+"></option>")
									.attr("value",this.option)
									.text(this.value));
								}
								else{
									$select
									.append($("<option "+((defaultOption==this)? 'selected':'')+"></option>")
									.attr("value",this)
									.text(v));
								}
							});
							$select.select2();
						}
						else if(typeof(filter.dataSource) === "function"){
							var response = filter.dataSource($select);
							if(typeof(response)!== 'undefined'){
								response.done(function(){
									$select.chosen({allow_single_deselect:true,width: '100%'});
								});
							}
							else{
								$select.find('option').each(function(){
									if($(this).val() == value){
										$(this).attr("selected","selected");
									}
								});
								$select.chosen({allow_single_deselect:true,width: '100%'});
							}
						}
					}
					break;
			}
		});
		if(this.filterInfo.length>0){
			var filterbutton =	'<div class="form-group button-group">'+ 
				'<button name="filter_submit" class="btn btn-sm btn-primary">'+
				'<i class="ace-icon fa fa-filter align-top bigger-125"></i>'+
				$i.i18n('widgets:filter_button_submit')+
			'</button>'+
			'</div>';
		
			this.$.filter.append(filterbutton);
			this.appendFilterEventButton(); //Applies submit filter event to button.
		}
	}
	
	/**
	 * Returns the filter value given a filter field.
	 * If not found, returns null.
	 * @param field
	 * @return value | null
	 */
	function fillFilter(field){
		field = field.toLowerCase();
		var val = null;
		if(this.filters!=null){
			for (var i=0;i<this.filters.length;i++){
				if(field==this.filters[i].field){
					val = this.filters[i].value;
					break;
				}
			}
		}
		return val;
	}
	
	/**
	 * Set the click event to the filter submit button.
	 */
	function appendFilterEventButton(){
		var $button = this.$.filter.find('button[name="filter_submit"]');
		$button.click(function(event){
			_this.currentPage = 1; //When apply filters, set current page to 1
			event.preventDefault();
			event.stopPropagation();
			_this.filters = getFiltersData();
			_this.request();
		});
	}
	
	/**
	 * For all available and rendered filters, get the filter values.
	 * @return array
	 */
	function getFiltersData(){
		var filters = [];
		$.each(_this.filterInfo,function(){
			//console.log(this);
			var fname = this.field.toLowerCase();
			var $obj = _this.$.filter.find('[name="'+fname+'_filter"]');
			var fval = $obj.val();
			if(fval!=''){
				filters
				.push({	
					field: fname,
					value: fval,
					type : this.type
				});
			}
		});
		return filters;
	}
	
	/**
	 * Set the click event to the DROP_BUTTON action column button.
	 */
	function dropButton(){
		var $button = this.$.table.find('thead tr div[data-id="action_column"]');
		var liAction = $button.find('ul > li');
		//var liAction = $('#'+this.id+'-action-colum > ul > li');
		$.each(liAction,function(){
			if(!$(this).hasClass("divider")){
				$(this).on('click',function(event){
					event.preventDefault();
					event.stopPropagation();
					action = $(this).data('action');
					if(action=='ALL' || action=='NONE'){
						if(action=='ALL'){
							$('input:checkbox[name="action_checkbox_tr"]').each(function(){
								this.checked = true;
							});
						}
						else{
							$('input:checkbox[name="action_checkbox_tr"]').each(function(){
								this.checked = false;
							});
						}
					}
					else if((typeof(_this.actionCol.onclick)==='function')){
						var checks = $('input:checkbox[name="action_checkbox_tr"]:checked');
						checks = ((checks.length==0)? null:checks);
						var selected = [];
						if(checks!=null){
							var $tr = _this.$.table.find('tbody > tr');
							$tr.each(function(){
								if($(this).find('input:checkbox[name="action_checkbox_tr"]').is(':checked')){
									selected.push($(this).data('id'));
								}
							});
						}
						_this.actionCol.onclick(_this.id,action,selected);
					}
					else{
						if($i._get('debug')){
							console.log("Option onclick event does not has attached any function!");
						}
					}
					$button.removeClass('open');
				});
			}
		});
	}
	
	function tipsClickEvent(){
		if(_this.enableTips.action!=null && typeof(_this.enableTips.action)=="function"){
			var $tips = _this.$.table.find('tbody tr button[data-istooltip="true"]');
			$tips.click(function(event){
				event.preventDefault();
				event.stopPropagation();
				_this.enableTips.action($(this).parent().parent().parent(), $(this).data("button-action"));
			});
			var $tips = _this.$.table.find('tbody tr td[data-td-column="ACTIONS"] a[data-istooltip="true"]').click(function(event){
				event.preventDefault();
				event.stopPropagation();
				_this.enableTips.action($(this).parent().parent().parent().parent().parent().parent(),$(this).data("button-action"));
			});
		}
		else{
			console.log("Warning: This action has not method!, please add 'buttonsConfig' function on the list config!");
		}
	}

	function getColTitleByColName(cols,cn){
		for(var i=0;i<cols.length;i++){
			if(cols[i].name==cn){
				return $i.i18n(config.i18n+':col_'+cn.toLowerCase());
			}
		}
		return cn;
	}
	
	function HTMLcard(){
		var HTML = ''+
		'<div data-id="'+this.id+'_superview_main" class="row" data-plugin-name="'+_PLUGIN_NAME+'" data-plugin-id="'+_PLUGIN_ID+'">'+
			'<div data-id="'+this.id+'_superview_header" class="table-header">'+
				'&nbsp;&nbsp;&nbsp;'+
			'</div>'+
			'<div data-id="'+this.id+'_superview_content" class="table-responsive dataTables_wrapper_custom">'+
				'<div data-id="'+this.id+'_superview_wrapper" class="dataTable_wrapper" role="grid">'+
					'<div data-id="'+this.id+'_superview_options" class="row hide">'+
						'<div class="row">'+
							'<form class="form-inline" role="form" data-id="filter-form">'+
								'<div data-id="'+this.id+'_superview_filter" class="col-sm-9 col-xs-12"></div>'+
								'<div data-id="'+this.id+'_superview_search" class="col-sm-3 col-xs-12"></div>'+
							'</form>'+
						'</div>'+
					'</div>'+
					'<!-- START SUPERVIEW TABLE TABLE -->'+
						'<table data-id="'+this.id+'_superview_table" class="table table-striped table-bordered table-hover dataTable">'+
							'<thead>'+
								'<tr role="row"></tr>'+
							'</thead>'+
							'<tbody role="alert" aria-live="polite" aria-relevant="all">'+
								'<tr>'+
									'<td>'+
										'<span data-i18n="widgets:emptyGrid"></span>'+
									'</td>'+
								'</tr>'+
							'</tbody>'+
						'</table>'+
						'<!-- END SUPERVIEW TABLE TABLE -->'+
						'<!-- START SUPERVIEW CARDS DIV -->'+
						'<div data-id="'+this.id+'_superview_masonry" class="grid" style="overflow:hidden;"></div>'+
						'<!-- END SUPERVIEW CARDS DIV -->'+
					'<div class="row">'+
						'<div class="col-sm-6 col-xs-3">'+
							'<div data-id="'+this.id+'_superview_length" class="dataTables_length"></div>'+
						'</div>'+
					'</div>'+
					'<div class="row" data-id="'+this.id+'_superview_paging"></div>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>';
		this.appendTo.html(HTML);
	}
	
	/**
	 * INIT START VIEW AND CONFIG VIEWS.
	 * 
	 * A template object has the following form:
	 * {
			id : the template identifier
			name : the template name,
			values : the visible columns and their order (order is given by col array position),
			filters : the visible filters and their default filter value,
			type : the type of the view (at 20160303 the two types are TABLES or CARDS),
			defaultView : if this is the default view,
			rowsPerPage : how many records per page will be returned by the API,
			defaultOrder : the default order columns,
			orderDirection : the default order direction (+ or -),
			main : if this is the main view (internal checking).
		};
		
		The template is a way to render the data for the users. It's highly customizable by the users and
		they could change not only the columns rendered and their position in the grid, but also the default values
		of the filters and also the default ordering of the data, as the rows per page by default and other minor functionalities.
		This kind of setup is available for ALL views on the APP and to enable it you can simply do it by calling the method "renderConfig()"
	 */
	
	/**
	 * For a default filters on a view template, set it on the filter fields on the view.
	 * In case of we want to set the filters for the defaultview, and that view does not have default filters, set default values for all filters.
	 * @param array filters.
	 */
	function setFilters(filters){
		this.filters = null; //Clear al previous filter settings.
		var fil = [];
		$.each(this.filterInfo,function(){
			var finfo = this;
			$.each(filters,function(){
				if(finfo.field.toLowerCase()==this.name && this.value.toString()!=""){
					var value = this.value;
					if(finfo.type=='date'){
						var $times = $i.times(moment());
						$times.getMoment().subtract((-1)*this.value,'days').startOf('day');
						var start = $times.remote();
						$times = $i.times(moment());
						$times.getMoment().subtract((-1)*this.value,'days').endOf('day');
						var end = $times.remote();
						value = start+'-'+end;
					}
					if(finfo.type=='daterange'){
						$.each(getDefaultDateRanges(),function(){
							if(this[2]==value){
								var format = $i.times().getLocalFormat().dateFormat;
								var start = $i.times(this[0]).remote();
								var end = $i.times(this[1]).remote();
								value = start+'-'+end;
								return;
							}
						});
					}
					fil.push({	
						field: this.name,
						value: value,
						type : finfo.type
					});
				}
			});
		});
		if(fil.length>0){
			this.filters = fil;
		}
		return true;
	};
	
	/**
	 * Get template by given a template id
	 * @param integer template_id
	 * @return object template
	 */
	this.getTemplate = function(tid){
		var lt = null;
		$.each(this.viewTemplates,function(){
			if(this.id==tid){
				lt = this;
				return;
			}
		});
		
		if(lt == null){
			lt = this.viewTemplates[0]; //Load default template.
		}
		return lt;
	}
	
	/**
	 * Save template to the templates array and to the WEBSERVICE.
	 * @param boolean isNew
	 * @param array cols
	 * @param array filters
	 * @param integer nid
	 * @param string name
	 * @param boolean is_default_view
	 * @param integer rowsPerPage
	 * @param string defaultOrder
	 * @param char(+,-) orderDirection
	 */
	this.saveTemplate = function(isNew, cols,filters,nid,type,name,defaultView,rowsPerPage,defaultOrder,orderDirection){
		var t = {};
		t = {
			id : nid,
			name : name,
			values : cols,
			filters : filters,
			type : type,
			defaultView : defaultView,
			rowsPerPage : rowsPerPage,
			defaultOrder : defaultOrder,
			orderDirection : orderDirection,
			main : false
		};
		
		if(isNew){
			this.viewTemplates.push(t);
		}
		else{
			for(var i=0,j=this.viewTemplates.length;i<j;i++){
				if(this.viewTemplates[i].id == nid){
					this.viewTemplates[i] = t;
				}
				else{
					if(t.defaultView){
						this.viewTemplates[i].defaultView = false;
					}
				}
			}
		}
		
		var obj = {
			main : this.getDefaultView(),
			templates : this.viewTemplates.slice(1)
		};
		
		$i.setPageUISetup('superview',obj); //Saves superview config on permanent data storage (cookies...db...i don't care). Do not save default view
		this.updateSelectOnButton(nid);
		return true;
	};
	
	
	/**
	 * Get the ID of the default view.
	 * @return integer template_id
	 */
	this.getDefaultView = function(){
		var dv = 0; //By default, default view is located at 0
		$.each(this.viewTemplates,function(){
			if(this.defaultView){
				dv = this.id;
				return;
			}
		});
		return dv;
	}
	
	/**
	 * Removes a template on the array and also from the API. Also sets default view to the list when it happens.
	 * @returns true on success.
	 */
	this.removeTemplate = function(tid){
		var key;
		for(var i=0,j=this.viewTemplates.length;i<j;i++){
			if(this.viewTemplates[i].id == tid){
				key = i;
				break;
			}
		}
		this.viewTemplates.splice(key,1);
		
		
		var st =  this.viewTemplates.slice(1);
		var obj = {};
		
		if(st.length>0){
			obj = {
				main : this.getDefaultView(),
				templates : st 
			};
		}
	
		$i.setPageUISetup('superview',obj); //Saves superview config on permanent data storage (cookies...db...i don't care). Do not save default view
		this.updateSelectOnButton(0);
		this.loadTemplate(0); //Load default template.
		return true;
		
	}
	
	/**
	 * Load the specified template id and render it on the UI, also make a API request to get reload data.
	 */
	this.loadTemplate = function(tid){
		var lt = this.getTemplate(tid);
		this.sortInfo = lt.orderDirection+lt.defaultOrder; //Set order and direction.
		this.rowsPage = lt.rowsPerPage;
		this.setColumns(lt.values); //Set columns
		this.setFilters(lt.filters); //Set filters
		this.currentPage = 1; //On load the template, set currentPage to 1.
		this.reload(lt.type); //Reload View.
		this.currentTemplate = lt.id; //Set currentTemplate id.
	}
	
	/**
	 * Update this select (button-zone select view) every time we notice a change on the view templates.
	 */
	this.updateSelectOnButton = function(cid){
		var page = $i.pages.find.visible();
		var $buttonselectTemplate = page.buttons.buttons.find('div[data-content="others"] select[name="view_config_templates"]');
		if($buttonselectTemplate.length>0){
			$buttonselectTemplate.html('');
			$.each(this.viewTemplates,function(){
				$buttonselectTemplate.append('<option value="'+this.id+'">'+this.name+'</option>');
			});
			$buttonselectTemplate.val(cid);
		}
	};
	
	/**
	 * Get, from one column all the available values from it.
	 */
	function getColumnValues(tid,column){
		var t =_this.getTemplate(tid);
		return t.values[column];
	}
	
	this.updateSelectOnButton(this.currentTemplate); //Update button-zone select for list template.
	
	this.isNewTemplate = false;
	this.templateID; //template ID for new or editing template.
	
	/**
	 * Render view template config.
	 * This method calls a $i.modal widget and also to $i.steps to render wizard form inside a modal view. 
	 */
	this.renderConfig = function(){
		var $mod = null;
		var jqd = $i.modal({
			show: true,
			title : $i.i18n('widgets:config_title'),
			width: '30%',
			description : renderOptions,
			icon : 'fa fa-eye',
			defaultButtons : false,
			buttons : [
	           {
	        	   text :$i.i18n('widgets:bt_prev_step'),
	        	   'class' :"btn btn-sm btn-prev", 
	        	   click : function(){}
		        },
		        {
		        	text : $i.i18n('widgets:bt_next_step'),
		        	'class' :"btn btn-success btn-sm btn-next", 
		        	click : function(){}
		        }
			],
			execute : function(id,$modal,$buttons,$dialog,$widget){ //id -> modal id, modal jquery object.
				$buttons.find('button.btn-next').attr('data-last',$i.i18n('widgets:save_view')); //Add finish event to next button
				//$modal.find('input.date-picker').dpicker(); //Add datepicker to any input on modal who has datepicker class.
				//$modal.find('input.daterange').daterange(); //Add daterange to any input on modal who has daterange class.
				$modal.find('select.select2').select2(); //Add chosen to any select with class select2.
				var $typeB = $modal.find('div[data-step="2"] button'); //Set events for view type located at step2.
				var $type = $modal.find('input[name="type"]'); 
				$typeB.each(function(){
					$(this).on('click',function(){
						$typeB.each(function(){
							$(this).children().css('color','black');
						});
						$(this).children().css('color','#bb0b24');
						$type.val($(this).data('action'));
						return false;
					});
				});

				$modal.form(); //Adds form functionallity to this modal.
				
				//Adds wizard ($i.steps) on the modal.
				$dialog.steps({
					buttons : $buttons,
					onFinish : finish_steps,
					steps : [
					     { step: 1,direction : 'next', action: action_step_1_to_2},
				         { step: 2,direction : 'next', action: action_step_2_to_3}
			         ]
				});
				
				$removeButtons = $dialog.find('div.step-pane[data-step="1"]').find('i[data-action="REMOVE_TEMPLATE"]');
				$removeButtons.on('click',function(){
					var tid = $(this).data('remove');
					var $li = $(this).parent().parent();
					_this.removeTemplate(tid);
					$li.hide(function(){
						$li.remove();
					});
				});
				
				var currentEditingTemplate = null;
				function action_step_1_to_2(e){
					var tid = $modal.find('div.step-pane[data-step="1"]').find('input[name=template_id]:checked').val();
					if(tid=='new'){
						_this.templateID = (_this.viewTemplates.length+1); //Set to  new template.
						_this.isNewTemplate = true;
						currentEditingTemplate = _this.getTemplate(0); //If this is a new template, we take the default values from the default template.
						$typeB[0].click(); //Click on TABLE button.
					}
					else{
						_this.isNewTemplate = false;
						_this.templateID = tid;
						currentEditingTemplate = _this.getTemplate(tid);
					}
					//Set form values on step 2.
					var $step2 = $modal.find('div.step-pane[data-step="2"]');
					
					if(_this.isNewTemplate){
						$step2.find('input[name="template_name"]').val("");
					}
					else{
						$step2.find('input[name="template_name"]').val(currentEditingTemplate.name);
					}
					
					$type.val(currentEditingTemplate.type);
					$typeB[((currentEditingTemplate.type==_TABLE)? 0:1)].click(); //Click on TABLE button.
					
					if(currentEditingTemplate.defaultView){ //Mark checked defaultView if it need.
						$step2.find('input[name="is_default_view"]').prop('checked',true);
					}
					else{
						$step2.find('input[name="is_default_view"]').prop('checked',false);
					}
					
					$step2.find('select[name="rowsPerPage"]').val(currentEditingTemplate.rowsPerPage); //Set rowsPerPage config.
					$step2.find('select[name="default_order"]').val(currentEditingTemplate.defaultOrder); //Set order config.
					
					if(currentEditingTemplate.orderDirection=="+"){ //Mark order direction.
						$step2.find('input[name="order_direction"]').prop('checked',true);
					}
					else{
						$step2.find('input[name="order_direction"]').prop('checked',false);
					}
				}
				
				function action_step_2_to_3(e){
					//Check if form data is valid.
					if(!$modal.validate()){
						 e.preventDefault();
						 return;
					}
					else{
						$modal.applyChanges();
					}

					//Set events for view type.
					var $columnChosenCont = $modal.find('div[data-id="column_chooser_container"]');
					$columnChosenCont.html(''); //Clear step 3 which it is column container
					var $type = $modal.find('input[name="type"]');
					var type = $type.val();
					for(var i=0;i<_this.queryFields.available.length;i++){
						var render = "";
						render += '<!-- B:LABEL_'+i+' -->\
						<div class="form-group">\
							<label class="control-label col-xs-12 col-sm-3 no-padding-right blue" for="label_'+i+'">'+$i.i18n('widgets:'+(type==_TABLE? 'column_name':'file_name'),{key:(i+1)})+'</label>\
							<div class="col-xs-12 col-sm-9">\
								<select '+((type==_CARDS)? 'multiple ':'')+'class="col-xs-12 col-sm-12 tag-input-style chosen" name="show_'+i+'" data-id="show_'+i+'">\
								<option value=""></option>';
								for(var k=0,l=_this.queryFields.available.length;k<l;k++){
									var av = _this.queryFields.available[k];
									//'+(type==_TABLE && k==i? 'selected':'')+' <-- To default select.
									render +='<option value="'+av.toLowerCase()+'">'+$i.i18n(config.i18n+':col_'+av.toLowerCase())+'</option>';
								}
								render += '</select>\
							</div>\
						</div>\
						<!-- E:LABEL_'+i+' -->';
						$columnChosenCont.append(render);
					}
					
					
					if(!_this.isNewTemplate){
						//If this is not a new template, fill each column select with specified values.
						for(var i=0,j=_this.queryFields.available.length;i<j;i++){
							var values = getColumnValues(_this.templateID,i);
							//console.log(values);
							if(typeof(values)!=='undefined'){
								if(type==_TABLE){
									var opt = $columnChosenCont.find('select[name="show_'+i+'"] option[value="'+values.name+'"]');
									opt.attr('selected',true);
								}
								else{
									var opt = $columnChosenCont.find('select[name="show_'+i+'"] option[value="'+values.name+'"]');
									opt.attr('selected',true);
									if(values.alternativeCols!=null){
										$.each(values.alternativeCols,function(){
											var opt = $columnChosenCont.find('select[name="show_'+i+'"] option[value="'+this.name+'"]');
											opt.attr('selected',true);
										});
									}
								}
							}
						}
					}
					
					
					var $step4 = $modal.find('div.step-pane[data-step="4"]').find(':input');
					$step4.each(function(){
						var $filter = $(this);
						if(!_this.isNewTemplate){ //Set current editing template values to a filters. 
							$.each(currentEditingTemplate.filters,function(){
								if((this.name+'_filter')==$filter.attr('name')){
									$filter.val(this.value);
									if($filter.hasClass('select2')){
										$filter.trigger('chosen:updated');
									}
								}
							});
						}
						else{ //If new, clear form data.
							$filter.val("");
						}
					});
					
					if(type==_CARDS){
						$columnChosenCont.find('select.chosen').attr('data-placeholder',$i.i18n('widgets:plh_choose_options'));
						$columnChosenCont.find('select.chosen').select2({width : '100%',allow_single_deselect: true})
					}
				}
				
				function finish_steps(){
					var nid,name,defaultView,rowsPerPage,defaultOrder,orderDirection,type;
					var cols = [];
					var filters = [];
					var values = $modal.getValues(false);
					$.each(values,function(prop,value){
						switch(prop){
							case 'template_id':
								nid = value;
								break;
							case 'template_name':
								name = value;
								break;
							case 'is_default_view':
								defaultView = (value=="Y"? true:false);
								break;
							case 'rowsPerPage':
								rowsPerPage = value;
								break;
							case 'default_order':
								defaultOrder = value;
								break;
							case 'order_direction':
								orderDirection = (value=="Y"? '+':'-');
								break;
							case 'type':
								type = value;
								break;
							default:
								if(prop.startsWith('show_')){
									var col = constructColumns(value);
									if(col!=null){
										cols.push(col);
									}
								}
								if(prop.endsWith('_filter')){
									filters.push({
										name : prop.replace('_filter',''),
										value : value
									});
								}
						}
					});

					_this.saveTemplate( //Save template to template array
						_this.isNewTemplate,
						cols,
						filters,
						_this.templateID,
						type,
						name,
						defaultView,
						rowsPerPage,
						defaultOrder,
						orderDirection
					); 
					_this.loadTemplate(_this.templateID); //Load current template
					$widget.close(); //Close modal (after do checks).
				}
			},
			success : function(){}
		});
	}
	
	/**
	 * Returns well formatted columns object, used when you want to render 
	 * a view or save it. 
	 * Return null if colNames == null or empty.
	 */
	function constructColumns(colNames, colType){
		if(colNames==null) return null;
		var col = {};
		if($.isArray(colNames)){
			for(var k=0,l=colNames.length;k<l;k++){
				var colName = colNames[k];
				if(k==0){
					col.name = colName;
					col.dataType = 'text';
					col.alternativeCols = [];
				}
				else{
					col.alternativeCols.push({
						name : colName,
						dataType : 'text'
					});
				}
			}
		}
		else{
			if(colNames!=""){
				col.name = colNames;
				col.dataType = (typeof(colType)!=='undefined'? colType:'text');
				col.alternativeCols = null;
			}
			else{
				return null;
			}
		}
		return col;
	}
	
	/**
	 * Render modal view for view and edit templates.
	 */
	function renderOptions(){
		var render = '' +
		'<div data-id="view_config_steps"><!-- wrap step list and step panes -->\
	    	<!-- step list -->\
	    	<ul class="steps">\
				<li data-step="1" class="active">\
			  		<span class="step">1</span>\
			  		<span class="title">'+$i.i18n('widgets:title_view_list')+'</span>\
			  	</li>\
				<li data-step="2" class="active">\
	          		<span class="step">2</span>\
	          		<span class="title">'+$i.i18n('widgets:title_view_setup')+'</span>\
	          	</li>\
	          	<li data-step="3" class="active">\
			  		<span class="step">3</span>\
			  		<span class="title">'+$i.i18n('widgets:title_column_setup')+'</span>\
			  	</li>\
		       <li data-step="4">\
		          <span class="step">4</span>\
		          <span class="title">'+$i.i18n('widgets:title_filter_config')+'</span>\
		       </li>\
		   </ul>\
		   <!-- step panes -->\
		   <div class="step-content">\
		       <!-- STEP1 : NEW OR EDIT TEMPLATE -->\
			   <div class="step-pane active" data-step="1">\
		          	<ul style="list-style:none;">\
						<li>\
							<div class="radio">\
							<label>\
								<input type="radio" class="ace" name="template_id"  value="new" checked>\
								<span class="lbl"> '+$i.i18n('widgets:lbl_new_viewTemplate')+'</span>\
							</label>\
						</div></li>';
					$.each(_this.viewTemplates,function(){
						if(!this.main){
							render += '<li>\
								<div class="radio">\
								<label>\
									<input type="radio" class="ace" name="template_id"  value="'+this.id+'">\
									<span class="lbl"> '+this.name+'</span>\
								</label>\
								<i class="ace-icon fa fa-trash-o bigger-120 red" data-action="REMOVE_TEMPLATE" data-remove="'+this.id+'" style="cursor:pointer;"></i>\
							</div>\
							</li>';
						}
					});
					render += '</ul>\
			   </div>\
		       <!-- END STEP 1 -->\
			   <!-- STEP2 : TEMPLATE NAME AND TYPE OF VIEW SELECTION -->\
			   <div class="step-pane" data-step="2">\
					<form class="form-horizontal" role="form" name="form_options">\
						<div class="row">\
							<div class="col-xs-12">\
								<div class="form-group">\
					 				<label class="control-label col-xs-12 col-sm-4 no-padding-right blue">'+$i.i18n('widgets:name')+'</label>\
					 				<div class="col-xs-12 col-sm-8">\
					 				<div data-content="ntemplate">\
					 					<input type="text" value="" name="template_name" class="col-xs-12 col-sm-12" required autocomplete="off"/>\
					 				</div>\
					 			</div>\
					 		</div>\
					 		<div class="form-group">\
					 			<label class="control-label col-xs-12 col-sm-4 no-padding-right blue">'+$i.i18n('widgets:view_type')+'</label>\
					 			<div class="col-xs-12 col-sm-8">\
					 				<button class="btn btn-white" data-action="TABLE" data-placement="bottom" title="Table">\
					 					<i class="ace-icon fa fa-list align-top bigger-160 icon-only"></i>\
					 				</button>\
					 				<button class="btn btn-white" data-action="CARDS" data-placement="bottom" title="Cards">\
					  					<i class="ace-icon fa fa-table align-top bigger-160 icon-only"></i>\
					 				</button>\
					 				<input type="hidden" value="" name="type" />\
					 			</div>\
					 		</div>\
					 		<div class="form-group">\
					 			<label class="control-label col-xs-12 col-sm-4 no-padding-right blue">'+$i.i18n('widgets:is_default_view')+'</label>\
					 			<div class="col-xs-12 col-sm-8">\
					 				<div class="checkbox">\
							  			<label>\
							  				<input name="is_default_view" type="checkbox" class="ace">\
							  				<span class="lbl"></span>\
							  			</label>\
					 				</div>\
					 			</div>\
					 		</div>\
			  				<div class="form-group">\
					 			<label class="control-label col-xs-12 col-sm-4 no-padding-right blue">'+$i.i18n('widgets:lbl_rec_per_page')+'</label>\
					 			<div class="col-xs-12 col-sm-8">\
				 					<select size="1" name="rowsPerPage">';
									for(var i=0;i<_this.availableRowsPage.length;i++){
										render +='<option value="'+_this.availableRowsPage[i]+'">'+
											_this.availableRowsPage[i]+
										'</option>';
									}
									render +='</select>\
					 			</div>\
					 		</div>\
							<div class="form-group">\
					 			<label class="control-label col-xs-12 col-sm-4 no-padding-right blue">'+$i.i18n('widgets:lbl_default_order')+'</label>\
					 			<div class="col-xs-12 col-sm-8">\
				 					<select size="1" name="default_order">';
									$.each(_this.querySortables.available,function(){
										render +='<option value="'+this+'">'+$i.i18n(config.i18n+':col_'+this)+'</option>';
									})
									render +='</select>\
									<label>\
										<input type="checkbox" name="order_direction" class="ace ace-switch ace-switch-4 btn-rotate">\
										<span class="lbl" data-lbl="Asc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desc"></span>\
									</label>\
					 			</div>\
					 		</div>\
					 	</div>\
					</div>\
				</form>\
			   </div>\
			   <!-- END STEP2 -->\
			   <!-- STEP3 : COLUMN CHOOSER SELECTION -->\
		     <div class="step-pane" data-step="3">\
		        <form class="form-horizontal" role="form" name="form_options">\
	  				<div class="row">\
	  					<div class="col-xs-12" data-id="column_chooser_container">\
						</div>\
					</div>\
				</form>\
			</div>\
			<!-- END STEP3 -->\
			<!-- STEP4 : DEFAULT FILTERS SELECTION -->\
	      <div class="step-pane" data-step="4">\
			  <form class="form-horizontal" role="form" name="form_options">\
	  			<div class="row">\
          			<div class="col-xs-12" data-id="filter_chooser_container">';
			  			$.each(_this.filterInfo,function(k,filter){
			  				var output = '';
			  				render += '<div class="form-group">';
			  				switch(filter.type){
			  					case 'date+':
			  					case 'date-':
			  					case 'date':
			  						render +=''+
			  						'<label for="filter_date" class="control-label col-xs-12 col-sm-3 no-padding-right">'+filter.title+'</label>\
			  						<div class="col-xs-12 col-sm-7">\
			  						<select name="'+filter.field.toLowerCase()+'_filter" data-type="'+filter.type+'" class="form-control">\
			  							<option value="">'+$i.i18n('widgets:filter_tooltip_select_option_any')+'</option>';
			  							for(var i=-5,j=5;i<j;i++){
			  								if(i<-1){
			  									render +='<option value="'+i+'">'+$i.i18n('widgets:value_date_minus',{value: (-1)*i})+'</option>';
			  								}
			  								if(i==-1){
			  									render +='<option value="'+i+'">'+$i.i18n('widgets:value_date_yesterday')+'</option>';
			  								}
			  								if(i==0){
			  									render +='<option value="'+i+'">'+$i.i18n('widgets:value_date_today')+'</option>';
			  								}
			  								if(i==1){
			  									render +='<option value="'+i+'">'+$i.i18n('widgets:value_date_tomorrow')+'</option>';
			  								}
			  								if(i>1){
			  									render +='<option value="'+i+'">'+$i.i18n('widgets:value_date_add',{value: i})+'</option>';
			  								}
			  							}
			  						render += '\
			  						</select>\
			  						</div></div>';
			  						break;
			  					case 'daterange':
			  						render +=''+
			  						'<label for="filter_date" class="control-label col-xs-12 col-sm-3 no-padding-right">'+filter.title+'</label>\
			  						<div class="col-xs-12 col-sm-7">\
			  						<select name="'+filter.field.toLowerCase()+'_filter" data-type="'+filter.type+'" class="form-control">\
			  							<option value="">'+$i.i18n('widgets:filter_tooltip_select_option_any')+'</option>';
			  							$.each(getDefaultDateRanges(),function(name,range){
			  								render +='<option value="'+range[2]+'">'+name+'</option>';
			  							});
			  							render +='\
		  							</select>\
			  						</div></div>';
			  						break;			  						
			  					case 'text':
			  						render +=''+
			  						'<label for="filter_text">'+filter.title+'</label>'+
			  						'<input type="text" name="'+filter.field.toLowerCase()+'_filter" data-type="'+filter.type+'" class="form-control ui-autocomplete-input" autocomplete="off">'+
			  						'</div>';
			  						break;
			  					case 'select':
			  					case 'select2':
			  						render +=''+
			  						'<label for="filter_select" class="control-label col-xs-12 col-sm-3 no-padding-right">'+filter.title+'</label>'+
			  						'<div class="col-xs-12 col-sm-7">'+
			  						'<select name="'+filter.field.toLowerCase()+'_filter" data-type="'+filter.type+'" class="form-control '+filter.type+'">';
		  							var $opt = _this.$.filter.find('select[name="'+filter.field.toLowerCase()+'_filter"] option');
		  							$opt.each(function(){
		  								render +='<option value="'+$(this).val()+'">'+$(this).text()+'</option>';
		  							});
			  						render +='</select>'+
			  						'</div></div>';
			  						break;
			  				}
			  			});
			  		render +='</div></div>\
			  </form>\
	      </div>\
		  <!-- END STEP4 -->\
	   </div>\
	 </div>';
		return render;
	}
	
	/**
	 * END VIEW AND CONFIG VIEWS.
	 */
	
	/**
	 *  INIT START ADVANCED SEARCH.
	 */
	
	function casearch(config){
		this.maxRows;
		this.maxColumns;
		this.bootstrapCol;
		this.addButtons = addButtons;
		this.currentSearch = null;
		var _that = this;
		var superView = _this;
		
		var aSearchPageButton = null; //Advanced Search Page button jQuery object.

		/**
		 * Add advanced search button on page buttons.
		 */
		function addButtons(){
			if(superView.pageButtons!=null){
				var buttons  = superView.pageButtons.addExtraButton([{
					icon : 'fa fa-search',
					tooltip : $i.i18n('widgets:tlt_advanced_search'),
					click : this.render
				}]);
				
				aSearchPageButton = buttons[0];
				delete buttons;
			}
			else{
				console.log("Can not create advanced search because pageButtons object is null. This page does not contain a pagebuttons object");
			}
		}
		
		
		var onDone = function(fields,options){
			config.fields = fields; //Camps personalitzats pel programador a renderitzar en la cerca avançada
			config.options = options; //Opcions disponibles per a aquest objecte (atributs,usuaris,... i altres específics d'aquest objecte).
			/**
			 * Si l'usuari pertany a tots els departaments i l'objecte en qüestió té departaments, mostrem el filtre de cerca avançada per departament.
			 */
			if($i.user._get().user_department==null){
				if(superView.pageObject._get('object').hasDepartment){
					config.fields.push({
						field: 'OWNER_DEPARTMENT',
						type: 'chosen',
						title: $i.i18n('widgets:col_owner_department'),
						dataSource : $i._get('configuration').departments.results,choice: false,nullable:true});
				}
			}
			
			if(superView.pageObject._get('object').hasDynAttr){
				$.each(config.options.attributes,function(){
					config.fields.push({
						field : 'dynattr_'+this.attr_id,
						type : 'text',
						title : this.label,
						nullable: true
					});
				});
			}
			
			if(superView.pageObject._get('object').hasGranularP){
				
			}
			
			_that.maxRows = config.maxRows || 8;
			_that.maxColumns = Math.ceil(config.fields.length/_that.maxRows);
			_that.bootstrapCol = Math.ceil(12/_that.maxColumns);
		}
		
		if(typeof(config.data)==='function'){
			config.data(onDone);
		}
		else if(typeof(config.data)==='array'){
			onDone(config.data);
		}
		else{
			console.log("No data attached. Can not fill the advanced search, advanced Search disabled.");
			return;
		}

		this.$mod = null;
		this.data = null;

		var successF = function(){console.log("asearch this.done called")};
		if(typeof(config.done)==='function'){
			successF = config.done;
		}
		
		function renderASearch(cbk){
			cbk(doRenderASearch());
		}
		
		function renderChoice(field,cfield){
			var checked = false;
			if(cfield!==false && cfield.choice=="AND"){
				checked = true;
			}
			var c =  '<div class="col-xs-12 col-sm-1">'+
			'<label>'+
			'<input type="checkbox" name="choice_'+field+'" class="ace ace-switch ace-switch-4 btn-rotate" data-name="choice_'+field+'" data-id="choice_'+field+'" '+(checked? 'checked':'')+'>'+
			'<span class="lbl" data-lbl="AND&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OR"></span>'+
			'</label>'+
			'</div>';
			return c;
		}
		
		function renderisNull(field,cfield){
			var checked = false;
			if(cfield!==false && cfield.value==null){
				checked = true;
			}
			var c =  '<div class="col-xs-12 col-sm-2">'+
			'<label>'+
				'<input name="null_'+field+'" type="checkbox" class="ace" '+(checked? 'checked':'')+'>'+
				'<span class="lbl">'+$i.i18n("widgets:lbl_null")+'</span>'+
			'</label>'+
			'</div>';
			return c;
		}
		
		function doRenderASearch(){
			var rows = 0;
			var render = '' +
			'<div class="row">'+
			'<form class="form-horizontal" role="form" name="form_advanced_search">'+
				'<!-- ADVANCED SEARCH CONTENT BEGINS -->';
			$.each(config.fields,function(){
				if(rows == 0 || rows>=_that.maxRows){
					render +='<div class="col-xs-'+_that.bootstrapCol+'">';
				}
				var field = this.field.toLowerCase();
				var fieldUC = this.field.toUpperCase();
				var type = this.type.toUpperCase();
				var choice = this.choice || false;
				var nullable = this.nullable || false;
				var input_size = (choice || nullable)? '6':'6';
				var cfield = getValue(field);
				switch(type){
					case 'TEXT':
					case 'AUTOCOMPLETE':
						var element = ''+
						'<!-- B:'+fieldUC+' -->'+
						'<div class="form-group">'+
							'<label class="control-label col-xs-12 col-sm-2 no-padding-right" for="'+field+'" >'+this.title+'</label>'+
							'<div class="col-xs-12 col-sm-'+input_size+'">'+
								'<input type="text" name="'+field+'" data-id="'+field+'" class="col-xs-12 col-sm-12" autocomplete="off" '+((cfield!==false && cfield.value!=null)? 'value="'+cfield.value+'"':"")+'>'+
							'</div>';
						if(choice) element += renderChoice(field,cfield);
						if(nullable) element += renderisNull(field,cfield);
						element +='</div>'+
						'<!-- E:'+fieldUC+' -->';
						break;
					case 'CHOSEN':
					case 'SELECT2':
					case 'SELECT':
						var multiple = "";
						var style = ""
						if(type=='CHOSEN'){
							multiple = 'multiple';
							style = 'chosen';
						}
						else if(type=='SELECT2'){
							style = 'select2';
						}
						var selected = false;
						var element = ''+
						'<!-- B:'+fieldUC+' -->'+
						'<div class="form-group">'+
							'<label class="control-label col-xs-12 col-sm-2 no-padding-right" for="'+field+'">'+this.title+'</label>'+
							'<div class="col-xs-12 col-sm-'+input_size+'">'+
								'<select '+multiple+' class="col-xs-12 col-sm-12 /*tag-input-style*/ '+style+'" name="'+field+'" data-id="'+field+'">';
									element += '<option value=""></option>';
									$.each(this.dataSource,function(){
										var text = "";
										if(typeof(this.text)!=='undefined'){
											text = this.text;
										}
										else if(typeof(this.name)!=='undefined'){
											text = this.name;
										}
										if(cfield!==false && cfield.value!=null){
											var id = this.id
											$.each(cfield.value,function(){
												if(id == this){
													selected = true;
													return true;
												}
											});
										}
										element += '<option value="'+this.id+'" '+(selected? 'selected':'')+'>'+text+'</option>';
										selected = false;
									});
								element += '</select>'+
							'</div>';
							if(choice) element += renderChoice(field,cfield);
							if(nullable) element += renderisNull(field,cfield);
						element += '</div>'+
						'<!-- E:'+fieldUC+' -->';
						break;
				}
				render += element;
				rows++;
				if(rows == 0 || rows>=_that.maxRows){
					render +='</div>';
					rows = 0;
				}
			});
			render += '</div><!-- ADVANCED SEARCH CONTENT ENDS --></form></div>';
			return render;
		}
		
		function getValue(field){
			if(_that.currentSearch==null) return false;
			for(var i=0,j=_that.currentSearch.length;i<j;i++){
				if(_that.currentSearch[i].field == field){
					return _that.currentSearch[i];
				}
			}
			return false;
		}
		
		this.render = function(){
			createModal(doRenderASearch());
		};
		
		function cleanSearchFormF(){
			_that.$mod.restoreChanges(); //It's super easy to clear form if we add trackChanges on render form, as the default values will be always empty.
			aSearchPageButton.removeClass('btn-warning btn-bold');
		}
		
		function createModal(render){
			_that.$mod = null;
			$i.modal({
				show: true,
				title : $i.i18n('advanced_search'),
				description : render,
				buttons : [{ text : $i.i18n('widgets:clean_button'), 'class' : 'btn btn-sm btn-success',click : cleanSearchFormF}],
				width: '90%',
				height : '600',
				icon : 'fa fa-search',
				resizable : false,
				execute : function(id,$modal){ //id -> modal id, modal jquery object.
					_that.$mod = $modal;
					$modal.trackChanges(true); //It's super easy to clear form if we add trackChanges on render form, as the default values will be always empty
					$modal.find('.select2').select2();
					$modal.find('.chosen').attr('data-placeholder',$i.i18n('widgets:plh_choose_options'));
					$modal.find('.chosen').select({width: '100%'});
					 $.each(config.fields,function(k,currentField){
						 if(currentField.type.toUpperCase()=='AUTOCOMPLETE' && typeof(currentField.dataSource)!=='undefined'){
							 $modal.find('input[data-id="'+currentField.field.toLowerCase()+'"]').autocomplete({
								 minLength: 3,
								 source: function( request, response ) {
						    			 var $r = $modal.find('input[data-id="'+currentField.field.toLowerCase()+'"]');
								    	  if(!$r.hasClass('inputIcon')){
								    		  $r.addClass('inputIcon');
								    	  }
								    	  $i.promise._request(currentField.dataSource.method,currentField.dataSource.url+$r.val().toUpperCase())
								    	  .done(function(data){
								    		  var regions = [];
								    		  for(var i=0,j=data.data.length;i<j;i++){
								    			  regions.push(data.data[i].name);
								    		  }
								    		  response(regions); 
								    	  });
						    		 } 
							 });
					      }
					  });
					
				},
				success : function(){
					var form = _that.$mod.find('form[name="form_advanced_search"]');
					var asearch = [];
					var dynAttrIDs = ""; //Comma separated ID's
					var dynAttrValues = ""; //Comma separated Values.
					$.each(config.fields,function(){
						var eobj = {
							field : null,
							choice : 'OR',
							nullable : false,
							type : null,
							value : null
						};
						var type = this.type.toLowerCase();
						var field = this.field.toLowerCase();
						eobj.field = field;
						eobj.type = type;
						var $obj = null;
						if(type=='text' || type=='autocomplete'){
							$obj = form.find('input[name="'+field+'"]')
						}
						if(type=='chosen' || type=='select2' || type=='select'){
							$obj = form.find('select[name="'+field+'"]')
						}
						if(this.choice){
							var c = form.find('input[name="choice_'+field+'"]');
							if(c.is(':checked')){
								eobj.choice = 'AND';
							}
						}
						if(this.nullable){
							var c = form.find('input[name="null_'+field+'"]')
							if(c.is(':checked')){
								eobj.nullable = true;
							}
						}
						eobj.value = $obj.val();
						if(eobj.value=="") eobj.value = null;
						if(eobj.value!=null || (eobj.value==null && eobj.nullable)){
							asearch.push(eobj);
						}
					});
					if(asearch.length!=0){
						aSearchPageButton.addClass('btn-warning btn-bold');
					}
					else{
						aSearchPageButton.removeClass('btn-warning btn-bold');
					}
					
					_that.currentSearch = asearch;
					superView.advancedSearch(asearch); //superView is superview.
					return;
				},
			});
		};
	};
	/**
	 *  END ADVANCED SEARCH.
	 */
	
	var readOnlyF = this.readOnly = function(ro){
		console.log("POSEM A READ ONLY el plugin "+this.$.main.data('plugin-name')+" amb ID: "+this.$.main.data('plugin-id'));
		if(this.listType==_TABLE){
			this.readOnly = true;
			this.$.table.find('thead > tr').each(function(){
				//console.log(this);
			});
			
			this.$.table.find('thead th[data-column=":TIPS"]').remove();
		}
		readOnly = ro;
	}
	
	
	if(this.pageObject!==false){
		this.pageObject.plugins(_PLUGIN_NAME,this);
	}
	else{
		console.log("Page object not found");
	}
}

$i.view = function(cfg){
	return new idcp_superView(cfg,cfg.$primaryObject);
};

$.fn.extend({
	superview : function(config){
		return new idcp_superView(config,$(this));
	}
});