(function(page){
	page.config({
		autoshow : false, //Si volem gestionar manualment quan ensenyem la plana.
		object : 'groups', //Objecte de referencia el qual es mostrará algun dels seus fills en aquesta plana.
		type : 'list', //Tipus de pàgina.
	});
	
	var groupsL = $i('#groupsList').superview({
		rest: 'groups',
		allowTemplates : false,
		exportable : false,
		rowId: 'ID',
		sortable: true,
		defaultsort: '-NAME',
		rowClick: {enable:true,onClick: doRowClick},
		dataType: ["ID","NAME"],
		i18n : 'security',
		actionCol : {
       		colTitle: $i.i18n("security:col_drop_button"),
       		actions: [
               {action:'DELETE',title: $i.i18n("security:action_delete"),icon: 'fa fa-trash-o orange'},
            ],
            help: true, //Adds buttons to "check" and "uncheck" all checkboxes
            onclick: doAction
       	}, 
		cols: [{name: 'ID'},{name: 'NAME'},{name: 'USERS'},{name : 'CREATED'}]
	});
	
	function doRowClick($jqE){
		var gid = $jqE.data('id');
		var gname = $jqE.data('name');
		$i.promise._request({
			restURL : 'groups/'+gid,
			bsend : $jqE,
		})
		.done(function(response){
			$i.hash.follow('security/groups/' +  gid, gname,{'name':gname,'response' : response});
		});
	}
	
	
	function doAction(gridid,action,arrids){
		if(arrids.length==0){
			return;
		}
		switch(action){
			case 'DELETE':
				$i.modal({
					show: true,
					title: $i.i18n('security:title_delete_groups'),
					description :  $i.i18n('security:desc_delete_groups'),
					success: function(){
						$i.promise._DELETE('groups',{gids : arrids})
						.done(function(){
							groupsL.reload();
						});
					},
				});
			break;
		default:
			console.log(gridid+':'+action+':'+arrids);
			break;
		}
	}

	this.callback = function(){
		groupsL.reload();
	}
	
	page.loaded();
});