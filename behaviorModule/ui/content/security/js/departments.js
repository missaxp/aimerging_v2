(function(page){
	page.config({
		autoshow : false, //Si volem gestionar manualment quan ensenyem la plana.
		object : 'departments', //Objecte de referencia el qual es mostrará algun dels seus fills en aquesta plana.
		type : 'list', //Tipus de pàgina.
		submit : newDepartment //Funció que s'executarà quan cliquem sobre l'acció.
	});
	
	function newDepartment(){
		$i('#modal-addDepartment').modal();
	}
	
	$i('#modal-addDepartment-submit').on('click', function(){
		 event.preventDefault(); // Stop form from submitting normally
		 if($i('#modal-addDepartment input[name="name"]').val().length!=0){
			 $i("#department-response-msg").removeClass("hidden alert-danger").addClass("alert-info").html("<i class='fa fa-spinner fa-spin orange bigger-125'></i>" +  $i.i18n("security:info_departmentcreation"));
			 // Get ajax promise for form serialized data
			 var data = {
				name : $i('#modal-addDepartment input[name="name"]').val()
			 };
			 
			 $i.promise._POST("departments",data)
			 .done(function(data){
				 if(typeof(departments)!=="undefined"){
					 departments.reload();
				 }
				 $i("#department-response-msg").addClass("hidden");
				 $i('#modal-addDepartment input[name="name"]').val('');
				 $i("#modal-addDepartment").modal('hide');
				 // Bug: modal-backdrop remains after modal hide
				 $('body').removeClass('modal-open');
			 })
			 .fail(function( jqXHR, textStatus ) {
				 $('#modal-addDepartment input[name="name"]').val('');
				 
				 $i("#department-response-msg").removeClass("hidden alert-info").addClass("alert-danger").html("<i class='fa fa-exclamation-triangle bigger-125'></i>" + $i.i18n("security:info_creation_error"));
				$('#modal-addDepartment-submit').prop("disabled",true);
			});
		 }
		 else{
			 $i("#department-response-msg").removeClass("hidden alert-info").addClass("alert-danger").html("<i class='fa fa-exclamation-triangle bigger-125'></i>" + $i.i18n("security:info_creation_error"));
		 }
		 
		 	
		return false; // Stop the normal form submission
	});
	
	$i('#modal-addDepartment-cancel').on('click',function(){
		$i('#modal-addDepartment-submit').prop("disabled",false);
		$i("#department-response-msg").addClass("hidden");
		$i("#modal-addDepartment").modal('hide');
		 // Bug: modal-backdrop remains after modal hide
		 $('body').removeClass('modal-open');
	});
	
	var departments = $i('#departments-list').superview({
		rest: 'departments',
		rowId: 'ID',
		defaultsort: '-USERS',
		tipscol: {del:true,action:buttonsFunction},
		dataType: ["ID","NAME"],
		i18n : 'security',
		cols: [{name: 'NAME'},{name: 'USERS'}]
	});
		
	function buttonsFunction(jqElement,action){
		var id = jqElement.data('id');
		var name = jqElement.data('dname');
		switch(action){
		case "button-delete":
			$i.modal({
				show: true,
				title: 'Delete department',
				description :  '<p>Are you sure you want to delete '+name+' department?</p>',
				success: function(){
					$i.promise._request("DELETE","departments/"+id)
					.done(function(){
						jqElement.hide('slow', function(){ jqElement.remove(); });
						$i.gritter({
							type: "success",
							title: "Success",
							description: 'Department <a class="orange">'+name+'</a> successfully deleted'
						});
					});
				}
			});
			break;
		case 'button-flag':
			id = jqElement.data("id");
			idcp.followLink('./' +  id,$i.i18n('security:pushStateAddUsers'));
			break;
		}
		
	}

	this.callback = function(){
		departments.reload();
	}
	
	page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
});