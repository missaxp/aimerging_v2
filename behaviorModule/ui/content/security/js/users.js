(function(page){
	page.config({
		autoshow : false, //Si volem gestionar manualment quan ensenyem la plana.
		object : 'users', //Objecte de referencia el qual es mostrará algun dels seus fills en aquesta plana.
		type : 'list' //Tipus de pàgina.
	});
	
	var users = $i('#usersl').superview({
		rest: 'users',
		allowTemplates : true,
		exportable : true,
		rowId: 'ID',
		sortable: true,
		defaultsort: '-NAME',
		searchable: "USERNAME,SURNAME,RNAME",
		rowClick: {enable:true,onClick: doRowClick},
		dataType: ["ID","ROLE_ID","CONNECTED","USERNAME"],
		appendCode: process,
		i18n : 'security',
		actionCol : {
       		colTitle: $i.i18n("security:col_drop_button"),
       		actions: [
               {action:'DELETE',title: $i.i18n("security:action_delete"),icon: 'fa fa-trash-o orange'},
            ],
            help: true, //Adds buttons to "check" and "uncheck" all checkboxes
            onclick: doAction
       	}, 
		cols: [{name: 'USERNAME'},{name: 'NAME'},{name: 'SURNAME'},{name: 'RNAME'},{name: 'CONNECTED'},{name : "CREATED"}],
		filter: [
			{field: 'RNAME',type: 'select',title: $i.i18n("security:col_role")},
			{field: 'GNAME',type: 'select',title:$i.i18n("security:col_gname")}
		]
	});
	
	function doRowClick($jqE){
		var uname = $jqE.data('username');
		$i.promise._GET({
			restURL : 'users/'+uname,
			bsend : $jqE,
		})
		.done(function(response){
			$i.hash.follow('security/users/' +  uname, uname,{'name':uname,'response' : response});
		});
	}
	
	
	function doAction(gridid,action,arrids){
		if(arrids.length==0){
			return;
		}
		switch(action){
			case 'DELETE':
				$i.modal({
					show: true,
					title: $i.i18n('security:title_delete_users'),
					description :  $i.i18n('security:desc_delete_users'),
					success: function(){
						$i.promise._DELETE('users',{uids : arrids})
						.done(function(){
							users.reload();
						});
					},
				});
			break;
		default:
			console.log(gridid+':'+action+':'+arrids);
			break;
		}
	}
	
	function process($table){
		var dev,$td;
		//Apply circles when showing logged users.
		$table.find('tbody > tr').each(function(){
			dev = $(this).data('connected');
			$td = $(this).find('td[data-column="connected"]');
			if(dev <= 0){
				$td.html('<span class="badge badge-grey">'+dev+'</span>');
			}
			else{
				$td.html('<span class="badge badge-success">'+dev+'</span>');
			}
		});
	}
	
	
	this.callback = function(){
		users.reload();
	}
	
	page.loaded();
});