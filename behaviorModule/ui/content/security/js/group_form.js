(function(page){
	page.config({
		autoshow : false,
		object : 'groups',
		type : 'form',
		api : 'groups',
		resource : $i._GET('gid'),
		trackChanges : true,
		showBack : true,
		submit : function(values,response){
			var permissions = [];
			//El tractament dels valors que enviarem el farem aqui... enviarem en format semblant al chmod.
			$.each(tables,function(){
				var $checkboxes = $content.find('tr[data-tid="'+this.id+'"]').find('input[type="checkbox"]');
				var u = "00000";
				var g = "00000";
				var o = "00000";
				var ur,gr,or = 0;
				var uw,gw,ow = 0;
				var ud,gd,od = 0;
				var uc,ua,ga,oa = false;
				$checkboxes.each(function(){
					switch($(this).data('object')){
						case 'user':
							switch($(this).data('permission')){
								case 'read':
									ur = $(this).is(':checked')? 1:0;
									break;
								case 'write':
									uw = $(this).is(':checked')? 1:0;
									break;
								case 'delete':
									ud = $(this).is(':checked')? 1:0;
									break;
								case 'create':
									uc = $(this).is(':checked')? 1:0;
									break;
								case 'administrate':
									ua = $(this).is(':checked')? 1:0;
									break;
							}
							break;
						case 'group':
							switch($(this).data('permission')){
								case 'read':
									gr = $(this).is(':checked')? 1:0;
									break;
								case 'write':
									gw = $(this).is(':checked')? 1:0;
									break;
								case 'delete':
									gd = $(this).is(':checked')? 1:0;
								case 'administrate':
									ga = $(this).is(':checked')? 1:0;
									break;
							}
							break;
						case 'others':
							switch($(this).data('permission')){
							case 'read':
								or = $(this).is(':checked')? 1:0;
								break;
							case 'write':
								ow = $(this).is(':checked')? 1:0;
								break;
							case 'delete':
								od = $(this).is(':checked')? 1:0;
								break;
							case 'administrate':
								oa = $(this).is(':checked')? 1:0;
								break;
						}
							break;
					}
				});
				
				u = uc.toString()+ur.toString()+uw.toString()+ud.toString()+ua.toString();
				g = "0"+gr.toString()+gw.toString()+gd.toString()+ga.toString();
				o = "0"+or.toString()+ow.toString()+od.toString()+oa.toString();
				
				permissions.push({
					table_id : this.id,
					p_group : {
						user : u,
						group : g,
						others : o
					}
				});
				//console.log("Table: "+permission.table_name + ", user: " +u+", group: "+g+", others: "+o);
			});
			
			var toSend = {
				name : $i('#group_name').val(),
				permissions : permissions
			};
							
			
			var method = page.newElement? _POST_:_PUT_;
			var url = page.newElement? 'groups':'groups/'+gid;									
			$i.promise._request({
				method : method,
				restURL : url,
				resource : $i('#group_name').val(),
				data : toSend,
				always : response
			})
			.done(function(rsp){
				if(page.newElement){
					page.newElement = false;
					page._setResource(rsp.data.id);
					gid = rsp.data.id;
					page.buttons.showDelete();
					$i.hash._set('security/groups/'+gid, false); //Update hash but not follow symLink.					
					$i.breadcrumb._set(gid);
				}						
			});
		}
	});
	
	var gid = $i._GET('gid');
	var _POST = $i._POST();
	var tables = [];
	var $content;
	if(page.newElement){
		$i.promise._GET('mant/tables').done(loadPage);
	} else {
		if(_POST!==false && typeof(_POST.response)!=='undefined'){ 
			loadPage(_POST.response);
		}
		else{ 		
			$i.promise._GET("groups/"+gid).done(loadPage);
		}
	}

	function loadPage(rsp){
		if(!page.newElement){
			$i('#group_name').val(rsp.data.name);
		}

		$content = $i('#table-content');
		var permissions = ['create','read','write','delete','administrate'];
		var objects = ['user','group','others'];
		if(!page.newElement){
			$.each(rsp.data.permissions,function(){
				permission = this;
				tables.push({id : this.table_id, name : this.table_name});
				
				var $trTable = $(moduleCanvas(permission));
				$content.append($trTable);
				var chmod = {
						user : permission.group_p.u,
						group : permission.group_p.g,
						others : permission.group_p.o
				};
				for(var k=0,l=objects.length;k<l;k++){
					for(var i=0,j=permissions.length;i<j;i++){
						var $chk = $trTable.find('input[name="'+objects[k]+'_'+permissions[i]+'"]');
						$chk.prop('checked',((chmod[objects[k]].charAt(i)=="1")? true:false));
					}
				}
			});
		}
		else{
			$.each(rsp.data.results,function(){
				if(this.granular_p=="Y"){
					tables.push({id : this.id, name : this.name});
					var $trTable = $(moduleCanvas({ table_id : this.id,table_name : this.name}))
					$content.append($trTable);
				}
			});
		}
		
		
		var maxPos = 3; //0 -> read, 1-write 2-delete 3-administrate. Marca un id per a cada checkbox... us en el formulari.
		var $tablestr = $content.find('tr');
		$.each($tablestr,function(){
			$(this).find('input[type="checkbox"]').on('click',function(){
				var $table = $(this).closest('tr');
				var table_id = $table.data('tid');
				var chk_name = $(this).attr('name');
				var object = $(this).data('object');
				var pos = $(this).data('d');
				if($(this).is(":checked")){
					for(var i=pos,j=0;i>=j;i--){
						var $ochk = $table.find('input[type="checkbox"][data-d="'+i+'"][data-object="'+object+'"]');
						$ochk.prop('checked',true);
						$ochk.trigger('change');
						
					}
				}
				else{
					for(var i=pos,j=maxPos;i<=j;i++){
						var $ochk = $table.find('input[type="checkbox"][data-d="'+i+'"][data-object="'+object+'"]');
						$ochk.prop('checked',false);
						$ochk.trigger('change');
					}
				}
			});
		});
		
		page.loaded();
		
	}
	
	function moduleCanvas(permission){
		var title = '\
			<tr class="odd" data-tid="'+permission.table_id+'">\
				<td data-column-sortable="no" data-resizable="false" role="columnheader" rowspan="1" colspan="1">'+permission.table_name+'</td>\
				<td data-column-sortable="no" data-resizable="false" role="columnheader" rowspan="1" colspan="1" style="border-style:none;">\
					<label>\
						<input type="checkbox" name="user_create" class="ace" data-object="user" data-permission="create">\
						<span class="lbl"></span>\
					</label>\
				</td>\
				<td data-column-sortable="no" data-resizable="false" role="columnheader" rowspan="1" colspan="1" style="border-style:none;">\
					<label>\
						<input type="checkbox" name="user_read" class="ace" data-object="user" data-permission="read" data-d="0">\
						<span class="lbl"></span>\
					</label>\
				</td>\
				<td data-column-sortable="no" data-resizable="false" role="columnheader" rowspan="1" colspan="1" style="border-style:none;">\
					<label>\
						<input type="checkbox" name="user_write" class="ace" data-object="user" data-permission="write" data-d="1">\
						<span class="lbl"></span>\
					</label>\
				</td>\
				<td data-column-sortable="no" data-resizable="false" role="columnheader" rowspan="1" colspan="1" style="border-style:none;">\
					<label>\
						<input type="checkbox" name="user_delete" class="ace" data-object="user" data-permission="delete" data-d="2">\
						<span class="lbl"></span>\
					</label>\
				</td>\
				<td data-column-sortable="no" data-resizable="false" role="columnheader" rowspan="1" colspan="1" style="border-style:none;">\
					<label>\
						<input type="checkbox" name="user_administrate" class="ace" data-object="user" data-permission="administrate" data-d="3">\
						<span class="lbl"></span>\
					</label>\
				</td>\
				<td data-column-sortable="no" data-resizable="false" role="columnheader" rowspan="1" colspan="1" style="border-style: none;border-left:1px solid #ddd;">\
					<label>\
						<input type="checkbox" name="group_read" class="ace" data-object="group" data-permission="read" data-d="0">\
						<span class="lbl"></span>\
					</label>\
				</td>\
				<td data-column-sortable="no" data-resizable="false" role="columnheader" rowspan="1" colspan="1" style="border-style:none;">\
					<label>\
						<input type="checkbox" name="group_write" class="ace" data-object="group" data-permission="write" data-d="1">\
						<span class="lbl"></span>\
					</label>\
				</td>\
				<td data-column-sortable="no" data-resizable="false" role="columnheader" rowspan="1" colspan="1" style="border-style:none;">\
					<label>\
						<input type="checkbox" name="group_delete" class="ace" data-object="group" data-permission="delete" data-d="2">\
						<span class="lbl"></span>\
					</label>\
				</td>\
				<td data-column-sortable="no" data-resizable="false" role="columnheader" rowspan="1" colspan="1" style="border-style:none;">\
					<label>\
						<input type="checkbox" name="group_administrate" class="ace" data-object="group" data-permission="administrate" data-d="3">\
						<span class="lbl"></span>\
					</label>\
				</td>\
				<td data-column-sortable="no" data-resizable="false" role="columnheader" rowspan="1" colspan="1" style="border-style: none;border-left:1px solid #ddd;">\
					<label>\
						<input type="checkbox" name="others_read" class="ace" data-object="others" data-permission="read" data-d="0" style="border-style:none;">\
						<span class="lbl"></span>\
					</label>\
				</td>\
				<td data-column-sortable="no" data-resizable="false" role="columnheader" rowspan="1" colspan="1" style="border-style:none;">\
					<label>\
						<input type="checkbox" name="others_write" class="ace" data-object="others" data-permission="write" data-d="1" >\
						<span class="lbl"></span>\
					</label>\
				</td>\
				<td data-column-sortable="no" data-resizable="false" role="columnheader" rowspan="1" colspan="1" style="border-style:none;">\
					<label>\
						<input type="checkbox" name="others_delete" class="ace" data-object="others" data-permission="delete" data-d="2">\
						<span class="lbl"></span>\
					</label>\
				</td>\
				<td data-column-sortable="no" data-resizable="false" role="columnheader" rowspan="1" colspan="1" style="border-style:none;">\
					<label>\
						<input type="checkbox" name="others_administrate" class="ace" data-object="others" data-permission="administrate" data-d="3">\
						<span class="lbl"></span>\
					</label>\
				</td>\
			</tr>';
		return title;
	}
	
	
});