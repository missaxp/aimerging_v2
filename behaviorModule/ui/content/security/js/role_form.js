/**
 * One of the firsts scripts made in this project.
 * It may be different from the new ones, due the $i framework has suffer a lot of changes.
 * 
 * It works and it has been upgraded to use the new $i features, but the code is a little old.
 * 
 * @author phidalgo.
 */
(function(page){
	page.config({
		autoshow : false,
		object : 'roles',
		type : 'form',
		api : 'roles',
		resource : $i._GET('role'),
		trackChanges : true,
		showBack : true,
		submit : function(values,response){
				var toSend = {
					name : $i('#name').val(),
					modules : []
				};
				
				$.each(modules,function(){
					var module = { id: this.id,name : this.name, actions : []};
					$i('#actions_'+this.id+' :input').each(function(){
						module.actions.push({id : $(this).data('action-value'), allowed : $(this).is(':checked')});
					});
					toSend.modules.push(module);
				});
			
				var method = page.newElement? _POST_:_PUT_;
				var url = page.newElement? 'roles':'roles/'+rid;									
				$i.promise._request({
					method : method,
					restURL : url,
					resource : $i('#name').val(),
					data : toSend,
					always : response
				})
				.done(function(rsp){
					if(page.newElement){
						page.newElement = false;
						page._setResource(rsp.data.id);
						rid = rsp.data.id;
						page.buttons.showDelete();
						$i.hash._set('security/roles/'+rid, false); //Update hash but not follow symLink.					
						$i.breadcrumb._set($i('#name').val());
					}						
				});
			}
	});
	
	
	var rid = $i._GET('role');
	var _POST = $i._POST();	
	if(page.newElement){
		$i.promise._GET("roles/options").done(loadData);
	} else {
		if(_POST!==false && typeof(_POST.response)!=='undefined'){ 
			loadData(_POST.response);
		}
		else{ 		
			$i.promise._GET("roles/"+rid).done(loadData);
		}
	}
	
	var modules;
	function loadData(rsp){
		modules = rsp.data.options.modules;
		showInfo(modules);
		$i('input[data-toggleAccordion="true"]').on('click',function(){
			var mid = $(this).data('module-id');
			if($(this).is(':checked')){
				$i('#actions_'+mid).show("fast");
			}
			else{
				$i('#actions_'+mid).hide("fast");
				$i('#actions_'+mid).find(':input').attr('checked',false).trigger('change');
			}
		});
		
		
		if(!page.newElement){
			$i('#name').val(rsp.data.name);
			checkActions(rsp.data.modules);
		}
		
		page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
	}
	
	
	function checkActions(modules){
		$.each(modules,function(){
			var module = this;
			if(module.actions.length>0){
				$i('#module_'+this.id).click();
			}
			$.each(module.actions,function(){
				$i('#actions_'+module.id).find('input[data-action-value="'+this.action_id+'"]').attr('checked', true);
			});
		});
	}
	
	function showInfo(modules){
		$.each(modules,function(){
			module = this;
			$i('#role_content').append(moduleCanvas(module));
			actions = $i('#actions_'+module.id);
			var action = '';
			$.each(module.actions,function(){
				action += '<div class="row">'+
						'<div class="col-sm-1"></div>'+
						'<div class="col-sm-3">'+
							'<label>'+
								'<input name="action_'+this.id+'" type="checkbox" data-action-value="'+this.id+'" class="ace">'+
								'<span class="lbl"> '+$i.i18n('security:roleAction_'+this.alias_id)+'</span>'+
							'</label>'+
						'</div>'+
						'<div class="col-sm-7">'+
							'<span class="lbl"><small>'+this.description+'</small></span>'+
						'</div>'+
						'<div class="col-sm-1"></div>'+
					'</div>';
				
			});
			actions.append(action);
			
		});
	}
	
	function moduleCanvas(module){
		var title = ''+
		'<h3 class="header smaller lighter blue">'+
			'<span class="span7">'+$i.i18n('security:roleModule_'+module.name)+' </span>'+
			'<small>'+$i.i18n('security:lbl_module')+'</small>'+
			'<span class="span5">'+
				'<label class="pull-right inline">'+
					'<small class="muted"></small>'+
					'<input nosend name="module_'+module.id+'" data-id="module_'+module.id+'" data-module-id="'+module.id+'" type="checkbox" class="ace ace-switch ace-switch-5" data-toggleAccordion="true">'+
					'<span class="lbl"></span>'+
				'</label>'+
			'</span>'+
		'</h3>'+
		'<div class="panel-collapse" data-type="actions-list" data-id="actions_'+module.id+'" style="display:none;">'+
		'</div>';
		return title;
	}
});