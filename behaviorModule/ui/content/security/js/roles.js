(function(page){
	page.config({
		autoshow : false, //Si volem gestionar manualment quan ensenyem la plana.
		object : 'roles', //Objecte de referencia el qual es mostrará algun dels seus fills en aquesta plana.
		type : 'list', //Tipus de pàgina.
	});
	
	var roles = $('#roles-list').superview({
		rest: "roles",
		rowId: 'ID',
		defaultsort: '+NAME',
		tipscol: {edit:true,del:true,action:buttonsFunction},
		dataType: ["ID","NAME"],
		i18n : 'security',
		cols: [{name: 'NAME'},{name: 'USERS'}],
	});
	
	function buttonsFunction(jqelement,action){
		//Example delete action with confirm modal on delete 
		if($i._get('debug')){
			console.log(action+" on id "+jqelement.attr("id"));
		}
		switch(action){
		case 'button-delete':
			actionDelete(jqelement);
			break;
		case 'button-edit':
			$i.hash.follow('./' +  jqelement.data("id"),jqelement.data("name"));
			break;
		}
	}

	function actionDelete(jqElement){
		var id = jqElement.data("id");
		var name = jqElement.data('name');
		$i.modal({
			show: true,
			title: 'Delete Role',
			description :  '<p>Are you sure you want to delete '+name+' role?</p>',
			success: function(){
				$i.promise._request("DELETE","roles/"+id)
				.done(function(){
					jqElement.hide('slow', function(){ jqElement.remove(); });
					$i.gritter({
						type: "success",
						title: "Success",
						description: 'Role <a class="orange">'+name+'</a> successfully deleted'
					});
				});
			},
		});
	}
	
	this.callback = function(){
		roles.reload();	
	}
	
	page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
});