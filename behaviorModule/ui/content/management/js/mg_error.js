(function(page){
	var egrid = $i('#errorloggrid').superview({
		rest: "management/errorHandler",
		allowTemplates : true,
		exportable : true,
		rowId: 'CODE',
		defaultsort: '-DATETIME',
		dataType: ["CODE","TYPE"],
		i18n : 'management',
		cols: [{name: 'CODE'},{name: 'TYPE'},{name: 'MESSAGE'},{name: 'CUSTOM_MESSAGE'},{name: 'USERNAME'},
		    //{name: 'DATETIME',dataType:'DATETIME'},
		    {name: 'DATETIME'},
		    {name: 'IP'},{name: 'COUNTER'},{name: 'FILEE'},{name: 'LINE'},
		],
		filter: [
		    {field: 'USERNAME',type: 'select',title:$i.i18n('management:lbl_user')},
			{field: 'DATETIME',type: 'date',title:$i.i18n('management:lbl_date')},
		    {field : "TYPE",type : 'select',title : $i.i18n('management:lbl_type'),dataSource : [{option :'API',value : 'API'},{option : 'ORACLE', value : 'ORACLE'},{ option :'IDCP Error',value :'IDCP Error'},{ option :'Javascript Error',value :'Javascript Error'}]}
		]
	});
	
	function fillUsers(jqelement){
		return $i.promise._request("GET","users",{returnFields: ["ID","USERNAME"]})
		.done(function(data){
			$.each(data.data.results,function(){
				jqelement
				.append($("<option></option>")
				.attr("value",this.id)
				.text(this.username));
			});
		});
	}
	
	this.callback = function(){
		egrid.reload();
	}
});