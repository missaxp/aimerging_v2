(function(page){
	$i.require(["assets/js/flot/jquery.flot.js",
	            "assets/js/flot/jquery.flot.pie.js",
	            "assets/js/flot/jquery.flot.resize.js"]
	)
	.done(function(){
		var options = {
				series: {
					shadowSize: 0	// Drawing is faster without shadows
				},
				yaxis: {
					//min: 0,
					//max: 100
				},
				xaxis: {
					show: false
				}
			};
	
		var cpuLoad = [];
	
		var vhcpuuse = [];
	
		var memLoad = [];
	
		var vhreqpersec = [];
	
		var apreqpersec = [];
	
		var kbitsPerSec = [];
	
		var responseTime = [];
	
		var respkbytes = [];
	
		var plot = $.plot("#svr-cpu-load", [[0,0]], options);
	
		var plot2 = $.plot("#svr-memory-load", [[0,0]], options);
	
		var plot3 = $.plot("#apache-req-per-sec", [[0,0]], options);
	
		var plot4 = $.plot("#apache-kbits-sec", [[0,0]], options);
	
		var plot5 = $.plot("#svr-avg-response-time", [[0,0]], options);
	
		// Fetch one series, adding to what we already have
	
		function fetchSeries() {
			function onDataReceived(series) {
				// Push the new data onto our existing data array
				memFree = series.data.memory.MemFree;
				memTotal = series.data.memory.MemTotal;
				memAlloc = series.data.memory.MemTotal - series.data.memory.MemFree;
				memAllocP = ((memAlloc/memTotal)*100).toFixed(2);
				
				memLoad.push([0,memAllocP]);
				cpuLoad.push([0,series.data.cpu]);
				vhcpuuse.push([0,series.data.apache.CPULoad]);
				vhreqpersec.push([0,series.data.log.reqpersec]);
				respkbytes.push([0,series.data.log.respkbytes]);
				responseTime.push([0,series.data.log.avgms]);
				apreqpersec.push([0,series.data.apache.ReqPerSec]);
				kbitsPerSec.push([0,(series.data.apache.BytesPerSec/1024)]);
				
				plot = $.plot("#svr-cpu-load", cpuLoad, options);
				plot2 = $.plot("#svr-memory-load", memLoad, options);
				plot3 = $.plot("#apache-req-per-sec", vhreqpersec, options);
				plot4 = $.plot("#apache-kbits-sec", respkbytes, options);
				plot5 = $.plot("#svr-avg-response-time", responseTime, options);
				
				
			}
			
			$i.promise._request("POST","management/getServerStatus")
			.done(function(data){
				onDataReceived(data);
			});
		}
	
		// Initiate a recurring data update
	
		function update() {
	
			cpuLoad = [];
			$.plot("#svr-cpu-load", cpuLoad, options);
	
			var iteration = 0;
	
			function fetchData() {
	
				++iteration;
	
				function onDataReceived(series) {
	
					if (cpuLoad.length > 30){
						cpuLoad = cpuLoad.slice(1);
					}
					
					if(memLoad.length > 30){
						memLoad = memLoad.slice(1);
					}
					
					if(vhcpuuse.length > 30){
						vhcpuuse = vhcpuuse.slice(1);
					}
					
					if(vhreqpersec.length > 30){
						vhreqpersec = vhreqpersec.slice(1);
					}
					
					if(apreqpersec.length > 30){
						apreqpersec = apreqpersec.slice(1);
					}
					
					if(kbitsPerSec.length > 30){
						kbitsPerSec = kbitsPerSec.slice(1);
					}
					
					if(responseTime.length > 30){
						responseTime = responseTime.slice(1);
					}
					
					if(respkbytes.length > 30){
						respkbytes = respkbytes.slice(1);
					}
					
					
	
					
					memFree = series.data.memory.MemFree;
					memTotal = series.data.memory.MemTotal;
					memAlloc = series.data.memory.MemTotal - series.data.memory.MemFree;
					memAllocP = ((memAlloc/memTotal)*100).toFixed(2);
					
				
					memLoad.push([iteration,memAllocP]);
					vhcpuuse.push([iteration,series.data.apache.CPULoad]);
					cpuLoad.push([iteration,series.data.cpu]);
					vhreqpersec.push([iteration,series.data.log.reqpersec]);
					responseTime.push([iteration,series.data.log.avgms]);
					apreqpersec.push([iteration,series.data.apache.ReqPerSec]);
					kbitsPerSec.push([iteration,(series.data.apache.BytesPerSec/1024)]);
					respkbytes.push([iteration,series.data.log.respkbytes]);
					
					$.plot("#svr-cpu-load", [cpuLoad ], options);
					$.plot("#svr-memory-load", [ memLoad ], options);
					$.plot("#apache-req-per-sec", [ vhreqpersec ], options);
					$.plot("#apache-kbits-sec",[ respkbytes ], options);
					$.plot("#svr-avg-response-time",[ responseTime ], options);
				}
				
				$i.promise._request("POST","management/getServerStatus")
				.done(function(data){
					onDataReceived(data);
				});
			}
			$i.intervals.pages._add('api-graphs-interval',fetchData,2000);
		}
	
		$i('a[data-action="reload"]').on('click',function(){
			$i("#api-rest-urls-show").html('');
			requestedRestURLS();
		});
	
		function requestedRestURLS(){
			//Requested URLS
			$i.promise._request("POST","management/getRequestedURLS")
			.done(function(data){
				var inner = '';
				$.each(data.data,function(k,v){
					var icon = null;
					switch(v.RMETHOD){
						case "GET":
							icon = "fa fa-cloud-download blue bigger-200";
							break;
						case "PUT":
							icon = "fa fa-exchange blue bigger-200";
							break;
						case "POST":
							icon = "fa fa-cloud-upload green bigger-200";
							break;
						case "DELETE":
							icon = "fa fa-trash-o red bigger-200";
							break;
					}
					
					if(v.STATUS>=400){
						icon = "fa fa-eye-slash red bigger-200";
					}
					
					inner += ''+
					'<tr>'+
					'<td>'+
						'<span class="span7">'+v.APIV+'</span>'+
					'</td>'+
					'<td>'+
						'<span class="span7">'+v.USERNAME+'</span>'+
					'</td>'+				
					'<td>'+
						'<span class="smaller lighter blue span7">'+v.MODULE+' </span>'+
					'</td>'+
					'<td>'+
						'<span class="span7">'+v.DETAIL+'</span>'+
					'</td>'+
					'<td>'+
						'<span class="span7">'+v.COUNT+'</span>'+
					'</td>'+
					'<td>'+
						'<i class="'+icon+'" data-status="'+v.STATUS+'" title="'+v.STATUS+'"></i>'+
					'</td>'+
					'</tr>';
				});
				$i("#api-rest-urls-show").append(inner);
				
				$i('#recent_api_activities tbody td i').each(function(){
					$(this)
					.tooltip({
						show: {
							effect: "slideDown",
							delay: 250
						}
					});
				});
			});
		}
		
		// Load the first series by default, so we don't have an empty plot
		fetchSeries();
		update();
		requestedRestURLS();
	});
});