(function(page){
	page.config({
		autoshow : false, //Si volem gestionar manualment quan ensenyem la plana.
		object : 'error', //Objecte de referencia el qual es mostrará algun dels seus fills en aquesta plana.
		type : 'detail', //Tipus de pàgina.
	});
	
	var tasksList = $i('#ai-tasks-error').superview({
		rest: 'ai/error',
		allowTemplates : true,
		exportable : true,
		rowId: 'TYPE',
		sortable: true,
		defaultsort: '-EDATE',
		appendCode: process,
		i18n : 'ai',
		cols: [{name: 'TYPE'},{name: 'DESCRIPTION'},{name: 'EDATE'},{name: 'CLASS'}],
		filter: [
			//{field: 'TITLE',type: 'text',title: $i.i18n("ai:col_title")},
			{field: 'TYPE',type: 'select',title: $i.i18n("ai:col_type")},
			{field: 'EDATE',type: 'date',title: $i.i18n("ai:col_date")}
		]
	});
	
	
	function doRowClick($jqE){
		var usid = $jqE.data('id');
		$i.promise._GET({
			restURL : 'ai/task/'+usid,
			bsend : $jqE,
		})
		.done(function(response){
			$i.hash.follow('ai/tasks/' +  usid, usid,{'name':usid,'response' : response});
		});
	}
	
	function doAction(){
		
	}
	
	function process($table,hasresults){
		var tds = $table.find('tbody > tr > td[data-column="statename"]');
		$.each(tds,function(){
			var $td = $(this);
			var value = $td.data('value');
			switch(value){
				case "PROCESSED":
					$td.children().attr("class","label label-success arrowed-in arrowed-in-right");
					//$td.children().html($i.i18n("tasks:assigned"));
					break;
				case "PROCESSEDWITHWARNING":
					$td.children().attr("class","label label-warning arrowed-in arrowed-in-right");
					//$td.children().html($i.i18n("tasks:ready"));
					break;
				case "ERROR":
					$td.children().attr("class","label label-danger");
					$td.children().html('<i class="icon-warning-sign bigger-120"></i>ERROR');
					break;
				case "REJECTED":
					$td.children().attr("class","label label-info arrowed-in");
					//$td.children().html($i.i18n("tasks:delivered"));
					break;
				case 5:
					$td.children().attr("class","label label-info arrowed-in-right arrowed");
					//$td.children().html($i.i18n("tasks:received"));
					break;
				case 6:
					$td.children().attr("class","label label-inverse");
					$td.children().html($i.i18n("tasks:closed"));
					break;						
			}
		});
	}
	
	
	this.callback = function(){
	}
	
	page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
});