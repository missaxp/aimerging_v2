(function(page){
	var usid = $i._GET('usid');
	
	page.config({
		autoshow : false, //Si volem gestionar manualment quan ensenyem la plana.
		object : 'aitask', //Objecte de referencia el qual es mostrará algun dels seus fills en aquesta plana.
		showBack : true,
		type : 'detail' //Tipus de pàgina.
	});
	
	
	var _POST = $i._POST();
	if(_POST!==false && typeof(_POST.response)!=='undefined'){ //If user data is passed through var, load it.
		loadData(_POST.response);
	}
	else{ //If not, we load via web service.
		$i.promise._GET('ai/task/'+usid).done(loadData);
	}
	
	function loadData(rsp){
		var data = rsp.data;
//		console.log(data);
		$i.breadcrumb._set(data.uniquesourceid); // Set tpid on aria bar.
		$i('#task-header-title').html(data.title);
		$i('#task-header-usid').html(data.uniquesourceid);
		//$i('#task-usid').html(data.uniquesourceid);
		//$i('#task-utid').html(data.uniquetaskid);
		
		var stateNameClass = "";
		switch(data.state.statename){
			case "PROCESSED":
				stateNameClass = "label label-success arrowed-in arrowed-in-right";
				//$td.children().html($i.i18n("tasks:assigned"));
				break;
			case "PROCESSEDWITHWARNING":
				stateNameClass = "label label-warning arrowed-in arrowed-in-right";
				//$td.children().html($i.i18n("tasks:ready"));
				break;
			case "ERROR":
				stateNameClass = "label label-danger";
				break;
			case "REJECTED":
				stateNameClass = "label label-info arrowed-in";
				//$td.children().html($i.i18n("tasks:delivered"));
				break;
			case 5:
				stateNameClass = "label label-info arrowed-in-right arrowed";
				//$td.children().html($i.i18n("tasks:received"));
				break;
			case "COLLECTED":
				break;
			case 6:
				stateNameClass = "label label-inverse";
				break;						
		}
		
		
		$i('#task-status').html('<span class="'+stateNameClass+'">'+data.state.statename+'</span>');
		dueDate = data.duedate.date;
		dueDate = dueDate.split("").reverse().join("");
		dueDate = dueDate.slice(dueDate.indexOf(":"))
		dueDate = dueDate.slice(1)
		dueDate = dueDate.split("").reverse().join("");
		//dueDate = dueDate.slice()
		$i('#task-source').html(data.datasource.sourcename);
		$i('#task-tms-url').html(data.datasource.tmstaskurl);
		$i('#task-harvest-method').html(data.datasource.harvestmethod);
		$i('#task-client-id').html(data.idclientetask);
		$i('#task-duedate').html(dueDate);
		$i('#task-sourcelanguage').html(data.sourcelanguage.languagename);
		var tls = "";
		$.each(data.targetslanguage, function(){
			tls += this.languagename+',';
			$.each(this.analysis, function(k, v){
				if(k!=='idanalis'){
					$i('#task-wordcount').append('<tr><td><strong>'+$i.i18n('ai:wordcount_'+k)+'</strong></td><td>'+v+'</td>');
				}
			});
		});
		
		$.each(data.aditionalproperties, function(k, v){
			$i('#task-additional-properties').append('<tr><td><strong>'+v.propertyname+'</strong></td><td>'+v.propertyvalue+'</td>');
		});
		
		$i('#task-targetslanguage').html(tls.slice(0, -1));
		
		$.each(data.statushistory, function(){
			$i('#task-status-history').append('<li>'+this.timestamp+' - <strong>'+this.statename+'</strong> - <i>'+this.description+'</i></li>');
		});
		
		
		page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
	}
	
	
	this.callback = function(){
	}
	
	
});