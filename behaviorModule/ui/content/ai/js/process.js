(function(page){
	page.config({
		autoshow : false, //Si volem gestionar manualment quan ensenyem la plana.
		object : 'roles', //Objecte de referencia el qual es mostrará algun dels seus fills en aquesta plana.
		type : 'list', //Tipus de pàgina.
	});
	
	var tasksList = $i('#ai-process-view').superview({
		rest: 'ai/process',
		allowTemplates : true,
		exportable : true,
		rowId: 'IDPROCESS',
		sortable: true,
		defaultsort: '+PRIORITY',
		//searchable: "NAME",
		rowClick: {enable:true,onClick: doRowClick},
		dataType: ["NAME"],
		appendCode: process,
		i18n : 'ai',
		cols: [{name : 'ENABLED'},{name: 'NAME'},{ name : 'TASKSPROCESSED'},{ name : 'SOURCE'}],
		filter: [
			//{field: 'NAME',type: 'text',title: $i.i18n("ai:col_name")},
			{field: 'SOURCE',type: 'select',title: $i.i18n("ai:col_source")}
		]
	});
	
	function doRowClick($jqE){
		var processId = $jqE.attr('data-id');
		$i.promise._GET({
			restURL : 'ai/process/'+processId,
			bsend : $jqE,
		})
		.done(function(response){
			$i.hash.follow('ai/process/' +  processId, $jqE.attr('data-name'),{'name': $jqE.attr('data-name') ,'response' : response});
		});
	}
	
	function doAction(){
		
	}
	
	function process($table,hasresults){
		var tds = $table.find('tbody > tr > td[data-column="enabled"]');
		$.each(tds,function(){
			var enabled = ($(this).attr('data-value')=="1");
			//$(this).html('<input type="checkbox" name="myoption" class="ace ace-switch ace-switch-4 btn-rotate" '+(enabled? 'checked="checked"':'')+' >');
			
			//
			$(this).html('<label><input type="checkbox" name="auto_imputation" '+(enabled?'checked="checked"':'')+' data-id="auto_imputation" id="enabled" class="ace ace-switch ace-switch-4 btn-rotate" /><span class="lbl" data-lbl=""></span></label>');
			$(this).off();
			checkbox = $(this).find('input[type="checkbox"]');
			checkbox.on('click',function (event) {
				object = new Object();
				object.enabled = $(this).is(':checked');
				idProcess = $(this).parent().parent().parent().attr('data-id');
				$i.promise._PUT('ai/process/enabled/'+idProcess,object);
			});
		});
	}
	
	this.callback = function(){
		taskList.reload();
	}
	
	page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
});