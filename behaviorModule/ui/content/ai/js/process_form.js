	(function(page){
	var pid = $i._GET('pid');
	// console.log(page);
	page.config({
		autoshow : false,
		object : 'aiprocess',
		api : 'ai/process',
		resource : $i._GET('pid'),
		type : 'form',
		trackChanges : true,
		showBack : true,
		afterSubmit : function(rsp,values){
			if(page.newElement){
				if(rsp.data.process && rsp.data.process > 0){
					$i.breadcrumb._set(rsp.data.data.processname);
					page._setResource(rsp.data.process);
					var $btab = $i('.tabbable > li[data-tab="ai-rules"]');
					var $ctab = $i('.tabbable > li[data-tab="ai-actions"]');
					$btab.removeClass("disabled");
					$ctab.removeClass("disabled");
					$btab.off('click',disableTab);
					$ctab.off('click',disableTab);
					loadData(rsp.data);
				}
			}
		},
		beforeSubmit: function (process) {

			if(!validateEmails($i('#not-emails').val())){
				sendNotificationFail('Notificaction', 'No correo para enviar las notificaciones');
			}
		}
	});

	if (page.newElement) {
		$i('.tabbable > li[data-tab="ai-rules"]').on("click", function(e) {
			if ($(this).hasClass("disabled")) {
    			e.preventDefault();
    			return false;
 			}
			
 		});

 		$i('.tabbable > li[data-tab="ai-actions"]').on("click", function(e) {
			if ($(this).hasClass("disabled")) {
    			e.preventDefault();
    			return false;
 			}
 		});
	}
	if (!page.newElement) {
		var $btab = $i('.tabbable > li[data-tab="ai-rules"]');
		var $ctab = $i('.tabbable > li[data-tab="ai-actions"]');
		$btab.removeClass("disabled");
		$ctab.removeClass("disabled");
		$btab.off('click',disableTab);
		$ctab.off('click',disableTab);
	}

	function disableTab(e){
		e.stopPropagation();
		e.preventDefault();
		return;
	}

	$i("#not-emails").keypress(function (event) {
		if ($i("#not-emails").val()) {
			if(validateEmails($i("#not-emails").val())){
				parent = $i("#not-emails").parent().parent();
				parent.attr('class','col-md-8 has-success');
				parent.find("span > i").attr('class','ace-icon fa fa-check-circle');
			}else{
				parent = $i("#not-emails").parent().parent();
				parent.attr('class','col-md-8 has-error');
				parent.find("span > i").attr('class','ace-icon fa fa-times-circle');
			}
		}
	});
	
	var _POST = $i._POST();
	if(_POST!==false && typeof(_POST.response)!=='undefined'){ //If user data is passed through var, load it.
		loadData(_POST.response);
	}
	else{ //If not, we load via web service.
		$i.promise._GET('ai/process/'+pid).done(loadData);
	}
	
	// var taskProperties = null;
	
	function loadData(rsp){
		if (rsp.data.processname) {
			processName = rsp.data.processname;
		}

		if(!page.newElement){
			$i.breadcrumb._set(rsp.data.processname);
			page.buttons.addExtraButton([{
				icon: 'fa fa-clone', tooltip: $i.i18n('ai:btn_clone'),click: function(){
					goToProcess = false;
					idProcess = 0;
					$modal = $i.modal({
						title : "Nueva accion",
						description : '<form><input type="text" name="processClone" class="form-control" id="cloneName" data-id="cloneName" required="true"></form>',	
						show: true,
						width: '800px',
						height : '700px',
						resizable : false,
						execute : function(id,$modal){
							date = new Date();
							date = date.getDate() + "/" + (date.getMonth() +1) + "/" + date.getFullYear()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+" GTM "+ (-1*(date.getTimezoneOffset()/60));
							$modal.find('input').val("Copy of "+processName+" "+date);
						},
						success: function(id,$widget,$modal){
							if(!$modal.validate()){
								return false;
							}

							if (goToProcess) {
								$i.hash.follow('ai/process/'+idProcess);
								return goToProcess;
							}else{
								$i.promise._POST("ai/process/clone/"+rsp.data.idprocess,{process: $modal.find('input').val()},true,false).done(function (rsp) {
									if (rsp.data.result) {
										sendNotificationSuccess($i.i18n('ai:tlt_notification'),$i.i18n('ai:msg_process_clone'));
										button = $('.modal-dialog').find('div[class="modal-footer"] > button[class="btn btn-sm btn-primary"]');
										button.empty();
										button.append('<i class="fa fa-arrow-right"></i><span>'+$i.i18n('ai:btn_go_to_process')+'</span>');
										goToProcess = true;
										idProcess = rsp.data.idprocess;
									}else{
										sendNotificationFail($i.i18n('ai:tlt_notification'),$i.i18n('ai:msg_process_no_clone'));
									}
								});
							}
							return false;
						}
					});
					
				}
			}]);
		}
		else{
			$i.promise._GET('ai/source').done(function(data){
				console.log(data.data);
				$.each(data.data, function(k,v){
					$i('#source').append('<option value="'+v.id+'">'+v.name+'</option>');
				});
			});
		}
		
		var data = rsp.data;
		taskProperties = rsp.data.taskproperties;
		// console.log(taskProperties);
		
		if (data.idprocess) {		
			$i('#header-process-name').html(data.processname);
			$i('#name').val(data.processname);
			$i('#description').html(data.description);
			
			if (data.notification) {
				$i('#not-emails').val(data.notification.email);
				$i('#not-when-success').prop('checked',data.notification.whenprocess);
				$i('#not-when-warning').prop('checked',data.notification.whenwarnings);
				$i('#not-when-error').prop('checked',data.notification.whenerrors);
				$i('#not-when-change').prop('checked',data.notification.whenchanged);
				$i('#not-when-reject').prop('checked',data.notification.whenrejected);
				$i('#not-add-information').prop('checked',data.notification.addbasictaskinfo);
				$i('#not-add-file').prop('checked',data.notification.addsourcefile);
			}
			// $("#source option[value="+ data.idsource +"]").attr('selected',true);
			$i('#enabled').prop('checked',data.enabled);
			source = $i('#source').parent();
			$i('#source').remove();
			source.append('<input type="text" name="source" data-id="source" class="col-xs-12 col-sm-12" required autocomplete="off" value="'+data.sourcename+'" disabled>');


			$actionList = $i('#actions-list').list({
				name : 'actions', //Això serveix per definir el nom de la llista, usat al retornar els valors.
				columns : [{text : $i.i18n('lbl_name'), name : 'actionname'}],
				rowid : 'idaction',
				values : (data.actions?data.actions:new Array()), //Els valors inicials (tants valors com columnes)
				sort 	: true,
				showNewElement : false,
				onclick : function($jqE){
					onActionClick($jqE, data.actions,data.propertiesvalues);
				},
				onElementDelete: function (el) {
					validation = false;
					if(el.idaction){
						$i.promise._DELETE("ai/process/action/"+el.idaction,el,true,false).done(function (rsp) {
							if (rsp.data) {
								sendNotificationSuccess($i.i18n("ai:tlt_notification"),$i.i18n("ai:msg_rule_delete"));
								// $i('#rules-list').attr('data-ischanged',false);
								validation = true;
							}else{
								sendNotificationFail($i.i18n("ai:tlt_notification"),$i.i18n("ai:msg_rule_not_delete"));
							}
						});
					}

					return validation;
					
				}
			});
			
			$i('#newAction').on('click',function(event){
				$modal = $i.modal({
					title : "Nueva accion",
					description : "",	
					show: true,
					width: '800px',
					height : '700px',
					resizable : false,
					execute : function(id,$modal){
						$modal.form();
					},
					success: function(id,$widget,$modal){
						if(!$modal.validate()){
							return false;
						}
					 	option = $('#typeAction').val();
					 	switch(option){
					 		case 'create':
					 			result = saveCreateAction(data.idprocess,data.propertiesvalues,data.actioncreateproperties);
					 		break;
					 		case 'confirmEmail':
					 			result = saveConfirmMail(data.idprocess);
					 		break;
					 		case 'sendMail':
					 			result = saveActionSendEmail(data.idprocess);
					 		break;
					 		default:
					 			result = saveNewActionAccept(data.idprocess);
					 		break;
					 	}

					 	if (result.validation) {
					 		$actionList.add(result.action);
					 	}

						return result.validation;
					}
				});
				
				$i.require(['content/ai/actions/action.html'],$modal.getModelContent()).done(function (event) {
					if(data.actions){
						$.each(data.actions,function(){
							if(this.actiontype == "accept"){
								$('option[value="accept"]').remove();
							}
							if(this.actiontype == "create"){
								$('option[value="create"]').remove();
							}
							if(this.actiontype == "confirmEmail"){
								$('option[value="confirmEmail"]').remove();
							}
							if(this.actiontype == "sendMail"){
								$('option[value="sendMail"]').remove();
							}
						});
					}
					selectTypeAction(data.actioncreateproperties,data.propertiesvalues);

					$('#typeAction').on('change',function () {
						selectTypeAction(data.actioncreateproperties,data.propertiesvalues);					
					});


				});

				event.preventDefault();
			});
			
			$rulesList = $i('#rules-list').list({
				name : 'rules', //Això serveix per definir el nom de la llista, usat al retornar els valors.
				columns : [{text : $i.i18n('lbl_name'), name : 'description'}],
				rowid : 'idrule',
				values : (data.rules?data.rules:new Array()), //Els valors inicials (tants valors com columnes)
				sort 	: true,
				showNewElement : false,
				onclick : function($jqE,el){
					loadRule(el,taskProperties,data.languages);				
				},
				onElementDelete: function(el){
					validation = false;
					if(el.idrule){
						$i.promise._DELETE("ai/process/rule/"+el.idrule,el,true,false).done(function (rsp) {
							if (rsp.data.result) {
								sendNotificationSuccess($i.i18n("ai:tlt_notification"),$i.i18n("ai:msg_rule_delete"));
								// $i('#rules-list').attr('data-ischanged',false);
								validation = true;
							}else{
								sendNotificationFail($i.i18n("ai:tlt_notification"),$i.i18n("ai:msg_rule_not_delete"));
							}
						});
					}

					return validation;
				}
			});
			
			$("#newRule").on("click",function(){
				
				$modal = $i.modal({
					title : $i.i18n("ai:tlt_newRule"),
					description : "",	
					show: true,
					width: '600px',
					height : '500px',
					resizable : false,
					execute : function(id,$modal){
					},
					success: function(id,$widget,$modal){
						if(!$modal.validate()){
							return false;
						}
						result = saveRule(data.idprocess, taskProperties,data.languages);
						if (result.validation) {
							$rulesList.add(result.newRule,true);
						}
						
						return result.validation;
					}
				});

				$i.require(['content/ai/rule.html'],$modal.getModelContent()).done(function (event) {
					$.each(taskProperties,function(){
						$("#property").append('<option value="'+this.id+'">'+$i.i18n('ai:option_'+this.id)+'</option>');
					});

					$("#labelName").html($i.i18n("ai:rule_name"));
					$("#labelWeight").html($i.i18n("ai:rule_weight"));
					$("#labelProperty").html($i.i18n("ai:rule_property"));
					$("#labelOperator").html($i.i18n("ai:rule_operator"));
					$("#labelValue").html($i.i18n("ai:rule_value"));
					var slider = document.getElementById("range");
					var output = document.getElementById("counter");
					output.innerHTML = slider.value;
					slider.oninput = function() {
						output.innerHTML = this.value;
					}

					message = $('#property').parent().find('i');
					message.attr('title',$i.i18n('ai:desc_'+ $('#property').val()));

					$('#divRuleValue').append('<input type="text" name="label" data-id="label" id="ruleValue" class="col-xs-12 col-sm-12" value="" required maxlength="125">');

					$('#property').on('change',function () {
						message = $(this).parent().find('i');
						message.attr('data-original-title',$i.i18n('ai:desc_'+ $(this).val()));
						if ($("#property").val() === 'targetLanguages' || $("#property").val() === 'sourceLanguage') {
							$('#divRuleValue').empty();
							$('#divRuleValue').append('<select multiple class="col-xs-12 col-sm-12 tag-input-style chosen" name="ruleValue" id="ruleValue"></select>');
							$.each(data.languages,function () {
								$('#ruleValue').append('<option value="'+this.idlanguage+'">'+this.languagename+'</option>');
							});
							$("#ruleValue").select2();
						}if($("#property").val() === 'execution_type'){
							$('#divRuleValue').empty();
							$('#divRuleValue').append('<select class="col-xs-12 col-sm-12 tag-input-style chosen" name="ruleValue" id="ruleValue"></select>');
							$('#ruleValue').append('<option value="ON">ON DEMAND</option>');
							$('#ruleValue').append('<option value="IN">IN QUEUE</option>');
						}
						else{
							$('#divRuleValue').empty();
							$('#divRuleValue').append('<input type="text" name="label" data-id="label" id="ruleValue" class="col-xs-12 col-sm-12" value="" required maxlength="125">');
							// $('#ruleValue').val(rule.value);
						}
					});
					$('[data-toggle="tooltip"]').tooltip();
				});
				event.preventDefault();
			});
		}
		page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
	}


	function selectTypeAction(propertyactions, propertiesValues) {
		// console.log(propertiesValues);

		link = null;
		option = $('#typeAction').val();
			switch(option){
				case 'create':
					link = 'content/ai/actions/actionCreate.html'
				break;
				case 'confirmEmail':
					link = 'content/ai/actions/actionConfirmMail.html';
				break;
				case 'sendMail':
					link = 'content/ai/actions/actionEmail.html';
				break;
				default:
					$('#action-content').html('');
				break;
		}
		if (link != null) {
			$i.require([link,"assets/css/select2.css","assets/js/select2.js"],$('#action-content')).done(function () {
				$i('.wysiwyg-editor',$('#action-content')).rte();
				if($('#overwrite')){
					$('#overwrite').on('click',diseableOverWrite);
				}
				if($('#overwrite-fuzzy-relation')){
					$('#overwrite-fuzzy-relation').on('click',enableOverWriteFuzzyRelation);
				}
				
				if($('#overwrite-file-relation')){
					$('#overwrite-file-relation').on('click',enableOverWriteFileRelation);
				}
				
				if(option == 'create'){
					$list = $('#table').list({
						name : 'rules', //Això serveix per definir el nom de la llista, usat al retornar els valors.
						columns : [{icon: 'glyphicon-list-alt',text : $i.i18n('ai:col_property'),name : 'propertyname',source : propertyactions},
				           {text : $i.i18n('ai:col_value'), name: 'propertyvalue', source: propertiesValues.area}],
						// rowid : 'idrule',
						// values : (propertyactions?propertyactions:new Array()), //Els valors inicials (tants valors com columnes)
						sort 	: false,
						showNewElement: true,
						onElementCreate: function (el,$tbody) {

						},
						afterCreateElement: function (el,$tbody) {
							// console.log($tbody);
							$values = collectProperties();
							if($values.length != propertyactions.length){
								$.each($values,function () {
									$option = $tbody.find('tr[data-form-new-element="true"] > td[data-td-column="propertyname"] > select > option[value="'+this.propertyname+'"]');
									$option.remove();
								});

								$option = $tbody.find('tr[data-form-new-element="true"] > td[data-td-column="propertyname"] > select > option[value="'+el.propertyname.value+'"]');
								$option.remove();

								$selectPropertyName = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyname"] > select');
								selectPropertyValue = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select').parent();
								selectPropertyValue.html(createChosen(propertiesValues[$selectPropertyName.val().toLowerCase()].allowedValues),false);

								$selectPropertyName.select();
								selectPropertyValue.find('select').select();

								$tbody.find('tr[data-form-new-element="true"] > td[data-td-column="propertyname"] > select').on('change',function (event) {
									$('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select').parent().html(createChosen(propertiesValues[$(this).val().toLowerCase()].allowedValues));
									$select = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select');
									$select.select();
								});
							}else{
								$tbody.find('tr[data-form-new-element="true"]').attr("style",'display: none;');
							}
						},
						onElementSave: function (el,$ui) {
							$select = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select');
							el.propertyvalue.value = $select.val();
							$option = $select.find('option[value="'+$select.val()+'"]');
							el.propertyvalue.text = $option.html();
							// console.log(el);
							return el;
						},
						onElementDelete: function (el,$ui) {
						$values = collectProperties();

						if($values.length == propertyactions.length){
							// console.log("entre");
							$('#table').find('table > tbody > tr[data-form-new-element="true"]').removeAttr("style");


							$.each($values,function () {
								$option = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyname"] > select > option[value="'+this.propertyname+'"]');
								$option.remove();
							});

							$selectPropertyName = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyname"] > select');
							selectPropertyValue = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select').parent();
							selectPropertyValue.html(createChosen(propertiesValues[(el.propertyname.value?el.propertyname.value:el.propertyname).toLowerCase()],false));

							$selectPropertyName.select();
							selectPropertyValue.find('select').select();

							$('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyname"] > select').on('change',function (event) {
								$('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select').parent().html(createChosen(propertiesValues[$(this).val().toLowerCase()].allowedValues,false));
								$select = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select');
								$select.select();
							});
						}
						// console.log(el);
						if(el.propertyname.value){
							value = el.propertyname.value;
						}else{
							value = el.propertyname;
						}
						$select = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyname"] > select');
						$select.append('<option value="'+value+'" selected>'+value+'</option>');
						
						
					}
					});
					$selectPropertyName = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyname"] > select');

					aditionalproperties = [];
					$values = collectProperties();
					validation = true;
					$.each(propertyactions,function () {
						validation = true;
						for (var i = 0; i < $values.length; i++) {
							if($values[i].propertyname == this.id){
								validation = false;
								break;
							}
						}
						// console.log(validation);
						if(validation){
							aditionalproperties.push(this);
						}
					});
					$selectPropertyName.html(createChosen(aditionalproperties));
					selectPropertyValue = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select').parent();
					selectPropertyValue.html(createChosen(propertiesValues[$selectPropertyName.val().toLowerCase()].allowedValues));
					$selectPropertyName.on('change',function (event) {
						$('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select').parent().html(createChosen(propertiesValues[$(this).val().toLowerCase()].allowedValues,false));
						$select = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select');
						$select.select();
					});

					$selectPropertyName.select();
					selectPropertyValue.find('select').select();
				}
				
				
			});
			
		}
	}

	function onActionClick($jqE, processActions,propertiesvalues){
		var renderFormFunction = null;
		
		var idaction = $jqE.attr('data-rowid');
		
		var currentAction = null;
		processActions.forEach(function(action){
			if(action.idaction == idaction){
				currentAction = action;
				return true;
			}
		});
		
		loadAction(currentAction,propertiesvalues);
	}
	
	function loadAction(action,propertiesvalues){
		switch (action.actiontype) {
			case 'accept':
				loadViewActionAccept(action,$i.i18n('ai:tlt_action_accept'));
				break;
			case 'create':
				loadViewActionCreate(action,$i.i18n('ai:tlt_action_create'),propertiesvalues);
				break;
			case 'confirmEmail':
				loadViewActionConfirmEmail(action,$i.i18n('ai:tlt_action_confirm_email'));
				break;
			case 'sendMail':
				loadViewActionSend(action,$i.i18n('ai:tlt_action_send_email'));
				break;
			default:
				break;
		}
	}
	
	function updateRule(currentRule,taskProperties,languages){
		rule = createObjectRule(currentRule.idprocess);
		rule.idrule = currentRule.idrule;
		rule.priority = currentRule.priority;
		$i.promise._PUT("ai/process/rule/"+rule.idrule, rule).done(function (response){
			if(response.data.result){
				fila = $i('#rules-list').find('table > tbody > tr > td#'+rule.idrule);
				fila.html(rule.description);
				fila.off();
				fila.on('click',function(){
					loadRule(rule,taskProperties,languages);
				});
				sendNotificationSuccess($i.i18n('ai:tlt_notification'),$i.i18n('ai:msg_rule_update'));
			}
			else
				sendNotificationFail($i.i18n('ai:tlt_notification'),$i.i18n('ai:msg_not_update'));
		});
	}

	function createObjectRule(idprocess){
		newRule = new Object();
		newRule.idprocess = idprocess;
		newRule.description = $('#ruleName').val();
		newRule.weight = $('#range').val();
		newRule.priority = countRows('#rules-list') + 1;
		newRule.property = $('#property').val();
		newRule.operator = $('#operator').val();
		newRule.value = $('#ruleValue').val();
		if (typeof(newRule.value) === 'object') {
			newRule.value = newRule.value.toString();
		}
		newRule.enabled = $("#enabledRule").is(":checked");
		return newRule;
	}
	
	function saveRule(idprocess, taskProperties,languages){
		result = new Object();
		result.validation = false;
		newRule = createObjectRule(idprocess);

		$i.promise._POST("ai/process/rule", newRule,true,false).done(function (response){
			if(response.data.result){

				newRule.idrule = response.data.result;

				result.validation = true;
				sendNotificationSuccess($i.i18n('ai:tlt_notification'),$i.i18n('ai:msg_rule_save'));
			}
			else
				sendNotificationFail($i.i18n('ai:tlt_notification'),$i.i18n('ai:msg_rule_not_save'));
		});
		result.newRule = newRule;
		return result;
	}
	
	function tableRow(identify,sequence,description){
		return '<tr data-fe-id="1" data-committed="true" data-removed="false" data-rowid="'+identify+'"><td style="cursor: pointer" id="'+identify+'" data-column="description">'+description+'<input type="hidden" nosend="" name="description" value="'+description+'"></td><td data-removeonro="true" style="width:50px!important;"><i class="fa fa-trash press bigger-110" data-id="delete_element"></i></td><td data-removeonro="true" style="width:50px!important;"><input type="radio" name="rules_sort" value="'+sequence+'" nosend=""></td></tr>';
	}
	
	function removedTypeAction(){
		$('#lblTypeAction').remove();
		$('#divTypeAction').remove();
	}

	function loadViewActionAccept(action,title){
		$modal = $i.modal({
			title : $i.i18n("ai:tlt_editAction")+": "+ title,
			description : '',
			show: true,
			width: '600px',
			height : '500px',
			resizable : false,
			execute : function(id,$modal){
			},
			success: function(id,$widget,$modal){
				if(!$modal.validate())
					return false;
				return updateActionAccept(action);
			}
		});
		$i.require(["content/ai/actions/action.html"],$modal.getModelContent()).done(function () {
			removedTypeAction();
			$('#actionName').attr('value',action.actionname);
			$('#onfallback').attr('checked',action.onfallback);
			$('#enabledAction').attr('checked',action.enabled);
		});
	}
	
	function loadViewActionConfirmEmail(action,title){

		$modal = $i.modal({
			title : $i.i18n("ai:tlt_editAction")+": "+ title,
			description : '',
			show: true,
			width: '600px',
			height : '500px',
			resizable : false,
			execute : function(id,$modal){
				$modal.form();
			},
			success: function(id,$widget,$modal){
				if(!$modal.validate())
					return false;
				return updateConfirmEmail(action);
			}
		});

		$i.require(["content/ai/actions/action.html"],$modal.getModelContent()).done(function(){
			removedTypeAction();
			$i.require(["content/ai/actions/actionConfirmMail.html"],$('#action-content')).done(function () {
				$('#actionName').val(action.actionname);
				$('#mails').val(action.sendto);
				$('#title').val(action.subject);
				$('#taskInformation').attr("checked",action.addbasictaskinfo);
				$('#attachment').attr("checked",action.addsourcefile);
				$('#onfallback').attr("checked",action.onfallback);
				$('#enabledAction').attr('checked',action.enabled);
			});
		});
		
	}
	
	function loadViewActionCreate(action,title, propertiesValues){
		$modal = $i.modal({
			title : $i.i18n("ai:tlt_editAction")+": "+ title,
			description : '',
			show: true,
			width: '800px',
			height : '700px',
			resizable : false,
			execute : function(id,$modal){
			},
			success: function(id,$widget,$modal){
				if(!$modal.validate())
					return false;
				return updateActionCreate(action, propertiesValues);
				// return false;
			}
		});

		$i.require(["content/ai/actions/action.html"],$modal.getModelContent()).done(function(){
			removedTypeAction();
			$i.require(["content/ai/actions/actionCreate.html","assets/css/select2.css","assets/js/select2.js"],$('#action-content')).done(function () {
				// $('#actionDescription').attr('class','wysiwyg-editor');
				$('#actionName').val(action.actionname);
				$('#titleTask').val(action.nametask);
				$('#overwrite').attr('checked',action.enableoverwrite);
				$('#addIceWc').attr('checked',action.addicewc);
				if(action.maxwords != null)
					$('#valueOverwrite').val(action.maxwords);
				if(action.enableoverwrite){
					$('#P100').val(action.overwrite.percentage_100);
					$('#MIN').val(action.overwrite.minute);
					$('#ice').val(action.overwrite.percentage_101);
					$('#rep').val(action.overwrite.repetition);
					$('#P95').val(action.overwrite.percentage_95);
					$('#P85').val(action.overwrite.percentage_85);
					$('#P75').val(action.overwrite.percentage_75);
					$('#P50').val(action.overwrite.percentage_50);
					$('#NM').val(action.overwrite.notmatch);
				}
				$('#overwrite').attr('checked',action.enablefuzzyrelation);//[][]

				duedate = action.duedate;
				if (duedate.indexOf('PT') >= 0) {
					duedate = duedate.replace(/PT/,"");
					if (duedate.indexOf('-') >= 0) {
						number = duedate.replace(/-/,"");
						number = number.replace(/H/,"");
						$('#duedate').val(number);
						$('#operator').val("-");
						$('#typeDueDate').val("H");
					}else{
						number = duedate.replace(/[+]/,"");
						number = number.replace(/H/,"");
						$('#duedate').val(number);
						$('#operator').val("+");
						$('#typeDueDate').val("H");
					}
				}else{
					if (duedate.indexOf('D') >= 0) {
						duedate = duedate.replace(/P/,"");
						duedate = duedate.replace(/D/,"");
						if (duedate.indexOf('-') >= 0) {
							number = duedate.replace(/-/,"");
							$('#duedate').val(number);
							$('#operator').val("-");
							$('#typeDueDate').val("D");
						}else{
							number = duedate.replace(/[+]/,"");
							$('#duedate').val(number);
							$('#operator').val("+");
							$('#typeDueDate').val("D");
						}
					}else{
						if (duedate.indexOf('-') >= 0) {
							number = duedate.replace(/-/,"");
							$('#duedate').val(number);
							$('#operator').val("-");
							$('#typeDueDate').val("P");
						}else{
							number = duedate.replace(/[+]/,"");
							$('#duedate').val(number);
							$('#operator').val("+");
							$('#typeDueDate').val("P");
						}
					}
				}
				$list = $('#table').list({
					name : 'properties', //Això serveix per definir el nom de la llista, usat al retornar els valors.
					columns : [{icon: 'glyphicon-list-alt',text : $i.i18n('ai:col_property'),name : 'propertyname',source : action.aditionalproperties},
			           {text : $i.i18n('ai:col_value'), name: 'propertyvalue',source: propertiesValues.area}],
					rowid : 'idproperty',
					values : (action.propertyactions?action.propertyactions:new Array()), //Els valors inicials (tants valors com columnes)
					sort 	: false,
					showNewElement: true,
					onElementCreate: function (el,$tbody) {

					},
					afterCreateElement: function (el,$tbody) {
						// console.log($tbody.find('tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select'));
						$values = collectProperties();
						if($values.length != action.aditionalproperties.length){
							$.each($values,function () {
								$option = $tbody.find('tr[data-form-new-element="true"] > td[data-td-column="propertyname"] > select > option[value="'+this.propertyname+'"]');
								$option.remove();
							});

							$option = $tbody.find('tr[data-form-new-element="true"] > td[data-td-column="propertyname"] > select > option[value="'+el.propertyname.value+'"]');
							$option.remove();

							$selectPropertyName = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyname"] > select');
							selectPropertyValue = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select').parent();
							selectPropertyValue.html(createChosen(propertiesValues[$selectPropertyName.val().toLowerCase()].allowedValues,false));

							$selectPropertyName.select();
							selectPropertyValue.find('select').select();

							$tbody.find('tr[data-form-new-element="true"] > td[data-td-column="propertyname"] > select').on('change',function (event) {
								$('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select').parent().html(createChosen(propertiesValues[$(this).val().toLowerCase()].allowedValues,false));
								$select = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select');
								$select.select();
							});
						}else{
							$tbody.find('tr[data-form-new-element="true"]').attr("style",'display: none;');
						}
							
					},
					onElementSave: function (el,$ui) {
						$select = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select');
						el.propertyvalue.value = $select.val();
						$option = $select.find('option[value="'+$select.val()+'"]');
						el.propertyvalue.text = $option.html();
						// console.log(el);
						return el;
					},
					onElementDelete: function (el,$ui) {
						$values = collectProperties();

						if($values.length == action.aditionalproperties.length){
							// console.log("entre");
							$('#table').find('table > tbody > tr[data-form-new-element="true"]').removeAttr("style");


							$.each($values,function () {
								$option = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyname"] > select > option[value="'+this.propertyname+'"]');
								$option.remove();
							});

							$selectPropertyName = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyname"] > select');
							selectPropertyValue = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select').parent();
							selectPropertyValue.html(createChosen(propertiesValues[(el.propertyname.value?el.propertyname.value:el.propertyname).toLowerCase()].allowedValues,false));

							$selectPropertyName.select();
							selectPropertyValue.find('select').select();

							$('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyname"] > select').on('change',function (event) {
								$('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select').parent().html(createChosen(propertiesValues[$(this).val().toLowerCase()].allowedValues,false));
								$select = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select');
								$select.select();
							});
						}
						// console.log(el);
						if(el.propertyname.value){
							value = el.propertyname.value;
						}else{
							value = el.propertyname;
						}
						$select = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyname"] > select');
						$select.append('<option value="'+value+'" selected>'+value+'</option>');
						
						
					}
				});
				$selectPropertyName = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyname"] > select');

				aditionalproperties = [];
				$values = collectProperties();
				validation = true;
				$.each(action.aditionalproperties,function () {
					validation = true;
					for (var i = 0; i < $values.length; i++) {
						if($values[i].propertyname == this.id){
							validation = false;
							break;
						}
					}
					// console.log(validation);
					if(validation){
						aditionalproperties.push(this);
					}
				});
				$selectPropertyName.html(createChosen(aditionalproperties));
				selectPropertyValue = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select').parent();
				// console.log(propertiesValues[$selectPropertyName.val().toLowerCase()]);
				selectPropertyValue.html(createChosen(propertiesValues[$selectPropertyName.val().toLowerCase()].allowedValues,false));
				$selectPropertyName.on('change',function (event) {
					$('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select').parent().html(createChosen(propertiesValues[$(this).val().toLowerCase()].allowedValues,false));
					$select = $('#table').find('table > tbody > tr[data-form-new-element="true"] > td[data-td-column="propertyvalue"] > select');
					$select.select();
				});

				$selectPropertyName.select();
				selectPropertyValue.find('select').select();
				$('#overwrite-fuzzy-relation').on('click',enableOverWriteFuzzyRelation);
				$('#overwrite-file-relation').on('click',enableOverWriteFileRelation);
				if(action.fuzzyoverwrite!=null || action.overwritefuzzy!=null){
					$('#overwrite-fuzzy-relation').prop('checked', true);
					action.fuzzyoverwrite?loadFuzzyRelation(action.fuzzyoverwrite):loadFuzzyRelation(action.overwritefuzzy);
					$('#fuzzy-relation-container').attr('class','container-test accordion-fuzzy-open');
				}
				if(action.fileoverwrite!=null || action.overwritefile!=null){
					$('#overwrite-file-relation').prop('checked', true);
					action.fileoverwrite?loadFileRelation(action.fileoverwrite):loadFileRelation(action.overwritefile);
					$('#file-relation-container').attr('class','container-test accordion-file-relation-open');
					$('#compress_source_files').prop('checked',action.fileoverwrite.compress_source_files );
					$('#compress_reference_files').prop('checked',action.fileoverwrite.compress_reference_files );
					
				}
				if(action.enableoverwrite)
					diseableOverWrite();
				
				$('#appendWCProofForecast').attr('checked',(action.appendwcproofforecast == 1)?true:false);
				$('#overwrite').on('click',diseableOverWrite);
				$("#translation option[value="+ (action.appendmachinetransto != null? action.appendmachinetransto : '---') +"]").attr("selected",true);
				$('#actionDescription').html(action.description);
				$('#onfallback').attr("checked",action.onfallback);
				$('#enabledAction').attr('checked',action.enabled);
				$('#referenceFile').attr("checked",action.uploadoriginalmessageasreferencefile);
				$('.wysiwyg-editor').rte();	
			});
		});

		
		
	}

	function createChosen(options,translation = true) {
		html = '<select name="propertyname" data-id="propertyname">';
		$.each(options,function () {
			if (translation) {
				html += '<option value="'+this.id+'">'+$i.i18n('ai:'+this.id)+'</option>';
			}else{
				html += '<option value="'+this.id+'">'+this.name+'</option>';
			}
			
		});
		html += '</select>';
		return html;
	}

	function loadViewActionSend(action,title) {
		$modal = $i.modal({
			title : $i.i18n("ai:tlt_editAction")+": "+ title,
			description : '',
			show: true,
			width: '800px',
			height : '700px',
			resizable : false,
			execute : function(id,$modal){
			},
			success: function(id,$widget,$modal){
				if(!$modal.validate())
					return false;
				return updateActionSendEmail(action);
			}
		});

		$i.require(["content/ai/actions/action.html"],$modal.getModelContent()).done(function(){
			removedTypeAction();
			$i.require(["content/ai/actions/actionEmail.html"],$('#action-content')).done(function () {
				$('#contentEmail').val(action.content);
				$('#actionName').val(action.actionname);
				$('#enabledAction').attr('checked',action.enabled);
				$('#onfallback').attr('checked',action.onfallback);
				$('#subjectEmail').val(action.subject);
				$('#senders').val(action.sendto);
				$('#taskInformation').attr("checked",action.addbasictaskinfo);
				$('#attachment').attr("checked",action.addsourcefile);
				$('#enabledAction').attr('checked',action.enabled);
				$('.wysiwyg-editor').rte();
			});
		});
	}
	
	function diseableOverWrite(){
		$('#valueOverwrite').attr('disabled')?$('#valueOverwrite').removeAttr('disabled'):$('#valueOverwrite').attr('disabled',true);
		$('#valueOverwrite').attr('disabled')?$('#wordCountConteiner').removeClass('accordion-open'):$('#wordCountConteiner').attr('class','container-test accordion-open');
	}

	
	/**
	 * Enable the fuzzy relation overwrite
	 */
	function enableOverWriteFileRelation(){
		if( $('#overwrite-file-relation').is(':checked') ) {
			$('#file-relation-container').attr('class','container-test accordion-file-relation-open')
			//fillSelectorFuzzyRelation()
			$i.promise._GET('ai/process/filecategories').done(fillSelectorFileRelation);
		}else{
			$('#file-relation-container').removeClass('accordion-file-relation-open');
		}
	}

	/**
	 * Fill the selectors with the TMS fuzzies
	 */
	function fillSelectorFileRelation(file_categories){
		values_file_categories_TMS = ['ignorar'];
		values_file_categories_TMS = values_file_categories_TMS.concat(file_categories.data);
		count_select = 1
		$("#file-relation-container").find("select").each(function(){
			select_fuzzy = $(this);
			select_fuzzy.empty();
			count_value = 0;
			values_file_categories_TMS.forEach(function(element){
				if(count_select == count_value){
					select_fuzzy.append('<option selected value="'+element+'">'+element+'</option>');
				}else{
					select_fuzzy.append('<option value="'+element+'">'+element+'</option>');
				}
			count_value++;
			});
			count_select++;
		});
	}

	/**
	 * Load the selected elements from the fuzzy relations
	 */
	function loadFileRelation(elementosSeleccionados){
		$i.promise._GET('ai/process/filecategories').then(fillSelectorFileRelation).done(function(){
		keys_elementosSeleccionados = Object.keys(elementosSeleccionados)
		keys_elementosSeleccionados.forEach(function(key){
			$("#"+key).append("<option value='"+elementosSeleccionados[key]+"' selected disabled hidden>"+elementosSeleccionados[key]+"</option>")
		});
		});
	}

	/**
	 * Enable the fuzzy relation overwrite
	 */
	function enableOverWriteFuzzyRelation(){
		if( $('#overwrite-fuzzy-relation').is(':checked') ) {
			$('#fuzzy-relation-container').attr('class','container-test accordion-fuzzy-open')
			//fillSelectorFuzzyRelation()
			$i.promise._GET('ai/process/fuzzies').done(fillSelectorFuzzyRelation);
		}else{
			$('#fuzzy-relation-container').removeClass('accordion-fuzzy-open');
		}
	}

	/**
	 * Fill the selectors with the TMS fuzzies
	 */
	function fillSelectorFuzzyRelation(fuzzies){
		values_count_TMS = ['ignorar'];
		values_count_TMS = values_count_TMS.concat(fuzzies.data);
		count_select = 1
		$("#fuzzy-relation-container").find("select").each(function(){
			select_fuzzy = $(this);
			select_fuzzy.empty();
			count_value = 0;
			values_count_TMS.forEach(function(element){
				if(count_select == count_value){
					select_fuzzy.append('<option selected value="'+element+'">'+element+'</option>');
				}else{
					select_fuzzy.append('<option value="'+element+'">'+element+'</option>');
				}
			count_value++;
			});
			count_select++;
		});
	}

	/**
	 * Load the selected elements from the fuzzy relations
	 */
	function loadFuzzyRelation(elementosSeleccionados){
		console.log("Elementos seleccionados",elementosSeleccionados);
		$i.promise._GET('ai/process/fuzzies').then(fillSelectorFuzzyRelation).done(function(){
		keys_elementosSeleccionados = Object.keys(elementosSeleccionados)
		keys_elementosSeleccionados.forEach(function(key){
			$("#"+key).append("<option value='"+elementosSeleccionados[key]+"' selected disabled hidden>"+elementosSeleccionados[key]+"</option>")
		});
		});
	}


	function validateSelectorFuzzy(){
		selected_fuzzys = [];
		$("#fuzzy-relation-container").find("select").each(function(){
			selected_fuzzys.push($(this).find("option:selected").val());
		});
		$("#fuzzy-relation-container").find("select").each(function(){
			select_fuzzy = $(this).find("option:selected");
			$(this).find("option").each(function(){
				option_fuzzy = $(this)
				if(selected_fuzzys.includes(option_fuzzy.val())){
				    option_fuzzy.attr("disabled",true); 
				}else{
					option_fuzzy.attr("disabled",false); 
				}
				// option_fuzzy = $(this)
				// if(option_fuzzy.val() == optionSelected.val()){
				// 	option_fuzzy.attr("disabled",true);
				// }else if(option_fuzzy.attr("disabled",true)){
				// 	option_fuzzy.attr("disabled",false);
				// }
			});
			
		});

	}
	
	
	function saveNewActionAccept(idProcess){
		result = new Object();
		result.validation = false;
		newAction = createObjectActionAccept(idProcess);
		$i.promise._POST("ai/process/action", newAction).done(function (response){
			if(response.data.result){
				result.validation = true;
				result.action = newAction;
				sendNotificationSuccess($i.i18n('ai:tlt_notification'),$i.i18n('ai:msg_action_save'));
			}
			else
				sendNotificationFail($i.i18n('ai:tlt_notification'),$i.i18n('ai:msg_action_not_save'));
		});
		return result;	
	}
	
	function updateActionAccept(currentAction){
		action = createObjectActionAccept(currentAction.idprocess);
		action.sequence = currentAction.sequence;
		$i.promise._PUT("ai/process/action/"+action.idaction, action).done(function (response){
			if(response.data.result){
				chanceActionTable(action);
				sendNotificationSuccess($i.i18n('ai:tlt_notification'),$i.i18n('ai:msg_action_update'));
			}
			else
				sendNotificationFail($i.i18n('ai:tlt_notification'),$i.i18n('ai:msg_not_update'));
		});
	}
	
	function createObjectActionAccept(idProcess){
		newAction = new Object();
		newAction.actionname = $('#actionName').val();
		newAction.actiontype = "accept";
		newAction.enabled = $('#enabledAction').is(':checked');
		newAction.idprocess = idProcess;
		newAction.onfallback = $('#onfallback').is(':checked');
		newAction.idaction = 3;
		newAction.sequence = countRows('#actions-list') + 1;
		return newAction;
	}

	function createObjectActionSend(idProcess) {
	 	newAction = new Object();
	 	newAction.actionname = $('#actionName').val();
	 	newAction.actiontype = "sendMail";
	 	newAction.enabled = $('#enabledAction').is(':checked');
	 	newAction.idprocess = idProcess;
	 	newAction.onfallback = $('#onfallback').is(':checked');
	 	newAction.idaction = 4;
	 	newAction.sequence = countRows('#actions-list') + 1;
	 	newAction.sendto = $('#senders').val();
	 	newAction.emailcontent = $('#contentEmail').html();
	 	newAction.subject = $('#subjectEmail').val();
	 	newAction.addbasictaskinfo = $('#taskInformation').is(':checked');
		newAction.addsourcefile = $('#attachment').is(':checked');
	 	return newAction;
	}

	function saveActionSendEmail(idProcess) {
		result = new Object();
		result.validation = false;
		newAction = createObjectActionSend(idProcess);

		if(!validateEmails(newAction.sendto)){
			parent = $('#senders').parent();
			parent.attr('class','form-group col-md-12 has-error');
			parent.append('<div data-type="form-error-field" data-e-name="mails" class="help-block">'+$i.i18n('ai:msg_error_mails')+'</div>');
			return false;
		}

		$i.promise._POST("ai/process/action", newAction).done(function (response){
			if(response.data.result){
				newAction.idemailaction = response.data.result;
				result.validation = false;
				result.action = newAction;
				sendNotificationSuccess($i.i18n('ai:tlt_notification'),$i.i18n('ai:msg_action_save'));
			}
			else
				sendNotificationFail($i.i18n('ai:tlt_notification'),$i.i18n('ai:msg_action_not_save'));
			
		});

		return result;
	}

	function updateActionSendEmail(action){
		newAction = createObjectActionSend(action.idprocess);
		newAction.sequence = action.sequence;
		newAction.emailcontent = $('#contentEmail').html();
		if(!validateEmails(newAction.sendto)){
			parent = $('#senders').parent();
			parent.attr('class','form-group col-md-12 has-error');
			parent.append('<div data-type="form-error-field" data-e-name="mails" class="help-block">'+$i.i18n('ai:msg_error_mails')+'</div>');
			return false;
		}
		$i.promise._PUT("ai/process/action/"+action.idaction, newAction).done(function (response){
			if(response.data.result){
				chanceActionTable(newAction);
				sendNotificationSuccess($i.i18n('ai:tlt_notification'),$i.i18n('ai:msg_action_update'));
			}
			else
				sendNotificationFail($i.i18n('ai:tlt_notification'),$i.i18n('ai:msg_not_update'));
		});
	}

	function saveCreateAction(idProcess,values,properties){
		result = new Object();
		result.validation = false;
		newAction = createObjectCreateAction(idProcess);
		newAction.description = $('#actionDescription').html();
		// console.log(collectAditionalPropertiesCreateAction());
		// return false;
		$i.promise._POST("ai/process/action", newAction,true,false).done(function (response){
			if(response.data.result){
				result.action = response.data.action;
				result.validation = true;
				sendNotificationSuccess($i.i18n('ai:tlt_notification'),$i.i18n('ai:msg_action_save'));
			}
			else
				sendNotificationFail($i.i18n('ai:tlt_notification'),$i.i18n('ai:msg_action_not_save'));
			
		});

		return result;
	}
	//[][]
	function createObjectCreateAction(idProcess){

		newAction = new Object();
		newAction.actionname = $('#actionName').val();
		newAction.actiontype = 'create';
		newAction.enabled = $('#enabledAction').is(':checked');
		newAction.idprocess = idProcess;
		newAction.idaction = 1;

		switch($('#typeDueDate').val()){
			case 'P':
				newAction.duedate = $('#operator').val()+''+$('#duedate').val();
			break;
			case 'H':
				newAction.duedate = $('#operator').val()+'PT'+$('#duedate').val()+'H';
			break;
			case 'D':
				newAction.duedate = $('#operator').val()+'P'+$('#duedate').val()+'D';
			break;
		}
		newAction.addicewc = $('#addIceWc').is(':checked');
		newAction.nametask = $('#titleTask').val();
		newAction.maxwords = $('#valueOverwrite').val();
		newAction.enableoverwrite = $('#overwrite').is(':checked');
		newAction.enablefuzzyrelation = $('#overwrite-fuzzy-relation').is(':checked');
		newAction.enablefilerelation = $('#overwrite-file-relation').is(':checked');
		newAction.enablefilecompress = $('#enable-file-compress').is(':checked');
		if(newAction.enableoverwrite){
			overwrite = new Object();
			overwrite.machinetranslation = 0;
			overwrite.minute = $('#MIN').val();
			overwrite.notmatch = $('#NM').val();
			overwrite.percentage_50 = $('#P50').val();
			overwrite.percentage_75 = $('#P75').val();
			overwrite.percentage_85 = $('#P85').val();
			overwrite.percentage_95 = $('#P95').val();
			overwrite.percentage_100 = $('#P100').val();
			overwrite.percentage_101 = $('#ice').val();
			overwrite.repetition = $('#rep').val();
			overwrite.weightedwords = 0;
			
			newAction.overwrite = overwrite;
		}
		if(newAction.enablefuzzyrelation){
			overwriteFuzzy = new Object();
			overwriteFuzzy.min = $('#minute').find(":selected").val();
			overwriteFuzzy.nm = $('#notmatch').find(":selected").val();
			overwriteFuzzy.mt = $('#machinetranslation').find(":selected").val();
			overwriteFuzzy.percentage_50 = $('#percentage_50').find(":selected").val();
			overwriteFuzzy.percentage_75 = $('#percentage_75').find(":selected").val();
			overwriteFuzzy.percentage_85 = $('#percentage_85').find(":selected").val();
			overwriteFuzzy.percentage_95 = $('#percentage_95').find(":selected").val();
			overwriteFuzzy.percentage_100 = $('#percentage_100').find(":selected").val();
			overwriteFuzzy.percentage_101 = $('#percentage_101').find(":selected").val();
			overwriteFuzzy.repetition = $('#repetition').find(":selected").val();
			newAction.overwriteFuzzy = overwriteFuzzy;
		}
		if(newAction.enablefilerelation){
			overwriteFile = new Object();
			overwriteFile.source_files = $('#source_files').find(":selected").val();
			overwriteFile.reference_files = $('#reference_files').find(":selected").val();
			overwriteFile.tm_files = $('#tm_files').find(":selected").val();
			newAction.overwriteFile = overwriteFile;
				compressFiles = new Object();
				compressFiles.compress_source_files = $('#compress_source_files').is(':checked');
				compressFiles.compress_reference_files = $('#compress_reference_files').is(':checked');
				newAction.compressFiles = compressFiles;
		}

		newAction.onfallback = $('#onfallback').is(':checked');
		if ($('#translation').val() != '---') {
			newAction.appendmachinetransto = $('#translation').val();
		}

		newAction.appendwcproofforecast = $('#appendWCProofForecast').is(':checked');
		
		newAction.sequence = countRows('#actions-list') + 1;
		newAction.uploadoriginalmessageasreferencefile = $('#referenceFile').is(':checked');
		newAction.description = $('#actionDescription').val();
		newAction.propertyactions = collectAditionalPropertiesCreateAction();
		newAction.propertiesDelete = collectAditionalPropertiesCreateActionDeleted();
		return newAction;
	}

	function collectProperties() {
		properties = [];
		rows = $('#table').find('tbody > tr[data-removed="false"]');

		$.each(rows,function () {
			property = new Object();
			property.propertyname = $(this).find('td[data-column="propertyname"] > input[type="hidden"]').val();
			property.propertyvalue = $(this).find('td[data-column="propertyvalue"] > input[type="hidden"]').val();
			property.propertytype = 'M';
			properties.push(property);
		});

		// properties.pop();
		return properties;
	}

	function collectAditionalPropertiesCreateAction(){
		properties = [];
		rows = $('#table').find('tbody > tr[data-committed="false"]');
		rows = rows.filter('[data-removed="false"]');

		$.each(rows,function () {
				property = new Object();
				property.propertyname = $(this).find('td[data-column="propertyname"] > input').val();
				property.propertyvalue = $(this).find('td[data-column="propertyvalue"] > input').val();
				property.propertytype = 'M';
				properties.push(property);
			
		});

		// properties.pop();
		return properties;
	}

	function collectAditionalPropertiesCreateActionDeleted(){
		propertiesDelete = [];
		rows = $('#table').find('tbody > tr[data-removed="true"]');
		$.each(rows,function () {
			property = new Object();
			property.id = $(this).attr('data-rowid');
			propertiesDelete.push(property);
		});

		return propertiesDelete;
	}

	function updateActionCreate(action, values){
		validation = false;
		actionCreate = createObjectCreateAction(action.idprocess);
		actionCreate.sequence = action.sequence;
		actionCreate.idcreateaction = action.idcreate?action.idcreate:action.idcreateaction;
		actionCreate.description = $('#actionDescription').html();
		// console.log(actionCreate);
		// return false;
		$i.promise._PUT("ai/process/action/"+action.idaction, actionCreate).done(function (response){
			if(response.data.result){
				actionCreate = response.data.action;
				validation = true;
				chanceActionTable(actionCreate,values);
				sendNotificationSuccess($i.i18n('ai:tlt_notification'),$i.i18n('ai:msg_action_update'));
			}
			else
				alert('Can not save rule');
		});

		return validation;
	}
	
	function updateConfirmEmail(action){
		newAction = createObjectConfirmMail(action.idprocess);
		newAction.sequence = action.sequence;
		if(!validateEmails(newAction.sendto)){
			parent = $('#mails').parent();
			parent.attr('class','form-group col-md-12 has-error');
			parent.append('<div data-type="form-error-field" data-e-name="mails" class="help-block">'+$i.i18n('ai:msg_error_mails')+'</div>');
			return false;
		}
		$i.promise._PUT("ai/process/action/"+action.idaction, newAction).done(function (response){
			if(response.data.result){
				chanceActionTable(newAction);
				sendNotificationSuccess($i.i18n('ai:tlt_notification'),$i.i18n('ai:msg_action_update'));
			}
			else
				alert('Can not update action');
		});
	}
	
	function saveConfirmMail(idProcess){
		result = new Object();
		result.validation = false;
		newAction = createObjectConfirmMail(idProcess);
		if(!validateEmails(newAction.sendto)){
			parent = $('#mails').parent();
			parent.attr('class','form-group col-md-12 has-error');
			parent.append('<div data-type="form-error-field" data-e-name="mails" class="help-block">'+$i.i18n('ai:msg_error_mails')+'</div>');
			return false;
		}
		
		$i.promise._POST("ai/process/action", newAction).done(function (response){
			if(response.data.result){
				newAction.idconfirmemail = response.data.result;
				result.validation = true;
				result.action = newAction;
				sendNotificationSuccess($i.i18n('ai:tlt_notification'),$i.i18n('ai:msg_action_save'));
			}else
				sendNotificationFail($i.i18n('ai:tlt_notification'),$i.i18n('ai:msg_action_not_save'));
		});
		
		return result;
	}
	
	function updateActionTable(action,properties,values){
		$i('#actions-list').find('table > tbody').append(tableRow(action.identify,action.sequence,action.actionname));
		$i('#actions-list').find('table > tbody > tr > td#'+action.identify).on('click',function(){
			loadAction(action,values);
		});
	}
	
	function chanceActionTable(action,values){
		fila = $i('#actions-list').find('table > tbody > tr > td#'+action.idaction);
		fila.html(action.actionname);
		fila.off();
		fila.on('click',function(){
			loadAction(action,values);
		});
	}
	
	function createObjectConfirmMail(idProcess){
		newAction = new Object();
		newAction.actionname = $('#actionName').val();
		newAction.actiontype = 'confirmEmail';
		newAction.enabled = $('#enabledAction').is(':checked');
		newAction.idaction = 2;
		newAction.sequence = countRows('#actions-list') + 1;
		newAction.idprocess = idProcess;
		newAction.onfallback = $('#onfallback').is(':checked');;
		newAction.sendto = $('#mails').val();
		newAction.addbasictaskinfo = $('#taskInformation').is(':checked');
		newAction.addsourcefile = $('#attachment').is(':checked');
		newAction.subject = $('#title').val();
		return newAction;
	}
	
	function validateEmails(mails){
		validation = false;
		emails = mails.split(',');
		emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
		for(i = 0; i < emails.length; i++){
			validation = emailRegex.test(emails[i]);
			if(!validation){
				break;
			}
		}
		return validation;
	}
	
	function countRows(idTable){
		return $(idTable).find('table > tbody > tr').length;
	}
	
	function onRuleClick($jqE, processRules, taskProperties,languages){
		// var idrule = $jqE.attr('data-rowid');
		
		// var rule = null;
		// processRules.forEach(function(currentRule){
		// 	if(currentRule.idrule == idrule){
		// 		rule = currentRule;
		// 		return true;
		// 	}
		// });
		loadRule(rule,taskProperties,languages);
	}

	function validateAction() {
		return validateEmails($('#mails').val()?$('#mails').val():'');
	}

	function sendNotificationSuccess(title,message){
		$i.gritter({
			type: "success", //Mandatory
			title: title,
			description: message,
			Sticky: false
		});
	}

	function sendNotificationFail(title, message){
		$i.gritter({
			type: "warning", //Mandatory
			title: title,
			description: message,
			Sticky: false
		});
	}

	function loadRule(rule,taskProperties,languages){
		
		$modal = $i.modal({
			title : $i.i18n("ai:tlt_edit_rule"),
			description : "",	
			show: true,
			width: '600px',
			height : '500px',
			resizable : false,
			execute : function(id,$modal){
				$modal.form();
			},
			success: function(id,$widget,$modal){
				if(!$modal.validate()){
					return false;
				}
				return updateRule(rule,taskProperties,languages);
			}
		});
		$i.require(["content/ai/rule.html"],$modal.getModelContent()).done(function () {
			// console.log(rule);
			$("#labelName").html($i.i18n("ai:rule_name"));
			$("#labelWeight").html($i.i18n("ai:rule_weight"));
			$("#labelProperty").html($i.i18n("ai:rule_property"));
			$("#labelOperator").html($i.i18n("ai:rule_operator"));
			$("#labelValue").html($i.i18n("ai:rule_value"));
			
			$("#ruleName").attr("value",(rule? rule.description:''));
			$("#range").attr("value",(rule? rule.weight:'0'));
			$("#enabledRule").attr("checked",rule.enabled?rule.enabled:false);

			$.each(taskProperties,function(){
				$("#property").append('<option value="'+this.id+'">'+$i.i18n('ai:option_'+this.id)+'</option>');
			});
			$("#operator option[value="+ rule.operator +"]").attr("selected",true);
			$("#property option[value="+ rule.property +"]").attr("selected",true);

			if ($("#property").val() == 'targetLanguages' || $("#property").val() == 'sourceLanguage') {
				$('#divRuleValue').empty();
				$('#divRuleValue').append('<select multiple class="col-xs-12 col-sm-12 tag-input-style chosen" name="ruleValue" id="ruleValue"></select>');
				$.each(languages,function () {
					$('#ruleValue').append('<option value="'+this.idlanguage+'">'+this.languagename+'</option>');
				});
				languagesIds = rule.value.split(',');
				$.each(languagesIds,function () {
					$('#ruleValue').find('option[value="'+this+'"]').attr('selected',true);
				});
				$("#ruleValue").select2();
			}
			if($("#property").val() === 'execution_type'){
				$('#divRuleValue').empty();
				$('#divRuleValue').append('<select class="col-xs-12 col-sm-12 tag-input-style chosen" name="ruleValue" id="ruleValue"></select>');
				if(rule.value == 'ON'){
					$('#ruleValue').append('<option selected value="ON">ON DEMAND</option>');
					$('#ruleValue').append('<option value="IN">IN QUEUE</option>');
				}else{
					$('#ruleValue').append('<option value="ON">ON DEMAND</option>');
					$('#ruleValue').append('<option selected value="IN">IN QUEUE</option>');
				}
			}
			else{
				$('#divRuleValue').empty();
				$('#divRuleValue').append('<input type="text" name="label" data-id="label" id="ruleValue" class="col-xs-12 col-sm-12" value="" required maxlength="125">');
				$('#ruleValue').val(rule.value);
			}

			message = $('#property').parent().find('i');
			message.attr('title',$i.i18n('ai:desc_'+ $('#property').val()));
			$('#property').on('change',function () {
				message = $(this).parent().find('i');
				message.attr('data-original-title',$i.i18n('ai:'+ $(this).val()));
				if ($("#property").val() === 'targetLanguages' || $("#property").val() === 'sourceLanguage') {
					$('#divRuleValue').empty();
					$('#divRuleValue').append('<select multiple class="col-xs-12 col-sm-12 tag-input-style chosen" name="ruleValue" id="ruleValue"></select>');
					$.each(languages,function () {
						$('#ruleValue').append('<option value="'+this.idlanguage+'">'+this.languagename+'</option>');
					});
					$("#ruleValue").select2();
				}else{
					$('#divRuleValue').empty();
					$('#divRuleValue').append('<input type="text" name="label" data-id="label" id="ruleValue" class="col-xs-12 col-sm-12" value="" required maxlength="125">');
					$('#ruleValue').val(rule.value);
				}
			});

			var slider = document.getElementById("range");
			var output = document.getElementById("counter");
			output.innerHTML = slider.value;
			slider.oninput = function() {
				output.innerHTML = this.value;
			}
			
			
			$('[data-toggle="tooltip"]').tooltip();
		});
	}
	
	this.callback = function(){
	}
	
	
});