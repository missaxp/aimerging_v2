(function(page){
	page.config({
		autoshow : false, //Si volem gestionar manualment quan ensenyem la plana.
		object : 'roles', //Objecte de referencia el qual es mostrará algun dels seus fills en aquesta plana.
		type : 'list', //Tipus de pàgina.
	});
	
	this.callback = function(){
	}
	
	page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
});