(function(page){
	page.config({
		autoshow : false, //Si volem gestionar manualment quan ensenyem la plana.
		object : 'log', //Objecte de referencia el qual es mostrará algun dels seus fills en aquesta plana.
		type : 'detail', //Tipus de pàgina.
	});
	
	$i.promise._GET({
		restURL : 'ai/log',
		//bsend : $jqE,
	})
	.done(function(response){
		$i('#ai-log').html(response.data);
		//$i.hash.follow('ai/tasks/' +  usid, usid,{'name':usid,'response' : response});
		page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
	});
	
	
	this.callback = function(){
	}
	
	
});