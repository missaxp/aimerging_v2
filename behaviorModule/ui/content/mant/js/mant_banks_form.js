(function(page){
	page.config({
		autoshow : false,
		object : 'banks',
		api : 'mant/banks',
		resource : $i._GET('bank_id'),
		type : 'form',
		trackChanges : true,
		showBack : true,
		afterSubmit : function(rsp,values){
			$i('#lblbank').html($i('#id').val());
			if(page.newElement){
				$i('#pkgrup').hide();
			}
		}
	});
	
	var _POST = $i._POST();	
	if(page.newElement){
		loadData();
	} else {
		if(_POST!==false && typeof(_POST.response)!=='undefined'){ 
			loadData(_POST.response);
		}
		else{ 		
			$i.promise._request('GET','mant/banks/'+page._get('resource')).done(loadData);
		}
	}

	function loadData(rsp){		
		if (!page.newElement) {
			$i('#id').val(rsp.data.id);
			$i('#name').val(rsp.data.name);
			$i('#bic').val(rsp.data.bic);
			$i('#id').prop( "disabled", true );			
			$i('#lblbank').html(rsp.data.id); 
			$i('#pkgrup').hide();
		} else {
			$i('#id').val("");
			$i('#name').val("");
			$i('#bic').val("");
			$i('#pkgrup').removeClass('hidden');
			$i('#id').uniqueKey({rest : 'mant/banks/exist'});
		}	
		page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
	}
});