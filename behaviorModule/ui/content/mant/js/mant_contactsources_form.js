(function(page){
	page.config({
		autoshow : false,
		object : 'attributes',
		api : 'mant/contactsources',
		resource : $i._GET('contactsource_id'),
		type : 'form',
		trackChanges : true,
		showBack : true,
		afterSubmit : function(rsp,values){
			$i('#lblcontactsources').html($i('#name').val());
		}
	});
	var _POST = $i._POST();	
	if(page.newElement){
		loadData();
	} else {
		if(_POST!==false && typeof(_POST.response)!=='undefined'){ 
			loadData(_POST.response);
		}
		else{ 		
			$i.promise._request('GET','mant/contactsources/'+ page._get('resource')).done(loadData);
		}
	}
		
	function loadData(rsp){		
		if (!page.newElement) {
			$i('#id').val(rsp.data.id);
			$i('#name').val(rsp.data.name);
			$i('#id').prop( "disabled", true );			
			$i('#lblcontactsources').html(rsp.data.name); 
		} else {
			$i('#id').val("");
			$i('#name').val("");					
		}
		page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
	}
});