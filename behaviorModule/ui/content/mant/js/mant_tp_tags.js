(function(page){
	page.config({
		autoshow : false,
		object : 'tptags',
		type : 'list',
		submit : function(){ nestag.createTag();}
	});
	
	var nestag = $i('#tags-nestable').nest({
		restURL : 'crm/thirdparty/tags',
	});

	this.callback = function(){
		nestag.reload();
	}
	
	page.loaded();
});