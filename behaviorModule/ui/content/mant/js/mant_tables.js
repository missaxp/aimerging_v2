(function(page){
	var tables = $i('#tables').superview({
		rest: "mant/tables",
		colResizable: true,
		rowId: 'id',
		dataType: ["id","name","attributes"],
		rowClick : {enable:true,onClick: doClick},
		appendCode: process,
		i18n : 'mant',
		cols: [{name: 'NAME'},{name: 'ATTRIBUTES'},{name: 'TAGS'},{name: 'GRANULAR_P'},{name : 'SERIES'}]
	});
	
	function doClick($jq){
		$i.hash.follow('./'+$jq.data('id'),null,$jq.data('name'));
	}
	
	//Called every time we load the view. (TABLE).
	function process($table){
		var tds = $table.find('tbody > tr > td');
		$.each(tds,function(){
			var $td = $(this);
			if($td.data('column')=='attributes'){
				$td.addClass('center');
				if($td.data('value')=="Y"){
					$td.html('<i class="fa fa-check-circle bigger-160 green"></i>');
				}
				else{
					$td.html('<i class="fa fa-times-circle bigger-160 red"></i>');
				}
			}
			if($td.data('column')=='tags'){
				$td.addClass('center');
				if($td.data('value')=="Y"){
					$td.html('<i class="fa fa-check-circle bigger-160 green"></i>');
				}
				else{
					$td.html('<i class="fa fa-times-circle bigger-160 red"></i>');
				}
			}
			if($td.data('column')=='granular_p'){
				$td.addClass('center');
				if($td.data('value')=="Y"){
					$td.html('<i class="fa fa-check-circle bigger-160 green"></i>');
				}
				else{
					$td.html('<i class="fa fa-times-circle bigger-160 red"></i>');
				}
			}
			if($td.data('column')=='series'){
				$td.addClass('center');
				if($td.data('value')=="Y"){
					$td.html('<i class="fa fa-check-circle bigger-160 green"></i>');
				}
				else{
					$td.html('<i class="fa fa-times-circle bigger-160 red"></i>');
				}
			}

		});
	}
	
	this.callback = function(){
		tables.reload();
	}
	
});