(function(page){
	page.config({
		autoshow : false, //Si volem gestionar manualment quan ensenyem la plana.
		object : 'attributes', //Objecte de referencia el qual es mostrará algun dels seus fills en aquesta plana.
		type : 'list', //Tipus de pàgina.
	});
	
	var attrsview = $i('#grid-container').superview({
		rest: "mant/attributes",
		rowId: 'id',
		rowClick: {enable:true,onClick: doClick},
		defaultsort: '-NAME',
		appendCode: process,
		tipscol: {del:true,action:buttonsFunction},
		dataType: ["name","type"],
		i18n : 'mant',
		cols: [{name: 'NAME'},{name: 'TYPE'}]
	});
	
	function process($table){
		var tds = $table.find('tbody > tr');
		$.each(tds,function(){
			var $td = $(this).find('td[data-column="type"]');
			var attrType = $td.data('value');
			if(attrType == "T"){
				$td.html($i.i18n('mant:type_text'));
			}
			else if(attrType == "U"){
				$td.html($i.i18n('mant:type_url'));
			}
		});
	}
	
	function buttonsFunction(jqElement,action){
		var aid = jqElement.data('id');		
		var name = jqElement.data('name');
		switch(action){
		case "button-delete":
			$i.modal({
				show: true,				
				title: $i.i18n('md_title_confirm'),
				description :  $i.i18n('md_desc_confirm_delete',{resource: name }),					
				success: function() {
					$i.promise._request({
						method : _DELETE_,
						restURL : 'mant/attributes/'+aid,							
						successMessage : $i.i18n('resSuccessDelete')
					}).done(function(data){
						attrsview.reload();																				
					});											
				}
			});
			break;		
		}			
	}
	
	//Click on each row Action.
	function doClick($tr){
		var id = $tr.data("id");
		var name = $tr.data('name');
		$i.promise._GET('mant/attributes/'+id).done(function(rsp){
			$i.hash.follow('./' +  id, id,{'name':name ,'response' : rsp});
		});
		
	}
	
	
	this.callback = function(){
		attrsview.reload();
	}
	
	page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
});