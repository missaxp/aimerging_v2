(function(page){
	page.config({
		autoshow : false, //Si volem gestionar manualment quan ensenyem la plana.
		object : 'qualifications', //Objecte de referencia el qual es mostrará algun dels seus fills en aquesta plana.
		type : 'list', //Tipus de pàgina.
	});
	
	var qualifictions = $i('#grid-container').superview({
		rest: "mant/qualifications",
		colResizable: false,
		rowId: 'id',
		rowClick: {enable:true,onClick: doClick},
		defaultsort: '+DESCRIPTION',
		tipscol: {del:true,action:buttonsFunction},
		dataType: ["id","description","created"],
		i18n : 'mant',
		cols: [{name: 'id'},{name: 'description'},{name: 'OWNER_DEPARTMENT'},{name: 'created'}]
	});
	
	function buttonsFunction(jqElement,action){
		var qid = jqElement.data('id');		
		var name = jqElement.data('description');
		switch(action){
		case "button-delete":
			$i.modal({
				show: true,				
				title: $i.i18n('md_title_confirm'),
				description :  $i.i18n('md_desc_confirm_delete',{resource: name }),					
				success: function() {
					$i.promise._request({
						method : _DELETE_,
						restURL : 'mant/qualifications/'+qid,							
						successMessage : $i.i18n('resSuccessDelete')
					}).done(function(data){
						qualifictions.reload();																				
					});											
				}
			});
			break;		
		}			
	}
	
	function successf(id,modal,$modal){
		var validate = $modal.validate();
		if(!validate) return false;
		
		var data = $modal.getValues();
		var aid = $('#'+id + ' input[name="att_id"]').val();
		var aname = $('#'+id + ' input[name="name"]').val();
		
		var promiseObject = {
			data : data
		}
		
		if(aid==""){ //New resource
			promiseObject.method = 'POST';
			promiseObject.restURL = 'mant/attributes';
			promiseObject.successMessage = $i.i18n('desc_resource_created');
		}
		else{ //Update resource.
			promiseObject.method = 'PUT';
			promiseObject.restURL = 'mant/attributes/'+aid;
			promiseObject.successMessage = $i.i18n('desc_resource_updated',{resource : aid});
		}
		
		$i.promise._request(promiseObject).done(function(data,statusText,xhr){
			attrsview.reload();
			modal.close();
		});
	}
	
	
	//Click on each row Action.
	function doClick($tr){
		var id = $tr.data("id");
		$i.promise._request('GET','mant/qualifications/'+id)
		.done(function(response){
			$i.hash.follow('./' +  id, id,{'name':response.data.name,'response' : response});
		});
	}
	
	this.callback = function(){
		qualifictions.reload();
	}
	
	page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
});