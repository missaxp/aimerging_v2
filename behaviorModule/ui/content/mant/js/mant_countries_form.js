(function(page){
	page.config({
		autoshow : false,
		object : 'countries',
		api : 'mant/countries',
		resource : $i._GET('country_id'),
		type : 'form',
		trackChanges : true,
		showBack : true,
		afterSubmit : function(rsp,values){
			$i('#lblcountry').html($i('#id').val()); 
			if(page.newElement){
				$i('#pkgrup').hide();
			}
		}
	});
	var _POST = $i._POST();	
	if(page.newElement){
		loadData();
	} else {
		if(_POST!==false && typeof(_POST.response)!=='undefined'){ 
			loadData(_POST.response);
		}
		else{ 		
			$i.promise._request('GET','mant/countries/'+ page._get('resource')).done(loadData);
		}
	}
	
	function loadData(rsp){		
		if (!page.newElement) {
			$i('#id').val(rsp.data.id);
			$i('#name').val(rsp.data.name);
			$i('#id').prop( "disabled", true );			
			$i('#lblcountry').html(rsp.data.id); 					
		} else {
			$i('#id').val("");
			$i('#name').val("");
			$i('#id').uniqueKey({rest : 'mant/countries/exist'});
			$i('#pkgrup').removeClass("hidden");
		}	
		page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
	}
});