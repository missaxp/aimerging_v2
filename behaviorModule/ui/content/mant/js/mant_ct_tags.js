(function(page){
	page.config({
		autoshow : false,
		object : 'cttags',
		type : 'list',
		submit : function(){ nestag.createTag();}
	});
	
	var nestag = $i('#tags-nestable').nest({
		restURL : 'crm/contact/tags',
	});

	this.callback = function(){
		nestag.reload();
	}
	
	page.loaded();
});