(function(page){
	page.config({
		autoshow : false,
		type : 'form',
		trackChanges : true,
		showBack : true,
		submit : function(values,response){
			$i.promise._request({
				method: 'PUT',
				restURL : 'mant/tables/'+tid,
				data : values,
				resource : tname,
				always : response
			});
		}
	});
	
	
	var tid = $i._GET('table_id');
	var $dynAttr = $i('#dyn-attr-inputs');
	
	$i.promise._request('GET','mant/tables/'+tid)
	.done(function(response){
		loadPage(response.data);
	});
	var tname;
	function loadPage(response){
		//console.log(response);
		var table = response.table;
		tname = response.table.name;
		$i.breadcrumb._add($i.i18n(tname));
		var checked;
		$.each(response.options.attributes.results,function(){
			checked = false
			for(var i=0,j=response.attributes.length;i<j;i++){
				var selatt = response.attributes[i];
				if(this.id==selatt.attr_id){
					checked = true;
					break;
				}
			}
			var el = '<!-- B:ATTR_'+this.id+' -->'+
					'<div class="form-group">'+
						'<label class="control-label col-xs-2 col-sm-2 no-padding-right" for="name" >'+this.name+'</label>'+
						'<div class="col-xs-5 col-sm-5">'+
							'<label>'+
								'<input type="checkbox" name="attr_'+this.id+'" data-id="attr_'+this.id+'" class="ace" '+(checked? 'checked':'')+'>'+
								'<span class="lbl" data-i18n="tp_tab:lbl_real_client"></span>'+
							'</label>'+
						'</div>'+
					'</div>'+
					'<!-- E:ATTR_'+this.ID+' -->';
			$dynAttr.append(el);
		});
		
		
		if(table.hasSeries){
			$i('#enable_series').prop('checked',true);
		}
		
		var dptsList = $i._get('configuration').departments.results;
		dptsList.unshift({id: 'ALL',name : $i.i18n('lbl_allDepartment')});
		
		$i('#list-series').list({
			name : 'series', //Això serveix per definir el nom de la llista, usat al retornar els valors.
			columns : [{text : $i.i18n('mant:col_name'),name : 'name',length: 40},
			           {text : $i.i18n('mant:col_serie'), name: 'serie',length: 4},
			           {text : $i.i18n('mant:col_counter'), name: 'counter', length : 7},
			           {text : $i.i18n('mant:col_department'), name: 'department_id', source : dptsList}], //Les columnes de la llista.
			uniqueKey : { on: 'serie',url : 'mant/tables/'+tid+'/serie/exist'},
			values : table.series, //Els valors inicials (tants valors com columnes)
			onElementRender : function(el,$ui){
				console.log(el);
				var dptName;
				var dptID;
				if(el.department_id==null){
					dptID = "ALL";
				}
				else{
					if(el.department_id.value){
						dptID = el.department_id.value;
					}
					else{
						dptID = el.department_id;
					}
				}
				$.each(dptsList,function(){
					if(this.id == dptID){
						dptName = this.name;
						return false;
					}
				});
				var html;
				html += '<td data-column="name">'+el.name+
				'<input type="hidden" nosend name="name" value="'+el.name+'" /></td>'+
				'<td data-column="serie">'+el.serie+
				'<input type="hidden" nosend name="serie" value="'+el.serie+'" /></td>'+
				'<td data-column="counter">'+el.counter+
				'<input type="hidden" nosend name="counter" value="'+el.counter+'" /></td>'+
				'<td data-column="department_id">'+dptName+
				'<input type="hidden" nosend name="department_id" value="'+dptID+'" /></td>';
				return html;
			}
		});
		
		
		
		
		page.loaded();
	}
	
	this.callback = function(){
		console.log("callback");
	};
});