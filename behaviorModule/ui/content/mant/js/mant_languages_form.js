(function(page){
	page.config({
		autoshow : false,
		object : 'attributes',
		api : 'mant/languages',
		resource : $i._GET('lang_id'),
		type : 'form',
		trackChanges : true,
		showBack : true,
		afterSubmit : function(rsp,values){
			$i('#lblLanguage').html($i('#culture_code').val());
			if(page.newElement){
				$i('#pkgrup').hide();
			}
		}
	});

	var _POST = $i._POST();
	if(page.newElement){
		loadData();
	} else {
		if(_POST!==false && typeof(_POST.response)!=='undefined'){ 
			loadData(_POST.response);
		}
		else{ 		
			$i.promise._request('GET','mant/languages/'+ page._get('resource')).done(loadData);
		}
	}
	
	function loadData(rsp){					
		if (!page.newElement) {
			$i('#culture_code').val(rsp.data.culture_code);
			$i('#name').val(rsp.data.name);
			$i('#iso_639').val(rsp.data.iso_639);
			$i('#culture_code').prop( "disabled", true );			
			$i('#lblLanguage').html(rsp.data.culture_code); 					
		} else {
			$i('#culture_code').val("");
			$i('#name').val("");
			$i('#iso_639').val("");
			$i('#culture_code').uniqueKey({rest : 'mant/languages/exist'});
			$i('#pkgrup').removeClass("hidden");
		}	
		page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada		
	}
});