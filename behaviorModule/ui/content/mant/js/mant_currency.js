(function(page){
	page.config({
		autoshow : false, //Si volem gestionar manualment quan ensenyem la plana.
		object : 'currency', //Objecte de referencia el qual es mostrará algun dels seus fills en aquesta plana.
		type : 'list', //Tipus de pàgina.
	});
	
	var listview = $i('#currency-view').superview({
		type : 'TABLE',
		rest: "mant/currency",
		colResizable: false,
		rowId: 'ID',
		defaultsort: '+NAME',
		dataType: ["ID", "NAME"],
		searchable: "ID,NAME",
		tipscol: {del:true,action:buttonsFunction},
		rowClick : {enable:true,onClick: doClick},
		i18n : 'mant',
		/*actionCol :{
       		colTitle: $i.i18n("widgets:action_drop_button"),
       		actions: [
               {action:'DELETE',title: $i.i18n("widgets:action_delete"),icon: 'ace-icon fa fa-trash'},
            ],
            help: true, //Adds buttons to "check" and "uncheck" all checkboxes
            onclick: doAction
       	
       	},*/			
		cols: [{name: 'ID'},{name: 'NAME'},{name: 'DECIMALS',sort:false},{name: 'ALIAS',sort:false}]
	});
	
function doAction(gridid,action,arrids){
		
		if(arrids === null) {
			
			$i.gritter({
				type: "success",
				title: $i.i18n('nothing_todo'),				
			});
			
		} else {
		
			var numElements = arrids.length;
			
			switch(action){	
			case "DELETE":
				$i.modal({
					show: true,				
					title: $i.i18n('mant:delete_currency'),
					description :  '<p>' +  $i.i18n('mant:ask_delete_n_currency',{num: numElements })  + '</p>',					
					success: function() {
						var method = _DELETE_;
						var url = 'mant/currency';
						var values = {arrayIds : arrids };
						$i.promise._request({
							method : method,
							restURL : url,							
							data : values
						})
						.done(function(jqXHR){
							//There are no deleted items							
							var varArg = jqXHR.data;
							var listElements = "";							
							
							if (varArg.nodeleteditems != 0) {
									
								listElements = "";
								for (var i = 0; i < varArg.nodeleteditemscode.length; i++) {
									listElements = listElements + " - " + varArg.nodeleteditemscode[i];
					            }
																																												
								$i.gritter({																	
									type: "warning",
									title: $i.i18n('mant:elements_nodeleted'),
									description: '<p>' + $i.i18n('mant:list_elements_nodeleted', { listNum: listElements.substring(2) })  + '</p>'
								});
																
							} 
							
							if (varArg.deleteditems != 0) {								
								listElements = "";
								for (var i = 0; i < varArg.deleteditemscode.length; i++) {
									listElements = listElements + " - " + varArg.deleteditemscode[i];
					            }								
								$i.gritter({
									type: "success",
									title: $i.i18n('mant:elements_deleted'),
									description:  '<p>' + $i.i18n('mant:list_elements_deleted', { listNum: listElements.substring(2) })  + '</p>' 
								});															
							}
																												
							listview.reload();																				
						});											
					}
				});
				break;
			}			
		}	
	}
	
		
	function buttonsFunction(jqElement,action){
		
		var id = jqElement.data('id');		
		var name = jqElement.data('name');
		
		switch(action){
		case "button-delete":
			$i.modal({
				show: true,				
				title: $i.i18n('mant:delete_currency'),
				description :  '<p>' +  $i.i18n('mant:ask_delete_currency',{currency: name, idcurrency: id })  + '</p>',
				success: function(){
					
					var method = _DELETE_;
					var url = 'mant/currency/' + id;
					$i.promise._request({
						method : method,
						restURL : url
					})
					.done(function(){
						listview.reload();									
					});								
				}											
			});
			break;		
		}			
	}
	
	function doClick($jq){				
		var currency_id = $jq.data('id');		
		$i.hash.follow('./'+currency_id, null,{currency_id : currency_id});
	}
	
	this.callback = function(){
		listview.reload();
	}
	
	page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
});