(function(page){
	page.config({
		autoshow : false,
		object : 'attributes',
		resource : $i._GET('elid'),
		api : 'mant/attributes',
		type : 'form',
		trackChanges : true,
		showBack : true,
		submit : function(values,response){
			var toSend = {
				name : $i('#name').val(),
				type : $i('#type').val(),
				labels : [],
				tables : []
			}
			
			
			for(var i=0,j=langs.length;i<j;i++){
				toSend.labels.push({lang: langs[i],value: $i('#att_lang_'+langs[i].toUpperCase()).val()});
			}
			
			for(var i=0,j=tables.length;i<j;i++){
				var table = tables[i];
				if(table.hasDynAttr){
					toSend.tables.push({id : table.id,add : $i('#table_id'+table.id).is(':checked')});
				}
			}
			
			var method = page.newElement? _POST_:_PUT_;
			var url = page.newElement? 'mant/attributes':'mant/attributes/'+page._get('resource');									
			$i.promise._request({
				method : method,
				restURL : url,
				resource : $i('#name').val(), 
				data : toSend,
				always : response
			})
			.done(function(rsp){
				if(page.newElement){
					page._setResource(rsp.data.id);
					$i('#name').prop('disabled',true);
					$i('#type').prop('disabled',true);
					$i('#type').trigger('chosen:updated');
					page.buttons.showDelete();
					$i.hash._updateLast(page._get('resource'),false);
					$i.breadcrumb._set(page._get('resource'));
				}
			});
		}
	});

	
	
	var _POST = $i._POST();	
	if(page.newElement){
		loadData();
	} else {
		if(_POST!==false && typeof(_POST.response)!=='undefined'){ 
			loadData(_POST.response);
		}
		else{ 		
			$i.promise._GET('mant/attributes/'+page._get('resource')).done(loadData);
		}
	}
	
	var langs, tables;
	function loadData(rsp){
		langs = $i._get('langs');
		tables = $i._get('configuration').tables;
		
		if(!page.newElement){
			$i('#name').val(rsp.data.name); //Set name on input
			$i('#type').val(rsp.data.type); //Set type on select
			$i('#name').prop('disabled',true);
			$i('#type').prop('disabled',true);
		}
		
		//Fill languages.
		var html_langs = "";
		for(var i=0,j=langs.length;i<j;i++){
			var langid = null;
			langid = langs[i];
			var value = "";
			if(!page.newElement){
				for(var k=0,l=rsp.data.langs.length;k<l;k++){
					if(rsp.data.langs[k].lang==langid.toUpperCase() && rsp.data.langs[k].value!=null){
						value = rsp.data.langs[k].value;
						break;
					}
				}
			}
			langid = langid.toUpperCase();
			
			html_langs += '<!-- B:LANG_'+langid+' -->'+
			'			<div class="form-group">'+
			'				<label class="control-label col-xs-2 col-sm-2 no-padding-right" for="name">'+$i.i18n('lang_'+langid)+'</label>'+
			'				<div class="col-xs-12 col-sm-7">'+
			'					<input type="text" name="att_lang_'+langid+'" data-id="att_lang_'+langid+'" class="col-xs-12 col-sm-12" value="'+value+'">'+
			'				</div>'+
			'			</div>'+
			'			<!-- E:LANG_'+langid+' -->';
		}
		
		$i('#language_zone').html(html_langs);
		
		//Fill tables.
		var html_tables = "";
		for(var i=0,j=tables.length;i<j;i++){
			table = tables[i];
			if(table.hasDynAttr){
				var checked = false;
				if(!page.newElement){
					for(var k=0,l=rsp.data.assignee.length;k<l;k++){
						if(rsp.data.assignee[k]==table.id){
							checked = true;
							break;
						}
					}
				}
				html_tables +='			<div class="form-group">'+
				'				<label class="control-label col-xs-2 col-sm-2 no-padding-right" for="name">'+$i.i18n('table_'+table.name)+'</label>'+
				'				<div class="col-xs-12 col-sm-7">'+
				'					<div class="checkbox">'+
				'						<label>'+
				'							<input data-group="1" name="table_id'+table.id+'" type="checkbox" class="ace" data-id="table_id'+table.id+'" '+(checked? 'checked':'')+'>'+
				'							<span class="lbl"></span>'+
				'						</label>'+
				'					</div>'+		
				'				</div>'+
				'			</div>';
			}
		}
		
		$i('#table_zone').html(html_tables);
		
		page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
	}
});