(function(page){
	page.config({
		autoshow : false,
		object : 'qualifications',
		type : 'form',
		api : 'mant/qualifications',
		resource : $i._GET('qid'),
		trackChanges : true,
		showBack : true,
		afterSubmit : function(rsp,values){
			$i('#id').html($i('#description').val());
		}
	});
	
	var _POST = $i._POST();	
	if(_POST!==false && typeof(_POST.response)!=='undefined'){
		loadData(_POST.response);
	}
	else{
		if(page.newElement){
			$i.promise._request('GET','mant/qualifications/options').done(loadData);
		}
		else{
			$i.promise._request('GET','mant/qualifications/'+page._get('resource')).done(loadData);
		}
	}
	
	function loadData(rsp){
		if($i.user._get().user_department==null){
			$i('#department_id').append('<option value="ALL">'+$i.i18n('lbl_allDepartment')+'</option>');
			$.each(rsp.data.options.departments.results,function(){ //Fill departments on departments select.
				$i('#department_id').append('<option value="'+this.id+'">'+this.name+'</option>');
			});
		}
		else{
			$i('#department_zone').remove();
		}
		
		
		if (page.newElement) { //If new page
			$i('#id').html("");
			if($i.user._get().user_department==null){
				$i('#department_id').val("ALL");
			}
		}
		else {
			$i('#id').html(rsp.data.description);
			$i('#description').val(rsp.data.description);
			if($i.user._get().user_department==null){
				$i('#department_id').val(rsp.data.department_id==null? "ALL":rsp.data.department_id);
			}
			$i('#id').prop( "disabled", true );
		}
		if($i.user._get().user_department!=null){
			$i('#department_id').attr('disabled',true);
		}
		page.loaded();
	}
});