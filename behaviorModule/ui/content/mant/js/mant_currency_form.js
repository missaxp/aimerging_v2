(function(page){
	page.config({
		autoshow : false,
		object : 'attributes',
		api : 'mant/currency',
		resource : $i._GET('currency_id'),
		type : 'form',
		trackChanges : true,
		showBack : true,
		afterSubmit : function(rsp,values){
			$i('#lblcurrency').html($i('#id').val());
			if(page.newElement){
				$i('#pkgrup').hide();
			}
		}
	});
	
	var _POST = $i._POST();	
	if(page.newElement){
		loadData();
	} else {
		if(_POST!==false && typeof(_POST.response)!=='undefined'){ 
			loadData(_POST.response);
		}
		else{ 		
			$i.promise._GET('mant/currency/'+ page._get('resource')).done(loadData);
		}
	}
	
	function loadData(rsp){		
		if (!page.newElement) {
			$i('#id').val(rsp.data.id);
			$i('#name').val(rsp.data.name);
			$i('#decimals').val(rsp.data.decimals);
			$i('#alias').val(rsp.data.alias);
			$i('#id').prop( "disabled", true );			
			$i('#lblcurrency').html(rsp.data.id); 
			
		} else {
			$i('#id').val("");
			$i('#name').val("");
			$i('#decimals').val("");
			$i('#alias').val("");
			$i('#id').uniqueKey({rest : 'mant/currency/exist'});
			$i('#pkgrup').removeClass("hidden");
		}	
		page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
	}
});