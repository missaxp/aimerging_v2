(function(page){
	page.config({
		autoshow : false, //Si volem gestionar manualment quan ensenyem la plana.
		object : 'contactsources', //Objecte de referencia el qual es mostrará algun dels seus fills en aquesta plana.
		type : 'list', //Tipus de pàgina.
	});
	
	var listview = $i('#contactsources-view').superview({
		type : 'TABLE',
		rest: "mant/contactsources",
		colResizable: false,
		rowId: 'ID',
		defaultsort: '+NAME',
		dataType: ["NAME"],
		searchable: "NAME",
		tipscol: {success: false,edit:false,del:true,flag:false,action:buttonsFunction},
		rowClick : {enable:true,onClick: doClick},
		i18n : 'mant',
		actionCol : {
       		colTitle: $i.i18n("widgets:action_drop_button"),
       		actions: [
               {action:'DELETE',title: $i.i18n("widgets:action_delete"),icon: 'ace-icon fa fa-trash'},
            ],
            help: true, //Adds buttons to "check" and "uncheck" all checkboxes
            onclick: doAction
       	
       	},
		cols: [{name: 'NAME'}]
	});
	
function doAction(gridid,action,arrids){
		
		if(arrids === null) {
			
			$i.gritter({
				type: "success",
				title: $i.i18n('nothing_todo'),				
			});
			
		} else {
		
			var numElements = arrids.length;
			
			switch(action){	
			case "DELETE":
				$i.modal({
					show: true,				
					title: $i.i18n('mant:delete_contactsource'),
					description :  '<p>' +  $i.i18n('mant:ask_delete_n_contactsource',{num: numElements })  + '</p>',					
					success: function() {
						var method = _DELETE_;
						var url = 'mant/contactsources';
						var values = {arrayIds : arrids };
						$i.promise._request({
							method : method,
							restURL : url,							
							data : values
						})
						.done(function(jqXHR){
							//There are no deleted items							
							var varArg = jqXHR.data;
							var listElements = "";							
							
							if (varArg.nodeleteditems != 0) {
									
								listElements = "";
								for (var i = 0; i < varArg.nodeleteditemscode.length; i++) {
									listElements = listElements + " - " + varArg.nodeleteditemscode[i];
					            }
																																												
								$i.gritter({																	
									type: "warning",
									title: $i.i18n('mant:elements_nodeleted'),
									description: '<p>' + $i.i18n('mant:list_elements_nodeleted', { listNum: listElements.substring(2) })  + '</p>'
								});
																
							} 
							
							if (varArg.deleteditems != 0) {								
								listElements = "";
								for (var i = 0; i < varArg.deleteditemscode.length; i++) {
									listElements = listElements + " - " + varArg.deleteditemscode[i];
					            }								
								$i.gritter({
									type: "success",
									title: $i.i18n('mant:elements_deleted'),
									description:  '<p>' + $i.i18n('mant:list_elements_deleted', { listNum: listElements.substring(2) })  + '</p>' 
								});															
							}
																												
							listview.reload();																				
						});											
					}
				});
				break;
			}			
		}	
	}
	
		
	function buttonsFunction(jqElement,action){
		
		var id = jqElement.data('id');		
		var name = jqElement.data('name');
		
		switch(action){
		case "button-delete":
			$i.modal({
				show: true,				
				title: $i.i18n('mant:delete_contactsource'),
				description :  '<p>' +  $i.i18n('mant:ask_delete_contactsource',{contactsource: name, idcontactsource: id })  + '</p>',
				success: function(){
					
					var method = _DELETE_;
					var url = 'mant/contactsources/' + id;
					$i.promise._request({
						method : method,
						restURL : url
					})
					.done(function(){
						listview.reload();									
					});								
				}											
			});
			break;		
		}			
	}
	
	function doClick($jq){				
		var contactsource_id = $jq.data('id');		
		$i.hash.follow('./'+contactsource_id, null,{contactsource_id : contactsource_id});
	}
	
	this.callback = function(){
		listview.reload();
	}
	
	page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
});