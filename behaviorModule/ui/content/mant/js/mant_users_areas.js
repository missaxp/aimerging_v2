(function(page){
	page.config({
		autoshow : false,
		object : 'userareas',
		type : 'list',
		submit : function(){ nestag.createTag();}
	});
	
	var nestag = $i('#tags-nestable').nest({
		restURL : 'users/tags',
	});

	this.callback = function(){
		nestag.reload();
	}
	
	page.loaded();
});