(function(page){
	page.config({
		autoshow : false, //Si volem gestionar manualment quan ensenyem la plana.
		object : 'chapters', //Objecte de referencia el qual es mostrará algun dels seus fills en aquesta plana.
		type : 'list', //Tipus de pàgina.
	});
	
	var stview = $i('#grid-container').superview({
		rest: "mant/skillTool",
		colResizable: false,
		rowId: 'id',
		rowClick: {enable:true,onClick: doClick},
		defaultsort: '-NAME',
		tipscol: {del:true,action:buttonsFunction},
		dataType: ["name","id"],
		i18n : 'mant',
		cols: [{name: 'NAME'},{name : 'DEPARTMENT'}]
	});
	
	function buttonsFunction(jqElement,action){
		var aid = jqElement.data('id');
		var name = jqElement.data('name');
		switch(action){
		case "button-delete":
			$i.modal({
				show: true,
				title: $i.i18n('md_title_confirm'),
				description :  $i.i18n('md_desc_confirm_delete',{resource: name }),					
				success: function(){
					$i.promise._request({
						method : _DELETE_,
						restURL : 'mant/skillTool/'+aid,
						successMessage : $i.i18n('resSuccessDelete',{resource : name})
					}).done(function(data){
						stview.reload();																				
					});											
				}
			});
			break;		
		}
	}
	
	//Click on each row Action.
	function doClick(a){
		var stid = a.data("id");
		var name = a.data('name');
		$i.promise._request({
			restURL : 'mant/skillTool/'+stid,
			bsend : a,
		}).done(function(response){
			$i.hash.follow('./'+stid,name,{'name':response.data.name,'response' : response});
		});
	}
	
	this.callback = function(){
		stview.reload();
	}
	
	page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
});