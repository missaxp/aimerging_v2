(function(page){
	page.config({
		autoshow : false,
		object : 'attributes',
		api : 'mant/contactindustries',
		resource : $i._GET('contactindustry_id'),
		type : 'form',
		trackChanges : true,
		showBack : true,
		afterSubmit : function(rsp,values){
			$i('#lblcontactindustries').html($i('#name').val());
		}
	});
		
	var _POST = $i._POST();	
	if(page.newElement){
		loadData();
	} else {
		if(_POST!==false && typeof(_POST.response)!=='undefined'){ 
			loadData(_POST.response);
		}
		else{ 		
			$i.promise._request('GET','mant/contactindustries/'+ page._get('resource')).done(loadData);
		}
	}
		
	function loadData(rsp){				
		if (!page.newElement) {
			$i('#id').val(rsp.data.id);
			$i('#name').val(rsp.data.name);
			$i('#lblcontactindustries').html(rsp.data.name); 			
		} else {
			$i('#id').val("");
			$i('#name').val("");					
		}	
		page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
	}
});