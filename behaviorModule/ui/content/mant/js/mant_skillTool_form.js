(function(page){
	page.config({
		autoshow : false,
		object : 'attributes',
		type : 'form',
		api : 'mant/skillTool',
		resource : $i._GET('stid'),
		trackChanges : true,
		showBack : true,
		afterSubmit : function(rsp,values){
			$i('#stid').html($i('#name').val());
			if(page.newElement){
				renderTable();
			}
		}
	});

	var _POST = $i._POST();	
	if(page.newElement){
		$i.promise._request('GET','mant/skillTool/options').done(loadData);
	}
	else {
		if(_POST!==false && typeof(_POST.response)!=='undefined'){ 
			loadData(_POST.response);
		}
		else{ 		
			$i.promise._request('GET','mant/skillTool/'+ page._get('resource')).done(loadData);
		}
	}
	
	function loadData(rsp){
		if($i.user._get().user_department==null){
			$i('#department_id').append('<option value="ALL">'+$i.i18n('lbl_allDepartment')+'</option>');
			$.each(rsp.data.options.departments.results,function(){ //Fill departments on departments select.
				$i('#department_id').append('<option value="'+this.id+'">'+this.name+'</option>');
			});
		}
		else{
			$i('#department_zone').remove();
		}
		
		
		if (page.newElement) {
			$i('#stid').html("");
			$i('#name').val("");
			if($i.user._get().user_department==null){
				$i('#department_id').val("ALL");
			}
		} else {
			$i('#name').val(rsp.data.name);
			$i('#stid').html(rsp.data.name);
			if($i.user._get().user_department==null){
				$i('#department_id').val(rsp.data.department_id==null? "ALL":rsp.data.department_id);
			}
			renderTable();
		}

		page.loaded(); //Cridem aquest mètode quan la pàgina hagi sigut creada
	}
	
	function renderTable(){
		//chapter-content
		var steview = $i('#chapter-content').superview({
			rest: "mant/skillTool/"+page._get('resource')+"/elements",
			rowId: 'id',
			rowClick: {enable:true,onClick: doClick},
			defaultsort: '-NAME',
			dataType: ["name","id"],
			i18n : 'mant',
			addReloadButton : false,
			actionCol : {
	       		colTitle: $i.i18n("mant:dp_button"),
	       		actions: [
	       		   {action:'ADD',title: $i.i18n("mant:skillTool_add"),icon: 'ace-icon fa fa-plus blue'},
	               {action:'DELETE',title: $i.i18n("mant:skillTool_del"),icon: 'ace-icon fa fa-ban red'},
	               {action:'REFRESH',title: $i.i18n("lbl_refresh"),icon: 'ace-icon fa fa-refresh'}
	            ],
	            help: true, //Adds buttons to "check" and "uncheck" all checkboxes
	            onclick: function(id,action,arrids){
	            	console.log(action);
	            	if(action=="ADD"){
	            		renderElementForm(true);
	            	}
	            	if(action=="REFRESH"){
	            		steview.reload();
	            	}
	            	if(action=="DELETE"){
	            		if(arrids!=null){
	            			$i.promise._request({
		            			method : 'DELETE',
		            			restURL : 'mant/skillTool/'+page._get('resource')+'/elements',
		            			data : {ceid : arrids},
		            			successMessage : $i.i18n('resSuccessDelete',{resource : name})
		            		})
		            		.done(function(){
		            			steview.reload();
		            		});
	            		}
	            	}
	            }
	       	},
			cols: [{name:'NAME'}]
		});
		
		//Click on each row Action.
		function doClick(a){
			var id = a.attr("id").replace('steid-','');
			var name = a.data('name');
			renderElementForm(false,id,name);
			
		}
		
		/**
		 * "add" means if is a new element or updated element
		 * 
		 * Renders modal an shows the data.
		 * 
		 * @param add
		 * @param id (optional)
		 * @param name (optional)
		 */
		function renderElementForm(add,id,name){
			var el = '<form class="form-horizontal" role="form" name="tag-form">'+
			'		<input type="hidden" name="id_parent" data-id="id_parent" value="'+(id==null? '':id)+'" />'+
			'		<div class="row">'+
			'		<div class="col-xs-12">'+
			'		<!-- PAGE CONTENT BEGINS -->'+
			'			<!-- B:CENAME -->'+
			'			<div class="form-group">'+
			'				<label class="control-label col-xs-2 col-sm-2 no-padding-right" for="name">'+$i.i18n('mant:col_name')+'</label>'+
			'				<div class="col-xs-10 col-sm-10">'+
			'					<input type="text" name="name" data-id="name" class="col-xs-12 col-sm-12" required value="'+(name? name:'')+'" autocomplete="off">'+
			'				</div>'+
			'			</div>'+
			'			<!-- E:CENAME -->'+
			'		</div>'+
			'	</div>'+
			'	</form>';
			
			$i.modal({
    			title : (add? $i.i18n("mant:title_skillTool_add"):$i.i18n("mant:title_skillTool_update")),
    			description : el,
    			show: true,
    			width: '500px',
    			resizable : false,
    			success: function(mid,modal,$modal){
    				var isValid = $modal.validate();
    				if(isValid){
    					var data = $modal.getValues();
        				$i.promise._request({
        					method: (add? 'POST':'PUT'),
        					restURL :'mant/skillTool/'+page._get('resource')+'/elements'+(add? '':'/'+id),
        					data : data,
        					successMessage : (add? $i.i18n('desc_resource_created'):$i.i18n('desc_resource_updated',{resource : id}))
        				})
        				.done(function(){
        					steview.reload();
        				});
    				}
    				else{
    					return false;
    				}
    			}
    		});
		}
	}
});