(function(page){
	var username = false;
	if($i._GET('username')===false && $i.hash._get()=='#profile'){
		username = "me";
	}
	else{
		username = $i._GET('username');
	}
	
	page.config({
		autoshow : false,
		object : 'users',
		api : 'users',
		resource : username,
		type : 'form',
		trackChanges : true,
		showBack : true,
		afterSubmit : function(rsp,values){
			if(page.newElement){
				$i('#div_username').remove(); //Remove username input
				$i('#uid').html(rsp.data.username); //Render tp id
				/**
				 * Això ja ho fa la pàgina automàticament, però en comptes del username, posa el ID. aqui en aquest cas posem el username pq volem.
				 */
				$i.hash._updateLast(rsp.data.username,false); 
				$i.breadcrumb._set(rsp.data.username); // Set tpid on aria bar.
			}
			if($i.user._get().username==page._get('resource')){
				//If we have udpated our user, update $i.user._get() (current logged user), with the user data.
				$.each(values,function(prop,value){
					if(prop=='user_department' && value=="ALL"){
						$i.user._set(prop,null);
					}
					else{
						$i.user._set(prop,value);
					}
				});
			}
		} 
	});
	
	var _POST = $i._POST();
	if(_POST!==false && typeof(_POST.response)!=='undefined'){ //If user data is passed through var, load it.
		loadData(_POST.response);
	}
	else{ //If not, we load via web service.
		if(page.newElement){
			$i.promise._GET('users/options').done(loadData);
		}
		else{
			$i.promise._GET('users/'+username).done(loadData);
		}
	}
	
	function loadData(response){
		
		
		var user = response.data;
		var uid = user.id; //User ID
		var username = user.username;
		$i('#uid').html(username); //Username.
		
		//Set tags to select multiple
		$.each(user.options.tags,function(){
			var tagF = this;
			var selected = false;
			//Mark selected tags for this client.
			$.each(user.tags,function(){
				if(tagF.id==this.id){
					selected = true;
					return true;
				}
			});
			var el = '<option value="'+this.id+'" '+(selected? 'selected':'')+'>'+this.name+'</option>';
			$i('#tags').append(el);
		});
		
		
		//Fill departments on departments select.
		$i('#user_department').append('<option value="ALL">'+$i.i18n('users:lbl_allDepartment')+'</option>');
		if(typeof(user.options.departments)!=='undefined'){ //Els usuaris que pertanyen a un departament no poden canviar a altres usuaris de departament ( o a ell mateix clar)
			$.each(user.options.departments.results,function(){
				$i('#user_department').append('<option value="'+this.id+'">'+this.name+'</option>');
			});
		}
		
		
		
		//Fill countries in needed selects.
		$.each(user.options.countries.results,function(){
			$i('#country_id').append('<option value="'+this.id+'">'+this.name+'</option>');
			$i('#country_birth_id').append('<option value="'+this.id+'">'+this.name+'</option>');
		});
		
		//Fill roles in needed selects.
		$.each(user.options.roles.results,function(){
			$i('#role_id').append('<option value="'+this.id+'">'+this.name+'</option>');
		});
		
		
		//Fill groups in needed selects.
		$.each(user.options.groups.results,function(){
			$i('#user_group').append('<option value="'+this.id+'">'+this.name+'</option>');
		});
		
		//Fill timeZones.
		$.each($i.timeZones,function(){
			$i('#timezone').append('<option value="'+this+'">'+this+'</option>')
		});
		$i('#timezone').val(user.timezone);
		
		//Fill User Interface available languages.
		$.each($i._get('langs'),function(){
			$i('#ui_language').append('<option value="'+this+'">'+$i.i18n('lang_'+this.toUpperCase())+'</option>')
		});
		$i('#ui_language').val(user.ui_language);
		
		//TODO: Fer que llisti tots els punts de menu per poder seleccionar la home page.
		//console.warn($i.menu._get());
		var m = $i.menu._get();
		var options = getMenu(m,"","");
		function getMenu(m,subspace,route){
			var responses = [];
			if(route!=""){
				route += '/';
			}
			else{
				route += '#';
				responses.push({text : '/', value : '/'});
			}
			if(subspace!=""){
				subspace+= "::";
			}
			
			if(!m.menu){
				return
			}
			$.each(m.menu,function(){
				var fn = $i.i18n('menu:'+(route+this.route).replace('#',''));
				responses.push({text : subspace+fn, value : route+this.route});
				if(this.menu){
					var r2 = getMenu(this,subspace+fn,route+this.route);
					responses = responses.concat(r2);
				}
			});
			return responses;
		}
		
		
		$.each(options,function(){
			$i('#ui_home_page').append('<option value="'+this.value+'">'+this.text+'</option>');
		});
		
		
		
		
		$chapterZone = $i('#chapters-loadZone');
		$.each(user.options.chapters,function(){
			var chapter = this;
			var el = '<!-- B:CHAPTER_'+chapter.id+' -->'+
			'<div class="form-group">'+
				'<label class="control-label col-xs-12 col-sm-2 no-padding-right blue" for="'+chapter.id+'" >* '+chapter.name+'</label>'+
				'<div class="col-xs-12 col-sm-7">'+
					'<select multiple class="col-xs-12 col-sm-12 tag-input-style chosen" name="chapter_'+this.id+'" data-id="chapter_'+this.id+'">';
						$.each(chapter.elements,function(){
							var element = this;
							var selected = false;
							$.each(user.chapters,function(){
								if(this.chapter_id==chapter.id && this.element_id==element.id){
									selected = true;
									return true;
								}
							});
							el += '<option value="'+element.id+'" '+(selected? 'selected':'')+'>'+element.name+'</option>';
						});
					el +='</select>'+
				'</div>'+
			'</div>'+
			'<!-- E:CHAPTER_'+this.id+' -->';
			$chapterZone.append(el);
		});
		
		/**
		 * INICI ZONA QUALIFICACIONS.
		 */
		$qualifications = $i('#qualifications-loadZone'); //Div contenidor de les qualificacions
		if(user.qualifications.length>0){ //Si l'usuari ja té alguna qualificació acceptada i/o aprovada, marquem el checkbox de la confirmació de lectura
			$i('#q_advice').prop('disabled',true);
			$i('#q_advice').prop('checked',true);
		}
		//Per a cada qualificació disponible per a aquest usuari (que va cribat pel departament d'aquest usuari).
		$.each(user.options.qualifications,function(){
			//console.log(this);
			var uq = getUserQualification(this.id); //Comprovem si per aquesta qualificació, l'usuari ja l'ha aprovada.
			var el = '<!-- B:QUALIFICATION_'+this.id+' -->'+
			'<div class="row">'+
			'<div class="form-group">'+
				'<div class="col-xs-12 col-sm-5">'+
					'<div class="checkbox">'+
						'<label>'+
							'<input type="checkbox" data-group="qualification" data-id="qualification_'+this.id+'" name="qualification_'+this.id+'" class="ace" '+(uq!==false? 'checked':'')+'>&nbsp;'+
							'<span class="lbl">'+this.description+'</span>'+
						'</label>'+
					'</div>'+
				'</div>'+
				'<div class="col-xs-12 col-sm-5">';
			if(uq!==false && uq.approved==true){ //Si té dades per a aquesta quailficació i ha estat aprovada per a algu amb drets per a aprovar-la, ho mostrem.
				el += '<span>'+$i.i18n('users:lbl_qualification_approved_by',{res : uq.approved_by})+' ('+uq.approved_ts+')</span>';
			}
			else if(uq!==false && uq.approved==false){ //Si l'usuari ha confirmat aquesta qualificació però encara no ha estat aprovada, mostrem això
				//TODO:: Això ha d'anar filtrat en funció dels permisos de qui veu aquest usuari.
				el += '<div class="checkbox">'+
				'<label>'+
					'<input type="checkbox" data-id="q_approve_'+this.id+'" name="q_approve_'+this.id+'" class="ace">'+
					'<span class="lbl">'+$i.i18n('users:lbl_qualification_approve')+'</span>'+
				'</label>'+
				'</div>';
			}
			'</div>'+
			'</div>'+
			'</div>'+
			'<!-- E:QUALIFICATION_'+this.id+' -->';
			
			$qualifications.append(el);
		});
		
		//Quan canviem algun valor de les qualificacions, habilitem el botó de confirmació de lectura i el desmarquem.
		//Si no hi ha cap checkbox marcat, el camp de confirmació de lectura no serà comprovat al validar/desar el formulari
		$i('input[data-group="qualification"]').on('change',function(){
			$i('#q_advice').prop('required',false);
			$i('input[data-group="qualification"]:checked').each(function(){
				$i('#q_advice').prop('required',true);
			})
			$i('#q_advice').prop('disabled',false);
			$i('#q_advice').prop('checked',false);
		});
		
		function getUserQualification(id){
			if(typeof(user.qualifications)!=='object'){
				return false;
			}
			var q = user.qualifications;
			for(var i=0,j=q.length;i<j;i++){
				if(q[i].qualification_id==id){
					return q[i];
				}
			}
			return false;
		}
		
		
		
		
		/**
		 * FI ZONA QUALIFICACIONS.
		 */
		//As the object ct name and input:name are the same, we can do this:
		//Sure we have more properties than input:names, but it will be undefined and no exception are called.
		if(!page.newElement){
			$i('#div_username').remove(); //Remove username input
			$.each(user,function(key,value){
				if(key!='tags' && key!='qualifications'){
					$obj = $i('#'+key);
					if($obj.attr('type')=='checkbox'){
						$obj.prop('checked',(value=="Y"? true:false));
					}
					else{
						if($obj.is('select') || $obj.is('input')){
							$obj.val(value);
						}
						else if($obj.is('div')){
							$obj.html(value);
						}
					}
				}
			});
			$i('#department_id').val((user.department_id==null? "ALL":user.department_id))
			$i('#user_department').val((user.user_department==null? "ALL":user.user_department));
			if($i.user._get().user_department!=null){
				$i('#department_id').attr('disabled',true); //Si l'usuari que entra a editar l'usuari té un departament fixe, no podrà canviar la visibilitat de l'usuari.
				//$i('#user_department').attr('disabled',true); //Si l'usuari que entra a editar l'usuari té un departament fixe, no podrà canviar el departament de l'usuari que edita
			}
			
		}
		else{
			
			
			
			if($i._get('webservice').authentication_required){
				$i('#owner_uid').val($i.user._get().id);
				$i('#owner_gid').val($i.user._get().user_group);
				$i('#department_id').val($i.user._get().user_department);
				if($i.user._get().user_department!=null){
					$i('#department_id').val($i.user._get().user_department);
					$i('#department_id').attr('disabled',true);
				}
				else{
					$i('#department_id').val("ALL");
				}
			}
			else{
				$i('#user_id').val('');
				$i('#department_id').val('');
			}
			
			$i('#user_department').val('');
			$i('#user_group').val('');
			$i('#role_id').val(''); //Initialize to null;
			$i('#username').uniqueKey({rest : 'users/exist'});
		}
		
		$i('.rating').rating({
			cancel : false,
			half : false,
			name : 'priority',
			value : user.priority
		});
		
		
		
		
		$i('#avatar').avatar(user.avatar);
		
		
		$i('#tp_id').relation({rest : 'crm/thirdparty',columns : ["CODE","NAME"],render : "NAME",set: "CODE",i18n: 'users',value: user.tp_id,name : user.tp_name});
		$i( "#city" ).suggestion({rest : 'mant/cities'}); //city autocomplete helper.
		
		
		$i('#curriculum_upload').fupload({ //Upload documents.
			restURL: 'users/'+(!page.newElement? uid+'/':'')+'curriculums', //If this is a new user
			type :2,
			newElement: page.newElement,
			progressbar : true,
			autoSubmit: true,
			cancelButton: false,
			flist : {
				rest : 'users/'+uid+'/curriculums'
			},
			ondone : function(){ console.log("ALL LOADED");}
		});
		
		page.loaded({
			owner_uid : user.owner_uid, //Send current resource user id.
			owner_gid : user.owner_gid, //Send current resource group id
			dynattr : user.dynattr, //Send current resource dynAttr 
			tags : user.tags, //Send current resource tags
			dpt_id : user.department_id, //Send current resource department_id
			$dynAttr : $i('#dynAttr-loadZone'), //Send where $ will append the dynamic attributes
			$tags : $i('#tags-loadZone'), //Send where $ will append the tags
			options : user.options //Resource options
		});
	}
});