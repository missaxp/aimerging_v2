﻿<?php
/**
 * Inserció de flashos en el HTMLArea
 * @author Carles Temporal
 */

require $_SERVER["DOCUMENT_ROOT"]."/mant/inicialitza.php";
require BaseDir."/comu/funcions.php";
require BaseDir."/mant/lib/php/func_mant.php";
require BaseDir."/declara.php";
require BaseDir."/mant/inicia_mant.php";

$PHP_Error = "";
$Retorn_Error = "";
$Elements_Modificats = "";
$XMLResp = "";

// Obté el id del flash que es vol actualitzar
initReqVar("id_val","");
$id = str_replace("'","''",trim($_REQUEST['id_val']));

initReqVar("accio","");
if ($_REQUEST['accio']=="Recupera") {
	$sql = "select NOM_FLASH,DESC_FLASH from flash_".$sessio->idioma_contingut->id." where id_site='".$site->id_site."' and id_flash='".$id."'";
	$rs2 = $dbcon->execute($sql);
	if ($rs2->fetch()) {
		$Elements_Modificats = "<Flash>Existent</Flash>";
		$Elements_Modificats .= "<Valors parametre=\"f_alttext\"><![CDATA[".$rs2->getVal('DESC_FLASH')."]]></Valors>";
	}
	$rs2->close();
	
	$XMLResp = "<PHP_Estat>OK</PHP_Estat>";
	$XMLResp .= $Elements_Modificats;
}

enviaXML($XMLResp);
?>