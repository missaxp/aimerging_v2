/**
 * d2img plugin configuraction
 */

(function() {
	tinymce.create('tinymce.plugins.d2imgPlugin', {
		init : function(ed, url) {
			// Register commands
			ed.addCommand('mced2img', function() {
				// Internal image object like a flash placeholder
				if (ed.dom.getAttrib(ed.selection.getNode(), 'class').indexOf('mceItem') != -1)
					return;

				ed.windowManager.open({
					file : url + '/dialog.php',
					width : 500 + parseInt(ed.getLang('d2img.delta_width', 0)),
					height : 400 + parseInt(ed.getLang('d2img.delta_height', 0)),
					inline : 1
				}, {
					plugin_url : url
				});
			});

			// Register buttons
			ed.addButton('image', {
				title : 'Insert/edit image',
				cmd : 'mced2img'
			});
			
			ed.addMenuItem('image', {
				icon: 'image',
				text: 'Insert/edit image',
				cmd : 'mced2img',
				context: 'insert',
				prependToContext: true
			});
		},

		getInfo : function() {
			return {
				longname : 'd2img for d2web',
				author : 'iDisc',
				authorurl : 'http://www.idisc.es',
				version : '1.1'
			};
		}
	});

	// Register plugin
	tinymce.PluginManager.add('d2img', tinymce.plugins.d2imgPlugin);
})();