<?php

namespace model;

//use JsonSerializable;

abstract class Model /*implements JsonSerializable */{

	public function toArray(){

		return $this->processArray(get_object_vars($this));
	}

	private function processArray($array){

		foreach($array as $key => $value) {
			if (is_object($value)) {
				if(get_class($value)=="DateTime"){
					/**@var DateTime $value*/
					$array[$key] = $value->format("d/m/Y H:i:s");
				}
				else{
					$array[$key] = $value->toArray();
				}
				
			}
			if (is_array($value)) {
				$array[$key] = $this->processArray($value);
			}
		}
		// If the property isn't an object or array, leave it untouched
		return $array;
	}

	public function __toString(){

		return json_encode($this->toArray());
	}
	
	
}
?>