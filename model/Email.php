<?php

namespace model;

class Email extends Model{
	protected $email;
	protected $idConfirmEmail;

	public function __construct(){

	}

	public function getEmail(){

		return $this->email;
	}

	public function getIdConfirmEmail(){

		return $this->idConfirmEmail;
	}

	public function setEmail($email){

		$this->email = $email;
	}

	public function setIdConfirmEmail($idConfirmEmail){

		$this->idConfirmEmail = $idConfirmEmail;
	}
	
	public function checkIntegrity(){
		return (isset($this->email) && isset($this->idConfirmEmail));
	}
    /*public function jsonSerialize()
    {
        return array(
            "email" => $this->email,
            "idConfirmEmail" => $this->idConfirmEmail,
        );
    }*/
}
?>