<?php

/**
* @author zroque
* @version 1.0
* created on 21 jun. 2018
*/
namespace model;

use JsonSerializable;

include_once BaseDir. '/model/Model.php';

class Language extends Model implements JsonSerializable {
	
	/**
	 * Standardized identifier for this language
	 * 
	 * @var string
	 */
	protected $idLanguage;
	/**
	 * 
	 * @var string
	 */
	protected $languageName;
	
	/**
	 * 
	 * 
	 * @var Analysis
	 */
	protected $analysis;
	
	/**
     * @return \model\Analysis
     */
    public function getAnalysis()
    {
        return $this->analysis;
    }

	/**
     * @param \model\Analysis $analysis
     */
    public function setAnalysis($analysis)
    {
        $this->analysis = $analysis;
    }

	public function __construct() {
		$this->analysis = new Analysis();
	}
	/**
	 * @return string
	 */
	public function getIdLanguage() {
		return $this->idLanguage;
	}

	/**
	 * @return string
	 */
	public function getLanguageName() {
		return $this->languageName;
	}

	/**
	 * @param string $idLanguage
	 */
	public function setIdLanguage($idLanguage) {
		$this->idLanguage = $idLanguage;
	}

	/**
	 * @param string $languageName
	 */
	public function setLanguageName($languageName) {
		$this->languageName = $languageName;
	}

	public static function filterLanguage(string $idLanguage){
		switch ($idLanguage){
			case 'gl':
				return 'gl-ES';
			case 'ca':
				return 'ca-ES';
			case 'en':
				return 'en-US';
			case 'va':
			case 'vl-ES':
				return 'va-ES';
			case 'eu':
				return 'eu-ES';
			case 'es':
				return 'es-ES';
			case 'ja':
				return 'ja-JP';
			case 'esLatAM':
			case 'esLA':
				return 'es-LA';
			case 'fr':
				return 'fr-FR';
			case 'hu':
				return 'hu-HU';
            case 'Spanish (Spain)':
                return 'es-ES';
            case 'Catalan':
            	return 'ca';
            case 'English (US)':
                return 'en-US';
			default:
				return $idLanguage;
		}
	}

    public function jsonSerialize()
    {
        return array(
            "idLanguage" => $this->idLanguage,
            "analysis" => $this->analysis,
            "languageName" => $this->languageName,
        );
    }
}
?>