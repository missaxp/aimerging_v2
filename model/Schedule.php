<?php

namespace model;

class Schedule extends Model {
	/**
	 * Inqueue
	 * 
	 * @var string
	 */
	const _INQUEUE = "IN";
	
	/**
	 * Ondemand
	 * 
	 * @var string
	 */
	const _ONDEMAND = "ON";
	
	/**
	 * Identifier on database
	 * 
	 * @var integer
	 */
	protected $idSchedule;
	
	/**
	 * This could be ONDEMAND or INQUEUE
	 * 
	 * @var string
	 */
	protected $processType;
	
	/**
	 * Start date of the process
	 * 
	 * @var string
	 */
	protected $start;
	
	/**
	 * Due date of the process
	 * 
	 * @var string
	 */
	protected $end;
	
	/**
	 * Time interval in which this type of process is executed. Now is definite on hours
	 * 
	 * @var integer
	 */
	protected $interval;
	
	/**
	 * Identifier of the process to which it belongs
	 * 
	 * @var integer
	 */
	protected $idProcess;

	public function __construct(){
		
	}

	/**
	 * Schedule identifier
	 * 
	 * @return int
	 */
	public function getIdSchedule(){

		return $this->idSchedule;
	}

	/**
	 * 
	 * 
	 * @return string
	 */
	public function getProcessType(){

		//return $this->processType;
		return self::_ONDEMAND;
	}
	
	
	/**
	 * Check if has to process the TASK QUEUE if this process scheduling are in INQUEUE state.
	 * @return boolean
	 */
	public function checkIfHasProcessQueue(){
		return true;
	}
	

	/**
	 * Return start date of the process
	 * 
	 * @return string
	 */
	public function getStart(){

		return $this->start;
	}

	/**
	 * 
	 * @return string
	 */
	public function getEnd(){

		return $this->end;
	}

	/**
	 * 
	 * @return integer
	 */
	public function getInterval(){

		return $this->interval;
	}

	/**
	 * 
	 * @return integer
	 */
	public function getIdProcess(){

		return $this->idProcess;
	}

	public function setIdSchedule($idSchedule){

		$this->idSchedule = $idSchedule;
	}

	public function setProcessType($processType){

		$this->processType = $processType;
	}

	public function setStart($start){

		$this->start = $start;
	}

	public function setEnd($end){

		$this->end = $end;
	}

	public function setInterval($interval){

		$this->interval = $interval;
	}

	public function setIdProcess($idProcess){

		$this->idProcess = $idProcess;
	}
/*    public function jsonSerialize()
    {
        return get_object_vars($this);
    }*/
}

?>