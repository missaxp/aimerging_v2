<?php


namespace model;


use JsonSerializable;

class AvailableTask extends Model implements JsonSerializable {
    /**
     * @var string Task id in client source
     */
    protected $idClientTask;
    /**
     * @var string Task title
     */
    protected $title;
    /**
     * @var string Client defined deadline
     */
    protected $dueDate;//$deadline;
    /**
     * @var Language Source language
     */
    protected $sourceLanguage; //$sourceLang;
    /**
     * @var Language[] List of target languages
     */
    protected $targetsLanguage;//$targetLangs;
    /**
     * @var TaskAditionalProperties[] List of additional properties
     */
    protected $additionalProperties;
    /**
     * @var string task instructions
     */
    protected $message;

    /**
     * AvailableTask constructor.
     */
    public function __construct(){
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return AvailableTask
     */
    public function setMessage(string $message): AvailableTask
    {
        $this->message = $message;
        return $this;
    }


    /**
     * @return string
     */
    public function getIdClientTask(): string
    {
        return $this->idClientTask;
    }

    /**
     * @param string $idClientTask
     * @return AvailableTask
     */
    public function setIdClientTask(string $idClientTask): AvailableTask
    {
        $this->idClientTask = $idClientTask;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return AvailableTask
     */
    public function setTitle(string $title): AvailableTask
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getDueDate(): string
    {
        return $this->dueDate;
    }

    /**
     * @param string $dueDate
     * @return AvailableTask
     */
    public function setDueDate(string $dueDate): AvailableTask
    {
        $this->dueDate = $dueDate;
        return $this;
    }

    /**
     * @return Language
     */
    public function getSourceLanguage(): Language
    {
        return $this->sourceLanguage;
    }

    /**
     * @param Language $sourceLanguage
     * @return AvailableTask
     */
    public function setSourceLanguage(Language $sourceLanguage): AvailableTask
    {
        $this->sourceLanguage = $sourceLanguage;
        return $this;
    }

    /**
     * @return Language[]
     */
    public function getTargetsLanguage(): array
    {
        return $this->targetsLanguage;
    }

    /**
     * @param Language[] $targetsLanguage
     * @return AvailableTask
     */
    public function setTargetsLanguage(array $targetsLanguage): AvailableTask
    {
        $this->targetsLanguage = $targetsLanguage;
        return $this;
    }

    /**
     * @return TaskAditionalProperties[]
     */
    public function getAdditionalProperties(): array
    {
        return $this->additionalProperties;
    }

    /**
     * @param TaskAditionalProperties[] $additionalProperties
     * @return AvailableTask
     */
    public function setAdditionalProperties(array $additionalProperties): AvailableTask
    {
        $this->additionalProperties = $additionalProperties;
        return $this;
    }

    public function asTask(){
        $task = new Task();
        $task->setTitle($this->title);
        $task->setDueDate($this->dueDate);
        $task->setTargetsLanguage($this->targetsLanguage);
        $task->setSourceLanguage($this->sourceLanguage);
        $task->setAditionalProperties($this->additionalProperties);
        $task->setMenssage($this->message);
        $task->setIdClientetask($this->idClientTask);

        return $task;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}