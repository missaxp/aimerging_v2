<?php

/**
 *
 * @author Ljimenez
 * @version 1.0
 *          created on 22 jun. 2018
 */
namespace model;

class PropertyAction extends Model{
	const _TYPE_MANUAL = "M";
	const _TYPE_DYNAMIC = "D";
	protected $idProperty;
	protected $propertyName;
	protected $propertyValue;
	protected $idCreate;
	protected $propertyType;

	public function __construct(){

	}

	public function getIdProperty(){

		return $this->idProperty;
	}

	public function getPropertyName(){

		return $this->propertyName;
	}

	public function getPropetyValue(){

		return $this->propertyValue;
	}

	public function getIdCreate(){

		return $this->idCreate;
	}
	
	public function getPropertyType(){
		
		return $this->propertyType;
	}

	public function setIdProperty($idProperty){

		$this->idProperty = $idProperty;
	}

	public function setPropertyName($propertyName){

		$this->propertyName = $propertyName;
	}

	public function setPropetyValue($propetyValue){

		$this->propertyValue = $propetyValue;
	}

	public function setIdCreate($idCreate){

		$this->idCreate = $idCreate;
	}
	
	public function setPropertyType($propertyType){
		
		$this->propertyType = $propertyType;
	}
	
	public function checkIntegrity(){
		return (isset($this->propertyName) && isset($this->propertyValue) && isset($this->idCreate) && isset($this->propertyType));
	}
    /*public function jsonSerialize()
    {
        return get_object_vars($this);
    }*/
}
?>