<?php

namespace model;

class Category extends Model{
	/**
	 * Identifier within of storege system
	 * 
	 * @var integer
	 */
	protected $idCategory;
	/**
	 * 
	 * @var string
	 */
	protected $categoryName;
	
	/**
	 * 
	 * @var string
	 */
	protected $categoryDescription;
	public function __construct($idCategory, $categoryName, $categoryDescription) {
		$this->idCategory = $idCategory;
		$this->categoryName = $idCategory;
		$this->categoryDescription = $categoryDescription;
	}
	/**
	 *
	 * @return integer
	 */
	public function getIdCategory() {
		return $this->idCategory;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getCategoryName() {
		return $this->categoryName;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getCategoryDescription() {
		return $this->categoryDescription;
	}
	
	/**
	 *
	 * @param integer $idCategory
	 */
	public function setIdCategory($idCategory) {
		$this->idCategory = $idCategory;
	}
	
	/**
	 *
	 * @param string $categoryName
	 */
	public function setCategoryName($categoryName) {
		$this->categoryName = $categoryName;
	}
	
	/**
	 *
	 * @param string $categoryDescription
	 */
	public function setCategoryDescription($categoryDescription) {
		$this->categoryDescription = $categoryDescription;
	}

    /*public function jsonSerialize()
    {
        return array(
            //"idCategory" => $this->idCategory,
            "categoryDescription" => $this->categoryDescription,
            "categoryName" => $this->categoryName,
        );
    }*/
}
?>