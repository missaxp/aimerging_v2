<?php

namespace model;

class PreprocessRule {
	/**
	 * *
	 *
	 * @var integer
	 */
	protected $preprocessId;

	/**
	 * *
	 *
	 * @var integer
	 */
	protected $proprocessRuleId;

	/**
	 * *
	 *
	 * @var string
	 */
	protected $propertyName;

	/**
	 * *
	 *
	 *
	 * @var string
	 */
	protected $operator;

	/**
	 * *
	 *
	 * @var string
	 */
	protected $propertyValue;

	public function __construct(){

	}

	/**
	 *
	 * @return number
	 */
	public function getPreprocessId(){

		return $this->preprocessId;
	}

	/**
	 *
	 * @param number $preprocessId
	 */
	public function setPreprocessId($preprocessId){

		$this->preprocessId = $preprocessId;
	}

	/**
	 *
	 * @return number
	 */
	public function getProprocessRuleId(){

		return $this->proprocessRuleId;
	}

	/**
	 *
	 * @param number $proprocessRuleId
	 */
	public function setProprocessRuleId($proprocessRuleId){

		$this->proprocessRuleId = $proprocessRuleId;
	}

	/**
	 *
	 * @return string
	 */
	public function getPropertyName(){

		return $this->propertyName;
	}

	/**
	 *
	 * @param string $propertyName
	 */
	public function setPropertyName($propertyName){

		$this->propertyName = $propertyName;
	}

	/**
	 *
	 * @return string
	 */
	public function getOperator(){

		return $this->operator;
	}

	/**
	 *
	 * @param string $operator
	 */
	public function setOperator($operator){

		$this->operator = $operator;
	}

	/**
	 *
	 * @return string
	 */
	public function getPropertyValue(){

		return $this->propertyValue;
	}

	/**
	 *
	 * @param string $propertyValue
	 */
	public function setPropertyValue($propertyValue){

		$this->propertyValue = $propertyValue;
	}
	
	/***
	 * Checks if the rule checks against the current value of the task's property
	 * @param Task $task
	 * @return bool
	 */
	public function checkRule(Task $task){
		
	}
/*    public function jsonSerialize()
    {
        return get_object_vars($this);
    }*/
}

