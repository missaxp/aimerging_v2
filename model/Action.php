<?php

/**
 *
 * @author Ljimenez
 * @version 1.0
 *          created on 21 jun. 2018
 */
namespace model;

require_once BaseDir. '/model/Model.php';
require_once BaseDir.'/dataAccess/dao/TaskDAO.php';
use dataAccess\dao\SourceDAO;
class Action extends Model{
	const _ACTION_SUCCEDS = 'ACTION_SUCCEDS';
	const _ACTION_WITH_WARNINGS = 'ACTION_WARNING';
	const _ACTION_WITH_ERRORS = 'ACTION_ERRORS';
	const _ACTION_WITH_ERRORS_ONFALLBACK_CONTINUE = 'ACTION_ERRORS_ONFALLBACK_CONTINUE';
	const _ACTION_PENDING_CONFIRM ='ACTION_PENDING_CONFIRM';
	const _ACTION_FAIL = 'ACTION_FAIL';
	
	public const _TMS_CREATE_TASK = 1;
	public const _CONFIRMEMAIL = 2;
	public const _ACCEPT = 3;
	public const _SENDMAIL = 4;
	
	
	private $errors = array();
	private $warnings = array();
	protected $executed = false;
	protected $idAction;
	protected $idProcess;
	protected $actionType;
	protected $sequence;
	protected $enabled;
	protected $onFallBack;
	protected $actionName;

	/**
	 *
	 * @return mixed
	 */
	public function getIdAction(){

		return $this->idAction;
	}

	public function __construct(){
		$this->errors = array();
		$this->warnings = array();
	}

	/**
	 *
	 * @return string
	 */
	public function getActionType(){

		return $this->actionType;
	}

	/**
	 *
	 * @param mixed $idAction
	 */
	public function setIdAction($idAction){

		$this->idAction = $idAction;
	}

	/**
	 *
	 * @param mixed $actionType
	 */
	public function setActionType($actionType){

		$this->actionType = $actionType;
	}

	/**
	 * 
	 * @return int
	 */
	public function getSequence(){

		return $this->sequence;
	}

	/**
	 *
	 * @return bool
	 */
	public function getEnabled(){

		return $this->enabled;
	}

	/**
	 *
	 * @return bool
	 */
	public function getOnFallBack(){

		return $this->onFallBack;
	}

	/**
	 * 
	 * @return string
	 */
	public function getActionName(){

		return $this->actionName;
	}

	public function setSequence($sequence){

		$this->sequence = $sequence;
	}

	public function setEnabled($enabled){

		$this->enabled = $enabled;
	}

	public function setOnFallBack($onFallBack){

		$this->onFallBack = $onFallBack;
	}

	public function setActionName($actionName){

		$this->actionName = $actionName;
	}

	/**
	 * 
	 * @return int
	 */
	public function getIdProcess(){

		return $this->idProcess;
	}

	public function setIdProcess($idProcess){

		$this->idProcess = $idProcess;
	}
	
	/**
	 * 
	 * @return array
	 */
	public function getWarning(){
		return $this->warnings;
	}
	
	public function setWarning($warning){
		$this->warnings[] = $warning;
	}
	
	/**
	 * 
	 * @return array
	 */
	public function getError(){
		return $this->errors;
	}
	
	public function setError($error){
		$this->errors[] = $error;
	}
	
	public function hasWarnings(){
		return (count($this->warnings)>0);
	}
	
	public function hasErrors(){
		return (count($this->errors)>0);
	}
	
	public function isExecuted(){
		return $this->executed;
	}
	
	/**
	 * Executes the behavior of the task, this method should be overwritten by its subclasses. This superclass represents
	 * the accept task action hence, this method will accept a task.
	 * @param Task $task
	 * @return
	 */
	public function executeAction(Task $task){
		/*echo "DEFAULT EXECUTE ACTION<br />";
		$ai = AI::getInstance();
		$success = $ai->getHarvest()->acceptTask($task);
		
		if($success){
			$taskDAO = new TaskDAO();
			$taskDAO->updateTaskState($task, State::ACCEPTED, "Task was accept in ".$task->getDataSource()->getSourceName());
		}
		*/
		return true;
	}

	/**
	 *
	 *
	 * Given a $string, replaces the WILDCARDS with their task value.
	 *
	 * @param string $string
	 * @param Task $task
	 * @return string
	 */
	public function parseTaskWithWildCards($string, Task $task){

		$sourceDao = new SourceDAO();
		$source = $sourceDao->getSourceByName($task->getDataSource()->getSourceName());

		$wildCards[] = array(
			"alias" => "DATE_YMD",
			"value" => date("Ymd")
		);
		
		$wildCards[] = array(
				"alias" => "TASKID",
				"value" => $task->getIdClientetask()
		);

		$wildCards[] = array(
				"alias" => "TITLE",
				"value" => $task->getTitle()
		);
		$wildCards[] = array(
				"alias" => "TLANG",
				"value" => $task->getTargetsLanguage()[0]->getIdLanguage()
		); // Creo que aqui hay un problema con los idiomas...
		$wildCards[] = array(
				"alias" => "SLANG",
				"value" => $task->getSourceLanguage()->getIdLanguage()
		);
		// $wildCards[] = array("alias" => "SLANG", "value" => $task->getSourceLanguage()->getIdLanguage());
		$wildCards[] = array(
				"alias" => "STATE",
				"value" => $task->getState()->getStateName()
		);
		$wildCards[] = array(
				"alias" => "MESSAGE",
				"value" => $task->getMenssage()
		);
		$wildCards[] = array(
				"alias" => "TMSTASKURL",
				"value" => $task->getDataSource()->getTmsTaskUrl()
		);

		foreach($source->getAdditionalProperties() as $srcAddProperty) {
			/**@var TaskAditionalProperties as $taskProperty*/
			foreach($task->getAditionalProperties() as $taskProperty) {
				if ($taskProperty->getPropertyName() == $srcAddProperty->getPropertyName()) {
					$wildCards[] = array(
							"alias" => strtoupper($srcAddProperty->getPropertyName()),
							"value" => $taskProperty->getPropertyValue()
					);
					break;
				}
			}
		}

		foreach($wildCards as $wc) {
			$string = str_replace("%" . $wc["alias"] . "%", $wc["value"], $string);
		}

		return $string;
	}

	
	// public function jsonSerialize()
    // {
    //     return array(
    //         //"idAction" => $this->idAction,
    //         "actionName" => $this->actionName,
    //         "actionType" => $this->actionType,
    //         "enable" => $this->enabled,
    //         "executed" => $this->executed,
    //         "idProcess" => $this->idProcess,
    //         "onFallBack" => $this->onFallBack,
    //         "sequence" => $this->sequence,

    //     );
    // }
}
?>