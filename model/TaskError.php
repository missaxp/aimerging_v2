<?php

use model\Model;

include_once BaseDir . '/model/Model.php';
include_once BaseDir . '/model/TaskPredefinedError.php';
class TaskError extends model{

    private $uniqueSourceId;
    /**
     * @var String The origin class
     */
    private $location;
    /**
     * @var String to add related relevant data about the error
     */
    private $additionalData;
    /**
     * Valor de solo lectura, para escribir se usa el valor de la BD
     * @var DateTime of creation. The default in DB is current date so you don't need to setup.
     */
    private $timeStamp;
    /**
     * @var TaskPredefinedError error
     */
    private $predefinedError;

    /**
     * @return mixed
     */
    public function getUniqueSourceId()
    {
        return $this->uniqueSourceId;
    }

    /**
     * @param mixed $uniqueSourceId
     * @return TaskError
     */
    public function setUniqueSourceId($uniqueSourceId)
    {
        $this->uniqueSourceId = $uniqueSourceId;
        return $this;
    }

    /**
     * @return String
     */
    public function getLocation(): String
    {
        return $this->location;
    }

    /**
     * @param String $location
     * @return TaskError
     */
    public function setLocation(String $location)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return String
     */
    public function getAdditionalData()
    {
        return $this->additionalData;
    }

    /**
     * @param String $additionalData
     * @return TaskError
     */
    public function setAdditionalData(String $additionalData)
    {
        $this->additionalData = $additionalData;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTimeStamp()
    {
        return $this->timeStamp;
    }

    /**
     * @param \DateTime $timeStamp
     * @return TaskError
     */
    // se usa el valor por defecto de la BD
    public function setTimeStamp(\DateTime $timeStamp): TaskError
    {
        $this->timeStamp = $timeStamp;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPredefinedError()
    {
        return $this->predefinedError;
    }

    /**
     * @param mixed $predefinedError
     * @return TaskError
     */
    public function setPredefinedError($predefinedError)
    {
        $this->predefinedError = $predefinedError;
        return $this;
    }


}
