<?php

/**
 *
 * @author Ljimenez
 * @version 1.0
 *          created on 22 jun. 2018
 */
namespace model;

include_once BaseDir . '/model/Model.php';
class Source extends Model {
	public const MORAVIA_V2 = "MORAVIA_V2";
	public const MORAVIA_V1 = "MORAVIA_V1";
	public const JIRA = "JIRA";
	public const WELOCALIZE = "WELOCALIZE";
	public const NLG = "NLG";
	public const APPLE_LIOX_WS = "APPLE_LIOX_WS";
	public const APPLE_WELOCALIZE_WS = "APPLE_WELOCALIZE";
	public const SYMFONIEMORAVIA = "SymfonieMoravia";
    public const GOOGLE_SDL = "GOOGLE_SDL";
    public const ARGOS = "ARGOS";

        /**
     * @var string
     */
	private $className;

    /**
     * @return mixed
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * @param mixed $className
     */
    public function setClassName($className): void
    {
        $this->className = $className;
    }
	/**
	 * Identifier within of storage system
	 * 
	 * @var integer
	 */
	protected $idSource;
	
	/**
	 * @var string
	 */
	protected $sourceName;
	
	/**
	 * Priority to be executed
	 * 
	 * @var integer
	 */
	protected $priority;
	
	/**
	 * Boolena for decide if the files will be download
	 * 
	 * @var bool
	 */
	protected $download;
	
	/**
	 * 
	 * 
	 * @var string
	 */
	protected $description;
	
	/**
	 * Users to connect to the client portal
	 * 
	 * @var APIUser[]
	 */
	protected $users;
	
	/**
	 * @var SourceAditonalProperty[]
	 */
	protected $additionalProperties;

	/**
	 * Description of Source
	 * @return string
	 */
	public function getDescription(){

		return $this->description;
	}

	/**
	 *
	 * @param string $description
	 */
	public function setDescription($description){

		$this->description = $description;
	}

	/**
	 * 
	 * @return \model\SourceAditonalProperty[]
	 */
	public function getAdditionalProperties(){

		return $this->additionalProperties;
	}

	/**
	 * 
	 * @param SourceAditonalProperty[] $additionalProperties
	 */
	public function setAdditionalProperties($additionalProperties){

		$this->additionalProperties = $additionalProperties;
	}

	/**
	 *
	 * @return integer
	 */
	public function getPriority(){

		return $this->priority;
	}

	/**
	 *
	 * @return bool
	 */
	public function getDownload(){

		return $this->download;
	}

	/**
	 *
	 * @param integer $priority
	 */
	public function setPriority($priority){

		$this->priority = $priority;
	}

	/**
	 *
	 * @param bool $download
	 */
	public function setDownload($download){

		$this->download = $download;
	}

	/**
	 *
	 * @return APIUser[]
	 */
	public function getUsers(){

		return $this->users;
	}

	/**
	 *
	 * @param APIUser[] $users
	 */
	public function setUsers($users){

		$this->users = $users;
	}

	public function __construct(){
		$this->additionalProperties = array();
	}

	/**
	 *
	 * @return integer
	 */
	public function getIdSource(){

		return $this->idSource;
	}

	/**
	 *
	 * @return string
	 */
	public function getSourceName(){

		return $this->sourceName;
	}

	/**
	 *
	 * @param integer $idSource
	 */
	public function setIdSource($idSource){

		$this->idSource = $idSource;
	}

	/**
	 *
	 * @param string $sourceName
	 */
	public function setSourceName($sourceName){

		$this->sourceName = $sourceName;
	}
    /*public function jsonSerialize()
    {
        return array(
            //"idSource" => $this->idSource,
            "additionalProperties" => $this->additionalProperties,
            "description" => $this->description,
            "download" => $this->download,
            "priority" => $this->priority,
            "sourceName" => $this->sourceName,
            "users" => $this->users,
        );
    }*/
}
?>