<?php

use model\Model;

include_once BaseDir . '/model/Model.php';

class TaskPredefinedError extends model{
    /**
     * @var int Primary key auto increment in BD
     */
    private $idPredefinedError;
    /**
     * @var String name of the error
     */
    private $name;
    /**
     * @var String description of the error
     */
    private $description;

    /**
     * @return int
     */
    public function getIdPredefinedError(): int
    {
        return $this->idPredefinedError;
    }

    /**
     * @param int $idPredefinedError
     * @return TaskPredefinedError
     */
    public function setIdPredefinedError(int $idPredefinedError): TaskPredefinedError
    {
        $this->idPredefinedError = $idPredefinedError;
        return $this;
    }

    /**
     * @return String
     */
    public function getName(): String
    {
        return $this->name;
    }

    /**
     * @param String $name
     * @return TaskPredefinedError
     */
    public function setName(String $name): TaskPredefinedError
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return String
     */
    public function getDescription(): String
    {
        return $this->description;
    }

    /**
     * @param String $description
     * @return TaskPredefinedError
     */
    public function setDescription(String $description): TaskPredefinedError
    {
        $this->description = $description;
        return $this;
    }


}

