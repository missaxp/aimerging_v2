<?php

/**
* @author zroque
* @version 1.0
* created on 26 jun. 2018
*/
namespace model;
include_once BaseDir. '/model/Model.php';
class Process extends Model{
	const _ACTIONS_STATUS_SUCCESS = "ACTION_STATUS_SUCCESS";
	const _ACTIONS_STATUS_WARNING = "ACTION_STATUS_WARNING";
	const _ACTIONS_STATUS_ERROR = "ACTION_STATUS_ERROR";
	const _ACTIONS_STATUS_ERROR_ONFALLBACK_CONTINUE = "ACTION_STATUS_ERROR_ONFALLBACK_CONTINUE";
	
	
	protected $idProcess;
	protected $processName;
	protected $enabled;
	protected $description;
	protected $creator;
	protected $timeStamp;
	protected $idSource;
	
	/**
	 * Notification object for this process
	 * @var Notification
	 */
	protected $notification;
	/**
	 *
	 * @var Action[]
	 */
	protected $actions;
	
	/**
	 *
	 * @var Schedule
	 */
	protected $schedules;
	
	/**
	 *
	 * @var Rule[]
	 */
	protected $rules;
	
	/**
	 * 
	 * @var int
	 */
	protected $idCATTool;
	
	protected $CATToolSetup;

	public function __construct(){
		$this->actions = array();
		$this->schedules = array();
		$this->rules = array();

	}

	public function getIdProcess(){

		return $this->idProcess;
	}

	public function getProcessName(){

		return $this->processName;
	}

	public function getEnabled(){

		return $this->enabled;
	}

	public function getDescription(){

		return $this->description;
	}

	public function getCreator(){

		return $this->creator;
	}

	public function getTimeStamp(){

		return $this->timeStamp;
	}

	public function setIdProcess($idProcess){

		$this->idProcess = $idProcess;
	}

	public function setProcessName($processName){

		$this->processName = $processName;
	}

	public function setEnabled($enabled){

		$this->enabled = $enabled;
	}

	public function setDescription($description){

		$this->description = $description;
	}

	public function setCreator($creator){

		$this->creator = $creator;
	}

	public function setTimeStamp($timeStamp){

		$this->timeStamp = $timeStamp;
	}

	public function getIdSource(){

		return $this->idSource;
	}

	public function setIdSource($idSource){

		$this->idSource = $idSource;
	}

	public function getActions($onlyEnabled = false){
		$actions = array();
		if($onlyEnabled){
			foreach($this->actions as $action){
				if($action->getEnabled()){
					$actions[] = $action;
				}
			}
		}
		else{
			$actions = $this->actions;
		}
		return $actions;
	}

	/**
	 * 
	 * @return Notification
	 */
	public function getNotification(){

		return $this->notification;
	}

	/**
	 * Returns the current Schedule of the process
	 * @return \model\Schedule
	 */
	public function getSchedules(){

		return $this->schedules;
	}

    /**
     * @return Rule[]
     */
	public function getRules(){

		return $this->rules;
	}

	public function setActions($actions){

		$this->actions = $actions;
	}

	public function setNotification($notification){

		$this->notification = $notification;
	}

	/**
	 * Sets the schedule of the process, a single Schedule object must be given
	 * @param Schedule $schedules
	 */
	public function setSchedules($schedules){

		$this->schedules = $schedules;
	}

	public function setRules($rules){

		$this->rules = $rules;
	}

	/**
	 *
	 * @return number
	 */
	public function getIdCATTool(){

		return $this->idCATTool;
	}

	/**
	 *
	 * @param number $idCATTool
	 */
	public function setIdCATTool($idCATTool){

		$this->idCATTool = $idCATTool;
	}
	

	/**
     * @return mixed
     */
    public function getCATToolSetup()
    {
        return $this->CATToolSetup;
    }

	/**
     * @param mixed $CATToolSetup
     */
    public function setCATToolSetup($CATToolSetup)
    {
        $this->CATToolSetup = $CATToolSetup;
    }

	/**
	 * @author phidalgo
	 * Process the notifications
	 * 
	 * @return boolean
	 */
	public function processNotification(&$task, $memoqErrors = array()){
		// Mirar configuración notificacions
		$notificationConstruct = array("error" => array(), "warning" => array(), "changes" => array(), "memoqErrors"=>array());
		foreach($this->getActions() as $action) {
			if ($action->isExecuted()) {
				if ($this->notification->getWhenErrors() && $action->hasErrors()) {
					foreach ($action->getError() as $error){
						$notificationConstruct["error"][$action->getIdAction()][] = $error;
					}
				}
				if ($this->notification->getWhenWarnings() && $action->hasWarnings()) {
					foreach ($action->getWarning() as $warning){
						$notificationConstruct["warning"][$action->getIdAction()][] = $warning;
					}
				}
			} else {
				continue;
			}
		}
		$notificationConstruct["memoqErrors"] = $memoqErrors;
		if($this->notification != null){
			$this->notification->send($task, $notificationConstruct,"#" . $this->idProcess." - ".$this->processName);
		}
		
		return true;
	}

	/**
	 * @author phidalgo
	 * Return the overall status of the process actions.
	 * If any action has an error and fallBack is false, returns _ACTIONS_STATUS_ERROR.
	 * If any actions has an error and fallback is true, returns _ACTIONS_STATUS_ERROR_ONFALLBACK_CONTINUE
	 * If any action has a warning, returns _ACTIONS_STATUS_WARNING.
	 * If no errors or warnings, returns _ACTIONS_STATUS_SUCCESS
	 * @return string
	 */
	public function checkActionStatus(){
		$hasWarnings = false;
		foreach($this->actions as $action){
			if($action->hasErrors() && !$action->getOnFallBack()){
				return self::_ACTIONS_STATUS_ERROR;
			}else{
				if($action->hasErrors() && $action->getOnFallBack()){
					return self::_ACTIONS_STATUS_ERROR_ONFALLBACK_CONTINUE;
				}
			}
			if($action->hasWarnings()){
				return self::_ACTIONS_STATUS_WARNING;
			}
		}
		return self::_ACTIONS_STATUS_SUCCESS;
	}
	
	/**
	 * @author phidalgo
	 * Returns an array of WARNINGS, with key value array with the following structure:
	 * A key value pair with action_id = array of actions WARNINGS
	 * @return array
	 */
	public function getActionsWarnings(){
		$awarnings = array();
		foreach($this->actions as $action){
			foreach ($action->getWarning() as $warning){
				$awarnings[$action->getIdAction()][] = $warning;
			}
		}
		return $awarnings;
	}
	
	/**
	 * Returns an array of ERRORS, with key value array with the following structure:
	 * A key value pair with action_id = array of actions ERRORS
	 * @return array
	 */
	public function getActionsErrors(){
		$aerrors = array();
		foreach($this->actions as $action){
			foreach ($action->getError() as $error){
				$aerrors[$action->getIdAction()][] = $error;
			}
		}
		return $aerrors;
	}
	
	/**
	 * @author phidalgo
	 * Returns an string of a list of warnings by action Name
	 * @param boolean $html (default true)
	 * @return string
	 */
	public function getActionsWarningsAsString($html = true){
		$string = ($html? "<ul>":"");
		foreach($this->getActionsWarnings() as $action_id => $awarnings){
			if($html){
				$string .= "<li>".$this->getActionNameByID($action_id)."<ul></li>";
			}
			else{
				$string .= $this->getActionNameByID($action_id)."\r\n";
			}
			foreach($awarnings as $warning){
				if ($html) {
					$string .= "<li>" . $warning . "</li>";
				} else {
					$string .= "\t" . $warning . "\r\n";
				}
			}
			if($html){
				$string .= "</ul></li>";
			}
		}
		return $string;
	}
	
	/**
	 * @author phidalgo
	 * Returns an string of a list of errors by action Name.
	 * @param boolean $html (default true)
	 * @return string
	 */
	public function getActionsErrorsAsString($html = true){
		$string = ($html? "<ul>":"");
		foreach($this->getActionsErrors() as $action_id => $aerrors){
			if($html){
				$string .= "<li>".$this->getActionNameByID($action_id)."<ul></li>";
			}
			else{
				$string .= $this->getActionNameByID($action_id)."\r\n";
			}
			foreach($aerrors as $error){
				if ($html) {
					$string .= "<li>" . $error . "</li>";
				} else {
					$string .= "\t" . $error . "\r\n";
				}
			}
			if($html){
				$string .= "</ul></li>";
			}
		}
		return $string;
	}
	
	/**
	 * @author phidalgo 
	 * TODO:: Comprovar si hay otra manera mejor de obtener el action name.
	 * Given an action ID, returns action Name or false.
	 * @param integer $actionID
	 * @return string|boolean
	 */
	private function getActionNameByID($actionID){
		foreach($this->actions as $action){
			if($action->getIdAction()==$actionID){
				return $action->getActionName();
			}
		}
		return false;
	}
/*    public function jsonSerialize()
    {
        return get_object_vars($this);
    }*/
}
?>