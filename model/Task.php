<?php

/**
 *
 * @author Ljimenez
 * @version 1.0
 *          created on 21 jun. 2018
 */
namespace model;

use DateTime;
use common\exceptions\AIException;
use dataAccess\dao\TaskDAO;
use Functions;
use TaskError;

include_once BaseDir . '/model/Model.php';
include_once BaseDir . '/model/TaskError.php';
class Task extends Model {

    public const ONDEMAND = 'ON';
	public const INQUEUE = 'IN';

    /**
     * Unique Identifier of all object Task
     *
     * @var string
     */
    protected $uniqueSourceId;

    /**
     * Unique identifier of the properties collected from the task
     *
     * @var string
     */
    protected $uniqueTaskId;

    /**
     * Collection date of tasks 'Y/m/d H:i:s'
     *
     * @var string
     */
    protected $timeStamp;

    /**
     * Task identifier provided by the client
     *
     * @var string
     */
    protected $idClientetask;

    /**
     * Task name or title
     *
     * @var string
     */
    protected $title;

    /**
     *
     *
     * @var File[]
     */
    private $pos;

    /**
     * Description or instructions of task
     *
     * @var string
     */
    private $message;

    /**
     * Start date of task provided by the client
     *
     * @var string
     */
    private $startDate;

    /**
     * Due date of task provided by the client
     *
     * @var string
     */
    private $dueDate;

    /**
     * Identifier of the user with whom the task was accepted
     *
     * @var int
     */
    private $assignedUser;

    /**
     * Identifier of the project to which the task belongs
     *
     * @var string
     */
    private $projectManager;

    /**
     * identifier of the last process executed for this task
     *
     * @var int
     */
    private $idProcess;

    /**
     * identifier of the last action executed for this task
     *
     * @var int
     */
    private $idActions;

    /**
     * Identifier client's project
     *
     * @var string
     */
    protected $project;

    /**
     * Determines the number of tries for this task
     *
     * @author phidalgo
     * @var integer
     */
    protected $tries;

    /**
     * Array of change history.
     *
     * @author phidalgo
     * @var State[]
     */
    protected $statusHistory;
    /**
     * @var TaskError[]
     */
    private $taskErrors;

    /**
     * @return TaskError[]
     */
    public function getTaskErrors()
    {
        return $this->taskErrors;
    }

    /**
     * @param TaskError[] $taskErrors
     */
    public function setTaskErrors(array $taskErrors){
        $this->taskErrors = $taskErrors;
    }

    public function __construct(){
        $this->filesForReference = array();
        $this->filesToTranslate = array();
        $this->tries = 0;
        $this->statusHistory = array();
        $this->translationMemories = array();
        $state = new State(State::COLLECTING);
        $this->state = $state;
        $this->statusHistory[] = $state;
        $this->aditionalProperties = array();
    }

    /**
     * @param string $timeStamp
     * @param string $idClientTask
     * @param string $title
     * @param string $message
     * @param string $startDate
     * @param string $dueDate
     * @param string $project
     */
    public function construct( $timeStamp,  $idClientTask,  $title,  $message,  $startDate,  $dueDate,  $project){
        $this->idClientetask = $idClientTask;
        $this->title = $title;
        $this->dueDate = $dueDate;
        $this->message = $message;
        $this->startDate = $startDate;
        $this->project = $project;
    }

    /**
     * Aditionals porperties collected for this tasks
     *
     * @var TaskAditionalProperties[]
     */
    protected $aditionalProperties;

    /**
     * Reference files for task
     *
     * @var File[]
     */
    protected $filesForReference;

    /**
     * Files to trnanslation of task
     *
     * @var File[]
     */
    protected $filesToTranslate;

    /**
     * 	 *
     * @var Language
     */
    protected $sourceLanguage;

    /**
     *
     * @var Language[]
     */
    protected $targetsLanguage;

    /**
     *
     * @var Category
     */
    protected $category;

    /**
     *
     * @var DataSource
     */
    protected $dataSource;

    /**
     *
     * @var State
     */
    protected $state;

// 	/**
// 	 *
// 	 * @var Analysis[]
// 	 */
// 	protected $analysis;

    /***
     *
     * @var TM[]
     */
    protected $translationMemories;

    /***
     *
     * @var string
     */
    private $relatedOriginalData;

    /**
     * @return multitype:\model\File
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * @param multitype:\model\File  $pos
     */
    public function setPos($pos)
    {
        $this->pos = $pos;
    }

    /**
     *
     * @return TaskAditionalProperties[]
     */
    public function getAditionalProperties(){

        return $this->aditionalProperties;
    }

    /**
     * 	 *
     * @param TaskAditionalProperties[] $aditionalProperties
     */
    public function setAditionalProperties($aditionalProperties){

        $this->aditionalProperties = $aditionalProperties;
    }

// 	/**
// 	 *
// 	 * @return \model\Analysis[]
// 	 */
// 	public function getAnalysis(){

// 		return $this->analysis;
// 	}

// 	/**
// 	 *
// 	 * @param \model\Analysis[] $analysis
// 	 */
// 	public function setAnalysis($analysis){

// 		$this->analysis = $analysis;
// 	}

    /**
     *
     * @return File[]
     */
    public function getFilesForReference(){

        return $this->filesForReference;
    }

    public function getAttachments(){
        return array_merge($this->filesForReference,$this->filesToTranslate);
    }

    /**
     *
     * @return File[]
     */
    public function getFilesToTranslate(){

        return $this->filesToTranslate;
    }

    /**
     *
     * @param File[] $filesForReference
     */
    public function setFilesForReference($filesForReference){

        $this->filesForReference = $filesForReference;
    }

    /**
     *
     * @param File[] $filesToTranslate
     */
    public function setFilesToTranslate($filesToTranslate){

        $this->filesToTranslate = $filesToTranslate;
    }

    /**
     *
     * @return string
     */
    public function getProject(){

        return $this->project;
    }

    /**
     *
     * @return \model\Language
     */
    public function getSourceLanguage(){

        return $this->sourceLanguage;
    }

    /**
     *
     * @return Language[]
     */
    public function getTargetsLanguage(){

        return $this->targetsLanguage;
    }

    /**
     *
     * @param string $project
     */
    public function setProject($project){

        $this->project = $project;
    }

    /**
     *
     * @param \model\Language $sourceLanguage
     */
    public function setSourceLanguage($sourceLanguage){

        $this->sourceLanguage = $sourceLanguage;
    }

    /**
     *
     * @param Language[] $tagetLanguage
     */
    public function setTargetsLanguage($targetsLanguage){

        $this->targetsLanguage = $targetsLanguage;
    }

    /**
     *
     * @return string
     */
    public function getUniqueSourceId(){

        return $this->uniqueSourceId;
    }

    /**
     *
     * @return string
     */
    public function getUniqueTaskId(){

        return $this->uniqueTaskId;
    }

    /**
     *
     * @return string
     */
    public function getTimeStamp(){

		return $this->timeStamp;
	}

    /**
     *
     * @return string
     */
    public function getIdClientetask(){

        return $this->idClientetask;
    }

    /**
     *
     * @return string
     */
    public function getTitle(){

        return $this->title;
    }

    /**
     *
     * @return string
     */
    public function getMenssage(){

        return $this->message;
    }

    /**
     *
     * @return string
     */
    public function getStartDate(){
        if(gettype($this->startDate) == 'string'){
            $this->startDate = new DateTime($this->startDate);
        }
        return $this->startDate;
    }

    /**
     *
     * @return string
     */
    public function getDueDate(){
        if(gettype($this->dueDate) == 'string'){
            $this->dueDate = new DateTime($this->dueDate);
        }
        return $this->dueDate;
    }

    /**
     * Identifier of user that was used to collect the task
     *
     * @return int
     */
    public function getAssignedUser(){

        return $this->assignedUser;
    }

    /**
     *
     * @return string
     */
    public function getProjectManager(){

        return $this->projectManager;
    }

    /**
     *
     * @return Category
     */
    public function getCategory(){

        return $this->category;
    }

    /**
     *
     * @return DataSource
     */
    public function getDataSource(){

        return $this->dataSource;
    }

    /**
     *
     * @return State
     */
    public function getState(){

        return $this->state;
    }

    /**
     * Get number of tries a task has been tried to be processed
     *
     * @return integer
     */
    public function getTries(){

        return $this->tries;
    }

    /**
     * Get task status history
     *
     * @return \model\State[]
     */
    public function getStatusHistory(){

        return $this->statusHistory;
    }

    /**
     *
     * @param string $uniqueSourceId
     */
    public function setUniqueSourceId($uniqueSourceId){

        $this->uniqueSourceId = $uniqueSourceId;
    }

    /**
     *
     * @param string $uniqueTaskId
     */
    public function setUniqueTaskId($uniqueTaskId){

        $this->uniqueTaskId = $uniqueTaskId;
    }

    /**
     *
     * @param string $timeStamp
     */
    public function setTimeStamp($timeStamp){

        $this->timeStamp = $timeStamp;
    }

    /**
     *
     * @param string $idClientetask
     */
    public function setIdClientetask($idClientetask){

        $this->idClientetask = $idClientetask;
    }

    /**
     *
     * @param string $title
     */
    public function setTitle($title){

        $this->title = $title;
    }

    /**
     *
     * @param string $menssage
     */
    public function setMenssage($message){

        $this->message = $message;
    }

    /**
     *
     * @param string $startdate
     */
    public function setStartDate($startDate){
        if(gettype($startDate) == 'string'){
            $startDate = new DateTime($startDate);
        }
        $this->startDate = $startDate;
    }

    /**
     *
     * @param string $dueDate
     */
    public function setDueDate($dueDate){
        if(gettype($dueDate) == 'string'){
            $dueDate = new DateTime($dueDate);
        }
        $this->dueDate = $dueDate;
    }

    /**
     *
     * @param int $assignedUser
     */
    public function setAssignedUser($assignedUser){

        $this->assignedUser = $assignedUser;
    }

    /**
     *
     * @param string $projectManager
     */
    public function setProjectManager($projectManager){

        $this->projectManager = $projectManager;
    }

    /**
     *
     * @param Category $category
     */
    public function setCategory($category){

        $this->category = $category;
    }

    /**
     *
     * @param State $state
     */
    public function setState($state){
        if($this->state !== null){
            $this->statusHistory[]= $state;
        }
        $this->state = $state;

    }

    /**
     * Set number of tries a task has been tried to be processed
     *
     * @param integer $value
     */
    public function setTries($value){

        $this->tries = $value;
    }

    /**
     *
     * @return int
     */
    public function getIdProcess(){

        return $this->idProcess;
    }

    /**
     *
     * @return int
     */
    public function getIdActions(){

        return $this->idActions;
    }

    /**
     *
     * @param int $idProcess
     */
    public function setIdProcess($idProcess){

        $this->idProcess = $idProcess;
    }

    /**
     *
     * @param int $idActions
     */
    public function setIdActions($idActions){

        $this->idActions = $idActions;
    }

    /**
     * Set the task status history.
     *
     * @param State[] $data
     */
    public function setStatusHistory($data){

        $this->statusHistory = $data;
    }

    /**
     * Add this File to final at the end of the array FilesToTranslate
     *
     * @param File $file
     */
    public function addFileToTranslate(File $file){

        $this->filesToTranslate[] = $file;
    }

    /**
     * Add this File to final at the end of the array FilesForReference
     *
     * @param File $file
     */
    public function addFileForReference(File $file){

        $this->filesForReference[] = $file;
    }

    /**
     *
     * @param DataSource $dataSource
     */
    public function setDataSource($dataSource){

        $this->dataSource = $dataSource;
    }



    /**
     * @return TM[]
     */
    public function getTranslationMemories()
    {
        return $this->translationMemories;
    }

    /**
     * @param TM[] $translationMemories
     */
    public function setTranslationMemories($translationMemories)
    {
        $this->translationMemories = $translationMemories;
    }

    /**
     * @return string
     */
    public function getRelatedOriginalData()
    {
        return $this->relatedOriginalData;
    }

    /**
     * @param string $relatedOriginalData
     */
    public function setRelatedOriginalData($relatedOriginalData)
    {
        $this->relatedOriginalData = $relatedOriginalData;
    }

    /**
     * Generate and assign the uniqueSourceId
     *
     */
    public function generateUniqueSourceId(){

        $uniqueSourceId = '';

        $uniqueSourceId = $this->__toString();

        $this->uniqueSourceId = md5($uniqueSourceId);
    }

    /**
     * Generate and assign the uniqueTaskId
     *
     */
    public function generateUniqueTaskId(){

        $uniqueTaskId = '';

        $uniqueTaskId = $this->__toString();

        $this->uniqueTaskId = md5($uniqueTaskId);
    }

    public function taskCheck(){
        $checkup= true;

        $checkup = $checkup && isset($this->sourceLanguage);
        //echo 'Cumple $checkup && isset($this->sourceLanguage)? '.($checkup? "Sí":"No")."<br />";
        $checkup = $checkup && (count($this->targetsLanguage) > 0);
        $checkup = $checkup && $this->dataSource != null;
        //echo 'Cumple $checkup && (count($this->targetsLanguage) > 0)? '.($checkup? "Sí":"No")."<br />";

        //Comentado por PHIDALGO el 20180803 - Algunas tareas de moravia no traen ANAISIS. Tal vez haya que descartar las tareas, pero lo activo para recolectarlas.

        //$checkup = $checkup && (count($this->analysis) > 0);

        //echo 'Cumple $checkup && (count($this->analysis) > 0)? '.($checkup? "Sí":"No")."<br />";

        return $checkup;
    }

    	/**
	 *  Change the analysis according to the fuzzy relation  
	 *  @param array $file_relation
 	 *  @return		
     * 
     * 
	 * 
	 */
	public function overwriteFileRelations($file_relation){
        $file_relation = array_slice($file_relation, 2); 
        $compress_source_files = $file_relation["compress_source_files"];
        $compress_reference_files = $file_relation["compress_reference_files"];
        $file_relation = array_slice($file_relation, 0,3); 
        $refence_files = $this->filesForReference;
        $source_files = $this->filesToTranslate;
        $tm_files = $this->translationMemories;
        $ignored_files = array();
        $this->resetFilesToEmpty();
        $files = array(
            "reference_files"=>$refence_files,
            "source_files"=>$source_files,
            "tm_files"=>$tm_files,
        );
		foreach($file_relation as $key=>$value){
			$isZero = false;
			switch($value){
				case "Source files":
					$value = "filesToTranslate";
					break;
				case "Reference files":
					$value = "filesForReference";
                    break;
                case "TM files":
                    $value = "translationMemories";
                    break;
				case 'ignorar':
					$isZero = true;
					break;
				default:
					continue;
            }	
			if(!$isZero){
                $this->$value = array_merge($this->$value,$files[$key]);
			}else{
                $ignored_files = array_merge($ignored_files,$files[$key]);
				$this->$value = 0;
            }
        }
        if($compress_source_files){
            if($file_relation['source_files'] == "Reference files"){
                $this->compressFiles("Source","referenceFiles.zip");
            }else{
                $this->compressFiles("Source","sourceFiles.zip");
            }
        }
        if($compress_reference_files){
            if($file_relation['reference_files'] == "Source files"){
                $this->compressFiles("Reference","sourceFiles.zip");
            }else{
                $this->compressFiles("Reference","referenceFiles.zip");
            }
        }
        
        return $ignored_files;
    }
    
    public function resetFilesToEmpty(){
        $this->filesForReference=array();
        $this->filesToTranslate=array();
        $this->translationMemories=array();
    }

    public function compressFiles($type, $name){
        $validation = false;
        switch ($type){
            case File::REFERENCE:
                if(!empty($this->getFilesForReference())){
                    $dataFile = pathinfo($this->getFilesForReference()[0]->getPath(),PATHINFO_DIRNAME);
                    $zip = new \ZipArchive();
                    $path = $dataFile."/".$name;
                    if($zip->open($path,\ZIPARCHIVE::CREATE) === true){
                        foreach ($this->getFilesForReference() as $file){
                            $zip->addFile($file->getPath(),$file->getFileName());
                        }
                        $zip->close();
                        $taskDAO = new TaskDAO();
                        foreach ($this->getFilesForReference() as $file){
                            if($taskDAO->deleteFile($file->getIdFile())){
                                if(file_exists($file->getPath())){
                                    unlink($file->getPath());
                                }
                            } else {
                                $e = (new AIException("Cannot delete file"))->construct(__METHOD__, __NAMESPACE__, $func = func_get_args(), array("fileToDelete" => $file));
                                Functions::logException($e->getMessage(), Functions::ERROR, __CLASS__, $e);
                            }
                        }

                        $fileZip = new File();
                        $fileZip->construct(
                            $name,
                            "zip",
                            "aplication/zip",
                            "All refenrenceFiles",
                            Functions::currentDate(),
                            "",
                            "",
                            "TMS",
                            File::REFERENCE,
                            1,
                            filesize($path));
                        $fileZip->setPath($path);
                        $fileZip->setPathForDownload("");
                        $this->setFilesForReference(array());
                        $this->addFileForReference($fileZip);
                    }
                }
                break;
            case File::SOURCE:
                if(!empty($this->getFilesToTranslate())){
                    $dataFile = pathinfo($this->getFilesToTranslate()[0]->getPath(),PATHINFO_DIRNAME);
                    $zip = new \ZipArchive();
                    $path = $dataFile."/".$name;
                    if($zip->open($path,\ZIPARCHIVE::CREATE) === true){
                        foreach ($this->getFilesToTranslate() as $file){
                            $zip->addFile($file->getPath(),$file->getFileName());
                        }
                        $zip->close();
                        $taskDAO = new TaskDAO();
                        foreach ($this->getFilesToTranslate() as $file){
                            if($taskDAO->deleteFile($file->getIdFile())){
                                if(file_exists($file->getPath())){
                                    unlink($file->getPath());
                                }
                            } else {
                                $e = (new AIException("Cannot delete file"))->construct(__METHOD__, __NAMESPACE__, $func = func_get_args(), array("fileToDelete" => $file));
                                Functions::logException($e->getMessage(), Functions::ERROR, __CLASS__, $e);
                            }
                        }

                        $fileZip = new File();
                        $fileZip->construct(
                            $name,
                            "zip",
                            "aplication/zip",
                            "All refenrenceFiles",
                            Functions::currentDate(),
                            "",
                            "",
                            "TMS",
                            File::REFERENCE,
                            1,
                            filesize($path));
                        $fileZip->setPath($path);
                        $fileZip->setPathForDownload("");
                        $this->setFilesToTranslate(array());
                        $this->addFileToTranslate($fileZip);
                    }
                }
                    
                    break;
            default:
                break;
        }
        return $validation;
    }
}
?>