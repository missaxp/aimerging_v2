<?php

/**
 *
 * @author zroque
 * @version 1.0
 *          created on 22 jun. 2018
 */
namespace model;

use common\exceptions\AIException;
use dataAccess\dao\ActionCreateTMSResponseDAO;
use Functions;
use service\EmailManager;
use core\AI;
use dataAccess\dao\TaskDAO;
use TaskError;

include_once BaseDir . "/dataAccess/dao/ActionCreateTMSResponseDAO.php";
include_once BaseDir . "/model/NotificationMailAssets.php";

abstract class NotificationMailType{
    const SUCCESS = 0;
    const WARNING = 1;
    const ERROR = 2;
    const SEVERE = 3;
    const NEUTRAL = 4;

    public static function getTypeFromEnum(int $a){
        switch ($a){
            case self::SUCCESS: return "SUCCESS";
            case self::WARNING: return "WARNING";
            case self::ERROR: return "ERROR";
            case self::SEVERE: return "SEVERE";
            case self::NEUTRAL: return "NEUTRAL";
        }
        return "NEUTRAL";
    }
}
class Notification extends Model {
	protected $idProcess;
	protected $email;
	protected $whenProcess;
	protected $whenWarnings;
	protected $whenErrors;
	protected $whenRejected;
	protected $addBasicTaskInfo;
	protected $addSourceFile;
	protected $whenChanged;
    /**
     * @var int
     */
	private $mailType = NotificationMailType::SUCCESS;

	/**
	 * Array of the actions id and actions name
	 * $actionsIDName[] = array("id" => action_id, "name" => action_name);
	 *
	 * @var array
	 */
	protected $actionsIDName;

	public function __construct(){
        $this->email = "";
	}

	/**
	 *
	 * @return int
	 */
	public function getIdProcess(){

		return $this->idProcess;
	}

	/**
	 *
	 * @return string
	 */
	public function getEmail(){

		return $this->email;
	}

	/**
	 *
	 * @return bool
	 */
	public function getWhenProcess(){

		return $this->whenProcess;
	}

	/**
	 *
	 * @return bool
	 */
	public function getWhenWarnings(){

		return $this->whenWarnings;
	}

	/**
	 *
	 * @return bool
	 */
	public function getWhenErrors(){

		return $this->whenErrors;
	}

	/**
	 *
	 * @return bool
	 */
	public function getWhenRejected(){

		return $this->whenRejected;
	}

	/**
	 *
	 * @return bool
	 */
	public function getAddBasicTaskInfo(){

		return $this->addBasicTaskInfo;
	}

	/**
	 *
	 * @return bool
	 */
	public function getAddSourceFile(){

		return $this->addSourceFile;
	}

	public function setIdProcess($idProcess){

		$this->idProcess = $idProcess;
	}

	public function setEmail($email){

		$this->email = $email;
	}

	public function setWhenProcess($whenProcess){

		$this->whenProcess = $whenProcess;
	}

	public function setWhenWarnings($whenWarnings){

		$this->whenWarnings = $whenWarnings;
	}

	public function setWhenErrors($whenErrors){

		$this->whenErrors = $whenErrors;
	}

	public function setWhenRejected($whenRejected){

		$this->whenRejected = $whenRejected;
	}

	public function setAddBasicTaskInfo($value){

		$this->addBasicTaskInfo = $value;
	}

	public function setAddSourceFile($value){

		$this->addSourceFile = $value;
	}

	/**
	 *
	 * @return mixed
	 */
	public function getWhenChanged(){

		return $this->whenChanged;
	}

	/**
	 *
	 * @param mixed $whenChanged
	 */
	public function setWhenChanged($whenChanged){

		$this->whenChanged = $whenChanged;
	}

	/**
	 * Gets an array of actions id and actions name.
	 *
	 * @return array
	 * @return
	 */
	public function getActionsData(){

		return $this->actionsIDName;
	}

	/**
	 * Sets an array of actions id and actions name with the structure:
	 * array("id" => action_id, "name" => action_name);
	 *
	 * @param array $data
	 * @return
	 */
	public function setActionsData($data){

		$this->actionsIDName = $data;
	}

	/**
	 * Given an actionID returns the action name.
	 *
	 * @param integer $actionID
	 * @return string|boolean
	 * @return
	 */
	private function getActionNameById($actionID){

		foreach($this->actionsIDName as $actionName) {
			if ($actionName["id"] == $actionID) {
				return $actionName["name"];
			}
		}
		return false;
	}

    /**
     *
     * @param Task $task
     * @param array $messages
     * @param $process
     * @return boolean
     * @throws \Exception
     * @author phidalgo
     */
	public function send($task, $messages, $process){

		$sendMailToSetEmails = false;

		if ($this->getWhenErrors() && $task->getState()->getIdState() == State::ERROR || // Si hay que notificar cuando error y el estado de la tarrea es error ó...
		$this->getWhenWarnings() && $task->getState()->getIdState() == State::PROCESSEDWITHWARNING || // Si hay que notificar cuando warn y el estado de la tarea es warning ó...
		$this->getWhenProcess() && $task->getState()->getIdState() == State::PROCESSED || // Si hay que notificar cuando success y el estado de la tarea es processed
		$this->getWhenErrors() && $task->getState()->getIdState() == State::PROCESSED) { // Si hay que notificar cuando la tarea se procesó con errores
			$sendMailToSetEmails = true; // MArca que debe notificarse al usuario.
		}

		// Si no tenemos que enviar el email a lso usuarios especificados, porque no se cumplen requisitos, ni tampoco al administrador, entonces return true.
		if (! $sendMailToSetEmails && ! AI::getInstance()->getSetup()->sendNotificationEmailToAdmin) {
			return true;
		}

		$mail = new EmailManager();

        /**
		 * Array que contiene los emails a los cuales se enviará el correo-e.
		 *
		 * @var array $emails
		 */
        $emails = array();
        $testedMails = array();
        if ($sendMailToSetEmails) { // Si hay que enviar email al/los usuario/s especificados en la UI
            if(!empty($this->getEmail())){
                $emails = explode(",", trim($this->getEmail())); // Creamos un array desde la cadena separada por comas.
                if (!empty($emails)) { // Si no hemos podido crar el array és porque tal vez solo hay un correo y no lleva coma final.
                    foreach ($emails as $mailAddress) {
                        if (filter_var($mailAddress, FILTER_VALIDATE_EMAIL)) { // Comprovamos que la cadena sea un email.
                            $testedMails[] = trim($mailAddress);
                        } else {
                            $ex = (new AIException("Mail configured in process is not valid"))->construct(__METHOD__, __NAMESPACE__, $func = func_get_args(), array("mail" => $mailAddress));
                            Functions::logException($ex->getMessage(), Functions::WARNING, __CLASS__, $ex);
                        }
                    }
                }
                $emails = $testedMails;
            }
        }

		// Si en la configuración general (/declare.php) se ha especificado que to do correo de notificacion debe enviarse también al responsable de la aplicación, añadimos
		// su correo electronico.
		if (AI::getInstance()->getSetup()->sendNotificationEmailToAdmin) {
            if (filter_var(AI::getInstance()->getSetup()->mail_error_to, FILTER_VALIDATE_EMAIL)){
                $emails[] = AI::getInstance()->getSetup()->mail_error_to;
            } else {
                $ex = (new AIException("Mail configured in declare.php is not valid"))->construct(__METHOD__, __NAMESPACE__, $func = func_get_args(), array("mail" => AI::getInstance()->getSetup()->mail_error_to));
                Functions::logException($ex->getMessage(), Functions::WARNING, __CLASS__, $ex);
            }
		}
        $emailTitle = "";
		if(!isset($messages["error"])){
            $messages["error"] = array();
        }
        if(!isset($messages["warning"])){
            $messages["warning"] = array();
        }
        if(!isset($messages["memoqErrors"])){
            $messages["memoqErrors"] = array();
        }
        if(!isset($messages["changes"])){
        	$messages["changes"] = array();

        }else{
            if(!is_array($messages["changes"])){
                $messages["changes"] = array($messages["changes"]);
            }
        }

        if (count($messages["error"]) == 0) {
            if (count($messages["warning"]) == 0) {
                if(count($task->getTaskErrors()) > 0){
                    $emailTitle = "AI [WARNING]: " . $task->getDataSource()->getSourceName() . " Task: " . $task->getTitle() . " is processed with warnings.";
                    $this->mailType = NotificationMailType::WARNING;
                } else {
                    $emailTitle = "AI [SUCCESS]: " . $task->getDataSource()->getSourceName() . " Task: " . $task->getTitle() . " is processed.";
                    $this->mailType = NotificationMailType::SUCCESS;
                }
            } else {
                $emailTitle = "AI [WARNING]: " . $task->getDataSource()->getSourceName() . " Task: " . $task->getTitle() . " is processed with warnings.";
                $this->mailType = NotificationMailType::WARNING;
            }
            if (count($messages["changes"]) > 0) {
                $emailTitle = "AI [TASK CHANGED]" . $task->getDataSource()->getSourceName() . "Task: " . $task->getTitle() . " has changes for you to review";
            }
        } else {
            if ($task->getState()->getIdState() == State::PROCESSED || $task->getState()->getIdState() == State::PROCESSEDWITHWARNING) {
                $emailTitle = "AI [PROCESSED WITH ERRORS]: " . $task->getDataSource()->getSourceName() . " Task: " . $task->getTitle() . " some actions failed or had errors.";
                $this->mailType = NotificationMailType::ERROR;
            } elseif ($task->getTries() >= AI::getInstance()->getSetup()->numberOfTries) {
                $emailTitle = "AI [ERROR]: " . $task->getDataSource()->getSourceName() . " Task: " . $task->getTitle() . " can not be processed.";
                $this->mailType = NotificationMailType::SEVERE;
            } else {
                $emailTitle = "AI [ERROR]: " . $task->getDataSource()->getSourceName() . " Task: " . $task->getTitle() . " can not be processed. We will retry again";
                $this->mailType = NotificationMailType::SEVERE;
            }
        }
        if(AI::getInstance()->isDev()){
            $emailTitle = "[Development] - " . $emailTitle;
        }


        try {
            $startDate = $task->getStartDate();
            $dueDate = $task->getDueDate();
            $startDate->setTimezone(new \DateTimeZone(AI::getInstance()->getSetup()->timeZone_TMS));
            $dueDate->setTimezone(new \DateTimeZone(AI::getInstance()->getSetup()->timeZone_TMS));
        } catch(\Exception $e) {
            $startDate = $task->getStartDate();
            $dueDate = $task->getDueDate();
        }
        $startDate = is_string($startDate) ? $startDate : $startDate->format("d/m/Y H:i");
        $dueDate = is_string($dueDate) ? $dueDate : $dueDate->format("d/m/Y H:i");
        $mailColor = NotificationMailAssets::getColorByMailType($this->mailType);
        $bodyConstruct = "<html lang='en'><head>";
        $bodyConstruct .= "</head><body style='background-color: #ddd'>";
        $bodyConstruct .= "<center><div class=\"container\" width='600px' style='width: 600px;display: block;margin: 0 auto;background-color: #fff; text-align: left'><p class=\"header\" style='text-align: center;background-color: $mailColor;margin: 0;'>";
        $bodyConstruct .= "<img style='padding: 5px;background-color: white;border-radius: 50%;position: relative;top: 25px;' width='50px' height='50px' src='" . NotificationMailAssets::getImageByMailType($this->mailType) . "'>";
        $bodyConstruct .= "</p><div class=\"wrapper\" style='font-family: Arial, serif;padding: 1em;'>";

        if ($this->getWhenProcess() && count($messages["error"]) == 0 && count($messages["warning"]) == 0 ) {
            if(empty($task->getTaskErrors())){
                $bodyConstruct .= "<h2 style='text-align: center;'>Task processed successfully</h2>";
            } else {
                $bodyConstruct .= "<h2 style='text-align: center;'>Task processed with warnings</h2>";
            }
        } elseif ($this->getWhenProcess() && count($messages["error"]) == 0){
            $bodyConstruct .= "<h2 style='text-align: center;'>Task processed with warnings</h2>";
        }

        if ($this->getWhenErrors() && count($messages["error"]) > 0) {
            $bodyConstruct .= "<h2 style='text-align: center;'>This task executed all the configured actions however some of them failed or had errors</h2>";
        }

        $actionCreateTmsResponseDao = new ActionCreateTMSResponseDAO();
        $tmsLink = $actionCreateTmsResponseDao->getTMSResponseFromTask($task);
        $tmsLink = "http://idcp.idisc.es/tasca.asp?idtasca=" . $tmsLink;
        if(AI::getInstance()->isDev()){
            $tmsLink = str_replace("idcp.idisc.es", "traduc.idisc.es", $tmsLink);
        }
        if(!empty($tmsLink)){
            $bodyConstruct .= "<p style='text-align: center;' id=\"idcpLink\">";
            $bodyConstruct .= "<span style=\"background-color: $mailColor;font-weight: bold;display: inline-block;padding: .5em;border-radius: 3px;\">";
            $bodyConstruct .= "<a style='color: white;text-decoration: none;' href=\"$tmsLink\">IDCP link</a>";
            $bodyConstruct .= "</span>";
            $bodyConstruct .= "</p>";
        }

        $bodyConstruct .= "<h4 style='margin: 1.5em 0 1em 0;'>AI Task Management properties:</h4>";
        $bodyConstruct .= "<div class=\"taskProperties\" style='text-align: center;'>";
        $bodyConstruct .= "<table style='display: inline-block;border-spacing: 10px 5px;'>";
        $bodyConstruct .= "<tr><td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: .8em;'>Processed by: </td><td>" . $process . "</td></tr>";
        $bodyConstruct .= "<tr><td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: .8em;'>Status: </td><td>" . $this->getReadableStatus($task->getState()) . "</td></tr>";
//        $bodyConstruct .= "<tr><td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: .8em;'>usid: </td><td>" . $task->getUniqueSourceId() . "</td></tr>";
//        $bodyConstruct .= "<tr><td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: .8em;'>utid: </td><td>" . $task->getUniqueTaskId() . "</td></tr>";
        $bodyConstruct .= "</table>";
        $bodyConstruct .= "</div>";



        if ($this->getAddBasicTaskInfo()) {
            $bodyConstruct .= "<h4>Task properties:</h4>";

            $bodyConstruct .= "<div class=\"taskProperties\" style='text-align: center;'>";
            $bodyConstruct .= "<table style='display: inline-block;border-spacing: 10px 5px;'>";
            $bodyConstruct .= "<tr><td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: .8em;'>Title:</td><td>" . $task->getTitle() . "</td></tr>";
            $bodyConstruct .= "<tr><td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: .8em;'>Start date:</td><td>" . $startDate . "</td></tr>";
            $bodyConstruct .= "<tr><td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: .8em;'>Due date:</td><td>" . $dueDate . "</td></tr>";
            $bodyConstruct .= "<tr><td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: .8em;'>Source language:</td><td>" . $task->getSourceLanguage()->getLanguageName() . "</td></tr>";

            $targets = "";
            foreach($task->getTargetsLanguage() as $target) {
                $targets .= $target->getLanguageName() . "<br>";
            }
            $bodyConstruct .= "<tr><td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: .8em;'>Target language:</td><td>" . $targets . "</td></tr>";
            $bodyConstruct .= "</table>";
            $bodyConstruct .= "</div>";
            foreach($task->getTargetsLanguage() as $target) {
                $bodyConstruct .= "<h4>Word count for " . $target->getLanguageName() . ":</h4>";
                $bodyConstruct .= "<div class=\"taskProperties\" style='text-align: center;'>";
                $bodyConstruct .= "<table style='display: inline-block;border-spacing: 0;'>";
                $bodyConstruct .= $this->getRowsFromAnalisys($target->getAnalysis());
                $bodyConstruct .= "</table>";
                $bodyConstruct .= "</div>";
            }
        }
        if ($this->getWhenWarnings() && count($task->getTaskErrors()) > 0) {
            $bodyConstruct .= $this->constructHarvestErrors($task->getTaskErrors());
        }


		if ($this->getWhenWarnings() && count($messages["warning"]) > 0) {
            $bodyConstruct .= $this->constructShowNotices($messages["warning"], "Warnings");
        }

		if ($this->getWhenErrors() && count($messages["error"]) > 0) {
			$bodyConstruct .= $this->constructShowNotices($messages["error"], "Errors");
		}
		if ($this->getWhenErrors() && count($messages["memoqErrors"]) > 0) {
			$bodyConstruct .= $this->constructShowNotices($messages["memoqErrors"], "MemoQ Errors", true);
		}
		if ($this->getWhenChanged()) {
			if (isset($messages["changes"]) && count($messages["changes"]) > 0) {
                $bodyConstruct .= "<h4>Task properties changes</h4>";
				$bodyConstruct .= implode(', ',$messages["changes"]);
			}
		}


        $taskDAO = new TaskDAO();
        $history = $taskDAO->getTaskStatusHistory($task->getUniqueSourceId());
        $bodyConstruct .= "<h4>Task change History</h4>";
        $bodyConstruct .= "<div class=\"taskProperties\"><div class=\"changeHistory\"><ol>";
        foreach($history as $state) {
            $bodyConstruct .= "<li style='font-size: .8em;'><b>" . $state->getTimeStamp()->format("d/m/Y H:i") . ": " . $this->getReadableStatus($state) . "</b> - " . $state->getDescription() . "</li>";
        }
        $bodyConstruct .= "</ol></div>";
        $bodyConstruct .= "<p style='text-align: right; font-size: .8em;'><strong>USID: </strong>" . $task->getUniqueSourceId() . "</p>";
        $bodyConstruct .= "<p></p>";
        $bodyConstruct .= "<p style='font-size: .8em; text-align: center;'><strong>System notification sent by AI Automatic Task Management System<br><span style='color:red;'>i</span>DISC Information Technologies ".date("Y")."</strong></p>";
        $bodyConstruct .= "</div></center>";
        $bodyConstruct .= "</body></html>";


        if ($this->getAddSourceFile()) {
            foreach($task->getFilesToTranslate() as $file) {
                $mail->addAttachment($file->getPath(), $file->getFileName());
            }
        }

        if(!is_array($emails) || count($emails) === 0){
            $ex = (new AIException("There are no valid email address defined in process or in declare.php"))->construct(__METHOD__, __NAMESPACE__, $func = func_get_args());
            Functions::logException($ex->getMessage(), Functions::WARNING, __CLASS__, $ex);
            return false;
        }
		foreach($emails as $userEmail) {
			$mail->addAddress($userEmail, $userEmail);
		}
		$mail->sendMail("", $emailTitle, $bodyConstruct);

		return true;
	}

	/**
	 * Given an array of warnings of errors, constructs an HTML Body.
	 *
	 * @param array $notices
	 * @param string $noticeName
	 * @return string
	 */
	private function constructShowNotices($notices, $noticeName, $isMemoq = false){
        if(!is_array($notices) || empty($notices)) {
            return "";
        }
		$bodyConstruct = "";
		$bodyConstruct .= "<h4>$noticeName</h4>";
		$bodyConstruct .= "<div class=\"taskProperties\">";
		$bodyConstruct .= "<div class=\"changeHistory\">";
        $bodyConstruct .= "<ul>";
        if ($isMemoq) {
			foreach($notices as $notice){
				$bodyConstruct .= "<li>" . $notice . "</li>";
			}
		} else {
			foreach($notices as $actionid => $notice) {
				$actionName = $this->getActionNameById($actionid);
				if ($actionName === false) {
					$actionName = $actionid;
				}
				foreach($notice as $not) {
					$bodyConstruct .= "<li><strong>Action: " . $actionName . "</strong> - " . $not . "</li>";
				}
            }
        }
        $bodyConstruct .= "</ul></div></div>";

        return $bodyConstruct;
	}

    /**
     * Given an array of warnings of errors, constructs an HTML Body.
     *
     * @param TaskError[] $taskErrors
     * @return string
     */
    private function constructHarvestErrors(array $taskErrors){

        if(!is_array($taskErrors) || empty($taskErrors)) {
            return "";
        }
        $bodyConstruct = "";
        $bodyConstruct .= "<h4>Harvest warnings</h4>";
        $bodyConstruct .= "<div class=\"taskProperties\">";
        $bodyConstruct .= "<div class=\"changeHistory\">";
        $bodyConstruct .= "<ul>";
        foreach($taskErrors as $error) {
            $predefinedError = $error->getPredefinedError();
            if(isset($predefinedError)){
                $bodyConstruct .= "<li><strong>" . $error->getTimeStamp()->format("d/m/Y H:i") . ": " . $predefinedError->getName() . "</strong> - " . $predefinedError->getDescription() . "</li>";
            } else {
                $bodyConstruct .= "<li><strong>" . $error->getTimeStamp()->format("d/m/Y H:i") . ": </strong> - " . $error->getAdditionalData(). "</li>";
            }
        }
        $bodyConstruct .= "</ul></div></div>";

        return $bodyConstruct;
    }

    private function getReadableStatus(State $state){
	    switch ($state->getIdState()){
            case State::COLLECTED: return "Collected";
            case State::FILTERED: return "Filtered";
            case State::EXECUTINGACTIONS: return "Executing actions";
            case State::PROCESSED: return "Processed";
            case State::REJECTED: return "Rejected";
            case State::RETRY: return "Retry";
            case State::PENDINGCONFIRMATION: return "Pending confirmation";
            case State::PROCESSEDWITHWARNING: return "Processed with warnings";
            case State::ERROR: return "Error";
            case State::CONFIRMED: return "Confirmed";
            case State::REJECTEDBYCONFIRMTIMEOUT: return "Rejected by confirmation out of time";
            case State::COLLECTING: return "Collecting";
            case State::PREPROCESSING: return "Preprocessing";
            case State::PREPROCESSED: return "Preprocessed";
        }
        return "Unkown state";
    }

    private function getRowsFromAnalisys(Analysis $analysis){
        $string = "";


        $string .= "<tr style='background-color: #ffffff; border-bottom: 1px solid black;'>";
        $string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "ICE:" . "</td>";
        $string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getPercentage_101() . "</td>";
        $string .= "</tr>";

        $string .= "<tr style='background-color: #ededed; border-bottom: 1px solid black;'>";
        $string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "Repetitions:" . "</td>";
        $string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getRepetition() . "</td>";
        $string .= "</tr>";

        $string .= "<tr style='background-color: #ffffff; border-bottom: 1px solid black;'>";
        $string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "100%:" . "</td>";
        $string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getPercentage_100() . "</td>";
        $string .= "</tr>";

        $string .= "<tr style='background-color: #ededed; border-bottom: 1px solid black;'>";
        $string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "95%:" . "</td>";
        $string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getPercentage_95() . "</td>";
        $string .= "</tr>";

        $string .= "<tr style='background-color: #ffffff; border-bottom: 1px solid black;'>";
        $string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "85%:" . "</td>";
        $string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getPercentage_85() . "</td>";
        $string .= "</tr>";


        $string .= "<tr style='background-color: #ededed; border-bottom: 1px solid black;'>";
        $string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "75%:" . "</td>";
        $string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getPercentage_75() . "</td>";
        $string .= "</tr>";

        $string .= "<tr style='background-color: #ffffff; border-bottom: 1px solid black;'>";
        $string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "50%:" . "</td>";
        $string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getPercentage_50() . "</td>";
        $string .= "</tr>";


        $string .= "<tr style='background-color: #ededed; border-bottom: 1px solid black;'>";
        $string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "Not Match:" . "</td>";
        $string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getNotMatch() . "</td>";
        $string .= "</tr>";

        $string .= "<tr style='background-color: #ffffff; border-bottom: 1px solid black;'>";
        $string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "Minutes:" . "</td>";
        $string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getMinute() . "</td>";
        $string .= "</tr>";

        $string .= "<tr style='background-color: #ededed; border-bottom: 1px solid black;'>";
        $string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "Total words:" . "</td>";
        $string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getTotalWords() . "</td>";
        $string .= "</tr>";

        $string .= "<tr style='background-color: #ffffff; border-bottom: 1px solid black;'>";
        $string .= "<td style='font-weight: 600;text-align: right;text-transform: uppercase;font-size: 12px; padding: 10px;'>" . "Weighted words:" . "</td>";
        $string .= "<td style='font-size: 12px; padding: 10px; 10px 0 0;'>" . $analysis->getWeightedWord() . "</td>";
        $string .= "</tr>";

        return $string;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
?>