<?php

/**
 *
 * @author zroque
 * @version 1.0
 * created on 21 jun. 2018
 */
namespace model;

include_once BaseDir. '/model/Model.php';

class File extends Model{
	
	const SOURCE = 'Source';
	const ANALYSIS = 'Analysis';
	const REFERENCE = 'Reference';
	const PO = "PO";

    /**
     * @var boolean
     */
	private $isDelete;

    /**
     * @return bool
     */
    public function isDelete(): bool
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete(bool $isDelete): void
    {
        $this->isDelete = $isDelete;
    }

	/**
	 * @var string
	 */
	protected $fileName;
	/**
	 * @var string
	 */
	protected $fileNameExtension;
	/**
	 * @var string
	 */
	protected $mimeType;
	
	/**
	 * Description of the file if it brings
	 *
	 * @var string
	 */
	protected $description;
	
	/**
	 * @var string
	 */
	private $timeStamp;
	
	/**
	 * Path within of API, TMS, FTP.
	 *
	 * @var string
	 */
	protected $sourcePath;
	
	/**
	 *
	 * @var integer
	 */
	private $size;
	
	/**
	 * Path where was download
	 *
	 * @var string
	 */
	protected $path;
	
	/**
	 *
	 * @var string
	 */
	protected $checksum;
	
	/**
	 * Identifier within of storage system
	 *
	 * @var integer
	 */
	protected $idFile;
	
	/**
	 * Origin of the file (FTP, email attachment, API, etc.)
	 *
	 * @var string
	 */
	protected $source;
	
	/**
	 * Analysis, Source, Reference, Other
	 *
	 * @var string
	 */
	protected $fileType;
	
	/**
	 * File has been download
	 *
	 * @var boolean
	 */
	protected $successfulDownload;
	
	/**
	 * Path for download of Source
	 *
	 * @var string
	 */
	protected $pathForDownload;
	
	/**
	 * Asynchronous download token
	 *
	 * @var string
	 */
	private $asyncHash;
	
	/**
	 * @return boolean
	 */
	public function isSuccessfulDownload(){
		return $this->successfulDownload;
	}
	
	/**
	 * @return string
	 */
	public function getAsyncHash(){
		return $this->asyncHash;
	}
	
	/**
	 * @param string $asyncHash
	 */
	public function setAsyncHash($asyncHash)
	{
		$this->asyncHash = $asyncHash;
	}
	
	public function __construct(){
		$this->asyncHash = null;
		$this->isDelete = false;
	}
	
	/**
	 * Simulates an overloaded constructor
	 *
	 * @param string $fileName
	 * @param string $fileNameExtension
	 * @param string $mimeType
	 * @param string $description
	 * @param string $timeStamp
	 * @param string $sourcePath
	 * @param string $checksum
	 * @param string $source
	 * @param string $fileType
	 * @param bool $successfulDownload
	 * @param int $size
	 */
	public function construct( $fileName,  $fileNameExtension,  $mimeType, $description, $timeStamp,  $sourcePath,  $checksum, $source, $fileType, $successfulDownload,  $size){
		$this->successfulDownload = false;
		$this->fileName = $fileName;
		$this->fileNameExtension = $fileNameExtension;
		$this->mimeType = $mimeType;
		$this->description = $description;
		$this->timeStamp = $timeStamp;
		$this->sourcePath = $sourcePath;
		$this->checksum = $checksum;
		$this->source = $source;
		$this->fileType = $fileType;
		$this->successfulDownload = $successfulDownload;
		$this->size = $size;
	}
	
	/**
	 * @return boolean
	 */
	public function getSuccessfulDownload() {
		return $this->successfulDownload;
	}
	
	/**
	 * @return string
	 */
	public function getPathForDownload() {
		return $this->pathForDownload;
	}
	
	/**
	 * @param boolean $successfulDownload
	 */
	public function setSuccessfulDownload($successfulDownload) {
		$this->successfulDownload = $successfulDownload;
	}
	
	/**
	 * @param string $pathForDownload
	 */
	public function setPathForDownload($pathForDownload) {
		$this->pathForDownload = $pathForDownload;
	}
	
	/**
	 * @return string
	 */
	public function getFileType() {
		return $this->fileType;
	}
	
	/**
	 * @param string $fileType
	 */
	public function setFileType($fileType) {
		$this->fileType = $fileType;
	}
	/**
	 * @return string
	 */
	public function getSource() {
		return $this->source;
	}
	
	/**
	 * @param string $source
	 */
	public function setSource($source) {
		$this->source = $source;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getFileName() {
		return $this->fileName;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getFileNameExtension() {
		return $this->fileNameExtension;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getMimeType() {
		return $this->mimeType;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getTimeStamp() {
		return $this->timeStamp;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getSourcePath() {
		return $this->sourcePath;
	}
	
	/**
	 *
	 * @return integer
	 */
	public function getSize() {
		return $this->size;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getPath() {
		return $this->path;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getChecksum() {
		return $this->checksum;
	}
	
	/**
	 *
	 * @return integer
	 */
	public function getIdFile() {
		return $this->idFile;
	}
	
	/**
	 *
	 * @param string $fileName
	 */
	public function setFileName($fileName) {
		$this->fileName = $fileName;
	}
	
	/**
	 *
	 * @param string $fileNameExtension
	 */
	public function setFileNameExtension($fileNameExtension) {
		$this->fileNameExtension = $fileNameExtension;
	}
	
	/**
	 *
	 * @param string $mimeType
	 */
	public function setMimeType($mimeType) {
		$this->mimeType = $mimeType;
	}
	
	/**
	 *
	 * @param string $description
	 */
	public function setDescription($description) {
		$this->description = $description;
	}
	
	/**
	 *
	 * @param string $timeStamp
	 */
	public function setTimeStamp($timeStamp) {
		$this->timeStamp = $timeStamp;
	}
	
	/**
	 *
	 * @param string $sourcePath
	 */
	public function setSourcePath($sourcePath) {
		$this->sourcePath = $sourcePath;
	}
	
	/**
	 *
	 * @param integer $size
	 */
	public function setSize($size) {
		$this->size = $size;
	}
	
	/**
	 *
	 * @param string $path
	 */
	public function setPath($path) {
		$this->path = $path;
	}
	
	/**
	 *
	 * @param string $checksum
	 */
	public function setChecksum($checksum) {
		$this->checksum = $checksum;
	}
	
	/**
	 *
	 * @param integer $idFile
	 */
	public function setIdFile($idFile) {
		$this->idFile = $idFile;
	}
	
	/**
	 * Validate if file was download correct
	 *
	 * @return boolean true if file download is correct or false else
	 */
	 public function validateDownload(){
	 	return file_exists($this->path) && filesize($this->path) > 0;
	 	// return true;
	 }
/*
    public function jsonSerialize()
    {
        return array(
            //"idFile" => $this->idFile,
            "checksum" => $this->checksum,
            "description" => $this->description,
            "fileName" => $this->fileName,
            "fileNameExtension" => $this->fileNameExtension,
            "fileType" => $this->fileType,
            "mimeType" => $this->mimeType,
            //"path" => $this->path,
            //"pathForDownload" => $this->pathForDownload,
            "source" => $this->source,
            //"sourcePath" => $this->sourcePath, // Some sources have different url between logins
            "successfulDownload" => $this->successfulDownload,
        );
    }*/
}
?>