<?php

/**
* @author Ljimenez
* @version 1.0
* created on 22 jun. 2018
*/
namespace model;

class SourceAditonalProperty extends Model{
	
	/**
	 * Identifier within of storage system
	 * 
	 * @var integer
	 */
	protected $idSourceAditionalProperty;
	
	/**
	 * Identifier of the source to which it belongs
	 * 
	 * @var integer
	 */
	protected $idSource;
	
	/**
	 * 
	 * @var string
	 */
	protected $propertyName;
	
	/**
	 * 
	 * @var mixed
	 */
	protected $propertyValue;

	/**
	 * @return integer
	 */
	public function getIdSourceAditionalProperty() {
		return $this->idSourceAditionalProperty;
	}

	/**
	 * @return integer
	 */
	public function getIdSource() {
		return $this->idSource;
	}

	/**
	 * @return string
	 */
	public function getPropertyName() {
		return $this->propertyName;
	}

	/**
	 * @return mixed
	 */
	public function getPropertyValue() {
		return $this->propertyValue;
	}

	/**
	 * @param integer $idSourceAditionalProperty
	 */
	public function setIdSourceAditionalProperty($idSourceAditionalProperty) {
		$this->idSourceAditionalProperty = $idSourceAditionalProperty;
	}

	/**
	 * @param integer $idSource
	 */
	public function setIdSource($idSource) {
		$this->idSource = $idSource;
	}

	/**
	 * @param string $propertyName
	 */
	public function setPropertyName($propertyName) {
		$this->propertyName = $propertyName;
	}

	/**
	 * @param string $propertyValue
	 */
	public function setPropertyValue($propertyValue) {
		$this->propertyValue = $propertyValue;
	}

/*    public function jsonSerialize()
    {
        return get_object_vars($this);
    }*/
	
}
?>