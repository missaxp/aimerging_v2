<?php

namespace model;

class Preprocess {
	/**
	 * *
	 *
	 * @var string
	 */
	protected $preprocessName;

	/**
	 * *
	 *
	 * @var integer
	 */
	protected $idPreprocess;

	/**
	 * *
	 *
	 * @var integer
	 */
	protected $idSource;

	/**
	 * *
	 *
	 * @var bool
	 */
	protected $enabled;

	/**
	 * *
	 *
	 * @var string
	 */
	protected $codeToExecute;

	/**
	 * *
	 *
	 * @var Rule[]
	 */
	protected $rules;

	public function __construct(){
		$this->codeToExecute = "";
		$this->rules = array();
	}

	/**
	 *
	 * @return string
	 */
	public function getPreprocessName(){

		return $this->preprocessName;
	}

	/**
	 *
	 * @param string $preprocessName
	 */
	public function setPreprocessName($preprocessName){

		$this->preprocessName = $preprocessName;
	}

	/**
	 *
	 * @return number
	 */
	public function getIdPreprocess(){

		return $this->idPreprocess;
	}

	/**
	 *
	 * @param number $idPreprocess
	 */
	public function setIdPreprocess($idPreprocess){

		$this->idPreprocess = $idPreprocess;
	}

	/**
	 *
	 * @return number
	 */
	public function getIdSource(){

		return $this->idSource;
	}

	/**
	 *
	 * @param number $idSource
	 */
	public function setIdSource($idSource){

		$this->idSource = $idSource;
	}

	/**
	 *
	 * @return boolean
	 */
	public function isEnabled(){

		return $this->enabled;
	}

	/**
	 *
	 * @param boolean $enabled
	 */
	public function setEnabled($enabled){

		$this->enabled = $enabled;
	}

	/**
	 *
	 * @return string
	 */
	public function getCodeToExecute(){

		return $this->codeToExecute;
	}

	/**
	 *
	 * @param string $codeToExecute
	 */
	public function setCodeToExecute($codeToExecute){

		$this->codeToExecute = $codeToExecute;
	}

	/**
	 *
	 * @return Rule[]
	 */
	public function getRules(){

		return $this->rules;
	}

	/**
	 *
	 * @param \model\Rule[] $rules
	 */
	public function setRules($rules){

		$this->rules = $rules;
	}
/*    public function jsonSerialize()
    {
        return get_object_vars($this);
    }*/
}

