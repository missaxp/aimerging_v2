<?php

/**
* @author Ljimenez
* @version 1.0
* created on 21 jun. 2018
*/
namespace model;

class State extends Model{
	
	/**
	 * This state will be set when a task has been collected from a Source
	 * 
	 * @var integer
	 */
	const COLLECTED = 1;
	
	/**
	 * This state will be set when a task has checked all the rules of certain process
	 * 
	 * @var integer
	 */
	const FILTERED = 2;
	
	/**
	 * This state will be set when a task has started executing its actions
	 * 
	 * @var integer
	 */
	const EXECUTINGACTIONS = 3;
	
	/**
	 * This state will be set when a task has finished all its actions
	 * 
	 * @var integer
	 */
	const PROCESSED = 4;
	
	/**
	 * This state will be set when a task did not check any rule or exceeded its weight..
	 * 
	 * @var integer
	 */
	const REJECTED = 5;
	
	/**
	 * This state will be set when a task has failed in any action.
	 * 
	 * @var integer
	 */
	const RETRY = 6;
	
	/**
	 * This state will be set when a task needs confirmation for continue
	 * 
	 * @var integer
	 */
	const PENDINGCONFIRMATION = 7;
	
	/**
	 * This state will be set when a task has finished all its actions but it has warning
	 * 
	 * @var integer
	 */
	const PROCESSEDWITHWARNING = 8;
	
	/**
	 * This state will be set when a state has a error
	 * 
	 * @var integer
	 */
	const ERROR = 9;
	
	/**
	 * This state will be set when a task has been confirmed by someone
	 * 
	 * @var integer
	 */
	const CONFIRMED = 10;
	
	/**
	 * This state will be set when a task was not confirmed by someone
	 * 
	 * @var integer
	 */
	const REJECTEDBYCONFIRMTIMEOUT = 11;
	
	/**
	 * This state will be set when the system has started to collect the task
	 * @var integer
	 */
	const COLLECTING = 12;
	
	/**
	 * This state will be set when the task is in the preprocessor
	 * @var integer
	 */
	const PREPROCESSING = 13;
	
	/**
	 * This state will be set when the task has finished the actions of the preprocessor
	 * @var integer
	 */
	const PREPROCESSED = 14;
	
	
	/**
	 * Identifier within of storage system
	 * 
	 * @var integer
	 */
	protected $idState;
	
	/**
	 * @var string
	 */
	protected $stateName;

	/**
	 * Description of the change to this state
	 * 
	 * @var string
	 */
	protected $description;
	
	/**
	 * Change status timestamp.
	 * 
	 * @var \DateTime
	 */
	private $timeStamp;
	
	public function __construct(int $idState){
		$this->idState = $idState;
		$this->stateName = $this->generateStateName($idState);
		$this->timeStamp = new \DateTime();
		$this->description = $this->generateStateDescription($idState);
	}
	
	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * @return integer
	 */
	public function getIdState() {
		return $this->idState;
	}

	/**
	 * @return string
	 */
	public function getStateName() {
		return $this->stateName;
	}
	
	/**
	 * Gets state timestamp
	 * @return \DateTime $value
	 */
	public function getTimeStamp(){
		return $this->timeStamp;
	}

	/**
	 * @param integer $idState
	 */
	public function setIdState($idState) {
		$this->idState = $idState;
	}

	/**
	 * @param string $stateName
	 */
	public function setStateName($stateName) {
		$this->stateName = $stateName;
	}
	
	/**
	 * Sets state timestamp
	 * @param \DateTime $value
	 * @return
	 */
	public function setTimeStamp($value){
		$this->timeStamp = $value;
	}
	
	/**
	 * Return state name according to the identifier
	 * 
	 * @param int $idState
	 * @return string
	 */
	public function generateStateName(int $idState){
		switch ($idState){
			case 1:
				return 'COLLECTED';
				break;
			case 2:
				return 'FILTERED';
				break;
			case 3:
				return 'EXECUTINGACTIONS';
				break;
			case 4:
				return 'PROCESSED';
				break;
			case 5:
				return 'REJECTED';
				break;
			case 6:
				return 'RETRY';
				break;
			case 7:
				return 'PENDINGCONFIRMATION';
				break;
			case 8:
				return 'PROCESSEDWITHWARNING';
				break;
			case 9:
				return 'ERROR';
				break;
			case 10:
				return 'CONFIRMED';
				break;
			case 11:
				return 'REJECTEDBYCONFIRMTIMEOUT';
				break;
			case 12:
				return 'COLLECTING';
				break;
			case 13:
				return 'PREPROCESSING';
				break;
			case 14:
				return 'PREPROCESSED';
				break;
		}
	}
	
	/**
	 * Return state description according to the identifier
	 *
	 * @param int $idState
	 * @return string
	 */
	public function generateStateDescription(int $idState){
		switch ($idState){
			case 1:
				return 'Task collected';
				break;
			case 2:
				return 'Task filtered';
				break;
			case 3:
				return 'The actions are being executed';
				break;
			case 4:
				return 'The task was processed';
				break;
			case 5:
				return 'The task was rejected';
				break;
			case 6:
				return 'Something went wrong';
				break;
			case 7:
				return 'Waiting for confirmation';
				break;
			case 8:
				return 'Task processed with warnings';
				break;
			case 9:
				return 'Something went wrong';
				break;
			case 10:
				return 'The task was confirmed';
				break;
			case 11:
				return 'The task was rejected because it excedeed the confirmation time';
				break;
			case 12:
				return 'Start collecting task';
				break;
			case 13:
				return 'Preprocessing task';
				break;
			case 14:
				return 'The task was preprocessed';
				break;
		}
	}
/*    public function jsonSerialize()
    {
        return array(
            "idState" => $this->idState,
            "description" => $this->description,
            "stateName" => $this->stateName,
        );
    }*/
}
?>