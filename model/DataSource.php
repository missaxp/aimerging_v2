<?php

/**
* @author Ljimenez
* @version 1.0
* created on 27 jun. 2018
*/
namespace model;

class DataSource extends Model{
	/**
	 * Identifier within of storage system
	 * 
	 * @var integer
	 */
	protected $idDataSource;
	
	/**
	 * Source name where was collect the task
	 * 
	 * @var string
	 */
	protected $sourceName;
	
	/**
	 * Source type. ONDEMAND or INQUEUE
	 * 
	 * @var string
	 */
	private $type;
	
	/**
	 * Date when this object was create
	 * 
	 * @var string
	 */
	private $timeStamp;
	
	/**
	 * The method by which this task is received (API, email, TMS, etc.)
	 * 
	 * @var string
	 */
	protected $harvestMethod;
	
	/**
	 * URL of TMS where the task comes from
	 * 
	 * @var string
	 */
	protected $tmsURL;
	
	/**
	 * URL of Task within TMS or Web page
	 * 
	 * @var string
	 */
	protected $tmsTaskUrl;
	
	/**
	 * Data collect of API, mail or FTP in json format
	 * 
	 * @var string
	 */
	private $originalData;
	
	/**
	 * Identifier of Task
	 * 
	 * @var string
	 */
	protected $uniqueSourceId;
	
	/**
	 * Identifier of Source
	 * 
	 * @var string
	 */
	protected $idSource;
	
	/**
	 * The API user that accept the task
	 * @var int
	 */
	private $tmsUserCollector;
	
	public function __construct(){
		
	}
	
	/**
	 * Simulates an overloaded constructor
	 * 
	 * @param string $sourceName
	 * @param string $type
	 * @param string $timeStamp
	 * @param string $harvestMethod
	 * @param string $tmsURL
	 * @param string $tmsTaskUrl
	 * @param string $originalData
	 * @param int $idSource
	 */
	public function construct( $sourceName,  $type,  $timeStamp,  $harvestMethod,
			 $tmsURL,  $tmsTaskUrl,  $originalData,  $idSource,  $tmsUserCollector){
		$this->sourceName = $sourceName;
		$this->type = $type;
		$this->timeStamp = $timeStamp;
		$this->harvestMethod = $harvestMethod;
		$this->tmsURL = $tmsURL;
		$this->tmsTaskUrl = $tmsTaskUrl;
		$this->originalData = $originalData;
		$this->idSource = $idSource;
		$this->tmsUserCollector = $tmsUserCollector;
	}
	
	/**
	 * @return integer
	 */
	public function getIdDataSource() {
		return $this->idDataSource;
	}

	/**
	 * @return string
	 */
	public function getSourceName() {
		return $this->sourceName;
	}

	/**
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @return string
	 */
	public function getTimeStamp() {
		return $this->timeStamp;
	}

	/**
	 * @return string
	 */
	public function getHarvestMethod() {
		return $this->harvestMethod;
	}

	/**
	 * @return string
	 */
	public function getTmsURL() {
		return $this->tmsURL;
	}

	/**
	 * @return string
	 */
	public function getTmsTaskUrl() {
		return $this->tmsTaskUrl;
	}

	/**
	 * @return string
	 */
	public function getOriginalData() {
		return $this->originalData;
	}

	/**
	 * @return string
	 */
	public function getUniqueSourceId() {
		return $this->uniqueSourceId;
	}

	/**
	 * @return integer
	 */
	public function getIdSource() {
		return $this->idSource;
	}

	/**
	 * @param integer $idDataSource
	 */
	public function setIdDataSource($idDataSource) {
		$this->idDataSource = $idDataSource;
	}

	/**
	 * @param string $sourceName
	 */
	public function setSourceName($sourceName) {
		$this->sourceName = $sourceName;
	}

	/**
	 * @param string $type
	 */
	public function setType($type) {
		$this->type = $type;
	}

	/**
	 * @param string $timeStamp
	 */
	public function setTimeStamp($timeStamp) {
		$this->timeStamp = $timeStamp;
	}

	/**
	 * @param string $harvestMethod
	 */
	public function setHarvestMethod($harvestMethod) {
		$this->harvestMethod = $harvestMethod;
	}

	/**
	 * @param string $tmsURL
	 */
	public function setTmsURL($tmsURL) {
		$this->tmsURL = $tmsURL;
	}

	/**
	 * @param string $tmsTaskUrl
	 */
	public function setTmsTaskUrl($tmsTaskUrl) {
		$this->tmsTaskUrl = $tmsTaskUrl;
	}

	/**
	 * @param string $originalData
	 */
	public function setOriginalData($originalData) {
		$this->originalData = $originalData;
	}

	/**
	 * @param string $uniqueSourceId
	 */
	public function setUniqueSourceId($uniqueSourceId) {
		$this->uniqueSourceId = $uniqueSourceId;
	}

	/**
	 * @param integer $idSource
	 */
	public function setIdSource($idSource) {
		$this->idSource = $idSource;
	}
	/**
     * @return number
     */
    public function getTmsUserCollector()
    {
        return $this->tmsUserCollector;
    }

	/**
     * @param number $tmsUserCollector
     */
    public function setTmsUserCollector($tmsUserCollector)
    {
        $this->tmsUserCollector = $tmsUserCollector;
    }

    /*public function jsonSerialize()
    {
        return array(
            //"idDataSource" => $this->idDataSource,
            "idSource" => $this->idSource,
            "harvestMethod" => $this->harvestMethod,
            "sourceName" => $this->sourceName,
            "tmsTaskUrl" => $this->tmsTaskUrl,
            "tmsURL" => $this->tmsURL,
            "type" => $this->type,
        );
    }*/
	

}
?>