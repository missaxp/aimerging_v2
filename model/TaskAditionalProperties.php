<?php

/**
* @author zroque
* @version 1.0
* created on 22 jun. 2018
*/
namespace model;

use JsonSerializable;

class TaskAditionalProperties extends Model implements JsonSerializable {
	
	/**
	 * Identifier in the storage system
	 * 
	 * @var integer
	 */
	protected $idTaskAditionalProperties;
	
	/**
	 * @var string
	 */
	protected $propertyName;
	/**
	 * @var mixed
	 */
	protected $propertyValue;
	
	/**
	 * Identifier of the task to which it belongs
	 * 
	 * @var string
	 */
	protected $uniqueSourceId;
	
    /**
	 * @return integer
	 */
	public function getIdTaskAditionalProperties() {
		return $this->idTaskAditionalProperties;
	}

	/**
	 * @return string
	 */
	public function getPropertyName() {
		return $this->propertyName;
	}

	/**
	 * @return mixed
	 */
	public function getPropertyValue() {
		return $this->propertyValue;
	}

	/**
	 * @return string
	 */
	public function getUniqueSourceId() {
		return $this->uniqueSourceId;
	}

	/**
	 * @param int $idTaskAditionalProperties
	 */
	public function setIdTaskAditionalProperties($idTaskAditionalProperties) {
		$this->idTaskAditionalProperties = $idTaskAditionalProperties;
	}

	/**
	 * @param string $propertyName
	 */
	public function setPropertyName($propertyName) {
		$this->propertyName = $propertyName;
	}

	/**
	 * @param mixed $propertyValue
	 */
	public function setPropertyValue($propertyValue) {
		$this->propertyValue = $propertyValue;
	}

	/**
	 * @param string $uniqueSourceId
	 */
	public function setUniqueSourceId($uniqueSourceId) {
		$this->uniqueSourceId = $uniqueSourceId;
	}

    public function jsonSerialize()
    {
        return array(
            "propertyName" => $this->propertyName,
            "propertyValue" => $this->propertyValue,
        );
    }
}
?>