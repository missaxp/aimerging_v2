<?php

namespace model;

include_once BaseDir . '/model/Model.php';
/**
* @author zroque
* @version 1.0
* created on 20/03/2019
*/
class TM extends Model {

	/***
	 * 
	 * @var int
	 */
	protected $idTaskFilesTM;
	
	/**
	 * *
	 *
	 * @var Language
	 */
	protected $sourceLanguage;

	/**
	 * *
	 *
	 * @var Language
	 */
	protected $targetLanguage;

	/**
	 * *
	 *
	 * @var string
	 */
	protected $version;

	/**
	 * *
	 *
	 * @var bool
	 */
	protected $isMachineTranslation;

	/**
	 * *
	 *
	 * @var File
	 */
	protected $file;
	
	/**
	 * 
	 * @var boolean
	 */
	protected $successfulDownload;

	public function __construct(){

	}

	
	
	/**
     * @return number
     */
    public function getIdTaskFilesTM()
    {
        return $this->idTaskFilesTM;
    }

	/**
     * @param number $idTaskFilesTM
     */
    public function setIdTaskFilesTM($idTaskFilesTM)
    {
        $this->idTaskFilesTM = $idTaskFilesTM;
    }

	/**
	 *
	 * @return \model\Language
	 */
	public function getSourceLanguage(){

		return $this->sourceLanguage;
	}

	/**
	 *
	 * @param \model\Language $sourceLanguage
	 */
	public function setSourceLanguage($sourceLanguage){

		$this->sourceLanguage = $sourceLanguage;
	}

	/**
	 *
	 * @return \model\Language
	 */
	public function getTargetLanguage(){

		return $this->targetLanguage;
	}

	/**
	 *
	 * @param \model\Language $targetLanguage
	 */
	public function setTargetLanguage($targetLanguage){

		$this->targetLanguage = $targetLanguage;
	}

	/**
	 *
	 * @return string
	 */
	public function getVersion(){

		return $this->version;
	}

	/**
	 *
	 * @param string $version
	 */
	public function setVersion($version){

		$this->version = $version;
	}

	/**
	 *
	 * @return boolean
	 */
	public function isIsMachineTranslation(){

		return $this->isMachineTranslation;
	}

	/**
	 *
	 * @param boolean $isMachineTranslation
	 */
	public function setIsMachineTranslation($isMachineTranslation){

		$this->isMachineTranslation = $isMachineTranslation;
	}

	/**
	 *
	 * @return \model\File
	 */
	public function getFile(){

		return $this->file;
	}

	/**
	 *
	 * @param \model\File $file
	 */
	public function setFile($file){

		$this->file = $file;
	}
	/**
     * @return boolean
     */
    public function getSuccessfulDownload()
    {
        return $this->successfulDownload;
    }

	/**
     * @param boolean $successfulDownload
     */
    public function setSuccessfulDownload($successfulDownload)
    {
        $this->successfulDownload = $successfulDownload;
    }

    public function getPath(){
        return $this->file->getPath();
    }
/*
    public function jsonSerialize()
    {
        return array(
            "file" => $this->file,
            "idTaskFilesTM" => $this->idTaskFilesTM,
            "isMachineTranslation" => $this->isMachineTranslation,
            "sourceLanguage" => $this->sourceLanguage,
            "successfulDownload" => $this->successfulDownload,
            "targetLanguage" => $this->targetLanguage,
            "version" => $this->version,
        );
    }*/
}

