<?php

/**
* @author Ljimenez
* @version 1.0
* created on 21 jun. 2018
*/
namespace model;

use Functions;
use JsonSerializable;

class Analysis extends Model implements JsonSerializable {
	
	/**
	 * Identifier within of storage system
	 * 
	 * @var integer
	 */
	protected $idAnalis;
	/**
	 * 
	 * @var integer
	 */
	protected $percentage_101;
	/**
	 * 
	 * @var integer
	 */
	protected $percentage_100;
	/**
	 * 
	 * @var integer
	 */
	protected $percentage_95;
	/**
	 * 
	 * @var integer
	 */
	protected $percentage_85;
	/**
	 * 
	 * @var integer
	 */
	protected $percentage_75;
	
	/**
	 * 
	 * @var integer
	 */
	protected $percentage_50;
	
	/**
	 * 
	 * @var integer
	 */
	protected $machineTranslation;
	
	/**
	 * Minutes to complete the translation
	 * 
	 * @var integer
	 */
	protected $minute;
	
	/**
	 * 
	 * @var float
	 */
	protected $weightedWords;
	
	/**
	 * Repeated words
	 * 
	 * @var integer
	 */
	protected $repetition;
	
	/**
	 * New words
	 * 
	 * @var integer
	 */
	protected $notMatch;
	
	/**
	 * 
	 * @var integer
	 */
	protected $totalWords;
	
	/**
	 * This variable must remain private, useful for the change in overwrite
	 * @var integer
	 */
	private $WeightedWordsTresshold;
	private $idCreateAction;
	
	public function __construct(){
        $this->setFieldsToZero();
    }
	
	
	
	/**
	 * @return integer
	 */
	public function getTotalWords() {
		return $this->totalWords;
	}

	/**
	 * @param integer $totalWords
	 */
	public function setTotalWords($totalWords) {
		$this->totalWords = $totalWords;
	}

	/**
	 * @return int
	 */
	public function getIdAnalis() {
		return $this->idAnalis;
	}

	/**
	 * @return int
	 */
	public function getPercentage_101() {
		return $this->percentage_101;
	}

	/**
	 * @return int
	 */
	public function getPercentage_100() {
		return $this->percentage_100;
	}

	/**
	 * @return int
	 */
	public function getPercentage_95() {
		return $this->percentage_95;
	}

	/**
	 * @return int
	 */
	public function getPercentage_85() {
		return $this->percentage_85;
	}

	/**
	 * @return int
	 */
	public function getPercentage_75() {
		return $this->percentage_75;
	}

	/**
	 * @return int
	 */
	public function getPercentage_50() {
		return $this->percentage_50;
	}

	/**
	 * @return int
	 */
	public function getMachineTanslation() {
		return $this->machineTranslation;
	}

	/**
	 * @return int
	 */
	public function getMinute() {
		return $this->minute;
	}

	/**
	 * @return float
	 */
	public function getWeightedWord() {
		return $this->weightedWords;
	}

	/**
	 * @return int
	 */
	public function getRepetition() {
		return $this->repetition;
	}

	/**
	 * @return int
	 */
	public function getNotMatch() {
		return $this->notMatch;
	}

	/**
	 * @param int $idAnalis
	 */
	public function setIdAnalis($idAnalis) {
		$this->idAnalis = $idAnalis;
	}

	/**
	 * @param int $percentage_101
	 */
	public function setPercentage_101($percentage_101) {
		$this->percentage_101 = $percentage_101;
	}

	/**
	 * @param int $percentage_100
	 */
	public function setPercentage_100($percentage_100) {
		$this->percentage_100 = $percentage_100;
	}

	/**
	 * @param int $percentage_95
	 */
	public function setPercentage_95($percentage_95) {
		$this->percentage_95 = $percentage_95;
	}

	/**
	 * @param int $percentage_85
	 */
	public function setPercentage_85($percentage_85) {
		$this->percentage_85 = $percentage_85;
	}

	/**
	 * @param int $percentage_75
	 */
	public function setPercentage_75($percentage_75) {
		$this->percentage_75 = $percentage_75;
	}

	/**
	 * @param int $percentage_50
	 */
	public function setPercentage_50($percentage_50) {
		$this->percentage_50 = $percentage_50;
	}

	/**
	 * @param int $machineTanslation
	 */
	public function setMachineTanslation($machineTanslation) {
		$this->machineTranslation = $machineTanslation;
	}

	/**
	 * @param int $minute
	 */
	public function setMinute($minute) {
		$this->minute = $minute;
	}

	/**
	 * @param float $weightedWord
	 */
	public function setWeightedWord($weightedWord) {
		$this->weightedWords = $weightedWord;
	}

	/**
	 * @param int $repetition
	 */
	public function setRepetition($repetition) {
		$this->repetition = $repetition;
	}

	/**
	 * @param int $notMatch
	 */
	public function setNotMatch($notMatch) {
		$this->notMatch = $notMatch;
	}
	
	

	/**
     * @return number
     */
    public function getWeightedWordsTresshold()
    {
        return $this->WeightedWordsTresshold;
    }

	/**
     * @param number $WeightedWordsTresshold
     */
    public function setWeightedWordsTresshold($WeightedWordsTresshold)
    {
        $this->WeightedWordsTresshold = $WeightedWordsTresshold;
    }
    
    

	/**
     * @return mixed
     */
    public function getIdCreateAction()
    {
        return $this->idCreateAction;
    }

	/**
     * @param mixed $idCreateAction
     */
    public function setIdCreateAction($idCreateAction)
    {
        $this->idCreateAction = $idCreateAction;
    }

	public function calculateTotalWords(){
		$this->totalWords=0;
		$this->totalWords += $this->notMatch + $this->percentage_101 + $this->percentage_100 + $this->percentage_95 + $this->percentage_85 + $this->percentage_75 +
		$this->percentage_50 + $this->repetition + 	$this->machineTranslation;
	}
	
	// /**
	//  * Appends th machine translation words to the fuzzy given as parameter
	//  * @param string $fuzzy
	//  * @return
	//  */
	// public function appendMachineTranslation(string $fuzzy){
	// 	$mTWords = $this->machineTranslation;
	// 	switch(strtoupper($fuzzy)){
	// 		case "P101":
	// 			$this->percentage_101+=$mTWords;
	// 			break;
	// 		case "P100":
	// 			$this->percentage_100+=$mTWords;
	// 			break;
	// 		case "P95":
	// 			$this->percentage_95+=$mTWords;
	// 			break;
	// 		case "P85":
	// 			$this->percentage_85+=$mTWords;
	// 			break;
	// 		case "P75":
	// 			$this->percentage_75+=$mTWords;
	// 			break;
	// 		case "P50":
	// 			$this->percentage_50+=$mTWords;
	// 			break;
	// 		case "NOMATCH":
	// 			$this->notMatch+=$mTWords;
	// 			break;
	// 		case "REP":
	// 			$this->repetition+=$mTWords;
	// 			break;
	// 	}
	// 	$this->machineTranslation = 0;
	// }

	/**
	 *  Change the analysis according to the fuzzy relation  
	 *  @param array $fuzzy_relation
 	 *  @return
	 * 		
	 * 
	 */
	public function appendFuzzyRelations($fuzzy_relation){
		$fuzzy_relation = array_slice($fuzzy_relation, 2); 
		$old = clone($this);
		$this->setFieldsToZero();
		foreach($fuzzy_relation as $key=>$value){
			$isZero = false;
			switch($value){
				case "XTranslated":
					$value = "percentage_101";
					break;
				case "100%":
					$value = "percentage_100";
					break;
				case "95%":
					$value = "percentage_95";
					break;
				case "85%":
					$value = "percentage_85";
					break;
				case "75%":
					$value = "percentage_75";
					break;
				case "50%":
					$value = "percentage_50";
					break;
				case "Hours":
					$value = "minute";
					break;
				case "Repetition":
					$value = "repetition";
					break;
				case "New Words":
					$value = "notMatch";
					break;
				case "MT":
					$value = "machineTranslation";
					break;
				case 'ignorar':
					$isZero = true;
					break;
				default:
					continue;
			}	
			if(!$isZero){
				$this->$value = $old->$key + $this->$value;
			}else{
				$this->$value = 0;
			}
		}
		$this->calculateTotalWords();
		$this->calculateWeightedWords();	

	}




	
	public function calculateWeightedWords(){
		$this->weightedWords = $this->notMatch * 1 + ($this->repetition+$this->percentage_100) * 6/100 +
        $this->percentage_95*25/100+$this->percentage_85*45/100+$this->percentage_75*50/100;
	}
     public function jsonSerialize()
     {
         return array(
             //"idAnalis" => $this->idAnalis,
             "machineTranslation" => $this->machineTranslation,
             "minute" => $this->minute,
             "notMatch" => $this->notMatch,
             "percentage_100" => $this->percentage_100,
             "percentage_101" => $this->percentage_101,
             "percentage_50" => $this->percentage_50,
             "percentage_75" => $this->percentage_75,
             "percentage_85" => $this->percentage_85,
             "percentage_95" => $this->percentage_95,
             "repetition" => $this->repetition,
             "totalWords" => $this->totalWords,
             "weightedWords" => $this->weightedWords,
         );
     }

    private function setFieldsToZero(): void
    {
        $this->minute = 0;
        $this->notMatch = 0;
        $this->percentage_101 = 0;
        $this->percentage_100 = 0;
        $this->percentage_95 = 0;
        $this->percentage_85 = 0;
        $this->percentage_75 = 0;
        $this->percentage_50 = 0;
        $this->repetition = 0;
        $this->weightedWords = 0.0;
        $this->machineTranslation = 0;
        $this->totalWords = 0;
        $this->WeightedWordsTresshold = 0;
        $this->idCreateAction = 0;
    }
}
?>