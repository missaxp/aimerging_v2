<?php

/**
 *
 * @author zroque
 * @version 1.0
 *          created on 21 jun. 2018
 */
namespace model;

use behaviorModule\tms\idcp\idcp;
use behaviorModule\tms\idcp\IDCPtaskType;
use dataAccess\dao\ProcessDAO;
use DateTime;
use Exception;
use Functions;

include_once BaseDir . '/model/Model.php';
include_once BaseDir . "/behaviorModule/tms/idcp.php";
class Rule extends Model {
    protected $idRule;
    protected $weight;
    protected $ruleTimeStamp;
    protected $idProcess;
    protected $description;
    protected $enabled;
    protected $value;
    protected $property;
    protected $operator;
    protected $priority;
    /**
	 * Rule properties
	 */
	private const SOURCE = "SOURCE";

    private const SOURCELANGUAGE = "SOURCELANGUAGE";
    private const STARTDATE = "STARTDATE";
    private const DUEDATE = "DUEDATE";
    private const PROJECT = "PROJECT";
    private const CATEGORY = "CATEGORY";
    private const TOTALWORDS = "TOTALWORDS";
    private const TARGETLANGUAGES = "TARGETLANGUAGES";
    private const TITLE = "TITLE";
    private const USER = "USER";
    private const MAX_LOAD = "WORKGROUP_MAX_LOAD";
    private const EXECUTION_TYPE = "EXECUTION_TYPE";
    private const ASSIGNEDUSER = "ASSIGNEDUSER";
    const IDCLIENTTASK = "IDCLIENTTASK";
    const WEIGHTEDWORD = "WEIGHTEDWORD";

    /**
	 * Rule operators
	 */
	const EQUALS = "EQUALS";
	const LESS = "LESS";
	const LESSEQUALS = "LESSEQUALS";
	const GREATER = "GREATER";
	const GREATEREQUALS = "GREATEREQUALS";
	const CONTAINS = "CONTAINS";
	const NOTCONTAINS = "NOTCONTAINS";

	public function __construct(){

	}

	/**
	 *
	 * @return int
	 */
	public function getIdRule(){

		return $this->idRule;
	}

	/**
	 *
	 * @return number
	 */
	public function getWeight(){

		return $this->weight;
	}

	/**
	 *
	 * @return string
	 */
	public function getRuleTimeStamp(){

		return $this->ruleTimeStamp;
	}

	/**
	 *
	 * @return int
	 */
	public function getIdProcess(){

		return $this->idProcess;
	}

	/**
	 *
	 * @return string
	 */
	public function getDescription(){

		return $this->description;
	}

	/**
	 *
	 * @return bool
	 */
	public function getEnabled(){

		return $this->enabled;
	}

	/**
	 *
	 * @return string
	 */
	public function getValue(){

		return $this->value;
	}

	/**
	 *
	 * @return string
	 */
	public function getProperty(){

		return $this->property;
	}

	/**
	 *
	 * @return string
	 */
	public function getOperator(){

		return $this->operator;
	}

	public function setIdRule($idRule){

		$this->idRule = $idRule;
	}

	public function setWeight($weight){

		$this->weight = $weight;
	}

	public function setRuleTimeStamp($ruleTimeStamp){

		$this->ruleTimeStamp = $ruleTimeStamp;
	}

	public function setIdProcess($idProcess){

		$this->idProcess = $idProcess;
	}

	public function setDescription($description){

		$this->description = $description;
	}

	public function setEnabled($enabled){

		$this->enabled = $enabled;
	}

	public function setValue($value){

		$this->value = $value;
	}

	public function setProperty($property){

		$this->property = $property;
	}

	public function setOperator($operator){

		$this->operator = $operator;
	}

	/**
	 *
	 * @return int
	 */
	public function getPriority(){

		return $this->priority;
	}

	public function setPriority($priority){

		$this->priority = $priority;
	}

	/**
	 * Verifies if a task match this rule
	 *
	 * @param Task $task
	 * @return boolean true if match, false otherwise
	 */
	public function check(Task $task){

		$check = false;
		switch (strtoupper($this->property)) {
			case self::SOURCE :
				$value = $task->getDataSource()->getSourceName();
				$check = $this->checkOperator($value, $this->operator, $this->value);
				break;
			case self::SOURCELANGUAGE :
				$check = $this->checkOperator($task->getSourceLanguage()->getIdLanguage(), $this->operator, $this->getValue());
				break;
			case self::STARTDATE :
				$check = $this->checkOperator($task->getStartDate(), $this->operator, $this->getValue());
				break;
			case self::DUEDATE :
				$check = $this->checkOperator($task->getDueDate()->format("Y-m-d H:i:s"), $this->operator, $this->getValue());
				break;
			case self::PROJECT :
				$check = $this->checkOperator($task->getProject(), $this->operator, $this->getValue());
				break;
			case self::CATEGORY :
				$check = $this->checkOperator($task->getCategory()->getCategoryName(), $this->operator, $this->getValue());
				break;
			case self::TITLE:
				$check = $this->checkOperator($task->getTitle(), $this->operator, $this->getValue());
				break;
			case self::TOTALWORDS : //No es total words sino Weighted Words. Modifico el método pero no nos confundamos con el nobmre de variable incorrecto
				foreach($task->getTargetsLanguage() as $targetLanguage) {
					$check = $this->checkOperator($targetLanguage->getAnalysis()->getWeightedWord(), $this->operator, $this->getValue());
				}
				break;
            case self::WEIGHTEDWORD: //ASI ES COMO DEBERÍA DE FUNCIONAR. HAY QUE CAMBIAR LOS REGISTRO EN LA BD Y ACTUALIZAR LA UI.
                foreach($task->getTargetsLanguage() as $targetLanguage) {
                    $check = $this->checkOperator($targetLanguage->getAnalysis()->getWeightedWord(), $this->operator, $this->getValue());
                }
                break;
			case self::TARGETLANGUAGES :
				$languages = $task->getTargetsLanguage();
				foreach($languages as $target) {
					$check = $this->checkOperator($target->getIdLanguage(), $this->operator, $this->getValue());
				}
				break;
			case self::USER :
				$check = $this->checkOperator($task->getDataSource()->getTmsUserCollector(), $this->operator, $this->getValue());
				break;
            case self::ASSIGNEDUSER :
                $check = $this->checkOperator($task->getAssignedUser(), $this->operator, $this->getValue());
                break;
            case self::IDCLIENTTASK:
                $check = $this->checkOperator($task->getIdClientetask(), $this->operator, $this->getValue());
                break;
            case self::MAX_LOAD:
                $processDao = new ProcessDAO();
                $currentLoad = $processDao->getGlobalWorkLoad();
                $possibleLoad = $currentLoad + self::calculateForecast($task, $this->idProcess);
                $check = $this->checkOperator($possibleLoad, self::LESSEQUALS, MAX_LOAD);
                break;
            case self::EXECUTION_TYPE:
                $check = $this->checkOperator($task->getDataSource()->getType(), $this->operator, $this->getValue());
                break;
            default :
				if (! empty($task->getAditionalProperties())) {
					foreach($task->getAditionalProperties() as $property) {
						if ($this->property == $property->getPropertyName()) {
							$check = $this->checkOperator($property->getPropertyValue(), $this->operator, $this->value);
						}
					}
				} else {
					$check = false;
				}
				break;
		}
		return $check;
	}

	/**
	 * Compares two values using the specified operator
	 *
	 * @param mixed $value
	 * @param mixed $operator
	 * @param mixed $compareTo
	 * @return
	 */
	public function checkOperator($value, $operator, $compareTo){

		if (! is_numeric($value)) {
			if (\Functions::is_date($value)) {
				try {
					$value = DateTime::createFromFormat('Y-m-d H:i:s', $value);
					$valueCopy = clone ($value);
					$compareTo = $valueCopy->modify($compareTo);
					
					// $compareTo = \DateTime::createFromFormat('Y-m-d H:i:s', $compareTo)
				} catch(Exception $ex) {
					$ex->getMessage();
				}
			}
		}
		
		$check = false;
		switch (strtoupper($operator)) {
			case self::LESS :
				if ($value < $compareTo) {
					$check = true;
				}
				break;
			case self::EQUALS :
				if (is_string($value)) {
					if (strcasecmp($value, $compareTo) === 0) {
						$check = true;
					}
				} else {
					if ($value == $compareTo) {
						$check = true;
					}
				}
				break;
			case self::LESSEQUALS :
				if ($value <= $compareTo) {
					$check = true;
				}
				break;
			case self::GREATER :
				if ($value > $compareTo) {
					$check = true;
				}
				break;
			case self::GREATEREQUALS :
				if ($value >= $compareTo) {
					$check = true;
				}
				break;
			case self::CONTAINS :
				if (strpos(strtoupper($compareTo), strtoupper($value . '')) !== false || strpos(strtoupper($value . ''),strtoupper($compareTo) )!== false) {
					$check = true;
				}
				break;
			case self::NOTCONTAINS :
				if(strpos(strtoupper($compareTo), strtoupper($value.''))===false){
					$check = true;
				}
				break;
			default :
				$check = false;
				break;
		}
		return $check;
	}

    /**
     * @param Task $task
     * @param $idProcess
     * @return int
     */
    public static function calculateForecast(Task $task, $idProcess){
        $forecast = 0;
        $processDao = new ProcessDAO();
        $actionProperties = $processDao->getActionCreatePropertiesByProcess($idProcess);
        $neededProperties = array(
            "tarifa" => null,
            "tipus_tasca" => null,
        );

        if(empty($actionProperties)){
            $forecast = $task->getTargetsLanguage()[0]->getAnalysis()->getWeightedWord() / 300;
        } else {
            foreach ($actionProperties as $property){
                switch (($property->getPropertyName())){
                    case "tarifa":
                        $neededProperties["tarifa"] = $property->getPropetyValue();
                        break;
                    case "tipus_tasca":
                        $neededProperties["tipus_tasca"] = $property->getPropetyValue();
                        break;
                }
            }

            $idcpObj = new idcp($task);
            $idcpObj->setTargetLanguages($task->getTargetsLanguage());
            $idcpObj->setWordCount($task->getTargetsLanguage()[0]->getAnalysis());
            $forecast = $idcpObj->calculateForecast($neededProperties["tipus_tasca"], $neededProperties["tarifa"], $neededProperties["tipus_tasca"]==IDCPtaskType::_TR);
        }
        return $forecast;

    }
    /*    public function jsonSerialize()
        {
            return get_object_vars($this);
        }*/
}

?>