<?php

/**
* @author zroque
* @version 1.0
* created on 21 jun. 2018
*/
namespace model;

class APIUser extends Model{
	/**
	 * Identifier within of storage system
	 * 
	 * @var int
	 */
	protected $idApiUser;
	
	/**
	 * 
	 * @var string
	 */
	protected $userName;
	
	/**
	 * 
	 * @var string
	 */
	protected $passWord;
	
	/**
	 * 
	 * @var int
	 */
	protected $idSource;
	
	/**
	 * 
	 * @var string
	 */
	protected $pmName;
	
	/**
	 * 
	 * @var string
	 */
	protected $token;
	
	/**
	 * Priority to be used
	 * 
	 * @var int
	 */
	protected $priority;
	
	protected $timeLife;
	
	protected $clientSecret;
	
	/**
     * @return mixed
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }

	/**
     * @param mixed $clientSecret
     */
    public function setClientSecret($clientSecret)
    {
        $this->clientSecret = $clientSecret;
    }

	/**
     * @return mixed
     */
    public function getTimeLife()
    {
        return $this->timeLife;
    }

	/**
     * @param mixed $timeLife
     */
    public function setTimeLife($timeLife)
    {
        $this->timeLife = $timeLife;
    }

	/**
	 * @return integer
	 */
	public function getPriority() {
		return $this->priority;
	}

	/**
	 * @param integer $priority
	 */
	public function setPriority($priority) {
		$this->priority = $priority;
	}

	/**
	 * @return integer
	 */
	public function getIdApiUser() {
		return $this->idApiUser;
	}

	/**
	 * @return string
	 */
	public function getUserName() {
		return $this->userName;
	}

	/**
	 * @return string
	 */
	public function getPassWord() {
		return $this->passWord;
	}

	/**
	 * @return integer
	 */
	public function getIdSource() {
		return $this->idSource;
	}

	/**
	 * @return string
	 */
	public function getPmName() {
		return $this->pmName;
	}

	/**
	 * @return string
	 */
	public function getToken() {
		return $this->token;
	}

	/**
	 * @param integer $idApiUser
	 */
	public function setIdApiUser($idApiUser) {
		$this->idApiUser = $idApiUser;
	}

	/**
	 * @param string $userName
	 */
	public function setUserName($userName) {
		$this->userName = $userName;
	}

	/**
	 * @param string $passWord
	 */
	public function setPassWord($passWord) {
		$this->passWord = $passWord;
	}

	/**
	 * @param integer $idSource
	 */
	public function setIdSource($idSource) {
		$this->idSource = $idSource;
	}

	/**
	 * @param string $pmName
	 */
	public function setPmName($pmName) {
		$this->pmName = $pmName;
	}

	/**
	 * @param string $token
	 */
	public function setToken($token) {
		$this->token = $token;
	}


    /*public function jsonSerialize()
    {
        return array(
            //"idApiUser" => $this->idApiUser,
            "clientSecret" => $this->clientSecret,
            "idSource" => $this->idSource,
            "passWord" => $this->passWord,
            "pmName" => $this->pmName,
            "priority" => $this->priority,
            "timeLife" => $this->timeLife,
            "token" => $this->token,
            "userName" => $this->userName,
        );
    }*/


	
}
?>