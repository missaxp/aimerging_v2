<?php

namespace model;

use core\AI;
use preprocessorModule\memoQ\MemoQTmManager;
use preprocessorModule\memoQ\MemoQFileManager;
use preprocessorModule\memoQ\MemoQProjectManager;
use dataAccess\dao\TaskDAO;

include_once BaseDir . '/CATTools/memoQ/MemoQProjectManager.php';
include_once BaseDir . '/CATTools/memoQ/MemoQFileManager.php';
include_once BaseDir . '/CATTools/memoQ/MemoQSecurityManager.php';
include_once BaseDir . '/CATTools/memoQ/MemoQTmManager.php';

/**
 *
 * @author zroque
 * @version 1.0
 *          created on 12/03/2019
 */
class MemoQToolSetup {

    /**
     *
     * @var int
     */
    protected $idProcess;

    /**
     *
     * @var boolean
     */
    protected $feedTM;

    /**
     *
     * @var string
     */
    protected $tmName;

    /**
     *
     * @var string
     */
    protected $tmGuid;

    /**
     *
     * @var boolean
     */
    protected $uploadSourceFileToProject;

    /**
     *
     * @var string
     */
    protected $projectName;

    /**
     *
     * @var string
     */
    protected $projectGuid;

    /**
     *
     * @var boolean
     */
    protected $analyzeSourceFile;

    /**
     *
     * @var boolean
     */
    protected $updateTaskWordCount;
    private $tmManager;
    private $projectManager;
    private $fileManager;
    private $errors = array();

    public function __construct() {

        $this->tmManager = new MemoQTmManager();
        $this->projectManager = new MemoQProjectManager();
        $this->fileManager = new MemoQFileManager();
    }

    /**
     *
     * @return number
     */
    public function getIdProcess() {

        return $this->idProcess;
    }

    /**
     *
     * @param number $idProcess
     */
    public function setIdProcess($idProcess) {

        $this->idProcess = $idProcess;
    }

    /**
     *
     * @return boolean
     */
    public function getFeedTM() {

        return $this->feedTM;
    }

    /**
     *
     * @param boolean $feedTM
     */
    public function setFeedTM($feedTM) {

        $this->feedTM = $feedTM;
    }

    /**
     *
     * @return string
     */
    public function getTmName() {

        return $this->tmName;
    }

    /**
     *
     * @param string $tmName
     */
    public function setTmName($tmName) {

        $this->tmName = $tmName;
    }

    /**
     *
     * @return boolean
     */
    public function getUploadSourceFileToProject() {

        return $this->uploadSourceFileToProject;
    }

    /**
     *
     * @param boolean $uploadSourceFileToProject
     */
    public function setUploadSourceFileToProject($uploadSourceFileToProject) {

        $this->uploadSourceFileToProject = $uploadSourceFileToProject;
    }

    /**
     *
     * @return string
     */
    public function getProjectName() {

        return $this->projectName;
    }

    /**
     *
     * @param string $projectName
     */
    public function setProjectName($projectName) {

        $this->projectName = $projectName;
    }

    /**
     *
     * @return boolean
     */
    public function getAnalyzeSourceFile() {

        return $this->analyzeSourceFile;
    }

    /**
     *
     * @param boolean $analyzeSourceFile
     */
    public function setAnalyzeSourceFile($analyzeSourceFile) {

        $this->analyzeSourceFile = $analyzeSourceFile;
    }

    /**
     *
     * @return boolean
     */
    public function updateTaskWordCount() {

        return $this->updateTaskWordCount;
    }

    /**
     *
     * @param boolean $updateTaskWordCount
     */
    public function setUpdateTaskWordCount($updateTaskWordCount) {

        $this->updateTaskWordCount = $updateTaskWordCount;
    }

    /**
     * @return string
     */
    public function getTmGuid() {
        return $this->tmGuid;
    }

    /**
     * @param string $tmGuid
     */
    public function setTmGuid($tmGuid) {
        $this->tmGuid = $tmGuid;
    }

    /**
     * @return string
     */
    public function getProjectGuid() {
        return $this->projectGuid;
    }

    /**
     * @param string $projectGuid
     */
    public function setProjectGuid($projectGuid) {
        $this->projectGuid = $projectGuid;
    }

    public function getErrors() {

        if ($this->tmManager->hasErrors()) {
            $this->errors = array_merge($this->errors, $this->tmManager->getErrors());
        }
        if ($this->projectManager->hasErrors()) {
            $this->errors = array_merge($this->errors, $this->projectManager->getErrors());
        }
        if ($this->fileManager->hasErrors()) {
            $this->errors = array_merge($this->errors, $this->fileManager->getErrors());
        }
        return $this->errors;
    }

    public function executeMemoqActions(Task $task) {

        $newWC = null;
        $fileGuids = null;

        if ($this->feedTM) {
            if ($this->tmName !== null && $this->tmGuid !== null) {
                if (!empty($task->getTranslationMemories())) {
                    foreach ($task->getTranslationMemories() as $tm) {
                        //Don't add the MT Translation Memory
                        if (strpos($tm->getFile()->getFileName(), "MT") === false) {
                            $this->tmManager->importFileToTM($this->tmGuid, $tm->getFile()->getPath());
                        }

                        \Functions::console("Feeding TM");
                    }
                }
            } else {
                $this->tmManager->addError("Cannot feed TM, no TM name has been set");
            }
        }
        if ($this->uploadSourceFileToProject) {
            if ($this->projectName !== null && $this->projectGuid !== null) {

                $sourcePath = $this->getSourceFile($task->getFilesToTranslate());
                if ($sourcePath !== null) {
                    $fileGuid = $this->fileManager->uploadFile($sourcePath);
                    if ($fileGuid !== null) {

                        $fileGuids = $this->projectManager->addFilesToProject($this->projectGuid, array(
                            $fileGuid
                        ));
                    }
                } else {
                    $this->projectManager->addError("Error uploading file to server");
                }
            } else {
                $this->projectManager->addError("Cannot upload source file, No project has been set");
            }
        }
        if ($this->analyzeSourceFile && $this->projectGuid !== null) {
            // TODO::si no se debe subir al proyecto hay que crear un proyecto vacío para hacer el análisis

            if ($this->uploadSourceFileToProject) {

                if ($fileGuids !== null) {
                    $analysisOptions["projectTM"] = true;
                    $wc = $this->projectManager->getDocumentsAnalysis($this->projectGuid, $fileGuids, $analysisOptions);
                    if ($wc !== null) {

                        $newWC = new Analysis();
                        $newWC->setPercentage_101($wc["101"]);
                        $newWC->setPercentage_100($wc["100"]);
                        $newWC->setPercentage_95($wc["95"]);
                        $newWC->setPercentage_85($wc["85"]);
                        $newWC->setPercentage_75($wc["75"]);
                        $newWC->setPercentage_50($wc["50"]);
                        $newWC->setRepetition($wc["repetitions"]);
                        $newWC->setNotMatch($wc["noMatch"]);
                        $newWC->calculateTotalWords();
                        $newWC->calculateWeightedWords();
                    } else {
                        $this->projectManager->addError("There was an error analyzing the source file");
                    }
                } else {
                    $this->projectManager->addError("Error adding files to project");
                }
            } else {
                $projectInfo = array();
                $projectInfo["CreatorUser"] = "58200521-e12e-e911-9460-00155d640756";
                $date = new \DateTime();
                $date->setTimezone(new \DateTimeZone(AI::getInstance()->getSetup()->timeZone_TMS));

                $projectInfo["Name"] = "TEST_AI_PROJECT_" . $date->format("YmdHis");
                $sourceLang = $this->projectManager->getMemoqLanguageCode($task->getSourceLanguage()->getIdLanguage());
                $targetLang = $this->projectManager->getMemoqLanguageCode($task->getTargetsLanguage()[0]->getIdLanguage());
                if ($sourceLang !== null && $targetLang !== null) {
                    $projectInfo["SourceLanguage"] = $sourceLang;
                    $projectInfo["TargetLanguages"] = array(
                        $targetLang
                    );
                }

                $tempProjectGuid = $this->projectManager->createProject($projectInfo);
                if ($tempProjectGuid !== null) {
                    $fileGuid = $this->fileManager->uploadFile($this->getSourceFile($task->getFilesToTranslate()));
                    if ($fileGuid !== null) {
                        $fileGuids = $this->projectManager->addFilesToProject($tempProjectGuid, array(
                            $fileGuid
                        ));
                        if ($fileGuids !== null) {

                            $wc = $this->projectManager->getDocumentsAnalysis($tempProjectGuid, $fileGuids);
                            if ($wc !== null) {
                                $newWC = new Analysis();
                                $newWC->setPercentage_101($wc["101"]);
                                $newWC->setPercentage_100($wc["100"]);
                                $newWC->setPercentage_95($wc["95"]);
                                $newWC->setPercentage_85($wc["85"]);
                                $newWC->setPercentage_75($wc["75"]);
                                $newWC->setPercentage_50($wc["50"]);
                                $newWC->setRepetition($wc["repetitions"]);
                                $newWC->setNotMatch($wc["noMatch"]);
                                $newWC->calculateTotalWords();
                                $newWC->calculateWeightedWords();
                            } else {
                                $this->projectManager->addError("Error analyzing source file");
                            }
                        } else {
                            $this->projectManager->addError("Error adding the files to project");
                        }
                    } else {
                        $this->projectManager->addError("Error uploading the files to server");
                    }
                } else {
                    $this->projectManager->addError("Error when creating empty project to analyze file");
                }
                $this->projectManager->deleteProject($tempProjectGuid);
            }
        } else {
            $this->projectManager->addError("Cannot analyze file, no project has been set");
        }
        if ($newWC !== null) {
            //if ($task->getAnalysis()[0]->getWeightedWord() > $newWC->getWeightedWord() && $newWC->getWeightedWord() > 0 && $this->updateTaskWordCount) {
            if ($task->getTargetsLanguage()[0]->getAnalysis()->getWeightedWord() > $newWC->getWeightedWord() && $newWC->getWeightedWord() > 0 && $this->updateTaskWordCount) {

                $taskDao = new TaskDAO();
                foreach ($task->getTargetsLanguage()[0]->getAnalysis() as $analysis) {
                    $newWC->setIdAnalis($analysis->getIdAnalis());
                    $taskDao->updateAnalysis($newWC);
                }
            }
        }
    }

    /**
     *
     * @param File[] $files
     * @return
     */
    public function getSourceFile($files) {

        $sourcePath = null;
        foreach ($files as $file) {
            $zip = new \ZipArchive();
            $openResult = $zip->open($file->getPath());
            if ($openResult === true) {
                $numFiles = $zip->numFiles;
                for ($i = 0; $i < $numFiles; $i++) {
                    $fileName = $zip->getNameIndex($i);
                    if (preg_match('#\.(xlf)$#i', $fileName)) {
                        $fileInfo = pathinfo($fileName);
                        $zipFileInfo = pathinfo($file->getPath());
                        $tmName = $fileInfo['basename'];
                        $path = $zipFileInfo["dirname"];
                        copy("zip://" . $file->getPath() . "#" . $fileName, $path . "/" . $tmName);
                        $sourcePath = $path . "/" . $tmName;
                    }
                }
            }
            $zip->close();
        }

        return $sourcePath;
    }
}

